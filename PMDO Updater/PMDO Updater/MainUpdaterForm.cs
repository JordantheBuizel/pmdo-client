﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace PMDO_Updater
{
    public partial class MainUpdaterForm : Form
    {

        #region button1Constructors

        public List<int> missingRevisions = new List<int>();
        public bool revisionsXMLLatest = false;
        public int latestUpdateRevision = 0;
        public List<string> missingRevisionsStrings = new List<string>();
        public List<string> revisionBuilder = new List<string>();
        public List<string> updateTypes = new List<string>();
        public List<string> updateNotes = new List<string>();
        public List<string> installedRevisionsList = new List<string>();

        #endregion

        LoadingDialg dia;

        public MainUpdaterForm()
        {
            InitializeComponent();

            dia = new LoadingDialg()
            {
                Location = new Point(Location.X + 30, Location.Y + 40)
            };
            LocationChanged += new EventHandler(MainUpdaterForm_LocationChanged);
            dia.Show(this);

            //backgroundWorker1.RunWorkerAsync();
            UpdatePMDO();

        }

        private void MainUpdaterForm_LocationChanged(object sender, EventArgs e)
        {
            dia.Location = new Point(Location.X + 30, Location.Y + 24);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void UpdatePMDO()
        {
            bool isnotFirstTimeInstall = File.Exists(Path.Combine(Application.StartupPath, "InstalledUpdates.xml"));
            string domain = "pmdonlineupdates.serveftp.com";
            int bytesRead = 0;
            byte[] buffer = new byte[2048];
            string UpdateLocation = Path.Combine(Application.StartupPath, "UpdaterCache");

            //Check if main domain is down
            {
                try
                {

                    FtpWebRequest requestdir = (FtpWebRequest)WebRequest.Create("ftp://" + domain);
                    requestdir.Credentials = new NetworkCredential("anonomyous", "");
                    requestdir.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                    WebResponse response = requestdir.GetResponse();

                }
                catch
                {

                    domain = "pmdonline.servegame.com";

                }
            }

            if (isnotFirstTimeInstall)
            {
                XmlDocument installedUpdates = new XmlDocument();
                installedUpdates.Load(Path.Combine(Application.StartupPath, "InstalledUpdates.xml"));
                XmlNode installedRevisions = installedUpdates.DocumentElement["InstalledRevisions"];

                installedRevisionsList = installedRevisions.InnerText.Split(",".ToCharArray()).ToList();

                foreach (string oldRevs in installedRevisionsList)
                {
                    if (File.Exists(Path.Combine(UpdateLocation, "UpdateData" + oldRevs + ".xml")))
                    {
                        File.Delete(Path.Combine(UpdateLocation, "UpdateData" + oldRevs + ".xml"));
                    }
                    
                }

                if (File.Exists(Path.Combine(UpdateLocation, "UpdateData.xml")))
                {
                    File.Delete(Path.Combine(UpdateLocation, "UpdateData.xml"));
                }
                
                for (int i = 0; i < Int32.Parse(installedRevisionsList[installedRevisionsList.Count - 1]) + 1; i++)
                {
                    int revisionNum = Int32.Parse(installedRevisionsList[i]);
                    if (i + 1 < installedRevisionsList.Count)
                    {
                        if (revisionNum + 1 != Int32.Parse(installedRevisionsList[i + 1]))
                        {
                            missingRevisions.Add(revisionNum + 1);
                        }
                    }
                    else
                    {
                        missingRevisions.Add(revisionNum + 1);
                    }
                }

                {
                    int missingCounter = 0;
                    int AvailibleCounter = 0;
                    for (int i = Int32.Parse(installedRevisionsList[installedRevisionsList.Count - 1]); i > -1; i--)
                    {
                        try
                        {

                            FtpWebRequest request = CreateFtpWebRequest("ftp://" + domain + "/UpdateData" + i + ".xml", "anonymous", "", true);
                            request.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                            WebResponse response = request.GetResponse();

                            AvailibleCounter++;
                            if (AvailibleCounter > 2)
                            {
                                break;
                            }
                            
                        }
                        catch
                        {
                            missingCounter++;
                            if (missingCounter > 2)
                            {
                                missingRevisions.Add(666);
                            }
                        }
                    }
                }

                for (int i = 0; i < missingRevisions.Count; i++)
                {
                    try
                    {
                        FtpWebRequest request = CreateFtpWebRequest("ftp://" + domain + "/UpdateData" + missingRevisions[i] + ".xml", "anonymous", "", true);
                        request.Method = WebRequestMethods.Ftp.DownloadFile;

                        Stream reader = request.GetResponse().GetResponseStream();
                        FileStream fileStream = new FileStream(Path.Combine(Application.StartupPath, "UpdaterCache", "UpdateData" + missingRevisions[i].ToString() + ".xml"), FileMode.Create);

                        while (true)
                        {
                            bytesRead = reader.Read(buffer, 0, buffer.Length);

                            if (bytesRead == 0)
                                break;

                            fileStream.Write(buffer, 0, bytesRead);
                        }
                        fileStream.Dispose();
                        fileStream.Close();
                    }
                    catch
                    {
                        if (missingRevisions.Count == 1)
                        {
                            Process.Start(Path.Combine(Application.StartupPath, "Pokemon Mystery Dungeon Online.exe"));
                            Close();
                        }
                    }
                }

                while (true)
                {
                    try
                    {
                        int i = missingRevisions[missingRevisions.Count - 1];
                        i++;
                        FtpWebRequest request = CreateFtpWebRequest("ftp://" + domain + "/UpdateData" + i + ".xml", "anonymous", "", true);
                        request.Method = WebRequestMethods.Ftp.DownloadFile;

                        Stream reader = request.GetResponse().GetResponseStream();
                        FileStream fileStream = new FileStream(Path.Combine(Application.StartupPath, "UpdaterCache", "UpdateData" + i + ".xml"), FileMode.Create);

                        while (true)
                        {
                            bytesRead = reader.Read(buffer, 0, buffer.Length);

                            if (bytesRead == 0)
                                break;

                            fileStream.Write(buffer, 0, bytesRead);
                        }
                        fileStream.Dispose();
                        fileStream.Close();

                        missingRevisions.Add(i);
                    }
                    catch
                    {
                        break;
                    }
                }

            }

            else
            {
                FtpWebRequest request = CreateFtpWebRequest("ftp://" + domain + "/UpdateData.xml", "anonymous", "", true);
                request.Method = WebRequestMethods.Ftp.DownloadFile;

                Stream reader = request.GetResponse().GetResponseStream();
                FileStream fileStream = new FileStream(Path.Combine(Application.StartupPath, "UpdaterCache", "UpdateData.xml"), FileMode.Create);

                while (true)
                {
                    bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                        break;

                    fileStream.Write(buffer, 0, bytesRead);
                }
                fileStream.Dispose();
                fileStream.Close();
            }

            XmlDocument newSettingsXml = new XmlDocument();

            if (isnotFirstTimeInstall)
            {

                for (int i = 0; i < missingRevisions.Count; i++)
                {
                    newSettingsXml.Load(Path.Combine(Application.StartupPath, "UpdaterCache", "UpdateData" + missingRevisions[i].ToString() + ".xml"));
                    
                    XmlNode ziporexe = newSettingsXml.DocumentElement["SimplePacked"];
                    XmlNode changelog = newSettingsXml.DocumentElement["UpdateNotes"];
                    
                    updateTypes.Add(ziporexe.InnerText);
                    updateNotes.Add(changelog.InnerText);
                }
            }

            else
            {
                newSettingsXml.Load(Path.Combine(Application.StartupPath, "UpdaterCache", "UpdateData.xml"));
                
                XmlNode ziporexe = newSettingsXml.DocumentElement["SimplePacked"];
                XmlNode changelog = newSettingsXml.DocumentElement["UpdateNotes"];
                
                updateTypes.Add(ziporexe.InnerText);
                updateNotes.Add(changelog.InnerText);
            }

            //probably not needed
            /*XmlDocument SettingsXml = new XmlDocument();
            SettingsXml.Load(Path.Combine(Application.StartupPath, "UpdateData.xml"));
            XmlNode appRevision = SettingsXml.DocumentElement["Revision"];

            int anonymousRevision = Int32.Parse(appRevision.InnerText);*/

            if (missingRevisions.Contains(666))
            {
                string failsafetext = updateNotes[missingRevisions.IndexOf(666)];
                updateTypes.Clear();
                updateTypes.Add("zip");
                updateNotes.Clear();
                updateNotes.Add(failsafetext);
                missingRevisions.Clear();
                missingRevisions.Add(666);
            }

            for (int i = 0; i < updateTypes.Count; i++)
            {
                if (updateTypes[i] == "zip")
                {
                    int a = i;
                    if (isnotFirstTimeInstall)
                    {
                        try
                        {
                            FtpWebRequest updaterequest = CreateFtpWebRequest("ftp://" + domain + "/update" + missingRevisions[i].ToString() + ".zip", "anonymous", "", true);
                            updaterequest.Method = WebRequestMethods.Ftp.DownloadFile;

                            Stream updatereader = updaterequest.GetResponse().GetResponseStream();
                            FileStream updatefileStream = new FileStream(Path.Combine(Application.StartupPath, "UpdaterCache", "update" + missingRevisions[a].ToString() + ".zip"), FileMode.Create);

                            while (true)
                            {
                                bytesRead = updatereader.Read(buffer, 0, buffer.Length);

                                if (bytesRead == 0)
                                    break;

                                updatefileStream.Write(buffer, 0, bytesRead);
                            }
                            updatefileStream.Dispose();
                            updatefileStream.Close();
                        }

                        catch
                        {

                        }

                        if (!Directory.Exists(Path.Combine(Application.StartupPath, "UpdaterCache", "UpdateRevision" + missingRevisions[a].ToString())))
                        {
                            Directory.CreateDirectory(Path.Combine(Application.StartupPath, "UpdaterCache", "UpdateRevision" + missingRevisions[a].ToString()));
                        }
                        System.IO.Compression.ZipFile.ExtractToDirectory(Path.Combine(Application.StartupPath, "UpdaterCache", "update" + missingRevisions[a].ToString() + ".zip"), Path.Combine(Application.StartupPath, "UpdaterCache", "UpdateRevision" + missingRevisions[a].ToString()));
                        if (a != missingRevisions.Count - 1)
                        {
                            File.Delete(Path.Combine(Application.StartupPath, "UpdaterCache", "UpdateRevision" + missingRevisions[a].ToString(), "Release", "Pokemon Mystery Dungeon Online.exe"));
                        }
                        CopyAll(new DirectoryInfo(Path.Combine(Application.StartupPath, "UpdaterCache", "UpdateRevision" + missingRevisions[a].ToString(), "Release")), new DirectoryInfo(Application.StartupPath));

                        Directory.Delete(Path.Combine(Application.StartupPath, "UpdaterCache", "UpdateRevision" + missingRevisions[a].ToString()), true);
                        File.Delete(Path.Combine(Application.StartupPath, "UpdaterCache", "update" + missingRevisions[a].ToString() + ".zip"));

                        if (missingRevisions[a] != 666)
                            missingRevisionsStrings.Add(missingRevisions[a].ToString());
                    }


                    else
                    {

                        /*File.Delete(Path.Combine(Application.StartupPath, "UpdateData.xml"));
                        File.Move(Path.Combine(Application.StartupPath, "UpdaterCache", "UpdateData.xml"), Path.Combine(Application.StartupPath, "UpdateData.xml"));*/
                        FtpWebRequest updaterequest = CreateFtpWebRequest("ftp://" + domain + "/update.zip", "anonymous", "", true);
                        updaterequest.Method = WebRequestMethods.Ftp.DownloadFile;

                        Stream updatereader = updaterequest.GetResponse().GetResponseStream();
                        FileStream updatefileStream = new FileStream(Path.Combine(Application.StartupPath, "UpdaterCache", "update.zip"), FileMode.Create);

                        while (true)
                        {
                            bytesRead = updatereader.Read(buffer, 0, buffer.Length);

                            if (bytesRead == 0)
                                break;

                            updatefileStream.Write(buffer, 0, bytesRead);
                        }
                        updatefileStream.Dispose();
                        updatefileStream.Close();

                        var dirInfo = new DirectoryInfo(Path.Combine(Application.StartupPath, "UpdaterCache"));
                        dirInfo.Attributes &= ~FileAttributes.ReadOnly;
                        System.IO.Compression.ZipFile.ExtractToDirectory(Path.Combine(Application.StartupPath, "UpdaterCache", "update.zip"), Path.Combine(Application.StartupPath, "UpdaterCache"));

                        CopyAll(new DirectoryInfo(Path.Combine(Application.StartupPath, "UpdaterCache", "Release")), new DirectoryInfo(Application.StartupPath));

                        Directory.Delete(Path.Combine(Application.StartupPath, "UpdaterCache", "Release"), true);
                        File.Delete(Path.Combine(Application.StartupPath, "UpdaterCache", "update.zip"));

                        XmlTextWriter writer = new XmlTextWriter(Path.Combine(Application.StartupPath, "InstalledUpdates.xml"), System.Text.Encoding.UTF8);
                        writer.WriteStartDocument(true);
                        writer.Formatting = Formatting.Indented;
                        writer.Indentation = 2;
                        writer.WriteStartElement("InstalledUpdates");
                        writer.WriteStartElement("InstalledRevisions");
                        writer.WriteEndElement();
                        writer.WriteEndElement();
                        writer.WriteEndDocument();
                        writer.Close();

                        XmlDocument settingsXml = new XmlDocument();
                        settingsXml.Load(Path.Combine(Application.StartupPath, "InstalledUpdates.xml"));
                        XmlNode revision = settingsXml.DocumentElement["InstalledRevisions"];

                        revision.InnerText = "1";
                        settingsXml.Save(Path.Combine(Application.StartupPath, "InstalledUpdates.xml"));
                        Process.Start(Path.Combine(Application.StartupPath, "PMDO Launcher.exe"));
                        Close();
                        break;
                    }
                }

                //Sometime soon
                else if (updateTypes[i] == "rar")
                {

                    //Code HERE
                    
                }

                else
                {
                    FtpWebRequest updaterequest = CreateFtpWebRequest("ftp://" + domain + "/Pokemon Mystery Dungeon Online.exe", "anonymous", "", true);
                    updaterequest.Method = WebRequestMethods.Ftp.DownloadFile;

                    Stream updatereader = updaterequest.GetResponse().GetResponseStream();
                    FileStream updatefileStream = new FileStream(Path.Combine(Application.StartupPath, "Pokemon Mystery Dungeon Online.exe"), FileMode.Create);

                    while (true)
                    {
                        bytesRead = updatereader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                            break;

                        updatefileStream.Write(buffer, 0, bytesRead);
                    }
                    updatefileStream.Dispose();
                    updatefileStream.Close();

                    if (missingRevisions[i] != 666)
                        missingRevisionsStrings.Add(missingRevisions[i].ToString());

                }

            }

            CreateRevisionXML();
            MessageBox.Show("Changelog: " + String.Join(" ", updateNotes), "PMDO Update Finished!");
            Process.Start(Path.Combine(Application.StartupPath, "Pokemon Mystery Dungeon Online.exe"));
            Close();

        }

        private FtpWebRequest CreateFtpWebRequest(string ftpDirectoryPath, string userName, string password, bool keepAlive = false)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(ftpDirectoryPath));

            //Set proxy to null. Under current configuration if this option is not set then the proxy that is used will get an html response from the web content gateway (firewall monitoring system)
            request.Proxy = null;

            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = keepAlive;

            request.Credentials = new NetworkCredential(userName, password);

            return request;
        }

        public void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            if (source.FullName.ToLower() == target.FullName.ToLower())
            {
                return;
            }

            // Check if the target directory exists, if not, create it.
            if (Directory.Exists(target.FullName) == false)
            {
                Directory.CreateDirectory(target.FullName);
            }

            Invoke(new Action(() =>
            {
                progressBar1.Visible = true;
                progressBar1.Maximum = source.GetFiles().Length;
                progressBar1.Invalidate();
            }));
            int progress = 0;
            // Copy each file into it's new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                progress++;
                Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                Invoke(new Action(() =>
                {
                    progressBar1.Value = progress;
                    progressBar1.Invalidate();
                    label1.Text = "Copying "+ target.FullName + "\\" + fi.Name;
                    label1.Invalidate();
                }));
                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }
            Invoke(new Action(() =>
            {
                progressBar1.Value = 0;
                progressBar1.Visible = false;
                progressBar1.Invalidate();
                label1.Text = "Finishing up current update package...";
                label1.Invalidate();
            }));

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }



        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(UpdatePMDO));
            t.SetApartmentState(System.Threading.ApartmentState.STA); //Set the thread to STA
            t.Start();
        }

        private void CreateRevisionXML()
        {
            XmlDocument settingsXml = new XmlDocument();
            settingsXml.Load(Path.Combine(Application.StartupPath, "InstalledUpdates.xml"));
            XmlNode revision = settingsXml.DocumentElement["InstalledRevisions"];


            revisionBuilder.Add(revision.InnerText);
            revisionBuilder.AddRange(missingRevisionsStrings);
            revision.InnerText = String.Join(",", revisionBuilder);
            settingsXml.Save(Path.Combine(Application.StartupPath, "InstalledUpdates.xml"));
        }
    }
}
