﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.ExceptionServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PMDO_Launcher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            Application.ThreadException += new ThreadExceptionEventHandler(ThreadExceptionHandler);
            AppDomain.CurrentDomain.FirstChanceException += new EventHandler<System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs>(CurrentDomain_FirstChanceException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            foreach (string arg in Environment.GetCommandLineArgs())
            {
                if (arg.Contains("-reinstall"))
                {
                    string InstallPath = arg.Split('=')[1].Replace("\"", "");
                    try
                    {
                        Directory.Delete(InstallPath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("This application does not have permission to access " + InstallPath + " Please manually delete it and press OK. Note: PMDO will not work until you do this! Error details: " + ex.Message + "\n" + ex.Source + "\n" + ex.StackTrace);
                        Main();
                        return;
                    }

                    MessageBox.Show("For one reason or another you will now have to reinstall PMDO. Please press \"OK\" to open the installer...");
                }
            }

            if (!File.Exists(Path.Combine(Application.StartupPath, "Pokemon Mystery Dungeon Online.exe")))
            {
                Application.Run(new InstallerForm1());
            }

            else
            {
                string domain = "pmdonlineupdates.serveftp.com";

                //Check if main domain is down
                {

                    FtpWebRequest requestdir = (FtpWebRequest)WebRequest.Create("ftp://" + domain);
                    requestdir.Credentials = new NetworkCredential("anonomyous", "");
                    requestdir.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                    try
                    {
                        WebResponse response = requestdir.GetResponse();
                    }
                    catch
                    {
                        domain = "pmdonline.servegame.com";
                    }
                }

                int bytesRead = 0;
                byte[] buffer = new byte[2048];

                File.Delete(Path.Combine(Application.StartupPath, "PMDO Updater.exe"));

                try
                {
                    FtpWebRequest updaterequest = CreateFtpWebRequest("ftp://" + domain + "/PMDO Updater.exe", "anonymous", "", true);
                    updaterequest.Method = WebRequestMethods.Ftp.DownloadFile;

                    Stream updatereader = updaterequest.GetResponse().GetResponseStream();
                    FileStream updatefileStream = new FileStream(Path.Combine(Application.StartupPath, "PMDO Updater.exe"), FileMode.Create);

                    while (true)
                    {
                        bytesRead = updatereader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                            break;

                        updatefileStream.Write(buffer, 0, bytesRead);
                    }
                    updatefileStream.Dispose();
                    updatefileStream.Close();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }

                if (!Directory.Exists(Path.Combine(Application.StartupPath, "UpdaterCache")))
                {
                    Directory.CreateDirectory(Path.Combine(Application.StartupPath, "UpdaterCache"));
                }

                Process proc = new Process();
                proc.StartInfo.FileName = Path.Combine(Application.StartupPath, "PMDO Updater.exe");
                proc.StartInfo.UseShellExecute = true;
                proc.StartInfo.Verb = "runas";
                proc.Start();
            }
            
        }

        private static void ThreadExceptionHandler(object sender, ThreadExceptionEventArgs e)
        {
            MessageBox.Show($"{e.Exception.Message}\n{e.Exception.StackTrace}\n{e.Exception.Source}");
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show((e.ExceptionObject as Exception).Message + "\n" + (e.ExceptionObject as Exception).StackTrace + (e.ExceptionObject as Exception).Source);
        }

        private static void CurrentDomain_FirstChanceException(object sender, FirstChanceExceptionEventArgs e)
        {
            MessageBox.Show($"{e.Exception.Message}\n{e.Exception.StackTrace}\n{e.Exception.Source}");
        }

        private static FtpWebRequest CreateFtpWebRequest(string ftpDirectoryPath, string userName, string password, bool keepAlive = false)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(ftpDirectoryPath));

            //Set proxy to null. Under current configuration if this option is not set then the proxy that is used will get an html response from the web content gateway (firewall monitoring system)
            request.Proxy = null;

            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = keepAlive;

            request.Credentials = new NetworkCredential(userName, password);

            return request;
        }

    }
}
