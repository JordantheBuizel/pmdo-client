﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using IWshRuntimeLibrary;
using System.Diagnostics;

namespace PMDO_Launcher
{
    public partial class InstallerForm1 : Form
    {
        Stopwatch SecondTimer = new Stopwatch();
        string InstallLocation = null;

        public InstallerForm1()
        {
            InitializeComponent();

            btnNext2.Click += new EventHandler(btnNext2_Click);
            btnBrowse.Click += new EventHandler(btnBrowse_Click);
            btnFinish.Click += new EventHandler(btnFinish_Click);

            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
            txtInstallLoc.Text = Path.Combine(Application.StartupPath.Split('\\')[0] + "\\", "Program Files", "PMDO");

            lblLoading.Location = new Point(progressBar1.Location.X, lblLoading.Location.Y);

            progressBar1.Visible = false;
            
        }

        

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnNext1_Click(object sender, EventArgs e)
        {

            rtbTerms.Visible = false;
            btnNext1.Visible = false;
            btnNext2.Visible = true;
            lblSelect.Visible = true;
            txtInstallLoc.Visible = true;
            btnBrowse.Visible = true;



        }

        private void btnNext2_Click(object sender, EventArgs e)
        {

            if (!Directory.Exists(txtInstallLoc.Text))
            {
                Directory.CreateDirectory(txtInstallLoc.Text);
            }

            InstallLocation = txtInstallLoc.Text;


            btnNext2.Visible = false;
            lblSelect.Visible = false;
            txtInstallLoc.Visible = false;
            btnBrowse.Visible = false;
            progressBar1.Visible = true;
            pbLoading.Visible = true;
            lblLoading.Visible = true;

            backgroundWorker1.RunWorkerAsync();
            
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pbLoading.Visible = false;
            lblLoading.Visible = false;
            btnFinish.Visible = true;
            btnCancel.Visible = false;
            chbxLaunch.Visible = true;
            chbxShortcut.Visible = true;
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            if (chbxShortcut.Checked)
            {
                object shDesktop = (object)"Desktop";
                WshShell shell = new WshShell();
                string shortcutAddress = (string)shell.SpecialFolders.Item(ref shDesktop) + @"\PMDO.lnk";
                IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutAddress);
                shortcut.Description = "Pokemon Mystery Dungeon Online Shortcut";
                shortcut.TargetPath = Path.Combine(InstallLocation, "PMDO Launcher.exe");
                shortcut.Save();
            }

            if (chbxLaunch.Checked)
            {
                Process proc = new Process();
                proc.StartInfo.FileName = Path.Combine(InstallLocation, "PMDO Launcher.exe");
                proc.StartInfo.UseShellExecute = true;
                proc.StartInfo.Verb = "runas";
                proc.Start();
            }
            
            if (!Directory.Exists(Path.Combine(InstallLocation, "UpdaterCache")))
            {
                Directory.CreateDirectory(Path.Combine(InstallLocation, "UpdaterCache"));
            }

            Close();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog installFolderPicker = new FolderBrowserDialog();
            DialogResult result = installFolderPicker.ShowDialog();

            if (!string.IsNullOrWhiteSpace(installFolderPicker.SelectedPath))
            {
                if (!installFolderPicker.SelectedPath.Contains("PMDO"))
                    txtInstallLoc.Text = Path.Combine(installFolderPicker.SelectedPath, "PMDO");

                else
                    txtInstallLoc.Text = installFolderPicker.SelectedPath;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to cancel the installation?", "Cancel Installation", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                Close();
            }
        }

        private FtpWebRequest CreateFtpWebRequest(string ftpDirectoryPath, string userName, string password, bool keepAlive = false)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(ftpDirectoryPath));

            //Set proxy to null. Under current configuration if this option is not set then the proxy that is used will get an html response from the web content gateway (firewall monitoring system)
            request.Proxy = null;

            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = keepAlive;

            request.Credentials = new NetworkCredential(userName, password);

            return request;
        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            if (source.FullName.ToLower() == target.FullName.ToLower())
            {
                return;
            }

            // Check if the target directory exists, if not, create it.
            if (Directory.Exists(target.FullName) == false)
            {
                Directory.CreateDirectory(target.FullName);
            }

            // Copy each file into it's new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            string domain = "pmdonlineupdates.serveftp.com";

            //Check if main domain is down
            {

                FtpWebRequest requestdir = (FtpWebRequest)WebRequest.Create("ftp://" + domain);
                requestdir.Credentials = new NetworkCredential("anonomyous", "");
                requestdir.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                try
                {
                    WebResponse response = requestdir.GetResponse();
                }
                catch
                {
                    domain = "pmdonline.servegame.com";
                }
            }

            int bytesRead = 0;
            byte[] buffer = new byte[1024 * 1024];

            

            FtpWebRequest request = CreateFtpWebRequest("ftp://" + domain + "/Release.rar", "anonymous", "", true);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            Stream reader = request.GetResponse().GetResponseStream();
            FileStream fileStream = new FileStream(Path.Combine(InstallLocation, "Release.rar"), FileMode.Create);

            
            SecondTimer.Start();
            long totalbytes = 0;
            while (true)
            {
                bytesRead = reader.Read(buffer, 0, buffer.Length);

                totalbytes += bytesRead;
                double percentage = totalbytes * 100.0 / 153815951;

                Invoke(new Action(() =>
                {
                    progressBar1.Value = (int)percentage;
                    progressBar1.Invalidate();
                }));

                Invoke(new Action(() =>
                {
                    lblLoading.Text = "Downloading " + percentage + "%...";
                }));


                if (bytesRead == 0)
                    break;

                fileStream.Write(buffer, 0, bytesRead);
            }
            fileStream.Dispose();
            fileStream.Close();

            Invoke(new Action(() =>
            {
                lblLoading.Text = "Installing...";
            }));

            //System.IO.Compression.ZipFile.ExtractToDirectory(Path.Combine(InstallLocation, "Release.zip"), InstallLocation);
            NUnrar.Archive.RarArchive.WriteToDirectory(Path.Combine(InstallLocation, "Release.rar"), InstallLocation, NUnrar.Common.ExtractOptions.ExtractFullPath);


        }

    }
}
