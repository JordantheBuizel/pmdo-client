﻿// <copyright file="Music.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Music
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Music
    {
        private static IAudioPlayer audioPlayer;

        public static IAudioPlayer AudioPlayer
        {
            get
            {
                return audioPlayer;
            }
        }

        public static void Initialize()
        {
            audioPlayer = new Bass.BassAudioPlayer();
        }

        public static void Dispose()
        {
            audioPlayer.Dispose();
        }
    }
}
