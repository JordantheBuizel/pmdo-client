﻿// <copyright file="AudioDataCache.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Music.Bass
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;

    internal class AudioDataCache
    {
        public string CachePath { get; set; }

        public FileStream CacheStream { get; set; }

        public BassAudioPlayer.DownloadCallbackDelegate CallbackDelegate { get; set; }

        public IntPtr AudioStreamPointer { get; set; }
    }
}
