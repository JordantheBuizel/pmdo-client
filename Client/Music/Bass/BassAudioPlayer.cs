﻿// <copyright file="BassAudioPlayer.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Music.Bass
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Windows.Forms;
    using PMU.Core;
    using Un4seen.Bass;

    public class BassAudioPlayer : IDisposable, IAudioPlayer
    {
        private const uint BASSPOSBYTE = 0;

        private readonly string musicURL = IO.Options.MusicAddress;
        private readonly string soundFXURL = IO.Options.SoundAddress;

        private bool abortMusic;
        private string currentSong;
        private int loadedStream = 0;
        private int musicPlayCount = 0;
        private int musicRepeatAmount = 0;
        private bool paused = false;
        private readonly ManualResetEvent pauseResetEvent;
        private readonly ManualResetEvent stopResetEvent;
        private Thread playbackThread;

        // string url;
        // string urlCachePath;
        // FileStream urlCacheStream;
        private Delegate callbackDelegate;
        private DOWNLOADPROC downloadproc;
        private readonly ListPair<string, AudioDataCache> audioDataCache = new ListPair<string, AudioDataCache>();

        public delegate void DownloadCallbackDelegate(IntPtr bufferPointer, int length, IntPtr user);

        /// <inheritdoc/>
        public string CurrentSong
        {
            get { return this.currentSong; }
        }

        public string NextSong { get; set; }

        public int TimeOfNextSong { get; set; }

        /// <inheritdoc/>
        public void Dispose()
        {
            // Get rid of the Audio object.
            this.StopMusic();
            Bass.BASS_Free();
        }

        // public void MusicDownloadCallback(IntPtr bufferPointer, int length, IntPtr user) {
        //    try {
        //        if (length > 0) {
        //            byte[] buffer = new byte[length];
        //            Marshal.Copy(bufferPointer, buffer, 0, length);
        //            urlCacheStream.Write(buffer, 0, length);
        //        } else if (length == 0) {
        //            urlCacheStream.Close();
        //            File.Copy(urlCachePath + ".tmp", urlCachePath, true);
        //            File.Delete(urlCachePath + ".tmp");
        //        }
        //    } catch (Exception ex) {
        //        throw new Exception(ex.Message + " @ Music Download Callback");
        //    }
        // }
        public BassAudioPlayer()
        {
            Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
            Bass.BASS_PluginLoad("bassmidi.dll");

            this.pauseResetEvent = new ManualResetEvent(false);
            this.stopResetEvent = new ManualResetEvent(false);
        }

        /// <inheritdoc/>
        public bool IsMusicPaused()
        {
            return this.paused;
        }

        // public void LoadMusicStreamFromUrl(string url) {
        //    this.url = url;

        // if (loadedStream != IntPtr.Zero) {
        //        Bass.BASS_StreamFree(loadedStream);
        //        loadedStream = IntPtr.Zero;
        //    }

        // urlCacheStream = new FileStream(IO.Paths.MusicPath + Path.GetFileName(url) + ".tmp", FileMode.Create);
        //    urlCachePath = IO.Paths.MusicPath + Path.GetFileName(url);
        //    this.currentSong = IO.Paths.MusicPath + Path.GetFileName(url);

        // callbackDelegate = new DownloadCallbackDelegate(MusicDownloadCallback);
        //    loadedStream = Bass.BASS_StreamCreateURL(url, 0, 0, callbackDelegate, IntPtr.Zero);

        // if (loadedStream == IntPtr.Zero) {
        //        // Uh oh! We couldn't play the music stream!
        //        urlCacheStream.Close();
        //        File.Delete(urlCachePath + ".tmp");
        //        urlCacheStream = null;
        //        urlCachePath = null;
        //    }

        // musicPlayCount = 0;
        // }

        /// <inheritdoc/>
        public void Pause()
        {
            if (!this.paused)
            {
                Bass.BASS_ChannelPause(this.loadedStream);
                this.pauseResetEvent.Reset();
                this.pauseResetEvent.WaitOne();
            }
        }

        public void FadeToNext(string nextSong, int milliseconds)
        {
            if ((this.currentSong == null || !this.currentSong.EndsWith(nextSong)) && nextSong != this.NextSong)
            {
                Bass.BASS_ChannelSlideAttribute(this.loadedStream, BASSAttribute.BASS_ATTRIB_VOL, -1, milliseconds);
                this.NextSong = nextSong;
                this.TimeOfNextSong = Globals.Tick + milliseconds;
            }
        }

        public void PlayNextMusic()
        {
            if (this.NextSong != null)
            {
                this.StopMusic();
                this.PlayMusic(this.NextSong, -1);
                this.NextSong = null;
                this.TimeOfNextSong = 0;
            }
        }

        /// <inheritdoc/>
        public void PlayMusic(string songName)
        {
            this.PlayMusic(songName, -1);
        }

        /// <inheritdoc/>
        public void PlayMusic(string songName, int numberOfTimes)
        {
            this.PlayMusic(songName, numberOfTimes, false, true);
        }

        /// <inheritdoc/>
        public void PlayMusic(string songName, int numberOfTimes, bool ignoreMusicSetting, bool ignoreIfPlaying)
        {
            Thread musicLoadThread = new Thread(new ParameterizedThreadStart(this.PlayMusicBackground));
            musicLoadThread.IsBackground = true;
            musicLoadThread.Start(new object[] { songName, numberOfTimes, ignoreMusicSetting, ignoreIfPlaying });
        }

        private void PlayMusicBackground(object param)
        {
            object[] args = param as object[];
            string songName = args[0] as string;
            int numberOfTimes = (int)args[1];
            bool ignoreMusicSetting = false; // (bool)args[2];
            bool ignoreIfPlaying = (bool)args[3];

            this.VerifySongName(ref songName);
            if (ignoreMusicSetting == false)
            {
                if (IO.Options.Music == false)
                {
                    this.StopMusic();
                    return;
                }
            }

            if (string.IsNullOrEmpty(songName))
            {
                return;
            }

            if (ignoreIfPlaying)
            {
                if (this.currentSong == IO.Paths.MusicPath + songName)
                {
                    return;
                }
            }

            lock (this.audioUrlStreamLoaderLockObject)
            {
                this.StopMusic();
                this.LoadMusic(songName);
                this.playbackThread = Thread.CurrentThread;
            }

            this.currentSong = IO.Paths.MusicPath + songName;
            this.PlayMusicInternal(numberOfTimes);
        }

        /// <inheritdoc/>
        public void StopMusic()
        {
            if (this.playbackThread != null)
            {
                // Stop the music.
                // abortMusic = true;
                // stopResetEvent.Reset();
                // stopResetEvent.WaitOne();
                if (this.loadedStream != 0)
                {
                    this.currentSong = null;
                    lock (this.audioDataCache)
                    {
                        for (int i = 0; i < this.audioDataCache.Count; i++)
                        {
                            if (this.audioDataCache.ValueByIndex(i).AudioStreamPointer == (IntPtr)this.loadedStream)
                            {
                                this.audioDataCache.ValueByIndex(i).AudioStreamPointer = IntPtr.Zero;
                            }
                        }
                    }

                    Bass.BASS_StreamFree(this.loadedStream);
                    Bass.BASS_ChannelStop(this.loadedStream);
                    this.loadedStream = (int)IntPtr.Zero;
                }

                if (this.playbackThread != null)
                {
                    this.playbackThread.Abort();
                    this.playbackThread = null;
                }
            }
        }

        private bool IsMusicDownloaded(string songName)
        {
            return File.Exists(IO.Paths.MusicPath + songName);
        }

        private void LoadMusic(string songName)
        {
            if (File.Exists(songName))
            {
                // Support for using a full file path as the song name
                this.loadedStream = Bass.BASS_StreamCreateFile(songName, 0, 0, BASSFlag.BASS_DEFAULT);
            }
            else
            {
                if (this.IsMusicDownloaded(songName))
                {
                    // We already have the song cached, load from the file
                    this.loadedStream = Bass.BASS_StreamCreateFile(IO.Paths.MusicPath + songName, 0, 0,
                        BASSFlag.BASS_DEFAULT);
                }
                else
                {
                    // We don't have the song cached, stream from the web
                    WebClient client = new WebClient();
                    client.DownloadFile(IO.Options.MusicAddress + songName, IO.Paths.MusicPath + songName);
                    this.LoadMusic(songName);
                }
            }
        }

        private void MusicThread()
        {
            // This is the actual routine that's playing the music.  I use a background thread to play it.
            long intLength = 0;

            // stream = new IntPtr(Un4seen.Bass.Bass.BASS_StreamCreateURL(url, 0, Un4seen.Bass.BASSFlag.BASS_DEFAULT, null, IntPtr.Zero));
            // stream = //BASS_StreamCreateFile(false, strDirs[i], 0, 0, 0, 0, 0);
            intLength = Bass.BASS_ChannelGetLength(this.loadedStream, (int)BASSPOSBYTE);

            if (this.loadedStream != 0)
            {
                // BASS_SetVolume(0.5f);
                Bass.BASS_ChannelPlay(this.loadedStream, false);

                // System.IO.FileStream fs = new System.IO.FileStream("SoundTest.ogg", System.IO.FileMode.Create);
                while (!(Bass.BASS_ChannelGetPosition(this.loadedStream, (int)BASSPOSBYTE) >= intLength || this.abortMusic))
                {
                    // byte[] buffer = new byte[65536];
                    // int len = Un4seen.Bass.Bass.BASS_ChannelGetData(stream.ToInt32(), buffer, (int)intLength);
                    // fs.Write(buffer, 0, len);
                    Thread.Sleep(500);
                    if (this.abortMusic)
                    {
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }

                // fs.Close();
                // byte[] buffer = new byte[intLength];
                // Un4seen.Bass.Bass.BASS_ChannelSetPosition(stream.ToInt32(), 0);

                // System.IO.File.WriteAllBytes("SoundTest.ogg", buffer);
            }

            Bass.BASS_StreamFree(this.loadedStream);

            // if (booAbortMusic)
            //    break; // TODO: might not be correct. Was : Exit For
            Bass.BASS_ChannelStop(this.loadedStream);
            this.loadedStream = 0;

            this.playbackThread = null;

            if (this.musicRepeatAmount == -1)
            {
                this.PlayMusic(Path.GetFileName(this.currentSong), -1, false, false);
            }
            else if (this.musicPlayCount < this.musicRepeatAmount)
            {
                this.musicPlayCount++;
                this.PlayMusicInternal(this.musicRepeatAmount);
            }

            GC.KeepAlive(this);
            this.stopResetEvent.Set();
        }

        private void PlayMusicInternal(int numberOfTimes)
        {
            if (numberOfTimes != 0 && this.loadedStream != 0)
            {
                this.musicRepeatAmount = numberOfTimes;

                // Start up the music.
                this.abortMusic = false;

                // RunMusicPlaybackThread();
                this.MusicThread();
            }
        }

        /// <inheritdoc/>
        public void Resume()
        {
            if (this.paused)
            {
                Bass.BASS_ChannelPlay(this.loadedStream, false);
                GC.KeepAlive(this);
                this.pauseResetEvent.Set();
                this.paused = true;
            }
        }

        private void RunMusicPlaybackThread()
        {
            if (this.playbackThread == null)
            {
                this.playbackThread = new Thread(this.MusicThread);
                this.playbackThread.Name = "BackgroundMusic";
                this.playbackThread.IsBackground = true;
                this.playbackThread.Start();
            }
        }

        private List<string> Shuffle(List<string> strTempList)
        {
            // Shuffle a list of music randomly.
            List<string> strResult = new List<string>();

            int i = 0;

            for (int j = 1; j <= strTempList.Count; j++)
            {
                i = MathFunctions.Rand(0, strTempList.Count);
                strResult.Add(strTempList[i]);
                strTempList.RemoveAt(i);
            }

            return strResult;
        }

        private void VerifySongName(ref string songName)
        {
            if (!string.IsNullOrEmpty(songName))
            {
                if (songName.EndsWith(".ogg") == false && songName.EndsWith(".mid") == false)
                {
                    songName = Path.ChangeExtension(songName, ".ogg");
                }
            }
        }

        /// <inheritdoc/>
        public void FadeOut(int milliseconds)
        {
            Bass.BASS_ChannelSlideAttribute(this.loadedStream, BASSAttribute.BASS_ATTRIB_VOL, -1, milliseconds);
        }

        /// <inheritdoc/>
        public void PlaySoundEffect(string soundEffect)
        {
            if (IO.Options.Sound)
            {
                Thread soundEffectThread = new Thread(new ParameterizedThreadStart(this.PlaySoundEffectThread));
                soundEffectThread.IsBackground = true;
                soundEffectThread.Name = "SFX Playback Thread";
                soundEffectThread.Start(Path.ChangeExtension(soundEffect, ".ogg"));
            }
        }

        private void PlaySoundEffectThread(object paramenter)
        {
            string soundEffect = paramenter as string;

            int sfxStream;

            // Load the SFX
            if (this.IsSFXDownloaded(soundEffect))
            {
                // We already have the song cached, load from the file
                sfxStream = Bass.BASS_StreamCreateFile(IO.Paths.SfxPath + soundEffect, 0, 0, BASSFlag.BASS_DEFAULT);
            }
            else
            {
                // We don't have the song cached, stream from the web
                sfxStream = this.LoadAudioStreamFromUrl(this.soundFXURL + soundEffect, IO.Paths.SfxPath);
            }

            this.PlaySFXStream(sfxStream);
        }

        private void PlaySFXStream(int sfxStream)
        {
            // This is the actual routine that's playing the SFX.
            long intLength = 0;

            if (sfxStream != 0)
            {
                intLength = Bass.BASS_ChannelGetLength(sfxStream, (int)BASSPOSBYTE);
                Bass.BASS_ChannelPlay(sfxStream, false);

                while (!(Bass.BASS_ChannelGetPosition(sfxStream, (int)BASSPOSBYTE) >= intLength))
                {
                    Thread.Sleep(500);
                }

                Bass.BASS_StreamFree(sfxStream);
                Bass.BASS_ChannelStop(sfxStream);
            }

            GC.KeepAlive(this);
        }

        private readonly object audioUrlStreamLoaderLockObject = new object();

        public int LoadAudioStreamFromUrl(string url, string cacheDirectory)
        {
            lock (this.audioUrlStreamLoaderLockObject)
            {
                lock (this.audioDataCache)
                {
                    if (this.audioDataCache.ContainsKey(url))
                    {
                        return (int)this.audioDataCache[url].AudioStreamPointer;
                    }
                }

                AudioDataCache dataCache = new AudioDataCache { CachePath = cacheDirectory + Path.GetFileName(url) };

                dataCache.CacheStream = new FileStream(dataCache.CachePath + ".tmp", FileMode.Create, FileAccess.Write);

                dataCache.CallbackDelegate = this.AudioDownloadCallback;

                IntPtr urlPtr = Marshal.StringToHGlobalUni(url);
                lock (this.audioDataCache)
                {
                    if (this.audioDataCache.ContainsKey(url) == false)
                    {
                        this.audioDataCache.Add(url, dataCache);
                    }
else
                    {
                        dataCache.CallbackDelegate = null;
                        if (urlPtr != IntPtr.Zero)
                        {
                            Marshal.FreeHGlobal(urlPtr);
                            urlPtr = IntPtr.Zero;
                        }
                    }
                }

                dataCache.AudioStreamPointer = (IntPtr)Bass.BASS_StreamCreateURL(url, 0, 0, null, urlPtr);

                if (dataCache.AudioStreamPointer == IntPtr.Zero)
                {
                    // Uh oh! We couldn't play the sfx stream!
                    lock (this.audioDataCache)
                    {
                        if (this.audioDataCache.ContainsKey(url))
                        {
                            this.audioDataCache.RemoveAtKey(url);
                        }

                        if (urlPtr != IntPtr.Zero)
                        {
                            Marshal.FreeHGlobal(urlPtr);
                            urlPtr = IntPtr.Zero;
                        }
                    }

                    dataCache.CacheStream.Close();
                    File.Delete(dataCache.CachePath + ".tmp");
                    dataCache.CachePath = null;
                }

                return (int)dataCache.AudioStreamPointer;
            }
        }

        private bool IsSFXDownloaded(string soundEffect)
        {
            return IO.IO.FileExists(IO.Paths.SfxPath + soundEffect);
        }

        public void AudioDownloadCallback(IntPtr bufferPointer, int length, IntPtr user)
        {
            try
            {
                AudioDataCache dataCache = null;
                string url = Marshal.PtrToStringUni(user);
                lock (this.audioDataCache)
                {
                    if (this.audioDataCache.ContainsKey(url))
                    {
                        dataCache = this.audioDataCache[url];
                    }
                }

                if (dataCache != null)
                {
                    if (length > 0)
                    {
                        byte[] buffer = new byte[length];
                        Marshal.Copy(bufferPointer, buffer, 0, length);
                        dataCache.CacheStream.Write(buffer, 0, length);
                    }
                    else if (length == 0)
                    {
                        Marshal.FreeHGlobal(user);
                        dataCache.CacheStream.Close();
                        lock (this.audioDataCache)
                        {
                            if (this.audioDataCache.ContainsKey(url))
                            {
                                this.audioDataCache.RemoveAtKey(url);
                            }
                        }

                        if (dataCache.AudioStreamPointer != IntPtr.Zero)
                        {
                            File.Copy(dataCache.CachePath + ".tmp", dataCache.CachePath, true);
                        }

                        File.Delete(dataCache.CachePath + ".tmp");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " @ Audio Download Callback");
            }
        }
    }
}