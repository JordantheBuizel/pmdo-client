﻿// <copyright file="TrackListEntry.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Music
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class TrackListEntry
    {
        private string trackName;

        public string TrackName
        {
            get { return this.trackName; }
            set { this.trackName = value; }
        }
    }
}
