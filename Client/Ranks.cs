﻿// <copyright file="Ranks.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic
{
    using System;
    using System.Drawing;
    using Players;

    /// <summary>
    /// Description of Ranks.
    /// </summary>
    internal class Ranks
    {
        /// <summary>
        /// Checks if the player has the specified rank permissions
        /// </summary>
        /// <param name="player">The player to check</param>
        /// <param name="rankToCheck">The rank permissions to test</param>
        /// <returns>True if the player has the rank permissions; otherwise, false</returns>
        public static bool IsAllowed(IPlayer player, Enums.Rank rankToCheck)
        {
            Enums.Rank playerRank = player.Access;
            switch (rankToCheck)
            {
                case Enums.Rank.Normal:
                    return true;
                case Enums.Rank.Moniter:
                    if (playerRank == Enums.Rank.Moniter || playerRank == Enums.Rank.Mapper || playerRank == Enums.Rank.Developer || playerRank == Enums.Rank.Admin || playerRank == Enums.Rank.ServerHost || playerRank == Enums.Rank.Scriptor)
                    {
                        return true;
                    }

                    break;
                case Enums.Rank.Mapper:
                    if (playerRank == Enums.Rank.Mapper || playerRank == Enums.Rank.Developer || playerRank == Enums.Rank.Admin || playerRank == Enums.Rank.ServerHost || playerRank == Enums.Rank.Scriptor)
                    {
                        return true;
                    }

                    break;
                case Enums.Rank.Developer:
                    if (playerRank == Enums.Rank.Developer || playerRank == Enums.Rank.Admin || playerRank == Enums.Rank.ServerHost || playerRank == Enums.Rank.Scriptor)
                    {
                        return true;
                    }

                    break;
                case Enums.Rank.Admin:
                    if (playerRank == Enums.Rank.Admin || playerRank == Enums.Rank.ServerHost || playerRank == Enums.Rank.Scriptor)
                    {
                        return true;
                    }

                    break;
                case Enums.Rank.ServerHost:
                case Enums.Rank.Scriptor:
                    if (playerRank == Enums.Rank.ServerHost || playerRank == Enums.Rank.Scriptor)
                    {
                        return true;
                    }

                    break;
            }

            return false;
        }

        /// <summary>
        /// Checks if the player does not have the specified rank permissions
        /// </summary>
        /// <param name="player">The player to check</param>
        /// <param name="rankToCheck">The rank permissions to test</param>
        /// <returns>True if the player does not have the rank permissions; otherwise, false</returns>
        public static bool IsDisallowed(IPlayer player, Enums.Rank rankToCheck)
        {
            if (player == null)
            {
                return false;
            }

            return !IsAllowed(player, rankToCheck);
        }

        /// <summary>
        /// Gets the color associated with the specified rank
        /// </summary>
        /// <param name="rank">The rank used to determine the color returned</param>
        /// <returns>The color associated with the specified rank</returns>
        public static Color GetRankColor(Enums.Rank rank)
        {
            switch (rank)
            {
                case Enums.Rank.Normal:
                    return Color.Brown;
                case Enums.Rank.Moniter:
                    return Color.FromArgb(255, 254, 150, 46);
                case Enums.Rank.Mapper:
                    return Color.Cyan;
                case Enums.Rank.Developer:
                    return Color.FromArgb(255, 0, 110, 210);
                case Enums.Rank.Admin:
                    return Color.Pink;
                case Enums.Rank.ServerHost:
                    return Color.Yellow;
                case Enums.Rank.Scriptor:
                    return Color.LightCyan;
                default:
                    return Color.DarkRed;
            }
        }
    }
}
