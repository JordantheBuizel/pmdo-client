﻿// <copyright file="SpokenTextMenu.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Components
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class SpokenTextMenu : Widges.BorderedPanel, Menus.Core.IMenu
    {
        private readonly Label lblText;
        private readonly PictureBox picSpeaker;

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        public SpokenTextMenu(string name, Size storyBounds)
            : base(name)
            {
            this.Size = new Size(storyBounds.Width - 10, 100);
            this.Location = new Point(5, storyBounds.Height - this.Height);

            this.lblText = new Label("lblText");
            this.lblText.BackColor = Color.Transparent;
            this.lblText.ForeColor = Color.WhiteSmoke;
            this.lblText.AutoSize = false;
            this.lblText.Font = Graphic.FontManager.LoadFont("PMU", 24);
            this.lblText.Location = new Point(15, 10);
            this.lblText.Size = new Size(this.WidgetPanel.Width - this.lblText.Location.X, this.WidgetPanel.Height - this.lblText.Location.Y);

            this.picSpeaker = new PictureBox("picSpeaker");
            this.picSpeaker.Size = new Size(40, 40);
            this.picSpeaker.Location = new Point(10, DrawingSupport.GetCenter(this.WidgetPanel.Height, 40));
            this.picSpeaker.BorderWidth = 1;
            this.picSpeaker.BorderStyle = SdlDotNet.Widgets.BorderStyle.FixedSingle;

            this.WidgetPanel.AddWidget(this.lblText);
            this.WidgetPanel.AddWidget(this.picSpeaker);
        }

        public void DisplayText(string text)
        {
            this.DisplayText(text, -1);
        }

        public void DisplayText(string text, int mugshot)
        {
            bool displayMugshot = false;
            if (mugshot > -1)
            {
                Graphic.Mugshot mugshotImg = Graphic.GraphicsManager.GetMugshot(mugshot, string.Empty, 0, 0);
                if (mugshotImg != null)
                {
                    displayMugshot = true;
                }
            }

            if (displayMugshot)
            {
                this.picSpeaker.Image = Graphic.GraphicsManager.GetMugshot(mugshot, string.Empty, 0, 0).GetEmote(0); // Tools.CropImage(Logic.Graphic.GraphicsManager.Speakers, new Rectangle((mugshot % 15) * 40, string.Empty, 40, 40));
                this.lblText.Location = new Point(this.picSpeaker.X + this.picSpeaker.Width + 10, 10);
                this.lblText.Size = new Size(this.WidgetPanel.Width - this.lblText.Location.X, this.WidgetPanel.Height - this.lblText.Location.Y);
                this.picSpeaker.Show();
            }
else
            {
                this.picSpeaker.Hide();
                this.lblText.Location = new Point(15, 10);
                this.lblText.Size = new Size(this.WidgetPanel.Width - this.lblText.Location.X, this.WidgetPanel.Height - this.lblText.Location.Y);
            }

            CharRenderOptions[] renderOptions = new CharRenderOptions[text.Length];
            for (int i = 0; i < renderOptions.Length; i++)
            {
                renderOptions[i] = new CharRenderOptions(Color.WhiteSmoke);
            }

            renderOptions = Network.MessageProcessor.ParseText(renderOptions, ref text);
            this.lblText.SetText(text, renderOptions);
        }

        /// <inheritdoc/>
        public override void Close()
        {
            base.Close();
            if (this.ParentContainer != null)
            {
                this.ParentContainer.RemoveWidget(this.GroupedWidget.Name);
            }
        }
    }
}
