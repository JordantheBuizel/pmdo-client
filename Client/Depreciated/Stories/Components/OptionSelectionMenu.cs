﻿// <copyright file="OptionSelectionMenu.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Components
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class OptionSelectionMenu : Widges.BorderedPanel, Menus.Core.IMenu
    {
        private readonly Label[] lblOptions;

        public delegate void OptionSelectedDelegate(string option);

        public event OptionSelectedDelegate OptionSelected;

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        public OptionSelectionMenu(string name, Size storyBounds, string[] options)
            : base(name)
            {
            this.lblOptions = new Label[options.Length];

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(0, 15);

            int maxWidth = 140;

            for (int i = 0; i < options.Length; i++)
            {
                this.lblOptions[i] = new Label("lblOptions" + i);
                this.lblOptions[i].Font = Graphic.FontManager.LoadFont("PMU", 24);
                this.lblOptions[i].Location = new Point(15, i * 26);
                this.lblOptions[i].AutoSize = true;
                this.lblOptions[i].ForeColor = Color.WhiteSmoke;
                this.lblOptions[i].Text = options[i];

                if (this.lblOptions[i].Width > maxWidth)
                {
                    maxWidth = this.lblOptions[i].Width;
                }

                this.WidgetPanel.AddWidget(this.lblOptions[i]);
            }

            this.Size = new Size(maxWidth + 30, (options.Length * 26) + 20);
            this.Location = new Point(storyBounds.Width - this.Width - 20, storyBounds.Height - this.Height - 110);

            // lblText = new Label("lblText");
            // lblText.BackColor = Color.Transparent;
            // lblText.Font = Graphic.FontManager.LoadFont("unown", 36);
            // lblText.Location = new Point(15, 10);
            // lblText.Size = new System.Drawing.Size(this.Size.Width - lblText.Location.X, this.Size.Height - lblText.Location.Y);

            // picSpeaker = new PictureBox("picSpeaker");
            // picSpeaker.Size = new Size(40, 40);
            // picSpeaker.Location = new Point(10, DrawingSupport.GetCenter(WidgetPanel.Height, 40));
            // picSpeaker.BorderWidth = 1;
            // picSpeaker.BorderStyle = SdlDotNet.Widgets.BorderStyle.FixedSingle;

            // this.WidgetPanel.AddWidget(lblText);
            // this.WidgetPanel.AddWidget(picSpeaker);
            this.WidgetPanel.AddWidget(this.itemPicker);
        }

        private readonly Widges.MenuItemPicker itemPicker;

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(0, 15 + (22 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == this.lblOptions.Length - 1)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(this.lblOptions.Length - 1);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectOption(this.itemPicker.SelectedItem);
                    }

                    break;
            }
        }

        private void SelectOption(int optionSlot)
        {
            if (this.OptionSelected != null)
            {
                this.OptionSelected(this.lblOptions[optionSlot].Text);
            }
        }

        /// <inheritdoc/>
        public override void Close()
        {
            base.Close();
            if (this.ParentContainer != null)
            {
                this.ParentContainer.RemoveWidget(this.GroupedWidget.Name);
            }
        }
    }
}
