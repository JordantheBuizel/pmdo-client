﻿// <copyright file="mnuAddNotice.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using SdlDotNet.Widgets;

    internal class MnuAddNotice : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        public static Label LblAddTile;

        private readonly Label lblAddTile2;
        private readonly TextBox txtHouse1;
        private readonly TextBox txtHouse2;
        private readonly ScrollingListBox lstSound;
        private static Label lblPrice;
        private readonly Button btnAccept;
        private readonly Button btnCancel;
        public static int Price;
        public static int WordPrice;

        public MnuAddNotice(string name, int getprice, int getwordPrice)
            : base(name)
            {
            Price = getprice;
            WordPrice = getwordPrice;

            this.Size = new Size(250, 350);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = Graphic.DrawingSupport.GetCenter(Windows.WindowSwitcher.GameWindow.MapViewer.Size, this.Size);

            LblAddTile = new Label("lblAddTile");
            LblAddTile.Location = new Point(25, 15);
            LblAddTile.AutoSize = false;
            LblAddTile.Size = new Size(this.Width - (LblAddTile.X * 2), 40);
            LblAddTile.Text = "Write what you want your notice to say:";
            LblAddTile.ForeColor = Color.WhiteSmoke;

            this.txtHouse1 = new TextBox("txtHouse1");
            this.txtHouse1.Location = new Point(LblAddTile.X, LblAddTile.Y + LblAddTile.Height + 10);
            this.txtHouse1.Size = new Size(this.Width - (LblAddTile.X * 2), 16);
            Skins.SkinManager.LoadTextBoxGui(this.txtHouse1);
            this.txtHouse1.TextChanged += new EventHandler(this.TxtHouse_TextChanged);

            this.txtHouse2 = new TextBox("txtHouse2");
            this.txtHouse2.Location = new Point(this.txtHouse1.X, this.txtHouse1.Y + this.txtHouse1.Height + 10);
            this.txtHouse2.Size = new Size(this.Width - (this.txtHouse1.X * 2), 16);
            Skins.SkinManager.LoadTextBoxGui(this.txtHouse2);
            this.txtHouse2.TextChanged += new EventHandler(this.TxtHouse_TextChanged);

            this.lblAddTile2 = new Label("lblAddTile2");
            this.lblAddTile2.Location = new Point(this.txtHouse2.X, this.txtHouse2.Y + this.txtHouse2.Height + 10);
            this.lblAddTile2.AutoSize = false;
            this.lblAddTile2.Size = new Size(this.Width - (this.lblAddTile2.X * 2), 20);
            this.lblAddTile2.Text = "Choose a sound to play:";
            this.lblAddTile2.ForeColor = Color.WhiteSmoke;

            this.lstSound = new ScrollingListBox("lstSound");
            this.lstSound.Location = new Point(this.lblAddTile2.X, this.lblAddTile2.Y + this.lblAddTile2.Height);
            this.lstSound.Size = new Size(180, 120);

                SdlDotNet.Graphics.Font font = FontManager.LoadFont("PMU", 18);
            this.lstSound.Items.Add(new ListBoxTextItem(font, "None"));
                string[] sfxFiles = System.IO.Directory.GetFiles(IO.Paths.SfxPath);
                for (int i = 0; i < sfxFiles.Length; i++)
                {
                this.lstSound.Items.Add(new ListBoxTextItem(font, System.IO.Path.GetFileName(sfxFiles[i])));
                }

            this.lstSound.ItemSelected += new EventHandler(this.LstSound_ItemSelected);

            lblPrice = new Label("lblPrice");
            lblPrice.Location = new Point(LblAddTile.X, this.lstSound.Y + this.lstSound.Height + 10);
            lblPrice.AutoSize = false;
            lblPrice.Size = new Size(120, 30);
            lblPrice.Text = "Placing this tile will cost " + (((this.txtHouse1.Text.Length + this.txtHouse2.Text.Length) * WordPrice) + Price) + " " + Items.ItemHelper.Items[1].Name + ".";
            lblPrice.ForeColor = Color.WhiteSmoke;

            this.btnAccept = new Button("btnAccept");
            this.btnAccept.Location = new Point(LblAddTile.X, lblPrice.Y + lblPrice.Height + 10);
            this.btnAccept.Size = new Size(80, 30);
            this.btnAccept.Text = "Place Notice";
            this.btnAccept.Font = FontManager.LoadFont("tahoma", 10);
            Skins.SkinManager.LoadButtonGui(this.btnAccept);
            this.btnAccept.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAccept_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(this.btnAccept.X + this.btnAccept.Width, lblPrice.Y + lblPrice.Height + 10);
            this.btnCancel.Size = new Size(80, 30);
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Font = FontManager.LoadFont("tahoma", 10);
            Skins.SkinManager.LoadButtonGui(this.btnCancel);
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.AddWidget(LblAddTile);
            this.AddWidget(this.lblAddTile2);

            // this.AddWidget(txtHouse1);
            // this.AddWidget(txtHouse2);
            this.AddWidget(this.lstSound);
            this.AddWidget(lblPrice);
            this.AddWidget(this.btnAccept);
            this.AddWidget(this.btnCancel);
        }

        private void TxtHouse_TextChanged(object sender, EventArgs e)
        {
            lblPrice.Text = "Placing this tile will cost " + (((this.txtHouse1.Text.Length + this.txtHouse2.Text.Length) * WordPrice) + Price) + " " + Items.ItemHelper.Items[1].Name + ".";
        }

        private void BtnAccept_Click(object sender, MouseButtonEventArgs e)
        {
            string sound = string.Empty;
            if (this.lstSound.SelectedItems.Count > 0)
            {
                sound = ((ListBoxTextItem)this.lstSound.SelectedItems[0]).Text;
            }

            Messenger.SendAddNoticeRequest(this.txtHouse1.Text, this.txtHouse2.Text, sound);
            MenuSwitcher.CloseAllMenus();
            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            MenuSwitcher.CloseAllMenus();
            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
        }

        private void LstSound_ItemSelected(object sender, EventArgs e)
        {
            Music.Music.AudioPlayer.PlaySoundEffect(((ListBoxTextItem)this.lstSound.SelectedItems[0]).Text);
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public static void PriceText(string text)
        {
            lblPrice.Text = text;
        }
    }
}
