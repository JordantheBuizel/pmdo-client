﻿// <copyright file="mnuRecruitSelected.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuRecruitSelected : Widges.BorderedPanel, Core.IMenu
    {
        private int recruitSlot;
        private readonly Label lblJoinTeam;
        private readonly Label lblMakeLeader;
        private readonly Label lblStandby;
        private readonly Label lblRelease;
        private readonly Label lblRename;
        private readonly TextBox txtRename;
        private readonly Widges.MenuItemPicker itemPicker;
        private readonly int maxItems;
        private readonly int activeTeamStatus;

        public int RecruitSlot
        {
            get { return this.recruitSlot; }

            set
            {
                this.recruitSlot = value;
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuRecruitSelected(string name, int recruitSlot, int activeTeamStatus)
            : base(name)
        {
            if (activeTeamStatus == 0)
            {
                this.maxItems = 0;
                this.Size = new Size(165, 95);
            }
            else if (activeTeamStatus == -1)
            {
                this.maxItems = 1;
                this.Size = new Size(165, 95);
            }
else
            {
                this.maxItems = 2;
                this.Size = new Size(165, 155);
            }

            this.MenuDirection = Enums.MenuDirection.Horizontal;
            this.Location = new Point(310, 40);

            if (activeTeamStatus == 0)
            {
                this.lblRename = new Label("lblRename");
                this.lblRename.Font = FontManager.LoadFont("PMU", 32);
                this.lblRename.AutoSize = true;
                this.lblRename.Text = "Rename:";
                this.lblRename.Location = new Point(30, 8);
                this.lblRename.ForeColor = Color.WhiteSmoke;

                // lblRename.HoverColor = Color.Red;
                // lblRename.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(lblRename_Click);
                this.AddWidget(this.lblRename);

                this.txtRename = new TextBox("txtRename");
                this.txtRename.Size = new Size(120, 24);
                this.txtRename.Location = new Point(32, 43);
                this.txtRename.Font = FontManager.LoadFont("PMU", 16);
                this.AddWidget(this.txtRename);
            }
            else if (activeTeamStatus > 0 && activeTeamStatus < 4)
            {
                this.lblMakeLeader = new Label("lblMakeLeader");
                this.lblMakeLeader.Font = FontManager.LoadFont("PMU", 32);
                this.lblMakeLeader.AutoSize = true;
                this.lblMakeLeader.Text = "Make Leader";
                this.lblMakeLeader.Location = new Point(30, 8);
                this.lblMakeLeader.ForeColor = Color.WhiteSmoke;

                // lblMakeLeader.HoverColor = Color.Red;
                // lblMakeLeader.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(lblMakeLeader_Click);
                this.AddWidget(this.lblMakeLeader);

                this.lblStandby = new Label("lblStandby");
                this.lblStandby.Font = FontManager.LoadFont("PMU", 32);
                this.lblStandby.AutoSize = true;
                this.lblStandby.Text = "Standby";
                this.lblStandby.Location = new Point(30, 38);
                this.lblStandby.ForeColor = Color.WhiteSmoke;

                // lblStandby.HoverColor = Color.Red;
                // lblStandby.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(lblStandby_Click);
                this.AddWidget(this.lblStandby);

                this.lblRename = new Label("lblRename");
                this.lblRename.Font = FontManager.LoadFont("PMU", 32);
                this.lblRename.AutoSize = true;
                this.lblRename.Text = "Rename:";
                this.lblRename.Location = new Point(30, 68);
                this.lblRename.ForeColor = Color.WhiteSmoke;

                // lblRename.HoverColor = Color.Red;
                // lblRename.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(lblRename_Click);
                this.AddWidget(this.lblRename);

                this.txtRename = new TextBox("txtRename");
                this.txtRename.Size = new Size(120, 24);
                this.txtRename.Location = new Point(32, 103);
                this.txtRename.Font = FontManager.LoadFont("PMU", 16);
                this.AddWidget(this.txtRename);
            }
else
            {
                this.lblJoinTeam = new Label("lblJoinTeam");
                this.lblJoinTeam.Font = FontManager.LoadFont("PMU", 32);
                this.lblJoinTeam.AutoSize = true;
                this.lblJoinTeam.Text = "Join Team";
                this.lblJoinTeam.Location = new Point(30, 8);
                this.lblJoinTeam.ForeColor = Color.WhiteSmoke;

                // lblJoinTeam.HoverColor = Color.Red;
                // lblJoinTeam.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(lblJoinTeam_Click);
                this.AddWidget(this.lblJoinTeam);

                this.lblRelease = new Label("lblRelease");
                this.lblRelease.Font = FontManager.LoadFont("PMU", 32);
                this.lblRelease.AutoSize = true;
                this.lblRelease.Text = "Release";
                this.lblRelease.Location = new Point(30, 38);
                this.lblRelease.ForeColor = Color.WhiteSmoke;

                // lblRelease.HoverColor = Color.Red;
                // lblRelease.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(lblRelease_Click);
                this.AddWidget(this.lblRelease);
            }

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 23);
            this.AddWidget(this.itemPicker);

            this.RecruitSlot = recruitSlot;
            this.activeTeamStatus = activeTeamStatus;
        }

        private void LblJoinTeam_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(0, this.activeTeamStatus, this.recruitSlot);
        }

        private void LblMakeLeader_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1, this.activeTeamStatus, this.recruitSlot);
        }

        private void LblStandby_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(2, this.activeTeamStatus, this.recruitSlot);
        }

        private void LblRelease_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(3, this.activeTeamStatus, this.recruitSlot);
        }

        private void LblRename_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(3, this.activeTeamStatus, this.recruitSlot);
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 23 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                    {
                        if (this.itemPicker.SelectedItem == this.maxItems)
                        {
                            this.ChangeSelected(0);
                        }
                        else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                    {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(this.maxItems);
                        }
                        else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                    {
                        this.SelectItem(this.itemPicker.SelectedItem, this.activeTeamStatus, this.recruitSlot);
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                    {
                        this.CloseMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum, int menuType, int recruitSlot)
        {
            if (menuType == 0)
            {
                menuType = 2;
            }
            else if (menuType > 0 && menuType < 4)
            {
                menuType = 1;
            }
else
            {
                menuType = 0;
            }

            if (menuType == 1 && itemNum == 0)
            {
                // Make Leader
                Network.Messenger.SendSwitchLeader(this.activeTeamStatus);
                this.CloseMenu();
            }
            else if (menuType == 1 && itemNum == 1)
            {
                // standby
                Network.Messenger.SendStandbyFromTeam(this.activeTeamStatus);
                this.CloseMenu();
            }
            else if (menuType == 0 && itemNum == 0)
            {
                // join team
                int freeSlot = -1;
                for (int i = 1; i < 4; i++)
                {
                    if (Players.PlayerManager.MyPlayer.Team[i] == null || Players.PlayerManager.MyPlayer.Team[i].Loaded == false)
                    {
                        freeSlot = i;
                        break;
                    }
                }

                if (freeSlot == -1)
                {
                    // tell it's not possible
                }
else
                {
                    Network.Messenger.SendAddToTeam(freeSlot, recruitSlot);
                }

                this.CloseMenu();
            }
            else if (menuType == 0 && itemNum == 1)
            {
                // farewell
                Network.Messenger.SendReleaseRecruit(recruitSlot);
                this.CloseMenu();
                MenuSwitcher.CloseAllMenus();
            }
            else if (menuType == 2 || itemNum == 2)
            {
                // rename
                // if (txtRename.Text != "") {
                    Network.Messenger.SendChangeRecruitName(this.activeTeamStatus, this.txtRename.Text);

                // }
                this.CloseMenu();
            }

            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");

            /*
            switch (itemNum)
            {
                case 0:
                    { // Use move
                        Players.PlayerManager.MyPlayer.UseMove(moveSlot);
                        CloseMenu();
                    }
                    break;
                case 1:
                    { // Shift Up
                        if (moveSlot > 0)
                        {
                            Players.PlayerManager.MyPlayer.ShiftMove(moveSlot, true);
                            CloseMenu();
                        }
                    }
                    break;
                case 2:
                    { // Shift Down
                        if (moveSlot < 3)
                        {
                            Players.PlayerManager.MyPlayer.ShiftMove(moveSlot, false);
                            CloseMenu();
                        }
                    }
                    break;
                case 3:
                    { // Forget move
                        Players.PlayerManager.MyPlayer.ForgetMove(moveSlot);
                        CloseMenu();
                    }
                    break;
            }
            */
        }

        private void CloseMenu()
        {
            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(this);
            Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuAssembly");
        }

        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }
    }
}
