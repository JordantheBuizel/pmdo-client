﻿// <copyright file="mnuGuild.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using SdlDotNet.Widgets;

    internal class MnuGuild : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private const int MAXITEMS = 2;

        private readonly Widges.MenuItemPicker itemPicker;
        private readonly Label lblGuild;
        private readonly Label lblName;
        private readonly TextBox txtName;
        private readonly Label lblMakeTrainee;
        private readonly Label lblMakeMember;
        private readonly Label lblDisown;

        public MnuGuild(string name)
            : base(name)
            {
            this.Size = new Size(280, 206);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 111);

            this.lblGuild = new Label("lblGuild");
            this.lblGuild.Location = new Point(20, 0);
            this.lblGuild.AutoSize = true;
            this.lblGuild.Font = FontManager.LoadFont("PMU", 48);
            this.lblGuild.Text = "Guild";
            this.lblGuild.ForeColor = Color.WhiteSmoke;

            this.lblName = new Label("lblName");
            this.lblName.AutoSize = true;
            this.lblName.Location = new Point(30, 48);
            this.lblName.Font = FontManager.LoadFont("PMU", 32);
            this.lblName.Text = "Name";
            this.lblName.ForeColor = Color.WhiteSmoke;

            // lblName.HoverColor = Color.Red;
            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(120, 24);
            this.txtName.Location = new Point(32, 80);
            this.txtName.Font = FontManager.LoadFont("PMU", 16);
            Skins.SkinManager.LoadTextBoxGui(this.txtName);

            this.lblMakeTrainee = new Label("lblMakeTrainee");
            this.lblMakeTrainee.AutoSize = true;
            this.lblMakeTrainee.Location = new Point(30, 96);
            this.lblMakeTrainee.Font = FontManager.LoadFont("PMU", 32);
            this.lblMakeTrainee.Text = "Make Trainee";
            this.lblMakeTrainee.HoverColor = Color.Red;
            this.lblMakeTrainee.ForeColor = Color.WhiteSmoke;
            this.lblMakeTrainee.Click += new EventHandler<MouseButtonEventArgs>(this.LblMakeTrainee_Click);

            this.lblMakeMember = new Label("lblMakeMember");
            this.lblMakeMember.AutoSize = true;
            this.lblMakeMember.Location = new Point(30, 126);
            this.lblMakeMember.Font = FontManager.LoadFont("PMU", 32);
            this.lblMakeMember.Text = "Make Member";
            this.lblMakeMember.HoverColor = Color.Red;
            this.lblMakeMember.ForeColor = Color.WhiteSmoke;
            this.lblMakeMember.Click += new EventHandler<MouseButtonEventArgs>(this.LblMakeMember_Click);

            this.lblDisown = new Label("lblDisown");
            this.lblDisown.AutoSize = true;
            this.lblDisown.Location = new Point(30, 156);
            this.lblDisown.Font = FontManager.LoadFont("PMU", 32);
            this.lblDisown.Text = "Disown";
            this.lblDisown.HoverColor = Color.Red;
            this.lblDisown.ForeColor = Color.WhiteSmoke;
            this.lblDisown.Click += new EventHandler<MouseButtonEventArgs>(this.LblDisown_Click);

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblGuild);
            this.AddWidget(this.lblName);
            this.AddWidget(this.txtName);
            this.AddWidget(this.lblMakeTrainee);
            this.AddWidget(this.lblMakeMember);
            this.AddWidget(this.lblDisown);
        }

        private void LblMakeTrainee_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(0);
        }

        private void LblMakeMember_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1);
        }

        private void LblDisown_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(2);
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 111 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem);
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                    {
                        // if (txtName.
                        MenuSwitcher.ShowMainMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum)
        {
            // switch (itemNum) {
            //    case 0: {
            //            Messenger.MakeTrainee(txtName.Text);
            //        }
            //        break;
            //    case 1: {
            //            Messenger.MakeMember(txtName.Text);
            //        }
            //        break;
            //    case 2: {
            //            Messenger.Disown(txtName.Text);
            //        }
            //        break;

            // }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }
    }
}
