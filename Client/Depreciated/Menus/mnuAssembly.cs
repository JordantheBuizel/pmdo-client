﻿// <copyright file="mnuAssembly.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class MnuAssembly : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private int maxItems;

        public int CurrentTen;

        private readonly int[] team;
        private readonly List<Players.Recruit> recruitList;
        private readonly List<int> recruitIndex;
        private List<int> sortedRecruits;

        private readonly Widges.MenuItemPicker itemPicker;
        private readonly PictureBox picMugshot;
        private readonly Label lblName;
        private readonly Label lblLevel;
        private readonly Label lblAssembly;
        private readonly Label lblRecruitNum;
        private readonly Label[] lblAllRecruits;
        private readonly TextBox txtFind;
        private readonly Button btnFind;
        private readonly ComboBox cbxSortType;
        private readonly Button btnSort;

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuAssembly(string name, string[] parse)
            : base(name)
            {
            this.Size = new Size(315, 450);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 20);

            this.CurrentTen = 0;

            this.team = new int[4];

            this.recruitList = new List<Players.Recruit>();
            this.recruitIndex = new List<int>();

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");

            this.lblAssembly = new Label("lblAssembly");
            this.lblAssembly.AutoSize = true;
            this.lblAssembly.Font = FontManager.LoadFont("PMU", 48);
            this.lblAssembly.Text = "Assembly";
            this.lblAssembly.Location = new Point(20, 0);
            this.lblAssembly.ForeColor = Color.WhiteSmoke;

            this.lblRecruitNum = new Label("lblRecruitNum");

            // lblItemNum.Size = new Size(100, 30);
            this.lblRecruitNum.AutoSize = true;
            this.lblRecruitNum.Location = new Point(222, 14);
            this.lblRecruitNum.Font = FontManager.LoadFont("PMU", 32);
            this.lblRecruitNum.BackColor = Color.Transparent;
            this.lblRecruitNum.Text = "0/0";
            this.lblRecruitNum.ForeColor = Color.WhiteSmoke;

            this.txtFind = new TextBox("txtFind");
            this.txtFind.Size = new Size(130, 20);
            this.txtFind.Location = new Point(32, 48);
            this.txtFind.Font = FontManager.LoadFont("PMU", 16);
            Skins.SkinManager.LoadTextBoxGui(this.txtFind);

            this.btnFind = new Button("btnFind");
            this.btnFind.Size = new Size(40, 20);
            this.btnFind.Location = new Point(174, 48);
            this.btnFind.Font = FontManager.LoadFont("PMU", 16);
            this.btnFind.Text = "Find";
            Skins.SkinManager.LoadButtonGui(this.btnFind);
            this.btnFind.Click += new EventHandler<MouseButtonEventArgs>(this.BtnFind_Click);

            this.cbxSortType = new ComboBox("cbxSortType");
            this.cbxSortType.Items.Clear();
            this.cbxSortType.Size = new Size(130, 20);
            this.cbxSortType.BorderColor = Color.Black;
            this.cbxSortType.BorderWidth = 1;
            this.cbxSortType.BorderStyle = this.BorderStyle.FixedSingle;
            this.cbxSortType.Items.Add(new ListBoxTextItem(FontManager.LoadFont("PMU", 16), "Default"));
            this.cbxSortType.Items.Add(new ListBoxTextItem(FontManager.LoadFont("PMU", 16), "Dex Number"));
            this.cbxSortType.Items.Add(new ListBoxTextItem(FontManager.LoadFont("PMU", 16), "Level"));
            this.cbxSortType.Items.Add(new ListBoxTextItem(FontManager.LoadFont("PMU", 16), "Type"));
            this.cbxSortType.Location = new Point(32, 72);
            this.cbxSortType.SelectItem(0);

            this.btnSort = new Button("btnSort");
            this.btnSort.Size = new Size(40, 20);
            this.btnSort.Location = new Point(174, 72);
            this.btnSort.Text = "Sort";
            Skins.SkinManager.LoadButtonGui(this.btnSort);
            this.btnSort.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSort_Click);

            this.picMugshot = new PictureBox("picMugshot");
            this.picMugshot.Size = new Size(40, 40);
            this.picMugshot.BackColor = Color.Transparent;
            this.picMugshot.Location = new Point(35, 100);

            this.lblName = new Label("lblName");
            this.lblName.AutoSize = true;
            this.lblName.Centered = true;
            this.lblName.Font = FontManager.LoadFont("PMU", 16);
            this.lblName.ForeColor = Color.WhiteSmoke;
            this.lblName.Location = new Point(75, 100);

            this.lblLevel = new Label("lblLevel");
            this.lblLevel.AutoSize = true;
            this.lblLevel.Centered = true;
            this.lblLevel.Font = FontManager.LoadFont("PMU", 16);
            this.lblLevel.ForeColor = Color.WhiteSmoke;
            this.lblLevel.Location = new Point(75, 120);

            this.lblAllRecruits = new Label[10];
            for (int i = 0; i < 10; i++)
            {
                this.lblAllRecruits[i] = new Label("lblAllRecruits" + i);

                // lblAllRecruits[i].AutoSize = true;
                // lblAllRecruits[i].Centered = true;
                this.lblAllRecruits[i].Width = 200;
                this.lblAllRecruits[i].Font = FontManager.LoadFont("PMU", 32);

                this.lblAllRecruits[i].Location = new Point(35, (i * 30) + 138);

                // lblAllRecruits[i].HoverColor = Color.Red;
                // lblAllRecruits[i].Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(assemblyItem_Click);
                this.AddWidget(this.lblAllRecruits[i]);
            }

            this.LoadRecruitsFromPacket(parse);

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.txtFind);
            this.AddWidget(this.btnFind);
            this.AddWidget(this.cbxSortType);
            this.AddWidget(this.btnSort);
            this.AddWidget(this.lblAssembly);
            this.AddWidget(this.lblRecruitNum);
            this.AddWidget(this.picMugshot);
            this.AddWidget(this.lblName);
            this.AddWidget(this.lblLevel);

            this.FindMaxItems();
            this.DisplayRecruitList();
            this.UpdateSelectedRecruitInfo();
            this.ChangeSelected(0);
        }

        private void BtnSort_Click(object sender, MouseButtonEventArgs e)
        {
            this.sortedRecruits = null;
            this.sortedRecruits = new List<int>();
            if (this.cbxSortType.SelectedItem.TextIdentifier == "Default")
            {
                this.sortedRecruits = null;
            }
            else if (this.cbxSortType.SelectedItem.TextIdentifier == "Level")
            {
                for (int i = 0; i < this.recruitList.Count; i++)
                {
                    if (i == 0)
                    {
                        this.sortedRecruits.Add(i);
                    }
                    else
                    {
                        for (int h = 0; h < this.sortedRecruits.Count; h++)
                        {
                            if (this.recruitList[i].Level == this.recruitList[this.sortedRecruits[h]].Level)
                            {
                                this.sortedRecruits.Insert(h, i);
                                break;
                            }

                            if (this.recruitList[i].Level < this.recruitList[this.sortedRecruits[h]].Level)
                            {
                                for (int j = h - 1; this.sortedRecruits.Count - j > 0; j--)
                                {
                                    if (this.recruitList[i].Level > this.recruitList[this.sortedRecruits[j]].Level)
                                    {
                                        this.sortedRecruits.Insert(j + 1, i);
                                    }
                                }

                                break;
                            }
                        }
                    }
                }
            }
        }

        public void LoadRecruitsFromPacket(string[] parse)
        {
            int recruitCount = parse[1].ToInt();
            this.team[0] = parse[2].ToInt();
            this.team[1] = parse[3].ToInt();
            this.team[2] = parse[4].ToInt();
            this.team[3] = parse[5].ToInt();
            int n = 6;
            for (int i = 0; i < recruitCount; i++)
            {
                this.recruitIndex.Add(parse[n].ToInt());
                Players.Recruit recruit = new Players.Recruit();
                recruit.Num = parse[n + 1].ToInt();
                recruit.Sex = (Enums.Sex)parse[n + 2].ToInt();
                recruit.Form = parse[n + 3].ToInt();
                recruit.Shiny = (Enums.Coloration)parse[n + 4].ToInt();
                recruit.Level = parse[n + 5].ToInt();
                recruit.Name = parse[n + 6];
                this.recruitList.Add(recruit);
                n += 7;
            }
        }

        private void BtnFind_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.sortedRecruits == null)
            {
                if (this.txtFind.Text.Trim() != string.Empty)
                {
                    this.sortedRecruits = new List<int>();

                    for (int i = 0; i < this.recruitList.Count; i++)
                    {
                        if (this.recruitList[i].Name.ToLower().Contains(this.txtFind.Text.ToLower()))
                        {
                            this.sortedRecruits.Add(i);
                        }
                    }

                    this.btnFind.Text = "Cancel";
                }
            }
else
            {
                this.sortedRecruits = null;
                this.btnFind.Text = "Find";
            }

            this.CurrentTen = 0;
            this.ChangeSelected(0);
            this.FindMaxItems();
            this.DisplayRecruitList();
            this.UpdateSelectedRecruitInfo();
        }

        private void AssemblyItem_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectRecruit(Array.IndexOf(this.lblAllRecruits, sender) + (this.CurrentTen * 10));
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 154 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        public void UpdateSelectedRecruitInfo()
        {
            if (this.sortedRecruits == null)
            {
                if (this.itemPicker.SelectedItem + (this.CurrentTen * 10) >= 0 && this.itemPicker.SelectedItem + (this.CurrentTen * 10) < this.recruitList.Count)
                {
                    System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
                    watch.Start();
                    Players.Recruit recruit = this.recruitList[this.itemPicker.SelectedItem + (this.CurrentTen * 10)];
                    this.picMugshot.Image = GraphicsManager.GetMugshot(recruit.Num, string.Empty, (int)recruit.Shiny, (int)recruit.Sex).GetEmote(0); // Tools.CropImage(Logic.Graphic.GraphicsManager.Speakers, new Rectangle((recruitList[itemPicker.SelectedItem + currentTen * 10].Mugshot % 15) * 40, string.Empty, 40, 40));
                    watch.Stop();
                    this.lblName.Text = this.recruitList[this.itemPicker.SelectedItem + (this.CurrentTen * 10)].Name;
                    string levelString = "Lv. " + this.recruitList[this.itemPicker.SelectedItem + (this.CurrentTen * 10)].Level + "  ";
                    if (this.recruitList[this.itemPicker.SelectedItem + (this.CurrentTen * 10)].Sex == Enums.Sex.Male)
                    {
                        levelString += "(♂)";
                    }
                    else if (this.recruitList[this.itemPicker.SelectedItem + (this.CurrentTen * 10)].Sex == Enums.Sex.Female)
                    {
                        levelString += "(♀)";
                    }

                    this.lblLevel.Text = levelString;

                    this.lblRecruitNum.Text = (this.CurrentTen + 1) + "/" + (((this.recruitList.Count - 1) / 10) + 1);
                }
            }
else
            {
                if (this.itemPicker.SelectedItem + (this.CurrentTen * 10) >= 0 && this.itemPicker.SelectedItem + (this.CurrentTen * 10) < this.sortedRecruits.Count)
                {
                    System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
                    watch.Start();

                    // picMugshot.BlitToBuffer(Logic.Graphic.GraphicsManager.Speakers, new Rectangle((recruitList[itemPicker.SelectedItem + currentTen * 10].Mugshot % 15) * 40, (recruitList[itemPicker.SelectedItem + currentTen * 10].Mugshot / 15) * 40, 40, 40));
                    Players.Recruit recruit = this.recruitList[this.sortedRecruits[this.itemPicker.SelectedItem + (this.CurrentTen * 10)]];
                    this.picMugshot.Image = GraphicsManager.GetMugshot(recruit.Num, string.Empty, (int)recruit.Shiny, (int)recruit.Sex).GetEmote(0); // Tools.CropImage(Logic.Graphic.GraphicsManager.Speakers, new Rectangle((recruitList[itemPicker.SelectedItem + currentTen * 10].Mugshot % 15) * 40, string.Empty, 40, 40));
                    watch.Stop();
                    this.lblName.Text = this.recruitList[this.sortedRecruits[this.itemPicker.SelectedItem + (this.CurrentTen * 10)]].Name;
                    string levelString = "Lv. " + this.recruitList[this.sortedRecruits[this.itemPicker.SelectedItem + (this.CurrentTen * 10)]].Level + "  ";
                    if (this.recruitList[this.sortedRecruits[this.itemPicker.SelectedItem + (this.CurrentTen * 10)]].Sex == Enums.Sex.Male)
                    {
                        levelString += "(♂)";
                    }
                    else if (this.recruitList[this.sortedRecruits[this.itemPicker.SelectedItem + (this.CurrentTen * 10)]].Sex == Enums.Sex.Female)
                    {
                        levelString += "(♀)";
                    }

                    this.lblLevel.Text = levelString;

                    this.lblRecruitNum.Text = (this.CurrentTen + 1) + "/" + (((this.sortedRecruits.Count - 1) / 10) + 1);
                }
else
                {
                    this.picMugshot.Image = GraphicsManager.GetMugshot(0, string.Empty, 0, 0).GetEmote(0);
                    this.lblName.Text = "---";
                    this.lblLevel.Text = "---";
                    this.lblRecruitNum.Text = "0/0";
                }
            }
        }

        public void DisplayRecruitList()
        {
            // this.BeginUpdate();
            for (int i = 0; i < 10; i++)
            {
                if (this.sortedRecruits == null)
                {
                    if ((i + (this.CurrentTen * 10)) < this.recruitList.Count)
                    {
                        if (this.recruitIndex[i + (this.CurrentTen * 10)] == this.team[0])
                        {
                            this.lblAllRecruits[i].ForeColor = Color.Cyan;
                        }
                        else if (this.recruitIndex[i + (this.CurrentTen * 10)] == this.team[1] || this.recruitIndex[i + (this.CurrentTen * 10)] == this.team[2] || this.recruitIndex[i + (this.CurrentTen * 10)] == this.team[3])
                        {
                            this.lblAllRecruits[i].ForeColor = Color.Yellow;
                        }
else
                        {
                            this.lblAllRecruits[i].ForeColor = Color.WhiteSmoke;
                        }

                        if (!string.IsNullOrEmpty(this.recruitList[i + (this.CurrentTen * 10)].Name))
                        {
                            this.lblAllRecruits[i].Text = this.recruitList[i + (this.CurrentTen * 10)].Name;
                        }
else
                        {
                            this.lblAllRecruits[i].Text = "No Name";
                        }

                        if (this.lblAllRecruits[i].Visible == false)
                        {
                            this.lblAllRecruits[i].Visible = true;
                        }
                    }
else
                    {
                        this.lblAllRecruits[i].Visible = false;
                    }
                }
else
                {
                    if ((i + (this.CurrentTen * 10)) < this.sortedRecruits.Count)
                    {
                        if (this.recruitIndex[this.sortedRecruits[i + (this.CurrentTen * 10)]] == this.team[0])
                        {
                            this.lblAllRecruits[i].ForeColor = Color.Cyan;
                        }
                        else if (this.recruitIndex[this.sortedRecruits[i + (this.CurrentTen * 10)]] == this.team[1] || this.recruitIndex[this.sortedRecruits[i + (this.CurrentTen * 10)]] == this.team[2] || this.recruitIndex[this.sortedRecruits[i + (this.CurrentTen * 10)]] == this.team[3])
                        {
                            this.lblAllRecruits[i].ForeColor = Color.Yellow;
                        }
else
                        {
                            this.lblAllRecruits[i].ForeColor = Color.WhiteSmoke;
                        }

                        if (!string.IsNullOrEmpty(this.recruitList[this.sortedRecruits[i + (this.CurrentTen * 10)]].Name))
                        {
                            this.lblAllRecruits[i].Text = this.recruitList[this.sortedRecruits[i + (this.CurrentTen * 10)]].Name;
                        }
else
                        {
                            this.lblAllRecruits[i].Text = "No Name";
                        }

                        if (this.lblAllRecruits[i].Visible == false)
                        {
                            this.lblAllRecruits[i].Visible = true;
                        }
                    }
else
                    {
                        if (i == 0)
                        {
                            this.lblAllRecruits[i].Text = "None";
                            this.lblAllRecruits[i].ForeColor = Color.Gray;
                            if (this.lblAllRecruits[i].Visible == false)
                            {
                                this.lblAllRecruits[i].Visible = true;
                            }
                        }
else
                        {
                            this.lblAllRecruits[i].Visible = false;
                        }
                    }
                }
            }

            this.RequestRedraw();
        }

        public void FindMaxItems()
        {
            if (this.sortedRecruits == null)
            {
                if (this.recruitList.Count < (this.CurrentTen + 1) * 10)
                {
                    this.maxItems = (this.recruitList.Count % 10) - 1;
                }
else
                {
                    this.maxItems = 9;
                }
            }
else
            {
                if (this.sortedRecruits.Count < (this.CurrentTen + 1) * 10)
                {
                    this.maxItems = (this.sortedRecruits.Count % 10) - 1;
                }
else
                {
                    this.maxItems = 9;
                }
            }
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            // if (Loaded) {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.maxItems > -1)
                        {
                            if (this.itemPicker.SelectedItem >= this.maxItems)
                            {
                                this.ChangeSelected(0);
                            }
else
                            {
                                this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                            }

                            this.UpdateSelectedRecruitInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                        }
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.maxItems > -1)
                        {
                            if (this.itemPicker.SelectedItem <= 0)
                            {
                                this.ChangeSelected(this.maxItems);
                            }
else
                            {
                                this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                            }

                            this.UpdateSelectedRecruitInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                        }
                    }

                    break;
                case SdlDotNet.Input.Key.LeftArrow:
                {
                        if (this.maxItems > -1)
                        {
                            if (this.CurrentTen <= 0)
                            {
                                if (this.sortedRecruits == null)
                                {
                                    this.CurrentTen = (this.recruitList.Count - 1) / 10;
                                }
else
                                {
                                    this.CurrentTen = (this.sortedRecruits.Count - 1) / 10;
                                }
                            }
else
                            {
                                this.CurrentTen--;
                            }

                            this.FindMaxItems();
                            if (this.itemPicker.SelectedItem > this.maxItems)
                            {
                                this.ChangeSelected(this.maxItems);
                            }

                            this.DisplayRecruitList();
                            this.UpdateSelectedRecruitInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                        }
                    }

                    break;
                case SdlDotNet.Input.Key.RightArrow:
                {
                        if (this.maxItems > -1)
                        {
                            if (this.sortedRecruits == null)
                            {
                                if (this.CurrentTen >= ((this.recruitList.Count - 1) / 10))
                                {
                                    this.CurrentTen = 0;
                                }
else
                                {
                                    this.CurrentTen++;
                                }
                            }
else
                            {
                                if (this.CurrentTen >= ((this.sortedRecruits.Count - 1) / 10))
                                {
                                    this.CurrentTen = 0;
                                }
else
                                {
                                    this.CurrentTen++;
                                }
                            }

                            this.FindMaxItems();
                            if (this.itemPicker.SelectedItem > this.maxItems)
                            {
                                this.ChangeSelected(this.maxItems);
                            }

                            this.DisplayRecruitList();
                            this.UpdateSelectedRecruitInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                        }
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectRecruit(this.itemPicker.SelectedItem + (this.CurrentTen * 10));
                    }

                    break;
            }

            // }
        }

        private void SelectRecruit(int slot)
        {
            int activeTeamStatus;
            if (this.sortedRecruits == null)
            {
                if (this.recruitIndex[slot] == this.team[0])
                {
                    activeTeamStatus = 0;
                }
                else if (this.recruitIndex[slot] == this.team[1])
                {
                    activeTeamStatus = 1;
                }
                else if (this.recruitIndex[slot] == this.team[2])
                {
                    activeTeamStatus = 2;
                }
                else if (this.recruitIndex[slot] == this.team[3])
                {
                    activeTeamStatus = 3;
                }
else
                {
                    activeTeamStatus = -1;
                }

                Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuRecruitSelected("mnuRecruitSelected", this.recruitIndex[slot], activeTeamStatus));
                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuRecruitSelected");
                Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
            }
            else if (this.sortedRecruits.Count > slot)
            {
                if (this.recruitIndex[this.sortedRecruits[slot]] == this.team[0])
                {
                    activeTeamStatus = 0;
                }
                else if (this.recruitIndex[this.sortedRecruits[slot]] == this.team[1])
                {
                    activeTeamStatus = 1;
                }
                else if (this.recruitIndex[this.sortedRecruits[slot]] == this.team[2])
                {
                    activeTeamStatus = 2;
                }
                else if (this.recruitIndex[this.sortedRecruits[slot]] == this.team[3])
                {
                    activeTeamStatus = 3;
                }
else
                {
                    activeTeamStatus = -1;
                }

                Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuRecruitSelected("mnuRecruitSelected", this.recruitIndex[this.sortedRecruits[slot]], activeTeamStatus));
                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuRecruitSelected");
                Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
            }
        }
    }
}
