﻿// <copyright file="mnuMainMenu.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuMainMenu : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private const int MAXITEMS = 4;

        private readonly Widges.MenuItemPicker itemPicker;

        // Label lblGuild;
        private readonly Label lblItems;
        private readonly Label lblJobList;
        private readonly Label lblMoves;
        private readonly Label lblOthers;
        private readonly Label lblTeam;

        public MnuMainMenu(string name)
            : base(name)
            {
            this.Size = new Size(135, 178);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 23);

            this.lblMoves = new Label("lblMoves");
            this.lblMoves.AutoSize = true;
            this.lblMoves.Location = new Point(30, 8);
            this.lblMoves.Font = FontManager.LoadFont("PMU", 32);
            this.lblMoves.Text = "Moves";
            this.lblMoves.HoverColor = Color.Red;
            this.lblMoves.ForeColor = Color.WhiteSmoke;
            this.lblMoves.Click += new EventHandler<MouseButtonEventArgs>(this.LblMoves_Click);

            this.lblItems = new Label("lblItems");
            this.lblItems.AutoSize = true;
            this.lblItems.Location = new Point(30, 38);
            this.lblItems.Font = FontManager.LoadFont("PMU", 32);
            this.lblItems.Text = "Items";
            this.lblItems.HoverColor = Color.Red;
            this.lblItems.ForeColor = Color.WhiteSmoke;
            this.lblItems.Click += new EventHandler<MouseButtonEventArgs>(this.LblItems_Click);

            this.lblTeam = new Label("lblTeam");
            this.lblTeam.AutoSize = true;
            this.lblTeam.Location = new Point(30, 68);
            this.lblTeam.Font = FontManager.LoadFont("PMU", 32);
            this.lblTeam.Text = "Team";
            this.lblTeam.HoverColor = Color.Red;
            this.lblTeam.ForeColor = Color.WhiteSmoke;
            this.lblTeam.Click += new EventHandler<MouseButtonEventArgs>(this.LblTeam_Click);

            // lblGuild = new Label("lblGuild");
            // lblGuild.AutoSize = true;
            // lblGuild.Location = new Point(30, 98);
            // lblGuild.Font = FontManager.LoadFont("PMU", 32);
            // lblGuild.Text = "Guild";
            // lblGuild.HoverColor = Color.Red;
            // lblGuild.ForeColor = Color.WhiteSmoke;
            // lblGuild.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(lblGuild_Click);
            this.lblJobList = new Label("lblJobList");
            this.lblJobList.AutoSize = true;
            this.lblJobList.Location = new Point(30, 98);
            this.lblJobList.Font = FontManager.LoadFont("PMU", 32);
            this.lblJobList.Text = "Job List";
            this.lblJobList.HoverColor = Color.Red;
            this.lblJobList.ForeColor = Color.WhiteSmoke;
            this.lblJobList.Click += new EventHandler<MouseButtonEventArgs>(this.LblJobList_Click);

            this.lblOthers = new Label("lblOthers");
            this.lblOthers.AutoSize = true;
            this.lblOthers.Location = new Point(30, 128);
            this.lblOthers.Font = FontManager.LoadFont("PMU", 32);
            this.lblOthers.Text = "Others";
            this.lblOthers.HoverColor = Color.Red;
            this.lblOthers.ForeColor = Color.WhiteSmoke;
            this.lblOthers.Click += new EventHandler<MouseButtonEventArgs>(this.LblOthers_Click);

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblMoves);
            this.AddWidget(this.lblItems);
            this.AddWidget(this.lblTeam);

            // this.AddWidget(lblGuild);
            this.AddWidget(this.lblJobList);
            this.AddWidget(this.lblOthers);
        }

        private void LblMoves_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(0);
        }

        private void LblItems_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1);
        }

        private void LblTeam_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(2);
        }

        // void lblGuild_Click(object sender, SdlDotNet.Widgets.MouseButtonEventArgs e) {
        //    SelectItem(3);
        // }
        private void LblJobList_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(3);
        }

        private void LblOthers_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(4);
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 23 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem);
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum)
        {
            switch (itemNum)
            {
                case 0:
                {
                        MenuSwitcher.ShowMovesMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case 1:
                {
                        MenuSwitcher.ShowInventoryMenu(1);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case 2:
                {
                        MenuSwitcher.ShowTeamMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;

                // case 3: {
                //        MenuSwitcher.ShowGuildMenu();
                //        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                //    }
                //    break;
                case 3:
                {
                        MenuSwitcher.ShowJobListMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case 4:
                {
                        MenuSwitcher.ShowOthersMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
            }
        }
    }
}