﻿// <copyright file="mnuJobSelected.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using SdlDotNet.Widgets;

    internal class MnuJobSelected : Widges.BorderedPanel, Core.IMenu
    {
        private int jobSlot;
        private readonly Label lblAccept;
        private readonly Label lblDescription;
        private readonly Label lblDelete;
        private readonly Label lblSend;
        private readonly TextBox txtSend;
        private readonly Widges.MenuItemPicker itemPicker;
        private readonly int maxItems;

        public int JobSlot
        {
            get { return this.jobSlot; }

            set
            {
                this.jobSlot = value;
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuJobSelected(string name, int jobSlot)
            : base(name)
            {
                int size = 95;
            this.maxItems = 1;
                if (Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].Accepted == Enums.JobStatus.Finished ||
                    Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].Accepted == Enums.JobStatus.Failed)
                    {
                }
else
                {
                    size += 30;
                this.maxItems++;
                }

                if (Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].CanSend)
                {
                    size += 60;
                this.maxItems++;
                }

            this.Size = new Size(180, size);
            this.MenuDirection = Enums.MenuDirection.Horizontal;
            this.Location = new Point(300, 34);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 23);

            int locY = 8;

            if (Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].Accepted == Enums.JobStatus.Finished ||
                Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].Accepted == Enums.JobStatus.Failed)
                {
            }
else
            {
                this.lblAccept = new Label("lblAccept");
                this.lblAccept.Font = FontManager.LoadFont("PMU", 32);
                this.lblAccept.AutoSize = true;
                if (Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].Accepted == Enums.JobStatus.Obtained ||
                    Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].Accepted == Enums.JobStatus.Suspended)
                    {
                    this.lblAccept.Text = "Accept";
                }
                    else if (Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].Accepted == Enums.JobStatus.Taken)
                {
                    this.lblAccept.Text = "Cancel";
                }

                this.lblAccept.Location = new Point(30, locY);
                this.lblAccept.HoverColor = Color.Red;
                this.lblAccept.ForeColor = Color.WhiteSmoke;
                this.lblAccept.Click += new EventHandler<MouseButtonEventArgs>(this.LblAccept_Click);

                this.AddWidget(this.lblAccept);

                locY += 30;
            }

            this.lblDescription = new Label("lblDescription");
            this.lblDescription.Font = FontManager.LoadFont("PMU", 32);
            this.lblDescription.AutoSize = true;
            this.lblDescription.Text = "Description";
            this.lblDescription.Location = new Point(30, locY);
            this.lblDescription.HoverColor = Color.Red;
            this.lblDescription.ForeColor = Color.WhiteSmoke;
            this.lblDescription.Click += new EventHandler<MouseButtonEventArgs>(this.LblDescription_Click);
            locY += 30;

            this.lblDelete = new Label("lblDelete");
            this.lblDelete.Font = FontManager.LoadFont("PMU", 32);
            this.lblDelete.AutoSize = true;
            this.lblDelete.Text = "Delete";
            this.lblDelete.Location = new Point(30, locY);
            this.lblDelete.HoverColor = Color.Red;
            this.lblDelete.ForeColor = Color.WhiteSmoke;
            this.lblDelete.Click += new EventHandler<MouseButtonEventArgs>(this.LblDelete_Click);
            locY += 30;

            if (Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].CanSend)
            {
                this.lblSend = new Label("lblSend");
                this.lblSend.Font = FontManager.LoadFont("PMU", 32);
                this.lblSend.AutoSize = true;
                this.lblSend.Text = "Send to:";
                this.lblSend.Location = new Point(30, locY);
                this.lblSend.HoverColor = Color.Red;
                this.lblSend.ForeColor = Color.WhiteSmoke;
                this.lblSend.Click += new EventHandler<MouseButtonEventArgs>(this.LblSend_Click);
                locY += 40;

                this.txtSend = new TextBox("txtSend");
                this.txtSend.Size = new Size(120, 24);
                this.txtSend.Location = new Point(32, locY);
                this.txtSend.Font = FontManager.LoadFont("PMU", 16);
                Skins.SkinManager.LoadTextBoxGui(this.txtSend);

                this.AddWidget(this.lblSend);
                this.AddWidget(this.txtSend);
            }

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblDescription);
            this.AddWidget(this.lblDelete);

            this.jobSlot = jobSlot;
        }

        private void LblAccept_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(0, this.jobSlot);
        }

        private void LblDescription_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1, this.jobSlot);
        }

        private void LblDelete_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(2, this.jobSlot);
        }

        private void LblSend_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(3, this.jobSlot);
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 23 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == this.maxItems)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(this.maxItems);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem, this.jobSlot);
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                        this.CloseMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum, int jobSlot)
        {
            if (Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].Accepted == Enums.JobStatus.Finished ||
                Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].Accepted == Enums.JobStatus.Failed)
                {
                itemNum++;
            }

            switch (itemNum)
            {
                case 0:
                { // Accept
                        if (Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].Accepted == Enums.JobStatus.Obtained ||
                            Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].Accepted == Enums.JobStatus.Suspended)
                            {
                            Messenger.SendStartMission(jobSlot);
                        }
                            else if (Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot].Accepted == Enums.JobStatus.Taken)
                        {
                            Messenger.SendCancelJob(jobSlot);
                        }

                        MenuSwitcher.ShowJobListMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case 1:
                { // Description
                    MenuSwitcher.ShowJobSummary(Players.PlayerManager.MyPlayer.JobList.Jobs[jobSlot]);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case 2:
                { // Delete
                        Messenger.SendDeleteJob(jobSlot);
                        this.CloseMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case 3:
                { // Send
                        Messenger.SendSendMission(jobSlot, this.txtSend.Text);
                        this.CloseMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
            }
        }

        private void CloseMenu()
        {
            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(this);
            Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuJobList");
        }

        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }
    }
}
