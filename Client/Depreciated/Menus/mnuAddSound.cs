﻿// <copyright file="mnuAddSound.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using SdlDotNet.Widgets;

    internal class MnuAddSound : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        // Label lblAddTile;
        private readonly Label lblAddTile;
        private readonly ScrollingListBox lstSound;
        private readonly Label lblPrice;
        private readonly Button btnAccept;
        private readonly Button btnCancel;
        private int price;

        public MnuAddSound(string name, int price)
            : base(name)
            {
            this.price = price;

            this.Size = new Size(250, 250);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = Graphic.DrawingSupport.GetCenter(Windows.WindowSwitcher.GameWindow.MapViewer.Size, this.Size);

            this.lblAddTile = new Label("lblAddTile");
            this.lblAddTile.Location = new Point(25, 15);
            this.lblAddTile.AutoSize = false;
            this.lblAddTile.Size = new Size(this.Width - (this.lblAddTile.X * 2), 20);
            this.lblAddTile.Text = "Choose a sound to play:";
            this.lblAddTile.ForeColor = Color.WhiteSmoke;

            this.lstSound = new ScrollingListBox("lstSound");
            this.lstSound.Location = new Point(this.lblAddTile.X, this.lblAddTile.Y + this.lblAddTile.Height);
            this.lstSound.Size = new Size(180, 120);

                SdlDotNet.Graphics.Font font = FontManager.LoadFont("PMU", 18);
                string[] sfxFiles = System.IO.Directory.GetFiles(IO.Paths.SfxPath);
                for (int i = 0; i < sfxFiles.Length; i++)
                {
                this.lstSound.Items.Add(new ListBoxTextItem(font, System.IO.Path.GetFileName(sfxFiles[i])));
                }

            this.lstSound.ItemSelected += new EventHandler(this.LstSound_ItemSelected);

            this.lblPrice = new Label("lblPrice");
            this.lblPrice.Location = new Point(this.lblAddTile.X, this.lstSound.Y + this.lstSound.Height + 10);
            this.lblPrice.AutoSize = false;
            this.lblPrice.Size = new Size(120, 30);
            this.lblPrice.Text = "Placing this tile will cost " + price + " " + Items.ItemHelper.Items[1].Name + ".";
            this.lblPrice.ForeColor = Color.WhiteSmoke;

            this.btnAccept = new Button("btnAccept");
            this.btnAccept.Location = new Point(this.lblAddTile.X, this.lblPrice.Y + this.lblPrice.Height + 10);
            this.btnAccept.Size = new Size(80, 30);
            this.btnAccept.Text = "Place Sound";
            this.btnAccept.Font = FontManager.LoadFont("tahoma", 10);
            Skins.SkinManager.LoadButtonGui(this.btnAccept);
            this.btnAccept.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAccept_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(this.btnAccept.X + this.btnAccept.Width, this.lblPrice.Y + this.lblPrice.Height + 10);
            this.btnCancel.Size = new Size(80, 30);
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Font = FontManager.LoadFont("tahoma", 10);
            Skins.SkinManager.LoadButtonGui(this.btnCancel);
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.AddWidget(this.lblAddTile);
            this.AddWidget(this.lstSound);
            this.AddWidget(this.lblPrice);
            this.AddWidget(this.btnAccept);
            this.AddWidget(this.btnCancel);
        }

        private void BtnAccept_Click(object sender, MouseButtonEventArgs e)
        {
            string sound = string.Empty;
            if (this.lstSound.SelectedItems.Count > 0)
            {
                sound = ((ListBoxTextItem)this.lstSound.SelectedItems[0]).Text;
            }

            Messenger.SendAddSoundRequest(sound);
            MenuSwitcher.CloseAllMenus();
            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            MenuSwitcher.CloseAllMenus();
            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        private void LstSound_ItemSelected(object sender, EventArgs e)
        {
            Music.Music.AudioPlayer.PlaySoundEffect(((ListBoxTextItem)this.lstSound.SelectedItems[0]).Text);
        }
    }
}
