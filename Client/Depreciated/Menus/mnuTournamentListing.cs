﻿// <copyright file="mnuTournamentListing.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Tournaments;
    using SdlDotNet.Widgets;

    internal class MnuTournamentListing : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Enums.TournamentListingMode mode;
        private readonly Label[] lblActiveTournaments;
        private readonly Label lblJoinTournament;
        private readonly Widges.MenuItemPicker itemPicker;
        public int CurrentTen;
        private readonly Label lblItemNum;
        private readonly TournamentListing[] listings;

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuTournamentListing(string name, TournamentListing[] listings, Enums.TournamentListingMode mode)
            : base(name)
            {
            this.listings = listings;
            this.mode = mode;

            this.Size = new Size(315, 360);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 63);

            this.lblJoinTournament = new Label("lblJoinTournament");
            this.lblJoinTournament.AutoSize = true;
            this.lblJoinTournament.Font = FontManager.LoadFont("PMU", 48);
            if (mode == Enums.TournamentListingMode.Join)
            {
                this.lblJoinTournament.Text = "Join A Tournament";
            }
            else if (mode == Enums.TournamentListingMode.Spectate)
            {
                this.lblJoinTournament.Text = "Spectate In A Tournament";
            }

            this.lblJoinTournament.ForeColor = Color.WhiteSmoke;
            this.lblJoinTournament.Location = new Point(20, 0);

            this.lblItemNum = new Label("lblItemNum");

            // lblItemNum.Size = new Size(100, 30);
            this.lblItemNum.AutoSize = true;
            this.lblItemNum.Location = new Point(182, 15);
            this.lblItemNum.Font = FontManager.LoadFont("PMU", 32);
            this.lblItemNum.BackColor = Color.Transparent;
            this.lblItemNum.ForeColor = Color.WhiteSmoke;
            this.lblItemNum.Text = string.Empty; // "0/" + ((MaxInfo.MaxInv - 1) / 10 + 1);

            this.lblActiveTournaments = new Label[10];
            for (int i = 0; i < this.lblActiveTournaments.Length; i++)
            {
                this.lblActiveTournaments[i] = new Label("lblActiveTournaments" + i);

                // lblVisibleItems[i].AutoSize = true;
                // lblVisibleItems[i].Size = new Size(200, 32);
                this.lblActiveTournaments[i].Width = 200;
                this.lblActiveTournaments[i].Font = FontManager.LoadFont("PMU", 32);
                this.lblActiveTournaments[i].Location = new Point(35, (i * 30) + 48);

                // lblVisibleItems[i].HoverColor = Color.Red;
                this.lblActiveTournaments[i].ForeColor = Color.WhiteSmoke;
                this.lblActiveTournaments[i].Click += new EventHandler<MouseButtonEventArgs>(this.LblActiveTournament_Click);
                this.AddWidget(this.lblActiveTournaments[i]);
            }

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblJoinTournament);
            this.AddWidget(this.lblItemNum);

            this.CurrentTen = 0;
            this.DisplayItems(this.CurrentTen * 10);
            this.ChangeSelected(0 % 10);
        }

        private void LblActiveTournament_Click(object sender, MouseButtonEventArgs e)
        {
            if (Players.PlayerManager.MyPlayer.GetInvItemNum((this.CurrentTen * 10) + Array.IndexOf(this.lblActiveTournaments, sender)) > 0)
            {
                this.ChangeSelected(Array.IndexOf(this.lblActiveTournaments, sender));

                Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuTournamentListingSelected("mnuTournamentListingSelected", this.listings[this.GetSelectedItemSlot()], this.mode));
                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuTournamentListingSelected");

                Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
            }
        }

        public void DisplayItems(int startNum)
        {
            this.BeginUpdate();
            for (int i = 0; i < this.lblActiveTournaments.Length; i++)
            {
                if (startNum + i >= this.listings.Length)
                {
                    break;
                }

                this.lblActiveTournaments[i].Text = this.listings[startNum + i].Name;
            }

            this.EndUpdate();
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 63 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        private int GetSelectedItemSlot()
        {
            return this.itemPicker.SelectedItem + (this.CurrentTen * 10);
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem >= 9 || this.itemPicker.SelectedItem + 1 + this.CurrentTen >= this.listings.Length)
                        {
                            this.ChangeSelected(0);

                            // DisplayItems(1);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem <= 0)
                        {
                            this.ChangeSelected(Math.Min(9, (this.listings.Length - 1) % 10));
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.LeftArrow:
                {
                        // int itemSlot = (currentTen + 1) - 10;//System.Math.Max(1, GetSelectedItemSlot() - (11 - itemPicker.SelectedItem));
                        if (this.CurrentTen <= 0)
                        {
                            this.CurrentTen = (this.listings.Length - 1) / 10;
                        }
else
                        {
                            this.CurrentTen--;
                        }

                        this.DisplayItems(this.CurrentTen * 10);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.RightArrow:
                {
                        // int itemSlot = currentTen + 1 + 10;
                        if (this.CurrentTen >= ((this.listings.Length - 1) / 10))
                        {
                            this.CurrentTen = 0;
                        }
else
                        {
                            this.CurrentTen++;
                        }

                        this.DisplayItems(this.CurrentTen * 10);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuTournamentListingSelected("mnuTournamentListingSelected", this.listings[this.GetSelectedItemSlot()], this.mode));
                        Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuTournamentListingSelected");

                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                    }

                    break;
            }
        }
    }
}
