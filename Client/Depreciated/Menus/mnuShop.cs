﻿// <copyright file="mnuShop.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using PMU.Core;
    using SdlDotNet.Widgets;

    /// <summary>
    /// Description of mnuShop.
    /// </summary>
    internal class MnuShop : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private bool loaded;
        private readonly int tempSelected;
        private readonly Label[] lblVisibleItems;
        private readonly Label[] lblVisiblePrices;
        private readonly Label lblItemCollection;
        private readonly Widges.MenuItemPicker itemPicker;
        private readonly PictureBox picPreview;
        public int CurrentTen;
        private readonly Label lblItemNum;

        public List<Shops.ShopItem> ShopItems
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuShop(string name, int itemSelected)
            : base(name)
            {
            this.Size = new Size(421, 360);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.CurrentTen = itemSelected / 10;

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 63);

            this.lblItemCollection = new Label("lblItemCollection");
            this.lblItemCollection.AutoSize = true;
            this.lblItemCollection.Font = FontManager.LoadFont("PMU", 48);
            this.lblItemCollection.Text = "Shop";
            this.lblItemCollection.Location = new Point(20, 0);
            this.lblItemCollection.ForeColor = Color.WhiteSmoke;

            this.picPreview = new PictureBox("picPreview");
            this.picPreview.Size = new Size(32, 32);
            this.picPreview.BackColor = Color.Transparent;
            this.picPreview.Location = new Point(361, 20);

            this.lblItemNum = new Label("lblItemNum");

            // lblItemNum.Size = new Size(100, 30);
            this.lblItemNum.AutoSize = true;
            this.lblItemNum.Location = new Point(288, 15);
            this.lblItemNum.Font = FontManager.LoadFont("PMU", 32);
            this.lblItemNum.BackColor = Color.Transparent;
            this.lblItemNum.Text = "0/0";
            this.lblItemNum.ForeColor = Color.WhiteSmoke;

            this.lblVisibleItems = new Label[10];
            this.lblVisiblePrices = new Label[10];
            for (int i = 0; i < this.lblVisibleItems.Length; i++)
            {
                this.lblVisibleItems[i] = new Label("lblVisibleItems" + i);

                // lblVisibleItems[i].AutoSize = true;
                this.lblVisibleItems[i].Size = new Size(200, 32);

                // lblVisibleItems[i].Width = 200;
                this.lblVisibleItems[i].Font = FontManager.LoadFont("PMU", 32);
                this.lblVisibleItems[i].Location = new Point(35, (i * 30) + 48);

                // lblVisibleItems[i].HoverColor = Color.Red;
                this.lblVisibleItems[i].ForeColor = Color.WhiteSmoke;
                this.lblVisibleItems[i].Click += new EventHandler<MouseButtonEventArgs>(this.ShopItem_Click);
                this.AddWidget(this.lblVisibleItems[i]);

                this.lblVisiblePrices[i] = new Label("lblVisiblePrices" + i);

                // lblVisiblePrices[i].AutoSize = true;
                this.lblVisiblePrices[i].Size = new Size(200, 32);

                // lblVisiblePrices[i].Width = 200;
                this.lblVisiblePrices[i].Font = FontManager.LoadFont("PMU", 32);
                this.lblVisiblePrices[i].Location = new Point(240, (i * 30) + 48);

                // lblVisiblePrices[i].HoverColor = Color.Red;
                this.lblVisiblePrices[i].ForeColor = Color.WhiteSmoke;
                this.lblVisiblePrices[i].Click += new EventHandler<MouseButtonEventArgs>(this.ShopPrice_Click);
                this.AddWidget(this.lblVisiblePrices[i]);
            }

            this.AddWidget(this.picPreview);
            this.AddWidget(this.lblItemCollection);
            this.AddWidget(this.lblItemNum);
            this.AddWidget(this.itemPicker);

            this.tempSelected = itemSelected % 10;

            // DisplayItems(currentTen * 10 + 1);
            // ChangeSelected((itemSelected - 1) % 10);
            // UpdateSelectedItemInfo();
            // loaded = true;
            this.lblVisibleItems[0].Text = "Loading...";
        }

        public void LoadShopItems(string[] parse)
        {
            this.ShopItems = new List<Shops.ShopItem>();

            if (parse[3].ToInt() <= 0)
            {
                this.lblVisibleItems[0].Text = "Nothing";
                return;
            }

            int amount = parse[3].ToInt();
            int n = 4;

            for (int i = 0; i < amount; i++)
            {
                Shops.ShopItem shopItem = new Shops.ShopItem();
                shopItem.GiveItem = parse[n].ToInt();
                shopItem.GiveValue = parse[n + 1].ToInt();
                shopItem.GetItem = parse[n + 2].ToInt();

                this.ShopItems.Add(shopItem);

                n += 3;
            }

            // int length = parse.Length/3 - 3;
            // for (int i = 0; i <= length; i++)
            // {
            //    Shops.ShopItem shopItem = new Shops.ShopItem();

            // shopItem.GiveItem = parse[i*3 + 3].ToInt();
            //    shopItem.GiveValue = parse[i*3 + 4].ToInt();
            //    shopItem.GetItem = parse[i*3 + 5].ToInt();

            // ShopItems.Add(shopItem);

            // }
            this.DisplayItems(this.CurrentTen * 10);
            this.ChangeSelected(this.tempSelected);
            this.UpdateSelectedItemInfo();
            this.loaded = true;
        }

        private void ShopPrice_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.loaded)
            {
                if (this.ShopItems[(this.CurrentTen * 10) + Array.IndexOf(this.lblVisiblePrices, sender)].GetItem > 0)
                {
                    this.ChangeSelected(Array.IndexOf(this.lblVisiblePrices, sender));

                    MnuShopItemSelected selectedMenu = (MnuShopItemSelected)Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuShopItemSelected");
                    if (selectedMenu != null)
                    {
                        Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);

                        // selectedMenu.ItemSlot = GetSelectedItemSlot();
                        // selectedMenu.ItemNum = BankItems[GetSelectedItemSlot()].Num;
                    }

                    Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuShopItemSelected("mnuShopItemSelected", this.ShopItems[this.GetSelectedItemSlot()].GetItem, this.GetSelectedItemSlot(), Enums.InvMenuType.Buy));
                    Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuShopItemSelected");

                    this.UpdateSelectedItemInfo();
                    Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                }
            }
        }

        private void ShopItem_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.loaded)
            {
                if (this.ShopItems[(this.CurrentTen * 10) + Array.IndexOf(this.lblVisibleItems, sender)].GetItem > 0)
                {
                    this.ChangeSelected(Array.IndexOf(this.lblVisibleItems, sender));

                    MnuShopItemSelected selectedMenu = (MnuShopItemSelected)Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuShopItemSelected");
                    if (selectedMenu != null)
                    {
                        Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);

                        // selectedMenu.ItemSlot = GetSelectedItemSlot();
                        // selectedMenu.ItemNum = BankItems[GetSelectedItemSlot()].Num;
                    }

                    Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuShopItemSelected("mnuShopItemSelected", this.ShopItems[this.GetSelectedItemSlot()].GetItem, this.GetSelectedItemSlot(), Enums.InvMenuType.Buy));
                    Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuShopItemSelected");

                    this.UpdateSelectedItemInfo();
                    Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                }
            }
        }

        private delegate void DisplayItemsDelegate(int startNum);

        public void DisplayItems(int startNum)
        {
            // if (!InvokeRequired) {
            //    Invoke(new DisplayItemsDelegate(DisplayItems), startNum);
            // } else {
                for (int i = 0; i < this.lblVisibleItems.Length; i++)
                {
                    // shop menu; lists items and their prices
                    if (startNum + i >= this.ShopItems.Count)
                    {
                    this.lblVisibleItems[i].Text = string.Empty;
                    this.lblVisiblePrices[i].Text = string.Empty;
                    }
                    else if (this.ShopItems[startNum + i].GetItem > 0)
                    {
                        string getItem = Items.ItemHelper.Items[this.ShopItems[startNum + i].GetItem].Name;

                        string giveItem = Items.ItemHelper.Items[this.ShopItems[startNum + i].GiveItem].Name + "x" + this.ShopItems[startNum + i].GiveValue.ToString();
                        if (!string.IsNullOrEmpty(getItem))
                        {
                        this.lblVisibleItems[i].Text = getItem;
                        this.lblVisiblePrices[i].Text = giveItem;
                        }
else
                        {
                        this.lblVisibleItems[i].Text = "----";
                        this.lblVisiblePrices[i].Text = string.Empty;
                        }
                    }
else
                    {
                    this.lblVisibleItems[i].Text = "----";
                    this.lblVisiblePrices[i].Text = string.Empty;
                    }
                }

            // }
                this.RequestRedraw();
        }

        public void UpdateVisibleItems()
        {// appears to be unused
            this.DisplayItems(this.CurrentTen * 10);
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 63 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        private int GetSelectedItemSlot()
        {
            return this.itemPicker.SelectedItem + (this.CurrentTen * 10);
        }

        private void UpdateSelectedItemInfo()
        {
            // withdraw; shows bank item
            if (this.ShopItems[this.GetSelectedItemSlot()].GetItem > 0)
            {
                this.picPreview.Visible = true;
                this.picPreview.Image = Tools.CropImage(GraphicsManager.Items, new Rectangle((Items.ItemHelper.Items[this.ShopItems[this.GetSelectedItemSlot()].GetItem].Pic - ((int)(Items.ItemHelper.Items[this.ShopItems[this.GetSelectedItemSlot()].GetItem].Pic / 6) * 6)) * Constants.TILEWIDTH, (int)(Items.ItemHelper.Items[this.ShopItems[this.GetSelectedItemSlot()].GetItem].Pic / 6) * Constants.TILEWIDTH, Constants.TILEWIDTH, Constants.TILEHEIGHT));
            }
else
            {
                this.picPreview.Visible = false;
            }

            this.lblItemNum.Text = (this.CurrentTen + 1) + "/" + (((this.ShopItems.Count - 1) / 10) + 1);
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            if (this.ShopItems != null && this.ShopItems.Count == 0 && e.Key == SdlDotNet.Input.Key.Backspace)
            {
                MenuSwitcher.OpenShopOptions();
                Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
            }

            if (this.loaded)
            {
                base.OnKeyboardDown(e);
                switch (e.Key)
                {
                    case SdlDotNet.Input.Key.DownArrow:
                    {
                            if (this.itemPicker.SelectedItem >= 9 || (this.CurrentTen * 10) + this.itemPicker.SelectedItem >= this.ShopItems.Count - 1)
                            {
                                this.ChangeSelected(0);

                                // DisplayItems(1);
                            }
else
                            {
                                this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                            }

                            this.UpdateSelectedItemInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.UpArrow:
                    {
                            if (this.itemPicker.SelectedItem <= 0)
                            {
                                this.ChangeSelected(9);
                            }
else
                            {
                                this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                            }

                            if ((this.CurrentTen * 10) + this.itemPicker.SelectedItem > this.ShopItems.Count)
                            {
                                this.ChangeSelected(this.ShopItems.Count - (this.CurrentTen * 10) - 1);
                            }

                            this.UpdateSelectedItemInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.LeftArrow:
                    {
                            // int itemSlot = (currentTen + 1) - 10;//System.Math.Max(1, GetSelectedItemSlot() - (11 - itemPicker.SelectedItem));
                            if (this.CurrentTen <= 0)
                            {
                                this.CurrentTen = (this.ShopItems.Count - 1) / 10;
                            }
else
                            {
                                this.CurrentTen--;
                            }

                            if ((this.CurrentTen * 10) + this.itemPicker.SelectedItem >= this.ShopItems.Count)
                            {
                                this.ChangeSelected(this.ShopItems.Count - (this.CurrentTen * 10) - 1);
                            }

                            this.DisplayItems(this.CurrentTen * 10);
                            this.UpdateSelectedItemInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.RightArrow:
                    {
                            // int itemSlot = currentTen + 1 + 10;
                            if (this.CurrentTen >= ((this.ShopItems.Count - 1) / 10))
                            {
                                this.CurrentTen = 0;
                            }
else
                            {
                                this.CurrentTen++;
                            }

                            if ((this.CurrentTen * 10) + this.itemPicker.SelectedItem >= this.ShopItems.Count)
                            {
                                this.ChangeSelected(this.ShopItems.Count - (this.CurrentTen * 10) - 1);
                            }

                            this.DisplayItems(this.CurrentTen * 10);
                            this.UpdateSelectedItemInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.Return:
                    {
                            if (this.ShopItems[this.GetSelectedItemSlot()].GetItem > 0)
                            {
                                Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuShopItemSelected("mnuShopItemSelected", this.ShopItems[this.GetSelectedItemSlot()].GetItem, this.GetSelectedItemSlot(), Enums.InvMenuType.Buy));
                                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuShopItemSelected");
                                Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                            }
                        }

                        break;
                    case SdlDotNet.Input.Key.Backspace:
                    {
                            MenuSwitcher.OpenShopOptions();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                        }

                        break;
                }
            }
        }
    }
}
