﻿// <copyright file="mnuGuildCreate.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class MnuGuildCreate : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblGuild;
        private readonly Label lblName;
        private readonly TextBox txtName;
        private readonly Label lblMembers;
        private readonly ScrollingListBox lbxMembers;
        private readonly Button btnOK;
        private readonly Button btnCancel;

        public MnuGuildCreate(string name, string[] parse)
            : base(name)
            {
            this.Size = new Size(280, 280);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(180, 80);

            this.lblGuild = new Label("lblGuild");
            this.lblGuild.Location = new Point(20, 0);
            this.lblGuild.AutoSize = true;
            this.lblGuild.Font = FontManager.LoadFont("PMU", 48);
            this.lblGuild.Text = "Register Guild";
            this.lblGuild.ForeColor = Color.WhiteSmoke;

            this.lblName = new Label("lblName");
            this.lblName.Location = new Point(10, 50);
            this.lblName.AutoSize = true;
            this.lblName.Font = FontManager.LoadFont("PMU", 16);
            this.lblName.Text = "Guild Name:";
            this.lblName.ForeColor = Color.WhiteSmoke;

            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(180, 24);
            this.txtName.Location = new Point(20, 70);
            this.txtName.Font = FontManager.LoadFont("PMU", 16);
            Skins.SkinManager.LoadTextBoxGui(this.txtName);

            this.lblMembers = new Label("lblMembers");
            this.lblMembers.Location = new Point(10, 100);
            this.lblMembers.AutoSize = true;
            this.lblMembers.Font = FontManager.LoadFont("PMU", 16);
            this.lblMembers.Text = "Party Members: (Will become co-leaders)";
            this.lblMembers.ForeColor = Color.WhiteSmoke;

            this.lbxMembers = new ScrollingListBox("lbxMembers");
            this.lbxMembers.Location = new Point(20, 120);
            this.lbxMembers.Size = new Size(240, 80);

            this.btnOK = new Button("btnOK");
            this.btnOK.Size = new Size(60, 32);
            this.btnOK.Location = new Point(60, 210);
            this.btnOK.Font = FontManager.LoadFont("PMU", 16);
            this.btnOK.Text = "OK";
            Skins.SkinManager.LoadButtonGui(this.btnOK);
            this.btnOK.Click += new EventHandler<MouseButtonEventArgs>(this.BtnOK_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Size = new Size(60, 32);
            this.btnCancel.Location = new Point(150, 210);
            this.btnCancel.Font = FontManager.LoadFont("PMU", 16);
            this.btnCancel.Text = "Cancel";
            Skins.SkinManager.LoadButtonGui(this.btnCancel);
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.AddWidget(this.lblGuild);
            this.AddWidget(this.lblName);
            this.AddWidget(this.txtName);
            this.AddWidget(this.lblMembers);
            this.AddWidget(this.lbxMembers);
            this.AddWidget(this.btnOK);
            this.AddWidget(this.btnCancel);

            this.LoadPartyFromPacket(parse);
        }

        private void BtnOK_Click(object sender, MouseButtonEventArgs e)
        {
            Messenger.MakeGuild(this.txtName.Text);
            MenuSwitcher.CloseAllMenus();
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            MenuSwitcher.CloseAllMenus();
        }

        private void LoadPartyFromPacket(string[] parse)
        {
            int count = parse[1].ToInt();

            for (int i = 0; i < count; i++)
            {
                ListBoxTextItem lbiName = new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), parse[i + 2]);
                this.lbxMembers.Items.Add(lbiName);
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }
    }
}
