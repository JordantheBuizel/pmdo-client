﻿// <copyright file="mnuMoveSelected.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuMoveSelected : Widges.BorderedPanel, Core.IMenu
    {
        private int moveSlot;
        private readonly Label lblUse;
        private readonly Label lblShiftUp;
        private readonly Label lblShiftDown;
        private readonly Label lblForget;
        private readonly Widges.MenuItemPicker itemPicker;
        private const int MAXITEMS = 3;

        public int MoveSlot
        {
            get { return this.moveSlot; }

            set
            {
                this.moveSlot = value;
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuMoveSelected(string name, int moveSlot)
            : base(name)
            {
            this.Size = new Size(165, 155);
            this.MenuDirection = Enums.MenuDirection.Horizontal;
            this.Location = new Point(300, 34);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 23);

            this.lblUse = new Label("lblUse");
            this.lblUse.Font = FontManager.LoadFont("PMU", 32);
            this.lblUse.AutoSize = true;
            this.lblUse.Text = "Use";
            this.lblUse.Location = new Point(30, 8);
            this.lblUse.HoverColor = Color.Red;
            this.lblUse.ForeColor = Color.WhiteSmoke;
            this.lblUse.Click += new EventHandler<MouseButtonEventArgs>(this.LblUse_Click);

            this.lblShiftUp = new Label("lblShiftUp");
            this.lblShiftUp.Font = FontManager.LoadFont("PMU", 32);
            this.lblShiftUp.AutoSize = true;
            this.lblShiftUp.Text = "Shift Up";
            this.lblShiftUp.Location = new Point(30, 38);
            this.lblShiftUp.HoverColor = Color.Red;
            this.lblShiftUp.ForeColor = Color.WhiteSmoke;
            this.lblShiftUp.Click += new EventHandler<MouseButtonEventArgs>(this.LblShiftUp_Click);

            this.lblShiftDown = new Label("lblShiftDown");
            this.lblShiftDown.Font = FontManager.LoadFont("PMU", 32);
            this.lblShiftDown.AutoSize = true;
            this.lblShiftDown.Text = "Shift Down";
            this.lblShiftDown.Location = new Point(30, 68);
            this.lblShiftDown.HoverColor = Color.Red;
            this.lblShiftDown.ForeColor = Color.WhiteSmoke;
            this.lblShiftDown.Click += new EventHandler<MouseButtonEventArgs>(this.LblShiftDown_Click);

            this.lblForget = new Label("lblForget");
            this.lblForget.Font = FontManager.LoadFont("PMU", 32);
            this.lblForget.AutoSize = true;
            this.lblForget.Text = "Forget";
            this.lblForget.Location = new Point(30, 98);
            this.lblForget.HoverColor = Color.Red;
            this.lblForget.ForeColor = Color.WhiteSmoke;
            this.lblForget.Click += new EventHandler<MouseButtonEventArgs>(this.LblForget_Click);

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblUse);
            this.AddWidget(this.lblShiftUp);
            this.AddWidget(this.lblShiftDown);
            this.AddWidget(this.lblForget);

            this.MoveSlot = moveSlot;
        }

        private void LblUse_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(0, this.moveSlot);
        }

        private void LblShiftUp_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1, this.moveSlot);
        }

        private void LblShiftDown_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(2, this.moveSlot);
        }

        private void LblForget_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(3, this.moveSlot);
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 23 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem, this.moveSlot);
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                        this.CloseMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum, int moveSlot)
        {
            switch (itemNum)
            {
                case 0:
                { // Use move
                        Players.PlayerManager.MyPlayer.UseMove(moveSlot);
                        this.CloseMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case 1:
                { // Shift Up
                        if (moveSlot > 0)
                        {
                            Players.PlayerManager.MyPlayer.ShiftMove(moveSlot, true);
                            this.CloseMenu();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep6.wav");
                        }
                    }

                    break;
                case 2:
                { // Shift Down
                        if (moveSlot < 3)
                        {
                            Players.PlayerManager.MyPlayer.ShiftMove(moveSlot, false);
                            this.CloseMenu();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep6.wav");
                        }
                    }

                    break;
                case 3:
                { // Forget move
                        Players.PlayerManager.MyPlayer.ForgetMove(moveSlot);
                        this.CloseMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
            }
        }

        private void CloseMenu()
        {
            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(this);
            Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuMoves");
        }

        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }
    }
}
