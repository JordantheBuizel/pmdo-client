﻿// <copyright file="mnuVisitHouse.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using SdlDotNet.Widgets;

    internal class MnuVisitHouse : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblHouseSelection;
        private readonly TextBox txtHouse;
        private readonly Button btnAccept;
        private readonly Button btnMyHouse;
        private readonly Button btnCancel;

        public MnuVisitHouse(string name)
            : base(name)
            {
            this.Size = new Size(300, 200);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = Graphic.DrawingSupport.GetCenter(Windows.WindowSwitcher.GameWindow.MapViewer.Size, this.Size);

            this.lblHouseSelection = new Label("lblHouseSelection");
            this.lblHouseSelection.Location = new Point(25, 15);
            this.lblHouseSelection.AutoSize = false;
            this.lblHouseSelection.Size = new Size(this.Width - (this.lblHouseSelection.X * 2), 40);
            this.lblHouseSelection.Text = "Enter the name of the player whose house you wish to visit.";
            this.lblHouseSelection.ForeColor = Color.WhiteSmoke;

            this.txtHouse = new TextBox("txtHouse");
            this.txtHouse.Location = new Point(this.lblHouseSelection.X, this.lblHouseSelection.Y + this.lblHouseSelection.Height + 10);
            this.txtHouse.Size = new Size(this.Width - (this.lblHouseSelection.X * 2), 16);
            Skins.SkinManager.LoadTextBoxGui(this.txtHouse);

            this.btnAccept = new Button("btnAccept");
            this.btnAccept.Location = new Point(this.lblHouseSelection.X, this.txtHouse.Y + this.txtHouse.Height + 10);
            this.btnAccept.Size = new Size(50, 30);
            this.btnAccept.Text = "Visit!";
            this.btnAccept.Font = FontManager.LoadFont("tahoma", 10);
            Skins.SkinManager.LoadButtonGui(this.btnAccept);
            this.btnAccept.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAccept_Click);

            this.btnMyHouse = new Button("btnMyHouse");
            this.btnMyHouse.Location = new Point(this.btnAccept.X + this.btnAccept.Width, this.txtHouse.Y + this.txtHouse.Height + 10);
            this.btnMyHouse.Size = new Size(80, 30);
            this.btnMyHouse.Text = "Visit My House";
            this.btnMyHouse.Font = FontManager.LoadFont("tahoma", 10);
            Skins.SkinManager.LoadButtonGui(this.btnMyHouse);
            this.btnMyHouse.Click += new EventHandler<MouseButtonEventArgs>(this.BtnMyHouse_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(this.btnMyHouse.X + this.btnMyHouse.Width, this.txtHouse.Y + this.txtHouse.Height + 10);
            this.btnCancel.Size = new Size(50, 30);
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Font = FontManager.LoadFont("tahoma", 10);
            Skins.SkinManager.LoadButtonGui(this.btnCancel);
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.AddWidget(this.lblHouseSelection);
            this.AddWidget(this.txtHouse);
            this.AddWidget(this.btnAccept);
            this.AddWidget(this.btnMyHouse);
            this.AddWidget(this.btnCancel);
        }

        private void BtnMyHouse_Click(object sender, MouseButtonEventArgs e)
        {
            Messenger.SendHouseVisitRequest(Players.PlayerManager.MyPlayer.Name);
            MenuSwitcher.CloseAllMenus();
            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
        }

        private void BtnAccept_Click(object sender, MouseButtonEventArgs e)
        {
            Messenger.SendHouseVisitRequest(this.txtHouse.Text);
            MenuSwitcher.CloseAllMenus();
            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            MenuSwitcher.CloseAllMenus();
            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }
    }
}
