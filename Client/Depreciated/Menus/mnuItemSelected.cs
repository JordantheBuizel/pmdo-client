﻿// <copyright file="mnuItemSelected.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class MnuItemSelected : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly bool useable;
        private int itemSlot;
        private readonly Label lblHold;
        private readonly Label lblUse;
        private readonly Label lblDrop;
        private readonly Label lblSummary;
        private readonly Label lblThrow;
        private readonly NumericUpDown nudAmount;
        private readonly Widges.MenuItemPicker itemPicker;
        private readonly int maxItems;

        public int ItemSlot
        {
            get { return this.itemSlot; }

            set
            {
                this.itemSlot = value;
                if (Players.PlayerManager.MyPlayer.GetActiveRecruit().HeldItemSlot == this.itemSlot)
                {
                    this.lblHold.Text = "Take";
                }
else
                {
                    this.lblHold.Text = "Give";
                }
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuItemSelected(string name, int itemSlot)
            : base(name)
            {
            if ((int)Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(itemSlot)].Type < 8 || (int)Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(itemSlot)].Type == 15)
            {
                // cannot use item
                this.Size = new Size(165, 165);
                this.maxItems = 3;
                this.useable = false;
            }
else
            {
                // can use item
                this.Size = new Size(165, 195);
                this.maxItems = 4;
                this.useable = true;
            }

            this.MenuDirection = Enums.MenuDirection.Horizontal;
            this.Location = new Point(335, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 23);

            int widgetY = 8;

            // add choices
            this.lblHold = new Label("lblHold");
            this.lblHold.Size = new Size(120, 32);
            this.lblHold.Font = FontManager.LoadFont("PMU", 32);

            // lblHold.AutoSize = true;
            this.lblHold.Text = "Hold";
            this.lblHold.Location = new Point(30, widgetY);
            this.lblHold.HoverColor = Color.Red;
            this.lblHold.Click += new EventHandler<MouseButtonEventArgs>(this.LblHold_Click);
            this.lblHold.ForeColor = Color.WhiteSmoke;

            this.AddWidget(this.lblHold);
            widgetY += 30;

            if (this.useable)
            {
                this.lblUse = new Label("lblUse");
                this.lblUse.Font = FontManager.LoadFont("PMU", 32);
                this.lblUse.AutoSize = true;
                this.lblUse.Text = "Use";
                this.lblUse.Location = new Point(30, widgetY);
                this.lblUse.HoverColor = Color.Red;
                this.lblUse.ForeColor = Color.WhiteSmoke;
                this.lblUse.Click += new EventHandler<MouseButtonEventArgs>(this.LblUse_Click);

                this.AddWidget(this.lblUse);
                widgetY += 30;
            }

            this.lblThrow = new Label("lblThrow");
            this.lblThrow.Size = new Size(120, 32);
            this.lblThrow.Location = new Point(30, widgetY);
            this.lblThrow.Font = FontManager.LoadFont("PMU", 32);
            this.lblThrow.Text = "Throw";
            this.lblThrow.HoverColor = Color.Red;
            this.lblThrow.ForeColor = Color.WhiteSmoke;
            this.lblThrow.Click += new EventHandler<MouseButtonEventArgs>(this.LblThrow_Click);

            widgetY += 30;

            this.lblSummary = new Label("lblSummary");
            this.lblSummary.Size = new Size(120, 32);
            this.lblSummary.Location = new Point(30, widgetY);
            this.lblSummary.Font = FontManager.LoadFont("PMU", 32);
            this.lblSummary.Text = "Summary";
            this.lblSummary.HoverColor = Color.Red;
            this.lblSummary.ForeColor = Color.WhiteSmoke;
            this.lblSummary.Click += new EventHandler<MouseButtonEventArgs>(this.LblSummary_Click);

            widgetY += 30;

            this.lblDrop = new Label("lblDrop");
            this.lblDrop.Font = FontManager.LoadFont("PMU", 32);
            this.lblDrop.Size = new Size(130, 32);
            this.lblDrop.AutoSize = false;

            // lblDrop.Text = "Drop";
            this.lblDrop.Location = new Point(30, widgetY);
            this.lblDrop.HoverColor = Color.Red;
            this.lblDrop.ForeColor = Color.WhiteSmoke;
            this.lblDrop.Click += new EventHandler<MouseButtonEventArgs>(this.LblDrop_Click);

            widgetY += 32;

            if (Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(itemSlot)].Type == Enums.ItemType.Currency || Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(itemSlot)].StackCap > 0)
            {
                this.lblDrop.Text = "Drop Amount:";
                this.nudAmount = new NumericUpDown("nudAmount");
                this.nudAmount.Size = new Size(120, 24);
                this.nudAmount.Location = new Point(32, widgetY);
                this.nudAmount.Maximum = Players.PlayerManager.MyPlayer.Inventory[itemSlot].Value;
                this.nudAmount.Minimum = 1;

                this.AddWidget(this.nudAmount);
            }
else
            {
                this.lblDrop.Text = "Drop";
            }

            this.AddWidget(this.lblDrop);
            this.AddWidget(this.lblSummary);
            this.AddWidget(this.lblThrow);

            this.AddWidget(this.itemPicker);

            this.ItemSlot = itemSlot;
        }

        private void LblSummary_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.useable)
            {
                this.SelectItem(3);
            }
else
            {
                this.SelectItem(2);
            }
        }

        private void LblThrow_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.useable)
            {
                this.SelectItem(2);
            }
else
            {
                this.SelectItem(1);
            }
        }

        private void LblDrop_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.useable)
            {
                this.SelectItem(4);
            }
else
            {
                this.SelectItem(3);
            }
        }

        private void LblUse_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1);
        }

        private void LblHold_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(0);
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 23 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == this.maxItems)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(this.maxItems);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem);
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                        this.CloseMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum)
        {
            if (!this.useable && itemNum != 0)
            {
                itemNum++;
            }

            switch (itemNum)
            {
                case 0:// Hold/remove Item
                    {
                        if (Players.PlayerManager.MyPlayer.GetActiveRecruit().HeldItemSlot == this.itemSlot)
                        {
                            Messenger.SendRemoveItem(this.itemSlot);
                        }
else
                        {
                            Messenger.SendHoldItem(this.itemSlot);
                        }
                    }

                    break;
                case 1:
                { // Use item
                        if (Players.PlayerManager.MyPlayer.GetInvItemNum(this.itemSlot) > 0)
                        {
                              Messenger.SendUseItem(this.itemSlot);
                            switch (Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(this.itemSlot)].Type)
                            {
                                case Enums.ItemType.Key:
                                    this.CloseMenu();
                                    break;
                            }
                        }
                    }

                    break;
                case 2:
                { // Throw
                        if (Players.PlayerManager.MyPlayer.GetInvItemNum(this.itemSlot) > 0)
                        {
                            Messenger.SendThrowItem(this.itemSlot);
                        }
                    }

                    break;
                case 3:
                { // View item summary
                        MenuSwitcher.ShowItemSummary(Players.PlayerManager.MyPlayer.GetInvItemNum(this.itemSlot), this.itemSlot, Enums.InvMenuType.Use);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case 4:
                { // Drop item
                        if (Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(this.itemSlot)].Type == Enums.ItemType.Currency || Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(this.itemSlot)].StackCap > 0)
                        {
                            Messenger.SendDropItem(this.itemSlot, this.nudAmount.Value);
                        }
else
                        {
                            Messenger.SendDropItem(this.itemSlot, 0);
                        }
                    }

                    break;
            }

            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(this);
            Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuInventory");
        }

        private void CloseMenu()
        {
            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(this);
            Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuInventory");
        }
    }
}
