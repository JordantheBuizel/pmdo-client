﻿// <copyright file="mnuTournamentListingSelected.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using Tournaments;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class MnuTournamentListingSelected : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblJoin;
        private readonly Label lblViewRules;
        private readonly Widges.MenuItemPicker itemPicker;
        private const int MAXITEMS = 1;
        private readonly TournamentListing selectedListing;
        private readonly Enums.TournamentListingMode mode;

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuTournamentListingSelected(string name, TournamentListing selectedListing, Enums.TournamentListingMode mode)
            : base(name)
            {
            this.selectedListing = selectedListing;
            this.mode = mode;

            this.Size = new Size(185, 125);

            this.MenuDirection = Enums.MenuDirection.Horizontal;
            this.Location = new Point(335, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 23);

            this.lblJoin = new Label("lblJoin");
            this.lblJoin.Font = FontManager.LoadFont("PMU", 32);
            this.lblJoin.AutoSize = true;
            if (mode == Enums.TournamentListingMode.Join)
            {
                this.lblJoin.Text = "Join";
            }
            else if (mode == Enums.TournamentListingMode.Spectate)
            {
                this.lblJoin.Text = "Spectate";
            }

            this.lblJoin.Location = new Point(30, 8);
            this.lblJoin.HoverColor = Color.Red;
            this.lblJoin.ForeColor = Color.WhiteSmoke;
            this.lblJoin.Click += new EventHandler<MouseButtonEventArgs>(this.LblJoin_Click);

            this.lblViewRules = new Label("lblViewRules");
            this.lblViewRules.Font = FontManager.LoadFont("PMU", 32);
            this.lblViewRules.AutoSize = true;
            this.lblViewRules.Text = "View Rules";
            this.lblViewRules.Location = new Point(30, 58);
            this.lblViewRules.HoverColor = Color.Red;
            this.lblViewRules.ForeColor = Color.WhiteSmoke;
            this.lblViewRules.Click += new EventHandler<MouseButtonEventArgs>(this.LblViewRules_Click);

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblJoin);
            this.AddWidget(this.lblViewRules);
        }

        private void LblViewRules_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1);
        }

        private void LblJoin_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(0);
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 23 + (50 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem);
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                        this.CloseMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        private void SelectItem(int selectedItem)
        {
            switch (selectedItem)
            {
                case 0:
                { // Join tournament / Spectate
                    if (this.mode == Enums.TournamentListingMode.Join)
                    {
                        Messenger.SendJoinTournament(this.selectedListing.TournamentID);
                    }
                    else if (this.mode == Enums.TournamentListingMode.Spectate)
                    {
                        Messenger.SendSpectateTournament(this.selectedListing.TournamentID);
                    }

                        MenuSwitcher.CloseAllMenus();
                    }

                    break;
                case 1:
                { // View tournament rules
                        Messenger.SendViewTournamentRules(this.selectedListing.TournamentID);
                        MenuSwitcher.CloseAllMenus();
                    }

                    break;
            }
        }

        private void CloseMenu()
        {
            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(this);
        }
    }
}
