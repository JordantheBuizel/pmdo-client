﻿// <copyright file="mnuBank.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class MnuBank : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private bool loaded;
        private readonly int tempSelected;
        private readonly Label[] lblVisibleItems;
        private readonly Label lblItemCollection;
        private readonly Widges.MenuItemPicker itemPicker;
        private readonly PictureBox picPreview;
        public int CurrentTen;
        private readonly Label lblItemNum;
        private readonly TextBox txtFind;
        private readonly Button btnFind;
        private readonly Button btnSort;

        public List<Players.InventoryItem> BankItems
        {
            get;
            set;
        }

        public List<int> SortedItems
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuBank(string name, int itemSelected)
            : base(name)
        {
            this.Size = new Size(315, 400);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 32);

            this.CurrentTen = itemSelected / 10;

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 63);

            this.lblItemCollection = new Label("lblItemCollection");
            this.lblItemCollection.AutoSize = true;
            this.lblItemCollection.Font = FontManager.LoadFont("PMU", 48);
            this.lblItemCollection.Text = "Storage";
            this.lblItemCollection.Location = new Point(20, 0);
            this.lblItemCollection.ForeColor = Color.WhiteSmoke;

            this.picPreview = new PictureBox("picPreview");
            this.picPreview.Size = new Size(32, 32);
            this.picPreview.BackColor = Color.Transparent;
            this.picPreview.Location = new Point(255, 20);

            this.lblItemNum = new Label("lblItemNum");

            // lblItemNum.Size = new Size(100, 30);
            this.lblItemNum.AutoSize = true;
            this.lblItemNum.Location = new Point(182, 15);
            this.lblItemNum.Font = FontManager.LoadFont("PMU", 32);
            this.lblItemNum.BackColor = Color.Transparent;
            this.lblItemNum.Text = "0/0";
            this.lblItemNum.ForeColor = Color.WhiteSmoke;

            this.txtFind = new TextBox("txtFind");
            this.txtFind.Size = new Size(130, 20);
            this.txtFind.Location = new Point(32, 52);
            this.txtFind.Font = FontManager.LoadFont("PMU", 16);
            Skins.SkinManager.LoadTextBoxGui(this.txtFind);

            this.btnFind = new Button("btnFind");
            this.btnFind.Size = new Size(40, 20);
            this.btnFind.Location = new Point(174, 52);
            this.btnFind.Font = FontManager.LoadFont("PMU", 16);
            this.btnFind.Text = "Find";
            Skins.SkinManager.LoadButtonGui(this.btnFind);
            this.btnFind.Click += new EventHandler<MouseButtonEventArgs>(this.BtnFind_Click);

            this.btnSort = new Button("btnSort");
            this.btnSort.Size = new Size(40, 20);
            this.btnSort.Location = new Point(217, 52);
            this.btnSort.Font = FontManager.LoadFont("PMU", 16);
            this.btnSort.Text = "Sort";
            Skins.SkinManager.LoadButtonGui(this.btnSort);
            this.btnSort.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSort_Click);

            this.lblVisibleItems = new Label[10];
            for (int i = 0; i < this.lblVisibleItems.Length; i++)
            {
                this.lblVisibleItems[i] = new Label("lblVisibleItems" + i);

                // lblVisibleItems[i].AutoSize = true;
                // lblVisibleItems[i].Size = new Size(200, 32);
                this.lblVisibleItems[i].Width = 200;
                this.lblVisibleItems[i].Font = FontManager.LoadFont("PMU", 32);
                this.lblVisibleItems[i].Location = new Point(35, (i * 30) + 72);

                // lblVisibleItems[i].HoverColor = Color.Red;
                this.lblVisibleItems[i].ForeColor = Color.WhiteSmoke;
                this.lblVisibleItems[i].Click += new EventHandler<MouseButtonEventArgs>(this.BankItem_Click);
                this.AddWidget(this.lblVisibleItems[i]);
            }

            this.AddWidget(this.picPreview);
            this.AddWidget(this.txtFind);
            this.AddWidget(this.btnFind);
            this.AddWidget(this.btnSort);
            this.AddWidget(this.lblItemCollection);
            this.AddWidget(this.lblItemNum);
            this.AddWidget(this.itemPicker);

            this.tempSelected = itemSelected % 10;

            // DisplayItems(currentTen * 10 + 1);
            // ChangeSelected((itemSelected - 1) % 10);
            // UpdateSelectedItemInfo();
            // loaded = true;
            this.lblVisibleItems[0].Text = "Loading...";
        }

        public void LoadBankItems(string[] parse)
        {
            this.BankItems = new List<Players.InventoryItem>();
            int maxBank = (parse.Length - 1) / 2;

            for (int i = 1; i <= maxBank; i++)
            {
                Players.InventoryItem invItem = new Players.InventoryItem();

                invItem.Num = parse[((i - 1) * 2) + 1].ToInt();
                invItem.Value = parse[((i - 1) * 2) + 2].ToInt();

                this.BankItems.Add(invItem);
            }

            this.DisplayItems(this.CurrentTen * 10);
            this.ChangeSelected(this.tempSelected);
            this.UpdateSelectedItemInfo();
            this.loaded = true;
        }

        private void BtnSort_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.SortedItems == null)
            {
                this.SortedItems = new List<int>();
                List<int> bands = new List<int>();
                List<int> others = new List<int>();
                List<int> berries = new List<int>();
                List<int> ethers = new List<int>();
                List<int> seeds = new List<int>();
                List<int> tMs = new List<int>();
                List<int> boxes = new List<int>();
                List<int> orbs = new List<int>();
                List<int> apples = new List<int>();

                for (int i = 0; i < this.BankItems.Count; i++)
                {
                    if (this.BankItems[i].Num > 0 && Items.ItemHelper.Items[this.BankItems[i].Num].Name.ToLower().Contains("band") || this.BankItems[i].Num > 0 && Items.ItemHelper.Items[this.BankItems[i].Num].Name.ToLower().Contains("scarf"))
                    {
                        bands.Add(i);
                    }
                    else if (this.BankItems[i].Num > 0 && Items.ItemHelper.Items[this.BankItems[i].Num].Name.ToLower().Contains("seed"))
                    {
                        seeds.Add(i);
                    }
                    else if (this.BankItems[i].Num > 0 && Items.ItemHelper.Items[this.BankItems[i].Num].Name.ToLower().Contains("box"))
                    {
                        boxes.Add(i);
                    }
                    else if (this.BankItems[i].Num > 0 && Items.ItemHelper.Items[this.BankItems[i].Num].Name.ToLower().Contains("berry") && !Items.ItemHelper.Items[this.BankItems[i].Num].Name.ToLower().Contains("juice"))
                    {
                        berries.Add(i);
                    }
                    else if (this.BankItems[i].Num > 0 && Items.ItemHelper.Items[this.BankItems[i].Num].Name.ToLower().Contains("ether"))
                    {
                        ethers.Add(i);
                    }
                    else if (this.BankItems[i].Num > 0 && Items.ItemHelper.Items[this.BankItems[i].Num].Name.ToLower().StartsWith("tm "))
                    {
                        tMs.Add(i);
                    }
                    else if (this.BankItems[i].Num > 0 && Items.ItemHelper.Items[this.BankItems[i].Num].Name.ToLower().Contains("orb"))
                    {
                        orbs.Add(i);
                    }
                    else if (this.BankItems[i].Num > 0 && Items.ItemHelper.Items[this.BankItems[i].Num].Name.ToLower().Contains("apple"))
                    {
                        apples.Add(i);
                    }
                    else
                    {
                        others.Add(i);
                    }
                }

                this.SortedItems.AddRange(bands);
                this.SortedItems.AddRange(apples);
                this.SortedItems.AddRange(berries);
                this.SortedItems.AddRange(ethers);
                this.SortedItems.AddRange(seeds);
                this.SortedItems.AddRange(tMs);
                this.SortedItems.AddRange(orbs);
                this.SortedItems.AddRange(boxes);
                this.SortedItems.AddRange(others);

                this.btnSort.Text = "Cancel";
            }
            else
            {
                this.SortedItems = null;
                this.btnSort.Text = "Sort";
            }

            this.CurrentTen = 0;
            this.DisplayItems(this.CurrentTen * 10);
            this.ChangeSelected(0);
            this.UpdateSelectedItemInfo();
        }

        private void BtnFind_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.SortedItems == null)
            {
                if (this.txtFind.Text.Trim() != string.Empty)
                {
                    this.SortedItems = new List<int>();

                    for (int i = 0; i < this.BankItems.Count; i++)
                    {
                        if (this.BankItems[i].Num > 0 && Items.ItemHelper.Items[this.BankItems[i].Num].Name.ToLower().Contains(this.txtFind.Text.ToLower()))
                        {
                            this.SortedItems.Add(i);
                        }
                    }

                    this.btnFind.Text = "Cancel";
                }
            }
else
            {
                this.SortedItems = null;
                this.btnFind.Text = "Find";
            }

            this.CurrentTen = 0;
            this.DisplayItems(this.CurrentTen * 10);
            this.ChangeSelected(0);
            this.UpdateSelectedItemInfo();
        }

        private void BankItem_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.loaded)
            {
                if (this.SortedItems == null)
                {
                    if (this.BankItems[(this.CurrentTen * 10) + Array.IndexOf(this.lblVisibleItems, sender)].Num > 0)
                    {
                        this.ChangeSelected(Array.IndexOf(this.lblVisibleItems, sender));

                        MnuBankItemSelected selectedMenu = (MnuBankItemSelected)Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuBankItemSelected");
                        if (selectedMenu != null)
                        {
                            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);

                            // selectedMenu.ItemSlot = GetSelectedItemSlot();
                            // selectedMenu.ItemNum = BankItems[GetSelectedItemSlot()].Num;
                        }

                        Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuBankItemSelected("mnuBankItemSelected", this.BankItems[this.GetSelectedItemSlot()].Num, this.BankItems[this.GetSelectedItemSlot()].Value, this.GetSelectedItemSlot(), Enums.InvMenuType.Take));
                        Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuBankItemSelected");

                        this.UpdateSelectedItemInfo();
                    }
                }
else
                {
                    if ((this.CurrentTen * 10) + Array.IndexOf(this.lblVisibleItems, sender) < this.SortedItems.Count && this.BankItems[this.SortedItems[(this.CurrentTen * 10) + Array.IndexOf(this.lblVisibleItems, sender)]].Num > 0)
                    {
                        this.ChangeSelected(Array.IndexOf(this.lblVisibleItems, sender));

                        MnuBankItemSelected selectedMenu = (MnuBankItemSelected)Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuBankItemSelected");
                        if (selectedMenu != null)
                        {
                            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);

                            // selectedMenu.ItemSlot = GetSelectedItemSlot();
                            // selectedMenu.ItemNum = BankItems[GetSelectedItemSlot()].Num;
                        }

                        Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuBankItemSelected("mnuBankItemSelected", this.BankItems[this.SortedItems[this.GetSelectedItemSlot()]].Num, this.BankItems[this.SortedItems[this.GetSelectedItemSlot()]].Value, this.SortedItems[this.GetSelectedItemSlot()], Enums.InvMenuType.Take));
                        Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuBankItemSelected");

                        this.UpdateSelectedItemInfo();
                    }
                }
            }
        }

        public void DisplayItems(int startNum)
        {
            for (int i = 0; i < this.lblVisibleItems.Length; i++)
            {
                // withdraw menu; lists bank items
                if (this.SortedItems == null)
                {
                    if (this.BankItems[startNum + i].Num > 0)
                    {
                        string itemName = Items.ItemHelper.Items[this.BankItems[startNum + i].Num].Name;
                        if (!string.IsNullOrEmpty(itemName))
                        {
                            int itemAmount = this.BankItems[startNum + i].Value;
                            if (itemAmount > 0)
                            {
                                itemName += " (" + itemAmount + ")";
                            }

                            this.lblVisibleItems[i].Text = itemName;
                        }
else
                        {
                            this.lblVisibleItems[i].Text = string.Empty;
                        }
                    }
else
                    {
                        this.lblVisibleItems[i].Text = "-----";
                    }
                }
else
                {
                    if (startNum + i < this.SortedItems.Count && this.BankItems[this.SortedItems[startNum + i]].Num > 0)
                    {
                        string itemName = Items.ItemHelper.Items[this.BankItems[this.SortedItems[startNum + i]].Num].Name;
                        if (!string.IsNullOrEmpty(itemName))
                        {
                            int itemAmount = this.BankItems[this.SortedItems[startNum + i]].Value;
                            if (itemAmount > 0)
                            {
                                itemName += " (" + itemAmount + ")";
                            }

                            this.lblVisibleItems[i].Text = itemName;
                        }
else
                        {
                            this.lblVisibleItems[i].Text = string.Empty;
                        }
                    }
else
                    {
                        this.lblVisibleItems[i].Text = "-----";
                    }
                }
            }
        }

        public void UpdateVisibleItems()
        {// appears to be unused
            this.DisplayItems(this.CurrentTen * 10);
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 89 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        private int GetSelectedItemSlot()
        {
            return this.itemPicker.SelectedItem + (this.CurrentTen * 10);
        }

        private void UpdateSelectedItemInfo()
        {
            // withdraw; shows bank item
            if (this.SortedItems == null)
            {
                if (this.BankItems[this.GetSelectedItemSlot()].Num > 0)
                {
                    this.picPreview.Visible = true;
                    this.picPreview.Image = Tools.CropImage(GraphicsManager.Items, new Rectangle((Items.ItemHelper.Items[this.BankItems[this.GetSelectedItemSlot()].Num].Pic - ((int)(Items.ItemHelper.Items[this.BankItems[this.GetSelectedItemSlot()].Num].Pic / 6) * 6)) * Constants.TILEWIDTH, (int)(Items.ItemHelper.Items[this.BankItems[this.GetSelectedItemSlot()].Num].Pic / 6) * Constants.TILEWIDTH, Constants.TILEWIDTH, Constants.TILEHEIGHT));
                }
else
                {
                    this.picPreview.Visible = false;
                }
            }
else
            {
                if (this.GetSelectedItemSlot() < this.SortedItems.Count && this.BankItems[this.SortedItems[this.GetSelectedItemSlot()]].Num > 0)
                {
                    this.picPreview.Visible = true;
                    this.picPreview.Image = Tools.CropImage(GraphicsManager.Items, new Rectangle((Items.ItemHelper.Items[this.BankItems[this.SortedItems[this.GetSelectedItemSlot()]].Num].Pic - ((int)(Items.ItemHelper.Items[this.BankItems[this.SortedItems[this.GetSelectedItemSlot()]].Num].Pic / 6) * 6)) * Constants.TILEWIDTH, (int)(Items.ItemHelper.Items[this.BankItems[this.SortedItems[this.GetSelectedItemSlot()]].Num].Pic / 6) * Constants.TILEWIDTH, Constants.TILEWIDTH, Constants.TILEHEIGHT));
                }
else
                {
                    this.picPreview.Visible = false;
                }
            }

            this.lblItemNum.Text = (this.CurrentTen + 1) + "/" + (((this.BankItems.Count - 1) / 10) + 1);
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            if (this.loaded)
            {
                base.OnKeyboardDown(e);
                switch (e.Key)
                {
                    case SdlDotNet.Input.Key.DownArrow:
                        {
                            if (this.itemPicker.SelectedItem >= 9)
                            {
                                this.ChangeSelected(0);

                                // DisplayItems(1);
                            }
                            else
                            {
                                this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                            }

                            this.UpdateSelectedItemInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.UpArrow:
                        {
                            if (this.itemPicker.SelectedItem <= 0)
                            {
                                this.ChangeSelected(9);
                            }
                            else
                            {
                                this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                            }

                            this.UpdateSelectedItemInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.LeftArrow:
                        {
                            // int itemSlot = (currentTen + 1) - 10;//System.Math.Max(1, GetSelectedItemSlot() - (11 - itemPicker.SelectedItem));
                            if (this.CurrentTen <= 0)
                            {
                                this.CurrentTen = (this.BankItems.Count - 1) / 10;
                            }
                            else
                            {
                                this.CurrentTen--;
                            }

                            this.DisplayItems(this.CurrentTen * 10);
                            this.UpdateSelectedItemInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.RightArrow:
                        {
                            // int itemSlot = currentTen + 1 + 10;
                            if (this.CurrentTen >= ((this.BankItems.Count - 1) / 10))
                            {
                                this.CurrentTen = 0;
                            }
                            else
                            {
                                this.CurrentTen++;
                            }

                            this.DisplayItems(this.CurrentTen * 10);
                            this.UpdateSelectedItemInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.Return:
                        {
                            if (this.SortedItems == null)
                            {
                                if (this.BankItems[this.GetSelectedItemSlot()].Num > 0)
                                {
                                    Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuBankItemSelected("mnuBankItemSelected", this.BankItems[this.GetSelectedItemSlot()].Num, this.BankItems[this.GetSelectedItemSlot()].Value, this.GetSelectedItemSlot(), Enums.InvMenuType.Take));
                                    Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuBankItemSelected");
                                    Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                                }
                            }
else
                            {
                                if (this.GetSelectedItemSlot() < this.SortedItems.Count && this.BankItems[this.SortedItems[this.GetSelectedItemSlot()]].Num > 0)
                                {
                                    Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuBankItemSelected("mnuBankItemSelected", this.BankItems[this.SortedItems[this.GetSelectedItemSlot()]].Num, this.BankItems[this.SortedItems[this.GetSelectedItemSlot()]].Value, this.SortedItems[this.GetSelectedItemSlot()], Enums.InvMenuType.Take));
                                    Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuBankItemSelected");
                                    Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                                }
                            }
                        }

                        break;
                    case SdlDotNet.Input.Key.Backspace:
                        {
                            if (!this.txtFind.Focused)
                            {
                                MenuSwitcher.OpenBankOptions();
                                Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                            }
                        }

                        break;
                }
            }
        }
    }
}
