﻿// <copyright file="mnuTournamentRulesViewer.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Tournaments;
    using SdlDotNet.Widgets;

    internal class MnuTournamentRulesViewer : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblViewRules;
        private TournamentRules rules;

        private readonly Label lblSleepClause;
        private readonly Label lblAccuracyClause;
        private readonly Label lblSpeciesClause;
        private readonly Label lblFreezeClause;
        private readonly Label lblOHKOClause;
        private readonly Label lblSelfKOClause;

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuTournamentRulesViewer(string name, TournamentRules rules)
            : base(name)
            {
            this.rules = rules;

            this.Size = new Size(315, 360);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.lblViewRules = new Label("lblViewRules");
            this.lblViewRules.AutoSize = true;
            this.lblViewRules.Font = FontManager.LoadFont("PMU", 48);
            this.lblViewRules.Text = "View Rules";
            this.lblViewRules.ForeColor = Color.WhiteSmoke;
            this.lblViewRules.Location = new Point(20, 0);

            this.lblSleepClause = new Label("lblSleepClause");
            this.lblSleepClause.Location = new Point(15, 48);
            this.lblSleepClause.Size = new Size(200, 32);
            this.lblSleepClause.BackColor = Color.Transparent;
            this.lblSleepClause.Font = FontManager.LoadFont("PMU", 32);
            this.lblSleepClause.ForeColor = Color.WhiteSmoke;
            this.lblSleepClause.Text = "Sleep Clause: " + (rules.SleepClause ? "Yes" : "No");

            this.lblAccuracyClause = new Label("lblAccuracyClause");
            this.lblAccuracyClause.Location = new Point(15, 80);
            this.lblAccuracyClause.Size = new Size(200, 32);
            this.lblAccuracyClause.BackColor = Color.Transparent;
            this.lblAccuracyClause.Font = FontManager.LoadFont("PMU", 32);
            this.lblAccuracyClause.ForeColor = Color.WhiteSmoke;
            this.lblAccuracyClause.Text = "Accuracy Clause: " + (rules.AccuracyClause ? "Yes" : "No");

            this.lblSpeciesClause = new Label("lblSpeciesClause");
            this.lblSpeciesClause.Location = new Point(15, 112);
            this.lblSpeciesClause.Size = new Size(200, 32);
            this.lblSpeciesClause.BackColor = Color.Transparent;
            this.lblSpeciesClause.Font = FontManager.LoadFont("PMU", 32);
            this.lblSpeciesClause.ForeColor = Color.WhiteSmoke;
            this.lblSpeciesClause.Text = "Species Clause: " + (rules.SpeciesClause ? "Yes" : "No");

            this.lblFreezeClause = new Label("lblFreezeClause");
            this.lblFreezeClause.Location = new Point(15, 144);
            this.lblFreezeClause.Size = new Size(200, 32);
            this.lblFreezeClause.BackColor = Color.Transparent;
            this.lblFreezeClause.Font = FontManager.LoadFont("PMU", 32);
            this.lblFreezeClause.ForeColor = Color.WhiteSmoke;
            this.lblFreezeClause.Text = "Freeze Clause: " + (rules.FreezeClause ? "Yes" : "No");

            this.lblOHKOClause = new Label("lblOHKOClause");
            this.lblOHKOClause.Location = new Point(15, 176);
            this.lblOHKOClause.Size = new Size(200, 32);
            this.lblOHKOClause.BackColor = Color.Transparent;
            this.lblOHKOClause.Font = FontManager.LoadFont("PMU", 32);
            this.lblOHKOClause.ForeColor = Color.WhiteSmoke;
            this.lblOHKOClause.Text = "OHKO Clause: " + (rules.OHKOClause ? "Yes" : "No");

            this.lblSelfKOClause = new Label("lblSelfKOClause");
            this.lblSelfKOClause.Location = new Point(15, 208);
            this.lblSelfKOClause.Size = new Size(200, 32);
            this.lblSelfKOClause.BackColor = Color.Transparent;
            this.lblSelfKOClause.Font = FontManager.LoadFont("PMU", 32);
            this.lblSelfKOClause.ForeColor = Color.WhiteSmoke;
            this.lblSelfKOClause.Text = "Self-KO Clause: " + (rules.SelfKOClause ? "Yes" : "No");

            this.AddWidget(this.lblSleepClause);
            this.AddWidget(this.lblAccuracyClause);
            this.AddWidget(this.lblSpeciesClause);
            this.AddWidget(this.lblFreezeClause);
            this.AddWidget(this.lblOHKOClause);
            this.AddWidget(this.lblSelfKOClause);
            this.AddWidget(this.lblViewRules);
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.Backspace:
                {
                    MenuSwitcher.CloseAllMenus();
                    }

                    break;
            }
        }
    }
}
