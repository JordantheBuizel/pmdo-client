﻿// <copyright file="mnuRecruitSummary.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Players;

    public partial class MnuRecruitSummary : Form
    {
        private readonly Recruit requestedRecruit;

        public MnuRecruitSummary(int teamSlot)
        {
            this.InitializeComponent();

            this.requestedRecruit = PlayerManager.MyPlayer.Team[teamSlot];

            this.lblExp.Text = "Exp: " + this.requestedRecruit.ExpPercent + "%";
            this.pbarExp.Value = this.requestedRecruit.ExpPercent;
        }
    }
}
