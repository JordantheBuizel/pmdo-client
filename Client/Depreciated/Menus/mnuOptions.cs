﻿// <copyright file="mnuOptions.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuOptions : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private const int MAXITEMS = 13;

        private readonly Widges.MenuItemPicker itemPicker;
        private readonly Label lblOptions;
        private readonly Label lblPlayerData;
        private readonly Label lblPlayerDataName;
        private readonly Label lblPlayerDataDamage;
        private readonly Label lblPlayerDataMiniHP;
        private readonly Label lblPlayerDataAutoSaveSpeed;
        private readonly Label lblNpcData;
        private readonly Label lblNpcDataName;
        private readonly Label lblNpcDataDamage;
        private readonly Label lblNpcDataMiniHP;
        private readonly Label lblSoundData;
        private readonly Label lblSoundDataMusic;
        private readonly Label lblSoundDataSound;
        private readonly Label lblChatData;
        private readonly Label lblChatDataSpeechBubbles;
        private readonly Label lblChatDataTimeStamps;
        private readonly Label lblChatDataAutoScroll;
        private readonly Label lblChatDataRoleplay;
        private Label lblMovement;
        private readonly Label lblSave;

        private bool rPEnabled = IO.Options.EnableRolePlay;
        private bool isUsingArrows = IO.Options.ArrowMoveKeys;
        private readonly bool[] tempOptions;
        private int tempAutoSaveSpeed;

        public MnuOptions(string name)
            : base(name)
            {
            this.Size = new Size(280, 424);
            this.MenuDirection = Enums.MenuDirection.Vertical;

            this.Location = new Point(10, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(30, 83);

            this.lblOptions = new Label("lblOptions");
            this.lblOptions.Location = new Point(20, 0);
            this.lblOptions.AutoSize = true;
            this.lblOptions.Font = FontManager.LoadFont("PMU", 48);
            this.lblOptions.Text = "Options";
            this.lblOptions.ForeColor = Color.WhiteSmoke;

            this.lblPlayerData = new Label("lblPlayerData");
            this.lblPlayerData.Location = new Point(30, 48);
            this.lblPlayerData.AutoSize = true;
            this.lblPlayerData.Font = FontManager.LoadFont("PMU", 32);
            this.lblPlayerData.Text = "Player Data";
            this.lblPlayerData.ForeColor = Color.WhiteSmoke;

            this.lblPlayerDataName = new Label("lblPlayerDataName");
            this.lblPlayerDataName.Location = new Point(40, 78);
            this.lblPlayerDataName.AutoSize = true;
            this.lblPlayerDataName.Font = FontManager.LoadFont("PMU", 16);
            this.lblPlayerDataName.Text = "Name: ";
            this.lblPlayerDataName.ForeColor = Color.WhiteSmoke;

            // lblPlayerDataName.HoverColor = Color.Red;
            this.lblPlayerDataDamage = new Label("lblPlayerDataDamage");
            this.lblPlayerDataDamage.Location = new Point(40, 92);
            this.lblPlayerDataDamage.AutoSize = true;
            this.lblPlayerDataDamage.Font = FontManager.LoadFont("PMU", 16);
            this.lblPlayerDataDamage.Text = "Damage: ";
            this.lblPlayerDataDamage.ForeColor = Color.WhiteSmoke;

            // lblPlayerDataDamage.HoverColor = Color.Red;
            this.lblPlayerDataMiniHP = new Label("lblPlayerDataMiniHP");
            this.lblPlayerDataMiniHP.Location = new Point(40, 106);
            this.lblPlayerDataMiniHP.AutoSize = true;
            this.lblPlayerDataMiniHP.Font = FontManager.LoadFont("PMU", 16);
            this.lblPlayerDataMiniHP.Text = "Mini-HP: ";
            this.lblPlayerDataMiniHP.ForeColor = Color.WhiteSmoke;

            // lblPlayerDataMiniHP.HoverColor = Color.Red;
            this.lblPlayerDataAutoSaveSpeed = new Label("lblPlayerDataAutoSaveSpeed");
            this.lblPlayerDataAutoSaveSpeed.Location = new Point(40, 120);
            this.lblPlayerDataAutoSaveSpeed.AutoSize = true;
            this.lblPlayerDataAutoSaveSpeed.Font = FontManager.LoadFont("PMU", 16);
            this.lblPlayerDataAutoSaveSpeed.Text = "Auto-Save Speed: ";
            this.lblPlayerDataAutoSaveSpeed.ForeColor = Color.WhiteSmoke;

            // lblPlayerDataAutoSaveSpeed.HoverColor = Color.Red;
            this.lblNpcData = new Label("lblNpcData");
            this.lblNpcData.Location = new Point(30, 130);
            this.lblNpcData.AutoSize = true;
            this.lblNpcData.Font = FontManager.LoadFont("PMU", 32);
            this.lblNpcData.Text = "NPC Data";
            this.lblNpcData.ForeColor = Color.WhiteSmoke;

            this.lblNpcDataName = new Label("lblNpcDataName");
            this.lblNpcDataName.Location = new Point(40, 160);
            this.lblNpcDataName.AutoSize = true;
            this.lblNpcDataName.Font = FontManager.LoadFont("PMU", 16);
            this.lblNpcDataName.Text = "Name: ";
            this.lblNpcDataName.ForeColor = Color.WhiteSmoke;

            // lblNpcDataName.HoverColor = Color.Red;
            this.lblNpcDataDamage = new Label("lblNpcDataDamage");
            this.lblNpcDataDamage.Location = new Point(40, 174);
            this.lblNpcDataDamage.AutoSize = true;
            this.lblNpcDataDamage.Font = FontManager.LoadFont("PMU", 16);
            this.lblNpcDataDamage.Text = "Damage: ";
            this.lblNpcDataDamage.ForeColor = Color.WhiteSmoke;

            // lblNpcDataDamage.HoverColor = Color.Red;
            this.lblNpcDataMiniHP = new Label("lblNpcDataMiniHP");
            this.lblNpcDataMiniHP.Location = new Point(40, 188);
            this.lblNpcDataMiniHP.AutoSize = true;
            this.lblNpcDataMiniHP.Font = FontManager.LoadFont("PMU", 16);
            this.lblNpcDataMiniHP.Text = "Mini-HP: ";
            this.lblNpcDataMiniHP.ForeColor = Color.WhiteSmoke;

            // lblNpcDataMiniHP.HoverColor = Color.Red;
            this.lblSoundData = new Label("lblSoundData");
            this.lblSoundData.Location = new Point(30, 198);
            this.lblSoundData.AutoSize = true;
            this.lblSoundData.Font = FontManager.LoadFont("PMU", 32);
            this.lblSoundData.Text = "Sound Data";
            this.lblSoundData.ForeColor = Color.WhiteSmoke;

            this.lblSoundDataMusic = new Label("lblSoundDataMusic");
            this.lblSoundDataMusic.Location = new Point(40, 228);
            this.lblSoundDataMusic.AutoSize = true;
            this.lblSoundDataMusic.Font = FontManager.LoadFont("PMU", 16);
            this.lblSoundDataMusic.Text = "Music: ";
            this.lblSoundDataMusic.ForeColor = Color.WhiteSmoke;

            // lblSoundDataMusic.HoverColor = Color.Red;
            this.lblSoundDataSound = new Label("lblSoundDataSound");
            this.lblSoundDataSound.Location = new Point(40, 242);
            this.lblSoundDataSound.AutoSize = true;
            this.lblSoundDataSound.Font = FontManager.LoadFont("PMU", 16);
            this.lblSoundDataSound.Text = "Sound: ";
            this.lblSoundDataSound.ForeColor = Color.WhiteSmoke;

            // lblSoundDataSound.HoverColor = Color.Red;
            this.lblChatData = new Label("lblChatData");
            this.lblChatData.Location = new Point(30, 252);
            this.lblChatData.AutoSize = true;
            this.lblChatData.Font = FontManager.LoadFont("PMU", 32);
            this.lblChatData.Text = "Chat Data";
            this.lblChatData.ForeColor = Color.WhiteSmoke;

            this.lblChatDataSpeechBubbles = new Label("lblChatDataSpeechBubbles");
            this.lblChatDataSpeechBubbles.Location = new Point(40, 282);
            this.lblChatDataSpeechBubbles.AutoSize = true;
            this.lblChatDataSpeechBubbles.Font = FontManager.LoadFont("PMU", 16);
            this.lblChatDataSpeechBubbles.Text = "Speech Bubbles: ";
            this.lblChatDataSpeechBubbles.ForeColor = Color.WhiteSmoke;

            // lblChatDataSpeechBubbles.HoverColor = Color.Red;
            this.lblChatDataTimeStamps = new Label("lblChatDataTimeStamps");
            this.lblChatDataTimeStamps.Location = new Point(40, 296);
            this.lblChatDataTimeStamps.AutoSize = true;
            this.lblChatDataTimeStamps.Font = FontManager.LoadFont("PMU", 16);
            this.lblChatDataTimeStamps.Text = "TimeStamps: ";
            this.lblChatDataTimeStamps.ForeColor = Color.WhiteSmoke;

            // lblChatDataTimeStamps.HoverColor = Color.Red;
            this.lblChatDataAutoScroll = new Label("lblChatDataAutoScroll");
            this.lblChatDataAutoScroll.Location = new Point(40, 310);
            this.lblChatDataAutoScroll.AutoSize = true;
            this.lblChatDataAutoScroll.Font = FontManager.LoadFont("PMU", 16);
            this.lblChatDataAutoScroll.Text = "Auto-Scroll: ";
            this.lblChatDataAutoScroll.ForeColor = Color.WhiteSmoke;

            // lblChatDataAutoScroll.HoverColor = Color.Red;
            this.lblChatDataRoleplay = new Label("lblChatDataRoleplay");
            this.lblChatDataRoleplay.Location = new Point(40, 324);
            this.lblChatDataRoleplay.AutoSize = true;
            this.lblChatDataRoleplay.Font = FontManager.LoadFont("PMU", 16);
            this.lblChatDataRoleplay.Text = "Roleplay Channel: ";
            this.lblChatDataRoleplay.ForeColor = Color.WhiteSmoke;
            this.lblChatDataRoleplay.Click += new EventHandler<MouseButtonEventArgs>(this.LblChatDataRoleplay_Click);

            /*lblMovement = new Label("lblMovement");
            lblMovement.Location = new Point(40, 338);
            lblMovement.AutoSize = true;
            lblMovement.Font = FontManager.LoadFont("PMU", 16);
            lblMovement.Text = "Movement Keys: ";
            lblMovement.ForeColor = Color.WhiteSmoke;
            lblChatDataRoleplay.Click += new EventHandler<MouseButtonEventArgs>(lblMovement_Click);*/

            this.lblSave = new Label("lblSave");
            this.lblSave.Location = new Point(30, 358);
            this.lblSave.AutoSize = true;
            this.lblSave.Font = FontManager.LoadFont("PMU", 16);
            this.lblSave.Text = "Save";
            this.lblSave.HoverColor = Color.Red;
            this.lblSave.ForeColor = Color.WhiteSmoke;
            this.lblSave.Click += new EventHandler<MouseButtonEventArgs>(this.LblSave_Click);

            this.AddWidget(this.itemPicker);

            this.AddWidget(this.lblOptions);

            this.AddWidget(this.lblPlayerData);
            this.AddWidget(this.lblPlayerDataName);
            this.AddWidget(this.lblPlayerDataDamage);
            this.AddWidget(this.lblPlayerDataMiniHP);
            this.AddWidget(this.lblPlayerDataAutoSaveSpeed);

            this.AddWidget(this.lblNpcData);
            this.AddWidget(this.lblNpcDataName);
            this.AddWidget(this.lblNpcDataDamage);
            this.AddWidget(this.lblNpcDataMiniHP);

            this.AddWidget(this.lblSoundData);
            this.AddWidget(this.lblSoundDataMusic);
            this.AddWidget(this.lblSoundDataSound);

            this.AddWidget(this.lblChatData);
            this.AddWidget(this.lblChatDataSpeechBubbles);
            this.AddWidget(this.lblChatDataTimeStamps);
            this.AddWidget(this.lblChatDataAutoScroll);
            this.AddWidget(this.lblChatDataRoleplay);

            // this.AddWidget(lblMovement);
            this.AddWidget(this.lblSave);

            this.tempOptions = new bool[12];
            this.tempAutoSaveSpeed = default(int);

            for (int i = 0; i < 12; i++)
            {
                this.CreateTempOption(i);
                this.ShowOption(i);
            }

            if (this.isUsingArrows == true)
            {
                this.lblMovement.Text = "Movement Keys: Arrows";
            }
            else
            {
                this.lblMovement.Text = "Movement Keys: WSAD";
            }

            this.lblChatDataRoleplay.Text = "Roleplay Channel: " + this.BoolToString(this.rPEnabled);
        }

        private void LblMovement_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.isUsingArrows == IO.Options.ArrowMoveKeys)
            {
                this.isUsingArrows = !IO.Options.ArrowMoveKeys;
            }
            else
            {
                this.isUsingArrows = IO.Options.ArrowMoveKeys;
            }

            if (this.isUsingArrows == true)
            {
                this.lblMovement.Text = "Movement Keys: Arrows";
            }
            else
            {
                this.lblMovement.Text = "Movement Keys: WSAD";
            }
        }

        private void LblChatDataRoleplay_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.rPEnabled == IO.Options.EnableRolePlay)
            {
                this.rPEnabled = !IO.Options.EnableRolePlay;
            }
            else
            {
                this.rPEnabled = IO.Options.EnableRolePlay;
            }

            this.lblChatDataRoleplay.Text = "Roleplay Channel: " + this.BoolToString(this.rPEnabled);
        }

        private void LblSave_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(12);
        }

        public void ChangeSelected(int itemNum)
        {
            int pointerX = 30;
            int pointerY = 83;

            if (itemNum > 3)
            {
                pointerY += 26;
                if (itemNum > 6)
                {
                    pointerY += 26;
                    if (itemNum > 8)
                    {
                        pointerY += 26;
                        if (itemNum > 12)
                        {
                            pointerX -= 10;
                            pointerY += 6;
                        }
                    }
                }
            }

            this.itemPicker.Location = new Point(pointerX, pointerY + (14 * itemNum));

            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.LeftArrow:
                {
                        if (this.itemPicker.SelectedItem == 3)
                        {
                            if (this.tempAutoSaveSpeed > 0)
                            {
                                this.tempAutoSaveSpeed--;
                                this.ShowOption(3);
                            }
                        }
                        else if (this.itemPicker.SelectedItem != 12)
                        {
                            this.SelectItem(this.itemPicker.SelectedItem);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.RightArrow:
                {
                        if (this.itemPicker.SelectedItem == 3)
                        {
                            if (this.tempAutoSaveSpeed < 10)
                            {
                                this.tempAutoSaveSpeed++;
                                this.ShowOption(3);
                            }
                        }
                        else if (this.itemPicker.SelectedItem != 12)
                        {
                            this.SelectItem(this.itemPicker.SelectedItem);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                        // goes to the main menu; should it?
                        MenuSwitcher.ShowOthersMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum)
        {
            if (itemNum == 3)
            {
                if (this.tempAutoSaveSpeed < 10)
                {
                    this.tempAutoSaveSpeed++;
                    this.ShowOption(3);
                }
            }
            else if (itemNum < 12)
            {
                this.tempOptions[itemNum] = !this.tempOptions[itemNum];
                this.ShowOption(itemNum);
            }
else
            {
                // Save method goes here
                IO.Options.PlayerName = this.tempOptions[0];
                IO.Options.PlayerDamage = this.tempOptions[1];
                IO.Options.PlayerBar = this.tempOptions[2];

                IO.Options.AutoSaveSpeed = this.tempAutoSaveSpeed;

                IO.Options.NpcName = this.tempOptions[4];
                IO.Options.NpcDamage = this.tempOptions[5];
                IO.Options.NpcBar = this.tempOptions[6];
                IO.Options.Music = this.tempOptions[7];
                IO.Options.Sound = this.tempOptions[8];
                IO.Options.SpeechBubbles = this.tempOptions[9];
                IO.Options.Timestamps = this.tempOptions[10];
                IO.Options.AutoScroll = this.tempOptions[11];
                IO.Options.EnableRolePlay = this.rPEnabled;
                IO.Options.ArrowMoveKeys = this.isUsingArrows;

                IO.Options.SaveXml();
                IO.Options.UpdateActiveOptions();

                MenuSwitcher.ShowMainMenu();
            }
        }

        public void CreateTempOption(int itemNum)
        {
            switch (itemNum)
            {
                case 0:
                {
                        this.tempOptions[itemNum] = IO.Options.PlayerName;
                    }

                    break;
                case 1:
                {
                        this.tempOptions[itemNum] = IO.Options.PlayerDamage;
                    }

                    break;
                case 2:
                {
                        this.tempOptions[itemNum] = IO.Options.PlayerBar;
                    }

                    break;
                case 3:
                {
                        this.tempAutoSaveSpeed = IO.Options.AutoSaveSpeed;
                    }

                    break;
                case 4:
                {
                        this.tempOptions[itemNum] = IO.Options.NpcName;
                    }

                    break;
                case 5:
                {
                        this.tempOptions[itemNum] = IO.Options.NpcDamage;
                    }

                    break;
                case 6:
                {
                        this.tempOptions[itemNum] = IO.Options.NpcBar;
                    }

                    break;
                case 7:
                {
                        this.tempOptions[itemNum] = IO.Options.Music;
                    }

                    break;
                case 8:
                {
                        this.tempOptions[itemNum] = IO.Options.Sound;
                    }

                    break;
                case 9:
                {
                        this.tempOptions[itemNum] = IO.Options.SpeechBubbles;
                    }

                    break;
                case 10:
                {
                        this.tempOptions[itemNum] = IO.Options.Timestamps;
                    }

                    break;
                case 11:
                {
                        this.tempOptions[itemNum] = IO.Options.AutoScroll;
                    }

                    break;
            }
        }

        public void ShowOption(int itemNum)
        {
            switch (itemNum)
            {
                case 0:
                {
                        this.lblPlayerDataName.Text = "Name: " + this.BoolToString(this.tempOptions[itemNum]);
                    }

                    break;
                case 1:
                {
                        this.lblPlayerDataDamage.Text = "Damage: " + this.BoolToString(this.tempOptions[itemNum]);
                    }

                    break;
                case 2:
                {
                        this.lblPlayerDataMiniHP.Text = "Mini-HP Bar: " + this.BoolToString(this.tempOptions[itemNum]);
                    }

                    break;
                case 3:
                {
                        this.lblPlayerDataAutoSaveSpeed.Text = "Auto-Save Speed: " + this.tempAutoSaveSpeed;
                    }

                    break;
                case 4:
                {
                        this.lblNpcDataName.Text = "Name: " + this.BoolToString(this.tempOptions[itemNum]);
                    }

                    break;
                case 5:
                {
                        this.lblNpcDataDamage.Text = "Damage: " + this.BoolToString(this.tempOptions[itemNum]);
                    }

                    break;
                case 6:
                {
                        this.lblNpcDataMiniHP.Text = "Mini-HP Bar: " + this.BoolToString(this.tempOptions[itemNum]);
                    }

                    break;
                case 7:
                {
                        this.lblSoundDataMusic.Text = "Music: " + this.BoolToString(this.tempOptions[itemNum]);
                    }

                    break;
                case 8:
                {
                        this.lblSoundDataSound.Text = "Sound Effects: " + this.BoolToString(this.tempOptions[itemNum]);
                    }

                    break;
                case 9:
                {
                        this.lblChatDataSpeechBubbles.Text = "Speech Bubbles: " + this.BoolToString(this.tempOptions[itemNum]);
                    }

                    break;
                case 10:
                {
                        this.lblChatDataTimeStamps.Text = "TimeStamps: " + this.BoolToString(this.tempOptions[itemNum]);
                    }

                    break;
                case 11:
                {
                        this.lblChatDataAutoScroll.Text = "Auto-Scroll: " + this.BoolToString(this.tempOptions[itemNum]);
                    }

                    break;
            }
        }

        public string BoolToString(bool setting)
        {
            if (setting == true)
            {
                return "On";
            }

            return "Off";
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }
    }
}
