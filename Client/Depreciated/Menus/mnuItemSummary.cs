﻿// <copyright file="mnuItemSummary.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuItemSummary : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Enums.InvMenuType originalMenu;
        private readonly int itemSlot;
        private readonly PictureBox picPreview;
        private readonly Label lblItem;
        private readonly Label lblRarity;
        private readonly Label lblPrice;
        private readonly Label lblDroppable;
        private readonly Label lblLoseable;
        private readonly Label lblDescription;

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuItemSummary(string name, int itemNum, int itemSlot, Enums.InvMenuType originalMenu)
            : base(name)
        {
            this.Size = new Size(380, 288);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.originalMenu = originalMenu;
            this.itemSlot = itemSlot;

            this.picPreview = new PictureBox("picPreview");
            this.picPreview.Size = new Size(32, 32);
            this.picPreview.BackColor = Color.Transparent;
            this.picPreview.Location = new Point(10, 10);
            this.picPreview.Image = Tools.CropImage(GraphicsManager.Items, new Rectangle((Items.ItemHelper.Items[itemNum].Pic - ((int)(Items.ItemHelper.Items[itemNum].Pic / 6) * 6)) * Constants.TILEWIDTH, (int)(Items.ItemHelper.Items[itemNum].Pic / 6) * Constants.TILEWIDTH, Constants.TILEWIDTH, Constants.TILEHEIGHT));

            this.lblItem = new Label("lblItem");
            this.lblItem.Location = new Point(46, 10);
            this.lblItem.AutoSize = true;
            this.lblItem.Font = FontManager.LoadFont("PMU", 32);
            this.lblItem.Text = Items.ItemHelper.Items[itemNum].Name;
            this.lblItem.ForeColor = Color.WhiteSmoke;

            this.lblRarity = new Label("lblRarity");
            this.lblRarity.Location = new Point(20, 42);
            this.lblRarity.AutoSize = true;
            this.lblRarity.Font = FontManager.LoadFont("PMU", 16);
            this.lblRarity.Text = "Rarity: " + Items.ItemHelper.Items[itemNum].Rarity;
            this.lblRarity.ForeColor = Color.WhiteSmoke;

            int y = 42;
            y += 20;
            this.lblPrice = new Label("lblPrice");
            this.lblPrice.Location = new Point(20, y);
            this.lblPrice.AutoSize = true;
            this.lblPrice.Font = FontManager.LoadFont("PMU", 16);
            if (Items.ItemHelper.Items[itemNum].Price > 0)
            {
                this.lblPrice.Text = "Sell Price: " + Items.ItemHelper.Items[itemNum].Price;
            }
else
            {
                this.lblPrice.Text = "Cannot be sold.";
            }

            this.lblPrice.ForeColor = Color.WhiteSmoke;

            if (Items.ItemHelper.Items[itemNum].Bound)
            {
                y += 20;
                this.lblDroppable = new Label("lblDroppable");
                this.lblDroppable.Location = new Point(20, y);
                this.lblDroppable.AutoSize = true;
                this.lblDroppable.Font = FontManager.LoadFont("PMU", 16);
                this.lblDroppable.Text = "Cannot be dropped.";
                this.lblDroppable.ForeColor = Color.WhiteSmoke;
            }

            if (Items.ItemHelper.Items[itemNum].Bound)
            {
                y += 20;
                this.lblLoseable = new Label("lblLoseable");
                this.lblLoseable.Location = new Point(20, y);
                this.lblLoseable.AutoSize = true;
                this.lblLoseable.Font = FontManager.LoadFont("PMU", 16);
                this.lblLoseable.Text = "Cannot be lost.";
                this.lblLoseable.ForeColor = Color.WhiteSmoke;
            }

            y += 30;
            this.lblDescription = new Label("lblDescription");
            this.lblDescription.Location = new Point(20, y);

            // lblDescription.AutoSize = true;
            this.lblDescription.Size = new Size(300, 220);
            this.lblDescription.Font = FontManager.LoadFont("PMU", 16);
            this.lblDescription.Text = Items.ItemHelper.Items[itemNum].Desc;
            this.lblDescription.ForeColor = Color.WhiteSmoke;

            this.AddWidget(this.picPreview);
            this.AddWidget(this.lblItem);
            this.AddWidget(this.lblRarity);
            this.AddWidget(this.lblPrice);
            this.AddWidget(this.lblDroppable);
            this.AddWidget(this.lblLoseable);
            this.AddWidget(this.lblDescription);
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.Return:
                    {
                        this.MenuBack();
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                    {
                        // Show the main menu when the backspace key is pressed
                        this.MenuBack();
                    }

                    break;
            }
        }

        private void MenuBack()
        {
            switch (this.originalMenu)
            {
                    case Enums.InvMenuType.Use:
                    {
                            MenuSwitcher.ShowInventoryMenu(this.itemSlot);
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                        break;
                        case Enums.InvMenuType.Store:
                        {
                            MenuSwitcher.ShowBankDepositMenu(this.itemSlot);
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                        }

                        break;
                        case Enums.InvMenuType.Take:
                        {
                            MenuSwitcher.ShowBankWithdrawMenu(this.itemSlot);
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                        }

                        break;
                        case Enums.InvMenuType.Sell:
                        {
                            MenuSwitcher.ShowShopSellMenu(this.itemSlot);
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                        }

                        break;
                        case Enums.InvMenuType.Buy:
                        {
                            MenuSwitcher.ShowShopBuyMenu(this.itemSlot);
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                        }

                        break;
                        case Enums.InvMenuType.Recycle:
                        {
                        }

                        break;
            }
        }
    }
}
