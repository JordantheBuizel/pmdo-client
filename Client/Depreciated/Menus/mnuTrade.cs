﻿// <copyright file="mnuTrade.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuTrade : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly ScrollingListBox lstInv;
        private readonly Button btnItemSet;
        private readonly Label lblAmount;
        private readonly NumericUpDown nudAmount;
        private readonly Label lblClient;
        private readonly Label lblClientItem;
        private readonly Label lblMyItem;
        private readonly Button btnConfirm;
        private readonly Button btnCancel;
        private readonly string tradePartner;

        private bool itemSet;
        private bool partnerItemSet;

        public MnuTrade(string name, string tradePartner)
            : base(name)
            {
            this.tradePartner = tradePartner;

            this.Size = new Size(330, 350);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = Graphic.DrawingSupport.GetCenter(Windows.WindowSwitcher.GameWindow.MapViewer.Size, this.Size);

            this.lstInv = new ScrollingListBox("lstInv");
            this.lstInv.Location = new Point(40, 20);
            this.lstInv.Size = new Size(250, 170);
            this.lstInv.ItemSelected += new EventHandler(this.LstInv_ItemSelected);

            this.lblAmount = new Label("lblAmount");
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new Point(40, 194);
            this.lblAmount.Font = FontManager.LoadFont("PMU", 18);
            this.lblAmount.ForeColor = Color.WhiteSmoke;
            this.lblAmount.Text = "Amount:";
            this.lblAmount.Visible = false;

            this.nudAmount = new NumericUpDown("nudAmount");
            this.nudAmount.Size = new Size(120, 24);
            this.nudAmount.Location = new Point(120, 194);
            this.nudAmount.Maximum = int.MaxValue;
            this.nudAmount.Minimum = 1;
            this.nudAmount.Visible = false;

            this.lblClient = new Label("lblClient");
            this.lblClient.AutoSize = true;
            this.lblClient.Location = new Point(40, 220);
            this.lblClient.Font = FontManager.LoadFont("PMU", 18);
            this.lblClient.ForeColor = Color.WhiteSmoke;
            this.lblClient.Text = "Trading with: " + this.tradePartner;

            this.lblClientItem = new Label("lblClientItem");
            this.lblClientItem.AutoSize = true;
            this.lblClientItem.Location = new Point(40, 240);
            this.lblClientItem.Font = FontManager.LoadFont("PMU", 18);
            this.lblClientItem.ForeColor = Color.WhiteSmoke;
            this.lblClientItem.Text = "Trader's Item:";

            this.lblMyItem = new Label("lblMyItem");
            this.lblMyItem.AutoSize = true;
            this.lblMyItem.Location = new Point(40, 260);
            this.lblMyItem.Font = FontManager.LoadFont("PMU", 18);
            this.lblMyItem.ForeColor = Color.WhiteSmoke;
            this.lblMyItem.Text = "My Item:";

            this.btnConfirm = new Button("btnConfirm");
            this.btnConfirm.Size = new Size(82, 30);
            this.btnConfirm.Location = new Point(208, 300);
            this.btnConfirm.Font = FontManager.LoadFont("PMU", 18);
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.Click += new EventHandler<MouseButtonEventArgs>(this.BtnConfirm_Click);

            this.btnItemSet = new Button("btnItemSet");
            this.btnItemSet.Size = new Size(82, 30);
            this.btnItemSet.Location = new Point(126, 300);
            this.btnItemSet.Font = FontManager.LoadFont("PMU", 18);
            this.btnItemSet.Text = "Set Item";
            this.btnItemSet.Click += new EventHandler<MouseButtonEventArgs>(this.BtnItemSet_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Size = new Size(82, 30);
            this.btnCancel.Location = new Point(44, 300);
            this.btnCancel.Font = FontManager.LoadFont("PMU", 18);
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.AddWidget(this.lstInv);
            this.AddWidget(this.lblAmount);
            this.AddWidget(this.nudAmount);
            this.AddWidget(this.btnItemSet);
            this.AddWidget(this.lblClient);
            this.AddWidget(this.lblClientItem);
            this.AddWidget(this.btnConfirm);
            this.AddWidget(this.btnCancel);
            this.AddWidget(this.lblMyItem);

            this.LoadInventory();
        }

        private void LoadInventory()
        {
            SdlDotNet.Graphics.Font font = FontManager.LoadFont("tahoma", 10);
            this.lstInv.Items.Clear();
            for (int i = 1; i < Players.PlayerManager.MyPlayer.Inventory.Length; i++)
            {
                if (Players.PlayerManager.MyPlayer.Inventory[i].Num > 0)
                {
                    string itemName = Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.Inventory[i].Num].Name;
                    if (Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.Inventory[i].Num].Type == Enums.ItemType.Currency || Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.Inventory[i].Num].StackCap > 0)
                    {
                        itemName += " (" + Players.PlayerManager.MyPlayer.Inventory[i].Value + ")";
                    }

                    if (!string.IsNullOrEmpty(itemName))
                    {
                        if (this.lstInv.Items.Count <= i - 1)
                        {
                            ListBoxTextItem item = new ListBoxTextItem(font, itemName);
                            item.Tag = i;
                            this.lstInv.Items.Add(item);
                        }
else
                        {
                            ((ListBoxTextItem)this.lstInv.Items[i - 1]).Text = itemName;
                        }
                    }
                }
            }
        }

        private void LstInv_ItemSelected(object sender, EventArgs e)
        {
            int itemNum = Players.PlayerManager.MyPlayer.GetInvItemNum((int)this.lstInv.SelectedItems[0].Tag);
            if (Items.ItemHelper.Items[itemNum].Type == Enums.ItemType.Currency || Items.ItemHelper.Items[itemNum].StackCap > 0)
            {
                this.lblAmount.Visible = true;
                this.nudAmount.Visible = true;
                this.nudAmount.Value = 1;
            }
else
            {
                this.lblAmount.Visible = false;
                this.nudAmount.Visible = false;
            }
        }

        private void BtnItemSet_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.itemSet == false)
            {
                if (this.lstInv.SelectedItems.Count == 1)
                {
                    Network.Messenger.SendPacket(PMU.Sockets.TcpPacket.CreatePacket("settradeitem", (int)this.lstInv.SelectedItems[0].Tag, this.nudAmount.Value));

                    // lblMyItem.Text = "My Item: " + Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.Inventory[(int)lstInv.SelectedItems[0].Tag].Num].Name;
                    this.itemSet = true;

                    // btnItemSet.Text = "Unset Item";
                    Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                }
else
                {
                    // TODO: Allow Empty Trading
                }
            }
else
            {
                Network.Messenger.SendPacket(PMU.Sockets.TcpPacket.CreatePacket("settradeitem", "-1", "0"));

                // lblMyItem.Text = "My Item: No item offered yet";
                // itemSet = false;
                // btnItemSet.Text = "Set Item";
                Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
            }
        }

        private void BtnConfirm_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.itemSet && this.partnerItemSet)
            {
                Network.Messenger.SendPacket(PMU.Sockets.TcpPacket.CreatePacket("readytotrade"));
                this.btnConfirm.Selected = true;
                Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
            }
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            Network.Messenger.SendPacket(PMU.Sockets.TcpPacket.CreatePacket("endplayertrade"));
            MenuSwitcher.CloseAllMenus();
            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
        }

        public void UpdateSetItem(int itemNum, int amount)
        {
            if (itemNum > -1)
            {
                this.lblMyItem.Text = "My Item: " + Items.ItemHelper.Items[itemNum].Name;
                    if (Items.ItemHelper.Items[itemNum].Type == Enums.ItemType.Currency || Items.ItemHelper.Items[itemNum].StackCap > 0)
                    {
                    this.lblMyItem.Text += " (" + amount + ")";
                    }

                this.itemSet = true;
                this.btnItemSet.Text = "Unset Item";
            }
else
            {
                this.lblMyItem.Text = "My Item:";
                this.itemSet = false;
                this.btnItemSet.Text = "Set Item";
            }
        }

        public void UpdatePartnersSetItem(int itemNum, int amount)
        {
            if (itemNum > -1)
            {
                this.lblClientItem.Text = "Trader's Item: " + Items.ItemHelper.Items[itemNum].Name;
                if (Items.ItemHelper.Items[itemNum].Type == Enums.ItemType.Currency || Items.ItemHelper.Items[itemNum].StackCap > 0)
                {
                    this.lblClientItem.Text += " (" + amount + ")";
                }

                this.partnerItemSet = true;
            }
else
            {
                this.lblClientItem.Text = "Trader's Item:";
                this.partnerItemSet = false;
            }
        }

        public void UnconfirmTrade()
        {
            this.btnConfirm.Selected = false;
        }

        public void ResetTradeData()
        {
            this.lblMyItem.Text = "My Item:";
            this.itemSet = false;
            this.lblClientItem.Text = "Trader's Item:";
            this.partnerItemSet = false;
            this.btnItemSet.Text = "Set Item";
            this.UnconfirmTrade();
            if (this.lstInv.SelectedItems.Count == 1)
            {
                this.lstInv.SelectedItems.Remove(this.lstInv.SelectedItems[0]);
            }

            this.LoadInventory();
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }
    }
}
