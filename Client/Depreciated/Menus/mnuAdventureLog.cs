﻿// <copyright file="mnuAdventureLog.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class MnuAdventureLog : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Panel pnlGeneral;
        private readonly Panel pnlDungeons;
        private readonly Panel pnlPokedex;
        private readonly Label lblAdventureLog;
        private readonly Label lblPage;
        private int page;
        private bool loaded;
        private readonly Label lblGeneral;
        private readonly Label lblPlayerName;
        private readonly Label lblRank;
        private readonly Label lblPlayTime;
        private readonly Label lblDungeonsCompleted;
        private readonly Label lblMissionsCompleted;

        // Label lblRescuesCompleted;
        private readonly Label lblDungeons;
        private readonly Label lblEntered;
        private readonly Label lblCompleted;
        private readonly ScrollingListBox lbxDungeonList;
        private readonly Label lblPokedex;
        private readonly Label lblDefeated;
        private readonly Label lblJoined;
        private readonly ScrollingListBox lbxPokedex;

        public MnuAdventureLog(string name)
            : base(name)
        {
            this.Size = new Size(200, 260);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.pnlGeneral = new Panel("pnlGeneral");
            this.pnlGeneral.Size = new Size(200, 220);
            this.pnlGeneral.Location = new Point(0, 40);
            this.pnlGeneral.BackColor = Color.Transparent;
            this.pnlGeneral.Visible = false;

            this.pnlDungeons = new Panel("pnlDungeons");
            this.pnlDungeons.Size = new Size(200, 220);
            this.pnlDungeons.Location = new Point(0, 40);
            this.pnlDungeons.BackColor = Color.Transparent;
            this.pnlDungeons.Visible = false;

            this.pnlPokedex = new Panel("pnlPokedex");
            this.pnlPokedex.Size = new Size(200, 220);
            this.pnlPokedex.Location = new Point(0, 40);
            this.pnlPokedex.BackColor = Color.Transparent;
            this.pnlPokedex.Visible = false;

            this.lblAdventureLog = new Label("lblAdventureLog");
            this.lblAdventureLog.Location = new Point(20, 0);
            this.lblAdventureLog.Font = FontManager.LoadFont("PMU", 48);
            this.lblAdventureLog.AutoSize = true;
            this.lblAdventureLog.Text = "Profile";
            this.lblAdventureLog.ForeColor = Color.WhiteSmoke;

            this.page = 0;
            this.lblPage = new Label("lblPage");
            this.lblPage.Location = new Point(140, 20);
            this.lblPage.Font = FontManager.LoadFont("PMU", 16);
            this.lblPage.AutoSize = true;
            this.lblPage.Text = "Loading...";
            this.lblPage.ForeColor = Color.WhiteSmoke;
            this.lblGeneral = new Label("lblGeneral");
            this.lblGeneral.Location = new Point(20, 10);
            this.lblGeneral.Font = FontManager.LoadFont("PMU", 32);
            this.lblGeneral.AutoSize = true;
            this.lblGeneral.Text = "General";
            this.lblGeneral.ForeColor = Color.WhiteSmoke;

            this.lblPlayerName = new Label("lblPlayerName");
            this.lblPlayerName.Location = new Point(20, 40);
            this.lblPlayerName.Font = FontManager.LoadFont("PMU", 16);
            this.lblPlayerName.AutoSize = true;

            // lblPlayerName.Text = "Name: ";
            this.lblPlayerName.ForeColor = Color.WhiteSmoke;

            this.lblRank = new Label("lblRank");
            this.lblRank.Location = new Point(20, 60);
            this.lblRank.Font = FontManager.LoadFont("PMU", 16);
            this.lblRank.AutoSize = true;

            // lblRank.Text = "Rank: ";
            this.lblRank.ForeColor = Color.WhiteSmoke;

            this.lblPlayTime = new Label("lblPlayTime");
            this.lblPlayTime.Location = new Point(20, 80);
            this.lblPlayTime.Font = FontManager.LoadFont("PMU", 16);
            this.lblPlayTime.AutoSize = true;

            // lblPlayTime.Text = "Play Time: ";
            this.lblPlayTime.ForeColor = Color.WhiteSmoke;

            this.lblDungeonsCompleted = new Label("lblDungeonsCompleted");
            this.lblDungeonsCompleted.Location = new Point(20, 100);
            this.lblDungeonsCompleted.Font = FontManager.LoadFont("PMU", 16);
            this.lblDungeonsCompleted.AutoSize = true;

            // lblDungeonsCompleted.Text = "Dungeon Victories: ";
            this.lblDungeonsCompleted.ForeColor = Color.WhiteSmoke;

            this.lblMissionsCompleted = new Label("lblMissionsCompleted");
            this.lblMissionsCompleted.Location = new Point(20, 120);
            this.lblMissionsCompleted.Font = FontManager.LoadFont("PMU", 16);
            this.lblMissionsCompleted.AutoSize = true;

            // lblMissionsCompleted.Text = "Missions Completed: ";
            this.lblMissionsCompleted.ForeColor = Color.WhiteSmoke;

            // lblRescuesCompleted = new Label("lblRescuesCompleted");
            // lblRescuesCompleted.Location = new Point(20, 140);
            // lblRescuesCompleted.Font = FontManager.LoadFont("PMU", 16);
            // lblRescuesCompleted.AutoSize = true;
            // lblRescuesCompleted.Text = "Successful Rescues: ";
            // lblRescuesCompleted.ForeColor = Color.WhiteSmoke;

            this.lblDungeons = new Label("lblDungeons");
            this.lblDungeons.Location = new Point(20, 10);
            this.lblDungeons.Font = FontManager.LoadFont("PMU", 32);
            this.lblDungeons.AutoSize = true;
            this.lblDungeons.Text = "Dungeons";
            this.lblDungeons.ForeColor = Color.WhiteSmoke;

            this.lblEntered = new Label("lblEntered");
            this.lblEntered.Location = new Point(20, 40);
            this.lblEntered.Font = FontManager.LoadFont("PMU", 16);
            this.lblEntered.AutoSize = true;
            this.lblEntered.Text = "Red = Entered";
            this.lblEntered.ForeColor = Color.Red;

            this.lblCompleted = new Label("lblCompleted");
            this.lblCompleted.Location = new Point(100, 40);
            this.lblCompleted.Font = FontManager.LoadFont("PMU", 16);
            this.lblCompleted.AutoSize = true;
            this.lblCompleted.Text = "Blue = Completed";
            this.lblCompleted.ForeColor = Color.Cyan;

            this.lbxDungeonList = new ScrollingListBox("lstDungeonList");
            this.lbxDungeonList.Location = new Point(10, 60);
            this.lbxDungeonList.Size = new Size(this.pnlDungeons.Width - (this.lbxDungeonList.X * 2), this.pnlDungeons.Height - this.lbxDungeonList.Y - 10);
            this.lbxDungeonList.BackColor = Color.Transparent;
            this.lbxDungeonList.BorderStyle = SdlDotNet.Widgets.BorderStyle.FixedSingle;
            this.lblPokedex = new Label("lblPokedex");
            this.lblPokedex.Location = new Point(20, 10);
            this.lblPokedex.Font = FontManager.LoadFont("PMU", 32);
            this.lblPokedex.AutoSize = true;
            this.lblPokedex.Text = "Recruit List";
            this.lblPokedex.ForeColor = Color.WhiteSmoke;

            this.lblDefeated = new Label("lblDefeated");
            this.lblDefeated.Location = new Point(20, 40);
            this.lblDefeated.Font = FontManager.LoadFont("PMU", 16);
            this.lblDefeated.AutoSize = true;
            this.lblDefeated.Text = "Red = Defeated";
            this.lblDefeated.ForeColor = Color.Red;

            this.lblJoined = new Label("lblJoined");
            this.lblJoined.Location = new Point(100, 40);
            this.lblJoined.Font = FontManager.LoadFont("PMU", 16);
            this.lblJoined.AutoSize = true;
            this.lblJoined.Text = "Blue = Joined";
            this.lblJoined.ForeColor = Color.Cyan;

            this.lbxPokedex = new ScrollingListBox("lstPokedex");
            this.lbxPokedex.Location = new Point(10, 60);
            this.lbxPokedex.Size = new Size(this.pnlPokedex.Width - (this.lbxPokedex.X * 2), this.pnlPokedex.Height - this.lbxPokedex.Y - 10);
            this.lbxPokedex.BackColor = Color.Transparent;
            this.lbxPokedex.BorderStyle = SdlDotNet.Widgets.BorderStyle.FixedSingle;
            this.AddWidget(this.lblAdventureLog);
            this.AddWidget(this.lblPage);
            this.pnlGeneral.AddWidget(this.lblGeneral);
            this.pnlGeneral.AddWidget(this.lblPlayerName);
            this.pnlGeneral.AddWidget(this.lblRank);
            this.pnlGeneral.AddWidget(this.lblPlayTime);
            this.pnlGeneral.AddWidget(this.lblDungeonsCompleted);
            this.pnlGeneral.AddWidget(this.lblMissionsCompleted);

            // pnlGeneral.AddWidget(lblRescuesCompleted);
            this.pnlDungeons.AddWidget(this.lblDungeons);
            this.pnlDungeons.AddWidget(this.lblEntered);
            this.pnlDungeons.AddWidget(this.lblCompleted);
            this.pnlDungeons.AddWidget(this.lbxDungeonList);
            this.pnlPokedex.AddWidget(this.lblPokedex);
            this.pnlPokedex.AddWidget(this.lblDefeated);
            this.pnlPokedex.AddWidget(this.lblJoined);
            this.pnlPokedex.AddWidget(this.lbxPokedex);
            this.AddWidget(this.pnlGeneral);
            this.AddWidget(this.pnlDungeons);
            this.AddWidget(this.pnlPokedex);
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            if (!this.loaded)
            {
                return;
            }

            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.Backspace:
                    {
                        // Show the others menu when the backspace key is pressed
                        MenuSwitcher.ShowOthersMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.LeftArrow:
                    {
                        // change pages
                        this.SwitchToPage((this.page + 5) % 3);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.RightArrow:
                    {
                        // change pages
                        this.SwitchToPage((this.page + 1) % 3);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                    }

                    break;
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public void SwitchToPage(int newPage)
        {
            this.page = newPage;
            switch (this.page)
            {
                case 0:
                    {
                        this.pnlDungeons.Visible = false;
                        this.pnlPokedex.Visible = false;
                        this.pnlGeneral.Visible = true;
                    }

                    break;
                case 1:
                    {
                        this.pnlPokedex.Visible = false;
                        this.pnlGeneral.Visible = false;
                        this.pnlDungeons.Visible = true;
                    }

                    break;
                case 2:
                    {
                        this.pnlGeneral.Visible = false;
                        this.pnlDungeons.Visible = false;
                        this.pnlPokedex.Visible = true;
                    }

                    break;
            }

            this.lblPage.Text = "Page " + (this.page + 1);
        }

        public void LoadAdventureLogFromPacket(string[] parse)
        {
            int n;

            // General
            this.lblPlayerName.Text = "Name: " + Players.PlayerManager.MyPlayer.Name;
            this.lblRank.Text = "Rank: " + Missions.MissionManager.RankToString(Players.PlayerManager.MyPlayer.ExplorerRank) + " Rank (" + Players.PlayerManager.MyPlayer.MissionExp + " Pts.)";
            this.lblPlayTime.Text = "Play Time: " + (parse[1].ToInt() / 60).ToString().PadLeft(2, '0') + " : " + (parse[1].ToInt() % 60).ToString().PadLeft(2, '0');
            this.lblDungeonsCompleted.Text = "Dungeon Victories: " + parse[2];
            this.lblMissionsCompleted.Text = "Missions Completed: " + parse[3];

            // lblRescuesCompleted.Text = "Successful Rescues: " + parse[4];
            n = 5;

            // Dungeons
            for (int i = 0; i < parse[n].ToInt(); i++)
            {
                ListBoxTextItem item = new ListBoxTextItem(FontManager.LoadFont("PMU", 16), parse[n + (i * 2) + 1]);
                if (parse[n + (i * 2) + 2].ToInt() > 0)
                {
                    item.ForeColor = Color.Cyan;
                }
                else
                {
                    item.ForeColor = Color.Red;
                }

                this.lbxDungeonList.Items.Add(item);
            }

            n += this.lbxDungeonList.Items.Count * 2;

            for (int i = 1; i <= MaxInfo.TotalPokemon; i++)
            {
                ListBoxTextItem item;

                if (parse[n + 1].ToInt() <= 0)
                {
                    item = new ListBoxTextItem(FontManager.LoadFont("PMU", 16), "#" + i + ": -----");
                    item.ForeColor = Color.Gray;
                }
                else
                {
                    item = new ListBoxTextItem(FontManager.LoadFont("PMU", 16), "#" + i + ": " + Pokedex.PokemonHelper.Pokemon[i - 1].Name);
                    if (parse[n + 1].ToInt() == 1)
                    {
                        item.ForeColor = Color.Red;
                    }
                    else if (parse[n + 1].ToInt() == 2)
                    {
                        item.ForeColor = Color.Cyan;
                    }
                    else
                    {
                        item.ForeColor = Color.Black;
                    }
                }

                this.lbxPokedex.Items.Add(item);
                n++;
            }

            this.SwitchToPage(0);
            this.loaded = true;
        }
    }
}
