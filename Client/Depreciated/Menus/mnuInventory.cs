﻿// <copyright file="mnuInventory.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuInventory : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Enums.InvMenuType mode;
        private readonly Label[] lblVisibleItems;
        private readonly Label lblInventory;
        private readonly Widges.MenuItemPicker itemPicker;
        private readonly PictureBox picPreview;
        public int CurrentTen;
        private readonly Label lblItemNum;

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuInventory(string name, Enums.InvMenuType menuType, int itemSelected)
            : base(name)
            {
            this.Size = new Size(315, 360);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 63);

            this.lblInventory = new Label("lblInventory");
            this.lblInventory.AutoSize = true;
            this.lblInventory.Font = FontManager.LoadFont("PMU", 48);
            this.lblInventory.Text = "Inventory";
            this.lblInventory.ForeColor = Color.WhiteSmoke;
            this.lblInventory.Location = new Point(20, 0);

            this.picPreview = new PictureBox("picPreview");
            this.picPreview.Size = new Size(32, 32);
            this.picPreview.BackColor = Color.Transparent;
            this.picPreview.Location = new Point(255, 20);

            this.lblItemNum = new Label("lblItemNum");

            // lblItemNum.Size = new Size(100, 30);
            this.lblItemNum.AutoSize = true;
            this.lblItemNum.Location = new Point(182, 15);
            this.lblItemNum.Font = FontManager.LoadFont("PMU", 32);
            this.lblItemNum.BackColor = Color.Transparent;
            this.lblItemNum.ForeColor = Color.WhiteSmoke;
            this.lblItemNum.Text = "0/" + (((MaxInfo.MaxInv - 1) / 10) + 1);

            this.lblVisibleItems = new Label[10];
            for (int i = 0; i < this.lblVisibleItems.Length; i++)
            {
                this.lblVisibleItems[i] = new Label("lblVisibleItems" + i);

                // lblVisibleItems[i].AutoSize = true;
                this.lblVisibleItems[i].Size = new Size(200, 32);
                this.lblVisibleItems[i].Font = FontManager.LoadFont("PMU", 32);
                this.lblVisibleItems[i].Location = new Point(35, (i * 30) + 48);

                // lblVisibleItems[i].HoverColor = Color.Red;
                this.lblVisibleItems[i].ForeColor = Color.WhiteSmoke;
                this.lblVisibleItems[i].AllowDrop = true;
                this.lblVisibleItems[i].DragDrop += new EventHandler<DragEventArgs>(this.LblVisibleItems_DragDrop);
                this.lblVisibleItems[i].MouseDown += new EventHandler<MouseButtonEventArgs>(this.LblVisibleItems_MouseDown);
                this.lblVisibleItems[i].Click += new EventHandler<MouseButtonEventArgs>(this.InventoryItem_Click);
                this.AddWidget(this.lblVisibleItems[i]);
            }

            this.AddWidget(this.picPreview);
            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblInventory);
            this.AddWidget(this.lblItemNum);

            this.mode = menuType;
            this.CurrentTen = (itemSelected - 1) / 10;
            this.DisplayItems((this.CurrentTen * 10) + 1);
            this.ChangeSelected((itemSelected - 1) % 10);
            this.UpdateSelectedItemInfo();
        }

        private void LblVisibleItems_DragDrop(object sender, DragEventArgs e)
        {
            if (this.mode == Enums.InvMenuType.Use)
            {
                int oldSlot = Convert.ToInt32(e.Data.GetData(typeof(int)));
                Network.Messenger.SendSwapInventoryItems(oldSlot, Array.IndexOf(this.lblVisibleItems, sender) + 1 + (this.CurrentTen * 10));
            }
        }

        private void LblVisibleItems_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.mode == Enums.InvMenuType.Use)
            {
                if (Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuItemSelected") == null)
                {
                    Label label = (Label)sender;
                    SdlDotNet.Graphics.Surface dragSurf = new SdlDotNet.Graphics.Surface(label.Buffer.Size);
                    dragSurf.Fill(Color.Black);
                    dragSurf.Blit(label.Buffer, new Point(0, 0));
                    label.DoDragDrop(Array.IndexOf(this.lblVisibleItems, sender) + 1 + (this.CurrentTen * 10), DragDropEffects.Copy, dragSurf);
                }
            }
        }

        private void InventoryItem_Click(object sender, MouseButtonEventArgs e)
        {
            if (Players.PlayerManager.MyPlayer.GetInvItemNum((this.CurrentTen * 10) + 1 + Array.IndexOf(this.lblVisibleItems, sender)) > 0)
            {
                this.ChangeSelected(Array.IndexOf(this.lblVisibleItems, sender));

                if (this.mode == Enums.InvMenuType.Store)
                {
                    MnuBankItemSelected selectedMenu = (MnuBankItemSelected)Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuBankItemSelected");
                    if (selectedMenu != null)
                    {
                        Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);

                        // selectedMenu.ItemSlot = GetSelectedItemSlot();
                        // selectedMenu.ItemNum = Players.PlayerManager.MyPlayer.GetInvItemNum(GetSelectedItemSlot());
                    }

                    Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuBankItemSelected("mnuBankItemSelected", Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()), Players.PlayerManager.MyPlayer.Inventory[this.GetSelectedItemSlot()].Value, this.GetSelectedItemSlot(), Enums.InvMenuType.Store));
                        Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuBankItemSelected");
                }
                else if (this.mode == Enums.InvMenuType.Use)
                {
                    // Don't select the item, interferes with drag & drop
                    // mnuItemSelected selectedMenu = (mnuItemSelected)Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuItemSelected");
                    // if (selectedMenu != null) {
                    //    Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);
                    //    //selectedMenu.ItemSlot = GetSelectedItemSlot();
                    //    //ChangeSelected(Array.IndexOf(lblVisibleItems, sender));
                    // }
                    // Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new Menus.mnuItemSelected("mnuItemSelected", currentTen * 10 + 1 + Array.IndexOf(lblVisibleItems, sender)));
                    // Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuItemSelected");
                    ////ChangeSelected(Array.IndexOf(lblVisibleItems, sender));
                }
                else if (this.mode == Enums.InvMenuType.Sell)
                {
                        MnuShopItemSelected selectedMenu = (MnuShopItemSelected)Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuShopItemSelected");
                    if (selectedMenu != null)
                    {
                        Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);

                        // selectedMenu.ItemSlot = GetSelectedItemSlot();
                        // ChangeSelected(Array.IndexOf(lblVisibleItems, sender));
                    }

                        Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuShopItemSelected("mnuShopItemSelected", Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()), this.GetSelectedItemSlot(), Enums.InvMenuType.Sell));
                        Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuShopItemSelected");

                        // ChangeSelected(Array.IndexOf(lblVisibleItems, sender));
                }

                this.UpdateSelectedItemInfo();
                Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
            }
        }

        public void DisplayItems(int startNum)
        {
            // System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            // watch.Start();
            this.BeginUpdate();
            for (int i = 0; i < this.lblVisibleItems.Length; i++)
            {
                if (Players.PlayerManager.MyPlayer.GetInvItemNum(startNum + i) > 0)
                {
                    if (Players.PlayerManager.MyPlayer.IsEquiped(startNum + i))
                    { // Check if the item is equiped
                        // If it is equiped, set the labels forecolor to yellow, if it isn't yellow already
                        if (this.lblVisibleItems[i].ForeColor != Color.Yellow)
                        {
                            this.lblVisibleItems[i].ForeColor = Color.Yellow;
                        }
                    }
else
                    {
                        // If it isn't equiped, set the labels' forecolor to black, if it isn't black already
                        if (this.lblVisibleItems[i].ForeColor != Color.WhiteSmoke)
                        {
                            this.lblVisibleItems[i].ForeColor = Color.WhiteSmoke;
                        }
                    }

                    string itemName = Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(startNum + i)].Name;
                    if (!string.IsNullOrEmpty(itemName))
                    {
                        int itemAmount = Players.PlayerManager.MyPlayer.GetInvItemAmount(startNum + i);
                        if (itemAmount > 0)
                        {
                            itemName += " (" + itemAmount + ")";
                        }

                        if (Players.PlayerManager.MyPlayer.GetInvItemSticky(startNum + i))
                        {
                            itemName = "x" + itemName;
                        }

                        this.lblVisibleItems[i].Text = itemName;
                    }
else
                    {
                        this.lblVisibleItems[i].Text = string.Empty;
                    }
                }
else
                {
                    if (this.lblVisibleItems[i].ForeColor != Color.WhiteSmoke)
                    {
                        this.lblVisibleItems[i].ForeColor = Color.WhiteSmoke;
                    }

                    this.lblVisibleItems[i].Text = "----";
                }
            }

            this.EndUpdate();

            // watch.Stop();
            // Console.WriteLine(watch.Elapsed.ToString());
        }

        public void UpdateVisibleItem(int itemNum)
        {// appears to be unused
            this.BeginUpdate();
            if (itemNum / 10 != this.CurrentTen)
            {
                return;
            }

            int itemIndex = itemNum - (this.CurrentTen * 10) - 1;
            if (Players.PlayerManager.MyPlayer.GetInvItemNum(itemNum) > 0)
            {
                    if (Players.PlayerManager.MyPlayer.IsEquiped(itemNum))
                    { // Check if the item is equiped
                        // If it is equiped, set the labels forecolor to yellow, if it isn't yellow already
                        if (this.lblVisibleItems[itemIndex].ForeColor != Color.Yellow)
                        {
                        this.lblVisibleItems[itemIndex].ForeColor = Color.Yellow;
                        }
                    }
else
                    {
                        // If it isn't equiped, set the labels' forecolor to black, if it isn't black already
                        if (this.lblVisibleItems[itemIndex].ForeColor != Color.WhiteSmoke)
                        {
                        this.lblVisibleItems[itemIndex].ForeColor = Color.WhiteSmoke;
                        }
                    }

                    string itemName = Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(itemNum)].Name;
                    if (!string.IsNullOrEmpty(itemName))
                    {
                        int itemAmount = Players.PlayerManager.MyPlayer.GetInvItemAmount(itemNum);
                        if (itemAmount > 0)
                        {
                            itemName += " (" + itemAmount + ")";
                        }

                        if (Players.PlayerManager.MyPlayer.GetInvItemSticky(itemNum))
                        {
                            itemName = "x" + itemName;
                        }

                    this.lblVisibleItems[itemIndex].Text = itemName;
                    }
else
                    {
                    this.lblVisibleItems[itemIndex].Text = string.Empty;
                    }
                }
else
                {
                    if (this.lblVisibleItems[itemIndex].ForeColor != Color.WhiteSmoke)
                    {
                    this.lblVisibleItems[itemIndex].ForeColor = Color.WhiteSmoke;
                    }

                this.lblVisibleItems[itemIndex].Text = "----";
                }

            this.EndUpdate();
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 63 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        private int GetSelectedItemSlot()
        {
            return this.itemPicker.SelectedItem + (this.CurrentTen * 10) + 1;
        }

        public void UpdateSelectedItemInfo()
        {
            if (Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()) > 0)
            {
                this.picPreview.Visible = true;
                this.picPreview.Image = Tools.CropImage(GraphicsManager.Items, new Rectangle((Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot())].Pic - ((int)(Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot())].Pic / 6) * 6)) * Constants.TILEWIDTH, (int)(Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot())].Pic / 6) * Constants.TILEWIDTH, Constants.TILEWIDTH, Constants.TILEHEIGHT));
            }
else
            {
                this.picPreview.Visible = false;
            }

            this.lblItemNum.Text = (this.CurrentTen + 1) + "/" + (((MaxInfo.MaxInv - 1) / 10) + 1);
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem >= 9)
                        {
                            this.ChangeSelected(0);

                            // DisplayItems(1);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        this.UpdateSelectedItemInfo();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem <= 0)
                        {
                            this.ChangeSelected(9);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        this.UpdateSelectedItemInfo();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.LeftArrow:
                {
                        // int itemSlot = (currentTen + 1) - 10;//System.Math.Max(1, GetSelectedItemSlot() - (11 - itemPicker.SelectedItem));
                        if (this.CurrentTen <= 0)
                        {
                            this.CurrentTen = (MaxInfo.MaxInv - 1) / 10;
                        }
else
                        {
                            this.CurrentTen--;
                        }

                        this.DisplayItems((this.CurrentTen * 10) + 1);
                        this.UpdateSelectedItemInfo();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.RightArrow:
                {
                        // int itemSlot = currentTen + 1 + 10;
                        if (this.CurrentTen >= ((MaxInfo.MaxInv - 1) / 10))
                        {
                            this.CurrentTen = 0;
                        }
else
                        {
                            this.CurrentTen++;
                        }

                        this.DisplayItems((this.CurrentTen * 10) + 1);
                        this.UpdateSelectedItemInfo();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        if (Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()) > 0)
                        {
                            if (this.mode == Enums.InvMenuType.Store)
                            {
                                Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuBankItemSelected("mnuBankItemSelected", Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()), Players.PlayerManager.MyPlayer.Inventory[this.GetSelectedItemSlot()].Value, this.GetSelectedItemSlot(), Enums.InvMenuType.Store));
                                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuBankItemSelected");
                            }
                            else if (this.mode == Enums.InvMenuType.Use)
                            {
                                Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuItemSelected("mnuItemSelected", this.GetSelectedItemSlot()));
                                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuItemSelected");
                            }
                            else if (this.mode == Enums.InvMenuType.Sell)
                            {
                                Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuShopItemSelected("mnuShopItemSelected", Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()), this.GetSelectedItemSlot(), Enums.InvMenuType.Sell));
                                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuShopItemSelected");
                            }

                            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                        }
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                        if (this.mode == Enums.InvMenuType.Store)
                        {
                            MenuSwitcher.OpenBankOptions();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                        }
                        else if (this.mode == Enums.InvMenuType.Use)
                        {
                            MenuSwitcher.ShowMainMenu();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                        }
                        else if (this.mode == Enums.InvMenuType.Sell)
                        {
                            MenuSwitcher.OpenShopOptions();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                        }
                    }

                    break;
            }
        }
    }
}
