﻿// <copyright file="mnuOthers.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuOthers : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private const int MAXITEMS = 4;

        private readonly Widges.MenuItemPicker itemPicker;
        private readonly Label lblOthers;
        private readonly Label lblOptions;
        private readonly Label lblOnlineList;
        private readonly Label lblBattleLog;
        private readonly Label lblAdventureLog;
        private readonly Label lblHelp;

        public MnuOthers(string name)
            : base(name)
            {
            this.Size = new Size(185, 230);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 63);

            this.lblOthers = new Label("lblOthers");
            this.lblOthers.Location = new Point(20, 0);
            this.lblOthers.Font = FontManager.LoadFont("PMU", 48);
            this.lblOthers.AutoSize = true;
            this.lblOthers.ForeColor = Color.WhiteSmoke;
            this.lblOthers.Text = "Others";

            this.lblOptions = new Label("lblOptions");
            this.lblOptions.AutoSize = true;
            this.lblOptions.Location = new Point(30, 48);
            this.lblOptions.Font = FontManager.LoadFont("PMU", 32);
            this.lblOptions.Text = "Options";
            this.lblOptions.HoverColor = Color.Red;
            this.lblOptions.ForeColor = Color.WhiteSmoke;
            this.lblOptions.Click += new EventHandler<MouseButtonEventArgs>(this.LblOptions_Click);

            this.lblOnlineList = new Label("lblOnlineList");
            this.lblOnlineList.AutoSize = true;
            this.lblOnlineList.Location = new Point(30, 78);
            this.lblOnlineList.Font = FontManager.LoadFont("PMU", 32);
            this.lblOnlineList.Text = "Online List";
            this.lblOnlineList.HoverColor = Color.Red;
            this.lblOnlineList.ForeColor = Color.WhiteSmoke;
            this.lblOnlineList.Click += new EventHandler<MouseButtonEventArgs>(this.LblOnlineList_Click);

            this.lblBattleLog = new Label("lblBattleLog");
            this.lblBattleLog.AutoSize = true;
            this.lblBattleLog.Location = new Point(30, 108);
            this.lblBattleLog.Font = FontManager.LoadFont("PMU", 32);
            this.lblBattleLog.Text = "Battle Log";
            this.lblBattleLog.HoverColor = Color.Red;
            this.lblBattleLog.ForeColor = Color.WhiteSmoke;
            this.lblBattleLog.Click += new EventHandler<MouseButtonEventArgs>(this.LblBattleLog_Click);

            this.lblAdventureLog = new Label("lblAdventureLog");
            this.lblAdventureLog.AutoSize = true;
            this.lblAdventureLog.Location = new Point(30, 138);
            this.lblAdventureLog.Font = FontManager.LoadFont("PMU", 32);
            this.lblAdventureLog.Text = "Profile";
            this.lblAdventureLog.HoverColor = Color.Red;
            this.lblAdventureLog.ForeColor = Color.WhiteSmoke;
            this.lblAdventureLog.Click += new EventHandler<MouseButtonEventArgs>(this.LblAdventureLog_Click);

            this.lblHelp = new Label("lblHelp");
            this.lblHelp.AutoSize = true;
            this.lblHelp.Location = new Point(30, 168);
            this.lblHelp.Font = FontManager.LoadFont("PMU", 32);
            this.lblHelp.Text = "Help";
            this.lblHelp.HoverColor = Color.Red;
            this.lblHelp.ForeColor = Color.WhiteSmoke;
            this.lblHelp.Click += new EventHandler<MouseButtonEventArgs>(this.LblHelp_Click);

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblOthers);
            this.AddWidget(this.lblOptions);
            this.AddWidget(this.lblOnlineList);
            this.AddWidget(this.lblBattleLog);
            this.AddWidget(this.lblAdventureLog);
            this.AddWidget(this.lblHelp);
        }

        private void LblOptions_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(0);
        }

        private void LblOnlineList_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1);
        }

        private void LblBattleLog_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(2);
        }

        private void LblAdventureLog_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(3);
        }

        private void LblHelp_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(4);
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 63 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem);
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                        // Show the main menu when the backspace key is pressed
                        MenuSwitcher.ShowMainMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum)
        {
            switch (itemNum)
            {
                case 0:
                {
                        System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(RunOptions));
                        t.SetApartmentState(System.Threading.ApartmentState.STA); // Set the thread to STA
                        t.Start();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case 1:
                {
                        MenuSwitcher.ShowMenu(new MnuOnlineList("mnuOnlineList"));
                        Network.Messenger.SendOnlineListRequest();
                    }

                    break;
                case 2:
                {
                        MenuSwitcher.ShowMenu(new MnuBattleLog("mnuBattleLog"));
                    }

                    break;
                case 3:
                {
                        MenuSwitcher.ShowMenu(new MnuAdventureLog("mnuAdventureLog"));
                        Network.Messenger.SendAdventureLogRequest();
                    }

                    break;
                case 4:
                { // Help menu
                    MenuSwitcher.ShowHelpMenu();
                    Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        private static void RunOptions()
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.Run(new Windows.WinOptions());
        }
    }
}
