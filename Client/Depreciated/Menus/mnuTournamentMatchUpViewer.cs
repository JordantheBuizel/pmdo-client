﻿// <copyright file="mnuTournamentMatchUpViewer.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuTournamentMatchUpViewer : Widges.BorderedPanel, Core.IMenu
    {
        private readonly Label lblLeftArrow;
        private readonly Label lblRightArrow;
        private readonly PictureBox pbxPlayerOneMugshot;
        private readonly PictureBox pbxPlayerTwoMugshot;
        private readonly Label lblVSLabel;
        private readonly Label lblPlayerOneName;
        private readonly Label lblPlayerTwoName;

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        public MnuTournamentMatchUpViewer(string name)
            : base(name)
            {
            this.Size = new Size(300, 400);

            this.lblLeftArrow = new Label("lblLeftArrow");
            this.lblLeftArrow.AutoSize = false;
            this.lblLeftArrow.Size = new Size(50, 35);
            this.lblLeftArrow.Font = FontManager.LoadFont("PMU", 32);
            this.lblLeftArrow.Text = "<";
            this.lblLeftArrow.Location = new Point(5);

            this.lblRightArrow = new Label("lblRightArrow");
            this.lblRightArrow.AutoSize = false;
            this.lblRightArrow.Size = new Size(50, 35);
            this.lblRightArrow.Font = FontManager.LoadFont("PMU", 32);
            this.lblRightArrow.Text = ">";
            this.lblRightArrow.Location = new Point(this.Width - this.lblRightArrow.Width - 5);

            this.pbxPlayerOneMugshot = new PictureBox("pbxPlayerOneMugshot");
            this.pbxPlayerOneMugshot.Size = new Size(40, 40);
            this.pbxPlayerOneMugshot.Location = new Point(this.lblLeftArrow.X + this.lblLeftArrow.Width + 5, 5);

            this.lblVSLabel = new Label("lblVSLabel");
            this.lblVSLabel.Font = FontManager.LoadFont("PMU", 32);
            this.lblVSLabel.AutoSize = false;
            this.lblVSLabel.Size = new Size(100, 35);
            this.lblVSLabel.Location = new Point(this.pbxPlayerOneMugshot.X + this.pbxPlayerOneMugshot.Width + 5, this.pbxPlayerOneMugshot.Y + this.pbxPlayerOneMugshot.Height - this.lblVSLabel.Height);

            this.pbxPlayerTwoMugshot = new PictureBox("pbxPlayerTwoMugshot");
            this.pbxPlayerTwoMugshot.Size = new Size(40, 40);
            this.pbxPlayerTwoMugshot.Location = new Point(this.lblVSLabel.X + this.lblVSLabel.Width + 5, this.pbxPlayerOneMugshot.Y);

            this.lblPlayerOneName = new Label("lblPlayerOneName");
            this.lblPlayerOneName.Font = FontManager.LoadFont("PMU", 24);
            this.lblPlayerOneName.Location = new Point(this.pbxPlayerOneMugshot.X, this.pbxPlayerOneMugshot.Y + this.pbxPlayerOneMugshot.Height + 5);

            this.lblPlayerTwoName = new Label("lblPlayerTwoName");
            this.lblPlayerTwoName.Font = FontManager.LoadFont("PMU", 24);
            this.lblPlayerTwoName.Location = new Point(this.pbxPlayerTwoMugshot.X, this.pbxPlayerTwoMugshot.Y + this.pbxPlayerTwoMugshot.Height + 5);
        }

        public void LoadMatchUpsFromPacket(string[] parse)
        {
            // int i;
        }
    }
}