﻿// <copyright file="mnuShopOptions.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuShopOptions : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private const int MAXITEMS = 1;

        private readonly Widges.MenuItemPicker itemPicker;
        private readonly Label lblBuy;
        private readonly Label lblSell;

        public MnuShopOptions(string name)
            : base(name)
            {
            this.Size = new Size(155, 88);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 23);

            this.lblBuy = new Label("lblBuy");
            this.lblBuy.AutoSize = true;
            this.lblBuy.Location = new Point(30, 8);
            this.lblBuy.Font = FontManager.LoadFont("PMU", 32);
            this.lblBuy.Text = "Buy";
            this.lblBuy.HoverColor = Color.Red;
            this.lblBuy.ForeColor = Color.WhiteSmoke;
            this.lblBuy.Click += new EventHandler<MouseButtonEventArgs>(this.LblBuy_Click);

            this.lblSell = new Label("lblSell");
            this.lblSell.AutoSize = true;
            this.lblSell.Location = new Point(30, 38);
            this.lblSell.Font = FontManager.LoadFont("PMU", 32);
            this.lblSell.Text = "Sell";
            this.lblSell.HoverColor = Color.Red;
            this.lblSell.ForeColor = Color.WhiteSmoke;
            this.lblSell.Click += new EventHandler<MouseButtonEventArgs>(this.LblSell_Click);

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblBuy);
            this.AddWidget(this.lblSell);
        }

        private void LblBuy_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(0);
        }

        private void LblSell_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1);
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 23 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem);
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum)
        {
            switch (itemNum)
            {
                case 0:
                {
                        MenuSwitcher.ShowShopBuyMenu(0);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case 1:
                {
                        MenuSwitcher.ShowShopSellMenu(1);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
            }
        }

        public void Close(bool message)
        {
            if (message)
            {
                this.Close();
            }
else
            {
                base.Close();
            }
        }

        /// <inheritdoc/>
        public override void Close()
        {
            Network.Messenger.LeaveShop();
            base.Close();
        }
    }
}
