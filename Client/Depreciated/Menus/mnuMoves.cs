﻿// <copyright file="mnuMoves.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuMoves : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label[] lblAllMoves;
        private readonly Label[] lblAllMovesPP;
        private readonly Label lblMoves;

        private readonly Widges.MenuItemPicker itemPicker;
        private const int MAXITEMS = 3;

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuMoves(string name)
            : base(name)
            {
            this.Size = new Size(280, 188);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 63);

            this.lblMoves = new Label("lblMoves");
            this.lblMoves.Location = new Point(20, 0);
            this.lblMoves.AutoSize = true;
            this.lblMoves.Font = FontManager.LoadFont("PMU", 48);
            this.lblMoves.ForeColor = Color.WhiteSmoke;
            this.lblMoves.Text = "Moves";

            this.lblAllMoves = new Label[Players.PlayerManager.MyPlayer.Moves.Length];
            this.lblAllMovesPP = new Label[Players.PlayerManager.MyPlayer.Moves.Length];
            for (int i = 0; i < Players.PlayerManager.MyPlayer.Moves.Length; i++)
            {
                this.lblAllMoves[i] = new Label("lblAllMoves" + i);
                this.lblAllMoves[i].AutoSize = true;
                this.lblAllMoves[i].Font = FontManager.LoadFont("PMU", 32);
                this.lblAllMoves[i].Location = new Point(30, (i * 30) + 48);

                // lblAllMoves[i].HoverColor = Color.Red;
                this.lblAllMoves[i].AllowDrop = true;
                this.lblAllMoves[i].MouseDown += new EventHandler<MouseButtonEventArgs>(this.LblAllMoves_MouseDown);
                this.lblAllMoves[i].DragDrop += new EventHandler<DragEventArgs>(this.LblAllMoves_DragDrop);
                this.lblAllMoves[i].Click += new EventHandler<MouseButtonEventArgs>(this.Move_Click);
                this.AddWidget(this.lblAllMoves[i]);

                this.lblAllMovesPP[i] = new Label("lblAllMovesPP" + i);
                this.lblAllMovesPP[i].AutoSize = true;
                this.lblAllMovesPP[i].Centered = true;
                this.lblAllMovesPP[i].Font = FontManager.LoadFont("PMU", 32);
                this.lblAllMovesPP[i].Location = new Point(190, (i * 30) + 48);
                this.AddWidget(this.lblAllMovesPP[i]);
            }

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblMoves);

            this.DisplayMoves();

            this.ChangeSelected(0);
        }

        private void LblAllMoves_DragDrop(object sender, DragEventArgs e)
        {
            int oldSlot = Convert.ToInt32(e.Data.GetData(typeof(int)));
            Network.Messenger.SendSwapMoves(oldSlot, Array.IndexOf(this.lblAllMoves, sender));
        }

        private void LblAllMoves_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuMoveSelected") == null)
            {
                Label label = (Label)sender;
                SdlDotNet.Graphics.Surface dragSurf = new SdlDotNet.Graphics.Surface(label.Buffer.Size);
                dragSurf.Fill(Color.Black);
                dragSurf.Blit(label.Buffer, new Point(0, 0));
                label.DoDragDrop(Array.IndexOf(this.lblAllMoves, sender), DragDropEffects.Copy, dragSurf);
            }
        }

        private void Move_Click(object sender, MouseButtonEventArgs e)
        {
            // SelectMove(Array.IndexOf(lblAllMoves, sender));
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 63 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectMove(this.itemPicker.SelectedItem);
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                        // Show the main menu when the backspace key is pressed
                        MenuSwitcher.ShowMainMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        private void SelectMove(int moveSlot)
        {
            Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuMoveSelected("mnuMoveSelected", moveSlot));
            Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuMoveSelected");
            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
        }

        public void DisplayMoves()
        {
            for (int i = 0; i < this.lblAllMoves.Length; i++)
            {
                if (Players.PlayerManager.MyPlayer.Moves[i].MoveNum > 0)
                {
                    this.lblAllMoves[i].Text = Moves.MoveHelper.Moves[Players.PlayerManager.MyPlayer.Moves[i].MoveNum].Name;
                    this.lblAllMovesPP[i].Text = Players.PlayerManager.MyPlayer.Moves[i].CurrentPP + "/" + Players.PlayerManager.MyPlayer.Moves[i].MaxPP;
                    if (Players.PlayerManager.MyPlayer.Moves[i].CurrentPP > 0 && !Players.PlayerManager.MyPlayer.Moves[i].Sealed)
                    {
                        this.lblAllMoves[i].ForeColor = Color.WhiteSmoke;
                        this.lblAllMovesPP[i].ForeColor = Color.WhiteSmoke;
                    }
                    else
                    {
                        this.lblAllMoves[i].ForeColor = Color.Red;
                        this.lblAllMovesPP[i].ForeColor = Color.Red;
                    }
                }
else
                {
                    this.lblAllMoves[i].Text = "-----";
                    this.lblAllMovesPP[i].Text = "--/--";
                    this.lblAllMoves[i].ForeColor = Color.Gray;
                    this.lblAllMovesPP[i].ForeColor = Color.Gray;
                }
            }
        }
    }
}
