﻿// <copyright file="mnuOnlineList.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class MnuOnlineList : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblOnlineList;
        private readonly Label lblLoading;
        private readonly Label lblTotal;
        private readonly ScrollingListBox lstOnlinePlayers;

        public MnuOnlineList(string name)
            : base(name)
            {
            this.Size = new Size(185, 220);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.lblOnlineList = new Label("lblOnlineList");
            this.lblOnlineList.Location = new Point(20, 0);
            this.lblOnlineList.Font = FontManager.LoadFont("PMU", 36);
            this.lblOnlineList.AutoSize = true;
            this.lblOnlineList.Text = "Online List";
            this.lblOnlineList.ForeColor = Color.WhiteSmoke;

            this.lblLoading = new Label("lblLoading");
            this.lblLoading.Location = new Point(10, 50);
            this.lblLoading.Font = FontManager.LoadFont("PMU", 16);
            this.lblLoading.AutoSize = true;
            this.lblLoading.Text = "Loading...";
            this.lblLoading.ForeColor = Color.WhiteSmoke;

            this.lblTotal = new Label("lblTotal");
            this.lblTotal.Location = new Point(10, 34);
            this.lblTotal.Font = FontManager.LoadFont("PMU", 16);
            this.lblTotal.AutoSize = true;
            this.lblTotal.ForeColor = Color.WhiteSmoke;

            this.lstOnlinePlayers = new ScrollingListBox("lstOnlinePlayers");
            this.lstOnlinePlayers.Location = new Point(10, 50);
            this.lstOnlinePlayers.Size = new Size(this.Width - (this.lstOnlinePlayers.X * 2), this.Height - this.lstOnlinePlayers.Y - 10);
            this.lstOnlinePlayers.BackColor = Color.Transparent;
            this.lstOnlinePlayers.BorderStyle = SdlDotNet.Widgets.BorderStyle.None;

            this.AddWidget(this.lblOnlineList);
            this.AddWidget(this.lblLoading);
            this.AddWidget(this.lblTotal);
            this.AddWidget(this.lstOnlinePlayers);
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.Backspace:
                {
                        // Show the others menu when the backspace key is pressed
                        MenuSwitcher.ShowOthersMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public void AddOnlinePlayers(string[] parse)
        {
            this.lblLoading.Visible = false;
            int count = parse[1].ToInt();

            int n = 2;

            for (int i = 0; i < count; i++)
            {
                ListBoxTextItem item = new ListBoxTextItem(FontManager.LoadFont("PMU", 16), parse[i + n]);
                item.ForeColor = Color.WhiteSmoke;
                this.lstOnlinePlayers.Items.Add(item);
            }

            this.lblTotal.Text = count + " Players Online";
        }
    }
}
