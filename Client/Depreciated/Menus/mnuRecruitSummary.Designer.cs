﻿namespace Client.Logic.Menus
{
    partial class MnuRecruitSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbPortrait = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pbarExp = new System.Windows.Forms.ProgressBar();
            this.lblExp = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbPortrait)).BeginInit();
            this.SuspendLayout();
            // 
            // pbPortrait
            // 
            this.pbPortrait.Image = global::Client.Logic.Properties.Resources.Unknown;
            this.pbPortrait.Location = new System.Drawing.Point(212, 12);
            this.pbPortrait.Name = "pbPortrait";
            this.pbPortrait.Size = new System.Drawing.Size(40, 40);
            this.pbPortrait.TabIndex = 0;
            this.pbPortrait.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Level:";
            // 
            // pbarExp
            // 
            this.pbarExp.Location = new System.Drawing.Point(15, 104);
            this.pbarExp.Name = "pbarExp";
            this.pbarExp.Size = new System.Drawing.Size(137, 10);
            this.pbarExp.Step = 1;
            this.pbarExp.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pbarExp.TabIndex = 2;
            // 
            // lblExp
            // 
            this.lblExp.Location = new System.Drawing.Point(12, 117);
            this.lblExp.Name = "lblExp";
            this.lblExp.Size = new System.Drawing.Size(140, 15);
            this.lblExp.TabIndex = 3;
            this.lblExp.Text = "Exp: 100%";
            this.lblExp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mnuRecruitSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 174);
            this.Controls.Add(this.lblExp);
            this.Controls.Add(this.pbarExp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbPortrait);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "mnuRecruitSummary";
            this.Text = "mnuRecruitSummary";
            ((System.ComponentModel.ISupportInitialize)(this.pbPortrait)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbPortrait;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar pbarExp;
        private System.Windows.Forms.Label lblExp;
    }
}