﻿// <copyright file="mnuAdventurePack.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Graphic;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class MnuAdventurePack : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label[] lblVisibleItems;
        private readonly Label lblItemCollection;
        private readonly Widges.MenuItemPicker itemPicker;
        private readonly PictureBox picPreview;

        public List<Players.InventoryItem> BankItems
        {
            get;
            set;
        }

        public List<int> SortedItems
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        private readonly List<int> abilityItemNums = new List<int>
        {
            777,
            778,
            779,
            780,
            781
        };

        private readonly List<string> abilityNames = new List<string>()
        {
            "Magma Maker",
            "Cloud Lift",
            "Aqua Fin",
            "Cloudy Wings",
            "Abyssal Bubble"
        };

        public List<bool> Abilities
        {
            get;
            internal set;
        }

        public MnuAdventurePack(string name, bool canRockSmash, bool canRockClimb, bool canSurf, bool canFly, bool canDive)
            : base(name)
        {
            this.Abilities = new List<bool>
            {
                canRockSmash,
                canRockClimb,
                canSurf,
                canFly,
                canDive
            };

            this.Size = new Size(315, 400);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 32);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 89);

            this.lblItemCollection = new Label("lblItemCollection");
            this.lblItemCollection.AutoSize = true;
            this.lblItemCollection.Font = FontManager.LoadFont("PMU", 48);
            this.lblItemCollection.Text = "Adventure Pack";
            this.lblItemCollection.Location = new Point(20, 0);
            this.lblItemCollection.ForeColor = Color.WhiteSmoke;

            this.picPreview = new PictureBox("picPreview");
            this.picPreview.Size = new Size(32, 32);
            this.picPreview.BackColor = Color.Transparent;
            this.picPreview.Location = new Point(255, 20);
            this.lblVisibleItems = new Label[5];

            for (int i = 0; i < this.lblVisibleItems.Length; i++)
            {
                this.lblVisibleItems[i] = new Label("lblVisibleItems" + i);
                this.lblVisibleItems[i].Width = 200;
                this.lblVisibleItems[i].Font = FontManager.LoadFont("PMU", 32);
                this.lblVisibleItems[i].Location = new Point(35, (i * 30) + 72);
                this.lblVisibleItems[i].ForeColor = Color.WhiteSmoke;
                if (this.Abilities[i])
                {
                    this.lblVisibleItems[i].Text = this.abilityNames[i];
                }
                else
                {
                    this.lblVisibleItems[i].Text = "?????????????";
                }

                // TODO: Figure out what click will do.
                // lblVisibleItems[i].Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(bankItem_Click);
                this.AddWidget(this.lblVisibleItems[i]);
            }

            this.AddWidget(this.picPreview);
            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblItemCollection);

            this.ChangeSelected(0);
            this.UpdateSelectedItemInfo();
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 89 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        private void UpdateSelectedItemInfo()
        {
            if (-1 < this.itemPicker.SelectedItem && this.itemPicker.SelectedItem < 5)
            {
                this.picPreview.Visible = true;
                if (this.Abilities[this.itemPicker.SelectedItem])
                {
                    this.picPreview.Image = Tools.CropImage(GraphicsManager.Items, new Rectangle((Items.ItemHelper.Items[this.abilityItemNums[this.itemPicker.SelectedItem]].Pic - ((int)(Items.ItemHelper.Items[this.abilityItemNums[this.itemPicker.SelectedItem]].Pic / 6) * 6)) * Constants.TILEWIDTH, (int)(Items.ItemHelper.Items[this.abilityItemNums[this.itemPicker.SelectedItem]].Pic / 6) * Constants.TILEWIDTH, Constants.TILEWIDTH, Constants.TILEHEIGHT));
                }
                else
                {
                    SdlDotNet.Graphics.Surface unknown = new SdlDotNet.Graphics.Surface(32, 32);
                    unknown.Fill(Color.Transparent);
                    Size textSize = TextRenderer.SizeText2(FontManager.LoadFont("unown", 24), "?", false, 0);
                    CharRenderOptions[] renderOptions = new CharRenderOptions["?".Length];
                    for (int i = 0; i < renderOptions.Length; i++)
                    {
                        renderOptions[i] = new CharRenderOptions(Color.WhiteSmoke);
                    }

                    string unown = "?";
                    renderOptions = Network.MessageProcessor.ParseText(renderOptions, ref unown);
                    SdlDotNet.Graphics.Surface textsurf = TextRenderer.RenderTextBasic2(FontManager.LoadFont("unown", 24), unown, renderOptions, Color.WhiteSmoke, false, 32, 32, 0, 0);
                    unknown.Blit(textsurf, Graphic.DrawingSupport.GetCenter(unknown.Size, textsurf.Size));

                    this.picPreview.Image = unknown;
                }
            }
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            if (true)
            {
                base.OnKeyboardDown(e);
                switch (e.Key)
                {
                    case SdlDotNet.Input.Key.DownArrow:
                        {
                            if (this.itemPicker.SelectedItem >= 4)
                            {
                                this.ChangeSelected(0);

                                // DisplayItems(1);
                            }
                            else
                            {
                                this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                            }

                            this.UpdateSelectedItemInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.UpArrow:
                        {
                            if (this.itemPicker.SelectedItem <= 0)
                            {
                                this.ChangeSelected(4);
                            }
                            else
                            {
                                this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                            }

                            this.UpdateSelectedItemInfo();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                        }

                        break;
                    /*case SdlDotNet.Input.Key.Return:
                        {
                            if (SortedItems == null)
                            {
                                if (BankItems[GetSelectedItemSlot()].Num > 0)
                                {
                                    Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new Menus.mnuBankItemSelected("mnuBankItemSelected", BankItems[GetSelectedItemSlot()].Num, BankItems[GetSelectedItemSlot()].Value, GetSelectedItemSlot(), Enums.InvMenuType.Take));
                                    Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuBankItemSelected");
                                    Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                                }
                            }
                            else
                            {
                                if (GetSelectedItemSlot() < SortedItems.Count && BankItems[SortedItems[GetSelectedItemSlot()]].Num > 0)
                                {
                                    Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new Menus.mnuBankItemSelected("mnuBankItemSelected", BankItems[SortedItems[GetSelectedItemSlot()]].Num, BankItems[SortedItems[GetSelectedItemSlot()]].Value, SortedItems[GetSelectedItemSlot()], Enums.InvMenuType.Take));
                                    Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuBankItemSelected");
                                    Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                                }
                            }
                        }
                        break;*/
                    case SdlDotNet.Input.Key.Backspace:
                        {
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                        }

                        break;
                }
            }
        }
    }
}
