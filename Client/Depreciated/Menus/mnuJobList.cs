﻿// <copyright file="mnuJobList.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Widges;
    using SdlDotNet.Widgets;

    internal class MnuJobList : BorderedPanel, Core.IMenu
    {
        private const int MAXITEMS = 7;

        private readonly MenuItemPicker itemPicker;
        private readonly Label lblJobList;
        private readonly MissionTitle[] items;

        public MnuJobList(string name)
            : base(name)
            {
            this.Size = new Size(280, 460);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(15, 10);

            this.itemPicker = new MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(20, 64);

            this.lblJobList = new Label("lblJobList");
            this.lblJobList.AutoSize = true;
            this.lblJobList.Font = FontManager.LoadFont("PMU", 48);
            this.lblJobList.ForeColor = Color.WhiteSmoke;
            this.lblJobList.Text = "Job List";
            this.lblJobList.Location = new Point(20, 0);

            this.items = new MissionTitle[8];
            int lastY = 58;
            for (int i = 0; i < this.items.Length; i++)
            {
                this.items[i] = new MissionTitle("item" + i, this.Width);
                this.items[i].Location = new Point(15, lastY);

                if (Players.PlayerManager.MyPlayer.JobList.Jobs.Count > i)
                {
                    this.items[i].SetJob(Players.PlayerManager.MyPlayer.JobList.Jobs[i]);
                }
else
                {
                    this.items[i].SetJob(null);
                }

                this.AddWidget(this.items[i]);

                lastY += this.items[i].Height + 8;
            }

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblJobList);
        }

        /// <inheritdoc/>
        public BorderedPanel MenuPanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        public void DisplayItems()
        {
            for (int i = 0; i < this.items.Length; i++)
            {
                if (Players.PlayerManager.MyPlayer.JobList.Jobs.Count > i)
                {
                    this.items[i].SetJob(Players.PlayerManager.MyPlayer.JobList.Jobs[i]);
                }
else
                {
                    this.items[i].SetJob(null);
                }
            }

            Core.IMenu mnuJobSelected = Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuJobSelected");
            if (mnuJobSelected != null)
            {
                Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(mnuJobSelected);
            }
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(20, 64 + ((this.items[0].Height + 8) * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem);
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                        // Show the main menu when the backspace key is pressed
                        MenuSwitcher.ShowMainMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum)
        {
            if (Players.PlayerManager.MyPlayer.JobList.Jobs.Count > itemNum)
            {
                Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuJobSelected("mnuJobSelected", itemNum));
                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuJobSelected");
                Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
            }
        }
    }
}