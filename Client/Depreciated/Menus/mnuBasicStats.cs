﻿// <copyright file="mnuBasicStats.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using PMU.Sockets;
    using SdlDotNet.Widgets;

    internal class MnuBasicStats : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblExp;
        private readonly Label lblHP;
        private readonly Label lblPlayerName;
        private readonly Label lblWeather;
        private readonly Label lblBelly;
        private readonly Label lblMissionExp;
        private readonly Label lblExplorerRank;
        private readonly Label lblMoney;

        public MnuBasicStats(string name)
            : base(name)
        {
            this.Size = new Size(620, 200);

            // this.MenuDirection = Enums.MenuDirection.Horizontal;
            this.Location = new Point(10, 265);

            this.lblPlayerName = new Label("lblPlayerName");
            this.lblPlayerName.Location = new Point(30, 30);
            this.lblPlayerName.AutoSize = true;
            this.lblPlayerName.Font = FontManager.LoadFont("PMU", 32);
            this.lblPlayerName.ForeColor = Color.Yellow;
            this.lblPlayerName.Text = Players.PlayerManager.MyPlayer.Name;

            this.lblBelly = new Label("lblBelly");
            this.lblBelly.Location = new Point(30, 70);
            this.lblBelly.AutoSize = true;
            this.lblBelly.Font = FontManager.LoadFont("PMU", 32);
            this.lblBelly.Text = "Belly: " + Players.PlayerManager.MyPlayer.Belly + "/" + Players.PlayerManager.MyPlayer.MaxBelly;
            this.lblBelly.ForeColor = Color.WhiteSmoke;

            this.lblHP = new Label("lblHP");
            this.lblHP.Location = new Point(195, 30);
            this.lblHP.AutoSize = true;
            this.lblHP.Font = FontManager.LoadFont("PMU", 32);
            this.lblHP.Text = "HP: " + Players.PlayerManager.MyPlayer.GetActiveRecruit().HP + "/" + Players.PlayerManager.MyPlayer.GetActiveRecruit().MaxHP;
            this.lblHP.ForeColor = Color.WhiteSmoke;

            this.lblExp = new Label("lblExp");
            this.lblExp.Location = new Point(195, 70);
            this.lblExp.AutoSize = true;
            this.lblExp.Font = FontManager.LoadFont("PMU", 32);
            this.lblExp.Text = "Exp: " + Players.PlayerManager.MyPlayer.Exp + "/" + Players.PlayerManager.MyPlayer.MaxExp;
            this.lblExp.ForeColor = Color.WhiteSmoke;

            this.lblMissionExp = new Label("lblMissionExp");
            this.lblMissionExp.Location = new Point(30, 110);
            this.lblMissionExp.AutoSize = true;
            this.lblMissionExp.Font = FontManager.LoadFont("PMU", 32);
            this.lblMissionExp.Text = Missions.MissionManager.RankToString(Players.PlayerManager.MyPlayer.ExplorerRank) + " Rank (" + Players.PlayerManager.MyPlayer.MissionExp + " pts.)";
            this.lblMissionExp.ForeColor = Color.WhiteSmoke;

            this.lblMoney = new Label("lblMoney");
            this.lblMoney.Location = new Point(30, 140);
            this.lblMoney.AutoSize = true;
            this.lblMoney.Font = FontManager.LoadFont("PMU", 32);
            this.lblMoney.Text = "Poké: Loading...";
            this.lblMoney.ForeColor = Color.WhiteSmoke;

            this.lblExplorerRank = new Label("lblExplorerRank");
            this.lblExplorerRank.Location = new Point(360, 30);
            this.lblExplorerRank.AutoSize = true;
            this.lblExplorerRank.Font = FontManager.LoadFont("PMU", 32);
            this.lblExplorerRank.Text = "Rank: " + Players.PlayerManager.MyPlayer.ExplorerRank;
            this.lblExplorerRank.ForeColor = Color.WhiteSmoke;

            this.lblWeather = new Label("lblWeather");
            this.lblWeather.Location = new Point(360, 70);
            this.lblWeather.AutoSize = true;
            this.lblWeather.Font = FontManager.LoadFont("PMU", 32);
            this.lblWeather.Text = "Weather: Clear"; // +Logic.Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.Weather.ID.ToString();
            this.lblWeather.ForeColor = Color.WhiteSmoke;

            this.WidgetPanel.AddWidget(this.lblPlayerName);
            this.WidgetPanel.AddWidget(this.lblBelly);
            this.WidgetPanel.AddWidget(this.lblHP);
            this.WidgetPanel.AddWidget(this.lblExp);
            this.WidgetPanel.AddWidget(this.lblMissionExp);
            this.WidgetPanel.AddWidget(this.lblMoney);
            Messenger.SendPacket(TcpPacket.CreatePacket("requestmoney"));

            // this.WidgetPanel.AddWidget(lblExplorerRank);
            // this.WidgetPanel.AddWidget(lblWeather);
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public void SetMoney(string amount)
        {
            this.lblMoney.Text = "Poké: " + amount;
        }
    }
}