﻿// <copyright file="mnuHelpPage.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuHelpPage : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblHelpPage;
        private readonly Label lblPageNumber;
        private Button btnShowHelp;
        private readonly PictureBox picHelpPage;
        private readonly string helpFolder;
        private int page;

        public MnuHelpPage(string name, string helpFolder, int page)
            : base(name)
            {
            this.Size = new Size(620, 420);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.page = page;
            this.helpFolder = helpFolder;

            this.lblHelpPage = new Label("lblHelpTopics");
            this.lblHelpPage.Location = new Point(40, 5);
            this.lblHelpPage.Font = FontManager.LoadFont("PMU", 36);
            this.lblHelpPage.AutoSize = true;
            this.lblHelpPage.Text = helpFolder;
            this.lblHelpPage.ForeColor = Color.WhiteSmoke;

            this.lblPageNumber = new Label("lblPageNumber");
            this.lblPageNumber.AutoSize = true;
            this.lblPageNumber.Font = FontManager.LoadFont("PMU", 36);
            this.lblPageNumber.Text = "Page 1";
            this.lblPageNumber.Location = new Point(this.Width - this.lblPageNumber.Width - 40, 5);
            this.lblPageNumber.ForeColor = Color.WhiteSmoke;

            this.picHelpPage = new PictureBox("picHelpPage");
            this.picHelpPage.SizeMode = ImageSizeMode.StretchImage;
            this.picHelpPage.Location = new Point(50, 50);
            this.picHelpPage.Size = new Size(this.Width - (this.picHelpPage.X * 2), this.Height - this.picHelpPage.Y - 20);
            this.picHelpPage.BackColor = Color.Green;

            // lstHelpTopics = new ScrollingListBox("lstHelpTopics");
            // lstHelpTopics.Location = new Point(10, 50);
            // lstHelpTopics.Size = new Size(this.Width - lstHelpTopics.X * 2, this.Height - lstHelpTopics.Y - 10);
            // lstHelpTopics.BackColor = Color.Transparent;
            // lstHelpTopics.BorderStyle = SdlDotNet.Widgets.BorderStyle.None;
            this.AddWidget(this.lblHelpPage);
            this.AddWidget(this.picHelpPage);
            this.AddWidget(this.lblPageNumber);

            this.LoadHelpPage(this.page);
        }

        private void LoadHelpPage(int page)
        {
            if (System.IO.File.Exists(IO.Paths.StartupPath + "Help/" + this.helpFolder + "/" + "page" + (page + 1).ToString() + ".png"))
            {
                this.picHelpPage.Image = SurfaceManager.LoadSurface(IO.Paths.StartupPath + "Help/" + this.helpFolder + "/" + "page" + (page + 1).ToString() + ".png", true, false);
                this.lblPageNumber.Text = "Page " + (page + 1).ToString();
                this.lblPageNumber.Location = new Point(this.Width - this.lblPageNumber.Width - 40, 5);
            }
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.LeftArrow:
                {
                        if (this.page > 0)
                        {
                            this.page--;
                            this.LoadHelpPage(this.page);
                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                        }
                    }

                    break;
                case SdlDotNet.Input.Key.RightArrow:
                {
                        if (System.IO.File.Exists(IO.Paths.StartupPath + "Help/" + this.helpFolder + "/" + "page" + (this.page + 1).ToString() + ".png"))
                        {
                            this.page++;
                            this.LoadHelpPage(this.page);
                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                        }
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                        // Show the otherackspace key is pressed
                        MenuSwitcher.ShowHelpMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }
    }
}
