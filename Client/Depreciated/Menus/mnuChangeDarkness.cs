﻿// <copyright file="mnuChangeDarkness.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using SdlDotNet.Widgets;

    internal class MnuChangeDarkness : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblAddTile;
        private readonly NumericUpDown nudAmount;
        private readonly Label lblPrice;
        private readonly Button btnAccept;
        private readonly Button btnCancel;
        private readonly int price;

        public MnuChangeDarkness(string name, int price)
            : base(name)
            {
                this.price = price;
            this.Size = new Size(250, 250);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = Graphic.DrawingSupport.GetCenter(Windows.WindowSwitcher.GameWindow.MapViewer.Size, this.Size);

            this.lblAddTile = new Label("lblAddTile");
            this.lblAddTile.Location = new Point(25, 15);
            this.lblAddTile.AutoSize = false;
            this.lblAddTile.Size = new Size(this.Width - (this.lblAddTile.X * 2), 40);
            this.lblAddTile.Text = "Enter the size of the lights for the house (use -1 for full lighting):";
            this.lblAddTile.ForeColor = Color.WhiteSmoke;

            this.nudAmount = new NumericUpDown("nudAmount");
            this.nudAmount.Size = new Size(120, 24);
            this.nudAmount.Location = new Point(this.lblAddTile.X, this.lblAddTile.Y + this.lblAddTile.Height + 10);
            this.nudAmount.Maximum = 20;
            this.nudAmount.Minimum = -1;
            this.nudAmount.Value = Maps.MapHelper.ActiveMap.Darkness;
            this.nudAmount.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudAmount_ValueChanged);

            this.lblPrice = new Label("lblPrice");
            this.lblPrice.Location = new Point(this.lblAddTile.X, this.nudAmount.Y + this.nudAmount.Height + 10);
            this.lblPrice.AutoSize = false;
            this.lblPrice.Size = new Size(180, 40);
            this.lblPrice.Text = "Adjusting the lighting will cost " + price + " " + Items.ItemHelper.Items[1].Name + ".";
            this.lblPrice.ForeColor = Color.WhiteSmoke;

            this.btnAccept = new Button("btnAccept");
            this.btnAccept.Location = new Point(this.lblAddTile.X, this.lblPrice.Y + this.lblPrice.Height + 10);
            this.btnAccept.Size = new Size(80, 30);
            this.btnAccept.Text = "Set Lights";
            this.btnAccept.Font = FontManager.LoadFont("tahoma", 10);
            Skins.SkinManager.LoadButtonGui(this.btnAccept);
            this.btnAccept.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAccept_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(this.btnAccept.X + this.btnAccept.Width, this.lblPrice.Y + this.lblPrice.Height + 10);
            this.btnCancel.Size = new Size(80, 30);
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Font = FontManager.LoadFont("tahoma", 10);
            Skins.SkinManager.LoadButtonGui(this.btnCancel);
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.AddWidget(this.lblAddTile);
            this.AddWidget(this.nudAmount);
            this.AddWidget(this.lblPrice);
            this.AddWidget(this.btnAccept);
            this.AddWidget(this.btnCancel);
        }

        private void NudAmount_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.lblPrice.Text = "Adjusting the lighting will cost " + this.price + " " + Items.ItemHelper.Items[1].Name + ".";
        }

        private void BtnAccept_Click(object sender, MouseButtonEventArgs e)
        {
            Messenger.SendDarknessRequest(this.nudAmount.Value);
            MenuSwitcher.CloseAllMenus();
            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            MenuSwitcher.CloseAllMenus();
            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }
    }
}
