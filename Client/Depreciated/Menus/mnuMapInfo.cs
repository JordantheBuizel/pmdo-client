﻿// <copyright file="mnuMapInfo.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuMapInfo : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblMapName;

        public MnuMapInfo(string name)
            : base(name)
            {
            this.Size = new Size(420, 90);
            this.MenuDirection = Enums.MenuDirection.Horizontal;
            this.Location = new Point(190, 60);

            this.lblMapName = new Label("lblMapName");
            this.lblMapName.Size = new Size(this.Width, this.Height);

            // lblMapName.AutoSize = true;
            // lblMapName.Location = new Point(0, 0);
            this.lblMapName.Centered = true;
            this.lblMapName.Font = FontManager.LoadFont("PMU", 48);
            if (Windows.WindowSwitcher.GameWindow.MapViewer.ActiveMap != null)
            {
                this.lblMapName.Text = Windows.WindowSwitcher.GameWindow.MapViewer.ActiveMap.Name;
            }
else
            {
                this.lblMapName.Text = "Unknown Location";
            }

            this.lblMapName.ForeColor = Color.WhiteSmoke;

            this.AddWidget(this.lblMapName);
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }
    }
}