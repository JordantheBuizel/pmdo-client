﻿// <copyright file="mnuMissionBoard.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Missions;
    using Network;
    using Widges;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class MnuMissionBoard : BorderedPanel, Core.IMenu
    {
        private const int MAXITEMS = 7;

        private readonly MenuItemPicker itemPicker;
        private readonly Label lblJobList;
        private readonly Label lblLoading;
        private List<Job> jobs;
        private readonly MissionTitle[] items;

        public MnuMissionBoard(string name, string[] parse)
            : base(name)
            {
            this.Size = new Size(280, 460);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(15, 10);

            this.itemPicker = new MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(20, 64);

            this.lblJobList = new Label("lblJobList");
            this.lblJobList.AutoSize = true;
            this.lblJobList.Font = FontManager.LoadFont("PMU", 48);
            this.lblJobList.ForeColor = Color.WhiteSmoke;
            this.lblJobList.Text = "Missions";
            this.lblJobList.Location = new Point(20, 0);

            this.lblLoading = new Label("lblLoading");
            this.lblLoading.Location = new Point(10, 50);
            this.lblLoading.Font = FontManager.LoadFont("PMU", 16);
            this.lblLoading.AutoSize = true;
            this.lblLoading.Text = "Loading...";
            this.lblLoading.ForeColor = Color.WhiteSmoke;

            this.items = new MissionTitle[8];
            int lastY = 58;

            for (int i = 0; i < this.items.Length; i++)
            {
                this.items[i] = new MissionTitle("item" + i, this.Width);
                this.items[i].Location = new Point(15, lastY);

                this.AddWidget(this.items[i]);

                lastY += this.items[i].Height + 8;
            }

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblJobList);

            // this.AddWidget(lblLoading);
            this.LoadMissionsFromPacket(parse);
        }

        /// <inheritdoc/>
        public BorderedPanel MenuPanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        public void LoadMissionsFromPacket(string[] parse)
        {
            int count = parse[1].ToInt();
            int n = 2;
            this.jobs = new List<Job>();
            for (int i = 0; i < count; i++)
            {
                    Job job = new Job();
                    job.Title = parse[n];
                    job.Summary = parse[n + 1];
                    job.GoalName = parse[n + 2];
                    job.ClientSpecies = parse[n + 3].ToInt();
                    job.ClientForm = parse[n + 4].ToInt();
                    job.MissionType = (Enums.MissionType)parse[n + 5].ToInt();
                    job.Data1 = parse[n + 6].ToInt();
                    job.Data2 = parse[n + 7].ToInt();
                    job.Difficulty = (Enums.JobDifficulty)parse[n + 8].ToInt();
                    job.RewardNum = parse[n + 9].ToInt();
                    job.RewardAmount = parse[n + 10].ToInt();
                this.jobs.Add(job);

                n += 12;
            }

            this.RefreshMissionList();
        }

        public void RemoveJob(int index)
        {
            this.jobs.RemoveAt(index);
            this.RefreshMissionList();
        }

        public void AddJob(string[] parse)
        {
            if (this.jobs.Count >= 8)
            {
                this.jobs.RemoveAt(this.jobs.Count - 1);
            }

            int n = 1;
            Job job = new Job();
            job.Title = parse[n];
            job.Summary = parse[n + 1];
            job.GoalName = parse[n + 2];
            job.ClientSpecies = parse[n + 3].ToInt();
            job.ClientForm = parse[n + 4].ToInt();
            job.MissionType = (Enums.MissionType)parse[n + 5].ToInt();
            job.Data1 = parse[n + 6].ToInt();
            job.Data2 = parse[n + 7].ToInt();
            job.Difficulty = (Enums.JobDifficulty)parse[n + 8].ToInt();
            job.RewardNum = parse[n + 9].ToInt();
            job.RewardAmount = parse[n + 10].ToInt();

            // job.Mugshot = parse[n + 11].ToInt();
            this.jobs.Insert(0, job);
            this.RefreshMissionList();
        }

        private void RefreshMissionList()
        {
            for (int i = 0; i < this.items.Length; i++)
            {
                if (this.jobs.Count > i)
                {
                    this.items[i].SetJob(this.jobs[i]);
                }
else
                {
                    this.items[i].SetJob(null);
                }
            }

            this.ReloadJobDescription();
        }

        private void ReloadJobDescription()
        {
            Core.IMenu mnuJobDesc = Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuJobDescription");
            Job job = null;
            if (this.itemPicker.SelectedItem < this.jobs.Count)
            {
                job = this.jobs[this.itemPicker.SelectedItem];
            }

            if (mnuJobDesc == null)
            {
                Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuJobDescription("mnuJobDescription", job, true));
            }
else
            {
                ((MnuJobDescription)mnuJobDesc).UpdateJob(job);
            }
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(20, 64 + ((this.items[0].Height + 8) * itemNum));
            this.itemPicker.SelectedItem = itemNum;
            this.ReloadJobDescription();
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem);
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum)
        {
            if (this.jobs.Count > itemNum)
            {
                Messenger.SendAcceptMission(itemNum);

                // Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
            }
        }

        // List<Mission> missions;
        // PictureBox picCreator;
        // Label lblCreatorName;
        // Label lblMissionName;
        // Label lblGoal;
        // Label lblDifficulty;
        // Label lblMissionCompletionEvent;
        // Label lblMissionReward;
        // Label lblSlot;

        // Button btnAccept;
        // Button btnClose;

        // public mnuMissionBoard(string name, string[] parse)
        //    : base(name) {

        // base.Size = new Size(300, 300);
        //    base.MenuDirection = Enums.MenuDirection.Vertical;
        //    base.Location = Logic.Graphic.DrawingSupport.GetCenter(Windows.WindowSwitcher.GameWindow.MapViewer.Size, this.Size);

        // picCreator = new PictureBox("picCreator");
        //    picCreator.Size = new Size(40, 40);
        //    picCreator.Location = new Point(30, 20);
        //    picCreator.BorderStyle = SdlDotNet.Widgets.BorderStyle.FixedSingle;
        //    picCreator.BorderWidth = 1;
        //    //picCreator.Image = Logic.Graphic.GraphicsManager.GetMugshot(missionInfo.CreatorMugshot).Sheet;//Tools.CropImage(Logic.Graphic.GraphicsManager.Speakers, new Rectangle((missionInfo.CreatorMugshot % 15) * 40, string.Empty, 40, 40));

        // lblCreatorName = new Label("lblCreatorName");
        //    lblCreatorName.Font = FontManager.LoadFont("tahoma", 16);
        //    lblCreatorName.AutoSize = true;
        //    //lblCreatorName.Text = "Client: " + missionInfo.CreatorName;
        //    lblCreatorName.ForeColor = Color.WhiteSmoke;
        //    lblCreatorName.Location = new Point(picCreator.X + picCreator.Width + 10, picCreator.Y);

        // lblDifficulty = new Label("lblDifficulty");
        //    lblDifficulty.Font = FontManager.LoadFont("tahoma", 16);
        //    lblDifficulty.AutoSize = true;
        //    //lblDifficulty.Text = "Difficulty: " + missionInfo.Difficulty;
        //    lblDifficulty.ForeColor = Color.WhiteSmoke;
        //    lblDifficulty.Location = new Point(picCreator.X + picCreator.Width + 10, picCreator.Y + lblCreatorName.Height + 5);

        // lblMissionName = new Label("lblMissionName");
        //    lblMissionName.Font = FontManager.LoadFont("tahoma", 14);
        //    lblMissionName.Size = new Size(this.Width - 20, 40);
        //    //lblMissionName.Text = "Name: " + missionInfo.Name;
        //    lblMissionName.ForeColor = Color.WhiteSmoke;
        //    lblMissionName.Location = new Point(picCreator.X, picCreator.Y + picCreator.Height + 10);

        // lblGoal = new Label("lblGoal");
        //    lblGoal.Font = FontManager.LoadFont("tahoma", 14);
        //    lblGoal.Size = new Size(this.Width - 20, 40);
        //    //lblGoal.Text = "Location: " + missionInfo.Goal;
        //    lblGoal.ForeColor = Color.WhiteSmoke;
        //    lblGoal.Location = new Point(lblMissionName.X, lblMissionName.Y + lblMissionName.Height + 5);

        // btnAccept = new Button("btnAccept");
        //    btnAccept.Font = FontManager.LoadFont("tahoma", 12);
        //    btnAccept.Size = new Size(100, 20);
        //    btnAccept.Text = "Take Job";
        //    btnAccept.Location = new Point(Logic.Graphic.Graphic.DrawingSupport.GetCenterX(this.Width, btnAccept.Width) - (btnAccept.Width / 2), this.Height - 40);
        //    Skins.SkinManager.LoadButtonGui(btnAccept);
        //    //btnAccept.Click += new EventHandler<MouseButtonEventArgs>(btnAccept_Click);

        // btnClose = new Button("btnClose");
        //    btnClose.Font = FontManager.LoadFont("tahoma", 12);
        //    btnClose.Size = new Size(100, 20);
        //    btnClose.Text = "Close";
        //    btnClose.Location = new Point(Logic.Graphic.Graphic.DrawingSupport.GetCenterX(this.Width, btnClose.Width) + (btnClose.Width / 2), this.Height - 40);
        //    Skins.SkinManager.LoadButtonGui(btnClose);
        //    //btnClose.Click += new EventHandler<MouseButtonEventArgs>(btnClose_Click);

        // lblSlot = new Label("lblSlot");
        //    lblSlot.Font = FontManager.LoadFont("tahoma", 12);
        //    lblSlot.AutoSize = true;
        //    //lblSlot.Text = "Slot: " + missionInfo.Slot;
        //    lblSlot.ForeColor = Color.WhiteSmoke;
        //    lblSlot.Location = new Point(btnAccept.X - lblSlot.Width, btnAccept.Y);

        // lblMissionCompletionEvent = new Label("lblMissionCompletionEvent");
        //    lblMissionCompletionEvent.Font = FontManager.LoadFont("tahoma", 14);
        //    lblMissionCompletionEvent.Size = new Size(this.Width - 20, 40);
        //    //lblMissionCompletionEvent.Text = "Goal: " + missionInfo.CompletionEvent;
        //    lblMissionCompletionEvent.ForeColor = Color.WhiteSmoke;
        //    lblMissionCompletionEvent.Location = new Point(lblGoal.X, lblGoal.Y + lblGoal.Height + 5);

        // lblMissionReward = new Label("lblMissionReward");
        //    lblMissionReward.Font = FontManager.LoadFont("tahoma", 14);
        //    lblMissionReward.Size = new Size(this.Width - 20, 40);
        //    //lblMissionReward.Text = "Reward: " + missionInfo.Reward;
        //    lblMissionReward.ForeColor = Color.WhiteSmoke;
        //    lblMissionReward.Location = new Point(lblMissionCompletionEvent.X, lblMissionCompletionEvent.Y + lblMissionCompletionEvent.Height + 5);

        // this.AddWidget(picCreator);
        //    this.AddWidget(lblCreatorName);
        //    this.AddWidget(lblDifficulty);
        //    this.AddWidget(lblMissionName);
        //    this.AddWidget(lblGoal);
        //    this.AddWidget(btnAccept);
        //    this.AddWidget(btnClose);
        //    //this.AddWidget(lblSlot);
        //    this.AddWidget(lblMissionCompletionEvent);
        //    this.AddWidget(lblMissionReward);

        // LoadMissionsFromPacket(parse);

        // }

        // void btnClose_Click(object sender, MouseButtonEventArgs e) {
        //    MenuSwitcher.CloseAllMenus();
        // }

        // void btnAccept_Click(object sender, MouseButtonEventArgs e) {
        //    Messenger.SendAcceptMission(missionInfo.Slot);
        //    MenuSwitcher.CloseAllMenus();
        // }

    }
}