﻿// <copyright file="mnuMoveOverwrite.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Moves;
    using Network;
    using Players;
    using PMU.Sockets;
    using SdlDotNet.Widgets;

    internal class MnuMoveOverwrite : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private const int MAXITEMS = 3;

        private readonly Widges.MenuItemPicker itemPicker;
        private readonly Label lblMove1;
        private readonly Label lblMove2;
        private readonly Label lblMove3;
        private readonly Label lblMove4;

        public MnuMoveOverwrite(string name)
            : base(name)
            {
            this.Size = new Size(170, 178);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 23);

            this.lblMove1 = new Label("lblMove1");
            this.lblMove1.AutoSize = true;
            this.lblMove1.Location = new Point(30, 8);
            this.lblMove1.Font = FontManager.LoadFont("PMU", 32);
            this.lblMove1.Text = PlayerManager.MyPlayer.Moves[0].MoveNum > 0 ? MoveHelper.Moves[PlayerManager.MyPlayer.Moves[0].MoveNum].Name : "----"; ;
            this.lblMove1.HoverColor = Color.Red;
            this.lblMove1.ForeColor = Color.WhiteSmoke;
            this.lblMove1.Click += new EventHandler<MouseButtonEventArgs>(this.LblMove1_Click);

            this.lblMove2 = new Label("lblMove2");
            this.lblMove2.AutoSize = true;
            this.lblMove2.Location = new Point(30, 38);
            this.lblMove2.Font = FontManager.LoadFont("PMU", 32);
            this.lblMove2.Text = PlayerManager.MyPlayer.Moves[1].MoveNum > 0 ? MoveHelper.Moves[PlayerManager.MyPlayer.Moves[1].MoveNum].Name : "----"; ;
            this.lblMove2.HoverColor = Color.Red;
            this.lblMove2.ForeColor = Color.WhiteSmoke;
            this.lblMove2.Click += new EventHandler<MouseButtonEventArgs>(this.LblMove2_Click);

            this.lblMove3 = new Label("lblMove3");
            this.lblMove3.AutoSize = true;
            this.lblMove3.Location = new Point(30, 68);
            this.lblMove3.Font = FontManager.LoadFont("PMU", 32);
            this.lblMove3.Text = PlayerManager.MyPlayer.Moves[2].MoveNum > 0 ? MoveHelper.Moves[PlayerManager.MyPlayer.Moves[2].MoveNum].Name : "----"; ;
            this.lblMove3.HoverColor = Color.Red;
            this.lblMove3.ForeColor = Color.WhiteSmoke;
            this.lblMove3.Click += new EventHandler<MouseButtonEventArgs>(this.LblMove3_Click);

            this.lblMove4 = new Label("lblMove4");
            this.lblMove4.AutoSize = true;
            this.lblMove4.Location = new Point(30, 98);
            this.lblMove4.Font = FontManager.LoadFont("PMU", 32);
            this.lblMove4.Text = PlayerManager.MyPlayer.Moves[3].MoveNum > 0 ? MoveHelper.Moves[PlayerManager.MyPlayer.Moves[3].MoveNum].Name : "----";
            this.lblMove4.HoverColor = Color.Red;
            this.lblMove4.ForeColor = Color.WhiteSmoke;
            this.lblMove4.Click += new EventHandler<MouseButtonEventArgs>(this.LblMove4_Click);

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblMove1);
            this.AddWidget(this.lblMove2);
            this.AddWidget(this.lblMove3);
            this.AddWidget(this.lblMove4);
        }

        private void LblMove1_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(0);
        }

        private void LblMove2_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1);
        }

        private void LblMove3_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(2);
        }

        private void LblMove4_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(3);
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 23 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem);
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum)
        {
            switch (itemNum)
            {
                case 0:
                {
                        Messenger.SendPacket(TcpPacket.CreatePacket("overwritemove", "0"));
                    }

                    break;
                case 1:
                {
                        Messenger.SendPacket(TcpPacket.CreatePacket("overwritemove", "1"));
                    }

                    break;
                case 2:
                {
                        Messenger.SendPacket(TcpPacket.CreatePacket("overwritemove", "2"));
                    }

                    break;
                case 3:
                {
                        Messenger.SendPacket(TcpPacket.CreatePacket("overwritemove", "3"));
                    }

                    break;
            }

            MenuSwitcher.CloseAllMenus();
        }
    }
}
