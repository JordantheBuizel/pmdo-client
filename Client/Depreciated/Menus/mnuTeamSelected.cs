﻿// <copyright file="mnuTeamSelected.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuTeamSelected : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private int teamSlot;

        private readonly Label lblSwitch;
        private readonly Label lblSendHome;
        private readonly Label lblSummary;

        private readonly Widges.MenuItemPicker itemPicker;
        private const int MAXITEMS = 1;

        public int TeamSlot
        {
            get { return this.teamSlot; }

            set
            {
                this.teamSlot = value;
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuTeamSelected(string name, int teamSlot)
            : base(name)
            {
            this.Size = new Size(165, 125);
            this.MenuDirection = Enums.MenuDirection.Horizontal;
            this.Location = new Point(300, 34);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 23);

            this.lblSwitch = new Label("lblSwitch")
            {
                Font = FontManager.LoadFont("PMU", 32),
                AutoSize = true,
                Text = "Switch",
                Location = new Point(30, 08),
                HoverColor = Color.Red,
                ForeColor = Color.WhiteSmoke
            };
            this.lblSwitch.Click += new EventHandler<MouseButtonEventArgs>(this.LblSwitch_Click);

            this.lblSendHome = new Label("lblSendHome")
            {
                Font = FontManager.LoadFont("PMU", 32),
                AutoSize = true,
                Text = "Send Home",
                Location = new Point(30, 38),
                HoverColor = Color.Red,
                ForeColor = Color.WhiteSmoke
            };
            this.lblSendHome.Click += new EventHandler<MouseButtonEventArgs>(this.LblSendHome_Click);

            this.lblSummary = new Label("lblSummary")
            {
                Font = FontManager.LoadFont("PMU", 32),
                AutoSize = true,
                Text = "Summary",
                Location = new Point(30, 68),
                HoverColor = Color.Red,
                ForeColor = Color.WhiteSmoke
            };
            this.lblSummary.Click += new EventHandler<MouseButtonEventArgs>(this.LblSummary_Click);

            this.AddWidget(this.lblSwitch);
            this.AddWidget(this.lblSendHome);

            this.AddWidget(this.itemPicker);

            this.TeamSlot = teamSlot;
        }

        private void LblSummary_Click(object sender, MouseButtonEventArgs e)
        {
#if DEBUG
            System.Threading.Thread t = new System.Threading.Thread(() => RunSummary(teamSlot));
            t.SetApartmentState(System.Threading.ApartmentState.STA); //Set the thread to STA
            t.Start();
#endif

        }

        private void LblSwitch_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1, this.teamSlot);
        }

        private void LblSendHome_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(2, this.teamSlot);
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 23 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                    {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
                        else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                    {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
                        else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                    {
                        this.SelectItem(this.itemPicker.SelectedItem, this.teamSlot);
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                    {
                        this.CloseMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum, int teamSlot)
        {
            switch (itemNum)
            {
                // case 0:
                    // { // Make Leader
                        // Players.PlayerManager.MyPlayer.LeaderSwap(teamSlot);
                        // CloseMenu();
                    // }
                    // break;
                case 0:
                    { // Switch
                        Players.PlayerManager.MyPlayer.CharSwap(teamSlot);
                        this.CloseMenu();
                    }

                    break;
                case 1:
                    { // Send Home
                        Players.PlayerManager.MyPlayer.SendHome(teamSlot);
                        this.CloseMenu();
                    }

                    break;
            }
        }

        private void RunSummary(int Slot)
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.Run(new MnuRecruitSummary(Slot));
        }

        private void CloseMenu()
        {
            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(this);
            Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuTeam");
        }
    }
}
