﻿// <copyright file="mnuTeam.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuTeam : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private const int MAXITEMS = 3;

        private readonly Widges.MenuItemPicker itemPicker;
        private readonly Label lblPoke;
        private readonly Label[] lblAllPoke;

        /*Label lblPoke1;
        Label lblPoke2;
        Label lblPoke3;
        Label lblPoke4;
        */

        public MnuTeam(string name)
            : base(name)
            {
            this.Size = new Size(280, 188);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 63);

            this.lblPoke = new Label("lblPoke");
            this.lblPoke.Location = new Point(20, 0);
            this.lblPoke.AutoSize = true;
            this.lblPoke.Font = FontManager.LoadFont("PMU", 48);
            this.lblPoke.ForeColor = Color.WhiteSmoke;
            this.lblPoke.Text = "Team";

            this.lblAllPoke = new Label[4];
            for (int i = 0; i < 4; i++)
            {
                this.lblAllPoke[i] = new Label("lblAllPoke" + i);
                this.lblAllPoke[i].AutoSize = true;
                this.lblAllPoke[i].Font = FontManager.LoadFont("PMU", 32);
                this.lblAllPoke[i].Location = new Point(30, (i * 30) + 48);
                this.lblAllPoke[i].HoverColor = Color.Red;
                this.lblAllPoke[i].ForeColor = Color.WhiteSmoke;
                this.lblAllPoke[i].Click += new EventHandler<MouseButtonEventArgs>(this.Team_Click);
                this.AddWidget(this.lblAllPoke[i]);
            }

            /*
            lblPoke1 = new Label("lblPoke1");
            lblPoke1.AutoSize = true;
            lblPoke1.Location = new Point(30, 48);
            lblPoke1.Font = FontManager.LoadFont("PMU", 32);
            lblPoke1.Text = "Pokemon 1";
            lblPoke1.HoverColor = Color.Red;

            lblPoke2 = new Label("lblPoke2");
            lblPoke2.AutoSize = true;
            lblPoke2.Location = new Point(30, 78);
            lblPoke2.Font = FontManager.LoadFont("PMU", 32);
            lblPoke2.Text = "Pokemon 2";
            lblPoke2.HoverColor = Color.Red;

            lblPoke3 = new Label("lblPoke3");
            lblPoke3.AutoSize = true;
            lblPoke3.Location = new Point(30, 108);
            lblPoke3.Font = FontManager.LoadFont("PMU", 32);
            lblPoke3.Text = "Pokemon 3";
            lblPoke3.HoverColor = Color.Red;

            lblPoke4 = new Label("lblPoke4");
            lblPoke4.AutoSize = true;
            lblPoke4.Location = new Point(30, 138);
            lblPoke4.Font = FontManager.LoadFont("PMU", 32);
            lblPoke4.Text = "Pokemon 4";
            lblPoke4.HoverColor = Color.Red;
            */

            this.AddWidget(this.itemPicker);

            this.AddWidget(this.lblPoke);
            /*this.AddWidget(lblPoke1);
            this.AddWidget(lblPoke2);
            this.AddWidget(lblPoke3);
            this.AddWidget(lblPoke4);
            */

            this.DisplayTeam();

            this.ChangeSelected(0);
        }

        private void Team_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(Array.IndexOf(this.lblAllPoke, sender));
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 63 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem);
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                        MenuSwitcher.ShowMainMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum)
        {
            if (!string.IsNullOrEmpty(Players.PlayerManager.MyPlayer.Team[itemNum].Name))
            {
                Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuTeamSelected("mnuTeamSelected", itemNum));
                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuTeamSelected");
                Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
            }
        }

        public void DisplayTeam()
        {
            for (int i = 0; i < 4; i++)
            {
                if (!string.IsNullOrEmpty(Players.PlayerManager.MyPlayer.Team[i].Name))
                {
                    this.lblAllPoke[i].Text = Players.PlayerManager.MyPlayer.Team[i].Name;
                }
else
                {
                    this.lblAllPoke[i].Text = "-----";

                    // lblAllMovesPP[i].Text = "--/--";
                }

                /*lblPoke1.Text = Players.PlayerManager.Players.GetMyPlayerRecruit(0).Name;
                lblPoke2.Text = Players.PlayerManager.Players.GetMyPlayerRecruit(1).Name;
                lblPoke3.Text = Players.PlayerManager.Players.GetMyPlayerRecruit(2).Name;
                lblPoke4.Text = Players.PlayerManager.Players.GetMyPlayerRecruit(3).Name;*/
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }
    }
}
