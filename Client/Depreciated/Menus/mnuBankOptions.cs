﻿// <copyright file="mnuBankOptions.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuBankOptions : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private const int MAXITEMS = 1;

        private readonly Widges.MenuItemPicker itemPicker;
        private readonly Label lblDeposit;
        private readonly Label lblWithdraw;

        public MnuBankOptions(string name)
            : base(name)
        {
            this.Size = new Size(155, 88);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 23);

            this.lblDeposit = new Label("lblDeposit");
            this.lblDeposit.AutoSize = true;
            this.lblDeposit.Location = new Point(30, 8);
            this.lblDeposit.Font = FontManager.LoadFont("PMU", 32);
            this.lblDeposit.Text = "Store";
            this.lblDeposit.HoverColor = Color.Red;
            this.lblDeposit.ForeColor = Color.WhiteSmoke;
            this.lblDeposit.Click += new EventHandler<MouseButtonEventArgs>(this.LblDeposit_Click);

            this.lblWithdraw = new Label("lblWithdraw");
            this.lblWithdraw.AutoSize = true;
            this.lblWithdraw.Location = new Point(30, 38);
            this.lblWithdraw.Font = FontManager.LoadFont("PMU", 32);
            this.lblWithdraw.Text = "Take";
            this.lblWithdraw.HoverColor = Color.Red;
            this.lblWithdraw.ForeColor = Color.WhiteSmoke;
            this.lblWithdraw.Click += new EventHandler<MouseButtonEventArgs>(this.LblWithdraw_Click);

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblDeposit);
            this.AddWidget(this.lblWithdraw);
        }

        private void LblDeposit_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(0);
        }

        private void LblWithdraw_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1);
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 23 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                    {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
                        else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                    {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
                        else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                    {
                        this.SelectItem(this.itemPicker.SelectedItem);
                    }

                    break;
            }
        }

        private void SelectItem(int itemNum)
        {
            switch (itemNum)
            {
                case 0:
                    {
                        MenuSwitcher.ShowBankDepositMenu(1);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case 1:
                    {
                        MenuSwitcher.ShowBankWithdrawMenu(0);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
            }
        }
    }
}