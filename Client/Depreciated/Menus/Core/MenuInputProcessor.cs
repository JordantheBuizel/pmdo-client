﻿// <copyright file="MenuInputProcessor.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus.Core
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ExpKit.Modules;
    using SdlInput = SdlDotNet.Input;

    internal class MenuInputProcessor
    {
        public static void OnKeyDown(SdlInput.KeyboardEventArgs e)
        {
            // if (e.Key == SdlInput.Key.Escape) {
            //    if (Windows.WindowSwitcher.GameWindow.MenuManager.Visible && Windows.WindowSwitcher.GameWindow.MenuManager.HasModalMenu == false) {
            //        Windows.WindowSwitcher.GameWindow.MapViewer.Focus();
            //        Windows.WindowSwitcher.GameWindow.MenuManager.Visible = false;
            //        Windows.WindowSwitcher.GameWindow.MenuManager.CloseOpenMenus();
            //    }
            // } else if (e.Key == SdlInput.Key.F11) {
            //    Logic.Graphic.SurfaceManager.SaveSurface(SdlDotNet.Graphics.Video.Screen, IO.Paths.StartupPath + "Screenshot.png");
            // } else {
                if (Windows.WindowSwitcher.GameWindow.MenuManager.Visible)
                {
                    Windows.WindowSwitcher.GameWindow.MenuManager.HandleKeyDown(e);
                }

            // }
        }

        public static void OnKeyUp(SdlInput.KeyboardEventArgs e)
        {
            if (e.Key == SdlInput.Key.Escape)
            {
                if (Windows.WindowSwitcher.GameWindow.MenuManager.Visible && Windows.WindowSwitcher.GameWindow.MenuManager.HasModalMenu == false)
                {
                    Windows.WindowSwitcher.GameWindow.MapViewer.Focus();
                    Windows.WindowSwitcher.GameWindow.MenuManager.Visible = false;
                    Windows.WindowSwitcher.GameWindow.MenuManager.CloseOpenMenus();
                }
            }
            else if (e.Key == SdlInput.Key.F11)
            {
                if (System.IO.Directory.Exists(IO.Paths.StartupPath + "Screenshots") == false)
                {
                    System.IO.Directory.CreateDirectory(IO.Paths.StartupPath + "Screenshots");
                }

                int openScreenshot = -1;
                for (int i = 1; i < int.MaxValue; i++)
                {
                    if (System.IO.File.Exists(IO.Paths.StartupPath + "Screenshots/Screenshot" + i + ".png") == false)
                    {
                        openScreenshot = i;
                        break;
                    }
                }

                if (openScreenshot > -1)
                {
                    Graphic.SurfaceManager.SaveSurface(SdlDotNet.Graphics.Video.Screen, IO.Paths.StartupPath + "Screenshots/Screenshot" + openScreenshot + ".png");
                    KitChat chat = (KitChat)System.Windows.Forms.Application.OpenForms["kitChat"];
                    if (chat != null)
                    {
                        chat.AppendChat("Screenshot #" + openScreenshot + " saved!", System.Drawing.Color.Yellow);
                    }
                }
            }
else
            {
                if (Windows.WindowSwitcher.GameWindow.MenuManager.Visible)
                {
                    Windows.WindowSwitcher.GameWindow.MenuManager.HandleKeyUp(e);
                }
            }
        }
    }
}