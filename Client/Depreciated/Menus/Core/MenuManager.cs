﻿// <copyright file="MenuManager.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus.Core
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using System.Threading;
    using System.Windows.Forms;
    using Extensions;

    internal class MenuManager : Panel
    {
        private IMenu activeMenu;
        private bool blockInput;
        private readonly List<IMenu> openMenus;

        private SdlDotNet.Graphics.Surface destSurf;

        // ReaderWriterLockSlim rwLock;
        public List<IMenu> OpenMenus
        {
            get { return this.openMenus; }
        }

        private bool hasModalMenu;
        private Widges.MapViewer mapViewer;

        public MenuManager(Widges.MapViewer mapViewer)
        {
            this.BackColor = Color.Transparent;
            this.blockInput = true;
            this.openMenus = new List<IMenu>();

            // rwLock = new ReaderWriterLockSlim();
            this.mapViewer = mapViewer;

            // lblTest = new Label("lblTest");
            // lblTest.Location = new Point(100, 100);
            // lblTest.AutoSize = true;
            // lblTest.Font = Graphic.FontManager.LoadFont("PMU", 24);
            // lblTest.Text = "Test Menu Opened";

            // this.AddWidget(lblTest);
        }

        public bool BlockInput
        {
            get { return this.blockInput; }
            set { this.blockInput = value; }
        }

        public bool HasModalMenu
        {
            get { return this.hasModalMenu; }
        }

        public void AddMenu(IMenu menu)
        {
            this.AddMenu(menu, false);
        }

        public void AddMenu(IMenu menu, bool modal)
        {
            if (this.openMenus.Contains(menu) == false)
            {
                menu.MenuPanel.Parent = null;

                // rwLock.EnterWriteLock();
                try
                {
                    this.openMenus.Add(menu);
                }
                finally
                {
                    // rwLock.ExitWriteLock();
                }

                menu.Modal = modal;
                if (modal)
                {
                    this.hasModalMenu = true;
                }

                // menu.MenuPanel.Alpha = 155;
                // this.AddWidget(menu.MenuPanel);
            }
        }

        public void RemoveMenu(IMenu menu)
        {
            int index = -1;

            // bool localLock = false;
            // if (!rwLock.IsUpgradeableReadLockHeld) {
            //    rwLock.EnterUpgradeableReadLock();
            //    localLock = true;
            // }
            try
            {
                index = this.openMenus.IndexOf(menu);
                if (index > -1)
                {
                    this.openMenus[index].MenuPanel.Hide();

                    // this.RemoveWidget(menu.MenuPanel.Name);
                    if (this.activeMenu == menu)
                    {
                        this.activeMenu = null;
                    }

                    // rwLock.EnterWriteLock();
                    try
                    {
                        this.openMenus.RemoveAt(index);
                    }
                    finally
                    {
                        // rwLock.ExitWriteLock();
                    }
                }
            }
            finally
            {
                // if (localLock) {
                //    rwLock.ExitUpgradeableReadLock();
                // }
            }

            if (index > -1)
            {
                this.CheckForModalMenus();
            }
        }

        private void CheckForModalMenus()
        {
            // bool localLock = false;
            // if (!rwLock.IsReadLockHeld) {
            //    rwLock.EnterReadLock();
            //    localLock = true;
            // }
            try
            {
                for (int i = 0; i < this.openMenus.Count; i++)
                {
                    if (this.openMenus[i].Modal)
                    {
                        this.hasModalMenu = true;
                        return;
                    }
                }

                this.hasModalMenu = false;
            }
            finally
            {
                // if (localLock) {
                //    rwLock.ExitReadLock();
                // }
            }
        }

        public void CloseOpenMenus()
        {
            // rwLock.EnterWriteLock();
            try
            {
                for (int i = this.openMenus.Count - 1; i >= 0; i--)
                {
                    this.openMenus[i].MenuPanel.Hide();

                    // this.RemoveWidget(openMenus[i].MenuPanel.Name);
                    this.openMenus.RemoveAt(i);
                }
            }
            finally
            {
                // rwLock.ExitWriteLock();
            }

            this.hasModalMenu = false;
            this.activeMenu = null;
        }

        public IMenu FindMenu(string menuName)
        {
            // bool localLock = false;
            // if (!rwLock.IsReadLockHeld) {
            //    rwLock.EnterReadLock();
            //    localLock = true;
            // }
            try
            {
                for (int i = 0; i < this.openMenus.Count; i++)
                {
                    if (this.openMenus[i].MenuPanel.Name == menuName)
                    {
                        return this.openMenus[i];
                    }
                }

                return null;
            }
            finally
            {
                // if (localLock) {
                //    rwLock.ExitReadLock();
                // }
            }
        }

        public void HandleKeyDown(KeyEventArgs e)
        {
            if (this.activeMenu != null)
            {
                this.activeMenu.MenuPanel.OnKeyDown(e);
            }
            else
            {
                // rwLock.EnterUpgradeableReadLock();
                try
                {
                    if (this.openMenus.Count > 0)
                    {
                        this.openMenus[0].MenuPanel.OnKeyDown(e);
                    }
                }
                finally
                {
                    // rwLock.ExitUpgradeableReadLock();
                }
            }
        }

        public void HandleKeyUp(KeyEventArgs e)
        {
            if (this.activeMenu != null)
            {
                this.activeMenu.MenuPanel.OnKeyUp(e);
            }
            else
            {
                // rwLock.EnterUpgradeableReadLock();
                try
                {
                    if (this.openMenus.Count > 0)
                    {
                        this.openMenus[0].MenuPanel.OnKeyUp(e);
                    }
                }
                finally
                {
                    // rwLock.ExitUpgradeableReadLock();
                }
            }
        }

        public void SetActiveMenu(IMenu menu)
        {
            // rwLock.EnterReadLock();
            try
            {
                if (this.openMenus.Contains(menu))
                {
                    this.activeMenu = menu;
                }
            }
            finally
            {
                // rwLock.ExitReadLock();
            }
        }

        public void SetActiveMenu(string menuName)
        {
            IMenu menu = this.FindMenu(menuName);
            if (menu != null)
            {
                this.activeMenu = menu;
            }
        }

        /// <inheritdoc/>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            // rwLock.EnterUpgradeableReadLock();
            try
            {
                foreach (IMenu menu in this.openMenus)
                {
                    if (menu.MenuPanel.Bounds.Contains(e.Location))
                    {
                        menu.MenuPanel.OnMouseMove(e);
                        break;
                    }
                }
            }
            finally
            {
                // rwLock.ExitUpgradeableReadLock();
            }
        }

        /// <inheritdoc/>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            // rwLock.EnterUpgradeableReadLock();
            try
            {
                foreach (IMenu menu in this.openMenus)
                {
                    if (menu.MenuPanel.Bounds.Contains(e.Location))
                    {
                        menu.MenuPanel.OnMouseDown(e);
                        break;
                    }
                }
            }
            finally
            {
                // rwLock.ExitUpgradeableReadLock();
            }
        }

        /// <inheritdoc/>
        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            // rwLock.EnterUpgradeableReadLock();
            try
            {
                foreach (IMenu menu in this.openMenus)
                {
                    if (menu.MenuPanel.Bounds.Contains(e.Location))
                    {
                        menu.MenuPanel.OnMouseUp(e);
                        break;
                    }
                }
            }
            finally
            {
                // rwLock.ExitUpgradeableReadLock();
            }
        }
    }
}