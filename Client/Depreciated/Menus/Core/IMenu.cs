﻿// <copyright file="IMenu.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus.Core
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Extensions;

    internal interface IMenu
    {
        Widges.BorderedPanel MenuPanel { get; }

        bool Modal { get; set; }
    }
}
