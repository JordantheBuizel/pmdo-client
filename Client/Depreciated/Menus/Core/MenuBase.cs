﻿// <copyright file="MenuBase.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus.Core
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using System.Windows.Forms;

    internal class MenuBase : Panel
    {
        private Enums.MenuDirection menuDirection;

        public MenuBase()
        {
            this.BackColor = Color.Transparent;
        }

        public Enums.MenuDirection MenuDirection
        {
            get { return this.menuDirection; }

            set
            {
                this.menuDirection = value;
                switch (this.menuDirection)
                {
                    case Enums.MenuDirection.Horizontal:
                        {
                            this.BackgroundImage = GraphicsCache.MenuHorizontal.ToBitmap(true);
                        }

                        break;
                    case Enums.MenuDirection.Vertical:
                        {
                            this.BackgroundImage = GraphicsCache.MenuVertical.ToBitmap(true);
                        }

                        break;
                }
            }
        }
    }
}