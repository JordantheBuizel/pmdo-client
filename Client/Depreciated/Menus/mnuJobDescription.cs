﻿// <copyright file="mnuJobDescription.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Missions;
    using Network;
    using SdlDotNet.Widgets;

    internal class MnuJobDescription : Widges.BorderedPanel, Core.IMenu
    {
        private readonly Label lblDescription;
        private readonly PictureBox picCreator;
        private readonly Label lblCreatorName;
        private readonly Label lblNullTitle;
        private readonly Label lblTitle;
        private readonly Label lblSummary;
        private readonly Label lblObjective;
        private readonly Label lblGoal;
        private readonly Label lblDifficulty;
        private readonly PictureBox picReward;
        private readonly Label lblReward;
        private readonly Label lblPressEnter;

        public MnuJobDescription(string name, Job job, bool missionBoardView)
            : base(name)
            {
            this.Size = new Size(300, 460);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            if (missionBoardView)
            {
                this.Location = new Point(305, 10);
            }
else
            {
                this.Location = new Point(160, 10);
            }

            this.lblDescription = new Label("lblDescription");
            this.lblDescription.Font = FontManager.LoadFont("PMU", 48);
            this.lblDescription.AutoSize = true;
            this.lblDescription.Text = "Description";
            this.lblDescription.ForeColor = Color.WhiteSmoke;
            this.lblDescription.Location = new Point(20, 0);

            this.lblNullTitle = new Label("lblNullTitle");
            this.lblNullTitle.Font = FontManager.LoadFont("PMU", 32);
            this.lblNullTitle.Size = new Size(this.Width - 20, 40);
            this.lblNullTitle.ForeColor = Color.Gray;
            this.lblNullTitle.Text = "-----";
            this.lblNullTitle.Location = new Point(20, 50);

            this.picCreator = new PictureBox("picCreator");
            this.picCreator.Size = new Size(40, 40);
            this.picCreator.Location = new Point(30, 50);
            this.picCreator.BorderStyle = SdlDotNet.Widgets.BorderStyle.FixedSingle;
            this.picCreator.BorderWidth = 1;

            this.lblTitle = new Label("lblTitle");
            this.lblTitle.Font = FontManager.LoadFont("PMU", 16);
            this.lblTitle.AutoSize = true;
            this.lblTitle.ForeColor = Color.WhiteSmoke;
            this.lblTitle.Location = new Point(this.picCreator.X + this.picCreator.Width + 10, this.picCreator.Y);

            this.lblCreatorName = new Label("lblCreatorName");
            this.lblCreatorName.Font = FontManager.LoadFont("PMU", 16);
            this.lblCreatorName.AutoSize = true;
            this.lblCreatorName.ForeColor = Color.WhiteSmoke;
            this.lblCreatorName.Location = new Point(this.picCreator.X + this.picCreator.Width + 10, this.picCreator.Y + 20);

            this.lblSummary = new Label("lblSummary");
            this.lblSummary.Font = FontManager.LoadFont("PMU", 16);
            this.lblSummary.Size = new Size(this.Width - 50, 120);
            this.lblSummary.ForeColor = Color.WhiteSmoke;
            this.lblSummary.Location = new Point(this.lblNullTitle.X, this.picCreator.Y + this.picCreator.Height + 4);

            this.lblObjective = new Label("lblObjective");
            this.lblObjective.Font = FontManager.LoadFont("PMU", 16);
            this.lblObjective.Size = new Size(this.Width - 20, 40);
            this.lblObjective.ForeColor = Color.WhiteSmoke;
            this.lblObjective.Location = new Point(this.lblNullTitle.X, this.lblNullTitle.Y + this.lblNullTitle.Height + 5);

            this.lblGoal = new Label("lblGoal");
            this.lblGoal.Font = FontManager.LoadFont("PMU", 16);
            this.lblGoal.Size = new Size(this.Width - 20, 40);
            this.lblGoal.ForeColor = Color.WhiteSmoke;
            this.lblGoal.Location = new Point(this.lblNullTitle.X, this.lblSummary.Y + this.lblSummary.Height + 5);

            this.lblDifficulty = new Label("lblDifficulty");
            this.lblDifficulty.Font = FontManager.LoadFont("PMU", 16);
            this.lblDifficulty.AutoSize = true;
            this.lblDifficulty.ForeColor = Color.WhiteSmoke;
            this.lblDifficulty.Location = new Point(this.lblNullTitle.X, this.lblGoal.Y + 20);

            this.picReward = new PictureBox("picReward");
            this.picReward.Size = new Size(32, 32);
            this.picReward.BackColor = Color.Transparent;
            this.picReward.Location = new Point(this.lblNullTitle.X, this.lblDifficulty.Y + 24);

            this.lblReward = new Label("lblMissionReward");
            this.lblReward.Font = FontManager.LoadFont("PMU", 16);
            this.lblReward.Size = new Size(this.Width - 20, 40);
            this.lblReward.ForeColor = Color.WhiteSmoke;
            this.lblReward.Location = new Point(this.lblNullTitle.X + 32, this.lblDifficulty.Y + 20);

            this.lblPressEnter = new Label("lblPressEnter");
            this.lblPressEnter.Font = FontManager.LoadFont("PMU", 32);
            this.lblPressEnter.Size = new Size(this.Width - 20, 40);
            this.lblPressEnter.ForeColor = Color.WhiteSmoke;
            this.lblPressEnter.Text = "Press Enter to take this job.";
            this.lblPressEnter.Location = new Point(this.lblNullTitle.X, this.Height - 50);

            this.AddWidget(this.lblDescription);
            this.AddWidget(this.picCreator);
            this.AddWidget(this.lblTitle);
            this.AddWidget(this.lblCreatorName);
            this.AddWidget(this.lblNullTitle);
            this.AddWidget(this.lblSummary);

            // this.AddWidget(lblObjective);
            this.AddWidget(this.lblGoal);
            this.AddWidget(this.lblDifficulty);
            this.AddWidget(this.picReward);
            this.AddWidget(this.lblReward);
            if (missionBoardView)
            {
                this.AddWidget(this.lblPressEnter);
            }

            this.UpdateJob(job);
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        public void UpdateJob(Job job)
        {
            if (job == null)
            {
                this.lblDifficulty.Visible = false;
                this.picCreator.Visible = false;
                this.lblCreatorName.Visible = false;
                this.lblNullTitle.Visible = true;
                this.lblTitle.Visible = false;
                this.lblSummary.Visible = false;
                this.lblGoal.Visible = false;
                this.picReward.Visible = false;
                this.lblReward.Visible = false;
                this.lblPressEnter.Visible = false;
            }
else
            {
                this.lblDifficulty.Visible = true;
                this.lblDifficulty.Text = "Difficulty: " + MissionManager.DifficultyToString(job.Difficulty) + " (" + MissionManager.DetermineMissionExpReward(job.Difficulty) + " Explorer Points)";
                this.picCreator.Visible = true;
                this.picCreator.Image = GraphicsManager.GetMugshot(job.ClientSpecies, string.Empty, 0, 0).GetEmote(0); // Tools.CropImage(Logic.Graphic.GraphicsManager.Speakers, new Rectangle((this.creatorMugshot % 15) * 40, string.Empty, 40, 40));
                this.lblTitle.Visible = true;
                this.lblTitle.Text = "Title: " + job.Title;
                this.lblCreatorName.Visible = true;
                this.lblCreatorName.Text = "From: " + Pokedex.PokemonHelper.Pokemon[job.ClientSpecies - 1].Name;
                this.lblNullTitle.Visible = false;
                this.lblSummary.Visible = true;
                this.lblSummary.Text = "Summary: \n" + job.Summary;

                this.lblGoal.Visible = true;
                this.lblGoal.Text = "Place: " + job.GoalName;
                this.picReward.Visible = true;
                this.picReward.Image = Tools.CropImage(GraphicsManager.Items, new Rectangle((Items.ItemHelper.Items[job.RewardNum].Pic - ((int)(Items.ItemHelper.Items[job.RewardNum].Pic / 6) * 6)) * Constants.TILEWIDTH, (int)(Items.ItemHelper.Items[job.RewardNum].Pic / 6) * Constants.TILEWIDTH, Constants.TILEWIDTH, Constants.TILEHEIGHT));
                this.lblReward.Visible = true;
                if (Items.ItemHelper.Items[job.RewardNum].StackCap > 0)
                {
                    this.lblReward.Text = "Reward:\n" + job.RewardAmount + " " + Items.ItemHelper.Items[job.RewardNum].Name;
                }
else
                {
                    this.lblReward.Text = "Reward:\n" + Items.ItemHelper.Items[job.RewardNum].Name;
                }

                this.lblPressEnter.Visible = true;
            }
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.Backspace:
                {
                        MenuSwitcher.ShowJobListMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        MenuSwitcher.ShowJobListMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }
    }
}