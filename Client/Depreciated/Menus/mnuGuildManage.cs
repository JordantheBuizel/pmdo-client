﻿// <copyright file="mnuGuildManage.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class MnuGuildManage : Widges.BorderedPanel, Core.IMenu
    {
        public const int RECRUITPRICE = 1000;
        public const int PROMOTEPRICE = 10000;
        public const int CREATEPRICE = 100000;

        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private List<Enums.GuildRank> ranks;

        private readonly Label lblGuild;
        private readonly Label lblYourName;
        private readonly Button btnStepDown;
        private readonly Label lblMembers;
        private readonly ScrollingListBox lbxMembers;
        private readonly Label lblPromotePrice;
        private readonly Button btnPromote;
        private readonly Button btnDemote;
        private readonly Button btnOKAdd;
        private readonly Label lblName;
        private readonly TextBox txtName;
        private readonly Button btnCancel;

        public MnuGuildManage(string name, string[] parse)
            : base(name)
            {
            this.Size = new Size(380, 400);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(130, 50);

            this.lblGuild = new Label("lblGuild");
            this.lblGuild.Location = new Point(20, 0);
            this.lblGuild.AutoSize = true;
            this.lblGuild.Font = FontManager.LoadFont("PMU", 48);
            this.lblGuild.Text = "Manage Guild";
            this.lblGuild.ForeColor = Color.WhiteSmoke;

            this.lblYourName = new Label("lblYourName");
            this.lblYourName.Location = new Point(20, 56);
            this.lblYourName.AutoSize = true;
            this.lblYourName.Font = FontManager.LoadFont("PMU", 16);
            this.lblYourName.ForeColor = Color.WhiteSmoke;

            this.btnStepDown = new Button("btnStepDown");
            this.btnStepDown.Size = new Size(64, 24);
            this.btnStepDown.Location = new Point(260, 56);
            this.btnStepDown.Font = FontManager.LoadFont("PMU", 16);
            Skins.SkinManager.LoadButtonGui(this.btnStepDown);
            this.btnStepDown.Click += new EventHandler<MouseButtonEventArgs>(this.BtnStepDown_Click);

            this.lblMembers = new Label("lblMembers");
            this.lblMembers.Location = new Point(20, 76);
            this.lblMembers.AutoSize = true;
            this.lblMembers.Font = FontManager.LoadFont("PMU", 16);
            this.lblMembers.Text = "Guild Members:";
            this.lblMembers.ForeColor = Color.WhiteSmoke;

            this.lbxMembers = new ScrollingListBox("lbxMembers");
            this.lbxMembers.Location = new Point(20, 96);
            this.lbxMembers.Size = new Size(330, 140);
            this.lbxMembers.BackColor = Color.Transparent;
            this.lbxMembers.ItemSelected += new EventHandler(this.LbxMembers_ItemSelected);

            this.lblPromotePrice = new Label("lblPromotePrice");
            this.lblPromotePrice.Location = new Point(20, 240);
            this.lblPromotePrice.AutoSize = true;
            this.lblPromotePrice.Font = FontManager.LoadFont("PMU", 16);
            this.lblPromotePrice.ForeColor = Color.WhiteSmoke;

            this.btnPromote = new Button("btnPromote");
            this.btnPromote.Size = new Size(60, 24);
            this.btnPromote.Location = new Point(60, 260);
            this.btnPromote.Font = FontManager.LoadFont("PMU", 16);
            this.btnPromote.Text = "Promote";
            Skins.SkinManager.LoadButtonGui(this.btnPromote);
            this.btnPromote.Click += new EventHandler<MouseButtonEventArgs>(this.BtnPromote_Click);

            this.btnDemote = new Button("btnDemote");
            this.btnDemote.Size = new Size(60, 24);
            this.btnDemote.Location = new Point(240, 260);
            this.btnDemote.Font = FontManager.LoadFont("PMU", 16);
            Skins.SkinManager.LoadButtonGui(this.btnDemote);
            this.btnDemote.Click += new EventHandler<MouseButtonEventArgs>(this.BtnDemote_Click);

            this.btnOKAdd = new Button("btnOK");
            this.btnOKAdd.Size = new Size(60, 24);
            this.btnOKAdd.Location = new Point(280, 300);
            this.btnOKAdd.Font = FontManager.LoadFont("PMU", 16);
            this.btnOKAdd.Text = "Add";
            Skins.SkinManager.LoadButtonGui(this.btnOKAdd);
            this.btnOKAdd.Click += new EventHandler<MouseButtonEventArgs>(this.BtnOK_Click);

            this.lblName = new Label("lblName");
            this.lblName.Location = new Point(20, 280);
            this.lblName.AutoSize = true;
            this.lblName.Font = FontManager.LoadFont("PMU", 16);
            this.lblName.ForeColor = Color.WhiteSmoke;

            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(220, 24);
            this.txtName.Location = new Point(20, 300);
            this.txtName.Font = FontManager.LoadFont("PMU", 16);
            Skins.SkinManager.LoadTextBoxGui(this.txtName);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Size = new Size(60, 32);
            this.btnCancel.Location = new Point(150, 340);
            this.btnCancel.Font = FontManager.LoadFont("PMU", 16);
            this.btnCancel.Text = "Exit";
            Skins.SkinManager.LoadButtonGui(this.btnCancel);
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.AddWidget(this.lblGuild);
            this.AddWidget(this.lblYourName);
            this.AddWidget(this.btnStepDown);
            this.AddWidget(this.lblMembers);
            this.AddWidget(this.lbxMembers);
            this.AddWidget(this.lblPromotePrice);
            this.AddWidget(this.btnPromote);
            this.AddWidget(this.btnDemote);
            this.AddWidget(this.btnOKAdd);
            this.AddWidget(this.lblName);
            this.AddWidget(this.txtName);
            this.AddWidget(this.btnCancel);

            this.LoadGuildFromPacket(parse);
        }

        private void BtnStepDown_Click(object sender, MouseButtonEventArgs e)
        {
            Messenger.GuildStepDown();
            MenuSwitcher.CloseAllMenus();
        }

        private void BtnPromote_Click(object sender, MouseButtonEventArgs e)
        {
            Messenger.GuildPromote(this.lbxMembers.SelectedIndex);

            // MenuSwitcher.CloseAllMenus();
        }

        private void BtnDemote_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.ranks[this.lbxMembers.SelectedIndex] > Enums.GuildRank.Member)
            {
                Messenger.GuildDemote(this.lbxMembers.SelectedIndex);
            }
else
            {
                Messenger.GuildDisown(this.lbxMembers.SelectedIndex);
            }

            // MenuSwitcher.CloseAllMenus();
        }

        private void BtnOK_Click(object sender, MouseButtonEventArgs e)
        {
            Messenger.MakeGuildMember(this.txtName.Text);

            // MenuSwitcher.CloseAllMenus();
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            MenuSwitcher.CloseAllMenus();
        }

        private void LbxMembers_ItemSelected(object sender, EventArgs e)
        {
            this.UpdateOptions();
        }

        public void LoadGuildFromPacket(string[] parse)
        {
            this.ranks = new List<Enums.GuildRank>();
            this.lbxMembers.Items.Clear();

            int count = parse[1].ToInt();

            for (int i = 0; i < count; i++)
            {
                Enums.GuildRank rank = (Enums.GuildRank)parse[2 + (i * 3) + 1].ToInt();
                Color color;
                switch (rank)
                {
                    case Enums.GuildRank.Member:
                        color = Color.LightSkyBlue;
                        break;
                    case Enums.GuildRank.Admin:
                        color = Color.Yellow;
                        break;
                    case Enums.GuildRank.Founder:
                        color = Color.LawnGreen;
                        break;
                    default:
                        color = Color.Red;
                        break;
                }

                ListBoxTextItem lbiName = new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), "[" + rank + "] " + parse[2 + (i * 3)] + " (Last Login: " + parse[2 + (i * 3) + 2] + ")");
                this.ranks.Add(rank);
                lbiName.ForeColor = color;
                this.lbxMembers.Items.Add(lbiName);
            }

            this.Refresh();
        }

        public void AddMember(string name)
        {
            Color color = Color.LightSkyBlue;
            ListBoxTextItem lbiName = new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), "[" + Enums.GuildRank.Member + "] " + name + " (NEW)");
            this.ranks.Add(Enums.GuildRank.Member);
            lbiName.ForeColor = color;
            this.lbxMembers.Items.Add(lbiName);

            this.Refresh();
        }

        public void RemoveMember(int index)
        {
            this.ranks.RemoveAt(index);
            this.lbxMembers.Items.RemoveAt(index);

            this.Refresh();
        }

        public void UpdateMember(int index, Enums.GuildRank rank)
        {
            Color color;
            switch (rank)
            {
                case Enums.GuildRank.Member:
                    color = Color.LightSkyBlue;
                    break;
                case Enums.GuildRank.Admin:
                    color = Color.Yellow;
                    break;
                case Enums.GuildRank.Founder:
                    color = Color.LawnGreen;
                    break;
                default:
                    color = Color.Red;
                    break;
            }

            string name = ((ListBoxTextItem)this.lbxMembers.Items[index]).Text.Split(']')[1];
            ListBoxTextItem lbiName = new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), "[" + rank + "] " + name);
            this.ranks[index] = rank;
            ((ListBoxTextItem)this.lbxMembers.Items[index]).ForeColor = color;
            ((ListBoxTextItem)this.lbxMembers.Items[index]).Text = "[" + rank + "] " + name;

            this.Refresh();
        }

        public void Refresh()
        {
            this.lblYourName.Text = Players.PlayerManager.MyPlayer.Name + " (" + Players.PlayerManager.MyPlayer.GuildAccess + ")";

            this.UpdateOptions();

            int memberCount = 0;
            int adminCount = 0;

            foreach (Enums.GuildRank rank in this.ranks)
            {
                if (rank > Enums.GuildRank.Member)
                {
                    adminCount++;
                }

                memberCount++;
            }

            this.lblPromotePrice.Text = "Promote member: (" + (PROMOTEPRICE * adminCount) + " " + Items.ItemHelper.Items[1].Name + ")";
            this.lblName.Text = "Add member: (" + (RECRUITPRICE * memberCount) + " " + Items.ItemHelper.Items[1].Name + ")";
        }

        public void UpdateOptions()
        {
            if (Players.PlayerManager.MyPlayer.GuildAccess > Enums.GuildRank.Member)
            {
                this.btnStepDown.Text = "Step Down";
            }
else
            {
                this.btnStepDown.Text = "Leave Guild";
            }

            if (this.lbxMembers.SelectedIndex > -1)
            {
                Enums.GuildRank rank = this.ranks[this.lbxMembers.SelectedIndex];
                if (Players.PlayerManager.MyPlayer.GuildAccess == Enums.GuildRank.Founder)
                {
                    if (rank == Enums.GuildRank.Founder)
                    {
                        this.lblPromotePrice.Visible = false;
                        this.btnPromote.Visible = false;
                        this.btnDemote.Visible = false;
                    }
                    else if (rank == Enums.GuildRank.Admin)
                    {
                        this.lblPromotePrice.Visible = false;
                        this.btnPromote.Visible = false;
                        this.btnDemote.Visible = true;
                        this.btnDemote.Text = "Demote";
                    }
else
                    {
                        this.lblPromotePrice.Visible = true;
                        this.btnPromote.Visible = true;
                        this.btnDemote.Visible = true;
                        this.btnDemote.Text = "Remove";
                    }
                }
                else if (Players.PlayerManager.MyPlayer.GuildAccess == Enums.GuildRank.Admin)
                {
                    if (rank <= Enums.GuildRank.Member)
                    {
                        this.btnDemote.Visible = true;
                        this.btnDemote.Text = "Remove";
                    }
else
                    {
                        this.lblPromotePrice.Visible = false;
                        this.btnPromote.Visible = false;
                        this.btnDemote.Visible = false;
                    }
                }
else
                {
                    this.lblPromotePrice.Visible = false;
                    this.btnPromote.Visible = false;
                    this.btnDemote.Visible = false;
                }
            }
else
            {
                this.lblPromotePrice.Visible = false;
                this.btnPromote.Visible = false;
                this.btnDemote.Visible = false;
            }

            if (Players.PlayerManager.MyPlayer.GuildAccess >= Enums.GuildRank.Admin)
            {
                this.lblName.Visible = true;
                this.txtName.Visible = true;
                this.btnOKAdd.Visible = true;
            }
else
            {
                this.lblName.Visible = false;
                this.txtName.Visible = false;
                this.btnOKAdd.Visible = false;
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }
    }
}
