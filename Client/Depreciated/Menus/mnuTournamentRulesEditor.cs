﻿// <copyright file="mnuTournamentRulesEditor.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Tournaments;
    using SdlDotNet.Widgets;

    internal class MnuTournamentRulesEditor : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblEditRules;
        private TournamentRules rules;

        private readonly CheckBox chkSleepClause;
        private readonly CheckBox chkAccuracyClause;
        private readonly CheckBox chkSpeciesClause;
        private readonly CheckBox chkFreezeClause;
        private readonly CheckBox chkOHKOClause;
        private readonly CheckBox chkSelfKOClause;

        private readonly Button btnSave;

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuTournamentRulesEditor(string name, TournamentRules rules)
            : base(name)
            {
            this.rules = rules;

            this.Size = new Size(315, 360);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.lblEditRules = new Label("lblEditRules");
            this.lblEditRules.AutoSize = true;
            this.lblEditRules.Font = FontManager.LoadFont("PMU", 48);
            this.lblEditRules.Text = "Edit Rules";
            this.lblEditRules.ForeColor = Color.WhiteSmoke;
            this.lblEditRules.Location = new Point(20, 0);

            this.chkSleepClause = new CheckBox("chkSleepClause");
            this.chkSleepClause.Location = new Point(15, 48);
            this.chkSleepClause.Size = new Size(200, 32);
            this.chkSleepClause.BackColor = Color.Transparent;
            this.chkSleepClause.Font = FontManager.LoadFont("PMU", 32);
            this.chkSleepClause.ForeColor = Color.WhiteSmoke;
            this.chkSleepClause.Text = "Sleep Clause";
            this.chkSleepClause.Checked = rules.SleepClause;

            this.chkAccuracyClause = new CheckBox("chkAccuracyClause");
            this.chkAccuracyClause.Location = new Point(15, 80);
            this.chkAccuracyClause.Size = new Size(200, 32);
            this.chkAccuracyClause.BackColor = Color.Transparent;
            this.chkAccuracyClause.Font = FontManager.LoadFont("PMU", 32);
            this.chkAccuracyClause.ForeColor = Color.WhiteSmoke;
            this.chkAccuracyClause.Text = "Accuracy Clause";
            this.chkAccuracyClause.Checked = rules.AccuracyClause;

            this.chkSpeciesClause = new CheckBox("chkSpeciesClause");
            this.chkSpeciesClause.Location = new Point(15, 112);
            this.chkSpeciesClause.Size = new Size(200, 32);
            this.chkSpeciesClause.BackColor = Color.Transparent;
            this.chkSpeciesClause.Font = FontManager.LoadFont("PMU", 32);
            this.chkSleepClause.ForeColor = Color.WhiteSmoke;
            this.chkSpeciesClause.Text = "Species Clause";
            this.chkSpeciesClause.Checked = rules.SpeciesClause;

            this.chkFreezeClause = new CheckBox("chkFreezeClause");
            this.chkFreezeClause.Location = new Point(15, 144);
            this.chkFreezeClause.Size = new Size(200, 32);
            this.chkFreezeClause.BackColor = Color.Transparent;
            this.chkFreezeClause.Font = FontManager.LoadFont("PMU", 32);
            this.chkFreezeClause.ForeColor = Color.WhiteSmoke;
            this.chkFreezeClause.Text = "Freeze Clause";
            this.chkFreezeClause.Checked = rules.FreezeClause;

            this.chkOHKOClause = new CheckBox("chkOHKOClause");
            this.chkOHKOClause.Location = new Point(15, 176);
            this.chkOHKOClause.Size = new Size(200, 32);
            this.chkOHKOClause.BackColor = Color.Transparent;
            this.chkOHKOClause.Font = FontManager.LoadFont("PMU", 32);
            this.chkOHKOClause.ForeColor = Color.WhiteSmoke;
            this.chkOHKOClause.Text = "OHKO Clause";
            this.chkOHKOClause.Checked = rules.OHKOClause;

            this.chkSelfKOClause = new CheckBox("chkSelfKOClause");
            this.chkSelfKOClause.Location = new Point(15, 208);
            this.chkSelfKOClause.Size = new Size(200, 32);
            this.chkSelfKOClause.BackColor = Color.Transparent;
            this.chkSelfKOClause.Font = FontManager.LoadFont("PMU", 32);
            this.chkSelfKOClause.ForeColor = Color.WhiteSmoke;
            this.chkSelfKOClause.Text = "Self-KO Clause";
            this.chkSelfKOClause.Checked = rules.SelfKOClause;

            this.btnSave = new Button("btnSave");
            this.btnSave.Location = new Point(15, 245);
            this.btnSave.Size = new Size(100, 15);
            this.btnSave.Text = "Save";
            this.btnSave.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSave_Click);

            this.AddWidget(this.chkSleepClause);
            this.AddWidget(this.chkAccuracyClause);
            this.AddWidget(this.chkSpeciesClause);
            this.AddWidget(this.chkFreezeClause);
            this.AddWidget(this.chkOHKOClause);
            this.AddWidget(this.chkSelfKOClause);
            this.AddWidget(this.btnSave);
            this.AddWidget(this.lblEditRules);
        }

        private void BtnSave_Click(object sender, MouseButtonEventArgs e)
        {
            TournamentRules rules = new TournamentRules();
            rules.SleepClause = this.chkSleepClause.Checked;
            rules.AccuracyClause = this.chkAccuracyClause.Checked;
            rules.SpeciesClause = this.chkSpeciesClause.Checked;
            rules.FreezeClause = this.chkFreezeClause.Checked;
            rules.OHKOClause = this.chkOHKOClause.Checked;
            rules.SelfKOClause = this.chkSelfKOClause.Checked;

            Network.Messenger.SendSaveTournamentRules(rules);

            MenuSwitcher.CloseAllMenus();
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.Return:
                {
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                    }

                    break;
            }
        }
    }
}
