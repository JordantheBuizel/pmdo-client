﻿// <copyright file="mnuHelpTopics.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuHelpTopics : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblHelpTopics;
        private readonly ScrollingListBox lstHelpTopics;
        private readonly Button btnShowHelp;

        public MnuHelpTopics(string name)
            : base(name)
            {
            this.Size = new Size(185, 220);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.lblHelpTopics = new Label("lblHelpTopics");
            this.lblHelpTopics.Location = new Point(20, 0);
            this.lblHelpTopics.Font = FontManager.LoadFont("PMU", 36);
            this.lblHelpTopics.AutoSize = true;
            this.lblHelpTopics.Text = "Help Topics";
            this.lblHelpTopics.ForeColor = Color.WhiteSmoke;

            this.lstHelpTopics = new ScrollingListBox("lstHelpTopics");
            this.lstHelpTopics.Location = new Point(10, 50);
            this.lstHelpTopics.Size = new Size(this.Width - (this.lstHelpTopics.X * 2), this.Height - this.lstHelpTopics.Y - 10 - 30);
            this.lstHelpTopics.BackColor = Color.Transparent;
            this.lstHelpTopics.BorderStyle = SdlDotNet.Widgets.BorderStyle.None;

            this.btnShowHelp = new Button("btnShowHelp");
            this.btnShowHelp.Location = new Point(10, this.lstHelpTopics.Y + this.lstHelpTopics.Height + 5);
            this.btnShowHelp.Size = new Size(100, 30);
            this.btnShowHelp.Text = "Load Topic";
            Skins.SkinManager.LoadButtonGui(this.btnShowHelp);
            this.btnShowHelp.Click += new EventHandler<MouseButtonEventArgs>(this.BtnShowHelp_Click);

            this.AddWidget(this.lblHelpTopics);
            this.AddWidget(this.lstHelpTopics);
            this.AddWidget(this.btnShowHelp);

            this.LoadHelpTopics();
        }

        private void BtnShowHelp_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lstHelpTopics.SelectedItems.Count > 0)
            {
                MenuSwitcher.ShowHelpPage(((ListBoxTextItem)this.lstHelpTopics.SelectedItems[0]).Text, 0);
                Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
            }
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.Backspace:
                {
                        // Show the others menu when the backspace key is pressed
                        MenuSwitcher.ShowOthersMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        public void LoadHelpTopics()
        {
            string[] dirs = System.IO.Directory.GetDirectories(IO.Paths.StartupPath + "Help");
            for (int i = 0; i < dirs.Length; i++)
            {
                ListBoxTextItem lbi = new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), System.IO.Path.GetFileNameWithoutExtension(dirs[i]));
                lbi.ForeColor = Color.WhiteSmoke;
                this.lstHelpTopics.Items.Add(lbi);
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }
    }
}
