﻿// <copyright file="mnuMoveRecall.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using PMU.Core;
    using SdlDotNet.Widgets;

    /// <summary>
    /// Description of mnuLinkShop.
    /// </summary>
    internal class MnuMoveRecall : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private bool loaded;
        private readonly Label[] lblVisibleItems;
        private readonly Label lblItemCollection;
        private readonly Widges.MenuItemPicker itemPicker;
        public int CurrentTen;
        private readonly Label lblItemNum;

        public List<int> RecallMoves
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuMoveRecall(string name)
            : base(name)
        {
            this.Size = new Size(315, 360);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 63);

            this.lblItemCollection = new Label("lblItemCollection");
            this.lblItemCollection.AutoSize = true;
            this.lblItemCollection.Font = FontManager.LoadFont("PMU", 48);
            this.lblItemCollection.Text = "Move Recall";
            this.lblItemCollection.Location = new Point(20, 0);
            this.lblItemCollection.ForeColor = Color.WhiteSmoke;

            this.lblItemNum = new Label("lblItemNum");

            // lblItemNum.Size = new Size(100, 30);
            this.lblItemNum.AutoSize = true;
            this.lblItemNum.Location = new Point(196, 15);
            this.lblItemNum.Font = FontManager.LoadFont("PMU", 32);
            this.lblItemNum.BackColor = Color.Transparent;
            this.lblItemNum.Text = "0/0";
            this.lblItemNum.ForeColor = Color.WhiteSmoke;

            this.lblVisibleItems = new Label[10];
            for (int i = 0; i < this.lblVisibleItems.Length; i++)
            {
                this.lblVisibleItems[i] = new Label("lblVisibleItems" + i);

                // lblVisibleItems[i].AutoSize = true;
                // lblVisibleItems[i].Size = new Size(200, 32);
                this.lblVisibleItems[i].Width = 200;
                this.lblVisibleItems[i].Font = FontManager.LoadFont("PMU", 32);
                this.lblVisibleItems[i].Location = new Point(35, (i * 30) + 48);

                // lblVisibleItems[i].HoverColor = Color.Red;
                this.lblVisibleItems[i].ForeColor = Color.WhiteSmoke;
                this.lblVisibleItems[i].Click += new EventHandler<MouseButtonEventArgs>(this.MoveItem_Click);
                this.AddWidget(this.lblVisibleItems[i]);
            }

            this.AddWidget(this.lblItemCollection);
            this.AddWidget(this.lblItemNum);
            this.AddWidget(this.itemPicker);

            // DisplayItems(currentTen * 10 + 1);
            // ChangeSelected((itemSelected - 1) % 10);
            // UpdateSelectedItemInfo();
            // loaded = true;
            this.lblVisibleItems[0].Text = "Loading...";
        }

        public void LoadRecallMoves(string[] parse)
        {
            this.RecallMoves = new List<int>();

            if (parse.Length <= 2)
            {
                this.lblVisibleItems[0].Text = "Nothing";
                return;
            }

            for (int i = 1; i < parse.Length - 1; i++)
            {
                this.RecallMoves.Add(parse[i].ToInt());
            }

            this.DisplayItems(this.CurrentTen * 10);
            this.lblItemNum.Text = (this.CurrentTen + 1) + "/" + (((this.RecallMoves.Count - 1) / 10) + 1);
            this.loaded = true;
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 63 + (30 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        private void MoveItem_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.loaded)
            {
                if (this.RecallMoves[(this.CurrentTen * 10) + Array.IndexOf(this.lblVisibleItems, sender)] > 0)
                {
                    this.ChangeSelected(Array.IndexOf(this.lblVisibleItems, sender));
                    Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");

                    // mnuRecallMoveSelected selectedMenu = (mnuRecallMoveSelected)Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuRecallMoveSelected");
                    // if (selectedMenu != null)
                    // {
                    //    Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);
                        // selectedMenu.ItemSlot = GetSelectedItemSlot();
                        // selectedMenu.ItemNum = BankItems[GetSelectedItemSlot()].Num;
                    // }
                    //    Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new Menus.mnuRecallMoveSelected("mnuRecallMoveSelected", GetSelectedItemSlot()));
                    //    Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuRecallMoveSelected");
                }
            }
        }

        public void DisplayItems(int startNum)
        {
            this.BeginUpdate();
            for (int i = 0; i < this.lblVisibleItems.Length; i++)
            {
                // shop menu; lists items and their prices
                if (startNum + i >= this.RecallMoves.Count)
                {
                    this.lblVisibleItems[i].Text = string.Empty;
                }
                else if (this.RecallMoves[startNum + i] < 1)
                {
                    this.lblVisibleItems[i].Text = "---";
                }
else
                {
                    this.lblVisibleItems[i].Text = Moves.MoveHelper.Moves[this.RecallMoves[startNum + i]].Name;
                    }
            }

            this.EndUpdate();
        }

        private int GetSelectedItemSlot()
        {
            return this.itemPicker.SelectedItem + (this.CurrentTen * 10);
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            if (this.RecallMoves.Count == 0)
            {
                return;
            }

            if (this.loaded)
            {
                base.OnKeyboardDown(e);
                switch (e.Key)
                {
                    case SdlDotNet.Input.Key.DownArrow:
                        {
                            if (this.itemPicker.SelectedItem >= 9 || (this.CurrentTen * 10) + this.itemPicker.SelectedItem >= this.RecallMoves.Count - 1)
                            {
                                this.ChangeSelected(0);

                                // DisplayItems(1);
                            }
                            else
                            {
                                this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                            }

                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.UpArrow:
                        {
                            if (this.itemPicker.SelectedItem <= 0)
                            {
                                this.ChangeSelected(9);
                            }
                            else
                            {
                                this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                            }

                            if ((this.CurrentTen * 10) + this.itemPicker.SelectedItem > this.RecallMoves.Count)
                            {
                                this.ChangeSelected(this.RecallMoves.Count - (this.CurrentTen * 10) - 1);
                            }

                            Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.LeftArrow:
                        {
                            // int itemSlot = (currentTen + 1) - 10;//System.Math.Max(1, GetSelectedItemSlot() - (11 - itemPicker.SelectedItem));
                            if (this.CurrentTen <= 0)
                            {
                                this.CurrentTen = (this.RecallMoves.Count - 1) / 10;
                            }
                            else
                            {
                                this.CurrentTen--;
                            }

                            if ((this.CurrentTen * 10) + this.itemPicker.SelectedItem >= this.RecallMoves.Count)
                            {
                                this.ChangeSelected(this.RecallMoves.Count - (this.CurrentTen * 10) - 1);
                            }

                            this.DisplayItems(this.CurrentTen * 10);
                            this.lblItemNum.Text = (this.CurrentTen + 1) + "/" + (((this.RecallMoves.Count - 1) / 10) + 1);
                            Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.RightArrow:
                        {
                            // int itemSlot = currentTen + 1 + 10;
                            if (this.CurrentTen >= ((this.RecallMoves.Count - 1) / 10))
                            {
                                this.CurrentTen = 0;
                            }
                            else
                            {
                                this.CurrentTen++;
                            }

                            if ((this.CurrentTen * 10) + this.itemPicker.SelectedItem >= this.RecallMoves.Count)
                            {
                                this.ChangeSelected(this.RecallMoves.Count - (this.CurrentTen * 10) - 1);
                            }

                            this.DisplayItems(this.CurrentTen * 10);
                            this.lblItemNum.Text = (this.CurrentTen + 1) + "/" + (((this.RecallMoves.Count - 1) / 10) + 1);
                            Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                        }

                        break;
                    case SdlDotNet.Input.Key.Return:
                        {
                            if (this.GetSelectedItemSlot() > -1 && this.GetSelectedItemSlot() < this.RecallMoves.Count && this.RecallMoves[this.GetSelectedItemSlot()] > -1)
                            {
                                // Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new Menus.mnuRecallMoveSelected("mnuRecallMoveSelected", GetSelectedItemSlot()));
                                // Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuRecallMoveSelected");
                                Network.Messenger.SendRecallMove(this.RecallMoves[this.GetSelectedItemSlot()]);
                                MenuSwitcher.CloseAllMenus();
                            }
                        }

                        break;
                }
            }
        }
    }
}
