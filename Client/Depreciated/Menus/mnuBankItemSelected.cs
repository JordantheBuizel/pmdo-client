﻿// <copyright file="mnuBankItemSelected.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class MnuBankItemSelected : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private int itemNum;
        private int itemSlot;
        private readonly Enums.InvMenuType transaction;
        private readonly Label lblMove;
        private readonly NumericUpDown nudAmount;
        private readonly Label lblSummary;
        private readonly Widges.MenuItemPicker itemPicker;
        private const int MAXITEMS = 1;

        public int ItemNum
        {
            get { return this.itemNum; }

            set
            {
                this.itemNum = value;

                if (this.transaction == Enums.InvMenuType.Store)
                {
                    if (Items.ItemHelper.Items[this.itemNum].StackCap > 0 || Items.ItemHelper.Items[this.itemNum].Type == Enums.ItemType.Currency)
                    {
                        this.lblMove.Text = "Store Amount:";
                        this.nudAmount.Visible = true;
                    }
else
                    {
                        this.lblMove.Text = "Store";
                        this.nudAmount.Visible = false;
                    }
                }
                else if (this.transaction == Enums.InvMenuType.Take)
                {
                    if (Items.ItemHelper.Items[this.itemNum].StackCap > 0 || Items.ItemHelper.Items[this.itemNum].Type == Enums.ItemType.Currency)
                    {
                        this.lblMove.Text = "Take Amount:";
                        this.nudAmount.Visible = true;
                    }
else
                    {
                        this.lblMove.Text = "Take";
                        this.nudAmount.Visible = false;
                    }
                }
            }
        }

        public int ItemSlot
        {
            get { return this.itemSlot; }

            set
            {
                this.itemSlot = value;
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public MnuBankItemSelected(string name, int itemNum, int amount, int itemSlot, Enums.InvMenuType transactionType)
            : base(name)
            {
            this.transaction = transactionType;

            this.Size = new Size(185, 125);

            this.MenuDirection = Enums.MenuDirection.Horizontal;
            this.Location = new Point(335, 40);

            this.itemPicker = new Widges.MenuItemPicker("itemPicker");
            this.itemPicker.Location = new Point(18, 23);

            this.lblMove = new Label("lblMove");
            this.lblMove.Font = FontManager.LoadFont("PMU", 32);
            this.lblMove.AutoSize = true;
            this.lblMove.Text = "Store";
            this.lblMove.Location = new Point(30, 8);
            this.lblMove.HoverColor = Color.Red;
            this.lblMove.ForeColor = Color.WhiteSmoke;
            this.lblMove.Click += new EventHandler<MouseButtonEventArgs>(this.LblMove_Click);

            this.nudAmount = new NumericUpDown("nudAmount");
            this.nudAmount.Size = new Size(120, 24);
            this.nudAmount.Location = new Point(32, 42);
            this.nudAmount.Maximum = amount;
            this.nudAmount.Minimum = 1;

            this.lblSummary = new Label("lblSummary");
            this.lblSummary.Font = FontManager.LoadFont("PMU", 32);
            this.lblSummary.AutoSize = true;
            this.lblSummary.Text = "Summary";
            this.lblSummary.Location = new Point(30, 58);
            this.lblSummary.HoverColor = Color.Red;
            this.lblSummary.ForeColor = Color.WhiteSmoke;
            this.lblSummary.Click += new EventHandler<MouseButtonEventArgs>(this.LblSummary_Click);

            this.AddWidget(this.itemPicker);
            this.AddWidget(this.lblMove);
            this.AddWidget(this.nudAmount);
            this.AddWidget(this.lblSummary);

            this.ItemSlot = itemSlot;
            this.ItemNum = itemNum;
        }

        private void LblSummary_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(1);
        }

        private void LblMove_Click(object sender, MouseButtonEventArgs e)
        {
            this.SelectItem(0);
        }

        public void ChangeSelected(int itemNum)
        {
            this.itemPicker.Location = new Point(18, 23 + (50 * itemNum));
            this.itemPicker.SelectedItem = itemNum;
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.DownArrow:
                {
                        if (this.itemPicker.SelectedItem == MAXITEMS)
                        {
                            this.ChangeSelected(0);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.UpArrow:
                {
                        if (this.itemPicker.SelectedItem == 0)
                        {
                            this.ChangeSelected(MAXITEMS);
                        }
else
                        {
                            this.ChangeSelected(this.itemPicker.SelectedItem - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case SdlDotNet.Input.Key.Return:
                {
                        this.SelectItem(this.itemPicker.SelectedItem);
                    }

                    break;
                case SdlDotNet.Input.Key.Backspace:
                {
                        this.CloseMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        private void SelectItem(int selectedItem)
        {
            switch (selectedItem)
            {
                case 0:
                    { // deposit/withdraw item
                        if (this.itemNum > 0)
                        {
                            if (this.transaction == Enums.InvMenuType.Store)
                            {
                                if (Items.ItemHelper.Items[this.itemNum].StackCap > 0 || Items.ItemHelper.Items[this.itemNum].Type == Enums.ItemType.Currency)
                                {
                                    if (this.nudAmount.Value > 0)
                                    {
                                        Messenger.BankDeposit(this.itemSlot, this.nudAmount.Value);
                                    }
                                    else
                                    {
                                        // say you must store a number of items greater than 0.
                                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                                    }
                                }
else
                                {
                                    Messenger.BankDeposit(this.itemSlot, 0);
                                }
                            }
                            else if (this.transaction == Enums.InvMenuType.Take)
                            {
                                if (Items.ItemHelper.Items[this.itemNum].StackCap > 0 || Items.ItemHelper.Items[this.itemNum].Type == Enums.ItemType.Currency)
                                {
                                    if (this.nudAmount.Value > 0)
                                    {
                                        Messenger.BankWithdraw(this.itemSlot + 1, this.nudAmount.Value);
                                    }
                                    else
                                    {
                                        // say you must take a number of items greater than 0.
                                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                                    }
                                }
else
                                {
                                    Messenger.BankWithdraw(this.itemSlot + 1, 0);
                                }
                            }
                        }

                        this.CloseMenu();
                    }

                    break;
                case 1:
                    { // View item summary
                        MenuSwitcher.ShowItemSummary(this.itemNum, this.itemSlot, this.transaction);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                    }

                    break;
            }
        }

        private void CloseMenu()
        {
            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(this);
            if (this.transaction == Enums.InvMenuType.Store)
            {
                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuInventory");
            }
            else if (this.transaction == Enums.InvMenuType.Take)
            {
                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuBank");
            }
        }
    }
}
