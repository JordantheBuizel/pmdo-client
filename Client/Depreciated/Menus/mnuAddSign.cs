﻿// <copyright file="mnuAddSign.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using SdlDotNet.Widgets;

    internal class MnuAddSign : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblAddTile;
        private readonly TextBox txtHouse1;
        private readonly TextBox txtHouse2;
        private readonly TextBox txtHouse3;
        private readonly Label lblPrice;
        private readonly Button btnAccept;
        private readonly Button btnCancel;
        private readonly int price;

        public MnuAddSign(string name, int price)
            : base(name)
            {
                this.price = price;
            this.Size = new Size(250, 250);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = Graphic.DrawingSupport.GetCenter(Windows.WindowSwitcher.GameWindow.MapViewer.Size, this.Size);

            this.lblAddTile = new Label("lblAddTile");
            this.lblAddTile.Location = new Point(25, 15);
            this.lblAddTile.AutoSize = false;
            this.lblAddTile.Size = new Size(this.Width - (this.lblAddTile.X * 2), 40);
            this.lblAddTile.Text = "Write the things you want your sign to say:";
            this.lblAddTile.ForeColor = Color.WhiteSmoke;

            this.txtHouse1 = new TextBox("txtHouse1");
            this.txtHouse1.Location = new Point(this.lblAddTile.X, this.lblAddTile.Y + this.lblAddTile.Height + 10);
            this.txtHouse1.Size = new Size(this.Width - (this.lblAddTile.X * 2), 16);
            Skins.SkinManager.LoadTextBoxGui(this.txtHouse1);
            this.txtHouse1.TextChanged += new EventHandler(this.TxtHouse_TextChanged);

            this.txtHouse2 = new TextBox("txtHouse2");
            this.txtHouse2.Location = new Point(this.txtHouse1.X, this.txtHouse1.Y + this.txtHouse1.Height + 10);
            this.txtHouse2.Size = new Size(this.Width - (this.txtHouse1.X * 2), 16);
            Skins.SkinManager.LoadTextBoxGui(this.txtHouse2);
            this.txtHouse2.TextChanged += new EventHandler(this.TxtHouse_TextChanged);

            this.txtHouse3 = new TextBox("txtHouse3");
            this.txtHouse3.Location = new Point(this.txtHouse2.X, this.txtHouse2.Y + this.txtHouse2.Height + 10);
            this.txtHouse3.Size = new Size(this.Width - (this.txtHouse2.X * 2), 16);
            Skins.SkinManager.LoadTextBoxGui(this.txtHouse3);
            this.txtHouse3.TextChanged += new EventHandler(this.TxtHouse_TextChanged);

            this.lblPrice = new Label("lblPrice");
            this.lblPrice.Location = new Point(this.lblAddTile.X, this.txtHouse3.Y + this.txtHouse3.Height + 10);
            this.lblPrice.AutoSize = false;
            this.lblPrice.Size = new Size(120, 40);
            this.lblPrice.Text = "Placing this tile will cost " + ((this.txtHouse1.Text.Length + this.txtHouse2.Text.Length + this.txtHouse3.Text.Length) * price) + " " + Items.ItemHelper.Items[1].Name + ".";
            this.lblPrice.ForeColor = Color.WhiteSmoke;

            this.btnAccept = new Button("btnAccept");
            this.btnAccept.Location = new Point(this.lblAddTile.X, this.lblPrice.Y + this.lblPrice.Height + 10);
            this.btnAccept.Size = new Size(80, 30);
            this.btnAccept.Text = "Place Sign";
            this.btnAccept.Font = FontManager.LoadFont("tahoma", 10);
            Skins.SkinManager.LoadButtonGui(this.btnAccept);
            this.btnAccept.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAccept_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(this.btnAccept.X + this.btnAccept.Width, this.lblPrice.Y + this.lblPrice.Height + 10);
            this.btnCancel.Size = new Size(80, 30);
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Font = FontManager.LoadFont("tahoma", 10);
            Skins.SkinManager.LoadButtonGui(this.btnCancel);
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.AddWidget(this.lblAddTile);
            this.AddWidget(this.txtHouse1);
            this.AddWidget(this.txtHouse2);
            this.AddWidget(this.txtHouse3);
            this.AddWidget(this.lblPrice);
            this.AddWidget(this.btnAccept);
            this.AddWidget(this.btnCancel);
        }

        private void TxtHouse_TextChanged(object sender, EventArgs e)
        {
            this.lblPrice.Text = "Placing this tile will cost " + ((this.txtHouse1.Text.Length + this.txtHouse2.Text.Length + this.txtHouse3.Text.Length) * this.price) + " " + Items.ItemHelper.Items[1].Name + ".";
        }

        private void BtnAccept_Click(object sender, MouseButtonEventArgs e)
        {
            Messenger.SendAddSignRequest(this.txtHouse1.Text, this.txtHouse2.Text, this.txtHouse3.Text);
            MenuSwitcher.CloseAllMenus();
            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            MenuSwitcher.CloseAllMenus();
            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }
    }
}
