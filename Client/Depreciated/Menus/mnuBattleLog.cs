﻿// <copyright file="mnuBattleLog.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class MnuBattleLog : Widges.BorderedPanel, Core.IMenu
    {
        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private readonly Label lblBattleLog;
        private Label lblBattleEntries;
        private readonly ScrollingListBox lstBattleEntries;

        public MnuBattleLog(string name)
            : base(name)
            {
            this.Size = new Size(600, 440);
            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Location = new Point(10, 20);

            this.lblBattleLog = new Label("lblBattleLog");
            this.lblBattleLog.Location = new Point(20, 10);
            this.lblBattleLog.Font = FontManager.LoadFont("PMU", 36);
            this.lblBattleLog.AutoSize = true;
            this.lblBattleLog.Text = "Battle Log";
            this.lblBattleLog.ForeColor = Color.WhiteSmoke;

            this.lstBattleEntries = new ScrollingListBox("lstBattleEntries");
            this.lstBattleEntries.Location = new Point(16, 50);
            this.lstBattleEntries.Size = new Size(this.Width - (this.lstBattleEntries.X * 2), this.Height - this.lstBattleEntries.Y - 20);
            this.lstBattleEntries.BackColor = Color.Transparent;
            this.lstBattleEntries.BorderStyle = SdlDotNet.Widgets.BorderStyle.None;

            this.AddWidget(this.lblBattleLog);
            this.AddWidget(this.lstBattleEntries);
            this.AddEntries();
        }

        /// <inheritdoc/>
        public override void OnKeyboardDown(SdlDotNet.Input.KeyboardEventArgs e)
        {
            base.OnKeyboardDown(e);
            switch (e.Key)
            {
                case SdlDotNet.Input.Key.Backspace:
                {
                        // Show the others menu when the backspace key is pressed
                        MenuSwitcher.ShowOthersMenu();
                        Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                    }

                    break;
            }
        }

        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        public void AddEntries()
        {
            string[] messageArray = Logs.BattleLog.Messages.ToArray();
            Color[] colorArray = Logs.BattleLog.MessageColor.ToArray();

            for (int i = 0; i < messageArray.Length; i++)
            {
                ListBoxTextItem item = new ListBoxTextItem(FontManager.LoadFont("PMU", 16), messageArray[i]);
                item.ForeColor = colorArray[i];
                this.lstBattleEntries.Items.Add(item);
            }
        }
    }
}
