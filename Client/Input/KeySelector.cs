﻿// <copyright file="KeySelector.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Input
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using IO;
    using SdlInput = SdlDotNet.Input;

    public partial class KeySelector : Form
    {
        private readonly List<string> keyNames = new List<string>
        {
            "UpKey",
            "DownKey",
            "LeftKey",
            "RightKey",
            "TurnKey",
            "RunKey",
            "AttackKey",
            "Move1Key",
            "Move2Key",
            "Move3Key",
            "Move4Key",
            "AcceptKey",
            "Team1ItemKey",
            "Team2ItemKey",
            "Team3ItemKey",
            "Team4ItemKey",
            "ScreenshotKey",
            "OnlineListKey",
            "BattleLogKey",
            "RefreshKey",
            "AdventurePackKey"
        };

        public KeySelector()
        {
            this.InitializeComponent();

            this.UpKey = this.GetWindowsKeyFromSDL(ControlLoader.UpKey);
            this.DownKey = this.GetWindowsKeyFromSDL(ControlLoader.DownKey);
            this.LeftKey = this.GetWindowsKeyFromSDL(ControlLoader.LeftKey);
            this.RightKey = this.GetWindowsKeyFromSDL(ControlLoader.RightKey);
            this.TurnKey = this.GetWindowsKeyFromSDL(ControlLoader.TurnKey);
            this.RunKey = this.GetWindowsKeyFromSDL(ControlLoader.RunKey);
            this.AttackKey = this.GetWindowsKeyFromSDL(ControlLoader.AttackKey);
            this.Move1Key = this.GetWindowsKeyFromSDL(ControlLoader.Move1Key);
            this.Move2Key = this.GetWindowsKeyFromSDL(ControlLoader.Move2Key);
            this.Move3Key = this.GetWindowsKeyFromSDL(ControlLoader.Move3Key);
            this.Move4Key = this.GetWindowsKeyFromSDL(ControlLoader.Move4Key);
            this.AcceptKey = this.GetWindowsKeyFromSDL(ControlLoader.AcceptKey);
            this.Team1ItemKey = this.GetWindowsKeyFromSDL(ControlLoader.Team1ItemKey);
            this.Team2ItemKey = this.GetWindowsKeyFromSDL(ControlLoader.Team2ItemKey);
            this.Team3ItemKey = this.GetWindowsKeyFromSDL(ControlLoader.Team3ItemKey);
            this.Team4ItemKey = this.GetWindowsKeyFromSDL(ControlLoader.Team4ItemKey);
            this.ScreenshotKey = this.GetWindowsKeyFromSDL(ControlLoader.ScreenshotKey);
            this.OnlineListKey = this.GetWindowsKeyFromSDL(ControlLoader.OnlineListKey);
            this.BattleLogKey = this.GetWindowsKeyFromSDL(ControlLoader.BattleLogKey);
            this.RefreshKey = this.GetWindowsKeyFromSDL(ControlLoader.RefreshKey);
            this.AdventurePackKey = this.GetWindowsKeyFromSDL(ControlLoader.AdventurePackKey);

            List<Keys> testKeys = new List<Keys>()
            {
            this.UpKey,
            this.DownKey,
            this.LeftKey,
            this.RightKey,
            this.TurnKey,
            this.RunKey,
            this.AttackKey,
            this.Move1Key,
            this.Move2Key,
            this.Move3Key,
            this.Move4Key,
            this.AcceptKey,
            this.Team1ItemKey,
            this.Team2ItemKey,
            this.Team3ItemKey,
            this.Team4ItemKey,
            this.ScreenshotKey,
            this.OnlineListKey,
            this.BattleLogKey,
            this.RefreshKey,
            this.AdventurePackKey
            };
            {
                int i = 0;
                foreach (Keys key in testKeys)
                {
                    if (key == Keys.None)
                    {
                        var typ = typeof(ControlLoader);
                        var erroredSDLKeyprop = typ.GetProperty(this.keyNames[i], typeof(SdlInput.Key));
                        SdlInput.Key sDLKeyName = (SdlInput.Key)erroredSDLKeyprop.GetValue(null);
                        MessageBox.Show($"Key {this.keyNames[i]} failed to convert! The SDL key was {Enum.GetName(typeof(SdlInput.Key), sDLKeyName)}. " +
                            $"You can continue using the key selector, but please report this key on the forums so a special lookup can be created for it.");
                    }

                    i++;
                }
            }

            // Movement Stuff
            this.pbMoveDown.CreateGraphics().DrawString(Enum.GetName(typeof(SdlInput.Key), ControlLoader.DownKey), SystemFonts.DefaultFont, Brushes.Black, new PointF(32, 10));
            this.pbMoveLeft.CreateGraphics().DrawString(Enum.GetName(typeof(SdlInput.Key), ControlLoader.LeftKey), SystemFonts.DefaultFont, Brushes.Black, new PointF(32, 10));
            this.pbMoveRight.CreateGraphics().DrawString(Enum.GetName(typeof(SdlInput.Key), ControlLoader.RightKey), SystemFonts.DefaultFont, Brushes.Black, new PointF(32, 10));
            this.pbMoveUp.CreateGraphics().DrawString(Enum.GetName(typeof(SdlInput.Key), ControlLoader.UpKey), SystemFonts.DefaultFont, Brushes.Black, new PointF(32, 10));

            // Move Stuff
            this.pbAttackUp.CreateGraphics().DrawString(Enum.GetName(typeof(SdlInput.Key), ControlLoader.Move1Key), SystemFonts.DefaultFont, Brushes.Black, new PointF(32, 10));
            this.pbAttackLeft.CreateGraphics().DrawString(Enum.GetName(typeof(SdlInput.Key), ControlLoader.Move2Key), SystemFonts.DefaultFont, Brushes.Black, new PointF(32, 10));
            this.pbAttackDown.CreateGraphics().DrawString(Enum.GetName(typeof(SdlInput.Key), ControlLoader.Move3Key), SystemFonts.DefaultFont, Brushes.Black, new PointF(32, 10));
            this.pbAttackRight.CreateGraphics().DrawString(Enum.GetName(typeof(SdlInput.Key), ControlLoader.Move4Key), SystemFonts.DefaultFont, Brushes.Black, new PointF(32, 10));
            this.lblAttackKey.Text = Enum.GetName(typeof(SdlInput.Key), ControlLoader.AttackKey);

            // "Advanced" Stuff
            this.lblPickupRead.Text = this.lblPickupRead.Text.Split(':')[0] + ": " + ControlLoader.AcceptKey;
            this.lblTeam1.Text = this.lblTeam1.Text.Split(':')[0] + ": " + Enum.GetName(typeof(SdlInput.Key), ControlLoader.Team1ItemKey);
            this.lblTeam2.Text = this.lblTeam2.Text.Split(':')[0] + ": " + Enum.GetName(typeof(SdlInput.Key), ControlLoader.Team2ItemKey);
            this.lblTeam3.Text = this.lblTeam3.Text.Split(':')[0] + ": " + Enum.GetName(typeof(SdlInput.Key), ControlLoader.Team3ItemKey);
            this.lblTeam4.Text = this.lblTeam4.Text.Split(':')[0] + ": " + Enum.GetName(typeof(SdlInput.Key), ControlLoader.Team4ItemKey);
            this.lblScreenshot.Text = this.lblScreenshot.Text.Split(':')[0] + ": " + Enum.GetName(typeof(SdlInput.Key), ControlLoader.Team4ItemKey);
            this.lblOnlineList.Text = this.lblOnlineList.Text.Split(':')[0] + ": " + Enum.GetName(typeof(SdlInput.Key), ControlLoader.Team4ItemKey);
            this.lblBattleLog.Text = this.lblBattleLog.Text.Split(':')[0] + ": " + Enum.GetName(typeof(SdlInput.Key), ControlLoader.BattleLogKey);
        }

        private Keys UpKey { get; set; }

        private Keys DownKey { get; set; }

        private Keys LeftKey { get; set; }

        private Keys RightKey { get; set; }

        private Keys TurnKey { get; set; }

        private Keys RunKey { get; set; }

        private Keys AttackKey { get; set; }

        private Keys Move1Key { get; set; }

        private Keys Move2Key { get; set; }

        private Keys Move3Key { get; set; }

        private Keys Move4Key { get; set; }

        private Keys AcceptKey { get; set; }

        private Keys Team1ItemKey { get; set; }

        private Keys Team2ItemKey { get; set; }

        private Keys Team3ItemKey { get; set; }

        private Keys Team4ItemKey { get; set; }

        private Keys ScreenshotKey { get; set; }

        private Keys OnlineListKey { get; set; }

        private Keys BattleLogKey { get; set; }

        private Keys RefreshKey { get; set; }

        private Keys AdventurePackKey { get; set; }

        private SdlInput.Key GetSdlKeyFromWindows(Keys key)
        {
            for (int i = (int)Enum.GetValues(typeof(SdlInput.Key)).Cast<SdlInput.Key>().Min(); i < (int)Enum.GetValues(typeof(SdlInput.Key)).Cast<SdlInput.Key>().Max() + 1; i++)
            {
                if (Enum.GetName(typeof(Keys), key).ToLower() == Enum.GetName(typeof(SdlInput.Key), (SdlInput.Key)i).ToLower() ||
                    (Enum.GetName(typeof(SdlInput.Key), (SdlInput.Key)i).ToLower().Contains(Enum.GetName(typeof(Keys), key).ToLower()) && Enum.GetName(typeof(Keys), key).ToLower().Length > 1) ||
                    (Enum.GetName(typeof(Keys), key).ToLower().Contains(Enum.GetName(typeof(SdlInput.Key), (SdlInput.Key)i).ToLower()) && Enum.GetName(typeof(SdlInput.Key), (SdlInput.Key)i).ToLower().Length > 1))
                {
                    return (SdlInput.Key)i;
                }
            }

            return SdlInput.Key.Unknown;
        }

        private Keys GetWindowsKeyFromSDL(SdlInput.Key key)
        {
            for (int i = (int)Enum.GetValues(typeof(Keys)).Cast<Keys>().Min(); i < (int)Enum.GetValues(typeof(Keys)).Cast<Keys>().Max() + 1; i++)
            {
                if (Enum.GetName(typeof(Keys), (Keys)i).ToLower() == Enum.GetName(typeof(SdlInput.Key), key).ToLower() ||
                    (Enum.GetName(typeof(SdlInput.Key), key).ToLower().Contains(Enum.GetName(typeof(Keys), (Keys)i).ToLower()) && Enum.GetName(typeof(Keys), (Keys)i).ToLower().Length > 1) ||
                    (Enum.GetName(typeof(Keys), (Keys)i).ToLower().Contains(Enum.GetName(typeof(SdlInput.Key), key).ToLower()) && Enum.GetName(typeof(SdlInput.Key), key).ToLower().Length > 1))
                {
                    return (Keys)i;
                }
            }

            return Keys.None;
        }

        private void CbxAdvanced_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.cbxAdvanced.Checked)
            {
                this.Size = new Size(659, 416);
            }
            else
            {
                this.Size = new Size(659, 676);
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
        }

        private void PbMoveUp_Click(object sender, EventArgs e)
        {
        }

        private void PbMoveLeft_Click(object sender, EventArgs e)
        {
        }

        private void PbMoveDown_Click(object sender, EventArgs e)
        {
        }

        private void PbMoveRight_Click(object sender, EventArgs e)
        {
        }

        private void PbAttackUp_Click(object sender, EventArgs e)
        {
        }

        private void PbAttackLeft_Click(object sender, EventArgs e)
        {
        }

        private void PbAttackDown_Click(object sender, EventArgs e)
        {
        }

        private void PbAttackRight_Click(object sender, EventArgs e)
        {
        }

        private void LblAttackText_Click(object sender, EventArgs e)
        {
            this.LblAttackKey_Click(sender, e);
        }

        private void LblAttackKey_Click(object sender, EventArgs e)
        {
        }
    }
}
