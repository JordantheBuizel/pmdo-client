﻿// <copyright file="InputProcessor.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Input
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Runtime.InteropServices;
    using Graphic;
    using Network;
    using Players;
    using Windows;
    using ExpKit.Modules;
    using Graphic.Renderers;
    using Graphic.Renderers.Screen;
    using SdlDotNet.Graphics;
    using SdlInput = SdlDotNet.Input;

    /// <summary>
    /// Description of InputProcessor.
    /// </summary>
    internal class InputProcessor
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void Mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);

        private const int MOUSEEVENTFLEFTDOWN = 0x02;
        private const int MOUSEEVENTFLEFTUP = 0x04;
        private const int MOUSEEVENTFRIGHTDOWN = 0x08;
        private const int MOUSEEVENTFRIGHTUP = 0x10;

        public InputProcessor()
        {
        }

        public static int SelectedMove
        {
            get;
            set;
        }

        public static bool MoveUp { get; set; }

        public static bool MoveDown { get; set; }

        public static bool MoveLeft { get; set; }

        public static bool MoveRight { get; set; }

        public static bool Attacking { get; set; }

        public static bool Pickup { get; set; }

        public static void Initialize()
        {
            SelectedMove = -1;
        }

        public static void OnKeyDown(SdlInput.KeyboardEventArgs e)
        {
            if (e.Key == IO.ControlLoader.UpKey)
            {
                MoveUp = true;
                MovePlayer(Enums.Direction.Up, false);
            }
            else if (e.Key == IO.ControlLoader.DownKey)
            {
                MoveDown = true;
                MovePlayer(Enums.Direction.Down, false);
            }
            else if (e.Key == IO.ControlLoader.LeftKey)
            {
                MoveLeft = true;
                MovePlayer(Enums.Direction.Left, false);
            }
            else if (e.Key == IO.ControlLoader.RightKey)
            {
                MoveRight = true;
                MovePlayer(Enums.Direction.Right, false);
            }
            else if (e.Key == IO.ControlLoader.Move1Key)
            {
                if (PlayerManager.MyPlayer.Moves[0].MoveNum > -1)
                {
                    SelectedMove = 0;
                }
            }
            else if (e.Key == IO.ControlLoader.Move2Key)
            {
                if (PlayerManager.MyPlayer.Moves[1].MoveNum > -1)
                {
                    SelectedMove = 1;
                }
            }
            else if (e.Key == IO.ControlLoader.Move3Key)
            {
                if (PlayerManager.MyPlayer.Moves[2].MoveNum > -1)
                {
                    SelectedMove = 2;
                }
            }
            else if (e.Key == IO.ControlLoader.Move4Key)
            {
                if (PlayerManager.MyPlayer.Moves[3].MoveNum > -1)
                {
                    SelectedMove = 3;
                }
            }

            if (e.Key == IO.ControlLoader.AcceptKey)
            {
                if (!Pickup)
                {
                    PickupItem();
                    Pickup = true;
                }

                // } else if (e.Key == IO.ControlLoader.AttackKey) {
                //    GameProcessor.CheckAttack();
            }
            else if (e.Key == IO.ControlLoader.AttackKey)
            {
                if (SelectedMove > -1 && SelectedMove < 4)
                {
                    PlayerManager.MyPlayer.UseMove(SelectedMove);
                }
                else
                {
                    Attacking = true;
                    GameProcessor.CheckAttack();
                }
            } /*else if (e.Key == SdlInput.Key.T){

                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 10, 500, 0, 0);

            } */
            else if (e.Key == SdlInput.Key.L)
            {
#if TURTLE
                int settouse = 7;
                Console.Out.WriteLine("Drawing Tiles...");
                Tileset set = new Tileset(settouse, int.MaxValue);
                set.Load(Path.Combine(IO.Paths.GfxPath, "Tiles", "Tiles"+ settouse + ".tile"));
                Surface tilesettosurface = new Surface(448, (set.TileCount * 1024) / 448);
                tilesettosurface.ReplaceColor(Color.Black, Color.Transparent);
                RendererDestinationData destData = new RendererDestinationData(tilesettosurface, new Point(0, 0), tilesettosurface.Size);
                int x = 0;
                int y = 0;

                for (int i = 0; i < set.TileCount; i++)
                {
                    Logic.Graphic.Renderers.Maps.MapRenderer.DrawTile(destData, set.GetTileGraphic(i), x / 32, y / 32, false);

                    if (x + 32 == tilesettosurface.Width)
                    {
                        if (y + 32 == tilesettosurface.Height)
                        {
                            break;
                        }
                    }

                    if (x == 416)
                        y += 32;
                    if (x != 416)
                        x += 32;
                    else
                        x = 0;

                    Console.Out.WriteLine("Drawing Tile {0}/{1}", i, set.TileCount);
                }

                Console.Out.WriteLine("Bitmap drawn! Saving to {0}...", Path.Combine(IO.Paths.StartupPath, "GFX", "tile8.png"));
                tilesettosurface.SaveBmp(Path.Combine(IO.Paths.StartupPath, "GFX", "tile" + settouse +".bmp"));
                Console.Out.WriteLine("Finished!!!");
                tilesettosurface.Dispose();

#endif
            }
            else if (e.Key == IO.ControlLoader.Team1ItemKey)
            {
                int itemNum = 0;
                if (PlayerManager.MyPlayer.Team[1] != null)
                {
                    itemNum = PlayerManager.MyPlayer.GetInvItemNum(PlayerManager.MyPlayer.Team[0].HeldItemSlot);
                }

                if (itemNum > 0)
                {
                    if ((int)Items.ItemHelper.Items[itemNum].Type < 8 || (int)Items.ItemHelper.Items[itemNum].Type == 15)
                    {
                    }
                    else
                    {
                        if (SdlInput.Keyboard.IsKeyPressed(SdlInput.Key.LeftControl))
                        {
                            Messenger.SendThrowItem(PlayerManager.MyPlayer.Team[0].HeldItemSlot);
                        }
                        else
                        {
                            GameProcessor.CheckUseItem(0, PlayerManager.MyPlayer.Team[0].HeldItemSlot);
                        }
                    }
                }
            }
            else if (e.Key == IO.ControlLoader.Team2ItemKey)
            {
                int itemNum = 0;
                if (PlayerManager.MyPlayer.Team[1] != null)
                {
                    itemNum = PlayerManager.MyPlayer.GetInvItemNum(PlayerManager.MyPlayer.Team[1].HeldItemSlot);
                }

                if (itemNum > 0)
                {
                    if ((int)Items.ItemHelper.Items[itemNum].Type < 8 || (int)Items.ItemHelper.Items[itemNum].Type == 15)
                    {
                    }
                    else
                    {
                        if (SdlInput.Keyboard.IsKeyPressed(SdlInput.Key.LeftControl))
                        {
                            Messenger.SendThrowItem(PlayerManager.MyPlayer.Team[1].HeldItemSlot);
                        }
                        else
                        {
                            GameProcessor.CheckUseItem(1, PlayerManager.MyPlayer.Team[1].HeldItemSlot);
                        }
                    }
                }
            }
            else if (e.Key == IO.ControlLoader.Team3ItemKey)
            {
                int itemNum = 0;
                if (PlayerManager.MyPlayer.Team[1] != null)
                {
                    itemNum = PlayerManager.MyPlayer.GetInvItemNum(PlayerManager.MyPlayer.Team[2].HeldItemSlot);
                }

                if (itemNum > 0)
                {
                    if ((int)Items.ItemHelper.Items[itemNum].Type < 8 || (int)Items.ItemHelper.Items[itemNum].Type == 15)
                    {
                    }
                    else
                    {
                        if (SdlInput.Keyboard.IsKeyPressed(SdlInput.Key.LeftControl))
                        {
                            Messenger.SendThrowItem(PlayerManager.MyPlayer.Team[2].HeldItemSlot);
                        }
                        else
                        {
                            GameProcessor.CheckUseItem(2, PlayerManager.MyPlayer.Team[2].HeldItemSlot);
                        }
                    }
                }
            }
            else if (e.Key == IO.ControlLoader.Team4ItemKey)
            {
                int itemNum = 0;
                if (PlayerManager.MyPlayer.Team[1] != null)
                {
                    itemNum = PlayerManager.MyPlayer.GetInvItemNum(PlayerManager.MyPlayer.Team[3].HeldItemSlot);
                }

                if (itemNum > 0)
                {
                    if ((int)Items.ItemHelper.Items[itemNum].Type < 8 || (int)Items.ItemHelper.Items[itemNum].Type == 15)
                    {
                    }
                    else
                    {
                        if (SdlInput.Keyboard.IsKeyPressed(SdlInput.Key.LeftControl))
                        {
                            Messenger.SendThrowItem(PlayerManager.MyPlayer.Team[3].HeldItemSlot);
                        }
                        else
                        {
                            GameProcessor.CheckUseItem(3, PlayerManager.MyPlayer.Team[3].HeldItemSlot);
                        }
                    }
                }
            }
            else if (e.Key == IO.ControlLoader.ScreenshotKey)
            {
                if (Directory.Exists(IO.Paths.StartupPath + "Screenshots") == false)
                {
                    Directory.CreateDirectory(IO.Paths.StartupPath + "Screenshots");
                }

                int openScreenshot = -1;
                for (int i = 1; i < int.MaxValue; i++)
                {
                    if (File.Exists(IO.Paths.StartupPath + "Screenshots/Screenshot" + i + ".png") == false)
                    {
                        openScreenshot = i;
                        break;
                    }
                }

                if (openScreenshot > -1)
                {
                    SurfaceManager.SaveSurface(Video.Screen, IO.Paths.StartupPath + "Screenshots/Screenshot" + openScreenshot + ".png");
                    KitChat chat = (KitChat)System.Windows.Forms.Application.OpenForms["kitChat"];
                    if (chat != null)
                    {
                        chat.AppendChat("Screenshot #" + openScreenshot + " saved!", Color.Yellow);
                    }
                }
            }
            else if (e.Key == SdlInput.Key.F1)
            {
                if (Ranks.IsAllowed(PlayerManager.MyPlayer, Enums.Rank.Moniter))
                {
                    Windows.Editors.EditorManager.AdminPanel.Show();
                    SdlDotNet.Widgets.WindowManager.BringWindowToFront(Windows.Editors.EditorManager.AdminPanel);
                }

                /*
                } else if (e.Key == SdlInput.Key.F2) {
                    Players.Inventory inv = Players.PlayerManager.MyPlayer.Inventory;
                    for (int i = 1; i <= inv.Length; i++) {
                        if (inv[i].Num > 0) {
                            if (Items.ItemHelper.Items[inv[i].Num].Type == Enums.ItemType.PotionAddHP) {
                                ExpKit.Modules.kitChat chat = (ExpKit.Modules.kitChat)Windows.WindowSwitcher.ExpKit.KitContainer.ModuleSwitcher.FindKitModule(Enums.ExpKitModules.Chat);
                                if (chat != null) {
                                    chat.AppendChat("You have used a " + Items.ItemHelper.Items[inv[i].Num].Name + "!", Color.Yellow);
                                }
                                Messenger.SendUseItem(i);
                                break;
                            }
                        }
                    }
                } else if (e.Key == SdlInput.Key.F3) {
                    Players.Inventory inv = Players.PlayerManager.MyPlayer.Inventory;
                    for (int i = 1; i <= inv.Length; i++) {
                        if (inv[i].Num > 0) {
                            if (Items.ItemHelper.Items[inv[i].Num].Type == Enums.ItemType.PotionAddPP) {
                                ExpKit.Modules.kitChat chat = (ExpKit.Modules.kitChat)Windows.WindowSwitcher.ExpKit.KitContainer.ModuleSwitcher.FindKitModule(Enums.ExpKitModules.Chat);
                                if (chat != null) {
                                    chat.AppendChat("You have used a " + Items.ItemHelper.Items[inv[i].Num].Name + "!", Color.Yellow);
                                }
                                Messenger.SendUseItem(i);
                                break;
                            }
                        }
                    }
                */
            }

            // else if (e.Key == SdlInput.Key.F4) {
            //    if (Ranks.IsAllowed(PlayerManager.MyPlayer, Enums.Rank.Moniter)) {
            //        Windows.Editors.EditorManager.GuildPanel.Show();
            //        SdlDotNet.Widgets.WindowManager.BringWindowToFront(Windows.Editors.EditorManager.GuildPanel);
            //    }
            // }
            else if (e.Key == IO.ControlLoader.OnlineListKey)
            {
                Menus.MenuSwitcher.ShowMenu(new Menus.MnuOnlineList("mnuOnlineList"));
                Messenger.SendOnlineListRequest();
            }
            else if (e.Key == IO.ControlLoader.BattleLogKey)
            {
                Menus.MenuSwitcher.ShowMenu(new Menus.MnuBattleLog("mnuBattleLog"));
            }
            else if (e.Key == SdlInput.Key.Tab)
            {
            }
            else if (e.Key == SdlInput.Key.One)
            {
                Messenger.SendActiveCharSwap(0);
            }
            else if (e.Key == SdlInput.Key.Two)
            {
                Messenger.SendActiveCharSwap(1);
            }
            else if (e.Key == SdlInput.Key.Three)
            {
                Messenger.SendActiveCharSwap(2);
            }
            else if (e.Key == SdlInput.Key.Four)
            {
                Messenger.SendActiveCharSwap(3);
            }
        }

        public static void OnKeyUp(SdlInput.KeyboardEventArgs e)
        {
            if (e.Key == SdlInput.Key.Slash)
            {
                KitChat chat = KitChat.Instance;
                chat.FocusChat("Wow what an idiot");
            }
            else if (e.Key == IO.ControlLoader.UpKey)
            {
                MoveUp = false;
            }
            else if (e.Key == IO.ControlLoader.DownKey)
            {
                MoveDown = false;
            }
            else if (e.Key == IO.ControlLoader.LeftKey)
            {
                MoveLeft = false;
            }
            else if (e.Key == IO.ControlLoader.RightKey)
            {
                MoveRight = false;
            }
            else if (e.Key == IO.ControlLoader.AttackKey)
            {
                Attacking = false;
            }
            else if (e.Key == SdlInput.Key.M)
            {
                if (Ranks.IsAllowed(PlayerManager.MyPlayer, Enums.Rank.Mapper))
                {
                    ScreenRenderer.RenderOptions.MinimapVisible = !ScreenRenderer.RenderOptions.MinimapVisible;
                }
            }
            else if (e.Key == SdlInput.Key.Escape)
            {
                if (!WindowSwitcher.GameWindow.MenuManager.Visible)
                {
                    WindowSwitcher.GameWindow.MenuManager.Visible = true;
                    WindowSwitcher.GameWindow.MenuManager.Focus();
                    Menus.MenuSwitcher.ShowMainMenu();
                    Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                }
                else
                {
                    if (WindowSwitcher.GameWindow.MenuManager.HasModalMenu == false)
                    {
                        WindowSwitcher.GameWindow.MapViewer.Focus();
                        WindowSwitcher.GameWindow.MenuManager.Visible = false;
                        WindowSwitcher.GameWindow.MenuManager.CloseOpenMenus();
                    }
                }
            }

            // else if (e.Key == SdlInput.Key.F5) {
            //        if (!Windows.WindowSwitcher.GameWindow.MenuManager.Visible) {
            //            Windows.WindowSwitcher.GameWindow.MenuManager.Visible = true;
            //            Windows.WindowSwitcher.GameWindow.MenuManager.Focus();
            //            Menus.MenuSwitcher.ShowGuildMenu();
            //            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
            //        }
            //    }
            //
            else if (e.Key == IO.ControlLoader.TurnKey)
            {
                TurnPlayer();
            }
            else if (e.Key == IO.ControlLoader.RefreshKey)
            {
                if (Globals.Tick > PlayerManager.MyPlayer.GetTimer + 250)
                {
                    PlayerManager.MyPlayer.GetTimer = Globals.Tick;
                    Messenger.SendRefresh();
                }
            }
            else if (e.Key == IO.ControlLoader.AdventurePackKey)
            {
                Messenger.RequestAdventurePack();
            }
            else if (e.Key == IO.ControlLoader.AcceptKey)
            {
                if (PlayerManager.MyPlayer.Y - 1 > -1 && PlayerManager.MyPlayer.X >= 0 && PlayerManager.MyPlayer.X <= Maps.MapHelper.ActiveMap.MaxX && PlayerManager.MyPlayer.Y >= 0 && PlayerManager.MyPlayer.Y <= Maps.MapHelper.ActiveMap.MaxY)
                {
                    if (Maps.MapHelper.ActiveMap.Tile[PlayerManager.MyPlayer.X, PlayerManager.MyPlayer.Y - 1].Type == Enums.TileType.Sign && PlayerManager.MyPlayer.Direction == Enums.Direction.Up)
                    {
                        KitChat chat = (KitChat)System.Windows.Forms.Application.OpenForms["kitChat"];
                        if (chat != null)
                        {
                            chat.AppendChat("The sign reads:\n", new SdlDotNet.Widgets.CharRenderOptions(Color.Black));
                            if (!string.IsNullOrEmpty(Maps.MapHelper.ActiveMap.Tile[PlayerManager.MyPlayer.X, PlayerManager.MyPlayer.Y - 1].String1.Trim()))
                            {
                                chat.AppendChat(Maps.MapHelper.ActiveMap.Tile[PlayerManager.MyPlayer.X, PlayerManager.MyPlayer.Y - 1].String1.Trim() + "\n", new SdlDotNet.Widgets.CharRenderOptions(Color.Gray));
                            }

                            if (!string.IsNullOrEmpty(Maps.MapHelper.ActiveMap.Tile[PlayerManager.MyPlayer.X, PlayerManager.MyPlayer.Y - 1].String2.Trim()))
                            {
                                chat.AppendChat(Maps.MapHelper.ActiveMap.Tile[PlayerManager.MyPlayer.X, PlayerManager.MyPlayer.Y - 1].String2.Trim() + "\n", new SdlDotNet.Widgets.CharRenderOptions(Color.Gray));
                            }

                            if (!string.IsNullOrEmpty(Maps.MapHelper.ActiveMap.Tile[PlayerManager.MyPlayer.X, PlayerManager.MyPlayer.Y - 1].String3.Trim()))
                            {
                                chat.AppendChat(Maps.MapHelper.ActiveMap.Tile[PlayerManager.MyPlayer.X, PlayerManager.MyPlayer.Y - 1].String3.Trim() + "\n", new SdlDotNet.Widgets.CharRenderOptions(Color.Gray));
                            }
                        }
                    }
                }

                Pickup = false;
            }
        }

        public static void MovePlayer(Enums.Direction dir, bool lastMoved)
        {
            if (!Globals.GettingMap && !PlayerManager.MyPlayer.MovementLocked && !Stories.StoryProcessor.LoadingStory && !PlayerManager.MyPlayer.Dead)
            {
                if (PlayerManager.MyPlayer.Confused)
                {
                    dir = (Enums.Direction)MathFunctions.Random.Next(0, 4);
                }

                // dir = Enums.Direction.Up;
                // if (SdlInput.Keyboard.IsKeyPressed(IO.ControlLoader.TurnKey)) {
                //    Players.PlayerManager.MyPlayer.Direction = dir;
                //    Messenger.SendPlayerDir();
                // } else
                if (GameProcessor.CanMove(dir))
                {
                    MyPlayer player = PlayerManager.MyPlayer;

                    if (Globals.RefreshLock == false)
                    {
                        if (SdlInput.Keyboard.IsKeyPressed(IO.ControlLoader.RunKey))
                        {
                            player.MovementSpeed = PlayerManager.MyPlayer.SpeedLimit;
                        }
                        else
                        {
                            player.MovementSpeed = Enums.MovementSpeed.Walking;

                            if (player.MovementSpeed > PlayerManager.MyPlayer.SpeedLimit)
                            {
                                player.MovementSpeed = PlayerManager.MyPlayer.SpeedLimit;
                            }
                        }

                        if (Maps.MapHelper.ActiveMap.Tile[player.Location.X, player.Location.Y].Type == Enums.TileType.Slippery && lastMoved)
                        {
                            player.MovementSpeed = Enums.MovementSpeed.Slip;
                        }

                        if (Maps.MapHelper.ActiveMap.Tile[player.Location.X, player.Location.Y].Type == Enums.TileType.Slow)
                        {
                            int mobilityList = Maps.MapHelper.ActiveMap.Tile[player.Location.X, player.Location.Y].Data1;
                            bool slow = false;
                            for (int i = 0; i < 16; i++)
                            {
                                if (mobilityList % 2 == 1 && !PlayerManager.MyPlayer.Mobility[i])
                                {
                                    slow = true;
                                    break;
                                }

                                mobilityList /= 2;
                            }

                            if (slow && player.MovementSpeed > (Enums.MovementSpeed)Maps.MapHelper.ActiveMap.Tile[player.Location.X, player.Location.Y].Data2)
                            {
                                player.MovementSpeed = (Enums.MovementSpeed)Maps.MapHelper.ActiveMap.Tile[player.Location.X, player.Location.Y].Data2;
                            }
                        }

                        int x = player.X;
                        int y = player.Y;
                        switch (player.Direction)
                        {
                            case Enums.Direction.Up:
                                if (GameProcessor.CheckLocked(dir))
                                {
                                    Messenger.SendPlayerCriticalMove();
                                    player.MovementLocked = true;
                                }
                                else
                                {
                                    Messenger.SendPlayerMove();
                                }

                                y -= 1;

                                if (y < 0 && Graphic.Renderers.Maps.SeamlessWorldHelper.IsMapSeamless(Enums.MapID.Up))
                                {
                                    Maps.Map oldActive = Maps.MapHelper.Maps[Enums.MapID.Active];
                                    Maps.MapHelper.Maps[Enums.MapID.Active] = Maps.MapHelper.Maps[Enums.MapID.Up];
                                    Maps.MapHelper.Maps[Enums.MapID.Down] = oldActive;
                                    player.MapID = Maps.MapHelper.Maps[Enums.MapID.Active].MapID;
                                    player.Y = Maps.MapHelper.Maps[Enums.MapID.Active].MaxY + 1;

                                    Maps.MapHelper.Maps[Enums.MapID.BottomLeft] = Maps.MapHelper.Maps[Enums.MapID.Left];
                                    Maps.MapHelper.Maps[Enums.MapID.Left] = Maps.MapHelper.Maps[Enums.MapID.TopLeft];

                                    Maps.MapHelper.Maps[Enums.MapID.BottomRight] = Maps.MapHelper.Maps[Enums.MapID.Right];
                                    Maps.MapHelper.Maps[Enums.MapID.Right] = Maps.MapHelper.Maps[Enums.MapID.TopRight];

                                    Maps.MapHelper.HandleMapDone();
                                }

                                if (Maps.MapHelper.ActiveMap.Tile[player.X, player.Y - 1].Type == Enums.TileType.Warp)
                                {
                                    Globals.GettingMap = true;
                                }

                                if (Globals.GettingMap == false)
                                {
                                    player.Offset = new Point(player.Offset.X, Constants.TILEHEIGHT);
                                    player.Y -= 1;
                                }
                                else
                                {
                                    player.MovementSpeed = Enums.MovementSpeed.Standing;
                                }

                                break;
                            case Enums.Direction.Down:
                                if (GameProcessor.CheckLocked(dir))
                                {
                                    Messenger.SendPlayerCriticalMove();
                                    player.MovementLocked = true;
                                }
                                else
                                {
                                    Messenger.SendPlayerMove();
                                }

                                y += 1;

                                if (y > Maps.MapHelper.Maps[Enums.MapID.Active].MaxY && Graphic.Renderers.Maps.SeamlessWorldHelper.IsMapSeamless(Enums.MapID.Down))
                                {
                                    Maps.Map oldActive = Maps.MapHelper.Maps[Enums.MapID.Active];

                                    Maps.MapHelper.Maps[Enums.MapID.Active] = Maps.MapHelper.Maps[Enums.MapID.Down];
                                    Maps.MapHelper.Maps[Enums.MapID.Up] = oldActive;
                                    player.MapID = Maps.MapHelper.Maps[Enums.MapID.Active].MapID;
                                    player.Y = -1;

                                    Maps.MapHelper.Maps[Enums.MapID.TopLeft] = Maps.MapHelper.Maps[Enums.MapID.Left];
                                    Maps.MapHelper.Maps[Enums.MapID.Left] = Maps.MapHelper.Maps[Enums.MapID.BottomLeft];

                                    Maps.MapHelper.Maps[Enums.MapID.TopRight] = Maps.MapHelper.Maps[Enums.MapID.Right];
                                    Maps.MapHelper.Maps[Enums.MapID.Right] = Maps.MapHelper.Maps[Enums.MapID.BottomRight];

                                    Maps.MapHelper.HandleMapDone();
                                }

                                if (Maps.MapHelper.ActiveMap.Tile[player.X, player.Y + 1].Type == Enums.TileType.Warp)
                                {
                                    Globals.GettingMap = true;
                                }

                                if (Globals.GettingMap == false)
                                {
                                    player.Offset = new Point(player.Offset.X, Constants.TILEHEIGHT * -1);
                                    player.Y += 1;
                                }
                                else
                                {
                                    player.MovementSpeed = Enums.MovementSpeed.Standing;
                                }

                                break;
                            case Enums.Direction.Left:
                                if (GameProcessor.CheckLocked(dir))
                                {
                                    Messenger.SendPlayerCriticalMove();
                                    player.MovementLocked = true;
                                }
                                else
                                {
                                    Messenger.SendPlayerMove();
                                }

                                x -= 1;

                                if (x < 0 && Graphic.Renderers.Maps.SeamlessWorldHelper.IsMapSeamless(Enums.MapID.Left))
                                {
                                    Maps.Map oldActive = Maps.MapHelper.Maps[Enums.MapID.Active];
                                    Maps.MapHelper.Maps[Enums.MapID.Active] = Maps.MapHelper.Maps[Enums.MapID.Left];

                                    player.MapID = Maps.MapHelper.Maps[Enums.MapID.Active].MapID;

                                    Maps.MapHelper.Maps[Enums.MapID.TopRight] = Maps.MapHelper.Maps[Enums.MapID.Up];
                                    Maps.MapHelper.Maps[Enums.MapID.BottomRight] = Maps.MapHelper.Maps[Enums.MapID.Down];

                                    Maps.MapHelper.Maps[Enums.MapID.Up] = Maps.MapHelper.Maps[Enums.MapID.TopLeft];
                                    Maps.MapHelper.Maps[Enums.MapID.Down] = Maps.MapHelper.Maps[Enums.MapID.BottomLeft];

                                    Maps.MapHelper.Maps[Enums.MapID.Right] = oldActive;

                                    player.X = Maps.MapHelper.Maps[Enums.MapID.Active].MaxX + 1;

                                    Maps.MapHelper.HandleMapDone();
                                }

                                if (Maps.MapHelper.ActiveMap.Tile[player.X - 1, player.Y].Type == Enums.TileType.Warp)
                                {
                                    Globals.GettingMap = true;
                                }

                                if (Globals.GettingMap == false)
                                {
                                    player.Offset = new Point(Constants.TILEWIDTH, player.Offset.Y);
                                    player.X -= 1;
                                }
                                else
                                {
                                    player.MovementSpeed = Enums.MovementSpeed.Standing;
                                }

                                break;
                            case Enums.Direction.Right:
                                if (GameProcessor.CheckLocked(dir))
                                {
                                    Messenger.SendPlayerCriticalMove();
                                    player.MovementLocked = true;
                                }
                                else
                                {
                                    Messenger.SendPlayerMove();
                                }

                                x += 1;

                                if (x > Maps.MapHelper.Maps[Enums.MapID.Active].MaxX && Graphic.Renderers.Maps.SeamlessWorldHelper.IsMapSeamless(Enums.MapID.Right))
                                {
                                    Maps.Map oldActive = Maps.MapHelper.Maps[Enums.MapID.Active];

                                    Maps.MapHelper.Maps[Enums.MapID.Active] = Maps.MapHelper.Maps[Enums.MapID.Right];

                                    player.MapID = Maps.MapHelper.Maps[Enums.MapID.Active].MapID;

                                    Maps.MapHelper.Maps[Enums.MapID.TopLeft] = Maps.MapHelper.Maps[Enums.MapID.Up];
                                    Maps.MapHelper.Maps[Enums.MapID.BottomLeft] = Maps.MapHelper.Maps[Enums.MapID.Down];

                                    Maps.MapHelper.Maps[Enums.MapID.Up] = Maps.MapHelper.Maps[Enums.MapID.TopRight];
                                    Maps.MapHelper.Maps[Enums.MapID.Down] = Maps.MapHelper.Maps[Enums.MapID.BottomRight];

                                    Maps.MapHelper.Maps[Enums.MapID.Left] = oldActive;

                                    player.X = -1;

                                    Maps.MapHelper.HandleMapDone();
                                }

                                if (Maps.MapHelper.ActiveMap.Tile[player.X + 1, player.Y].Type == Enums.TileType.Warp)
                                {
                                    Globals.GettingMap = true;
                                }

                                if (Globals.GettingMap == false)
                                {
                                    player.Offset = new Point(Constants.TILEWIDTH * -1, player.Offset.Y);
                                    player.X += 1;
                                }
                                else
                                {
                                    player.MovementSpeed = Enums.MovementSpeed.Standing;
                                }

                                break;
                        }

                        ScreenRenderer.DeactivateOffscreenSprites();

                        if (player.ID == PlayerManager.MyPlayer.ID)
                        {
                            // PlayerManager.MyPlayer.SetCurrentRoom();
                        }

                        if (player.MovementSpeed > Enums.MovementSpeed.Standing && player.WalkingFrame == -1)
                        {
                            player.WalkingFrame = 0;
                            player.LastWalkTime = Globals.Tick;
                        }
                    }
                }
            }
        }

        private static void TurnPlayer()
        {
            if (!Globals.GettingMap && !PlayerManager.MyPlayer.MovementLocked && !Stories.StoryProcessor.LoadingStory && !PlayerManager.MyPlayer.Dead
                && PlayerManager.MyPlayer.MovementSpeed == Enums.MovementSpeed.Standing)
            {
                if (PlayerManager.MyPlayer.Confused)
                {
                    PlayerManager.MyPlayer.Direction = (Enums.Direction)MathFunctions.Random.Next(0, 4);
                }

                if (IO.Options.ArrowMoveKeys == true && !PlayerManager.MyPlayer.Confused)
                {
                    if (SdlInput.Keyboard.IsKeyPressed(IO.ControlLoader.UpKey))
                    {
                        PlayerManager.MyPlayer.Direction = Enums.Direction.Up;
                    }
                    else if (SdlInput.Keyboard.IsKeyPressed(IO.ControlLoader.DownKey))
                    {
                        PlayerManager.MyPlayer.Direction = Enums.Direction.Down;
                    }
                    else if (SdlInput.Keyboard.IsKeyPressed(IO.ControlLoader.LeftKey))
                    {
                        PlayerManager.MyPlayer.Direction = Enums.Direction.Left;
                    }
                    else if (SdlInput.Keyboard.IsKeyPressed(IO.ControlLoader.RightKey))
                    {
                        PlayerManager.MyPlayer.Direction = Enums.Direction.Right;
                    }
                    else
                    {
                        switch (PlayerManager.MyPlayer.Direction)
                        {
                            case Enums.Direction.Up:
                                {
                                    PlayerManager.MyPlayer.Direction = Enums.Direction.Right;
                                }

                                break;
                            case Enums.Direction.Down:
                                {
                                    PlayerManager.MyPlayer.Direction = Enums.Direction.Left;
                                }

                                break;
                            case Enums.Direction.Left:
                                {
                                    PlayerManager.MyPlayer.Direction = Enums.Direction.Up;
                                }

                                break;
                            case Enums.Direction.Right:
                                {
                                    PlayerManager.MyPlayer.Direction = Enums.Direction.Down;
                                }

                                break;
                        }
                    }
                }
                else if (IO.Options.ArrowMoveKeys == false && !PlayerManager.MyPlayer.Confused)
                {
                    if (SdlInput.Keyboard.IsKeyPressed(SdlInput.Key.W))
                    {
                        PlayerManager.MyPlayer.Direction = Enums.Direction.Up;
                    }
                    else if (SdlInput.Keyboard.IsKeyPressed(SdlInput.Key.S))
                    {
                        PlayerManager.MyPlayer.Direction = Enums.Direction.Down;
                    }
                    else if (SdlInput.Keyboard.IsKeyPressed(SdlInput.Key.A))
                    {
                        PlayerManager.MyPlayer.Direction = Enums.Direction.Left;
                    }
                    else if (SdlInput.Keyboard.IsKeyPressed(SdlInput.Key.D))
                    {
                        PlayerManager.MyPlayer.Direction = Enums.Direction.Right;
                    }
                    else
                    {
                        switch (PlayerManager.MyPlayer.Direction)
                        {
                            case Enums.Direction.Up:
                                {
                                    PlayerManager.MyPlayer.Direction = Enums.Direction.Right;
                                }

                                break;
                            case Enums.Direction.Down:
                                {
                                    PlayerManager.MyPlayer.Direction = Enums.Direction.Left;
                                }

                                break;
                            case Enums.Direction.Left:
                                {
                                    PlayerManager.MyPlayer.Direction = Enums.Direction.Up;
                                }

                                break;
                            case Enums.Direction.Right:
                                {
                                    PlayerManager.MyPlayer.Direction = Enums.Direction.Down;
                                }

                                break;
                        }
                    }
                }
            }

            Messenger.SendPlayerDir();
        }

        public static void PickupItem()
        {
            if (Globals.Tick > PlayerManager.MyPlayer.GetTimer + 250)
            {
                PlayerManager.MyPlayer.GetTimer = Globals.Tick;
                Messenger.SendPickupItem();
            }
        }

        public void VerifyKeys()
        {
            // if (SdlInput.Keyboard.IsKeyPressed(AttackKey) == false
        }
    }
}