﻿namespace Client.Logic.Input
{
    partial class KeySelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbMoveLeft = new System.Windows.Forms.PictureBox();
            this.pbMoveUp = new System.Windows.Forms.PictureBox();
            this.pbMoveRight = new System.Windows.Forms.PictureBox();
            this.pbMoveDown = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pbAttackUp = new System.Windows.Forms.PictureBox();
            this.pbAttackLeft = new System.Windows.Forms.PictureBox();
            this.pbAttackDown = new System.Windows.Forms.PictureBox();
            this.pbAttackRight = new System.Windows.Forms.PictureBox();
            this.lblAttackText = new System.Windows.Forms.Label();
            this.lblAttackKey = new System.Windows.Forms.Label();
            this.cbxAdvanced = new System.Windows.Forms.CheckBox();
            this.lblPickupRead = new System.Windows.Forms.Label();
            this.lblTeam1 = new System.Windows.Forms.Label();
            this.lblTeam2 = new System.Windows.Forms.Label();
            this.lblTeam3 = new System.Windows.Forms.Label();
            this.lblTeam4 = new System.Windows.Forms.Label();
            this.lblScreenshot = new System.Windows.Forms.Label();
            this.lblOnlineList = new System.Windows.Forms.Label();
            this.lblBattleLog = new System.Windows.Forms.Label();
            this.lblTurn = new System.Windows.Forms.Label();
            this.lblRefresh = new System.Windows.Forms.Label();
            this.lblAdventurePack = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbMoveLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMoveUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMoveRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMoveDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAttackUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAttackLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAttackDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAttackRight)).BeginInit();
            this.SuspendLayout();
            // 
            // pbMoveLeft
            // 
            this.pbMoveLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbMoveLeft.Image = global::Client.Logic.Properties.Resources.Walk_Left;
            this.pbMoveLeft.Location = new System.Drawing.Point(30, 139);
            this.pbMoveLeft.Name = "pbMoveLeft";
            this.pbMoveLeft.Size = new System.Drawing.Size(64, 64);
            this.pbMoveLeft.TabIndex = 0;
            this.pbMoveLeft.TabStop = false;
            this.pbMoveLeft.Click += new System.EventHandler(this.PbMoveLeft_Click);
            // 
            // pbMoveUp
            // 
            this.pbMoveUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbMoveUp.Image = global::Client.Logic.Properties.Resources.Walk_Up;
            this.pbMoveUp.Location = new System.Drawing.Point(123, 53);
            this.pbMoveUp.Name = "pbMoveUp";
            this.pbMoveUp.Size = new System.Drawing.Size(64, 64);
            this.pbMoveUp.TabIndex = 0;
            this.pbMoveUp.TabStop = false;
            this.pbMoveUp.Click += new System.EventHandler(this.PbMoveUp_Click);
            // 
            // pbMoveRight
            // 
            this.pbMoveRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbMoveRight.Image = global::Client.Logic.Properties.Resources.Walk_Right;
            this.pbMoveRight.Location = new System.Drawing.Point(217, 139);
            this.pbMoveRight.Name = "pbMoveRight";
            this.pbMoveRight.Size = new System.Drawing.Size(64, 64);
            this.pbMoveRight.TabIndex = 0;
            this.pbMoveRight.TabStop = false;
            this.pbMoveRight.Click += new System.EventHandler(this.PbMoveRight_Click);
            // 
            // pbMoveDown
            // 
            this.pbMoveDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbMoveDown.Image = global::Client.Logic.Properties.Resources.Walk_Down;
            this.pbMoveDown.Location = new System.Drawing.Point(123, 139);
            this.pbMoveDown.Name = "pbMoveDown";
            this.pbMoveDown.Size = new System.Drawing.Size(64, 64);
            this.pbMoveDown.TabIndex = 0;
            this.pbMoveDown.TabStop = false;
            this.pbMoveDown.Click += new System.EventHandler(this.PbMoveDown_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(263, 27);
            this.label1.TabIndex = 1;
            this.label1.Text = "Movement";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(371, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(251, 27);
            this.label2.TabIndex = 1;
            this.label2.Text = "Move Selection";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbAttackUp
            // 
            this.pbAttackUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbAttackUp.Image = global::Client.Logic.Properties.Resources.Attack_Up;
            this.pbAttackUp.Location = new System.Drawing.Point(464, 53);
            this.pbAttackUp.Name = "pbAttackUp";
            this.pbAttackUp.Size = new System.Drawing.Size(64, 64);
            this.pbAttackUp.TabIndex = 0;
            this.pbAttackUp.TabStop = false;
            this.pbAttackUp.Click += new System.EventHandler(this.PbAttackUp_Click);
            // 
            // pbAttackLeft
            // 
            this.pbAttackLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbAttackLeft.Image = global::Client.Logic.Properties.Resources.Attack_Left;
            this.pbAttackLeft.Location = new System.Drawing.Point(375, 139);
            this.pbAttackLeft.Name = "pbAttackLeft";
            this.pbAttackLeft.Size = new System.Drawing.Size(64, 64);
            this.pbAttackLeft.TabIndex = 0;
            this.pbAttackLeft.TabStop = false;
            this.pbAttackLeft.Click += new System.EventHandler(this.PbAttackLeft_Click);
            // 
            // pbAttackDown
            // 
            this.pbAttackDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbAttackDown.Image = global::Client.Logic.Properties.Resources.Attack_Down;
            this.pbAttackDown.Location = new System.Drawing.Point(464, 226);
            this.pbAttackDown.Name = "pbAttackDown";
            this.pbAttackDown.Size = new System.Drawing.Size(64, 64);
            this.pbAttackDown.TabIndex = 0;
            this.pbAttackDown.TabStop = false;
            this.pbAttackDown.Click += new System.EventHandler(this.PbAttackDown_Click);
            // 
            // pbAttackRight
            // 
            this.pbAttackRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbAttackRight.Image = global::Client.Logic.Properties.Resources.Attack_Right;
            this.pbAttackRight.Location = new System.Drawing.Point(558, 139);
            this.pbAttackRight.Name = "pbAttackRight";
            this.pbAttackRight.Size = new System.Drawing.Size(64, 64);
            this.pbAttackRight.TabIndex = 0;
            this.pbAttackRight.TabStop = false;
            this.pbAttackRight.Click += new System.EventHandler(this.PbAttackRight_Click);
            // 
            // lblAttackText
            // 
            this.lblAttackText.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttackText.Location = new System.Drawing.Point(458, 140);
            this.lblAttackText.Name = "lblAttackText";
            this.lblAttackText.Size = new System.Drawing.Size(76, 28);
            this.lblAttackText.TabIndex = 2;
            this.lblAttackText.Text = "Attack";
            this.lblAttackText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblAttackText.Click += new System.EventHandler(this.LblAttackText_Click);
            // 
            // lblAttackKey
            // 
            this.lblAttackKey.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttackKey.Location = new System.Drawing.Point(458, 168);
            this.lblAttackKey.Name = "lblAttackKey";
            this.lblAttackKey.Size = new System.Drawing.Size(76, 28);
            this.lblAttackKey.TabIndex = 2;
            this.lblAttackKey.Text = "F";
            this.lblAttackKey.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblAttackKey.Click += new System.EventHandler(this.LblAttackKey_Click);
            // 
            // cbxAdvanced
            // 
            this.cbxAdvanced.AutoSize = true;
            this.cbxAdvanced.Location = new System.Drawing.Point(30, 350);
            this.cbxAdvanced.Name = "cbxAdvanced";
            this.cbxAdvanced.Size = new System.Drawing.Size(146, 17);
            this.cbxAdvanced.TabIndex = 3;
            this.cbxAdvanced.Text = "Show Advanced Controls";
            this.cbxAdvanced.UseVisualStyleBackColor = true;
            this.cbxAdvanced.CheckedChanged += new System.EventHandler(this.CbxAdvanced_CheckedChanged);
            // 
            // lblPickupRead
            // 
            this.lblPickupRead.AutoSize = true;
            this.lblPickupRead.Location = new System.Drawing.Point(30, 387);
            this.lblPickupRead.Name = "lblPickupRead";
            this.lblPickupRead.Size = new System.Drawing.Size(159, 13);
            this.lblPickupRead.TabIndex = 4;
            this.lblPickupRead.Text = "Pickup Items/Read Signs: Enter";
            // 
            // lblTeam1
            // 
            this.lblTeam1.AutoSize = true;
            this.lblTeam1.Location = new System.Drawing.Point(30, 411);
            this.lblTeam1.Name = "lblTeam1";
            this.lblTeam1.Size = new System.Drawing.Size(187, 13);
            this.lblTeam1.TabIndex = 4;
            this.lblTeam1.Text = "Use First Team Member\'s Held Item: Z";
            // 
            // lblTeam2
            // 
            this.lblTeam2.AutoSize = true;
            this.lblTeam2.Location = new System.Drawing.Point(30, 436);
            this.lblTeam2.Name = "lblTeam2";
            this.lblTeam2.Size = new System.Drawing.Size(205, 13);
            this.lblTeam2.TabIndex = 4;
            this.lblTeam2.Text = "Use Second Team Member\'s Held Item: X";
            // 
            // lblTeam3
            // 
            this.lblTeam3.AutoSize = true;
            this.lblTeam3.Location = new System.Drawing.Point(30, 459);
            this.lblTeam3.Name = "lblTeam3";
            this.lblTeam3.Size = new System.Drawing.Size(192, 13);
            this.lblTeam3.TabIndex = 4;
            this.lblTeam3.Text = "Use Third Team Member\'s Held Item: C";
            // 
            // lblTeam4
            // 
            this.lblTeam4.AutoSize = true;
            this.lblTeam4.Location = new System.Drawing.Point(30, 481);
            this.lblTeam4.Name = "lblTeam4";
            this.lblTeam4.Size = new System.Drawing.Size(192, 13);
            this.lblTeam4.TabIndex = 4;
            this.lblTeam4.Text = "Use Forth Team Member\'s Held Item: V";
            // 
            // lblScreenshot
            // 
            this.lblScreenshot.AutoSize = true;
            this.lblScreenshot.Location = new System.Drawing.Point(30, 503);
            this.lblScreenshot.Name = "lblScreenshot";
            this.lblScreenshot.Size = new System.Drawing.Size(85, 13);
            this.lblScreenshot.TabIndex = 4;
            this.lblScreenshot.Text = "Screenshot: F11";
            // 
            // lblOnlineList
            // 
            this.lblOnlineList.AutoSize = true;
            this.lblOnlineList.Location = new System.Drawing.Point(30, 525);
            this.lblOnlineList.Name = "lblOnlineList";
            this.lblOnlineList.Size = new System.Drawing.Size(74, 13);
            this.lblOnlineList.TabIndex = 4;
            this.lblOnlineList.Text = "Online List: F9";
            // 
            // lblBattleLog
            // 
            this.lblBattleLog.AutoSize = true;
            this.lblBattleLog.Location = new System.Drawing.Point(30, 547);
            this.lblBattleLog.Name = "lblBattleLog";
            this.lblBattleLog.Size = new System.Drawing.Size(76, 13);
            this.lblBattleLog.TabIndex = 4;
            this.lblBattleLog.Text = "BattleLog: F10";
            // 
            // lblTurn
            // 
            this.lblTurn.AutoSize = true;
            this.lblTurn.Location = new System.Drawing.Point(30, 569);
            this.lblTurn.Name = "lblTurn";
            this.lblTurn.Size = new System.Drawing.Size(95, 13);
            this.lblTurn.TabIndex = 4;
            this.lblTurn.Text = "Turn Player: Home";
            // 
            // lblRefresh
            // 
            this.lblRefresh.AutoSize = true;
            this.lblRefresh.Location = new System.Drawing.Point(30, 591);
            this.lblRefresh.Name = "lblRefresh";
            this.lblRefresh.Size = new System.Drawing.Size(93, 13);
            this.lblRefresh.TabIndex = 4;
            this.lblRefresh.Text = "Refresh Map: End";
            // 
            // lblAdventurePack
            // 
            this.lblAdventurePack.AutoSize = true;
            this.lblAdventurePack.Location = new System.Drawing.Point(30, 613);
            this.lblAdventurePack.Name = "lblAdventurePack";
            this.lblAdventurePack.Size = new System.Drawing.Size(97, 13);
            this.lblAdventurePack.TabIndex = 4;
            this.lblAdventurePack.Text = "Adventure Pack: P";
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(452, 329);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(89, 36);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // KeySelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 678);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblAdventurePack);
            this.Controls.Add(this.lblRefresh);
            this.Controls.Add(this.lblTurn);
            this.Controls.Add(this.lblBattleLog);
            this.Controls.Add(this.lblOnlineList);
            this.Controls.Add(this.lblScreenshot);
            this.Controls.Add(this.lblTeam4);
            this.Controls.Add(this.lblTeam3);
            this.Controls.Add(this.lblTeam2);
            this.Controls.Add(this.lblTeam1);
            this.Controls.Add(this.lblPickupRead);
            this.Controls.Add(this.cbxAdvanced);
            this.Controls.Add(this.lblAttackKey);
            this.Controls.Add(this.lblAttackText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbAttackDown);
            this.Controls.Add(this.pbMoveDown);
            this.Controls.Add(this.pbAttackLeft);
            this.Controls.Add(this.pbAttackRight);
            this.Controls.Add(this.pbMoveRight);
            this.Controls.Add(this.pbAttackUp);
            this.Controls.Add(this.pbMoveUp);
            this.Controls.Add(this.pbMoveLeft);
            this.Name = "KeySelector";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "KeySelector";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.pbMoveLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMoveUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMoveRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMoveDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAttackUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAttackLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAttackDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAttackRight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbMoveLeft;
        private System.Windows.Forms.PictureBox pbMoveUp;
        private System.Windows.Forms.PictureBox pbMoveRight;
        private System.Windows.Forms.PictureBox pbMoveDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pbAttackUp;
        private System.Windows.Forms.PictureBox pbAttackLeft;
        private System.Windows.Forms.PictureBox pbAttackDown;
        private System.Windows.Forms.PictureBox pbAttackRight;
        private System.Windows.Forms.Label lblAttackText;
        private System.Windows.Forms.Label lblAttackKey;
        private System.Windows.Forms.CheckBox cbxAdvanced;
        private System.Windows.Forms.Label lblPickupRead;
        private System.Windows.Forms.Label lblTeam1;
        private System.Windows.Forms.Label lblTeam2;
        private System.Windows.Forms.Label lblTeam3;
        private System.Windows.Forms.Label lblTeam4;
        private System.Windows.Forms.Label lblScreenshot;
        private System.Windows.Forms.Label lblOnlineList;
        private System.Windows.Forms.Label lblBattleLog;
        private System.Windows.Forms.Label lblTurn;
        private System.Windows.Forms.Label lblRefresh;
        private System.Windows.Forms.Label lblAdventurePack;
        private System.Windows.Forms.Button btnSave;
    }
}