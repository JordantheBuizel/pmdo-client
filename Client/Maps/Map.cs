﻿// <copyright file="Map.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Maps
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Algorithms.Pathfinder;
    using Players;

    [Serializable]
    internal class Map
    {
        private IPathfinder pathfinder;

        public Map(string mapID)
        {
            this.MapID = mapID;
            this.Tile = new Tile[20, 15];
            this.OriginalTiles = new Tile[20, 15];
            this.Npc = new List<MapNpcSettings>();
            this.MapItems = new MapItem[MaxInfo.MaxMapItems];
            this.MapNpcs = new MapNpc[MaxInfo.MAXMAPNPCS];
            this.Loaded = false;
            for (int i = 0; i < MaxInfo.MAXMAPNPCS; i++)
            {
                this.MapNpcs[i] = new MapNpc();
            }

            for (int i = 0; i < MaxInfo.MaxMapItems; i++)
            {
                this.MapItems[i] = new MapItem();
            }
        }

        public MapItem[] MapItems
        {
            get;
            set;
        }

        public IPathfinder Pathfinder
        {
            get
            {
                if (this.pathfinder == null)
                {
                    this.pathfinder = new AStarPathfinder(this);
                }

                return this.pathfinder;
            }
        }

        public int MinNpcs { get; set; }

        public int MaxNpcs { get; set; }

        public int NpcSpawnTime { get; set; }

        public MapNpc[] MapNpcs
        {
            get;
            set;
        }

        public int Down
        {
            get;
            set;
        }

        public bool Indoors
        {
            get;
            set;
        }

        public int Left
        {
            get;
            set;
        }

        public string MapID
        {
            get;
            set;
        }

        public int MaxX
        {
            get;
            set;
        }

        public int MaxY
        {
            get;
            set;
        }

        public Enums.MapMoral Moral
        {
            get;
            set;
        }

        public string Music
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public List<MapNpcSettings> Npc
        {
            get;
            set;
        }

        public int Darkness
        {
            get;
            set;
        }

        public string Owner
        {
            get;
            set;
        }

        public int Revision
        {
            get;
            set;
        }

        public int Right
        {
            get;
            set;
        }

        // Public Shop As Integer
        public Tile[,] Tile
        {
            get;
            set;
        }

        public Tile[,] OriginalTiles
        {
            get;
            set;
        }

        public int Up
        {
            get;
            set;
        }

        public Enums.Weather Weather
        {
            get;
            set;
        }

        public bool Loaded
        {
            get;
            set;
        }

        public int DungeonIndex
        {
            get;
            set;
        }

        public bool HungerEnabled
        {
            get;
            set;
        }

        public bool RecruitEnabled
        {
            get;
            set;
        }

        public bool ExpEnabled
        {
            get;
            set;
        }

        public int TimeLimit
        {
            get;
            set;
        }

        public bool Cacheable
        {
            get;
            set;
        }

        public bool Instanced
        {
            get;
            set;
        }

        public string ImpersonatingMap
        {
            get;
            set;
        }

        public List<IPlayer> Players
        {
            get;
            set;
        }

        public void DoOverlayChecks()
        {
            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.SetOverlay(Enums.Overlay.None);
            if (this.Indoors == false)
            {
                // Logic.Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.SetWeather(Weather);
            }
else
            {
                // Logic.Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.SetOverlay(Enums.Overlay.None);
                // Logic.Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.SetWeather(Enums.Weather.None);
                Globals.ActiveWeather = Enums.Weather.None;
            }
        }

        public void LoadFromHouseClass(HouseProperties properties)
        {
            this.Music = properties.Music;
        }

        public void LoadFromPropertiesClass(MapProperties properties)
        {
            this.Name = properties.Name;
            this.Right = properties.Right;
            this.Left = properties.Left;
            this.Up = properties.Up;
            this.Down = properties.Down;
            this.Music = properties.Music;
            this.MaxX = properties.MaxX;
            this.MaxY = properties.MaxY;
            this.Moral = properties.Moral;
            this.Weather = properties.Weather;
            this.Darkness = properties.Darkness;
            this.Indoors = properties.Indoors;
            this.HungerEnabled = properties.Belly;
            this.RecruitEnabled = properties.Recruit;
            this.ExpEnabled = properties.Exp;
            this.TimeLimit = properties.TimeLimit;
            this.Instanced = properties.Instanced;
            this.DungeonIndex = properties.DungeonIndex;
            this.MinNpcs = properties.MinNpcs;
            this.MaxNpcs = properties.MaxNpcs;
            this.NpcSpawnTime = properties.NpcSpawnTime;

            this.Npc = new List<MapNpcSettings>();
            for (int i = 0; i < properties.Npcs.Count; i++)
            {
                this.Npc.Add(new MapNpcSettings());
                this.Npc[i].NpcNum = properties.Npcs[i].NpcNum;
                this.Npc[i].SpawnX = properties.Npcs[i].SpawnX;
                this.Npc[i].SpawnY = properties.Npcs[i].SpawnY;
                this.Npc[i].MinLevel = properties.Npcs[i].MinLevel;
                this.Npc[i].MaxLevel = properties.Npcs[i].MaxLevel;
                this.Npc[i].AppearanceRate = properties.Npcs[i].AppearanceRate;
                this.Npc[i].StartStatus = properties.Npcs[i].StartStatus;
                this.Npc[i].StartStatusCounter = properties.Npcs[i].StartStatusCounter;
                this.Npc[i].StartStatusChance = properties.Npcs[i].StartStatusChance;
            }
        }

        public HouseProperties ExportToHouseClass()
        {
            HouseProperties properties = new HouseProperties();
            properties.Music = this.Music;
            return properties;
        }

        public MapProperties ExportToPropertiesClass()
        {
            MapProperties properties = new MapProperties();
            properties.Name = this.Name;
            properties.Right = this.Right;
            properties.Left = this.Left;
            properties.Up = this.Up;
            properties.Down = this.Down;
            properties.Music = this.Music;
            properties.MaxX = this.MaxX;
            properties.MaxY = this.MaxY;
            properties.Moral = this.Moral;
            properties.Weather = this.Weather;
            properties.Darkness = this.Darkness;
            properties.Indoors = this.Indoors;
            properties.Belly = this.HungerEnabled;
            properties.Recruit = this.RecruitEnabled;
            properties.Exp = this.ExpEnabled;
            properties.TimeLimit = this.TimeLimit;
            properties.Instanced = this.Instanced;
            properties.DungeonIndex = this.DungeonIndex;
            properties.MinNpcs = this.MinNpcs;
            properties.MaxNpcs = this.MaxNpcs;
            properties.NpcSpawnTime = this.NpcSpawnTime;

            for (int i = 0; i < this.Npc.Count; i++)
            {
                properties.Npcs.Add(new MapNpcSettings());
                properties.Npcs[i].NpcNum = this.Npc[i].NpcNum;
                properties.Npcs[i].SpawnX = this.Npc[i].SpawnX;
                properties.Npcs[i].SpawnY = this.Npc[i].SpawnY;
                properties.Npcs[i].MinLevel = this.Npc[i].MinLevel;
                properties.Npcs[i].MaxLevel = this.Npc[i].MaxLevel;
                properties.Npcs[i].AppearanceRate = this.Npc[i].AppearanceRate;
                properties.Npcs[i].StartStatus = this.Npc[i].StartStatus;
                properties.Npcs[i].StartStatusCounter = this.Npc[i].StartStatusCounter;
                properties.Npcs[i].StartStatusChance = this.Npc[i].StartStatusChance;
            }

            return properties;
        }
    }
}