﻿// <copyright file="Tile.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Maps
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Graphic;

    [Serializable]
    public class Tile : ICloneable
    {
        public bool SeenBySelf { get; set; }

        public TileGraphic AnimGraphic
        {
            get;
            set;
        }

        public int Anim
        {
            get;
            set;
        }

        public int AnimSet
        {
            get;
            set;
        }

        public int AnimRotation
        {
            get;
            set;
        }

        public bool AnimFlipped
        {
            get;
            set;
        }

        public int Data1
        {
            get;
            set;
        }

        public int Data2
        {
            get;
            set;
        }

        public int Data3
        {
            get;
            set;
        }

        public bool DoorOpen
        {
            get;
            set;
        }

        public TileGraphic F2AnimGraphic
        {
            get;
            set;
        }

        public int F2Anim
        {
            get;
            set;
        }

        public int F2AnimSet
        {
            get;
            set;
        }

        public int F2AnimRotation
        {
            get;
            set;
        }

        public bool F2AnimFlipped
        {
            get;
            set;
        }

        public TileGraphic FAnimGraphic
        {
            get;
            set;
        }

        public int FAnim
        {
            get;
            set;
        }

        public int FAnimSet
        {
            get;
            set;
        }

        public int FAnimRotation
        {
            get;
            set;
        }

        public bool FAnimFlipped
        {
            get;
            set;
        }

        public TileGraphic FringeGraphic
        {
            get;
            set;
        }

        public int Fringe
        {
            get;
            set;
        }

        public int FringeSet
        {
            get;
            set;
        }

        public int FringeRotation
        {
            get;
            set;
        }

        public bool FringeFlipped
        {
            get;
            set;
        }

        public TileGraphic Fringe2Graphic
        {
            get;
            set;
        }

        public int Fringe2
        {
            get;
            set;
        }

        public int Fringe2Set
        {
            get;
            set;
        }

        public int Fringe2Rotation
        {
            get;
            set;
        }

        public bool Fringe2Flipped
        {
            get;
            set;
        }

        public TileGraphic GroundGraphic
        {
            get;
            set;
        }

        public int Ground
        {
            get;
            set;
        }

        public int GroundSet
        {
            get;
            set;
        }

        public int GroundRotation
        {
            get;
            set;
        }

        public bool GroundFlipped
        {
            get;
            set;
        }

        public TileGraphic GroundAnimGraphic
        {
            get;
            set;
        }

        public int GroundAnim
        {
            get;
            set;
        }

        public int GroundAnimSet
        {
            get;
            set;
        }

        public int GroundAnimRotation
        {
            get;
            set;
        }

        public bool GroundAnimFlipped
        {
            get;
            set;
        }

        public int RDungeonMapValue
        {
            get;
            set;
        }

        public TileGraphic M2AnimGraphic
        {
            get;
            set;
        }

        public int M2Anim
        {
            get;
            set;
        }

        public int M2AnimSet
        {
            get;
            set;
        }

        public int M2AnimRotation
        {
            get;
            set;
        }

        public bool M2AnimFlipped
        {
            get;
            set;
        }

        public TileGraphic MaskGraphic
        {
            get;
            set;
        }

        public int Mask
        {
            get;
            set;
        }

        public int MaskSet
        {
            get;
            set;
        }

        public int MaskRotation
        {
            get;
            set;
        }

        public bool MaskFlipped
        {
            get;
            set;
        }

        public TileGraphic Mask2Graphic
        {
            get;
            set;
        }

        public int Mask2
        {
            get;
            set;
        }

        public int Mask2Set
        {
            get;
            set;
        }

        public int Mask2Rotation
        {
            get;
            set;
        }

        public bool Mask2Flipped
        {
            get;
            set;
        }

        public string String1
        {
            get;
            set;
        }

        public string String2
        {
            get;
            set;
        }

        public string String3
        {
            get;
            set;
        }

        public Enums.TileType Type
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public object Clone()
        {
            Tile tile = new Tile();
            tile.Ground = this.Ground;
            tile.GroundAnim = this.GroundAnim;
            tile.Mask = this.Mask;
            tile.Anim = this.Anim;
            tile.Mask2 = this.Mask2;
            tile.M2Anim = this.M2Anim;
            tile.Fringe = this.Fringe;
            tile.FAnim = this.FAnim;
            tile.Fringe2 = this.Fringe2;
            tile.F2Anim = this.F2Anim;
            tile.Type = this.Type;
            tile.Data1 = this.Data1;
            tile.Data2 = this.Data2;
            tile.Data3 = this.Data3;
            tile.String1 = this.String1;
            tile.String2 = this.String2;
            tile.String3 = this.String3;
            tile.RDungeonMapValue = this.RDungeonMapValue;
            tile.GroundSet = this.GroundSet;
            tile.GroundAnimSet = this.GroundAnimSet;
            tile.MaskSet = this.MaskSet;
            tile.AnimSet = this.AnimSet;
            tile.Mask2Set = this.Mask2Set;
            tile.M2AnimSet = this.M2AnimSet;
            tile.FringeSet = this.FringeSet;
            tile.FAnimSet = this.FAnimSet;
            tile.Fringe2Set = this.Fringe2Set;
            tile.F2AnimSet = this.F2AnimSet;
            tile.AnimRotation = this.AnimRotation;
            tile.AnimFlipped = this.AnimFlipped;
            tile.F2AnimRotation = this.F2AnimRotation;
            tile.F2AnimFlipped = this.F2AnimFlipped;
            tile.FAnimRotation = this.FAnimRotation;
            tile.FAnimFlipped = this.FAnimFlipped;
            tile.Fringe2Rotation = this.Fringe2Rotation;
            tile.Fringe2Flipped = this.Fringe2Flipped;
            tile.FringeRotation = this.FringeRotation;
            tile.FringeFlipped = this.FringeFlipped;
            tile.GroundRotation = this.GroundRotation;
            tile.GroundFlipped = this.GroundFlipped;
            tile.GroundAnimRotation = this.GroundAnimRotation;
            tile.GroundAnimFlipped = this.GroundAnimFlipped;
            tile.M2AnimRotation = this.M2AnimRotation;
            tile.M2AnimFlipped = this.M2AnimFlipped;
            tile.Mask2Rotation = this.Mask2Rotation;
            tile.Mask2Flipped = this.Mask2Flipped;
            tile.MaskRotation = this.MaskRotation;
            tile.MaskFlipped = this.MaskFlipped;
            return tile;
        }
    }
}