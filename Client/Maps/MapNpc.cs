﻿// <copyright file="MapNpc.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Maps
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    [Serializable]
    internal class MapNpc : Graphic.Renderers.Sprites.ISprite
    {
        public MapNpc()
        {
            this.Location = default(System.Drawing.Point);
            this.Offset = default(System.Drawing.Point);
            this.VolatileStatus = new List<int>();
        }

        /// <inheritdoc/>
        public Graphic.SpriteSheet SpriteSheet
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int AttackTimer
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int TotalAttackTime { get; set; }

        /// <inheritdoc/>
        public bool Attacking
        {
            get;
            set;
        }

        // public bool Big {
        //    get;
        //    set;
        // }

        /// <inheritdoc/>
        public Enums.Direction Direction
        {
            get;
            set;
        }

        public bool Enemy
        {
            get;
            set;
        }

        public int EmotionNum
        {
            get;
            set;
        }

        public int EmotionTime
        {
            get;
            set;
        }

        public int EmotionVar
        {
            get;
            set;
        }

        public int HP
        {
            get;
            set;
        }

        public string Map
        {
            get;
            set;
        }

        public int MaxHP
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.MovementSpeed MovementSpeed
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.StatusAilment StatusAilment { get; set; }

        /// <inheritdoc/>
        public List<int> VolatileStatus
        {
            get;
            set;
        }

        public int Num
        {
            get;
            set;
        }

        public int Target
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Sprite
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Form { get; set; }
        /// <inheritdoc/>
        public Enums.Coloration Shiny { get; set; }
        /// <inheritdoc/>
        public Enums.Sex Sex { get; set; }

        /// <inheritdoc/>
        public int IdleTimer { get; set; }
        /// <inheritdoc/>
        public int IdleFrame { get; set; }
        /// <inheritdoc/>
        public int LastWalkTime { get; set; }
        /// <inheritdoc/>
        public int WalkingFrame { get; set; }

        /// <inheritdoc/>
        public System.Drawing.Point Offset
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public System.Drawing.Point Location
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int X
        {
            get
            {
                return this.Location.X;
            }

            set
            {
                this.Location = new System.Drawing.Point(value, this.Location.Y);
            }
        }

        /// <inheritdoc/>
        public int Y
        {
            get
            {
                return this.Location.Y;
            }

            set
            {
                this.Location = new System.Drawing.Point(this.Location.X, value);
            }
        }

        /// <inheritdoc/>
        public bool Leaving { get; set; }

        /// <inheritdoc/>
        public bool ScreenActive { get; set; }

        /// <inheritdoc/>
        public int SleepTimer
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int SleepFrame
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Graphic.Renderers.Sprites.SpeechBubble CurrentSpeech { get; set; }

        /// <inheritdoc/>
        public Graphic.Renderers.Sprites.Emoticon CurrentEmote { get; set; }
    }
}