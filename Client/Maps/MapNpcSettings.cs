﻿// <copyright file="MapNpcSettings.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Maps
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    [Serializable]
    internal class MapNpcSettings
    {
        public int NpcNum { get; set; }

        public int SpawnX { get; set; }

        public int SpawnY { get; set; }

        public int MinLevel { get; set; }

        public int MaxLevel { get; set; }

        public int AppearanceRate { get; set; }

        public Enums.StatusAilment StartStatus { get; set; }

        public int StartStatusCounter { get; set; }

        public int StartStatusChance { get; set; }

        public MapNpcSettings()
        {
            // If the NpcNum -2, the server will not save this MapNpc
            this.NpcNum = -2;
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (!(obj is MapNpcSettings))
            {
                return false;
            }

            MapNpcSettings npc = obj as MapNpcSettings;

            if (this.NpcNum != npc.NpcNum)
            {
                return false;
            }

            if (this.SpawnX != npc.SpawnX)
            {
                return false;
            }

            if (this.SpawnY != npc.SpawnY)
            {
                return false;
            }

            if (this.MinLevel != npc.MinLevel)
            {
                return false;
            }

            if (this.MaxLevel != npc.MaxLevel)
            {
                return false;
            }

            if (this.AppearanceRate != npc.AppearanceRate)
            {
                return false;
            }

            if (this.StartStatus != npc.StartStatus)
            {
                return false;
            }

            if (this.StartStatusCounter != npc.StartStatusCounter)
            {
                return false;
            }

            if (this.StartStatusChance != npc.StartStatusChance)
            {
                return false;
            }

            return true;
        }
    }
}
