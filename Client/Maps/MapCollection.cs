﻿// <copyright file="MapCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Maps
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class MapCollection
    {
        private Map activeMap;
        private Map mapDown;
        private Map mapLeft;
        private Map mapRight;
        private Map mapUp;

        private Map mapTopLeft;
        private Map mapBottomLeft;
        private Map mapTopRight;
        private Map mapBottomRight;

        private Map tempActiveMap;
        private Map tempUpMap;
        private Map tempDownMap;
        private Map tempLeftMap;
        private Map tempRightMap;

        private Map tempTopLeft;
        private Map tempBottomLeft;
        private Map tempTopRight;
        private Map tempBottomRight;

        internal MapCollection()
        {
        }

        public Map ActiveMap
        {
            get { return this.activeMap; }
        }

        public Map MapDown
        {
            get { return this.mapDown; }
        }

        public Map MapLeft
        {
            get { return this.mapLeft; }
        }

        public Map MapRight
        {
            get { return this.mapRight; }
        }

        public Map MapUp
        {
            get { return this.mapUp; }
        }

        public Map TempActiveMap
        {
            get { return this.tempActiveMap; }
        }

        public Map TempUpMap
        {
            get { return this.tempUpMap; }
        }

        public Map TempDownMap
        {
            get { return this.tempDownMap; }
        }

        public Map TempLeftMap
        {
            get { return this.tempLeftMap; }
        }

        public Map TempRightMap
        {
            get { return this.tempRightMap; }
        }

        public Map this[Enums.MapID id]
        {
            get { return this.GetMapFromID(id); }

            set
            {
                switch (id)
                {
                    case Enums.MapID.Active:
                        this.activeMap = value;
                        break;
                    case Enums.MapID.Down:
                        this.mapDown = value;
                        break;
                    case Enums.MapID.Left:
                        this.mapLeft = value;
                        break;
                    case Enums.MapID.Right:
                        this.mapRight = value;
                        break;
                    case Enums.MapID.Up:
                        this.mapUp = value;
                        break;

                    case Enums.MapID.TopLeft:
                        this.mapTopLeft = value;
                        break;
                    case Enums.MapID.BottomLeft:
                        this.mapBottomLeft = value;
                        break;
                    case Enums.MapID.TopRight:
                        this.mapTopRight = value;
                        break;
                    case Enums.MapID.BottomRight:
                        this.mapBottomRight = value;
                        break;

                    case Enums.MapID.TempActive:
                        this.tempActiveMap = value;
                        break;
                    case Enums.MapID.TempUp:
                        this.tempUpMap = value;
                        break;
                    case Enums.MapID.TempDown:
                        this.tempDownMap = value;
                        break;
                    case Enums.MapID.TempLeft:
                        this.tempLeftMap = value;
                        break;
                    case Enums.MapID.TempRight:
                        this.tempRightMap = value;
                        break;

                    case Enums.MapID.TempTopLeft:
                        this.tempTopLeft = value;
                        break;
                    case Enums.MapID.TempBottomLeft:
                        this.tempBottomLeft = value;
                        break;
                    case Enums.MapID.TempTopRight:
                        this.tempTopRight = value;
                        break;
                    case Enums.MapID.TempBottomRight:
                        this.tempBottomRight = value;
                        break;
                }
            }
        }

        private Map GetMapFromID(Enums.MapID id)
        {
            switch (id)
            {
                case Enums.MapID.Active:
                    return this.activeMap;
                case Enums.MapID.Down:
                    return this.mapDown;
                case Enums.MapID.Left:
                    return this.mapLeft;
                case Enums.MapID.Right:
                    return this.mapRight;
                case Enums.MapID.Up:
                    return this.mapUp;

                case Enums.MapID.TopLeft:
                    return this.mapTopLeft;
                case Enums.MapID.BottomLeft:
                    return this.mapBottomLeft;
                case Enums.MapID.TopRight:
                    return this.mapTopRight;
                case Enums.MapID.BottomRight:
                    return this.mapBottomRight;

                case Enums.MapID.TempActive:
                    return this.tempActiveMap;
                case Enums.MapID.TempUp:
                    return this.tempUpMap;
                case Enums.MapID.TempDown:
                    return this.tempDownMap;
                case Enums.MapID.TempLeft:
                    return this.tempLeftMap;
                case Enums.MapID.TempRight:
                    return this.tempRightMap;

                case Enums.MapID.TempTopLeft:
                    return this.tempTopLeft;
                case Enums.MapID.TempBottomLeft:
                    return this.tempBottomLeft;
                case Enums.MapID.TempTopRight:
                    return this.tempTopRight;
                case Enums.MapID.TempBottomRight:
                    return this.tempBottomRight;

                default:
                    return null;
            }
        }
    }
}