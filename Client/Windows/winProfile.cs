﻿// <copyright file="winProfile.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Network;

    public partial class WinProfile : Form
    {
        private bool drag = false; // determine if we should be moving the form
        private Point startPoint = new Point(0, 0); // also for the moving

        private bool toolTipShown = false;
        private readonly ToolTip partyTip;

        private readonly string foundName;
        private readonly string charID;
        private Image foundImage;
        private string blurb;
        private readonly string party;
        private readonly string friends;
        private WinPM pm;

        public WinProfile(string foundName, string foundcharID, Image foundImage, string foundBlurb, string partyStatus, string friendStatus)
        {
            this.InitializeComponent();

            this.pbHeader.MouseDown += new MouseEventHandler(this.Title_MouseDown);
            this.pbHeader.MouseUp += new MouseEventHandler(this.Title_MouseUp);
            this.pbHeader.MouseMove += new MouseEventHandler(this.Title_MouseMove);

            this.MouseMove += new MouseEventHandler(this.WinProfile_MouseMove);

            this.pbClose.Image = Image.FromFile(Path.Combine(Application.StartupPath, "Skins/" + Skins.SkinManager.ActiveSkin.Name + "/Widgets/Windows/closebutton.png"));

            if (friendStatus == "2")
            {
                this.rtbBlurb.ReadOnly = false;
            }

            Players.IPlayer foundplayer = null;
            foreach (Players.IPlayer player in Players.PlayerManager.Players.GetAllPlayers())
            {
                if (player.Name == foundName)
                {
                    foundplayer = player;
                    break;
                }
            }

            if (foundplayer == null)
            {
                Messenger.SendPrivateMessage("[Alert to Coding Buizel!!!] PM couldn't find the name " + this.foundName + "!", "JordantheBuizel");
                Messenger.SendPrivateMessage("[Error: PM] Tell Jordan about to give a look for the error occuring with PMs and " + this.foundName, "riko");
                Messenger.SendPrivateMessage("[Error: PM] Tell Jordan about to give a look for the error occuring with PMs and " + this.foundName, "Gale");
            }

            this.lblRank.Text = (Ranks.IsDisallowed(foundplayer, Enums.Rank.Moniter) || foundplayer == null) ? string.Empty : Enum.GetName(typeof(Enums.Rank), foundplayer.Access);
            this.lblRank.ForeColor = Ranks.GetRankColor(foundplayer.Access);

            this.foundName = foundName;
            this.charID = foundcharID;
            this.foundImage = foundImage;
            this.blurb = foundBlurb;
            this.party = partyStatus;
            this.friends = friendStatus;

            this.pbProfile.Image = foundImage;
            this.label1.Text = foundName;
            this.rtbBlurb.AppendText(foundBlurb);

            switch (partyStatus)
            {
                case "0":
                    {
                        this.partyTip = new ToolTip();
                        this.partyTip.SetToolTip(this.btnAddParty, "Click to send request to have " + foundName + " join your party.");
                    }

                    break;
                case "1":
                    {
                        this.partyTip = new ToolTip();
                        this.partyTip.SetToolTip(this.btnAddParty, foundName + " is already in a party!");
                        this.btnAddParty.Enabled = false;
                    }

                    break;
                case "2":
                    {
                        this.partyTip = new ToolTip();
                        this.partyTip.SetToolTip(this.btnAddParty, foundName + " is already in your party.");
                        this.btnAddParty.Enabled = false;
                    }

                    break;
                case "3":
                    {
                        this.partyTip = new ToolTip();
                        this.partyTip.SetToolTip(this.btnAddParty, "Your party is full!");
                        this.btnAddFriend.Enabled = false;
                    }

                    break;
                case "4":
                    {
                        this.partyTip = new ToolTip();
                        this.partyTip.SetToolTip(this.btnAddParty, "You do not have a party! Clicking this will create a party, then send a join request to " + foundName + "!");
                    }

                    break;
            }

            if (friendStatus == "1")
            {
                this.btnAddFriend.Text = "Remove Friend";
            }
            else if (friendStatus == "2")
            {
                this.btnAddFriend.Text = "Update Blurb";
            }
        }

        private void WinProfile_MouseMove(object sender, MouseEventArgs e)
        {
            var parent = sender as Control;
            if (parent == null)
            {
                return;
            }

            var ctrl = parent.GetChildAtPoint(e.Location);
            if (ctrl == this.btnAddParty && !this.btnAddParty.Enabled)
            {
                if (ctrl.Visible && this.partyTip.Tag == null)
                {
                    if (!this.toolTipShown)
                    {
                        var tipstring = this.partyTip.GetToolTip(ctrl);
                        this.partyTip.Show(tipstring.Trim(), ctrl, ctrl.Width / 2, ctrl.Height / 2);
                        this.partyTip.Tag = ctrl;
                        this.toolTipShown = true;
                    }
                }
            }
            else
            {
                ctrl = this.partyTip.Tag as Control;
                if (ctrl != null)
                {
                    this.partyTip.Hide(ctrl);
                    this.partyTip.Tag = null;
                    this.toolTipShown = false;
                }
            }
        }

        private void BtnAddFriend_Click(object sender, EventArgs e)
        {
            if (this.friends == "0")
            {
                Messenger.AddFriend(this.foundName);
                this.btnAddFriend.Text = "Remove Friend";
            }
            else if (this.friends == "1")
            {
                Messenger.RemoveFriend(this.foundName);
                this.btnAddFriend.Text = "Add Friend";
            }
            else
            {
                Messenger.SendProfileBlurb(this.rtbBlurb.Text);
            }
        }

        private void BtnPM_Click(object sender, EventArgs e)
        {
            if (this.pm == null && WinPM.Instance == null)
            {
                this.pm = new WinPM(this.foundName);
                this.pm.Show();
            }
        }

        private void BtnAddParty_Click(object sender, EventArgs e)
        {
            if (this.party == "4")
            {
                CommandProcessor.ProcessCommand("/createparty", Enums.ChatChannel.Local);
            }

            Messenger.SendPartyInvite(this.charID);
        }

        private void PbProfile_Click(object sender, EventArgs e)
        {
            if (this.foundName == Players.PlayerManager.MyPlayer.Name)
            {
                OpenFileDialog dia = new OpenFileDialog();
                if (dia.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Bitmap bmp = new Bitmap(dia.FileName);
                        this.pbProfile.Image = bmp;
                        Messenger.SendProfileImage(bmp);
                    }
                    catch
                    {
                        MessageBox.Show("The file you selected is not supported. Please try another.");
                    }
                }
            }
        }

        private void PbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Title_MouseUp(object sender, MouseEventArgs e)
        {
            this.drag = false;
        }

        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            this.startPoint = e.Location;
            this.drag = true;
        }

        private void Title_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.drag)
            { // if we should be dragging it, we need to figure out some movement
                Point p1 = new Point(e.X, e.Y);
                Point p2 = this.PointToScreen(p1);
                Point p3 = new Point(
                    p2.X - this.startPoint.X,
                    p2.Y - this.startPoint.Y);
                this.Location = p3;
            }
        }
    }
}
