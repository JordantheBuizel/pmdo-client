﻿namespace Client.Logic.Windows
{
    partial class WinProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbProfile = new System.Windows.Forms.PictureBox();
            this.rtbBlurb = new System.Windows.Forms.RichTextBox();
            this.pbHeader = new System.Windows.Forms.PictureBox();
            this.btnAddFriend = new System.Windows.Forms.Button();
            this.btnPM = new System.Windows.Forms.Button();
            this.btnAddParty = new System.Windows.Forms.Button();
            this.pbClose = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblRank = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbProfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).BeginInit();
            this.SuspendLayout();
            // 
            // pbProfile
            // 
            this.pbProfile.BackColor = System.Drawing.Color.Transparent;
            this.pbProfile.Location = new System.Drawing.Point(16, 46);
            this.pbProfile.Name = "pbProfile";
            this.pbProfile.Size = new System.Drawing.Size(120, 120);
            this.pbProfile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbProfile.TabIndex = 0;
            this.pbProfile.TabStop = false;
            this.pbProfile.Click += new System.EventHandler(this.PbProfile_Click);
            // 
            // rtbBlurb
            // 
            this.rtbBlurb.BackColor = System.Drawing.SystemColors.Control;
            this.rtbBlurb.Location = new System.Drawing.Point(142, 46);
            this.rtbBlurb.Name = "rtbBlurb";
            this.rtbBlurb.ReadOnly = true;
            this.rtbBlurb.Size = new System.Drawing.Size(437, 286);
            this.rtbBlurb.TabIndex = 1;
            this.rtbBlurb.Text = "";
            // 
            // pbHeader
            // 
            this.pbHeader.BackColor = System.Drawing.Color.Transparent;
            this.pbHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbHeader.Location = new System.Drawing.Point(0, 0);
            this.pbHeader.Name = "pbHeader";
            this.pbHeader.Size = new System.Drawing.Size(600, 26);
            this.pbHeader.TabIndex = 2;
            this.pbHeader.TabStop = false;
            // 
            // btnAddFriend
            // 
            this.btnAddFriend.AutoSize = true;
            this.btnAddFriend.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAddFriend.Location = new System.Drawing.Point(33, 353);
            this.btnAddFriend.Name = "btnAddFriend";
            this.btnAddFriend.Size = new System.Drawing.Size(68, 23);
            this.btnAddFriend.TabIndex = 3;
            this.btnAddFriend.Text = "Add Friend";
            this.btnAddFriend.UseVisualStyleBackColor = true;
            this.btnAddFriend.Click += new System.EventHandler(this.BtnAddFriend_Click);
            // 
            // btnPM
            // 
            this.btnPM.AutoSize = true;
            this.btnPM.Location = new System.Drawing.Point(234, 353);
            this.btnPM.Name = "btnPM";
            this.btnPM.Size = new System.Drawing.Size(96, 23);
            this.btnPM.TabIndex = 4;
            this.btnPM.Text = "Private Message";
            this.btnPM.UseVisualStyleBackColor = true;
            this.btnPM.Click += new System.EventHandler(this.BtnPM_Click);
            // 
            // btnAddParty
            // 
            this.btnAddParty.AutoSize = true;
            this.btnAddParty.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAddParty.Location = new System.Drawing.Point(468, 353);
            this.btnAddParty.Name = "btnAddParty";
            this.btnAddParty.Size = new System.Drawing.Size(75, 23);
            this.btnAddParty.TabIndex = 5;
            this.btnAddParty.Text = "Add to Party";
            this.btnAddParty.UseVisualStyleBackColor = true;
            this.btnAddParty.Click += new System.EventHandler(this.BtnAddParty_Click);
            // 
            // pbClose
            // 
            this.pbClose.Location = new System.Drawing.Point(570, 0);
            this.pbClose.Name = "pbClose";
            this.pbClose.Size = new System.Drawing.Size(30, 16);
            this.pbClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbClose.TabIndex = 6;
            this.pbClose.TabStop = false;
            this.pbClose.Click += new System.EventHandler(this.PbClose_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(15, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "{NAME}";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRank
            // 
            this.lblRank.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblRank.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblRank.Location = new System.Drawing.Point(15, 187);
            this.lblRank.Name = "lblRank";
            this.lblRank.Size = new System.Drawing.Size(121, 18);
            this.lblRank.TabIndex = 7;
            this.lblRank.Text = "{RANK}";
            this.lblRank.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // winProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::Client.Logic.Properties.Resources.menu_horizontal_border;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(600, 408);
            this.Controls.Add(this.lblRank);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbClose);
            this.Controls.Add(this.btnAddParty);
            this.Controls.Add(this.btnPM);
            this.Controls.Add(this.btnAddFriend);
            this.Controls.Add(this.pbHeader);
            this.Controls.Add(this.rtbBlurb);
            this.Controls.Add(this.pbProfile);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "winProfile";
            this.Text = "winProfile";
            ((System.ComponentModel.ISupportInitialize)(this.pbProfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbProfile;
        private System.Windows.Forms.RichTextBox rtbBlurb;
        private System.Windows.Forms.PictureBox pbHeader;
        private System.Windows.Forms.Button btnAddFriend;
        private System.Windows.Forms.Button btnPM;
        private System.Windows.Forms.Button btnAddParty;
        private System.Windows.Forms.PictureBox pbClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRank;
    }
}