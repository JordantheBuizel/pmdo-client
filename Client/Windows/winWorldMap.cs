﻿// <copyright file="winWorldMap.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Drawing = System.Drawing;

    public partial class WinWorldMap : Form
    {
        private static readonly float dScrollValue = .01F;
        private readonly Matrix transform = new Matrix();

        public WinWorldMap(Image map)
        {
            this.InitializeComponent();

            this.pbMap.Paint += new PaintEventHandler(this.PbMap_OnPaint);
            this.pbMap.Image = map;
        }

        /// <inheritdoc/>
        protected override void OnMouseWheel(MouseEventArgs mea)
        {
            this.pbMap.Focus();
            if (this.pbMap.Focused == true && mea.Delta != 0)
            {
                this.ZoomScroll(mea.Location, mea.Delta > 0);
            }
        }

        private void ZoomScroll(Point location, bool zoomIn)
        {
            // make zoom-point (cursor location) our origin
            this.transform.Translate(-location.X, -location.Y);

            // perform zoom (at origin)
            if (zoomIn)
            {
                this.transform.Scale(dScrollValue, dScrollValue);
            }
            else
            {
                this.transform.Scale(-dScrollValue, -dScrollValue);
            }

            // translate origin back to cursor
            this.transform.Translate(location.X, location.Y);

            this.pbMap.Invalidate();
        }

        private void PbMap_OnPaint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.Transform = this.transform;
        }
    }
}
