﻿// <copyright file="winDeleteAccount.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Network;
    using SdlDotNet.Widgets;

    internal class WinDeleteAccount : Core.WindowCore
    {
        private readonly Label lblName;
        private readonly Label lblPassword;
        private readonly Label lblDelete;
        private readonly Label lblBack;
        private readonly TextBox txtName;
        private readonly TextBox txtPassword;

        public WinDeleteAccount()
            : base("winDeleteAccount")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.TitleBar.Text = "Delete Account";
            this.TitleBar.CloseButton.Visible = false;
            this.BackgroundImage = Skins.SkinManager.LoadGui("Delete Account");
            this.Size = this.BackgroundImage.Size;
            this.Location = DrawingSupport.GetCenter(SdlDotNet.Graphics.Video.Screen.Size, this.Size);

            this.lblBack = new Label("lblBack");
            this.lblBack.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblBack.Location = new Point(140, 355);
            this.lblBack.AutoSize = true;
            this.lblBack.ForeColor = Color.Black;
            this.lblBack.Text = "Back to Login Screen";
            this.lblBack.Click += new EventHandler<MouseButtonEventArgs>(this.LblBack_Click);

            this.lblName = new Label("lblName");
            this.lblName.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblName.Location = new Point(28, 97);
            this.lblName.AutoSize = true;
            this.lblName.ForeColor = Color.Black;
            this.lblName.Text = "Account Name";

            this.lblPassword = new Label("lblPassword");
            this.lblPassword.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblPassword.Location = new Point(32, 143);
            this.lblPassword.AutoSize = true;
            this.lblPassword.ForeColor = Color.Black;
            this.lblPassword.Text = "Password";

            this.lblDelete = new Label("lblDelete");
            this.lblDelete.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblDelete.Location = new Point(35, 190);
            this.lblDelete.AutoSize = true;
            this.lblDelete.ForeColor = Color.Black;
            this.lblDelete.Text = "Delete Account";
            this.lblDelete.Click += new EventHandler<MouseButtonEventArgs>(this.LblDelete_Click);

            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(165, 16);
            this.txtName.Location = new Point(30, 118);

            this.txtPassword = new TextBox("txtPassword");
            this.txtPassword.Size = new Size(165, 16);
            this.txtPassword.Location = new Point(30, 164);

            this.AddWidget(this.lblBack);
            this.AddWidget(this.lblName);
            this.AddWidget(this.lblPassword);
            this.AddWidget(this.lblDelete);
            this.AddWidget(this.txtName);
            this.AddWidget(this.txtPassword);

            this.LoadComplete();
        }

        private void LblBack_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            WindowSwitcher.ShowMainMenu();
        }

        private void LblDelete_Click(object sender, MouseButtonEventArgs e)
        {
            string account = this.txtName.Text;
            string password = this.txtPassword.Text;

            if (NetworkManager.TcpClient.Socket.Connected)
            {
                Messenger.SendDeleteAccount(account, password);
                this.Close();
            }
else
            {
                this.Close();
                WindowSwitcher.ShowMainMenu();
                MessageBox.Show("You are not connected to the Server!", "----");
            }
        }
    }
}
