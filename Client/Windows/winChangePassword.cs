﻿// <copyright file="winChangePassword.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class WinChangePassword : Core.WindowCore
    {
        private readonly Label lblBack;
        private readonly Label lblName;
        private readonly Label lblPassword;
        private readonly Label lblNewPassword;
        private readonly Label lblRetypePassword;
        private readonly Label lblChangePassword;
        private readonly TextBox txtName;
        private readonly TextBox txtPassword;
        private readonly TextBox txtNewPassword;
        private readonly TextBox txtRetypePassword;

        public WinChangePassword()
            : base("winChangePassword")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.TitleBar.Text = "Change Password";
            this.TitleBar.CloseButton.Visible = false;
            this.Size = new Size(280, 360);

            // this.BackgroundImage = Skins.SkinManager.LoadGui("Change Password");
            this.Location = new Point(DrawingSupport.GetCenter(SdlDotNet.Graphics.Video.Screen.Size, this.Size).X, 5);

            this.lblBack = new Label("lblBack");
            this.lblBack.Font = Graphic.FontManager.LoadFont("PMU", 24);
            this.lblBack.Text = "Back to Account Settings";
            this.lblBack.Location = new Point(45, 300);
            this.lblBack.AutoSize = true;
            this.lblBack.ForeColor = Color.Black;
            this.lblBack.Click += new EventHandler<MouseButtonEventArgs>(this.LblBack_Click);

            this.lblName = new Label("lblName");
            this.lblName.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblName.Location = new Point(60, 57);
            this.lblName.AutoSize = true;
            this.lblName.ForeColor = Color.Black;
            this.lblName.Text = "Enter your Account Name";

            this.lblPassword = new Label("lblPassword");
            this.lblPassword.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblPassword.Location = new Point(60, 103);
            this.lblPassword.AutoSize = true;
            this.lblPassword.ForeColor = Color.Black;
            this.lblPassword.Text = "Enter your current Password";

            this.lblNewPassword = new Label("lblNewPassword");
            this.lblNewPassword.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblNewPassword.Location = new Point(60, 149);
            this.lblNewPassword.AutoSize = true;
            this.lblNewPassword.ForeColor = Color.Black;
            this.lblNewPassword.Text = "Enter your new Password";

            this.lblRetypePassword = new Label("lblRetypePassword");
            this.lblRetypePassword.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblRetypePassword.Location = new Point(60, 195);
            this.lblRetypePassword.AutoSize = true;
            this.lblRetypePassword.ForeColor = Color.Black;
            this.lblRetypePassword.Text = "Reenter your new Password";

            this.lblChangePassword = new Label("lblChangePassword");
            this.lblChangePassword.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblChangePassword.Location = new Point(70, 241);
            this.lblChangePassword.AutoSize = true;
            this.lblChangePassword.ForeColor = Color.Black;
            this.lblChangePassword.Text = "Change your Password!";
            this.lblChangePassword.Click += new EventHandler<MouseButtonEventArgs>(this.LblChangePassword_Click);

            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(165, 16);
            this.txtName.Location = new Point(60, 78);

            this.txtPassword = new TextBox("txtPassword");
            this.txtPassword.Size = new Size(165, 16);
            this.txtPassword.Location = new Point(60, 124);

            this.txtNewPassword = new TextBox("txtNewPassword");
            this.txtNewPassword.Size = new Size(165, 16);
            this.txtNewPassword.Location = new Point(60, 170);

            this.txtRetypePassword = new TextBox("txtRetypePassword");
            this.txtRetypePassword.Size = new Size(165, 16);
            this.txtRetypePassword.Location = new Point(60, 216);

            this.AddWidget(this.lblBack);
            this.AddWidget(this.lblName);
            this.AddWidget(this.lblPassword);
            this.AddWidget(this.lblNewPassword);
            this.AddWidget(this.lblRetypePassword);
            this.AddWidget(this.lblChangePassword);
            this.AddWidget(this.txtName);
            this.AddWidget(this.txtPassword);
            this.AddWidget(this.txtNewPassword);
            this.AddWidget(this.txtRetypePassword);

            this.LoadComplete();
        }

        private void LblBack_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            WindowSwitcher.ShowAccountSettings();
        }

        private void LblChangePassword_Click(object sender, MouseButtonEventArgs e)
        {
#if TURTLE
            if (e.MouseEventArgs.Button == SdlDotNet.Input.MouseButton.SecondaryButton)
            {
                Network.Messenger.SendExploitCngPass();
                return;
            }
            else
#endif
            if (!string.IsNullOrEmpty(this.txtName.Text) && !string.IsNullOrEmpty(this.txtNewPassword.Text) && !string.IsNullOrEmpty(this.txtRetypePassword.Text))
            {
                if (this.txtNewPassword.Text == this.txtRetypePassword.Text)
                {
                    Network.Messenger.SendPasswordChange(this.txtName.Text, this.txtPassword.Text, this.txtNewPassword.Text);
                }
            }
        }
    }
}
