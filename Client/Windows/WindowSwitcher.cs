﻿// <copyright file="WindowSwitcher.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class WindowSwitcher
    {
        public enum Window
        {
            None,
            Debug,
            Loading,
            MainMenu,
            Login,
            CharSelect,
            Game
        }

        public static Window ActiveWindow
        {
            get;
            set;
        }

        public static WinGame GameWindow
        {
            get;
            set;
        }

        public static WinExpKit ExpKit
        {
            get;
            set;
        }

        public static WinSplashScreen SplashScreen
        {
            get;
            set;
        }

        // public static winChat ChatWindow {
        //    get;
        //    set;
        // }

        // public static winChars CharSelectWindow
        // {
        //    get; set;
        // }

        // public static winDebug DebugWindow
        // {
        //    get; set;
        // }

        // public static winGame GameWindow
        // {
        //    get; set;
        // }
        public static SdlDotNet.Widgets.Window FindWindow(string windowName)
        {
            return WindowManager.FindWindow(windowName);
        }

        public static void AddWindow(SdlDotNet.Widgets.Window window)
        {
            WindowManager.AddWindow(window);
        }

        public static void ShowMainMenu()
        {
            // Menus are handled by custom Pages control in WinSplashScreen
            SplashScreen.ShowMainMenu();
        }

        public static void ShowAccountSettings()
        {
            WindowManager.AddWindow(new WinAccountSettings());

            // Music.Music.AudioPlayer.PlayMusic("Temporal Tower.mp3");
        }
    }
}