﻿// <copyright file="winEvolutionPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using PMU.Core;
    using PMU.Sockets;
    using SdlDotNet.Graphics;
    using SdlDotNet.Widgets;

    internal class WinEvolutionPanel : Core.WindowCore
    {
        private int evoNum = -1;
        private int currentTen = 0;
        private Evolutions.Evolution evolution;

        private readonly Panel pnlEvoList;
        private readonly Panel pnlEvoEditor;

        private readonly ScrollingListBox lbxEvoList;
        private readonly Button btnBack;
        private readonly Button btnForward;
        private readonly Button btnCancel;
        private readonly Button btnEdit;

        private readonly Button btnEditorCancel;
        private readonly Button btnEditorOK;

        private readonly PictureBox picSprite;
        private readonly PictureBox picNewSprite;
        private readonly Label lblArrow;
        private readonly Label lblName;
        private readonly TextBox txtName;
        private readonly Label lblSprite;
        private readonly NumericUpDown nudSpecies;

        private readonly Label lblMaxBranchEvos;
        private readonly NumericUpDown nudMaxBranchEvos;

        private readonly Button btnEvoBranchLoad;
        private readonly Button btnEvoBranchSave;
        private readonly Label lblSaveLoadMessage;
        private readonly Label lblEvoNum;
        private readonly NumericUpDown nudEvoNum;
        private readonly Label lblNewName;
        private readonly TextBox txtNewName;
        private readonly Label lblNewSprite;
        private readonly NumericUpDown nudNewSpecies;
        private readonly Label lblReqScript;
        private readonly NumericUpDown nudReqScript;

        private readonly Label lblData1;
        private readonly NumericUpDown nudData1;
        private readonly Label lblData2;
        private readonly NumericUpDown nudData2;
        private readonly Label lblData3;
        private readonly NumericUpDown nudData3;

        public WinEvolutionPanel()
            : base("winEvolutionPanel")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new Size(200, 230);
            this.Location = new Point(210, Windows.WindowSwitcher.GameWindow.ActiveTeam.Y + Windows.WindowSwitcher.GameWindow.ActiveTeam.Height + 0);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = true;
            this.TitleBar.Font = FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "Evolution Panel";

            this.pnlEvoList = new Panel("pnlEvoList");
            this.pnlEvoList.Size = new Size(200, 230);
            this.pnlEvoList.Location = new Point(0, 0);
            this.pnlEvoList.BackColor = Color.White;
            this.pnlEvoList.Visible = true;

            this.pnlEvoEditor = new Panel("pnlEvoEditor");
            this.pnlEvoEditor.Size = new Size(360, 400);
            this.pnlEvoEditor.Location = new Point(0, 0);
            this.pnlEvoEditor.BackColor = Color.White;
            this.pnlEvoEditor.Visible = false;

            this.lbxEvoList = new ScrollingListBox("lbxEvoList");
            this.lbxEvoList.Location = new Point(10, 10);
            this.lbxEvoList.Size = new Size(180, 140);
            for (int i = 0; i < 10; i++)
            {
                this.lbxEvoList.Items.Add(new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), (i + 1) + ": " + Evolutions.EvolutionHelper.Evolutions[i].Name));
            }

            this.lbxEvoList.SelectItem(0);

            this.btnBack = new Button("btnBack");
            this.btnBack.Location = new Point(10, 160);
            this.btnBack.Font = FontManager.LoadFont("tahoma", 10);
            this.btnBack.Size = new Size(64, 16);
            this.btnBack.Visible = true;
            this.btnBack.Text = "<--";
            this.btnBack.Click += new EventHandler<MouseButtonEventArgs>(this.BtnBack_Click);

            this.btnForward = new Button("btnForward");
            this.btnForward.Location = new Point(126, 160);
            this.btnForward.Font = FontManager.LoadFont("tahoma", 10);
            this.btnForward.Size = new Size(64, 16);
            this.btnForward.Visible = true;
            this.btnForward.Text = "-->";
            this.btnForward.Click += new EventHandler<MouseButtonEventArgs>(this.BtnForward_Click);

            this.btnEdit = new Button("btnEdit");
            this.btnEdit.Location = new Point(10, 190);
            this.btnEdit.Font = FontManager.LoadFont("tahoma", 10);
            this.btnEdit.Size = new Size(64, 16);
            this.btnEdit.Visible = true;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEdit_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(126, 190);
            this.btnCancel.Font = FontManager.LoadFont("tahoma", 10);
            this.btnCancel.Size = new Size(64, 16);
            this.btnCancel.Visible = true;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            // btnAddNew = new Button("btnAddNew");
            // btnAddNew.Location = new Point();
            // btnAddNew.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            // btnAddNew.Size = new System.Drawing.Size(64, 16);
            // btnAddNew.Visible = true;
            // btnAddNew.Text = "Add New";
            // btnAddNew.Click += new EventHandler<MouseButtonEventArgs>(btnAddNew_Click);
            this.btnEditorCancel = new Button("btnEditorCancel");
            this.btnEditorCancel.Location = new Point(100, 354);
            this.btnEditorCancel.Font = FontManager.LoadFont("tahoma", 10);
            this.btnEditorCancel.Size = new Size(64, 16);
            this.btnEditorCancel.Visible = true;
            this.btnEditorCancel.Text = "Cancel";
            this.btnEditorCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorCancel_Click);

            this.btnEditorOK = new Button("btnEditorOK");
            this.btnEditorOK.Location = new Point(10, 354);
            this.btnEditorOK.Font = FontManager.LoadFont("tahoma", 10);
            this.btnEditorOK.Size = new Size(64, 16);
            this.btnEditorOK.Visible = true;
            this.btnEditorOK.Text = "OK";
            this.btnEditorOK.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorOK_Click);

            // PictureBox picSprite;
            this.picSprite = new PictureBox("picSprite");
            this.picSprite.Size = new Size(32, 32);
            this.picSprite.BackColor = Color.Transparent;
            this.picSprite.Image = new Surface(32, 32);
            this.picSprite.X = 175 - this.picSprite.Width;
            this.picSprite.Y = 120 - (this.picSprite.Height / 2);

            // PictureBox picNewSprite;
            this.picNewSprite = new PictureBox("picNewSprite");
            this.picNewSprite.Size = new Size(32, 32);
            this.picNewSprite.BackColor = Color.Transparent;
            this.picNewSprite.Image = new Surface(32, 32);
            this.picNewSprite.X = 185;
            this.picNewSprite.Y = 120 - (this.picNewSprite.Height / 2);

            this.lblName = new Label("lblName");
            this.lblName.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblName.Text = "Name:";
            this.lblName.AutoSize = true;
            this.lblName.Location = new Point(10, 4);

            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(200, 16);
            this.txtName.Location = new Point(10, 16);

            this.lblSprite = new Label("lblSprite");
            this.lblSprite.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblSprite.Text = "Species:";
            this.lblSprite.AutoSize = true;
            this.lblSprite.Location = new Point(60, 36);

            this.nudSpecies = new NumericUpDown("nudSpecies");
            this.nudSpecies.Minimum = 0;
            this.nudSpecies.Maximum = int.MaxValue;
            this.nudSpecies.Size = new Size(80, 20);
            this.nudSpecies.Location = new Point(60, 50);
            this.nudSpecies.Font = FontManager.LoadFont("tahoma", 10);
            this.nudSpecies.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudSpecies_ValueChanged);

            this.lblMaxBranchEvos = new Label("lblMaxBranchEvos");
            this.lblMaxBranchEvos.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblMaxBranchEvos.Text = "Max Branch Evos:";
            this.lblMaxBranchEvos.AutoSize = true;
            this.lblMaxBranchEvos.Location = new Point(200, 36);

            this.nudMaxBranchEvos = new NumericUpDown("nudMaxBranchEvos");
            this.nudMaxBranchEvos.Minimum = 0;
            this.nudMaxBranchEvos.Maximum = int.MaxValue;
            this.nudMaxBranchEvos.Size = new Size(80, 20);
            this.nudMaxBranchEvos.Location = new Point(200, 50);
            this.nudMaxBranchEvos.Font = FontManager.LoadFont("tahoma", 10);

            this.lblEvoNum = new Label("lblEvoNum");
            this.lblEvoNum.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblEvoNum.Text = "Evolution Branch #:";
            this.lblEvoNum.AutoSize = true;
            this.lblEvoNum.Location = new Point(30, 170);

            this.nudEvoNum = new NumericUpDown("nudEvoNum");
            this.nudEvoNum.Minimum = 1;
            this.nudEvoNum.Maximum = int.MaxValue;
            this.nudEvoNum.Size = new Size(80, 20);
            this.nudEvoNum.Location = new Point(30, 184);
            this.nudEvoNum.Font = FontManager.LoadFont("tahoma", 10);

            this.btnEvoBranchLoad = new Button("btnEvoBranchLoad");
            this.btnEvoBranchLoad.Location = new Point(150, 180);
            this.btnEvoBranchLoad.Font = FontManager.LoadFont("tahoma", 10);
            this.btnEvoBranchLoad.Size = new Size(80, 16);
            this.btnEvoBranchLoad.Visible = true;
            this.btnEvoBranchLoad.Text = "Load Branch";
            this.btnEvoBranchLoad.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEvoBranchLoad_Click);

            this.btnEvoBranchSave = new Button("btnEvoBranchSave");
            this.btnEvoBranchSave.Location = new Point(250, 180);
            this.btnEvoBranchSave.Font = FontManager.LoadFont("tahoma", 10);
            this.btnEvoBranchSave.Size = new Size(80, 16);
            this.btnEvoBranchSave.Visible = true;
            this.btnEvoBranchSave.Text = "Save Branch";
            this.btnEvoBranchSave.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEvoBranchSave_Click);

            this.lblSaveLoadMessage = new Label("lblSaveLoadMessage");
            this.lblSaveLoadMessage.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblSaveLoadMessage.Text = "---";
            this.lblSaveLoadMessage.AutoSize = true;
            this.lblSaveLoadMessage.Location = new Point(70, 212);

            this.lblArrow = new Label("lblArrow");
            this.lblArrow.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblArrow.Text = "->";
            this.lblArrow.AutoSize = true;
            this.lblArrow.Location = new Point(176, 120);

            this.lblNewName = new Label("lblNewName");
            this.lblNewName.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblNewName.Text = "New Name:";
            this.lblNewName.AutoSize = true;
            this.lblNewName.Location = new Point(10, 234);

            this.txtNewName = new TextBox("txtNewName");
            this.txtNewName.Size = new Size(200, 16);
            this.txtNewName.Location = new Point(10, 250);

            this.lblNewSprite = new Label("lblNewSprite");
            this.lblNewSprite.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblNewSprite.Text = "New Species:";
            this.lblNewSprite.AutoSize = true;
            this.lblNewSprite.Location = new Point(60, 270);

            this.nudNewSpecies = new NumericUpDown("nudNewSpecies");
            this.nudNewSpecies.Minimum = 0;
            this.nudNewSpecies.Maximum = int.MaxValue;
            this.nudNewSpecies.Size = new Size(80, 20);
            this.nudNewSpecies.Location = new Point(60, 286);
            this.nudNewSpecies.Font = FontManager.LoadFont("tahoma", 10);
            this.nudNewSpecies.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudNewSpecies_ValueChanged);

            this.lblReqScript = new Label("lblReqScript");
            this.lblReqScript.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblReqScript.Text = "Requirement Script:";
            this.lblReqScript.AutoSize = true;
            this.lblReqScript.Location = new Point(200, 270);

            this.nudReqScript = new NumericUpDown("nudReqScript");
            this.nudReqScript.Minimum = 0;
            this.nudReqScript.Maximum = int.MaxValue;
            this.nudReqScript.Size = new Size(80, 20);
            this.nudReqScript.Location = new Point(200, 286);
            this.nudReqScript.Font = FontManager.LoadFont("tahoma", 10);

            this.lblData1 = new Label("lblData1");
            this.lblData1.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblData1.Text = "Data 1:";
            this.lblData1.AutoSize = true;
            this.lblData1.Location = new Point(30, 306);

            this.nudData1 = new NumericUpDown("nudData1");
            this.nudData1.Minimum = int.MinValue;
            this.nudData1.Maximum = int.MaxValue;
            this.nudData1.Size = new Size(80, 20);
            this.nudData1.Location = new Point(30, 320);
            this.nudData1.Font = FontManager.LoadFont("tahoma", 10);

            this.lblData2 = new Label("lblData2");
            this.lblData2.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblData2.Text = "Data 2:";
            this.lblData2.AutoSize = true;
            this.lblData2.Location = new Point(140, 306);

            this.nudData2 = new NumericUpDown("nudData2");
            this.nudData2.Minimum = int.MinValue;
            this.nudData2.Maximum = int.MaxValue;
            this.nudData2.Size = new Size(80, 20);
            this.nudData2.Location = new Point(140, 320);
            this.nudData2.Font = FontManager.LoadFont("tahoma", 10);

            this.lblData3 = new Label("lblData3");
            this.lblData3.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblData3.Text = "Data 3:";
            this.lblData3.AutoSize = true;
            this.lblData3.Location = new Point(250, 306);

            this.nudData3 = new NumericUpDown("nudData3");
            this.nudData3.Minimum = int.MinValue;
            this.nudData3.Maximum = int.MaxValue;
            this.nudData3.Size = new Size(80, 20);
            this.nudData3.Location = new Point(250, 320);
            this.nudData3.Font = FontManager.LoadFont("tahoma", 10);
            this.pnlEvoList.AddWidget(this.lbxEvoList);
            this.pnlEvoList.AddWidget(this.btnBack);
            this.pnlEvoList.AddWidget(this.btnForward);

            // pnlEvoList.AddWidget(btnAddNew);
            this.pnlEvoList.AddWidget(this.btnEdit);
            this.pnlEvoList.AddWidget(this.btnCancel);

            this.pnlEvoEditor.AddWidget(this.picSprite);
            this.pnlEvoEditor.AddWidget(this.picNewSprite);
            this.pnlEvoEditor.AddWidget(this.lblName);
            this.pnlEvoEditor.AddWidget(this.txtName);
            this.pnlEvoEditor.AddWidget(this.lblSprite);

            this.pnlEvoEditor.AddWidget(this.nudSpecies);

            this.pnlEvoEditor.AddWidget(this.lblMaxBranchEvos);
            this.pnlEvoEditor.AddWidget(this.nudMaxBranchEvos);

            //
            this.pnlEvoEditor.AddWidget(this.lblEvoNum);
            this.pnlEvoEditor.AddWidget(this.nudEvoNum);
            this.pnlEvoEditor.AddWidget(this.btnEvoBranchLoad);
            this.pnlEvoEditor.AddWidget(this.btnEvoBranchSave);
            this.pnlEvoEditor.AddWidget(this.lblSaveLoadMessage);
            this.pnlEvoEditor.AddWidget(this.lblArrow);
            this.pnlEvoEditor.AddWidget(this.lblNewName);
            this.pnlEvoEditor.AddWidget(this.txtNewName);
            this.pnlEvoEditor.AddWidget(this.lblNewSprite);
            this.pnlEvoEditor.AddWidget(this.nudNewSpecies);
            this.pnlEvoEditor.AddWidget(this.lblReqScript);
            this.pnlEvoEditor.AddWidget(this.lblData1);
            this.pnlEvoEditor.AddWidget(this.lblData2);
            this.pnlEvoEditor.AddWidget(this.lblData3);
            this.pnlEvoEditor.AddWidget(this.nudReqScript);
            this.pnlEvoEditor.AddWidget(this.nudData1);
            this.pnlEvoEditor.AddWidget(this.nudData2);
            this.pnlEvoEditor.AddWidget(this.nudData3);

            this.pnlEvoEditor.AddWidget(this.btnEditorCancel);
            this.pnlEvoEditor.AddWidget(this.btnEditorOK);

            this.AddWidget(this.pnlEvoList);
            this.AddWidget(this.pnlEvoEditor);

            this.LoadComplete();
        }

        public void LoadEvo(string[] parse)
        {
            this.evolution = new Evolutions.Evolution();
            this.evolution.Name = parse[1];
            this.evolution.Species = parse[2].ToInt();

            for (int i = 0; i < parse[3].ToInt(); i++)
            {
                this.evolution.Branches.Add(new Evolutions.EvolutionBranch());
                this.evolution.Branches[i].Name = parse[4 + (i * 6)];
                this.evolution.Branches[i].NewSpecies = parse[5 + (i * 6)].ToInt();
                this.evolution.Branches[i].ReqScript = parse[6 + (i * 6)].ToInt();
                this.evolution.Branches[i].Data1 = parse[7 + (i * 6)].ToInt();
                this.evolution.Branches[i].Data2 = parse[8 + (i * 6)].ToInt();
                this.evolution.Branches[i].Data3 = parse[9 + (i * 6)].ToInt();
            }

            this.pnlEvoList.Visible = false;
            this.pnlEvoEditor.Visible = true;
            this.Size = new Size(this.pnlEvoEditor.Width, this.pnlEvoEditor.Height);

            this.txtName.Text = this.evolution.Name;
            this.nudSpecies.Value = this.evolution.Species;
            this.nudMaxBranchEvos.Value = this.evolution.Branches.Count;

            this.btnEdit.Text = "Edit";
        }

        private void BtnBack_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen > 0)
            {
                this.currentTen--;
            }

            this.RefreshEvoList();
        }

        private void BtnForward_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen < (MaxInfo.MaxEvolutions / 10))
            {
                this.currentTen++;
            }

            this.RefreshEvoList();
        }

        public void RefreshEvoList()
        {
            for (int i = 0; i < 10; i++)
            {
                if ((i + (this.currentTen * 10)) < MaxInfo.MaxEvolutions)
                {
                    ((ListBoxTextItem)this.lbxEvoList.Items[i]).Text = ((i + 1) + (10 * this.currentTen)) + ": " + Evolutions.EvolutionHelper.Evolutions[i + (10 * this.currentTen)].Name;
                }
else
                {
                    ((ListBoxTextItem)this.lbxEvoList.Items[i]).Text = "---";
                }
            }
        }

        private void BtnEdit_Click(object sender, MouseButtonEventArgs e)
        {
            string[] index = ((ListBoxTextItem)this.lbxEvoList.SelectedItems[0]).Text.Split(':');
            if (index[0].IsNumeric())
            {
                this.evoNum = index[0].ToInt() - 1;
                this.btnEdit.Text = "Loading...";

                Messenger.SendEditEvo(index[0].ToInt() - 1);
            }
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            return;
        }

        private void BtnEditorCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.evoNum = -1;
            this.evolution = null;
            this.pnlEvoEditor.Visible = false;
            this.pnlEvoList.Visible = true;
            this.Size = new Size(this.pnlEvoList.Width, this.pnlEvoList.Height);
        }

        private void BtnEditorOK_Click(object sender, MouseButtonEventArgs e)
        {
            this.evolution.Name = this.txtName.Text;
            this.evolution.Species = this.nudSpecies.Value;

            Messenger.SendSaveEvo(this.evoNum, this.evolution, this.nudMaxBranchEvos.Value);

            this.evoNum = -1;
            this.evolution = null;
            this.pnlEvoEditor.Visible = false;
            this.pnlEvoList.Visible = true;
            this.Size = new Size(this.pnlEvoList.Width, this.pnlEvoList.Height);
        }

        private void BtnEvoBranchLoad_Click(object sender, MouseButtonEventArgs e)
        {
            // if evo num is higher than max branches
            if (this.nudEvoNum.Value > this.nudMaxBranchEvos.Value)
            {
                this.lblSaveLoadMessage.Text = "Cannot load branch higher than maximum!";
                return;
            }

            // if stored evo count is lower than max branches
            for (int i = this.evolution.Branches.Count; i < this.nudMaxBranchEvos.Value; i++)
            {
                this.evolution.Branches.Add(new Evolutions.EvolutionBranch());
            }

            this.txtNewName.Text = this.evolution.Branches[this.nudEvoNum.Value - 1].Name;
            this.nudNewSpecies.Value = this.evolution.Branches[this.nudEvoNum.Value - 1].NewSpecies;
            this.nudReqScript.Value = this.evolution.Branches[this.nudEvoNum.Value - 1].ReqScript;
            this.nudData1.Value = this.evolution.Branches[this.nudEvoNum.Value - 1].Data1;
            this.nudData2.Value = this.evolution.Branches[this.nudEvoNum.Value - 1].Data2;
            this.nudData3.Value = this.evolution.Branches[this.nudEvoNum.Value - 1].Data3;

            this.lblSaveLoadMessage.Text = "Branch loaded.";
        }

        private void BtnEvoBranchSave_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.nudEvoNum.Value > this.nudMaxBranchEvos.Value)
            {
                this.lblSaveLoadMessage.Text = "Cannot save branch higher than maximum.";
                return;
            }

            for (int i = this.evolution.Branches.Count; i < this.nudMaxBranchEvos.Value; i++)
            {
                this.evolution.Branches.Add(new Evolutions.EvolutionBranch());
            }

            this.evolution.Branches[this.nudEvoNum.Value - 1].Name = this.txtNewName.Text;
            this.evolution.Branches[this.nudEvoNum.Value - 1].NewSpecies = this.nudNewSpecies.Value;
            this.evolution.Branches[this.nudEvoNum.Value - 1].ReqScript = this.nudReqScript.Value;
            this.evolution.Branches[this.nudEvoNum.Value - 1].Data1 = this.nudData1.Value;
            this.evolution.Branches[this.nudEvoNum.Value - 1].Data2 = this.nudData2.Value;
            this.evolution.Branches[this.nudEvoNum.Value - 1].Data3 = this.nudData3.Value;

            this.lblSaveLoadMessage.Text = "Branch saved.";
        }

        private void NudSpecies_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Surface sprite = GraphicsManager.GetSpriteSheet(this.nudSpecies.Value).GetSheet(FrameType.Idle, Enums.Direction.Down);
            this.picSprite.X = 175 - (sprite.Width / 14);
            this.picSprite.Y = 120 - (sprite.Height / 2);
            this.picSprite.Size = new Size(sprite.Width / 14, sprite.Height);

            this.picSprite.Image = sprite.CreateSurfaceFromClipRectangle(new Rectangle(new Point(3 * this.picSprite.Width, 0), this.picSprite.Size));

            // Graphic.Renderers.RenderLocation loc = new Graphic.Renderers.RenderLocation(new Graphic.Renderers.RendererDestinationData(picSprite.Image, new Point(0, 0), picSprite.Size), new Point(0, 0));

            // loc.DestinationSurface.Blit(sprite, loc.DestinationPoint, new Rectangle(new Point(3 * picSprite.Width, 0), picSprite.Size));
            // picSprite.SelectiveDrawBuffer();
            // picSprite.Image = Tools.CropImage(sprite, new Rectangle(new Point(3 * picSprite.Width / 14, 0), picSprite.Size));
        }

        private void NudNewSpecies_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Surface sprite = GraphicsManager.GetSpriteSheet(this.nudSpecies.Value).GetSheet(FrameType.Idle, Enums.Direction.Down);
            this.picNewSprite.X = 185;
            this.picNewSprite.Y = 120 - (sprite.Height / 2);
            this.picNewSprite.Size = new Size(sprite.Width / 14, sprite.Height);

            this.picNewSprite.Image = sprite.CreateSurfaceFromClipRectangle(new Rectangle(new Point(3 * this.picNewSprite.Width, 0), this.picNewSprite.Size));
        }
    }
}
