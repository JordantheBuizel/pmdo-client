﻿// <copyright file="EditorManager.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class EditorManager
    {
        public static WinAdminPanel AdminPanel
        {
            get
            {
                Window window = WindowManager.FindWindow("winAdminPanel");
                if (window != null)
                {
                    return (WinAdminPanel)window;
                }
else
                {
                    WinAdminPanel adminPanel = new WinAdminPanel();
                    return adminPanel;
                }
            }
        }

        public static WinGuildPanel GuildPanel
        {
            get
            {
                Window window = WindowManager.FindWindow("winGuildPanel");
                if (window != null)
                {
                    return (WinGuildPanel)window;
                }
else
                {
                    WinGuildPanel guildPanel = new WinGuildPanel();
                    return guildPanel;
                }
            }
        }

        public static WinItemPanel ItemPanel
        {
            get
            {
                Window window = WindowManager.FindWindow("winItemPanel");
                if (window != null)
                {
                    return (WinItemPanel)window;
                }
else
                {
                    WinItemPanel itemPanel = new WinItemPanel();

                    return itemPanel;
                }
            }
        }

        public static WinMovePanel MovePanel
        {
            get
            {
                if (WinMovePanel.Instance != null)
                {
                    return WinMovePanel.Instance;
                }
else
                {
                    System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(RunMovePanel));
                    t.SetApartmentState(System.Threading.ApartmentState.STA); // Set the thread to STA
                    t.Start();
                    return WinMovePanel.Instance;
                }
            }
        }

        private static void RunMovePanel()
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.Run(new WinMovePanel());
        }

        public static WinArrowPanel ArrowPanel
        {
            get
            {
                Window window = WindowManager.FindWindow("winArrowPanel");
                if (window != null)
                {
                    return (WinArrowPanel)window;
                }
else
                {
                    WinArrowPanel arrowPanel = new WinArrowPanel();
                    return arrowPanel;
                }
            }
        }

        public static WinEmotionPanel EmotionPanel
        {
            get
            {
                Window window = WindowManager.FindWindow("winEmotionPanel");
                if (window != null)
                {
                    return (WinEmotionPanel)window;
                }
else
                {
                    WinEmotionPanel emotionPanel = new WinEmotionPanel();
                    return emotionPanel;
                }
            }
        }

        public static WinDungeonPanel DungeonPanel
        {
            get
            {
                Window window = WindowManager.FindWindow("winDungeonPanel");
                if (window != null)
                {
                    return (WinDungeonPanel)window;
                }
else
                {
                    WinDungeonPanel dungeonPanel = new WinDungeonPanel();
                    return dungeonPanel;
                }
            }
        }

        public static WinNPCPanel NPCPanel
        {
            get
            {
                Window window = WindowManager.FindWindow("winNPCPanel");
                if (window != null)
                {
                    return (WinNPCPanel)window;
                }
else
                {
                    WinNPCPanel nPCPanel = new WinNPCPanel();
                    return nPCPanel;
                }
            }
        }

        public static WinRDungeonPanel RDungeonPanel
        {
            get
            {
                Window window = WindowManager.FindWindow("winRDungeonPanel");
                if (window != null)
                {
                    return (WinRDungeonPanel)window;
                }
else
                {
                    WinRDungeonPanel rDungeonPanel = new WinRDungeonPanel();
                    return rDungeonPanel;
                }
            }
        }

        public static WinMissionPanel MissionPanel
        {
            get
            {
                Window window = WindowManager.FindWindow("winMissionPanel");
                if (window != null)
                {
                    return (WinMissionPanel)window;
                }
else
                {
                    WinMissionPanel missionPanel = new WinMissionPanel();
                    return missionPanel;
                }
            }
        }

        public static WinEvolutionPanel EvolutionPanel
        {
            get
            {
                Window window = WindowManager.FindWindow("winEvolutionPanel");
                if (window != null)
                {
                    return (WinEvolutionPanel)window;
                }
else
                {
                    WinEvolutionPanel evolutionPanel = new WinEvolutionPanel();
                    return evolutionPanel;
                }
            }
        }

        public static WinShopPanel ShopPanel
        {
            get
            {
                Window window = WindowManager.FindWindow("winShopPanel");
                if (window != null)
                {
                    return (WinShopPanel)window;
                }
else
                {
                    WinShopPanel shopPanel = new WinShopPanel();
                    return shopPanel;
                }
            }
        }

        public static WinStoryPanel StoryPanel
        {
            get
            {
                Window window = WindowManager.FindWindow("winStoryPanel");
                if (window != null)
                {
                    return (WinStoryPanel)window;
                }
else
                {
                    WinStoryPanel storyPanel = new WinStoryPanel();
                    return storyPanel;
                }
            }
        }

        public static ScriptEditor.FrmScriptEditor ScriptEditor
        {
            get;
            set;
        }

        public static void OpenMapEditor()
        {
            Windows.WindowSwitcher.GameWindow.InMapEditor = true;
            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayMapGrid = IO.Options.MapGrid;
            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayAttributes = IO.Options.DisplayAttributes;
            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayDungeonValues = IO.Options.DisplayDungeonValues;
            Windows.WindowSwitcher.GameWindow.EnableMapEditorWidgets(Enums.MapEditorLimitTypes.Full, true);
            Windows.WindowSwitcher.FindWindow("winExpKit").Visible = false;
        }

        public static void CloseMapEditor()
        {
            if (Windows.WindowSwitcher.GameWindow.InMapEditor)
            {
                Windows.WindowSwitcher.GameWindow.DisableMapEditorWidgets();
                Windows.WindowSwitcher.FindWindow("winExpKit").Visible = true;
                Windows.WindowSwitcher.GameWindow.InMapEditor = false;
                Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayMapGrid = false;
                Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayAttributes = false;
                Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayDungeonValues = false;
            }
        }

        public static void OpenHouseEditor()
        {
            Windows.WindowSwitcher.GameWindow.InMapEditor = true;
            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayMapGrid = IO.Options.MapGrid;
            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayAttributes = IO.Options.DisplayAttributes;
            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayDungeonValues = IO.Options.DisplayDungeonValues;
            Windows.WindowSwitcher.GameWindow.EnableMapEditorWidgets(Enums.MapEditorLimitTypes.House, false);
            Windows.WindowSwitcher.FindWindow("winExpKit").Visible = false;
        }

        public static void CloseHouseEditor()
        {
            if (Windows.WindowSwitcher.GameWindow.InMapEditor)
            {
                Windows.WindowSwitcher.GameWindow.DisableMapEditorWidgets();
                Windows.WindowSwitcher.FindWindow("winExpKit").Visible = true;
                Windows.WindowSwitcher.GameWindow.InMapEditor = false;
                Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayMapGrid = false;
                Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayAttributes = false;
                Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayDungeonValues = false;
            }
        }

        public static void OpenItemList()
        {
        }

        // TODO: StoryEditor Opening and Closing (Editor Manager)
        // public static void OpenStoryEditor() {
        // WindowManager.AddWindow(new winStoryPanel());
        // }
        public static void CloseStoryEditor()
        {
            WindowManager.FindWindow("winStoryPanel").Close();
        }
    }
}
