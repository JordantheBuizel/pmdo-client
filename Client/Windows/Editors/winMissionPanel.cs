﻿// <copyright file="winMissionPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Network;
    using PMU.Core;
    using PMU.Sockets;
    using SdlDotNet.Widgets;

    internal class WinMissionPanel : Core.WindowCore
    {
        private int difficultyNum = 0;
        private int currentTen = 0;
        private Logic.Editors.Missions.EditableMissionPool missionPool;

        private readonly Panel pnlMissionList;
        private readonly Panel pnlMissionEditor;
        private readonly ScrollingListBox lbxMissionList;

        private readonly Button btnBack;
        private readonly Button btnForward;
        private readonly Button btnCancel;
        private readonly Button btnEdit;
        private readonly Button btnGeneral;
        private readonly Button btnRewards;
        private readonly Button btnClients;
        private readonly Button btnEnemies;
        private readonly Panel pnlMissionGeneral;
        private readonly Panel pnlMissionRewards;
        private readonly Panel pnlMissionClients;
        private readonly Panel pnlMissionEnemies;
        private readonly Button btnEditorCancel;
        private readonly Button btnEditorOK;
        private readonly ScrollingListBox lbxMissionRewards;
        private readonly Label lblItemNum;
        private readonly NumericUpDown nudItemNum;
        private readonly Label lblItemAmount;
        private readonly NumericUpDown nudItemAmount;
        private readonly Label lblItemTag;
        private readonly TextBox txtItemTag;
        private readonly Button btnAddItem;
        private readonly Button btnLoadItem;
        private readonly Button btnRemoveItem;
        private readonly ScrollingListBox lbxMissionClients;
        private readonly Label lblDexNum;
        private readonly NumericUpDown nudDexNum;
        private readonly Label lblFormNum;
        private readonly NumericUpDown nudFormNum;
        private readonly Button btnAddMissionClient;
        private readonly Button btnLoadMissionClient;
        private readonly Button btnRemoveMissionClient;
        private readonly ScrollingListBox lbxMissionEnemies;
        private readonly Label lblNpcNum;
        private readonly NumericUpDown nudNpcNum;
        private readonly Button btnAddEnemy;
        private readonly Button btnLoadEnemy;
        private readonly Button btnRemoveEnemy;

        public WinMissionPanel()
            : base("winMissionPanel")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new Size(200, 230);
            this.Location = new Point(210, Windows.WindowSwitcher.GameWindow.ActiveTeam.Y + Windows.WindowSwitcher.GameWindow.ActiveTeam.Height + 0);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = true;
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "Mission Panel";
            this.pnlMissionList = new Panel("pnlMissionList");
            this.pnlMissionList.Size = new Size(200, 230);
            this.pnlMissionList.Location = new Point(0, 0);
            this.pnlMissionList.BackColor = Color.White;
            this.pnlMissionList.Visible = true;

            this.pnlMissionEditor = new Panel("pnlMissionEditor");
            this.pnlMissionEditor.Size = new Size(320, 300);
            this.pnlMissionEditor.Location = new Point(0, 0);
            this.pnlMissionEditor.BackColor = Color.White;
            this.pnlMissionEditor.Visible = false;

            this.pnlMissionGeneral = new Panel("pnlMissionGeneral");
            this.pnlMissionGeneral.Size = new Size(320, 270);
            this.pnlMissionGeneral.Location = new Point(0, 30);
            this.pnlMissionGeneral.BackColor = Color.White;
            this.pnlMissionGeneral.Visible = true;

            this.pnlMissionRewards = new Panel("pnlMissionRewards");
            this.pnlMissionRewards.Size = new Size(320, 270);
            this.pnlMissionRewards.Location = new Point(0, 30);
            this.pnlMissionRewards.BackColor = Color.White;
            this.pnlMissionRewards.Visible = false;

            this.pnlMissionClients = new Panel("pnlMissionClients");
            this.pnlMissionClients.Size = new Size(320, 270);
            this.pnlMissionClients.Location = new Point(0, 30);
            this.pnlMissionClients.BackColor = Color.White;
            this.pnlMissionClients.Visible = false;

            this.pnlMissionEnemies = new Panel("pnlMissionEnemies");
            this.pnlMissionEnemies.Size = new Size(320, 270);
            this.pnlMissionEnemies.Location = new Point(0, 30);
            this.pnlMissionEnemies.BackColor = Color.White;
            this.pnlMissionEnemies.Visible = false;
            this.lbxMissionList = new ScrollingListBox("lbxMissionList");
            this.lbxMissionList.Location = new Point(10, 10);
            this.lbxMissionList.Size = new Size(180, 140);
            for (int i = 0; i < 10; i++)
            {
                ListBoxTextItem lbiMission = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), i + ": ");
                this.lbxMissionList.Items.Add(lbiMission);
            }

            this.btnBack = new Button("btnBack");
            this.btnBack.Location = new Point(10, 160);
            this.btnBack.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnBack.Size = new Size(64, 16);
            this.btnBack.Visible = true;
            this.btnBack.Text = "<--";
            this.btnBack.Click += new EventHandler<MouseButtonEventArgs>(this.BtnBack_Click);

            this.btnForward = new Button("btnForward");
            this.btnForward.Location = new Point(126, 160);
            this.btnForward.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnForward.Size = new Size(64, 16);
            this.btnForward.Visible = true;
            this.btnForward.Text = "-->";
            this.btnForward.Click += new EventHandler<MouseButtonEventArgs>(this.BtnForward_Click);

            this.btnEdit = new Button("btnEdit");
            this.btnEdit.Location = new Point(10, 190);
            this.btnEdit.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEdit.Size = new Size(64, 16);
            this.btnEdit.Visible = true;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEdit_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(126, 190);
            this.btnCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnCancel.Size = new Size(64, 16);
            this.btnCancel.Visible = true;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);
            this.btnGeneral = new Button("btnGeneral");
            this.btnGeneral.Location = new Point(5, 5);
            this.btnGeneral.Size = new Size(70, 20);
            this.btnGeneral.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnGeneral.Text = "General";
            this.btnGeneral.Click += new EventHandler<MouseButtonEventArgs>(this.BtnGeneral_Click);

            this.btnClients = new Button("btnClients");
            this.btnClients.Location = new Point(80, 5);
            this.btnClients.Size = new Size(70, 20);
            this.btnClients.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnClients.Text = "Clients";
            this.btnClients.Click += new EventHandler<MouseButtonEventArgs>(this.BtnClients_Click);

            this.btnEnemies = new Button("btnEnemies");
            this.btnEnemies.Location = new Point(155, 5);
            this.btnEnemies.Size = new Size(70, 20);
            this.btnEnemies.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEnemies.Text = "Enemies";
            this.btnEnemies.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEnemies_Click);

            this.btnRewards = new Button("btnRewards");
            this.btnRewards.Location = new Point(230, 5);
            this.btnRewards.Size = new Size(70, 20);
            this.btnRewards.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnRewards.Text = "Rewards";
            this.btnRewards.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRewards_Click);
            this.btnEditorCancel = new Button("btnEditorCancel");
            this.btnEditorCancel.Location = new Point(120, 75);
            this.btnEditorCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorCancel.Size = new Size(64, 16);
            this.btnEditorCancel.Visible = true;
            this.btnEditorCancel.Text = "Cancel";
            this.btnEditorCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorCancel_Click);

            this.btnEditorOK = new Button("btnEditorOK");
            this.btnEditorOK.Location = new Point(20, 75);
            this.btnEditorOK.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorOK.Size = new Size(64, 16);
            this.btnEditorOK.Visible = true;
            this.btnEditorOK.Text = "OK";
            this.btnEditorOK.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorOK_Click);
            this.lblNpcNum = new Label("lblNpcNum");
            this.lblNpcNum.AutoSize = true;
            this.lblNpcNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblNpcNum.Location = new Point(10, 0);
            this.lblNpcNum.Text = "Num:";

            this.nudNpcNum = new NumericUpDown("nudNpcNum");
            this.nudNpcNum.Size = new Size(70, 20);
            this.nudNpcNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudNpcNum.Maximum = int.MaxValue;
            this.nudNpcNum.Minimum = 1;
            this.nudNpcNum.Location = new Point(10, 14);

            this.btnAddEnemy = new Button("btnAddEnemy");
            this.btnAddEnemy.Size = new Size(70, 16);
            this.btnAddEnemy.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnAddEnemy.Location = new Point(5, 72);
            this.btnAddEnemy.Text = "Add";
            this.btnAddEnemy.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddEnemy_Click);

            this.btnRemoveEnemy = new Button("btnRemoveEnemy");
            this.btnRemoveEnemy.Size = new Size(70, 16);
            this.btnRemoveEnemy.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnRemoveEnemy.Location = new Point(80, 72);
            this.btnRemoveEnemy.Text = "Remove";
            this.btnRemoveEnemy.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveEnemy_Click);

            this.btnLoadEnemy = new Button("btnLoadEnemy");
            this.btnLoadEnemy.Size = new Size(70, 16);
            this.btnLoadEnemy.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnLoadEnemy.Location = new Point(155, 72);
            this.btnLoadEnemy.Text = "Load";

            // btnLoadEnemy.Click += new EventHandler<MouseButtonEventArgs>(btnLoadEnemy_Click);
            this.lbxMissionEnemies = new ScrollingListBox("lbxMissionEnemies");
            this.lbxMissionEnemies.Location = new Point(10, 90);
            this.lbxMissionEnemies.Size = new Size(this.pnlMissionClients.Size.Width - 20, this.pnlMissionClients.Size.Height - 120);
            this.lbxMissionEnemies.MultiSelect = false;
            this.lblItemNum = new Label("lblItemNum");
            this.lblItemNum.AutoSize = true;
            this.lblItemNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblItemNum.Location = new Point(10, 0);
            this.lblItemNum.Text = "Num:";

            this.nudItemNum = new NumericUpDown("nudItemNum");
            this.nudItemNum.Size = new Size(170, 20);
            this.nudItemNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudItemNum.Maximum = 2000;
            this.nudItemNum.Minimum = 1;
            this.nudItemNum.Location = new Point(10, 14);
            this.nudItemNum.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudItemNum_ValueChanged);

            this.lblItemAmount = new Label("lblItemAmount");
            this.lblItemAmount.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblItemAmount.Text = "Amount:";
            this.lblItemAmount.AutoSize = true;
            this.lblItemAmount.Location = new Point(200, 0);

            this.nudItemAmount = new NumericUpDown("nudItemAmount");
            this.nudItemAmount.Size = new Size(70, 20);
            this.nudItemAmount.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudItemAmount.Location = new Point(200, 14);
            this.nudItemAmount.Maximum = int.MaxValue;

            this.lblItemTag = new Label("lblItemTag");
            this.lblItemTag.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblItemTag.Text = "Tag:";
            this.lblItemTag.AutoSize = true;
            this.lblItemTag.Location = new Point(10, 34);

            this.txtItemTag = new TextBox("txtItemTag");
            this.txtItemTag.Size = new Size(200, 20);
            this.txtItemTag.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.txtItemTag.Location = new Point(10, 48);

            this.btnAddItem = new Button("btnAddItem");
            this.btnAddItem.Size = new Size(70, 16);
            this.btnAddItem.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnAddItem.Location = new Point(5, 72);
            this.btnAddItem.Text = "Add";
            this.btnAddItem.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddItem_Click);

            this.btnRemoveItem = new Button("btnRemoveItem");
            this.btnRemoveItem.Size = new Size(70, 16);
            this.btnRemoveItem.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnRemoveItem.Location = new Point(80, 72);
            this.btnRemoveItem.Text = "Remove";
            this.btnRemoveItem.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveItem_Click);

            this.btnLoadItem = new Button("btnLoadItem");
            this.btnLoadItem.Size = new Size(70, 16);
            this.btnLoadItem.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnLoadItem.Location = new Point(155, 72);
            this.btnLoadItem.Text = "Load";
            this.btnLoadItem.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadItem_Click);

            this.lbxMissionRewards = new ScrollingListBox("lbxMissionRewards");
            this.lbxMissionRewards.Location = new Point(10, 90);
            this.lbxMissionRewards.Size = new Size(this.pnlMissionClients.Size.Width - 20, this.pnlMissionClients.Size.Height - 120);
            this.lbxMissionRewards.MultiSelect = false;
            this.lblDexNum = new Label("lblDexNum");
            this.lblDexNum.AutoSize = true;
            this.lblDexNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblDexNum.Location = new Point(2, 0);
            this.lblDexNum.Text = "Dex #";

            this.nudDexNum = new NumericUpDown("nudDexNum");
            this.nudDexNum.Size = new Size(70, 20);
            this.nudDexNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudDexNum.Maximum = 649;
            this.nudDexNum.Minimum = 1;
            this.nudDexNum.Location = new Point(10, 14);
            this.nudDexNum.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudDexNum_ValueChanged);

            this.lblFormNum = new Label("lblFormNum");
            this.lblFormNum.AutoSize = true;
            this.lblFormNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblFormNum.Location = new Point(240, 0);
            this.lblFormNum.Text = "Form #";

            this.nudFormNum = new NumericUpDown("nudFormNum");
            this.nudFormNum.Size = new Size(70, 20);
            this.nudFormNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudFormNum.Maximum = int.MaxValue;
            this.nudFormNum.Minimum = 0;
            this.nudFormNum.Location = new Point(240, 14);

            this.btnAddMissionClient = new Button("btnAddMissionClient");
            this.btnAddMissionClient.Size = new Size(70, 16);
            this.btnAddMissionClient.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnAddMissionClient.Location = new Point(5, 72);
            this.btnAddMissionClient.Text = "Add";
            this.btnAddMissionClient.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddMissionClient_Click);

            this.btnRemoveMissionClient = new Button("btnRemoveMissionClient");
            this.btnRemoveMissionClient.Size = new Size(70, 16);
            this.btnRemoveMissionClient.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnRemoveMissionClient.Location = new Point(80, 72);
            this.btnRemoveMissionClient.Text = "Remove";
            this.btnRemoveMissionClient.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveMissionClient_Click);

            this.btnLoadMissionClient = new Button("btnLoadMissionClient");
            this.btnLoadMissionClient.Size = new Size(70, 16);
            this.btnLoadMissionClient.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnLoadMissionClient.Location = new Point(155, 72);
            this.btnLoadMissionClient.Text = "Load";
            this.btnLoadMissionClient.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadMissionClient_Click);

            this.lbxMissionClients = new ScrollingListBox("lbxMissionClients");
            this.lbxMissionClients.Location = new Point(10, 90);
            this.lbxMissionClients.Size = new Size(this.pnlMissionEnemies.Size.Width - 20, this.pnlMissionEnemies.Size.Height - 120);
            this.lbxMissionClients.MultiSelect = false;



            // Dungeon List
            this.pnlMissionList.AddWidget(this.lbxMissionList);
            this.pnlMissionList.AddWidget(this.btnBack);
            this.pnlMissionList.AddWidget(this.btnForward);
            this.pnlMissionList.AddWidget(this.btnEdit);
            this.pnlMissionList.AddWidget(this.btnCancel);

            // General
            this.pnlMissionGeneral.AddWidget(this.btnEditorCancel);
            this.pnlMissionGeneral.AddWidget(this.btnEditorOK);

            // Clients
            this.pnlMissionClients.AddWidget(this.lblDexNum);
            this.pnlMissionClients.AddWidget(this.nudDexNum);
            this.pnlMissionClients.AddWidget(this.lblFormNum);
            this.pnlMissionClients.AddWidget(this.nudFormNum);
            this.pnlMissionClients.AddWidget(this.btnAddMissionClient);
            this.pnlMissionClients.AddWidget(this.btnRemoveMissionClient);
            this.pnlMissionClients.AddWidget(this.btnLoadMissionClient);
            this.pnlMissionClients.AddWidget(this.lbxMissionClients);

            // Enemies
            this.pnlMissionEnemies.AddWidget(this.lblNpcNum);
            this.pnlMissionEnemies.AddWidget(this.nudNpcNum);
            this.pnlMissionEnemies.AddWidget(this.btnAddEnemy);
            this.pnlMissionEnemies.AddWidget(this.btnRemoveEnemy);

            // pnlMissionEnemies.AddWidget(btnLoadEnemy);
            this.pnlMissionEnemies.AddWidget(this.lbxMissionEnemies);

            // Rewards
            this.pnlMissionRewards.AddWidget(this.lblItemNum);
            this.pnlMissionRewards.AddWidget(this.nudItemNum);
            this.pnlMissionRewards.AddWidget(this.lblItemAmount);
            this.pnlMissionRewards.AddWidget(this.nudItemAmount);
            this.pnlMissionRewards.AddWidget(this.lblItemTag);
            this.pnlMissionRewards.AddWidget(this.txtItemTag);
            this.pnlMissionRewards.AddWidget(this.btnAddItem);
            this.pnlMissionRewards.AddWidget(this.btnRemoveItem);
            this.pnlMissionRewards.AddWidget(this.btnLoadItem);
            this.pnlMissionRewards.AddWidget(this.lbxMissionRewards);

            // Editor panel
            this.pnlMissionEditor.AddWidget(this.btnGeneral);
            this.pnlMissionEditor.AddWidget(this.btnClients);
            this.pnlMissionEditor.AddWidget(this.btnEnemies);
            this.pnlMissionEditor.AddWidget(this.btnRewards);
            this.pnlMissionEditor.AddWidget(this.pnlMissionGeneral);
            this.pnlMissionEditor.AddWidget(this.pnlMissionClients);
            this.pnlMissionEditor.AddWidget(this.pnlMissionEnemies);
            this.pnlMissionEditor.AddWidget(this.pnlMissionRewards);

            // This
            this.AddWidget(this.pnlMissionList);
            this.AddWidget(this.pnlMissionEditor);
            this.RefreshMissionList();
        }

        private void BtnBack_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen > 0)
            {
                this.currentTen--;
            }

            this.RefreshMissionList();
        }

        private void BtnForward_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen < (15 / 10))
            {
                this.currentTen++;
            }

            this.RefreshMissionList();
        }

        private void BtnEdit_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxMissionList.SelectedItems.Count == 1)
            {
                string[] index = ((ListBoxTextItem)this.lbxMissionList.SelectedItems[0]).Text.Split(':');
                if (index[0].IsNumeric())
                {
                    this.difficultyNum = index[0].ToInt();
                    this.btnEdit.Text = "Loading...";

                    Messenger.SendEditMission(this.difficultyNum);

                    // LoadMission(new string[10] { "0", "0", "1", "492", "1", "1", "500", "0", "0", "0" });
                }
            }
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            return;
        }

        public void RefreshMissionList()
        {
            for (int i = 0; i < 10; i++)
            {
                if ((i + (this.currentTen * 10)) < 15)
                {
                    ((ListBoxTextItem)this.lbxMissionList.Items[i]).Text = ((i + 1) + (10 * this.currentTen)) + ": " + ((Enums.JobDifficulty)((i + 1) + (10 * this.currentTen))).ToString();
                }
else
                {
                    ((ListBoxTextItem)this.lbxMissionList.Items[i]).Text = "---";
                }
            }
        }

        public void LoadMission(string[] parse)
        {
            this.Size = this.pnlMissionEditor.Size;
            this.pnlMissionList.Visible = false;
            this.pnlMissionEditor.Visible = true;

            this.BtnGeneral_Click(null, null);
            this.lbxMissionRewards.Items.Clear();
            this.lbxMissionEnemies.Items.Clear();
            this.lbxMissionClients.Items.Clear();

            // this.Size = new System.Drawing.Size(pnlDungeonGeneral.Width, pnlDungeonGeneral.Height);
            this.missionPool = new Logic.Editors.Missions.EditableMissionPool();

            int clientCount = parse[2].ToInt();
            int n = 3;
            for (int i = 0; i < clientCount; i++)
            {
                Logic.Editors.Missions.EditableMissionClient missionClient = new Logic.Editors.Missions.EditableMissionClient();
                missionClient.DexNum = parse[n].ToInt();
                missionClient.FormNum = parse[n + 1].ToInt();
                this.missionPool.Clients.Add(missionClient);

                n += 2;

                ListBoxTextItem lbiClient = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (i + 1) + ": #" + missionClient.DexNum + " " + Pokedex.PokemonHelper.Pokemon[missionClient.DexNum - 1].Name + " (Form: " + missionClient.FormNum + ")");
                this.lbxMissionClients.Items.Add(lbiClient);
            }

            int enemyCount = parse[n].ToInt();
            n++;

            for (int i = 0; i < enemyCount; i++)
            {
                this.missionPool.Enemies.Add(parse[n].ToInt());

                ListBoxTextItem lbiEnemy = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (i + 1) + ": NPC #" + this.missionPool.Enemies[i] + ", " + Npc.NpcHelper.Npcs[this.missionPool.Enemies[i]].Name);
                this.lbxMissionEnemies.Items.Add(lbiEnemy);

                n++;
            }

            int rewardCount = parse[n].ToInt();
            n++;

            for (int i = 0; i < rewardCount; i++)
            {
                Logic.Editors.Missions.EditableMissionReward missionReward = new Logic.Editors.Missions.EditableMissionReward();
                missionReward.ItemNum = parse[n].ToInt();
                missionReward.ItemAmount = parse[n + 1].ToInt();
                missionReward.ItemTag = parse[n + 2];
                this.missionPool.Rewards.Add(missionReward);

                n += 3;

                ListBoxTextItem lbiReward = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (i + 1) + ": " + Items.ItemHelper.Items[missionReward.ItemNum].Name + " x" + missionReward.ItemAmount + " (Tag: " + missionReward.ItemTag + ")");
                this.lbxMissionRewards.Items.Add(lbiReward);
            }

            this.btnEdit.Text = "Edit";
        }

        private void BtnGeneral_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnGeneral.Selected)
            {
                this.btnGeneral.Selected = true;
                this.btnClients.Selected = false;
                this.btnEnemies.Selected = false;
                this.btnRewards.Selected = false;
                this.pnlMissionGeneral.Visible = true;
                this.pnlMissionClients.Visible = false;
                this.pnlMissionEnemies.Visible = false;
                this.pnlMissionRewards.Visible = false;
                this.TitleBar.Text = "Mission Editor - General";
            }
        }

        private void BtnClients_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnClients.Selected)
            {
                this.btnGeneral.Selected = false;
                this.btnClients.Selected = true;
                this.btnEnemies.Selected = false;
                this.btnRewards.Selected = false;
                this.pnlMissionGeneral.Visible = false;
                this.pnlMissionClients.Visible = true;
                this.pnlMissionEnemies.Visible = false;
                this.pnlMissionRewards.Visible = false;
                this.TitleBar.Text = "Mission Editor - Clients";
            }
        }

        private void BtnEnemies_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnEnemies.Selected)
            {
                this.btnGeneral.Selected = false;
                this.btnClients.Selected = false;
                this.btnEnemies.Selected = true;
                this.btnRewards.Selected = false;
                this.pnlMissionGeneral.Visible = false;
                this.pnlMissionClients.Visible = false;
                this.pnlMissionEnemies.Visible = true;
                this.pnlMissionRewards.Visible = false;
                this.TitleBar.Text = "Mission Editor - Enemies";
            }
        }

        private void BtnRewards_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnRewards.Selected)
            {
                this.btnGeneral.Selected = false;
                this.btnClients.Selected = false;
                this.btnEnemies.Selected = false;
                this.btnRewards.Selected = true;
                this.pnlMissionGeneral.Visible = false;
                this.pnlMissionClients.Visible = false;
                this.pnlMissionEnemies.Visible = false;
                this.pnlMissionRewards.Visible = true;
                this.TitleBar.Text = "Mission Editor - Rewards";
            }
        }

        private void BtnEditorCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.difficultyNum = -1;
            this.pnlMissionEditor.Visible = false;
            this.pnlMissionList.Visible = true;
            this.Size = new Size(this.pnlMissionList.Width, this.pnlMissionList.Height);
            this.TitleBar.Text = "Mission Panel";
        }

        private void BtnEditorOK_Click(object sender, MouseButtonEventArgs e)
        {
            Messenger.SendSaveMission(this.difficultyNum, this.missionPool);

            this.difficultyNum = -1;
            this.pnlMissionEditor.Visible = false;
            this.pnlMissionList.Visible = true;
            this.Size = new Size(this.pnlMissionList.Width, this.pnlMissionList.Height);
        }

        private void BtnRemoveEnemy_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxMissionEnemies.SelectedIndex > -1)
            {
                this.missionPool.Enemies.RemoveAt(this.lbxMissionEnemies.SelectedIndex);
                this.lbxMissionEnemies.Items.Clear();
                for (int enemies = 0; enemies < this.missionPool.Enemies.Count; enemies++)
                {
                    ListBoxTextItem lbiEnemy = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (enemies + 1) + ": NPC #" + this.missionPool.Enemies[enemies] + ", " + Npc.NpcHelper.Npcs[this.missionPool.Enemies[enemies]].Name);
                    this.lbxMissionEnemies.Items.Add(lbiEnemy);
                }
            }
        }

        private void BtnAddEnemy_Click(object sender, MouseButtonEventArgs e)
        {
            this.missionPool.Enemies.Add(this.nudNpcNum.Value);

            ListBoxTextItem lbiEnemy = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), this.missionPool.Enemies.Count + ": NPC #" + this.missionPool.Enemies[this.missionPool.Enemies.Count - 1] + ", " + Npc.NpcHelper.Npcs[this.missionPool.Enemies[this.missionPool.Enemies.Count - 1]].Name);
            this.lbxMissionEnemies.Items.Add(lbiEnemy);
        }

        // void btnLoadEnemy_Click(object sender, MouseButtonEventArgs e) {
        //    if (lbxMissionEnemies.SelectedIndex > -1) {
        //        nudNpcNum.Value = dungeon.ScriptList.KeyByIndex(lbxMissionEnemies.SelectedIndex);
        //        txtScriptParam.Text = dungeon.ScriptList.ValueByIndex(lbxMissionEnemies.SelectedIndex);
        //    }
        // }

        private void NudItemNum_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.lblItemNum.Text = Items.ItemHelper.Items[this.nudItemNum.Value].Name;
        }

        private void BtnRemoveItem_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxMissionRewards.SelectedIndex > -1)
            {
                this.missionPool.Rewards.RemoveAt(this.lbxMissionRewards.SelectedIndex);
                this.lbxMissionRewards.Items.Clear();
                for (int rewards = 0; rewards < this.missionPool.Rewards.Count; rewards++)
                {
                    ListBoxTextItem lbiReward = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (rewards + 1) + ": " + Items.ItemHelper.Items[this.missionPool.Rewards[rewards].ItemNum].Name + " x" + this.missionPool.Rewards[rewards].ItemAmount + " (Tag: " + this.missionPool.Rewards[rewards].ItemTag + ")");
                    this.lbxMissionRewards.Items.Add(lbiReward);
                }
            }
        }

        private void BtnAddItem_Click(object sender, MouseButtonEventArgs e)
        {
            Logic.Editors.Missions.EditableMissionReward reward = new Logic.Editors.Missions.EditableMissionReward();
            reward.ItemNum = this.nudItemNum.Value;
            reward.ItemAmount = this.nudItemAmount.Value;
            reward.ItemTag = this.txtItemTag.Text;

            this.missionPool.Rewards.Add(reward);

            ListBoxTextItem lbiReward = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), this.missionPool.Rewards.Count + ": " + Items.ItemHelper.Items[reward.ItemNum].Name + " x" + reward.ItemAmount + " (Tag: " + reward.ItemTag + ")");
            this.lbxMissionRewards.Items.Add(lbiReward);
        }

        private void BtnLoadItem_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxMissionRewards.SelectedIndex > -1)
            {
                this.nudItemNum.Value = this.missionPool.Rewards[this.lbxMissionRewards.SelectedIndex].ItemNum;
                this.nudItemAmount.Value = this.missionPool.Rewards[this.lbxMissionRewards.SelectedIndex].ItemAmount;
                this.txtItemTag.Text = this.missionPool.Rewards[this.lbxMissionRewards.SelectedIndex].ItemTag;
            }
        }

        private void NudDexNum_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.lblDexNum.Text = Pokedex.PokemonHelper.Pokemon[this.nudDexNum.Value - 1].Name;
        }

        private void BtnRemoveMissionClient_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxMissionClients.SelectedIndex > -1)
            {
                this.missionPool.Clients.RemoveAt(this.lbxMissionClients.SelectedIndex);
                this.lbxMissionClients.Items.Clear();
                for (int clients = 0; clients < this.missionPool.Clients.Count; clients++)
                {
                    ListBoxTextItem lbiClient = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (clients + 1) + ": #" + this.missionPool.Clients[clients].DexNum + " " + Pokedex.PokemonHelper.Pokemon[this.missionPool.Clients[clients].DexNum - 1].Name + " (Form: " + this.missionPool.Clients[clients].FormNum + ")");
                    this.lbxMissionClients.Items.Add(lbiClient);
                }
            }
        }

        private void BtnAddMissionClient_Click(object sender, MouseButtonEventArgs e)
        {
            Logic.Editors.Missions.EditableMissionClient client = new Logic.Editors.Missions.EditableMissionClient();
            client.DexNum = this.nudDexNum.Value;
            client.FormNum = this.nudFormNum.Value;

            this.missionPool.Clients.Add(client);

            ListBoxTextItem lbiClient = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), this.missionPool.Clients.Count + ": #" + client.DexNum + " " + Pokedex.PokemonHelper.Pokemon[client.DexNum - 1].Name + " (Form: " + client.FormNum + ")");
            this.lbxMissionClients.Items.Add(lbiClient);
        }

        private void BtnLoadMissionClient_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxMissionClients.SelectedIndex > -1)
            {
                this.nudDexNum.Value = this.missionPool.Clients[this.lbxMissionClients.SelectedIndex].DexNum;
                this.nudFormNum.Value = this.missionPool.Clients[this.lbxMissionClients.SelectedIndex].FormNum;
            }
        }
    }
}
