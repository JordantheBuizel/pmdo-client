﻿namespace Client.Logic.Windows.Editors
{
    partial class WinMovePanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlMoveEdit = new System.Windows.Forms.Panel();
            this.cbiHitFreeze = new Client.Logic.Extensions.CheckBoxImage();
            this.cbiPerPlayer = new Client.Logic.Extensions.CheckBoxImage();
            this.btnPreview = new System.Windows.Forms.Button();
            this.pbPreview = new System.Windows.Forms.PictureBox();
            this.pnlTraveling = new System.Windows.Forms.Panel();
            this.cbxTrvType = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.nudTrvCycles = new System.Windows.Forms.NumericUpDown();
            this.nudTrvFrameTime = new System.Windows.Forms.NumericUpDown();
            this.nudTrvAnimation = new System.Windows.Forms.NumericUpDown();
            this.pnlDefender = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.nudDefCycles = new System.Windows.Forms.NumericUpDown();
            this.nudDefFrameTime = new System.Windows.Forms.NumericUpDown();
            this.nudDefAnimation = new System.Windows.Forms.NumericUpDown();
            this.pnlAttacker = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.nudAtkCycles = new System.Windows.Forms.NumericUpDown();
            this.nudAtkFrameTime = new System.Windows.Forms.NumericUpDown();
            this.nudAtkAnimation = new System.Windows.Forms.NumericUpDown();
            this.btnDefender = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnTraveling = new System.Windows.Forms.Button();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnAttacker = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.nudKeyItem = new System.Windows.Forms.NumericUpDown();
            this.nudRange = new System.Windows.Forms.NumericUpDown();
            this.nudEffect3 = new System.Windows.Forms.NumericUpDown();
            this.nudEffect2 = new System.Windows.Forms.NumericUpDown();
            this.nudEffect1 = new System.Windows.Forms.NumericUpDown();
            this.nudHitTime = new System.Windows.Forms.NumericUpDown();
            this.nudAccuracy = new System.Windows.Forms.NumericUpDown();
            this.nudData3 = new System.Windows.Forms.NumericUpDown();
            this.nudData2 = new System.Windows.Forms.NumericUpDown();
            this.nudSound = new System.Windows.Forms.NumericUpDown();
            this.nudData1 = new System.Windows.Forms.NumericUpDown();
            this.nudMaxPP = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMoveName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbxRangeType = new System.Windows.Forms.ComboBox();
            this.cbxTarget = new System.Windows.Forms.ComboBox();
            this.cbxCategory = new System.Windows.Forms.ComboBox();
            this.cbxElement = new System.Windows.Forms.ComboBox();
            this.cbxEffect = new System.Windows.Forms.ComboBox();
            this.pbHeader = new System.Windows.Forms.PictureBox();
            this.pnlMovesView = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.btnViewerCancel = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.lbxMoveList = new System.Windows.Forms.ListBox();
            this.tmrPreview = new System.Windows.Forms.Timer(this.components);
            this.tbFind = new System.Windows.Forms.TextBox();
            this.pnlMoveEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbiHitFreeze)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbiPerPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).BeginInit();
            this.pnlTraveling.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTrvCycles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTrvFrameTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTrvAnimation)).BeginInit();
            this.pnlDefender.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDefCycles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDefFrameTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDefAnimation)).BeginInit();
            this.pnlAttacker.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAtkCycles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAtkFrameTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAtkAnimation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKeyItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEffect3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEffect2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEffect1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHitTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAccuracy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudData3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudData2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudData1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxPP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeader)).BeginInit();
            this.pnlMovesView.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMoveEdit
            // 
            this.pnlMoveEdit.Controls.Add(this.cbiHitFreeze);
            this.pnlMoveEdit.Controls.Add(this.cbiPerPlayer);
            this.pnlMoveEdit.Controls.Add(this.btnPreview);
            this.pnlMoveEdit.Controls.Add(this.pbPreview);
            this.pnlMoveEdit.Controls.Add(this.pnlTraveling);
            this.pnlMoveEdit.Controls.Add(this.pnlDefender);
            this.pnlMoveEdit.Controls.Add(this.pnlAttacker);
            this.pnlMoveEdit.Controls.Add(this.btnDefender);
            this.pnlMoveEdit.Controls.Add(this.btnCancel);
            this.pnlMoveEdit.Controls.Add(this.btnTraveling);
            this.pnlMoveEdit.Controls.Add(this.btnAccept);
            this.pnlMoveEdit.Controls.Add(this.btnAttacker);
            this.pnlMoveEdit.Controls.Add(this.label19);
            this.pnlMoveEdit.Controls.Add(this.label18);
            this.pnlMoveEdit.Controls.Add(this.nudKeyItem);
            this.pnlMoveEdit.Controls.Add(this.nudRange);
            this.pnlMoveEdit.Controls.Add(this.nudEffect3);
            this.pnlMoveEdit.Controls.Add(this.nudEffect2);
            this.pnlMoveEdit.Controls.Add(this.nudEffect1);
            this.pnlMoveEdit.Controls.Add(this.nudHitTime);
            this.pnlMoveEdit.Controls.Add(this.nudAccuracy);
            this.pnlMoveEdit.Controls.Add(this.nudData3);
            this.pnlMoveEdit.Controls.Add(this.nudData2);
            this.pnlMoveEdit.Controls.Add(this.nudSound);
            this.pnlMoveEdit.Controls.Add(this.nudData1);
            this.pnlMoveEdit.Controls.Add(this.nudMaxPP);
            this.pnlMoveEdit.Controls.Add(this.label9);
            this.pnlMoveEdit.Controls.Add(this.label8);
            this.pnlMoveEdit.Controls.Add(this.label7);
            this.pnlMoveEdit.Controls.Add(this.label6);
            this.pnlMoveEdit.Controls.Add(this.label5);
            this.pnlMoveEdit.Controls.Add(this.label4);
            this.pnlMoveEdit.Controls.Add(this.label3);
            this.pnlMoveEdit.Controls.Add(this.label2);
            this.pnlMoveEdit.Controls.Add(this.txtMoveName);
            this.pnlMoveEdit.Controls.Add(this.label17);
            this.pnlMoveEdit.Controls.Add(this.label16);
            this.pnlMoveEdit.Controls.Add(this.label15);
            this.pnlMoveEdit.Controls.Add(this.label14);
            this.pnlMoveEdit.Controls.Add(this.label13);
            this.pnlMoveEdit.Controls.Add(this.label12);
            this.pnlMoveEdit.Controls.Add(this.label11);
            this.pnlMoveEdit.Controls.Add(this.label20);
            this.pnlMoveEdit.Controls.Add(this.label10);
            this.pnlMoveEdit.Controls.Add(this.label1);
            this.pnlMoveEdit.Controls.Add(this.cbxRangeType);
            this.pnlMoveEdit.Controls.Add(this.cbxTarget);
            this.pnlMoveEdit.Controls.Add(this.cbxCategory);
            this.pnlMoveEdit.Controls.Add(this.cbxElement);
            this.pnlMoveEdit.Controls.Add(this.cbxEffect);
            this.pnlMoveEdit.Location = new System.Drawing.Point(13, 23);
            this.pnlMoveEdit.Name = "pnlMoveEdit";
            this.pnlMoveEdit.Size = new System.Drawing.Size(537, 403);
            this.pnlMoveEdit.TabIndex = 0;
            this.pnlMoveEdit.Visible = false;
            this.pnlMoveEdit.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel1_Paint);
            // 
            // cbiHitFreeze
            // 
            this.cbiHitFreeze.Checked = false;
            this.cbiHitFreeze.Location = new System.Drawing.Point(180, 349);
            this.cbiHitFreeze.Name = "cbiHitFreeze";
            this.cbiHitFreeze.Size = new System.Drawing.Size(14, 14);
            this.cbiHitFreeze.TabIndex = 13;
            this.cbiHitFreeze.TabStop = false;
            // 
            // cbiPerPlayer
            // 
            this.cbiPerPlayer.Checked = false;
            this.cbiPerPlayer.Location = new System.Drawing.Point(180, 329);
            this.cbiPerPlayer.Name = "cbiPerPlayer";
            this.cbiPerPlayer.Size = new System.Drawing.Size(14, 14);
            this.cbiPerPlayer.TabIndex = 13;
            this.cbiPerPlayer.TabStop = false;
            // 
            // btnPreview
            // 
            this.btnPreview.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreview.Location = new System.Drawing.Point(180, 366);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(97, 34);
            this.btnPreview.TabIndex = 12;
            this.btnPreview.Text = "Preview";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.BtnPreview_Click);
            // 
            // pbPreview
            // 
            this.pbPreview.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pbPreview.Location = new System.Drawing.Point(305, 260);
            this.pbPreview.Name = "pbPreview";
            this.pbPreview.Size = new System.Drawing.Size(196, 128);
            this.pbPreview.TabIndex = 11;
            this.pbPreview.TabStop = false;
            // 
            // pnlTraveling
            // 
            this.pnlTraveling.Controls.Add(this.cbxTrvType);
            this.pnlTraveling.Controls.Add(this.label27);
            this.pnlTraveling.Controls.Add(this.label28);
            this.pnlTraveling.Controls.Add(this.label30);
            this.pnlTraveling.Controls.Add(this.label29);
            this.pnlTraveling.Controls.Add(this.nudTrvCycles);
            this.pnlTraveling.Controls.Add(this.nudTrvFrameTime);
            this.pnlTraveling.Controls.Add(this.nudTrvAnimation);
            this.pnlTraveling.Location = new System.Drawing.Point(293, 84);
            this.pnlTraveling.Name = "pnlTraveling";
            this.pnlTraveling.Size = new System.Drawing.Size(237, 174);
            this.pnlTraveling.TabIndex = 10;
            this.pnlTraveling.Visible = false;
            // 
            // cbxTrvType
            // 
            this.cbxTrvType.FormattingEnabled = true;
            this.cbxTrvType.Location = new System.Drawing.Point(19, 18);
            this.cbxTrvType.Name = "cbxTrvType";
            this.cbxTrvType.Size = new System.Drawing.Size(121, 21);
            this.cbxTrvType.TabIndex = 2;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(19, 130);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(75, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "Cycles: (1-10)";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(19, 85);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(109, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "Frame time: (1-1000)";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(19, 1);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(85, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "Animation Type:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(20, 41);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(54, 13);
            this.label29.TabIndex = 1;
            this.label29.Text = "Animation";
            // 
            // nudTrvCycles
            // 
            this.nudTrvCycles.Location = new System.Drawing.Point(19, 149);
            this.nudTrvCycles.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudTrvCycles.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudTrvCycles.Name = "nudTrvCycles";
            this.nudTrvCycles.Size = new System.Drawing.Size(120, 21);
            this.nudTrvCycles.TabIndex = 0;
            this.nudTrvCycles.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudTrvFrameTime
            // 
            this.nudTrvFrameTime.Location = new System.Drawing.Point(19, 104);
            this.nudTrvFrameTime.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudTrvFrameTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudTrvFrameTime.Name = "nudTrvFrameTime";
            this.nudTrvFrameTime.Size = new System.Drawing.Size(120, 21);
            this.nudTrvFrameTime.TabIndex = 0;
            this.nudTrvFrameTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudTrvAnimation
            // 
            this.nudTrvAnimation.Location = new System.Drawing.Point(19, 58);
            this.nudTrvAnimation.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudTrvAnimation.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nudTrvAnimation.Name = "nudTrvAnimation";
            this.nudTrvAnimation.Size = new System.Drawing.Size(120, 21);
            this.nudTrvAnimation.TabIndex = 0;
            this.nudTrvAnimation.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // pnlDefender
            // 
            this.pnlDefender.Controls.Add(this.label24);
            this.pnlDefender.Controls.Add(this.label25);
            this.pnlDefender.Controls.Add(this.label26);
            this.pnlDefender.Controls.Add(this.nudDefCycles);
            this.pnlDefender.Controls.Add(this.nudDefFrameTime);
            this.pnlDefender.Controls.Add(this.nudDefAnimation);
            this.pnlDefender.Location = new System.Drawing.Point(293, 87);
            this.pnlDefender.Name = "pnlDefender";
            this.pnlDefender.Size = new System.Drawing.Size(237, 141);
            this.pnlDefender.TabIndex = 10;
            this.pnlDefender.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(12, 101);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(75, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "Cycles: (1-10)";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(12, 56);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(109, 13);
            this.label25.TabIndex = 1;
            this.label25.Text = "Frame time: (1-1000)";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(12, 10);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 13);
            this.label26.TabIndex = 1;
            this.label26.Text = "Animation";
            // 
            // nudDefCycles
            // 
            this.nudDefCycles.Location = new System.Drawing.Point(12, 120);
            this.nudDefCycles.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudDefCycles.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDefCycles.Name = "nudDefCycles";
            this.nudDefCycles.Size = new System.Drawing.Size(120, 21);
            this.nudDefCycles.TabIndex = 0;
            this.nudDefCycles.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudDefFrameTime
            // 
            this.nudDefFrameTime.Location = new System.Drawing.Point(12, 75);
            this.nudDefFrameTime.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudDefFrameTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDefFrameTime.Name = "nudDefFrameTime";
            this.nudDefFrameTime.Size = new System.Drawing.Size(120, 21);
            this.nudDefFrameTime.TabIndex = 0;
            this.nudDefFrameTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudDefAnimation
            // 
            this.nudDefAnimation.Location = new System.Drawing.Point(12, 29);
            this.nudDefAnimation.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudDefAnimation.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nudDefAnimation.Name = "nudDefAnimation";
            this.nudDefAnimation.Size = new System.Drawing.Size(120, 21);
            this.nudDefAnimation.TabIndex = 0;
            this.nudDefAnimation.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // pnlAttacker
            // 
            this.pnlAttacker.Controls.Add(this.label23);
            this.pnlAttacker.Controls.Add(this.label22);
            this.pnlAttacker.Controls.Add(this.label21);
            this.pnlAttacker.Controls.Add(this.nudAtkCycles);
            this.pnlAttacker.Controls.Add(this.nudAtkFrameTime);
            this.pnlAttacker.Controls.Add(this.nudAtkAnimation);
            this.pnlAttacker.Location = new System.Drawing.Point(293, 87);
            this.pnlAttacker.Name = "pnlAttacker";
            this.pnlAttacker.Size = new System.Drawing.Size(237, 141);
            this.pnlAttacker.TabIndex = 10;
            this.pnlAttacker.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(12, 101);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Cycles: (1-10)";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 56);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(109, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "Frame time: (1-1000)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(12, 10);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Animation";
            // 
            // nudAtkCycles
            // 
            this.nudAtkCycles.Location = new System.Drawing.Point(12, 120);
            this.nudAtkCycles.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudAtkCycles.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudAtkCycles.Name = "nudAtkCycles";
            this.nudAtkCycles.Size = new System.Drawing.Size(120, 21);
            this.nudAtkCycles.TabIndex = 0;
            this.nudAtkCycles.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudAtkFrameTime
            // 
            this.nudAtkFrameTime.Location = new System.Drawing.Point(12, 75);
            this.nudAtkFrameTime.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudAtkFrameTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudAtkFrameTime.Name = "nudAtkFrameTime";
            this.nudAtkFrameTime.Size = new System.Drawing.Size(120, 21);
            this.nudAtkFrameTime.TabIndex = 0;
            this.nudAtkFrameTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudAtkAnimation
            // 
            this.nudAtkAnimation.Location = new System.Drawing.Point(12, 29);
            this.nudAtkAnimation.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudAtkAnimation.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nudAtkAnimation.Name = "nudAtkAnimation";
            this.nudAtkAnimation.Size = new System.Drawing.Size(120, 21);
            this.nudAtkAnimation.TabIndex = 0;
            this.nudAtkAnimation.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // btnDefender
            // 
            this.btnDefender.Location = new System.Drawing.Point(455, 59);
            this.btnDefender.Name = "btnDefender";
            this.btnDefender.Size = new System.Drawing.Size(75, 22);
            this.btnDefender.TabIndex = 9;
            this.btnDefender.Text = "Defender";
            this.btnDefender.UseVisualStyleBackColor = true;
            this.btnDefender.Click += new System.EventHandler(this.BtnDefender_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(88, 377);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 22);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnTraveling
            // 
            this.btnTraveling.Location = new System.Drawing.Point(374, 59);
            this.btnTraveling.Name = "btnTraveling";
            this.btnTraveling.Size = new System.Drawing.Size(75, 22);
            this.btnTraveling.TabIndex = 9;
            this.btnTraveling.Text = "Traveling";
            this.btnTraveling.UseVisualStyleBackColor = true;
            this.btnTraveling.Click += new System.EventHandler(this.BtnTraveling_Click);
            // 
            // btnAccept
            // 
            this.btnAccept.Font = new System.Drawing.Font("Tahoma", 8F);
            this.btnAccept.Location = new System.Drawing.Point(7, 377);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(75, 21);
            this.btnAccept.TabIndex = 9;
            this.btnAccept.Text = "OK";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.BtnAccept_Click);
            // 
            // btnAttacker
            // 
            this.btnAttacker.Font = new System.Drawing.Font("Tahoma", 8F);
            this.btnAttacker.Location = new System.Drawing.Point(293, 60);
            this.btnAttacker.Name = "btnAttacker";
            this.btnAttacker.Size = new System.Drawing.Size(75, 21);
            this.btnAttacker.TabIndex = 9;
            this.btnAttacker.Text = "Attacker";
            this.btnAttacker.UseVisualStyleBackColor = true;
            this.btnAttacker.Click += new System.EventHandler(this.BtnAttacker_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(199, 350);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(96, 13);
            this.label19.TabIndex = 8;
            this.label19.Text = "Freeze on Contact";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(199, 329);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = "Per Player";
            // 
            // nudKeyItem
            // 
            this.nudKeyItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudKeyItem.Location = new System.Drawing.Point(7, 340);
            this.nudKeyItem.Maximum = new decimal(new int[] {
            2001,
            0,
            0,
            0});
            this.nudKeyItem.Name = "nudKeyItem";
            this.nudKeyItem.Size = new System.Drawing.Size(163, 21);
            this.nudKeyItem.TabIndex = 6;
            // 
            // nudRange
            // 
            this.nudRange.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudRange.Location = new System.Drawing.Point(7, 301);
            this.nudRange.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudRange.Name = "nudRange";
            this.nudRange.Size = new System.Drawing.Size(163, 21);
            this.nudRange.TabIndex = 5;
            // 
            // nudEffect3
            // 
            this.nudEffect3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudEffect3.Location = new System.Drawing.Point(176, 301);
            this.nudEffect3.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudEffect3.Name = "nudEffect3";
            this.nudEffect3.Size = new System.Drawing.Size(101, 21);
            this.nudEffect3.TabIndex = 4;
            this.nudEffect3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudEffect3.ValueChanged += new System.EventHandler(this.NumericUpDown8_ValueChanged);
            // 
            // nudEffect2
            // 
            this.nudEffect2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudEffect2.Location = new System.Drawing.Point(176, 261);
            this.nudEffect2.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudEffect2.Name = "nudEffect2";
            this.nudEffect2.Size = new System.Drawing.Size(101, 21);
            this.nudEffect2.TabIndex = 4;
            this.nudEffect2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudEffect2.ValueChanged += new System.EventHandler(this.NumericUpDown7_ValueChanged);
            // 
            // nudEffect1
            // 
            this.nudEffect1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudEffect1.Location = new System.Drawing.Point(176, 221);
            this.nudEffect1.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudEffect1.Name = "nudEffect1";
            this.nudEffect1.Size = new System.Drawing.Size(101, 21);
            this.nudEffect1.TabIndex = 4;
            this.nudEffect1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudEffect1.ValueChanged += new System.EventHandler(this.NumericUpDown6_ValueChanged);
            // 
            // nudHitTime
            // 
            this.nudHitTime.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudHitTime.Location = new System.Drawing.Point(176, 180);
            this.nudHitTime.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudHitTime.Name = "nudHitTime";
            this.nudHitTime.Size = new System.Drawing.Size(101, 21);
            this.nudHitTime.TabIndex = 4;
            this.nudHitTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudHitTime.ValueChanged += new System.EventHandler(this.NumericUpDown5_ValueChanged);
            // 
            // nudAccuracy
            // 
            this.nudAccuracy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudAccuracy.Location = new System.Drawing.Point(176, 141);
            this.nudAccuracy.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudAccuracy.Name = "nudAccuracy";
            this.nudAccuracy.Size = new System.Drawing.Size(101, 21);
            this.nudAccuracy.TabIndex = 4;
            this.nudAccuracy.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudData3
            // 
            this.nudData3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudData3.Location = new System.Drawing.Point(176, 100);
            this.nudData3.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudData3.Name = "nudData3";
            this.nudData3.Size = new System.Drawing.Size(101, 21);
            this.nudData3.TabIndex = 4;
            this.nudData3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudData2
            // 
            this.nudData2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudData2.Location = new System.Drawing.Point(176, 60);
            this.nudData2.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudData2.Name = "nudData2";
            this.nudData2.Size = new System.Drawing.Size(101, 21);
            this.nudData2.TabIndex = 4;
            this.nudData2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudSound
            // 
            this.nudSound.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudSound.Location = new System.Drawing.Point(305, 21);
            this.nudSound.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudSound.Name = "nudSound";
            this.nudSound.Size = new System.Drawing.Size(101, 21);
            this.nudSound.TabIndex = 4;
            this.nudSound.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudSound.ValueChanged += new System.EventHandler(this.NudSound_ValueChanged);
            // 
            // nudData1
            // 
            this.nudData1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudData1.Location = new System.Drawing.Point(176, 21);
            this.nudData1.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudData1.Name = "nudData1";
            this.nudData1.Size = new System.Drawing.Size(101, 21);
            this.nudData1.TabIndex = 4;
            this.nudData1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudMaxPP
            // 
            this.nudMaxPP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudMaxPP.Location = new System.Drawing.Point(7, 60);
            this.nudMaxPP.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudMaxPP.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMaxPP.Name = "nudMaxPP";
            this.nudMaxPP.Size = new System.Drawing.Size(163, 21);
            this.nudMaxPP.TabIndex = 4;
            this.nudMaxPP.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(4, 324);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Key Item:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(4, 284);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Range: >=0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(4, 244);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Range Type:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(4, 204);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Targets:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 164);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Category:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Element:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Effect Type:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Max PP: >=1";
            // 
            // txtMoveName
            // 
            this.txtMoveName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoveName.Location = new System.Drawing.Point(7, 20);
            this.txtMoveName.Name = "txtMoveName";
            this.txtMoveName.Size = new System.Drawing.Size(163, 21);
            this.txtMoveName.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(176, 285);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Effect 3:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(176, 244);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Effect 2:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(176, 204);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Effect 1:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(176, 164);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Hit Time: >=1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(176, 124);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Accuracy: >=-1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(176, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Data 3:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(176, 44);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Data 2:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(302, 4);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Sound:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(176, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Data 1:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Move Name:";
            // 
            // cbxRangeType
            // 
            this.cbxRangeType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxRangeType.FormattingEnabled = true;
            this.cbxRangeType.Location = new System.Drawing.Point(7, 260);
            this.cbxRangeType.Name = "cbxRangeType";
            this.cbxRangeType.Size = new System.Drawing.Size(163, 21);
            this.cbxRangeType.TabIndex = 0;
            // 
            // cbxTarget
            // 
            this.cbxTarget.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxTarget.FormattingEnabled = true;
            this.cbxTarget.Location = new System.Drawing.Point(7, 220);
            this.cbxTarget.Name = "cbxTarget";
            this.cbxTarget.Size = new System.Drawing.Size(163, 21);
            this.cbxTarget.TabIndex = 0;
            // 
            // cbxCategory
            // 
            this.cbxCategory.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxCategory.FormattingEnabled = true;
            this.cbxCategory.Location = new System.Drawing.Point(7, 180);
            this.cbxCategory.Name = "cbxCategory";
            this.cbxCategory.Size = new System.Drawing.Size(163, 21);
            this.cbxCategory.TabIndex = 0;
            // 
            // cbxElement
            // 
            this.cbxElement.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxElement.FormattingEnabled = true;
            this.cbxElement.Location = new System.Drawing.Point(7, 140);
            this.cbxElement.Name = "cbxElement";
            this.cbxElement.Size = new System.Drawing.Size(163, 21);
            this.cbxElement.TabIndex = 0;
            // 
            // cbxEffect
            // 
            this.cbxEffect.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEffect.FormattingEnabled = true;
            this.cbxEffect.Location = new System.Drawing.Point(7, 100);
            this.cbxEffect.Name = "cbxEffect";
            this.cbxEffect.Size = new System.Drawing.Size(163, 21);
            this.cbxEffect.TabIndex = 0;
            // 
            // pbHeader
            // 
            this.pbHeader.BackColor = System.Drawing.Color.Transparent;
            this.pbHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbHeader.Location = new System.Drawing.Point(0, 0);
            this.pbHeader.Name = "pbHeader";
            this.pbHeader.Size = new System.Drawing.Size(564, 26);
            this.pbHeader.TabIndex = 1;
            this.pbHeader.TabStop = false;
            // 
            // pnlMovesView
            // 
            this.pnlMovesView.Controls.Add(this.tbFind);
            this.pnlMovesView.Controls.Add(this.label31);
            this.pnlMovesView.Controls.Add(this.btnViewerCancel);
            this.pnlMovesView.Controls.Add(this.btnEdit);
            this.pnlMovesView.Controls.Add(this.lbxMoveList);
            this.pnlMovesView.Location = new System.Drawing.Point(6, 23);
            this.pnlMovesView.Name = "pnlMovesView";
            this.pnlMovesView.Size = new System.Drawing.Size(200, 270);
            this.pnlMovesView.TabIndex = 2;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(8, 246);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(60, 13);
            this.label31.TabIndex = 2;
            this.label31.Text = "Find Move:";
            // 
            // btnViewerCancel
            // 
            this.btnViewerCancel.Location = new System.Drawing.Point(118, 214);
            this.btnViewerCancel.Name = "btnViewerCancel";
            this.btnViewerCancel.Size = new System.Drawing.Size(75, 23);
            this.btnViewerCancel.TabIndex = 1;
            this.btnViewerCancel.Text = "Cancel";
            this.btnViewerCancel.UseVisualStyleBackColor = true;
            this.btnViewerCancel.Click += new System.EventHandler(this.BtnViewerCancel_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(7, 214);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // lbxMoveList
            // 
            this.lbxMoveList.FormattingEnabled = true;
            this.lbxMoveList.Location = new System.Drawing.Point(7, 4);
            this.lbxMoveList.Name = "lbxMoveList";
            this.lbxMoveList.Size = new System.Drawing.Size(186, 199);
            this.lbxMoveList.TabIndex = 0;
            // 
            // tbFind
            // 
            this.tbFind.Location = new System.Drawing.Point(74, 243);
            this.tbFind.Name = "tbFind";
            this.tbFind.Size = new System.Drawing.Size(118, 21);
            this.tbFind.TabIndex = 3;
            // 
            // winMovePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Logic.Properties.Resources.menu_horizontal_border;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(564, 440);
            this.Controls.Add(this.pnlMovesView);
            this.Controls.Add(this.pbHeader);
            this.Controls.Add(this.pnlMoveEdit);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "winMovePanel";
            this.Text = "winMovePanel";
            this.pnlMoveEdit.ResumeLayout(false);
            this.pnlMoveEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbiHitFreeze)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbiPerPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).EndInit();
            this.pnlTraveling.ResumeLayout(false);
            this.pnlTraveling.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTrvCycles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTrvFrameTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTrvAnimation)).EndInit();
            this.pnlDefender.ResumeLayout(false);
            this.pnlDefender.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDefCycles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDefFrameTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDefAnimation)).EndInit();
            this.pnlAttacker.ResumeLayout(false);
            this.pnlAttacker.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAtkCycles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAtkFrameTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAtkAnimation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKeyItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEffect3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEffect2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEffect1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHitTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAccuracy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudData3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudData2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudData1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxPP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeader)).EndInit();
            this.pnlMovesView.ResumeLayout(false);
            this.pnlMovesView.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMoveEdit;
        private System.Windows.Forms.NumericUpDown nudMaxPP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMoveName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxEffect;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxTarget;
        private System.Windows.Forms.ComboBox cbxCategory;
        private System.Windows.Forms.ComboBox cbxElement;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbxRangeType;
        private System.Windows.Forms.PictureBox pbHeader;
        private System.Windows.Forms.NumericUpDown nudKeyItem;
        private System.Windows.Forms.NumericUpDown nudRange;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nudEffect3;
        private System.Windows.Forms.NumericUpDown nudEffect2;
        private System.Windows.Forms.NumericUpDown nudEffect1;
        private System.Windows.Forms.NumericUpDown nudHitTime;
        private System.Windows.Forms.NumericUpDown nudAccuracy;
        private System.Windows.Forms.NumericUpDown nudData3;
        private System.Windows.Forms.NumericUpDown nudData2;
        private System.Windows.Forms.NumericUpDown nudData1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown nudSound;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel pnlAttacker;
        private System.Windows.Forms.Button btnDefender;
        private System.Windows.Forms.Button btnTraveling;
        private System.Windows.Forms.Button btnAttacker;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.PictureBox pbPreview;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown nudAtkCycles;
        private System.Windows.Forms.NumericUpDown nudAtkFrameTime;
        private System.Windows.Forms.NumericUpDown nudAtkAnimation;
        private System.Windows.Forms.Panel pnlDefender;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.NumericUpDown nudDefCycles;
        private System.Windows.Forms.NumericUpDown nudDefFrameTime;
        private System.Windows.Forms.NumericUpDown nudDefAnimation;
        private System.Windows.Forms.Panel pnlTraveling;
        private System.Windows.Forms.ComboBox cbxTrvType;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.NumericUpDown nudTrvCycles;
        private System.Windows.Forms.NumericUpDown nudTrvFrameTime;
        private System.Windows.Forms.NumericUpDown nudTrvAnimation;
        private System.Windows.Forms.Panel pnlMovesView;
        private System.Windows.Forms.ListBox lbxMoveList;
        private System.Windows.Forms.Button btnViewerCancel;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Timer tmrPreview;
        private Extensions.CheckBoxImage cbiHitFreeze;
        private Extensions.CheckBoxImage cbiPerPlayer;
        private System.Windows.Forms.TextBox tbFind;
    }
}