﻿// <copyright file="winItemPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Network;
    using PMU.Core;
    using PMU.Sockets;
    using SdlDotNet.Widgets;

    internal class WinItemPanel : Core.WindowCore
    {
        private int itemNum = -1;
        private int currentTen = 0;

        private readonly Panel pnlItemList;
        private readonly Panel pnlItemEditor;

        private readonly ScrollingListBox lbxItemList;
        private readonly ListBoxTextItem lbiItem;

        // Button btnAddNew; (Can implement later...)
        private readonly Button btnBack;
        private readonly Button btnForward;
        private readonly Button btnCancel;
        private readonly Button btnEdit;
        private readonly Label lblSearch;
        private readonly NumericUpDown findTen;

        private readonly Button btnEditorCancel;
        private readonly Button btnEditorOK;

        private readonly Label lblName;
        private readonly TextBox txtName;
        private readonly Label lblDescription;
        private readonly TextBox txtDescription;
        private readonly Label lblSprite;
        private readonly Widges.ItemsetViewer pic;

        private readonly Label lblType;
        private readonly RadioButton optTypeNone;
        private readonly RadioButton optTypeHeld;
        private readonly RadioButton optTypeHeldByParty;
        private readonly RadioButton optTypeHeldInBag;
        private readonly RadioButton optTypePotionAddHP;
        private readonly RadioButton optTypePotionAddPP;
        private readonly RadioButton optTypePotionAddBelly;
        private readonly RadioButton optTypePotionSubHP;
        private readonly RadioButton optTypePotionSubPP;
        private readonly RadioButton optTypePotionSubBelly;
        private readonly RadioButton optTypeKey;
        private readonly RadioButton optTypeCurrency;
        private readonly RadioButton optTypeTM;
        private readonly RadioButton optTypeScripted;

        private readonly Label lblData1;
        private readonly NumericUpDown nudData1;
        private readonly Label lblData2;
        private readonly NumericUpDown nudData2;
        private readonly Label lblData3;
        private readonly NumericUpDown nudData3;

        private readonly Label lblRarity;
        private readonly NumericUpDown nudRarity;

        private readonly Label lblAtkReq;
        private readonly NumericUpDown nudAtkReq;
        private readonly Label lblDefReq;
        private readonly NumericUpDown nudDefReq;
        private readonly Label lblSpAtkReq;
        private readonly NumericUpDown nudSpAtkReq;
        private readonly Label lblSpDefReq;
        private readonly NumericUpDown nudSpDefReq;
        private readonly Label lblSpeedReq;
        private readonly NumericUpDown nudSpeedReq;
        private readonly Label lblScriptedReq;
        private readonly NumericUpDown nudScriptedReq;

        // No need for durability
        private readonly Label lblAddHP;
        private readonly NumericUpDown nudAddHP;
        private readonly Label lblAddPP;
        private readonly NumericUpDown nudAddPP;
        private readonly Label lblAddAtk;
        private readonly NumericUpDown nudAddAtk;
        private readonly Label lblAddDef;
        private readonly NumericUpDown nudAddDef;
        private readonly Label lblAddSpAtk;
        private readonly NumericUpDown nudAddSpAtk;
        private readonly Label lblAddSpDef;
        private readonly NumericUpDown nudAddSpDef;
        private readonly Label lblAddSpeed;
        private readonly NumericUpDown nudAddSpeed;
        private readonly Label lblAddEXP;
        private readonly NumericUpDown nudAddEXP;
        private readonly Label lblAttackSpeed;
        private readonly NumericUpDown nudAttackSpeed;
        private readonly Label lblSellPrice;
        private readonly NumericUpDown nudSellPrice;
        private readonly Label lblStackCap;
        private readonly NumericUpDown nudStackCap;

        // Label lblBound;
        private readonly CheckBox chkBound;

        // Label lblLoseable;
        private readonly CheckBox chkLoseable;
        private readonly Label lblRecruitBonus;
        private readonly NumericUpDown nudRecruitBonus;

        public WinItemPanel()
            : base("winItemPanel")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new Size(200, 270);
            this.Location = new Point(210, Windows.WindowSwitcher.GameWindow.ActiveTeam.Y + Windows.WindowSwitcher.GameWindow.ActiveTeam.Height + 0);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = true;
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "Item Panel";

            this.pnlItemList = new Panel("pnlItemList");
            this.pnlItemList.Size = new Size(200, 270);
            this.pnlItemList.Location = new Point(0, 0);
            this.pnlItemList.BackColor = Color.White;
            this.pnlItemList.Visible = true;

            this.pnlItemEditor = new Panel("pnlItemEditor");
            this.pnlItemEditor.Size = new Size(580, 380);
            this.pnlItemEditor.Location = new Point(0, 0);
            this.pnlItemEditor.BackColor = Color.White;
            this.pnlItemEditor.Visible = false;

            this.lbxItemList = new ScrollingListBox("lbxItemList");
            this.lbxItemList.Location = new Point(10, 10);
            this.lbxItemList.Size = new Size(180, 140);
            for (int i = 0; i < 10; i++)
            {
                this.lbiItem = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (i + 1) + ": " + Items.ItemHelper.Items[(i + 1) + (10 * this.currentTen)].Name);
                this.lbxItemList.Items.Add(this.lbiItem);
            }

            this.lbxItemList.SelectItem(0);

            this.btnBack = new Button("btnBack");
            this.btnBack.Location = new Point(10, 160);
            this.btnBack.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnBack.Size = new Size(64, 16);
            this.btnBack.Visible = true;
            this.btnBack.Text = "<--";
            this.btnBack.Click += new EventHandler<MouseButtonEventArgs>(this.BtnBack_Click);

            this.btnForward = new Button("btnForward");
            this.btnForward.Location = new Point(126, 160);
            this.btnForward.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnForward.Size = new Size(64, 16);
            this.btnForward.Visible = true;
            this.btnForward.Text = "-->";
            this.btnForward.Click += new EventHandler<MouseButtonEventArgs>(this.BtnForward_Click);

            this.btnEdit = new Button("btnEdit");
            this.btnEdit.Location = new Point(10, 190);
            this.btnEdit.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEdit.Size = new Size(64, 16);
            this.btnEdit.Visible = true;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEdit_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(126, 190);
            this.btnCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnCancel.Size = new Size(64, 16);
            this.btnCancel.Visible = true;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.lblSearch = new Label("lblSearch");
            this.lblSearch.Location = new Point(11, 220);
            this.lblSearch.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblSearch.Text = "Find Item Number:";
            this.lblSearch.AutoSize = true;

            this.findTen = new NumericUpDown("findTen");
            this.findTen.Location = new Point(126, 220);
            this.findTen.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.findTen.Size = new Size(64, 16);
            this.findTen.Visible = true;
            this.findTen.Minimum = 1;
            this.findTen.Maximum = MaxInfo.MaxItems;
            this.findTen.KeyUp += new EventHandler<SdlDotNet.Input.KeyboardEventArgs>(this.FindTen_KeyUp);

            // btnAddNew = new Button("btnAddNew");
            // btnAddNew.Location = new Point();
            // btnAddNew.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            // btnAddNew.Size = new System.Drawing.Size(64, 16);
            // btnAddNew.Visible = true;
            // btnAddNew.Text = "Add New";
            // btnAddNew.Click += new EventHandler<MouseButtonEventArgs>(btnAddNew_Click);
            this.btnEditorCancel = new Button("btnEditorCancel");
            this.btnEditorCancel.Location = new Point(100, 334);
            this.btnEditorCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorCancel.Size = new Size(64, 16);
            this.btnEditorCancel.Visible = true;
            this.btnEditorCancel.Text = "Cancel";
            this.btnEditorCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorCancel_Click);

            this.btnEditorOK = new Button("btnEditorOK");
            this.btnEditorOK.Location = new Point(10, 334);
            this.btnEditorOK.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorOK.Size = new Size(64, 16);
            this.btnEditorOK.Visible = true;
            this.btnEditorOK.Text = "OK";
            this.btnEditorOK.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorOK_Click);

            this.lblName = new Label("lblName");
            this.lblName.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblName.Text = "Item Name:";
            this.lblName.AutoSize = true;
            this.lblName.Location = new Point(10, 4);

            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(200, 16);
            this.txtName.Location = new Point(10, 16);

            this.lblSprite = new Label("lblSprite");
            this.lblSprite.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblSprite.Text = "Item Sprite:";
            this.lblSprite.AutoSize = true;
            this.lblSprite.Location = new Point(10, 36);

            this.pic = new Widges.ItemsetViewer("pic");
            this.pic.Location = new Point(10, 48);
            this.pic.Size = new Size(204, 144);
            this.pic.ActiveItemSurface = Graphic.GraphicsManager.Items;

            this.lblSellPrice = new Label("lblSellPrice");
            this.lblSellPrice.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblSellPrice.Text = "Sell Price:";
            this.lblSellPrice.AutoSize = true;
            this.lblSellPrice.Location = new Point(10, 200);

            this.nudSellPrice = new NumericUpDown("nudSellPrice");
            this.nudSellPrice.Size = new Size(200, 16);
            this.nudSellPrice.Location = new Point(10, 212);
            this.nudSellPrice.Minimum = 0;
            this.nudSellPrice.Maximum = int.MaxValue;

            this.nudStackCap = new NumericUpDown("nudStackCap");
            this.nudStackCap.Location = new Point(10, 232);
            this.nudStackCap.Size = new Size(95, 17);
            this.nudStackCap.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudStackCap.Minimum = 0;
            this.nudStackCap.Maximum = int.MaxValue;

            this.lblStackCap = new Label("lblStackCap");
            this.lblStackCap.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblStackCap.Text = "Stack Cap";
            this.lblStackCap.AutoSize = true;
            this.lblStackCap.Location = new Point(110, 232);

            this.chkBound = new CheckBox("chkBound");
            this.chkBound.Location = new Point(10, 252);
            this.chkBound.Size = new Size(95, 17);
            this.chkBound.BackColor = Color.Transparent;
            this.chkBound.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.chkBound.Text = "Bound";

            this.chkLoseable = new CheckBox("chkLoseable");
            this.chkLoseable.Location = new Point(10, 272);
            this.chkLoseable.Size = new Size(95, 17);
            this.chkLoseable.BackColor = Color.Transparent;
            this.chkLoseable.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.chkLoseable.Text = "Loseable";

            this.lblDescription = new Label("lblDescription");
            this.lblDescription.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblDescription.Text = "Description:";
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new Point(10, 292);

            this.txtDescription = new TextBox("txtDescription");
            this.txtDescription.Size = new Size(300, 16);
            this.txtDescription.Location = new Point(10, 304);

            this.lblType = new Label("lblType");
            this.lblType.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblType.Text = "Item Type:";
            this.lblType.AutoSize = true;
            this.lblType.Location = new Point(220, 4);

            this.optTypeNone = new RadioButton("optTypeNone");
            this.optTypeNone.BackColor = Color.Transparent;
            this.optTypeNone.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypeNone.Location = new Point(220, 24);
            this.optTypeNone.Size = new Size(95, 17);
            this.optTypeNone.Text = "None";
            this.optTypeNone.Checked = true;

            this.optTypeHeld = new RadioButton("optTypeHeld");
            this.optTypeHeld.BackColor = Color.Transparent;
            this.optTypeHeld.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypeHeld.Location = new Point(220, 44);
            this.optTypeHeld.Size = new Size(95, 17);
            this.optTypeHeld.Text = "Held Item";

            this.optTypeHeldByParty = new RadioButton("optTypeHeldByParty");
            this.optTypeHeldByParty.BackColor = Color.Transparent;
            this.optTypeHeldByParty.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypeHeldByParty.Location = new Point(220, 64);
            this.optTypeHeldByParty.Size = new Size(95, 17);
            this.optTypeHeldByParty.Text = "Party Item";

            this.optTypeHeldInBag = new RadioButton("optTypeHeldInBag");
            this.optTypeHeldInBag.BackColor = Color.Transparent;
            this.optTypeHeldInBag.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypeHeldInBag.Location = new Point(220, 84);
            this.optTypeHeldInBag.Size = new Size(95, 17);
            this.optTypeHeldInBag.Text = "Bag Item";

            this.optTypePotionAddHP = new RadioButton("optTypePotionAddHP");
            this.optTypePotionAddHP.BackColor = Color.Transparent;
            this.optTypePotionAddHP.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypePotionAddHP.Location = new Point(220, 104);
            this.optTypePotionAddHP.Size = new Size(95, 17);
            this.optTypePotionAddHP.Text = "HP Heal";

            this.optTypePotionAddPP = new RadioButton("optTypePotionAddPP");
            this.optTypePotionAddPP.BackColor = Color.Transparent;
            this.optTypePotionAddPP.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypePotionAddPP.Location = new Point(220, 124);
            this.optTypePotionAddPP.Size = new Size(95, 17);
            this.optTypePotionAddPP.Text = "PP Heal";

            this.optTypePotionAddBelly = new RadioButton("optTypePotionAddBelly");
            this.optTypePotionAddBelly.BackColor = Color.Transparent;
            this.optTypePotionAddBelly.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypePotionAddBelly.Location = new Point(220, 144);
            this.optTypePotionAddBelly.Size = new Size(95, 17);
            this.optTypePotionAddBelly.Text = "Belly Heal";

            this.optTypePotionSubHP = new RadioButton("optTypePotionSubHP");
            this.optTypePotionSubHP.BackColor = Color.Transparent;
            this.optTypePotionSubHP.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypePotionSubHP.Location = new Point(220, 164);
            this.optTypePotionSubHP.Size = new Size(95, 17);
            this.optTypePotionSubHP.Text = "HP Loss";

            this.optTypePotionSubPP = new RadioButton("optTypePotionSubPP");
            this.optTypePotionSubPP.BackColor = Color.Transparent;
            this.optTypePotionSubPP.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypePotionSubPP.Location = new Point(220, 184);
            this.optTypePotionSubPP.Size = new Size(95, 17);
            this.optTypePotionSubPP.Text = "PP Loss";

            this.optTypePotionSubBelly = new RadioButton("optTypePotionSubBelly");
            this.optTypePotionSubBelly.BackColor = Color.Transparent;
            this.optTypePotionSubBelly.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypePotionSubBelly.Location = new Point(220, 204);
            this.optTypePotionSubBelly.Size = new Size(95, 17);
            this.optTypePotionSubBelly.Text = "Belly Loss";

            this.optTypeKey = new RadioButton("optTypeKey");
            this.optTypeKey.BackColor = Color.Transparent;
            this.optTypeKey.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypeKey.Location = new Point(220, 224);
            this.optTypeKey.Size = new Size(95, 17);
            this.optTypeKey.Text = "Key";

            this.optTypeCurrency = new RadioButton("optTypeCurrency");
            this.optTypeCurrency.BackColor = Color.Transparent;
            this.optTypeCurrency.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypeCurrency.Location = new Point(220, 244);
            this.optTypeCurrency.Size = new Size(95, 17);
            this.optTypeCurrency.Text = "Currency";

            this.optTypeTM = new RadioButton("optTypeTM");
            this.optTypeTM.BackColor = Color.Transparent;
            this.optTypeTM.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypeTM.Location = new Point(220, 264);
            this.optTypeTM.Size = new Size(95, 17);
            this.optTypeTM.Text = "TM";

            this.optTypeScripted = new RadioButton("optTypeScripted");
            this.optTypeScripted.BackColor = Color.Transparent;
            this.optTypeScripted.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.optTypeScripted.Location = new Point(220, 284);
            this.optTypeScripted.Size = new Size(95, 17);
            this.optTypeScripted.Text = "Scripted";

            this.lblData1 = new Label("lblData1");
            this.lblData1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblData1.Text = "Data1:";
            this.lblData1.AutoSize = true;
            this.lblData1.Location = new Point(340, 4);

            this.nudData1 = new NumericUpDown("nudData1");
            this.nudData1.Size = new Size(100, 16);
            this.nudData1.Location = new Point(340, 16);
            this.nudData1.Minimum = int.MinValue;
            this.nudData1.Maximum = int.MaxValue;

            this.lblData2 = new Label("lblData2");
            this.lblData2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblData2.Text = "Data2:";
            this.lblData2.AutoSize = true;
            this.lblData2.Location = new Point(340, 36);

            this.nudData2 = new NumericUpDown("nudData2");
            this.nudData2.Size = new Size(100, 16);
            this.nudData2.Location = new Point(340, 48);
            this.nudData2.Minimum = int.MinValue;
            this.nudData2.Maximum = int.MaxValue;

            this.lblData3 = new Label("lblData3");
            this.lblData3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblData3.Text = "Data3:";
            this.lblData3.AutoSize = true;
            this.lblData3.Location = new Point(340, 68);

            this.nudData3 = new NumericUpDown("nudData3");
            this.nudData3.Size = new Size(100, 16);
            this.nudData3.Location = new Point(340, 80);
            this.nudData3.Minimum = int.MinValue;
            this.nudData3.Maximum = int.MaxValue;

            this.lblRarity = new Label("lblRarity");
            this.lblRarity.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblRarity.Text = "Rarity: (1-10)";
            this.lblRarity.AutoSize = true;
            this.lblRarity.Location = new Point(340, 100);

            this.nudRarity = new NumericUpDown("nudRarity");
            this.nudRarity.Size = new Size(100, 16);
            this.nudRarity.Location = new Point(340, 112);
            this.nudRarity.Minimum = 1;
            this.nudRarity.Maximum = 10;

            this.lblAtkReq = new Label("lblAtkReq");
            this.lblAtkReq.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblAtkReq.Text = "Req Data 1:";
            this.lblAtkReq.AutoSize = true;
            this.lblAtkReq.Location = new Point(340, 132);

            this.nudAtkReq = new NumericUpDown("nudAtkReq");
            this.nudAtkReq.Size = new Size(100, 16);
            this.nudAtkReq.Location = new Point(340, 144);
            this.nudAtkReq.Minimum = int.MinValue;
            this.nudAtkReq.Maximum = int.MaxValue;

            this.lblDefReq = new Label("lblDefReq");
            this.lblDefReq.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblDefReq.Text = "Req Data 2:";
            this.lblDefReq.AutoSize = true;
            this.lblDefReq.Location = new Point(340, 164);

            this.nudDefReq = new NumericUpDown("nudDefReq");
            this.nudDefReq.Size = new Size(100, 16);
            this.nudDefReq.Location = new Point(340, 176);
            this.nudDefReq.Minimum = int.MinValue;
            this.nudDefReq.Maximum = int.MaxValue;

            this.lblSpAtkReq = new Label("lblSpAtkReq");
            this.lblSpAtkReq.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblSpAtkReq.Text = "Req Data 3:";
            this.lblSpAtkReq.AutoSize = true;
            this.lblSpAtkReq.Location = new Point(340, 196);

            this.nudSpAtkReq = new NumericUpDown("nudSpAtkReq");
            this.nudSpAtkReq.Size = new Size(100, 16);
            this.nudSpAtkReq.Location = new Point(340, 208);
            this.nudSpAtkReq.Minimum = int.MinValue;
            this.nudSpAtkReq.Maximum = int.MaxValue;

            this.lblSpDefReq = new Label("lblSpDefReq");
            this.lblSpDefReq.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblSpDefReq.Text = "Req Data 4:";
            this.lblSpDefReq.AutoSize = true;
            this.lblSpDefReq.Location = new Point(340, 228);

            this.nudSpDefReq = new NumericUpDown("nudSpDefReq");
            this.nudSpDefReq.Size = new Size(100, 16);
            this.nudSpDefReq.Location = new Point(340, 240);
            this.nudSpDefReq.Minimum = int.MinValue;
            this.nudSpDefReq.Maximum = int.MaxValue;

            this.lblSpeedReq = new Label("lblSpeedReq");
            this.lblSpeedReq.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblSpeedReq.Text = "Req Data 5:";
            this.lblSpeedReq.AutoSize = true;
            this.lblSpeedReq.Location = new Point(340, 260);

            this.nudSpeedReq = new NumericUpDown("nudSpeedReq");
            this.nudSpeedReq.Size = new Size(100, 16);
            this.nudSpeedReq.Location = new Point(340, 272);
            this.nudSpeedReq.Minimum = int.MinValue;
            this.nudSpeedReq.Maximum = int.MaxValue;

            this.lblScriptedReq = new Label("lblScriptedReq");
            this.lblScriptedReq.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblScriptedReq.Text = "Req Script: >= -1";
            this.lblScriptedReq.AutoSize = true;
            this.lblScriptedReq.Location = new Point(340, 292);

            this.nudScriptedReq = new NumericUpDown("nudScriptedReq");
            this.nudScriptedReq.Size = new Size(100, 16);
            this.nudScriptedReq.Location = new Point(340, 304);
            this.nudScriptedReq.Minimum = -1;
            this.nudScriptedReq.Maximum = int.MaxValue;

            this.lblAddHP = new Label("lblAddHP");
            this.lblAddHP.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblAddHP.Text = "Add HP:";
            this.lblAddHP.AutoSize = true;
            this.lblAddHP.Location = new Point(460, 4);

            this.nudAddHP = new NumericUpDown("nudAddHP");
            this.nudAddHP.Size = new Size(100, 16);
            this.nudAddHP.Location = new Point(460, 16);
            this.nudAddHP.Minimum = int.MinValue;
            this.nudAddHP.Maximum = int.MaxValue;

            this.lblAddPP = new Label("lblAddPP");
            this.lblAddPP.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblAddPP.Text = "Add PP:";
            this.lblAddPP.AutoSize = true;
            this.lblAddPP.Location = new Point(460, 36);

            this.nudAddPP = new NumericUpDown("nudAddPP");
            this.nudAddPP.Size = new Size(100, 16);
            this.nudAddPP.Location = new Point(460, 48);
            this.nudAddPP.Minimum = int.MinValue;
            this.nudAddPP.Maximum = int.MaxValue;

            this.lblAddEXP = new Label("lblAddEXP");
            this.lblAddEXP.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblAddEXP.Text = "Add EXP: (+-100)";
            this.lblAddEXP.AutoSize = true;
            this.lblAddEXP.Location = new Point(460, 68);

            this.nudAddEXP = new NumericUpDown("nudAddEXP");
            this.nudAddEXP.Size = new Size(100, 16);
            this.nudAddEXP.Location = new Point(460, 80);
            this.nudAddEXP.Minimum = -100;
            this.nudAddEXP.Maximum = 100;

            this.lblAddAtk = new Label("lblAddAtk");
            this.lblAddAtk.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblAddAtk.Text = "Add Atk:";
            this.lblAddAtk.AutoSize = true;
            this.lblAddAtk.Location = new Point(460, 100);

            this.nudAddAtk = new NumericUpDown("nudAddAtk");
            this.nudAddAtk.Size = new Size(100, 16);
            this.nudAddAtk.Location = new Point(460, 112);
            this.nudAddAtk.Minimum = int.MinValue;
            this.nudAddAtk.Maximum = int.MaxValue;

            this.lblAddDef = new Label("lblAddDef");
            this.lblAddDef.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblAddDef.Text = "Add Def:";
            this.lblAddDef.AutoSize = true;
            this.lblAddDef.Location = new Point(460, 132);

            this.nudAddDef = new NumericUpDown("nudAddDef");
            this.nudAddDef.Size = new Size(100, 16);
            this.nudAddDef.Location = new Point(460, 144);
            this.nudAddDef.Minimum = int.MinValue;
            this.nudAddDef.Maximum = int.MaxValue;

            this.lblAddSpAtk = new Label("lblAddSpAtk");
            this.lblAddSpAtk.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblAddSpAtk.Text = "Add SpAtk:";
            this.lblAddSpAtk.AutoSize = true;
            this.lblAddSpAtk.Location = new Point(460, 164);

            this.nudAddSpAtk = new NumericUpDown("nudAddSpAtk");
            this.nudAddSpAtk.Size = new Size(100, 16);
            this.nudAddSpAtk.Location = new Point(460, 176);
            this.nudAddSpAtk.Minimum = int.MinValue;
            this.nudAddSpAtk.Maximum = int.MaxValue;

            this.lblAddSpDef = new Label("lblAddSpDef");
            this.lblAddSpDef.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblAddSpDef.Text = "Add SpDef:";
            this.lblAddSpDef.AutoSize = true;
            this.lblAddSpDef.Location = new Point(460, 196);

            this.nudAddSpDef = new NumericUpDown("nudAddSpDef");
            this.nudAddSpDef.Size = new Size(100, 16);
            this.nudAddSpDef.Location = new Point(460, 208);
            this.nudAddSpDef.Minimum = int.MinValue;
            this.nudAddSpDef.Maximum = int.MaxValue;

            this.lblAddSpeed = new Label("lblAddSpeed");
            this.lblAddSpeed.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblAddSpeed.Text = "Add Speed:";
            this.lblAddSpeed.AutoSize = true;
            this.lblAddSpeed.Location = new Point(460, 228);

            this.nudAddSpeed = new NumericUpDown("nudAddSpeed");
            this.nudAddSpeed.Size = new Size(100, 16);
            this.nudAddSpeed.Location = new Point(460, 240);
            this.nudAddSpeed.Minimum = int.MinValue;
            this.nudAddSpeed.Maximum = int.MaxValue;

            this.lblAttackSpeed = new Label("lblAttackSpeed");
            this.lblAttackSpeed.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblAttackSpeed.Text = "Hit Rate: (1-10000)"; // confirm this
            this.lblAttackSpeed.AutoSize = true;
            this.lblAttackSpeed.Location = new Point(460, 260);

            this.nudAttackSpeed = new NumericUpDown("nudAttackSpeed");
            this.nudAttackSpeed.Size = new Size(100, 16);
            this.nudAttackSpeed.Location = new Point(460, 272);
            this.nudAttackSpeed.Minimum = 1;
            this.nudAttackSpeed.Maximum = 10000;

            this.lblRecruitBonus = new Label("lblRecruitBonus");
            this.lblRecruitBonus.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblRecruitBonus.Text = "Add Recruit:";
            this.lblRecruitBonus.AutoSize = true;
            this.lblRecruitBonus.Location = new Point(460, 292);

            this.nudRecruitBonus = new NumericUpDown("nudRecruitBonus");
            this.nudRecruitBonus.Size = new Size(100, 16);
            this.nudRecruitBonus.Location = new Point(460, 304);
            this.nudRecruitBonus.Minimum = int.MinValue;
            this.nudRecruitBonus.Maximum = int.MaxValue;

            // lbxItems = new SdlDotNet.Widgets.ScrollingListBox("lbxItems");
            // lbxItems.Location = new Point(20, 20);

            // set font properties, items, etc.

            // this.AddWidget(lbxItems);
            this.pnlItemList.AddWidget(this.lbxItemList);
            this.pnlItemList.AddWidget(this.btnBack);
            this.pnlItemList.AddWidget(this.btnForward);

            // pnlItemList.AddWidget(btnAddNew);
            this.pnlItemList.AddWidget(this.btnEdit);
            this.pnlItemList.AddWidget(this.btnCancel);
            this.pnlItemList.AddWidget(this.lblSearch);
            this.pnlItemList.AddWidget(this.findTen);

            this.pnlItemEditor.AddWidget(this.lblName);
            this.pnlItemEditor.AddWidget(this.txtName);
            this.pnlItemEditor.AddWidget(this.lblSprite);
            this.pnlItemEditor.AddWidget(this.pic);
            this.pnlItemEditor.AddWidget(this.lblSellPrice);
            this.pnlItemEditor.AddWidget(this.nudSellPrice);
            this.pnlItemEditor.AddWidget(this.nudStackCap);
            this.pnlItemEditor.AddWidget(this.lblStackCap);
            this.pnlItemEditor.AddWidget(this.chkBound);
            this.pnlItemEditor.AddWidget(this.chkLoseable);
            this.pnlItemEditor.AddWidget(this.lblDescription);
            this.pnlItemEditor.AddWidget(this.txtDescription);

            this.pnlItemEditor.AddWidget(this.lblType);
            this.pnlItemEditor.AddWidget(this.optTypeNone);
            this.pnlItemEditor.AddWidget(this.optTypeHeld);
            this.pnlItemEditor.AddWidget(this.optTypeHeldByParty);
            this.pnlItemEditor.AddWidget(this.optTypeHeldInBag);
            this.pnlItemEditor.AddWidget(this.optTypePotionAddHP);
            this.pnlItemEditor.AddWidget(this.optTypePotionAddPP);
            this.pnlItemEditor.AddWidget(this.optTypePotionAddBelly);
            this.pnlItemEditor.AddWidget(this.optTypePotionSubHP);
            this.pnlItemEditor.AddWidget(this.optTypePotionSubPP);
            this.pnlItemEditor.AddWidget(this.optTypePotionSubBelly);
            this.pnlItemEditor.AddWidget(this.optTypeKey);
            this.pnlItemEditor.AddWidget(this.optTypeCurrency);
            this.pnlItemEditor.AddWidget(this.optTypeTM);
            this.pnlItemEditor.AddWidget(this.optTypeScripted);

            this.pnlItemEditor.AddWidget(this.lblData1);
            this.pnlItemEditor.AddWidget(this.nudData1);
            this.pnlItemEditor.AddWidget(this.lblData2);
            this.pnlItemEditor.AddWidget(this.nudData2);
            this.pnlItemEditor.AddWidget(this.lblData3);
            this.pnlItemEditor.AddWidget(this.nudData3);
            this.pnlItemEditor.AddWidget(this.lblRarity);
            this.pnlItemEditor.AddWidget(this.nudRarity);
            this.pnlItemEditor.AddWidget(this.lblAtkReq);
            this.pnlItemEditor.AddWidget(this.nudAtkReq);
            this.pnlItemEditor.AddWidget(this.lblDefReq);
            this.pnlItemEditor.AddWidget(this.nudDefReq);
            this.pnlItemEditor.AddWidget(this.lblSpAtkReq);
            this.pnlItemEditor.AddWidget(this.nudSpAtkReq);
            this.pnlItemEditor.AddWidget(this.lblSpDefReq);
            this.pnlItemEditor.AddWidget(this.nudSpDefReq);
            this.pnlItemEditor.AddWidget(this.lblSpeedReq);
            this.pnlItemEditor.AddWidget(this.nudSpeedReq);
            this.pnlItemEditor.AddWidget(this.lblScriptedReq);
            this.pnlItemEditor.AddWidget(this.nudScriptedReq);

            this.pnlItemEditor.AddWidget(this.lblAddHP);
            this.pnlItemEditor.AddWidget(this.nudAddHP);
            this.pnlItemEditor.AddWidget(this.lblAddPP);
            this.pnlItemEditor.AddWidget(this.nudAddPP);
            this.pnlItemEditor.AddWidget(this.lblAddAtk);
            this.pnlItemEditor.AddWidget(this.nudAddAtk);
            this.pnlItemEditor.AddWidget(this.lblAddDef);
            this.pnlItemEditor.AddWidget(this.nudAddDef);
            this.pnlItemEditor.AddWidget(this.lblAddSpAtk);
            this.pnlItemEditor.AddWidget(this.nudAddSpAtk);
            this.pnlItemEditor.AddWidget(this.lblAddSpDef);
            this.pnlItemEditor.AddWidget(this.nudAddSpDef);
            this.pnlItemEditor.AddWidget(this.lblAddSpeed);
            this.pnlItemEditor.AddWidget(this.nudAddSpeed);
            this.pnlItemEditor.AddWidget(this.lblAddEXP);
            this.pnlItemEditor.AddWidget(this.nudAddEXP);
            this.pnlItemEditor.AddWidget(this.lblAttackSpeed);
            this.pnlItemEditor.AddWidget(this.nudAttackSpeed);
            this.pnlItemEditor.AddWidget(this.lblRecruitBonus);
            this.pnlItemEditor.AddWidget(this.nudRecruitBonus);

            this.pnlItemEditor.AddWidget(this.btnEditorCancel);
            this.pnlItemEditor.AddWidget(this.btnEditorOK);

            this.AddWidget(this.pnlItemList);
            this.AddWidget(this.pnlItemEditor);

            this.LoadComplete();
        }

        private void BtnBack_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen > 0)
            {
                this.currentTen--;
            }

            this.RefreshItemList();
        }

        private void BtnForward_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen < (MaxInfo.MaxItems / 10))
            {
                this.currentTen++;
            }

            this.RefreshItemList();
        }

        public void RefreshItemList()
        {
            for (int i = 0; i < 10; i++)
            {
                if ((i + (this.currentTen * 10)) < MaxInfo.MaxItems)
                {
                    ((ListBoxTextItem)this.lbxItemList.Items[i]).Text = ((i + 1) + (10 * this.currentTen)) + ": " + Items.ItemHelper.Items[(i + 1) + (10 * this.currentTen)].Name;
                }
else
                {
                    ((ListBoxTextItem)this.lbxItemList.Items[i]).Text = "---";
                }
            }
        }

        private void BtnEdit_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxItemList.SelectedItems.Count == 1)
            {
                string[] index = ((ListBoxTextItem)this.lbxItemList.SelectedItems[0]).Text.Split(':');
                if (index[0].IsNumeric())
                {
                    this.LoadItem(index[0].ToInt());
                }
            }
        }

        public void LoadItem(int index)
        {
            this.itemNum = index;
            Items.Item item = Items.ItemHelper.Items[this.itemNum];
            this.pnlItemList.Visible = false;
            this.pnlItemEditor.Visible = true;
            this.Size = new Size(this.pnlItemEditor.Width, this.pnlItemEditor.Height);

            this.txtName.Text = item.Name;
            this.pic.SelectedTile = new Point(item.Pic % (this.pic.Width / Constants.TILEWIDTH), item.Pic / (this.pic.Width / Constants.TILEWIDTH));
            this.nudSellPrice.Value = item.Price;
            this.nudStackCap.Value = item.StackCap;
            this.chkBound.Checked = item.Bound;
            this.chkLoseable.Checked = item.Loseable;
            this.txtDescription.Text = item.Desc;
            this.nudRarity.Value = item.Rarity;
            switch (item.Type)
            {
                case Enums.ItemType.None:
                {
                        this.optTypeNone.Checked = true;
                    }

                    break;
                case Enums.ItemType.Held:
                {
                        this.optTypeHeld.Checked = true;
                    }

                    break;
                case Enums.ItemType.HeldByParty:
                {
                        this.optTypeHeldByParty.Checked = true;
                    }

                    break;
                case Enums.ItemType.HeldInBag:
                {
                        this.optTypeHeldInBag.Checked = true;
                    }

                    break;
                case Enums.ItemType.PotionAddHP:
                {
                        this.optTypePotionAddHP.Checked = true;
                    }

                    break;
                case Enums.ItemType.PotionAddPP:
                {
                        this.optTypePotionAddPP.Checked = true;
                    }

                    break;
                case Enums.ItemType.PotionAddBelly:
                {
                        this.optTypePotionAddBelly.Checked = true;
                    }

                    break;
                case Enums.ItemType.PotionSubHP:
                {
                        this.optTypePotionSubHP.Checked = true;
                    }

                    break;
                case Enums.ItemType.PotionSubPP:
                {
                        this.optTypePotionSubPP.Checked = true;
                    }

                    break;
                case Enums.ItemType.PotionSubBelly:
                {
                        this.optTypePotionSubBelly.Checked = true;
                    }

                    break;
                case Enums.ItemType.Key:
                {
                        this.optTypeKey.Checked = true;
                    }

                    break;
                case Enums.ItemType.Currency:
                {
                        this.optTypeCurrency.Checked = true;
                    }

                    break;
                case Enums.ItemType.TM:
                {
                        this.optTypeTM.Checked = true;
                    }

                    break;
                case Enums.ItemType.Scripted:
                {
                        this.optTypeScripted.Checked = true;
                    }

                    break;
                default:
                {
                        this.optTypeNone.Checked = true;
                    }

                    break;
            }

            this.nudData1.Value = item.Data1;
            this.nudData2.Value = item.Data2;
            this.nudData3.Value = item.Data3;

            this.nudAtkReq.Value = item.AttackReq;
            this.nudDefReq.Value = item.DefenseReq;
            this.nudSpAtkReq.Value = item.SpAtkReq;
            this.nudSpDefReq.Value = item.SpDefReq;
            this.nudSpeedReq.Value = item.SpeedReq;
            this.nudScriptedReq.Value = item.ScriptedReq;

            this.nudAddHP.Value = item.AddHP;
            this.nudAddPP.Value = item.AddPP;
            this.nudAddEXP.Value = item.AddEXP;
            this.nudAddAtk.Value = item.AddAttack;
            this.nudAddDef.Value = item.AddDefense;
            this.nudAddSpAtk.Value = item.AddSpAtk;
            this.nudAddSpDef.Value = item.AddSpDef;
            this.nudAddSpeed.Value = item.AddSpeed;
            this.nudAttackSpeed.Value = item.AttackSpeed;
            this.nudRecruitBonus.Value = item.RecruitBonus;
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            return;
        }

        private void FindTen_KeyUp(object sender, SdlDotNet.Input.KeyboardEventArgs e)
        {
            if (e.Key == SdlDotNet.Input.Key.Return)
            {
                int getTen = this.findTen.Value / 10;
                for (int i = 0; i < MaxInfo.MaxItems / 10; i++)
                {
                    if (i < getTen && i >= getTen - 1)
                    {
                        this.currentTen = i + 1;

                        /*bool evenTen = false;
                        string[] getselectedMove = new string[0];
                        try
                        {
                            getselectedMove = getTen.ToString().Split('.');
                        }
                        catch
                        {
                            evenTen = true;
                        }

                        if (evenTen == false && getselectedMove[1] != null)
                        {
                            lbxMoveList.SelectItem(Int32.Parse(getselectedMove[1]) - 1);
                        }
                        else
                        {
                            lbxMoveList.SelectItem(9);
                        }*/
                    }
                }

                this.RefreshItemList();
            }
        }

        private void BtnEditorCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.itemNum = -1;
            this.pnlItemEditor.Visible = false;
            this.pnlItemList.Visible = true;
            this.Size = new Size(this.pnlItemList.Width, this.pnlItemList.Height);
        }

        private void BtnEditorOK_Click(object sender, MouseButtonEventArgs e)
        {
            Items.Item itemToSend = new Items.Item();

            itemToSend.Name = this.txtName.Text;
            itemToSend.Pic = this.pic.DetermineTileNumber(this.pic.SelectedTile.X, this.pic.SelectedTile.Y);
            itemToSend.Price = this.nudSellPrice.Value;
            itemToSend.StackCap = this.nudStackCap.Value;
            itemToSend.Bound = this.chkBound.Checked;
            itemToSend.Loseable = this.chkLoseable.Checked;
            itemToSend.Desc = this.txtDescription.Text;

            itemToSend.Type = Enums.ItemType.None;
            if (this.optTypeNone.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.None;
            }

            if (this.optTypeHeld.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.Held;
            }

            if (this.optTypeHeldByParty.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.HeldByParty;
            }

            if (this.optTypeHeldInBag.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.HeldInBag;
            }

            if (this.optTypePotionAddHP.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.PotionAddHP;
            }

            if (this.optTypePotionAddPP.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.PotionAddPP;
            }

            if (this.optTypePotionAddBelly.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.PotionAddBelly;
            }

            if (this.optTypePotionSubHP.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.PotionSubHP;
            }

            if (this.optTypePotionSubPP.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.PotionSubPP;
            }

            if (this.optTypePotionSubBelly.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.PotionSubBelly;
            }

            if (this.optTypeKey.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.Key;
            }

            if (this.optTypeCurrency.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.Currency;
            }

            if (this.optTypeTM.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.TM;
            }

            if (this.optTypeScripted.Checked == true)
            {
                itemToSend.Type = Enums.ItemType.Scripted;
            }

            itemToSend.Data1 = this.nudData1.Value;

            itemToSend.Data2 = this.nudData2.Value;
            itemToSend.Data3 = this.nudData3.Value;
            itemToSend.Rarity = this.nudRarity.Value;
            itemToSend.AttackReq = this.nudAtkReq.Value;
            itemToSend.DefenseReq = this.nudDefReq.Value;
            itemToSend.SpAtkReq = this.nudSpAtkReq.Value;
            itemToSend.SpDefReq = this.nudSpDefReq.Value;
            itemToSend.SpeedReq = this.nudSpeedReq.Value;
            itemToSend.ScriptedReq = this.nudScriptedReq.Value;
            itemToSend.AddHP = this.nudAddHP.Value;
            itemToSend.AddPP = this.nudAddPP.Value;
            itemToSend.AddEXP = this.nudAddEXP.Value;
            itemToSend.AddAttack = this.nudAddAtk.Value;
            itemToSend.AddDefense = this.nudAddDef.Value;
            itemToSend.AddSpAtk = this.nudAddSpAtk.Value;
            itemToSend.AddSpDef = this.nudAddSpDef.Value;
            itemToSend.AddSpeed = this.nudAddSpeed.Value;
            itemToSend.AttackSpeed = this.nudAttackSpeed.Value;
            itemToSend.RecruitBonus = this.nudRecruitBonus.Value;

            Messenger.SendSaveItem(this.itemNum, itemToSend);
            this.pnlItemEditor.Visible = false;
            this.pnlItemList.Visible = true;
            this.Size = new Size(this.pnlItemList.Width, this.pnlItemList.Height);
        }
    }
}
