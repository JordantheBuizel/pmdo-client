﻿// <copyright file="winHouseProperties.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors.MapEditor
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Maps;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class WinHouseProperties : Window
    {
        private readonly Button btnCancel;
        private readonly Button btnOk;
        private readonly Button btnPlay;
        private readonly Button btnStop;
        private readonly ComboBox cmbMusic;
        private readonly Label lblMusic;

        private readonly HouseProperties properties;

        public WinHouseProperties()
            : base("winHouseProperties")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.TitleBar.Text = "Music";
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.TitleBar.CloseButton.Visible = true;
            this.AlwaysOnTop = true;
            this.BackColor = Color.White;

            // this.BorderStyle = SdlDotNet.Widgets.BorderStyle.FixedSingle;
            // this.BorderWidth = 1;
            // this.BorderColor = Color.Black;
            this.Size = new Size(500, 160);
            this.Location = DrawingSupport.GetCenter(SdlDotNet.Graphics.Video.Screen.Size, this.Size);

            this.properties = MapHelper.ActiveMap.ExportToHouseClass();
            this.lblMusic = new Label("lblMusic");
            this.lblMusic.Font = Graphic.FontManager.LoadFont("PMU", 22);
            this.lblMusic.AutoSize = true;
            this.lblMusic.Location = new Point(20, 10);
            this.lblMusic.Text = "Music";

            this.cmbMusic = new ComboBox("cmbMusic");
            this.cmbMusic.Size = new Size(375, 30);
            this.cmbMusic.Location = new Point(25, 50);

            this.btnPlay = new Button("btnPlay");
            this.btnPlay.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.btnPlay.Size = new Size(75, 30);
            this.btnPlay.Location = new Point(410, 20);
            Skins.SkinManager.LoadButtonGui(this.btnPlay);
            this.btnPlay.Text = "Play";
            this.btnPlay.Click += new EventHandler<MouseButtonEventArgs>(this.BtnPlay_Click);

            this.btnStop = new Button("btnStop");
            this.btnStop.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.btnStop.Size = new Size(75, 30);
            this.btnStop.Location = new Point(410, 50);
            Skins.SkinManager.LoadButtonGui(this.btnStop);
            this.btnStop.Text = "Stop";
            this.btnStop.Click += new EventHandler<MouseButtonEventArgs>(this.BtnStop_Click);

            this.btnOk = new Button("btnOk");
            this.btnOk.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.btnOk.Size = new Size(75, 30);
            this.btnOk.Location = new Point(20, 100);
            Skins.SkinManager.LoadButtonGui(this.btnOk);
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new EventHandler<MouseButtonEventArgs>(this.BtnOk_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.btnCancel.Size = new Size(75, 30);
            this.btnCancel.Location = new Point(95, 100);
            Skins.SkinManager.LoadButtonGui(this.btnCancel);
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);
            this.AddWidget(this.lblMusic);
            this.AddWidget(this.cmbMusic);
            this.AddWidget(this.btnPlay);
            this.AddWidget(this.btnStop);
            this.AddWidget(this.btnOk);
            this.AddWidget(this.btnCancel);

            this.LoadComplete();

            this.LoadMusic();
        }

        private void BtnStop_Click(object sender, MouseButtonEventArgs e)
        {
            // Stop the music
            Music.Music.AudioPlayer.StopMusic();

            // Play the map music
            if (!string.IsNullOrEmpty(MapHelper.ActiveMap.Music))
            {
                // Music.Music.AudioPlayer.PlayMusic(MapHelper.ActiveMap.Music);
                ((Music.Bass.BassAudioPlayer)Music.Music.AudioPlayer).FadeToNext(MapHelper.ActiveMap.Music, 1000);
            }
        }

        private void BtnPlay_Click(object sender, MouseButtonEventArgs e)
        {
            string song = null;
            if (this.cmbMusic.SelectedIndex > -1)
            {
                song = ((ListBoxTextItem)this.cmbMusic.SelectedItem).Text;
            }

            if (!string.IsNullOrEmpty(song))
            {
                Music.Music.AudioPlayer.PlayMusic(song, -1, true, true);
            }
        }

        private void BtnOk_Click(object sender, MouseButtonEventArgs e)
        {
            this.properties.Music = (this.cmbMusic.SelectedItem == null || string.IsNullOrEmpty(this.cmbMusic.SelectedItem.TextIdentifier)) ? this.properties.Music : this.cmbMusic.SelectedItem.TextIdentifier;
            MapHelper.ActiveMap.LoadFromHouseClass(this.properties);
            this.Close();
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void LoadMusic()
        {
            SdlDotNet.Graphics.Font font = Graphic.FontManager.LoadFont("PMU", 18);
            string[] musicFiles = System.IO.Directory.GetFiles(IO.Paths.MusicPath);
            for (int i = 0; i < musicFiles.Length; i++)
            {
                this.cmbMusic.Items.Add(new ListBoxTextItem(font, System.IO.Path.GetFileName(musicFiles[i])));
            }

            this.cmbMusic.SelectItem(this.properties.Music);
        }
    }
}
