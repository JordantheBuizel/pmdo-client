﻿// <copyright file="winScreenshotOptions.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors.MapEditor
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class WinScreenshotOptions : Window
    {
        private readonly CheckBox chkCaptureRegion;
        private readonly CheckBox chkCaptureAttributes;
        private readonly CheckBox chkCaptureMapGrid;
        private readonly Button btnTakeScreenshot;
        private readonly Button btnCancel;
        private readonly Label lblSaved;
        private readonly Timer tmrHideInfo;

        public WinScreenshotOptions()
            : base("winScreenshotOptions")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.TitleBar.Text = "Screenshot Options";
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.TitleBar.CloseButton.Visible = false;
            this.AlwaysOnTop = true;
            this.BackColor = Color.White;
            this.BorderStyle = BorderStyle.FixedSingle;
            this.BorderWidth = 2;
            this.BorderColor = Color.Black;
            this.Size = new Size(200, 150);
            this.Location = DrawingSupport.GetCenter(SdlDotNet.Graphics.Video.Screen.Size, this.Size);

            this.chkCaptureRegion = new CheckBox("chkCaptureRegion");
            this.chkCaptureRegion.BackColor = Color.Transparent;
            this.chkCaptureRegion.Location = new Point(5, 5);
            this.chkCaptureRegion.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.chkCaptureRegion.Size = new Size(200, 15);
            this.chkCaptureRegion.Text = "Only capture visible area";

            this.chkCaptureAttributes = new CheckBox("chkCaptureAttributes");
            this.chkCaptureAttributes.BackColor = Color.Transparent;
            this.chkCaptureAttributes.Location = new Point(5, 20);
            this.chkCaptureAttributes.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.chkCaptureAttributes.Size = new Size(200, 15);
            this.chkCaptureAttributes.Text = "Capture Attributes";
            this.chkCaptureAttributes.Checked = true;

            this.chkCaptureMapGrid = new CheckBox("chkCaptureMapGrid");
            this.chkCaptureMapGrid.BackColor = Color.Transparent;
            this.chkCaptureMapGrid.Location = new Point(5, 35);
            this.chkCaptureMapGrid.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.chkCaptureMapGrid.Size = new Size(200, 15);
            this.chkCaptureMapGrid.Text = "Capture Map Grid";
            this.chkCaptureMapGrid.Checked = true;

            this.btnTakeScreenshot = new Button("btnTakeScreenshot");
            this.btnTakeScreenshot.Size = new Size(70, 20);
            this.btnTakeScreenshot.Location = new Point(5, 70);
            this.btnTakeScreenshot.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.btnTakeScreenshot.Text = "Save!";
            this.btnTakeScreenshot.Click += new EventHandler<MouseButtonEventArgs>(this.BtnTakeScreenshot_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Size = new Size(70, 20);
            this.btnCancel.Location = new Point(80, 70);
            this.btnCancel.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.lblSaved = new Label("lblSaved");
            this.lblSaved.AutoSize = true;
            this.lblSaved.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.lblSaved.Location = new Point(5, this.btnTakeScreenshot.Y + this.btnTakeScreenshot.Height + 5);
            this.lblSaved.Text = "Saved!";
            this.lblSaved.Visible = false;

            this.tmrHideInfo = new Timer("tmrHideInfo");
            this.tmrHideInfo.Interval = 2000;
            this.tmrHideInfo.Elapsed += new EventHandler(this.TmrHideInfo_Elapsed);

            this.AddWidget(this.chkCaptureRegion);
            this.AddWidget(this.chkCaptureAttributes);
            this.AddWidget(this.chkCaptureMapGrid);
            this.AddWidget(this.btnTakeScreenshot);
            this.AddWidget(this.btnCancel);
            this.AddWidget(this.lblSaved);
            this.AddWidget(this.tmrHideInfo);

            this.LoadComplete();
        }

        private void TmrHideInfo_Elapsed(object sender, EventArgs e)
        {
            this.tmrHideInfo.Stop();
            this.lblSaved.Visible = false;
        }

        private void ChkCaptureRegion_Click(object sender, MouseButtonEventArgs e)
        {
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void BtnTakeScreenshot_Click(object sender, MouseButtonEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Portable Network Graphic File|*.png|GIF File|*.gif|JPEG File|*.jpg|Bitmap File|*.bmp|Icon File|*.ico";
            sfd.AddExtension = true;
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                if (Windows.WindowSwitcher.GameWindow.MapViewer.ActiveMap != null && Windows.WindowSwitcher.GameWindow.MapViewer.ActiveMap.Loaded)
                {
                    SdlDotNet.Graphics.Surface surf = Windows.WindowSwitcher.GameWindow.MapViewer.CaptureMapImage(this.chkCaptureRegion.Checked, this.chkCaptureAttributes.Checked, this.chkCaptureMapGrid.Checked);
                    Graphic.SurfaceManager.SaveSurface(surf, sfd.FileName);
                    this.tmrHideInfo.Start();
                    this.lblSaved.Visible = true;
                }
            }
        }
    }
}
