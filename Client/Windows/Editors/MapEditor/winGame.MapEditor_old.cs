﻿// <copyright file="winGame.MapEditor_old.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using Extensions;
    using Network;
    using PMU.Core;
    using SdlDotNet.Graphics;
    using SdlDotNet.Widgets;
    using SdlInput = SdlDotNet.Input;

    internal partial class WinGame
    {
        private bool inLiveMode;
        private List<Maps.Map> mapMemento;
        private int mementoIndex;
        private Enums.MapEditorLimitTypes limiter;
        private Panel mapEditorMenu;
        private Button btnTerrain;
        private Button btnAttributes;
        private Widges.TilesetViewer tilesetViewer;
        private MappingTool activeTool;
        private Panel pnlAttributes;
        private int mode;
        private Panel pnlAttOptions;
        // Button btnBack;
        private Button btnOK;
        private Button btnTitle;
        internal Label Lbl1;
        private Label lbl2;
        private Label lbl3;
        private Label lbl4;
        private Label lblMode;
        private TextBox txt1;
        private TextBox txt2;
        private TextBox txt3;
        private HScrollBar hsb1;
        private HScrollBar hsb2;
        private HScrollBar hsb3;
        private Rectangle rec = new Rectangle(96, 0, 32, 64);
        private Rectangle rec2 = new Rectangle(192, 0, 64, 64);
        private PictureBox picSprite;
        private PictureBox picSprite2;
        private ScrollingListBox lstSound;
        private CheckBox chkTake;
        private CheckBox chkHidden;
        private RadioButton optAllow;
        private RadioButton optBlock;
        private NumericUpDown nudStoryLevel;
        private bool[] tempArrayForMobility;
        private RadioButton optBlocked;
        private RadioButton optWarp;
        private RadioButton optItem;
        private RadioButton optNpcAvoid;
        private RadioButton optKey;
        private RadioButton optKeyOpen;
        private RadioButton optHeal;
        private RadioButton optKill;
        private RadioButton optSound;
        private RadioButton optScripted;
        private RadioButton optNotice;
        private RadioButton optLinkShop;
        private RadioButton optDoor;
        private RadioButton optSign;
        private RadioButton optSpriteChange;
        private RadioButton optShop;
        private RadioButton optArena;
        private RadioButton optBank;
        private RadioButton optGuildBlock;
        private RadioButton optSpriteBlock;
        private RadioButton optMobileBlock;
        private RadioButton optLevelBlock;
        private RadioButton optAssembly;
        private RadioButton optEvolution;
        private RadioButton optStory;
        private RadioButton optMission;
        private RadioButton optScriptedSign;
        private RadioButton optRoomWarp;
        private RadioButton optAmbiguous;
        private RadioButton optSlippery;
        private RadioButton optSlow;
        private RadioButton optDropShop;
        // RadioButton optHouseOwnerBlock;
        private Label lblDungeonTileValue;
        private NumericUpDown nudDungeonTileValue;
        private Panel pnlMapping;
        private Button btnMapping;
        private Button btnMapProperties;
        private Button btnHouseProperties;
        private Button btnLoadMap;
        private Button btnSaveMap;
        private Button btnTakeScreenshot;
        public Button BtnExit;
        private Button btnLayers;
        private Panel pnlLayers;
        private CheckBox chkAnim;
        private RadioButton optGround;
        private RadioButton optMask;
        private RadioButton optMask2;
        private RadioButton optFringe;
        private RadioButton optFringe2;
        private HScrollBar hRotationSelect;
        private Label lblRotation;
        private CheckBox chkFlip;
        private RadioButton optRotate0;
        private RadioButton optRotate90;
        private RadioButton optRotate180;
        private RadioButton optRotate270;
        private Button btnFill;
        private Button btnClear;
        private Button btnEyeDropper;
        private Panel pnlTileset;
        private Button btnTileset;
        private HScrollBar hTilesetSelect;
        private Label lblSelectedTileset;
        private PictureBox[] picTilesetPreview;
        private Button btnLoadTileset;
        private Panel pnlSettings;
        private Button btnSettings;
        private Label lblDisplaySettings;
        private CheckBox chkDisplayMapGrid;
        private CheckBox chkDisplayAttributes;
        private CheckBox chkDisplayDungeonValues;
        private Label lblMappingSettings;
        private CheckBox chkDragAndPlace;

        public enum MappingTool
        {
            Editor,
            EyeDropper
        }

        public void InitMapEditorWidgets()
        {
            this.mapEditorMenu = new Panel("mapEditor_Menu")
            {
                Location = new Point(0, this.shortcutBar.Y),
                Size = this.shortcutBar.Size,
                BackColor = Color.Transparent
            };
            this.tilesetViewer = new Widges.TilesetViewer("tilesetViewer")
            {
                Location = new Point(0, this.pnlTeam.Height + 32),
                Size = new Size(this.mapViewer.X, Screen.Height - this.pnlTeam.Height - this.shortcutBar.Height - 32),
                ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[0],
                Visible = false
            };
            this.btnTerrain = new Button("btnTerrain")
            {
                Font = Graphic.FontManager.LoadFont("PMU", 18),
                Location = new Point(0, this.pnlTeam.Height),
                Size = new Size(this.mapViewer.X / 2, 32),
                Text = "Terrain",
                Selected = true
            };
            this.btnTerrain.Click += new EventHandler<MouseButtonEventArgs>(this.BtnTerrain_Click);
            this.btnTerrain.Visible = false;
            Skins.SkinManager.LoadButtonGui(this.btnTerrain);

            this.btnAttributes = new Button("btnAttributes")
            {
                Font = Graphic.FontManager.LoadFont("PMU", 18),
                Location = new Point(this.mapViewer.X / 2, this.pnlTeam.Height),
                Size = new Size(this.mapViewer.X / 2, 32),
                Text = "Attributes",
                Selected = false
            };
            this.btnAttributes.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAttributes_Click);
            this.btnAttributes.Visible = false;
            Skins.SkinManager.LoadButtonGui(this.btnAttributes);
            this.pnlAttributes = new Panel("pnlAttributes")
            {
                Size = this.tilesetViewer.Size,
                Location = this.tilesetViewer.Location,
                BackColor = Color.White,
                Visible = false
            };
            this.pnlAttOptions = new Panel("pnlAttOptions")
            {
                Size = this.tilesetViewer.Size,
                Location = new Point(0, this.pnlTeam.Height),
                BackColor = Color.White,
                Visible = false
            };
            this.btnTitle = new Button("btnTitle")
            {
                Location = new Point(0, 0),
                Size = new Size(134, 32),
                Font = Graphic.FontManager.LoadFont("PMU", 18),
                Visible = false
            };
            Skins.SkinManager.LoadButtonGui(this.btnTitle);

            this.Lbl1 = new Label("lbl1")
            {
                AutoSize = true,
                Font = Graphic.FontManager.LoadFont("PMU", 18),
                Location = new Point(0, 35),
                Visible = false
            };
            this.lbl2 = new Label("lbl2")
            {
                AutoSize = true,
                Font = Graphic.FontManager.LoadFont("PMU", 18),
                Visible = false
            };
            this.lbl3 = new Label("lbl3")
            {
                AutoSize = true,
                Font = Graphic.FontManager.LoadFont("PMU", 18),
                Visible = false
            };
            this.lbl4 = new Label("lbl4")
            {
                AutoSize = true,
                Font = Graphic.FontManager.LoadFont("PMU", 18),
                Visible = false
            };
            this.lblMode = new Label("lblMode")
            {
                AutoSize = true,
                Font = Graphic.FontManager.LoadFont("PMU", 18),
                Location = default(Point),
                Visible = false
            };
            this.txt1 = new TextBox("txt1")
            {
                Size = new Size(134, 18),
                Visible = false
            };
            this.txt2 = new TextBox("txt2")
            {
                Size = new Size(134, 18),
                Visible = false
            };
            this.txt3 = new TextBox("txt3")
            {
                Size = new Size(134, 18),
                Visible = false
            };
            this.hsb1 = new HScrollBar("hsb1")
            {
                Size = new Size(134, 20),
                Visible = false
            };
            this.hsb1.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.Hsb1_ValueChanged);

            this.hsb2 = new HScrollBar("hsb2")
            {
                Size = new Size(134, 20),
                Visible = false
            };
            this.hsb2.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.Hsb2_ValueChanged);

            this.hsb3 = new HScrollBar("hsb3")
            {
                Size = new Size(134, 20),
                Visible = false
            };
            this.hsb3.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.Hsb3_ValueChanged);

            this.picSprite = new PictureBox("picSprite")
            {
                Size = new Size(32, 64)
            };
            this.picSprite.BlitToBuffer(Graphic.GraphicsManager.GetSpriteSheet(this.hsb1.Value).GetSheet(Graphic.FrameType.Idle, Enums.Direction.Down), new Rectangle(96, 0, 32, 64));

            this.picSprite.Location = new Point(140, 35);
            this.picSprite.BackColor = Color.White;
            this.picSprite.BorderStyle = BorderStyle.FixedSingle;
            this.picSprite.Visible = false;

            this.picSprite2 = new PictureBox("picSprite2")
            {
                Size = new Size(32, 64)
            };
            this.picSprite2.BlitToBuffer(Graphic.GraphicsManager.GetSpriteSheet(this.hsb2.Value).GetSheet(Graphic.FrameType.Idle, Enums.Direction.Down), new Rectangle(96, 0, 32, 64));

            this.picSprite2.Location = new Point(75, 130);
            this.picSprite2.BackColor = Color.White;
            this.picSprite2.BorderStyle = BorderStyle.FixedSingle;
            this.picSprite2.Visible = false;

            this.lstSound = new ScrollingListBox("lstSound")
            {
                Location = new Point(10, 60),
                Size = new Size(180, 140)
            };
            {
                SdlDotNet.Graphics.Font font = Graphic.FontManager.LoadFont("PMU", 18);
                string[] sfxFiles = Directory.GetFiles(IO.Paths.SfxPath);
                for (int i = 0; i < sfxFiles.Length; i++)
                {
                    this.lstSound.Items.Add(new ListBoxTextItem(font, Path.GetFileName(sfxFiles[i])));
                }
            }

            this.lstSound.Visible = false;

            this.chkTake = new CheckBox("chkTake")
            {
                BackColor = Color.Transparent,
                Size = new Size(125, 17),
                Font = Graphic.FontManager.LoadFont("tahoma", 8),
                Text = "Take away key upon use",
                Visible = false
            };
            this.chkHidden = new CheckBox("chkHidden")
            {
                BackColor = Color.Transparent,
                Size = new Size(125, 17),
                Font = Graphic.FontManager.LoadFont("tahoma", 8),
                Text = "Hidden",
                Visible = false
            };
            this.chkHidden.CheckChanged += new EventHandler(this.ChkHidden_CheckChanged);

            this.nudStoryLevel = new NumericUpDown("nudStoryLevel")
            {
                Size = new Size(134, 20),
                Font = Graphic.FontManager.LoadFont("PMU", 18),
                Visible = false
            };
            this.optAllow = new RadioButton("optAllow")
            {
                Size = new Size(75, 17),
                Location = new Point(0, 200),
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Text = "Allow",
                Visible = false
            };
            this.optAllow.CheckChanged += new EventHandler(this.OptAllow_CheckChanged);

            this.optBlock = new RadioButton("optBlock")
            {
                Size = new Size(75, 17),
                Location = new Point(100, 200),
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Text = "Block",
                Visible = false
            };
            this.optBlock.CheckChanged += new EventHandler(this.OptBlock_CheckChanged);

            this.btnOK = new Button("btnOK")
            {
                Location = new Point(this.mapViewer.X / 4, Screen.Height - this.shortcutBar.Height - 32),
                Size = new Size(this.mapViewer.X / 2, 32),
                Font = Graphic.FontManager.LoadFont("PMU", 18),
                Text = "Ok",
                Visible = false
            };
            this.btnOK.Click += new EventHandler<MouseButtonEventArgs>(this.BtnOK_Click);
            Skins.SkinManager.LoadButtonGui(this.btnOK);

            /*btnBack = new Button("btnBack");
            btnBack.Location = new Point(0, Screen.Height - shortcutBar.Height - 32);
            btnBack.Size = new System.Drawing.Size(this.mapViewer.X / 2, 32);
            btnBack.Font = Graphic.FontManager.LoadFont("PMU", 18);
            btnBack.Text = "Back";
            btnBack.Visible = false;
            btnBack.Click +=new EventHandler<MouseButtonEventArgs>(btnBack_Click);
            Skins.SkinManager.LoadButtonGui(btnBack);*/

            this.pnlAttOptions.AddWidget(this.Lbl1);
            this.pnlAttOptions.AddWidget(this.lbl2);
            this.pnlAttOptions.AddWidget(this.lbl3);
            this.pnlAttOptions.AddWidget(this.lbl4);
            this.pnlAttOptions.AddWidget(this.lblMode);
            this.pnlAttOptions.AddWidget(this.txt1);
            this.pnlAttOptions.AddWidget(this.txt2);
            this.pnlAttOptions.AddWidget(this.txt3);
            this.pnlAttOptions.AddWidget(this.hsb1);
            this.pnlAttOptions.AddWidget(this.hsb2);
            this.pnlAttOptions.AddWidget(this.hsb3);
            this.pnlAttOptions.AddWidget(this.picSprite);
            this.pnlAttOptions.AddWidget(this.picSprite2);
            this.pnlAttOptions.AddWidget(this.lstSound);
            this.pnlAttOptions.AddWidget(this.chkTake);
            this.pnlAttOptions.AddWidget(this.chkHidden);
            this.pnlAttOptions.AddWidget(this.nudStoryLevel);
            this.pnlAttOptions.AddWidget(this.optAllow);
            this.pnlAttOptions.AddWidget(this.optBlock);
            this.pnlAttOptions.AddWidget(this.btnTitle);
            this.optBlocked = new RadioButton("optBlocked")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 6),
                Size = new Size(95, 17),
                Text = "Blocked",
                Checked = true
            };
            this.optNpcAvoid = new RadioButton("optNpcAvoid")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 75),
                Size = new Size(95, 17),
                Text = "Npc Avoid"
            };
            this.optNotice = new RadioButton("optNotice")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 236),
                Size = new Size(95, 17),
                Text = "Notice"
            };
            this.optNotice.Click += new EventHandler<MouseButtonEventArgs>(this.OptNotice_CheckChanged);

            this.optSign = new RadioButton("optSign")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 6),
                Size = new Size(95, 17),
                Text = "Sign"
            };
            this.optSign.Click += new EventHandler<MouseButtonEventArgs>(this.OptSign_CheckChanged);

            this.optHeal = new RadioButton("optHeal")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 144),
                Size = new Size(95, 17),
                Text = "Heal"
            };
            this.optKill = new RadioButton("optKill")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 167),
                Size = new Size(95, 17),
                Text = "Kill"
            };
            this.optGuildBlock = new RadioButton("optGuildBlock")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 98),
                Size = new Size(95, 17),
                Text = "Guild"
            };

            // optGuildBlock.Click += new EventHandler<MouseButtonEventArgs>(optGuildBlock_CheckChanged);
            this.optLevelBlock = new RadioButton("optLevelBlock")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 144),
                Size = new Size(95, 17),
                Text = "Level Block"
            };
            this.optLevelBlock.Click += new EventHandler<MouseButtonEventArgs>(this.OptLevelBlock_CheckChanged);

            this.optSpriteChange = new RadioButton("optSpriteChange")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 282),
                Size = new Size(112, 17),
                Text = "New Sprite"
            };
            this.optSpriteChange.Click += new EventHandler<MouseButtonEventArgs>(this.OptSpriteChange_CheckChanged);

            this.optWarp = new RadioButton("optWarp")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 29),
                Size = new Size(95, 17),
                Text = "Warp"
            };
            this.optWarp.CheckChanged += new EventHandler(this.OptWarp_CheckChanged);

            this.optItem = new RadioButton("optItem")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 52),
                Size = new Size(95, 17),
                Text = "Item"
            };
            this.optItem.CheckChanged += new EventHandler(this.OptItem_CheckChanged);

            this.optKey = new RadioButton("optKey")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 98),
                Size = new Size(95, 17),
                Text = "Key"
            };
            this.optKey.CheckChanged += new EventHandler(this.OptKey_CheckChanged);

            this.optKeyOpen = new RadioButton("optKeyOpen")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 121),
                Size = new Size(95, 17),
                Text = "Key Open"
            };
            this.optKeyOpen.CheckChanged += new EventHandler(this.OptKeyOpen_CheckChanged);

            this.optSound = new RadioButton("optSound")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 190),
                Size = new Size(95, 17),
                Text = "Sound"
            };
            this.optSound.CheckChanged += new EventHandler(this.OptSound_CheckChanged);

            this.optScripted = new RadioButton("optScripted")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 213),
                Size = new Size(95, 17),
                Text = "Scripted"
            };
            this.optScripted.CheckChanged += new EventHandler(this.OptScripted_CheckChanged);

            this.optDoor = new RadioButton("optDoor")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 259),
                Size = new Size(95, 17),
                Text = "Door"
            };
            this.optShop = new RadioButton("optShop")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 29),
                Size = new Size(95, 17),
                Text = "Shop"
            };
            this.optShop.CheckChanged += new EventHandler(this.OptShop_CheckChanged);

            this.optArena = new RadioButton("optArena")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 52),
                Size = new Size(95, 17),
                Text = "Arena"
            };
            this.optArena.CheckChanged += new EventHandler(this.OptArena_CheckChanged);

            this.optBank = new RadioButton("optBank")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 75),
                Size = new Size(95, 17),
                Text = "Bank"
            };
            this.optBank.CheckChanged += new EventHandler(this.OptBank_CheckChanged);

            this.optSpriteBlock = new RadioButton("optSpriteBlock")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 121),
                Size = new Size(105, 17),
                Text = "Sprite Block"
            };
            this.optSpriteBlock.CheckChanged += new EventHandler(this.OptSpriteBlock_CheckChanged);

            this.optAssembly = new RadioButton("optAssembly")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 328),
                Size = new Size(145, 17),
                Text = "Assembly"
            };
            this.optEvolution = new RadioButton("optEvolution")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 167),
                Size = new Size(95, 17),
                Text = "Evolution"
            };
            this.optStory = new RadioButton("optStory")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 190),
                Size = new Size(95, 17),
                Text = "Story"
            };
            this.optStory.CheckChanged += new EventHandler(this.OptStory_CheckChanged);

            this.optLinkShop = new RadioButton("optLinkShop")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 213),
                Size = new Size(95, 17),
                Text = "Link Shop"
            };
            this.optLinkShop.CheckChanged += new EventHandler(this.OptLinkShop_CheckChanged);

            this.optMobileBlock = new RadioButton("optMobileBlock")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 236),
                Size = new Size(105, 17),
                Text = "Mobile Block"
            };
            this.optMobileBlock.CheckChanged += new EventHandler(this.OptMobileBlock_CheckChanged);

            this.optMission = new RadioButton("optMission")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 351),
                Size = new Size(115, 17),
                Text = "Mission Board"
            };
            this.optScriptedSign = new RadioButton("optScriptedSign")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(8, 305),
                Size = new Size(105, 17),
                Text = "Scripted Sign"
            };
            this.optScriptedSign.CheckChanged += new EventHandler(this.OptScriptedSign_CheckChanged);

            this.optAmbiguous = new RadioButton("optAmbiguous")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 259),
                Size = new Size(95, 17),
                Text = "Ambiguous"
            };
            this.optSlippery = new RadioButton("optSlippery")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 282),
                Size = new Size(95, 17),
                Text = "Slippery"
            };
            this.optSlippery.CheckChanged += new EventHandler(this.OptSlippery_CheckChanged);

            this.optSlow = new RadioButton("optSlow")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 305),
                Size = new Size(95, 17),
                Text = "Slow"
            };
            this.optSlow.CheckChanged += new EventHandler(this.OptSlow_CheckChanged);

            this.optDropShop = new RadioButton("optDropShop")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(105, 328),
                Size = new Size(95, 17),
                Text = "DropShop"
            };
            this.optDropShop.CheckChanged += new EventHandler(this.OptDropShop_CheckChanged);

            this.lblDungeonTileValue = new Label("lblDungeonTileValue")
            {
                AutoSize = true,
                Font = Graphic.FontManager.LoadFont("PMU", 18),
                Text = "Dungeon Tile Value:",
                Location = new Point(8, 376)
            };
            this.nudDungeonTileValue = new NumericUpDown("nudDungeonTileValue")
            {
                Size = new Size(134, 20),
                Maximum = int.MaxValue,
                Font = Graphic.FontManager.LoadFont("PMU", 18),
                Location = new Point(10, 400)
            };
            this.optRoomWarp = new RadioButton("optRoomWarp")
            {
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma", 12),
                Location = new Point(115, 6),
                Size = new Size(95, 17),
                Text = "Warp to Room"
            };
            this.tempArrayForMobility = new bool[16];

            this.pnlAttributes.AddWidget(this.optBlocked);
            this.pnlAttributes.AddWidget(this.optWarp);
            this.pnlAttributes.AddWidget(this.optItem);
            this.pnlAttributes.AddWidget(this.optNpcAvoid);
            this.pnlAttributes.AddWidget(this.optKey);
            this.pnlAttributes.AddWidget(this.optKeyOpen);
            this.pnlAttributes.AddWidget(this.optHeal);
            this.pnlAttributes.AddWidget(this.optKill);
            this.pnlAttributes.AddWidget(this.optSound);
            this.pnlAttributes.AddWidget(this.optScripted);
            this.pnlAttributes.AddWidget(this.optNotice);
            this.pnlAttributes.AddWidget(this.optDoor);
            this.pnlAttributes.AddWidget(this.optSign);
            this.pnlAttributes.AddWidget(this.optSpriteChange);
            this.pnlAttributes.AddWidget(this.optShop);
            this.pnlAttributes.AddWidget(this.optArena);
            this.pnlAttributes.AddWidget(this.optBank);
            this.pnlAttributes.AddWidget(this.optGuildBlock);
            this.pnlAttributes.AddWidget(this.optSpriteBlock);
            this.pnlAttributes.AddWidget(this.optMobileBlock);
            this.pnlAttributes.AddWidget(this.optLevelBlock);
            this.pnlAttributes.AddWidget(this.optAssembly);
            this.pnlAttributes.AddWidget(this.optEvolution);
            this.pnlAttributes.AddWidget(this.optStory);
            this.pnlAttributes.AddWidget(this.optLinkShop);
            this.pnlAttributes.AddWidget(this.optMission);
            this.pnlAttributes.AddWidget(this.optScriptedSign);
            this.pnlAttributes.AddWidget(this.optAmbiguous);
            this.pnlAttributes.AddWidget(this.optSlippery);
            this.pnlAttributes.AddWidget(this.optSlow);
            this.pnlAttributes.AddWidget(this.optDropShop);
            this.pnlAttributes.AddWidget(this.lblDungeonTileValue);
            this.pnlAttributes.AddWidget(this.nudDungeonTileValue);
            this.btnMapping = new Button("btnMapping")
            {
                Size = new Size(100, 30),
                Location = new Point(0, 0),
                Font = Graphic.FontManager.LoadFont("PMD.ttf", 24),
                Text = "Mapping",
                MouseHoverDelay = 100
            };
            this.btnMapping.Click += new EventHandler<MouseButtonEventArgs>(this.BtnMapping_Click);
            this.btnMapping.MouseHover += new EventHandler(this.BtnMapping_MouseHover);
            Skins.SkinManager.LoadButtonGui(this.btnMapping);

            this.pnlMapping = new Panel("pnlMapping")
            {
                Size = new Size(150, 190)
            };
            this.pnlMapping.Location = new Point(0, this.btnMapping.Y + this.mapEditorMenu.Y - this.pnlMapping.Height);
            this.pnlMapping.BackColor = Color.White;
            this.pnlMapping.BorderStyle = BorderStyle.FixedSingle;
            this.pnlMapping.SetAutoHide();

            this.btnMapProperties = new Button("btnMapProperties")
            {
                Size = new Size(100, 30)
            };
            this.btnMapProperties.Location = new Point(DrawingSupport.GetCenter(this.pnlMapping.Width, this.btnMapProperties.Width), 10);
            this.btnMapProperties.Font = Graphic.FontManager.LoadFont("tahoma.ttf", 10);
            this.btnMapProperties.Text = "Properties";
            this.btnMapProperties.Click += new EventHandler<MouseButtonEventArgs>(this.BtnMapProperties_Click);
            Skins.SkinManager.LoadButtonGui(this.btnMapProperties);

            this.btnHouseProperties = new Button("btnHouseProperties")
            {
                Size = new Size(100, 30),
                Location = new Point(DrawingSupport.GetCenter(this.pnlMapping.Width, this.btnMapProperties.Width), 10),
                Font = Graphic.FontManager.LoadFont("tahoma.ttf", 10),
                Text = "Music"
            };
            this.btnHouseProperties.Click += new EventHandler<MouseButtonEventArgs>(this.BtnHouseProperties_Click);
            Skins.SkinManager.LoadButtonGui(this.btnHouseProperties);

            this.btnLoadMap = new Button("btnLoadMap")
            {
                Size = new Size(100, 30)
            };
            this.btnLoadMap.Location = new Point(DrawingSupport.GetCenter(this.pnlMapping.Width, this.btnLoadMap.Width), 40);
            this.btnLoadMap.Font = Graphic.FontManager.LoadFont("tahoma.ttf", 10);
            this.btnLoadMap.Text = "Load Map";
            this.btnLoadMap.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadMap_Click);
            Skins.SkinManager.LoadButtonGui(this.btnLoadMap);

            this.btnSaveMap = new Button("btnSaveMap")
            {
                Size = new Size(100, 30)
            };
            this.btnSaveMap.Location = new Point(DrawingSupport.GetCenter(this.pnlMapping.Width, this.btnSaveMap.Width), 70);
            this.btnSaveMap.Font = Graphic.FontManager.LoadFont("tahoma.ttf", 10);
            this.btnSaveMap.Text = "Save Map";
            this.btnSaveMap.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSaveMap_Click);
            Skins.SkinManager.LoadButtonGui(this.btnSaveMap);

            this.btnTakeScreenshot = new Button("btnTakeScreenshot")
            {
                Size = new Size(100, 30)
            };
            this.btnTakeScreenshot.Location = new Point(DrawingSupport.GetCenter(this.pnlMapping.Width, this.btnTakeScreenshot.Width), 100);
            this.btnTakeScreenshot.Font = Graphic.FontManager.LoadFont("tahoma.ttf", 10);
            this.btnTakeScreenshot.Text = "Take Screenshot";
            this.btnTakeScreenshot.Click += new EventHandler<MouseButtonEventArgs>(this.BtnTakeScreenshot_Click);
            Skins.SkinManager.LoadButtonGui(this.btnTakeScreenshot);

            this.BtnExit = new Button("btnExit")
            {
                Size = new Size(100, 30)
            };
            this.BtnExit.Location = new Point(DrawingSupport.GetCenter(this.pnlMapping.Width, this.BtnExit.Width), 130);
            this.BtnExit.Font = Graphic.FontManager.LoadFont("tahoma.ttf", 10);
            this.BtnExit.Text = "Exit";
            this.BtnExit.Click += new EventHandler<MouseButtonEventArgs>(this.BtnExit_Click);
            Skins.SkinManager.LoadButtonGui(this.BtnExit);

            this.pnlMapping.AddWidget(this.btnMapProperties);
            this.pnlMapping.AddWidget(this.btnHouseProperties);
            this.pnlMapping.AddWidget(this.btnLoadMap);
            this.pnlMapping.AddWidget(this.btnSaveMap);
            this.pnlMapping.AddWidget(this.btnTakeScreenshot);
            this.pnlMapping.AddWidget(this.BtnExit);
            this.btnLayers = new Button("btnLayers")
            {
                Size = new Size(100, 30),
                Location = new Point(100, 0),
                Font = Graphic.FontManager.LoadFont("PMD.ttf", 24),
                Text = "Layers",
                MouseHoverDelay = 100
            };
            this.btnLayers.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLayers_Click);
            this.btnLayers.MouseHover += new EventHandler(this.BtnLayers_MouseHover);
            Skins.SkinManager.LoadButtonGui(this.btnLayers);

            this.pnlLayers = new Panel("pnlLayers")
            {
                Size = new Size(350, 100)
            };
            this.pnlLayers.Location = new Point(100, this.btnLayers.Y + this.mapEditorMenu.Y - this.pnlLayers.Height);
            this.pnlLayers.BackColor = Color.White;
            this.pnlLayers.BorderStyle = BorderStyle.FixedSingle;
            this.pnlLayers.SetAutoHide();

            this.optGround = new RadioButton("optGround")
            {
                BackColor = Color.Transparent,
                Location = new Point(8, 5),
                Size = new Size(60, 17),
                Font = Graphic.FontManager.LoadFont("tahoma", 8),
                Text = "Ground",
                Checked = true
            };
            this.optMask = new RadioButton("optMask")
            {
                BackColor = Color.Transparent,
                Location = new Point(74, 5),
                Size = new Size(51, 17),
                Font = Graphic.FontManager.LoadFont("tahoma", 8),
                Text = "Mask"
            };
            this.optMask2 = new RadioButton("optMask2")
            {
                BackColor = Color.Transparent,
                Location = new Point(131, 5),
                Size = new Size(60, 17),
                Font = Graphic.FontManager.LoadFont("tahoma", 8),
                Text = "Mask 2"
            };
            this.optFringe = new RadioButton("optFringe")
            {
                BackColor = Color.Transparent,
                Location = new Point(197, 5),
                Size = new Size(54, 17),
                Font = Graphic.FontManager.LoadFont("tahoma", 8),
                Text = "Fringe"
            };
            this.optFringe2 = new RadioButton("optFringe2")
            {
                BackColor = Color.Transparent,
                Location = new Point(257, 5),
                Size = new Size(63, 17),
                Font = Graphic.FontManager.LoadFont("tahoma", 8),
                Text = "Fringe 2"
            };
            this.chkFlip = new CheckBox("chkFlip")
            {
                BackColor = Color.Transparent,
                Location = new Point(197, 50),
                Size = new Size(70, 23),
                Font = Graphic.FontManager.LoadFont("tahoma", 8),
                Text = "Flip"
            };
            this.lblRotation = new Label("lblRotation")
            {
                AutoSize = true,
                Location = new Point(5, 34),
                Font = Graphic.FontManager.LoadFont("tahoma.ttf", 10),
                Text = "Rotate Tile 0°"
            };
            this.hRotationSelect = new HScrollBar("hRotationSelect")
            {
                Location = new Point(5, 21),
                Size = new Size(290, 12),
                Minimum = 0,
                Maximum = 3
            };
            this.hRotationSelect.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.HRotationSelect_MouseUp);
            this.chkAnim = new CheckBox("chkAnim")
            {
                BackColor = Color.Transparent,
                Location = new Point(8, 50),
                Size = new Size(70, 17),
                Font = Graphic.FontManager.LoadFont("tahoma", 8),
                Text = "Animation"
            };
            this.btnFill = new Button("btnFill")
            {
                Location = new Point(8, 70),
                Size = new Size(30, 20),
                Text = "Fill",
                Font = Graphic.FontManager.LoadFont("tahoma", 8)
            };
            this.btnFill.Click += new EventHandler<MouseButtonEventArgs>(this.BtnFill_Click);
            Skins.SkinManager.LoadButtonGui(this.btnFill);

            this.btnClear = new Button("btnClear")
            {
                Location = new Point(40, 70),
                Size = new Size(30, 20),
                Text = "Clear",
                Font = Graphic.FontManager.LoadFont("tahoma", 8)
            };
            this.btnClear.Click += new EventHandler<MouseButtonEventArgs>(this.BtnClear_Click);
            Skins.SkinManager.LoadButtonGui(this.btnClear);

            this.btnEyeDropper = new Button("btnEyeDropper")
            {
                Location = new Point(72, 70),
                Size = new Size(100, 20),
                Text = "Eye Dropper"
            };
            this.btnEyeDropper.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEyeDropper_Click);
            Skins.SkinManager.LoadButtonGui(this.btnEyeDropper);

            this.pnlLayers.AddWidget(this.optGround);
            this.pnlLayers.AddWidget(this.optMask);
            this.pnlLayers.AddWidget(this.optMask2);
            this.pnlLayers.AddWidget(this.optFringe);
            this.pnlLayers.AddWidget(this.optFringe2);
            this.pnlLayers.AddWidget(this.chkFlip);
            this.pnlLayers.AddWidget(this.lblRotation);
            this.pnlLayers.AddWidget(this.hRotationSelect);
            this.pnlLayers.AddWidget(this.chkAnim);
            this.pnlLayers.AddWidget(this.btnFill);
            this.pnlLayers.AddWidget(this.btnClear);
            this.pnlLayers.AddWidget(this.btnEyeDropper);
            this.btnTileset = new Button("btnTileset")
            {
                Size = new Size(100, 30),
                Location = new Point(200, 0),
                Font = Graphic.FontManager.LoadFont("PMD.ttf", 24),
                Text = "Tilesets",
                MouseHoverDelay = 100
            };
            this.btnTileset.Click += new EventHandler<MouseButtonEventArgs>(this.BtnTileset_Click);
            this.btnTileset.MouseHover += new EventHandler(this.BtnTileset_MouseHover);
            Skins.SkinManager.LoadButtonGui(this.btnTileset);

            this.pnlTileset = new Panel("pnlTileset")
            {
                Size = new Size(300, 100)
            };
            this.pnlTileset.Location = new Point(200, this.btnTileset.Y + this.mapEditorMenu.Y - this.pnlTileset.Height);
            this.pnlTileset.BackColor = Color.White;
            this.pnlTileset.BorderStyle = BorderStyle.FixedSingle;
            this.pnlTileset.SetAutoHide();

            this.hTilesetSelect = new HScrollBar("hTilesetSelect")
            {
                Location = new Point(5, 60),
                Size = new Size(290, 12),
                Maximum = 10
            };
            this.hTilesetSelect.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.HTilesetSelect_ValueChanged);

            this.lblSelectedTileset = new Label("lblSelectedTileset")
            {
                AutoSize = true,
                Location = new Point(5, 75),
                Font = Graphic.FontManager.LoadFont("tahoma.ttf", 10),
                Text = "Tileset -1"
            };
            this.btnLoadTileset = new Button("btnLoadTileset")
            {
                Size = new Size(30, 20),
                Location = new Point(265, 75),
                Text = "Load"
            };
            this.btnLoadTileset.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadTileset_Click);
            Skins.SkinManager.LoadButtonGui(this.btnLoadTileset);

            this.picTilesetPreview = new PictureBox[8];

            for (int i = 0; i < this.picTilesetPreview.Length; i++)
            {
                this.picTilesetPreview[i] = new PictureBox("picTilesetPreview" + i)
                {
                    Location = new Point(5 + (i * 37), 10),
                    Size = new Size(32, 32),
                    BackColor = Color.White,
                    BorderStyle = BorderStyle.FixedSingle
                };
            }

            this.pnlTileset.AddWidget(this.hTilesetSelect);
            this.pnlTileset.AddWidget(this.lblSelectedTileset);
            this.pnlTileset.AddWidget(this.btnLoadTileset);

            for (int i = 0; i < this.picTilesetPreview.Length; i++)
            {
                this.pnlTileset.AddWidget(this.picTilesetPreview[i]);
            }

            this.btnSettings = new Button("btnSettings")
            {
                Size = new Size(100, 30),
                Location = new Point(300, 0),
                Font = Graphic.FontManager.LoadFont("PMD.ttf", 24),
                Text = "Settings",
                MouseHoverDelay = 100
            };
            this.btnSettings.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSettings_Click);
            this.btnSettings.MouseHover += new EventHandler(this.BtnSettings_MouseHover);
            Skins.SkinManager.LoadButtonGui(this.btnSettings);

            this.pnlSettings = new Panel("pnlOptions")
            {
                Size = new Size(200, 200)
            };
            this.pnlSettings.Location = new Point(300, this.btnSettings.Y + this.mapEditorMenu.Y - this.pnlSettings.Height);
            this.pnlSettings.BackColor = Color.White;
            this.pnlSettings.BorderStyle = BorderStyle.FixedSingle;
            this.pnlSettings.SetAutoHide();

            this.lblDisplaySettings = new Label("lblDisplaySettings")
            {
                AutoSize = true,
                Location = new Point(5, 5),
                Font = Graphic.FontManager.LoadFont("tahoma.ttf", 12),
                Text = "Display Settings"
            };
            this.chkDisplayMapGrid = new CheckBox("chkDisplayMapGrid")
            {
                Location = new Point(5, 25),
                Size = new Size(125, 17),
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma.ttf", 10),
                Text = "Map Grid",
                Checked = IO.Options.MapGrid
            };
            this.chkDisplayMapGrid.CheckChanged += new EventHandler(this.ChkDisplayMapGrid_CheckChanged);

            this.chkDisplayAttributes = new CheckBox("chkDisplayAttributes")
            {
                Location = new Point(5, 45),
                Size = new Size(125, 17),
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma.ttf", 10),
                Text = "Attributes",
                Checked = IO.Options.DisplayAttributes
            };
            this.chkDisplayAttributes.CheckChanged += new EventHandler(this.ChkDisplayAttributes_CheckChanged);

            this.chkDisplayDungeonValues = new CheckBox("chkDisplayDungeonValues")
            {
                Location = new Point(5, 65),
                Size = new Size(125, 17),
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma.ttf", 10),
                Text = "DungeonValues",
                Checked = IO.Options.DisplayDungeonValues
            };
            this.chkDisplayDungeonValues.CheckChanged += new EventHandler(this.ChkDisplayDungeonValues_CheckChanged);

            this.lblMappingSettings = new Label("lblMappingSettings")
            {
                AutoSize = true,
                Location = new Point(5, 85),
                Font = Graphic.FontManager.LoadFont("tahoma.ttf", 12),
                Text = "Mapping Settings"
            };
            this.chkDragAndPlace = new CheckBox("chkDragAndPlace")
            {
                Location = new Point(5, 105),
                Size = new Size(125, 17),
                BackColor = Color.Transparent,
                Font = Graphic.FontManager.LoadFont("tahoma.ttf", 10),
                Text = "Drag and Place",
                Checked = IO.Options.DragAndPlace
            };
            this.chkDragAndPlace.CheckChanged += new EventHandler(this.ChkDragAndPlace_CheckChanged);

            this.pnlSettings.AddWidget(this.lblDisplaySettings);
            this.pnlSettings.AddWidget(this.chkDisplayMapGrid);
            this.pnlSettings.AddWidget(this.chkDisplayAttributes);
            this.pnlSettings.AddWidget(this.chkDisplayDungeonValues);
            this.pnlSettings.AddWidget(this.lblMappingSettings);
            this.pnlSettings.AddWidget(this.chkDragAndPlace);
            Screen.AddWidget(this.tilesetViewer);
            Screen.AddWidget(this.btnTerrain);
            Screen.AddWidget(this.btnAttributes);
            Screen.AddWidget(this.btnOK);

            // this.AddWidget(btnBack);
            Screen.AddWidget(this.pnlAttributes);
            Screen.AddWidget(this.pnlAttOptions);

            this.mapEditorMenu.AddWidget(this.btnMapping);
            Screen.AddWidget(this.pnlMapping);

            this.mapEditorMenu.AddWidget(this.btnLayers);
            Screen.AddWidget(this.pnlLayers);

            this.mapEditorMenu.AddWidget(this.btnTileset);
            Screen.AddWidget(this.pnlTileset);

            this.mapEditorMenu.AddWidget(this.btnSettings);
            Screen.AddWidget(this.pnlSettings);

            this.mapEditorMenu.Visible = false;
            Screen.AddWidget(this.mapEditorMenu);

            this.HTilesetSelect_ValueChanged(this.hTilesetSelect, new ValueChangedEventArgs(0, 0));
        }

        private void HRotationSelect_MouseUp(object sender, ValueChangedEventArgs e)
        {
            this.lblRotation.Text = $"Rotate Tile {this.hRotationSelect.Value * 90}°";
        }

        private void EnforceMapEditorLimits()
        {
            switch (this.limiter)
            {
                case Enums.MapEditorLimitTypes.Full:
                {
                        this.optBlocked.Show();
                        this.optWarp.Show();
                        this.optItem.Show();
                        this.optNpcAvoid.Show();
                        this.optKey.Show();
                        this.optKeyOpen.Show();
                        this.optHeal.Show();
                        this.optKill.Show();
                        this.optSound.Show();
                        this.optScripted.Show();
                        this.optNotice.Show();
                        this.optDoor.Show();
                        this.optSign.Show();
                        this.optSpriteChange.Show();
                        this.optShop.Show();
                        this.optArena.Show();
                        this.optBank.Show();
                        this.optGuildBlock.Show();
                        this.optSpriteBlock.Show();
                        this.optMobileBlock.Show();
                        this.optLevelBlock.Show();
                        this.optAssembly.Show();
                        this.optEvolution.Show();
                        this.optStory.Show();
                        this.optLinkShop.Show();
                        this.optMission.Show();
                        this.optScriptedSign.Show();
                        this.optAmbiguous.Show();
                        this.optSlippery.Show();
                        this.optSlow.Show();
                        this.optDropShop.Show();
                        this.btnMapProperties.Show();
                        this.btnHouseProperties.Hide();
                        this.lblDungeonTileValue.Show();
                        this.nudDungeonTileValue.Show();
                        Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayLocation = true;
                    }

                    break;
                case Enums.MapEditorLimitTypes.House:
                {
                        this.HideAllAttributes();
                        this.optBlocked.Show();
                        this.btnMapProperties.Hide();
                        this.btnHouseProperties.Show();
                        this.optMobileBlock.Show();
                    }

                    break;
            }
        }

        private void HideAllAttributes()
        {
            this.optBlocked.Hide();
            this.optWarp.Hide();
            this.optItem.Hide();
            this.optNpcAvoid.Hide();
            this.optKey.Hide();
            this.optKeyOpen.Hide();
            this.optHeal.Hide();
            this.optKill.Hide();
            this.optSound.Hide();
            this.optScripted.Hide();
            this.optNotice.Hide();
            this.optDoor.Hide();
            this.optSign.Hide();
            this.optSpriteChange.Hide();
            this.optShop.Hide();
            this.optArena.Hide();
            this.optBank.Hide();
            this.optGuildBlock.Hide();
            this.optSpriteBlock.Hide();
            this.optMobileBlock.Hide();
            this.optLevelBlock.Hide();
            this.optAssembly.Hide();
            this.optEvolution.Hide();
            this.optStory.Hide();
            this.optLinkShop.Hide();
            this.optMission.Hide();
            this.optScriptedSign.Hide();
            this.optSlippery.Hide();
            this.optSlow.Hide();
            this.optDropShop.Hide();
            this.optAmbiguous.Hide();
            this.lblDungeonTileValue.Hide();
            this.nudDungeonTileValue.Hide();
        }

        private void BtnEyeDropper_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.limiter == Enums.MapEditorLimitTypes.Full)
            {
                if (this.activeTool == MappingTool.EyeDropper)
                {
                    this.SetActiveTool(MappingTool.Editor);
                    this.btnEyeDropper.Selected = false;
                }
else
                {
                    this.SetActiveTool(MappingTool.EyeDropper);
                    this.btnEyeDropper.Selected = true;
                }
            }
        }

        public void SetActiveTool(MappingTool tool)
        {
            switch (tool)
            {
                case MappingTool.Editor:
                {
                        this.DisableActiveTool();
                    }

                    break;
                case MappingTool.EyeDropper:
                {
                        this.btnEyeDropper.Selected = true;
                    }

                    break;
            }

            this.activeTool = tool;
        }

        public void DisableActiveTool()
        {
            switch (this.activeTool)
            {
                case MappingTool.EyeDropper:
                {
                        if (this.btnEyeDropper.Selected)
                        {
                            this.btnEyeDropper.Selected = false;
                        }
                    }

                    break;
            }

            this.activeTool = MappingTool.Editor;
        }

        private void BtnExit_Click(object sender, MouseButtonEventArgs e)
        {
            Editors.EditorManager.CloseMapEditor();
            this.HideAutoHiddenPanels();
            this.btnAttributes.Selected = false;
            Messenger.SendRefresh();
        }

        private void BtnMapProperties_Click(object sender, MouseButtonEventArgs e)
        {
            if (Editors.MapEditor.WinProperties.Instance == null)
            {
                System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(RunMapProps));
                t.SetApartmentState(System.Threading.ApartmentState.STA); // Set the thread to STA
                t.Start();
            }
        }

        private static void RunMapProps()
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.Run(new Editors.MapEditor.WinProperties());
        }

        private void BtnHouseProperties_Click(object sender, MouseButtonEventArgs e)
        {
            if (WindowManager.Windows.Contains("winHouseProperties") == false)
            {
                WindowManager.AddWindow(new Editors.MapEditor.WinHouseProperties());
            }
else
            {
                WindowManager.BringWindowToFront(WindowManager.FindWindow("winHouseProperties"));
            }
        }

        private void BtnTakeScreenshot_Click(object sender, MouseButtonEventArgs e)
        {
            if (WindowManager.Windows.Contains("winScreenshotOptions") == false)
            {
                WindowManager.AddWindow(new Editors.MapEditor.WinScreenshotOptions());
            }
else
            {
                WindowManager.BringWindowToFront(WindowManager.FindWindow("winScreenshotOptions"));
            }
        }

        private void BtnSaveMap_Click(object sender, MouseButtonEventArgs e)
        {
            Messenger.SendSaveMap(Maps.MapHelper.ActiveMap);
            this.btnAttributes.Selected = false;
            this.optBlocked.Checked = true;
        }

        private void BtnLoadMap_Click(object sender, MouseButtonEventArgs e)
        {
            // TODO: Load Map [Map Editor]
            // System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            // ofd.Filter = "PMU Map|*.pmumap";
            // ofd.Multiselect = false;
            // if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
            //    Maps.Map map = new Maps.Map();
            //    map.Init();
            //    map.LoadMap(ofd.FileName);
            //    mapViewer.ActiveMap = map;
            // }
        }

        private void BtnAttributes_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnAttributes.Selected)
            {
                this.btnAttributes.Selected = true;
                this.btnTerrain.Selected = false;
                this.tilesetViewer.Visible = false;
                this.pnlAttributes.Visible = true;
            }
        }

        private void BtnTerrain_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnTerrain.Selected)
            {
                this.btnAttributes.Selected = false;
                this.btnTerrain.Selected = true;
                this.tilesetViewer.Visible = true;
                this.pnlAttributes.Visible = false;
            }
        }

        private void BtnClear_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.mapMemento == null)
            {
                this.mapMemento = new List<Maps.Map>();
            }

            if (this.mementoIndex != this.mapMemento.Count - 1)
            {
                for (int i = this.mementoIndex + 1; i < this.mapMemento.Count; i++)
                {
                    this.mapMemento.RemoveAt(i);
                }

                this.mementoIndex = this.mapMemento.Count - 1;
                this.mementoIndex = (this.mementoIndex < 0) ? 0 : this.mementoIndex;
            }

            if (this.btnTerrain.Selected)
            {
                this.mapMemento.Add(this.mapViewer.ActiveMap);

                this.FillLayer(this.GetActiveLayer(), 0, 0, 0, false);
                if (this.InLiveMode)
                {
                    Messenger.SendLayerFillData(this.GetActiveLayer(), 0, 0, 0, false);
                }
            }
            else if (this.btnAttributes.Selected)
            {
                this.mapMemento.Add(this.mapViewer.ActiveMap);

                this.FillAttributes(Enums.TileType.Walkable, 0, 0, 0, string.Empty, string.Empty, string.Empty, 0);
                if (this.InLiveMode)
                {
                    Messenger.SendAttributeFillData(Enums.TileType.Walkable, 0, 0, 0, string.Empty, string.Empty, string.Empty, 0);
                }
            }

            this.mementoIndex = this.mapMemento.Count - 1;
        }

        private void BtnFill_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.mapMemento == null)
            {
                this.mapMemento = new List<Maps.Map>();
            }

            if (this.mementoIndex != this.mapMemento.Count - 1)
            {
                for (int i = this.mementoIndex + 1; i < this.mapMemento.Count; i++)
                {
                    this.mapMemento.RemoveAt(i);
                }

                this.mementoIndex = this.mapMemento.Count - 1;
                this.mementoIndex = (this.mementoIndex < 0) ? 0 : this.mementoIndex;
            }

            if (this.btnTerrain.Selected)
            {
                this.mapMemento.Add(this.mapViewer.ActiveMap);

                this.FillLayer(this.GetActiveLayer(), this.tilesetViewer.ActiveTilesetSurface.TilesetNumber, (this.tilesetViewer.SelectedTile.Y * (this.tilesetViewer.ActiveTilesetSurface.Size.Width / Constants.TILEWIDTH)) + this.tilesetViewer.SelectedTile.X, this.hRotationSelect.Value * 90, this.chkFlip.Checked);
                if (this.InLiveMode)
                {
                    Messenger.SendLayerFillData(this.GetActiveLayer(), this.tilesetViewer.ActiveTilesetSurface.TilesetNumber, (this.tilesetViewer.SelectedTile.Y * (this.tilesetViewer.ActiveTilesetSurface.Size.Width / Constants.TILEWIDTH)) + this.tilesetViewer.SelectedTile.X, this.hRotationSelect.Value * 90, this.chkFlip.Checked);
                }
            }
            else if (this.btnAttributes.Selected)
            {
                this.mapMemento.Add(this.mapViewer.ActiveMap);

                this.FillAttributesFromSettings(true);
            }

            this.mementoIndex = this.mapMemento.Count - 1;
        }

        private void ChkDisplayMapGrid_CheckChanged(object sender, EventArgs e)
        {
            IO.Options.MapGrid = this.chkDisplayMapGrid.Checked;
            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayMapGrid = this.chkDisplayMapGrid.Checked;
        }

        private void ChkDisplayAttributes_CheckChanged(object sender, EventArgs e)
        {
            IO.Options.DisplayAttributes = this.chkDisplayAttributes.Checked;
            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayAttributes = this.chkDisplayAttributes.Checked;
        }

        private void ChkDisplayDungeonValues_CheckChanged(object sender, EventArgs e)
        {
            IO.Options.DisplayDungeonValues = this.chkDisplayDungeonValues.Checked;
            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.DisplayDungeonValues = this.chkDisplayDungeonValues.Checked;
        }

        private void ChkDragAndPlace_CheckChanged(object sender, EventArgs e)
        {
            IO.Options.DragAndPlace = this.chkDragAndPlace.Checked;
        }

        private void BtnLoadTileset_Click(object sender, MouseButtonEventArgs e)
        {
            this.HideAutoHiddenPanels();
            this.tilesetViewer.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[this.hTilesetSelect.Value];
        }

        private void HTilesetSelect_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.lblSelectedTileset.Text != "Tileset " + e.NewValue.ToString())
            {
                this.lblSelectedTileset.Text = "Tileset " + e.NewValue.ToString();
                for (int i = 0; i < this.picTilesetPreview.Length; i++)
                {
                    if (this.picTilesetPreview[i].Image != null)
                    {
                        this.picTilesetPreview[i].Image.Dispose();
                        this.picTilesetPreview[i].Image = null;
                    }

                    this.picTilesetPreview[i].Image = new Surface(Graphic.GraphicsManager.Tiles[e.NewValue][i + 1]);
                }
            }
        }

        private void BtnTileset_Click(object sender, MouseButtonEventArgs e)
        {
            this.ShowTilesetPanel();
        }

        private void BtnLayers_Click(object sender, MouseButtonEventArgs e)
        {
            this.ShowLayersPanel();
        }

        private void BtnMapping_Click(object sender, MouseButtonEventArgs e)
        {
            this.ShowMappingPanel();
        }

        private void BtnMapping_MouseHover(object sender, EventArgs e)
        {
            this.ShowMappingPanel();
        }

        private void BtnLayers_MouseHover(object sender, EventArgs e)
        {
            this.ShowLayersPanel();
        }

        private void BtnTileset_MouseHover(object sender, EventArgs e)
        {
            this.ShowTilesetPanel();
        }

        private void BtnSettings_MouseHover(object sender, EventArgs e)
        {
            this.ShowOptionsPanel();
        }

        private void BtnSettings_Click(object sender, MouseButtonEventArgs e)
        {
            this.ShowOptionsPanel();
        }

        private void ShowMappingPanel()
        {
            if (this.pnlMapping.Visible == false)
            {
                this.HideAutoHiddenPanels();
                this.pnlMapping.ShowHidden();
            }
        }

        private void ShowLayersPanel()
        {
            if (this.pnlLayers.Visible == false)
            {
                this.HideAutoHiddenPanels();
                this.pnlLayers.ShowHidden();
            }
        }

        private void ShowTilesetPanel()
        {
            if (this.pnlTileset.Visible == false)
            {
                this.HideAutoHiddenPanels();
                this.pnlTileset.ShowHidden();
            }
        }

        private void ShowOptionsPanel()
        {
            if (this.pnlSettings.Visible == false)
            {
                this.HideAutoHiddenPanels();
                this.pnlSettings.ShowHidden();
            }
        }

        private void HideAutoHiddenPanels()
        {
            this.pnlMapping.HideHidden();
            this.pnlLayers.HideHidden();
            this.pnlTileset.HideHidden();
            this.pnlSettings.HideHidden();
        }

        private void OptWarp_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("warp");
            }
        }

        private void OptItem_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("item");
            }
        }

        private void OptKey_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("key");
            }
        }

        private void OptKeyOpen_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("keyopen");
            }
        }

        private void OptSound_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("sound");
            }
        }

        private void OptScripted_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("scripted");
            }
        }

        private void OptNotice_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("notice");
            }
        }

        private void OptSign_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("sign");
            }
        }

        private void OptSpriteChange_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("sprite");
            }
        }

        private void OptShop_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("shop");
            }
        }

        private void OptArena_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("arena");
            }
        }

        private void OptBank_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("bank");
            }
        }

        private void OptGuildBlock_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("guildblock");
            }
        }

        private void OptSpriteBlock_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("spriteblock");
            }
        }

        private void OptMobileBlock_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("mobileblock");
            }
        }

        private void OptLevelBlock_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("levelblock");
            }
        }

        private void OptStory_CheckChanged(object sender, EventArgs e)
        {// TODO
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("story");
            }
        }

        private void OptLinkShop_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("linkshop");
            }
        }

        private void OptScriptedSign_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("scriptedsign");
            }
        }

        private void OptSlippery_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("slippery");
            }
        }

        private void OptSlow_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("slow");
            }
        }

        private void OptDropShop_CheckChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                this.OptionPanel("dropshop");
            }
        }

        private void OptionPanel(string option)
        {
            switch (option.ToLower())
            {
                case "warp":
                {
                        this.Lbl1.Text = "Maps:";
                        this.nudStoryLevel.Location = new Point(9, 60);
                        if (this.hsb1.Value > 50)
                        {
                            this.hsb1.Value = 50;
                        }

                        if (this.hsb2.Value > 50)
                        {
                            this.hsb2.Value = 50;
                        }

                        this.lbl2.Text = "X: " + this.hsb1.Value;
                        this.lbl2.Location = new Point(0, 78);
                        this.lbl3.Text = "Y: " + this.hsb2.Value;
                        this.lbl3.Location = new Point(0, 119);
                        this.hsb1.Location = new Point(9, 99);
                        this.nudStoryLevel.Minimum = 1;
                        this.nudStoryLevel.Maximum = 2000;
                        this.hsb1.Maximum = 50;
                        this.hsb2.Maximum = 50;
                        this.hsb2.Location = new Point(9, 141);
                        this.lbl2.Visible = true;
                        this.lbl3.Visible = true;
                        this.nudStoryLevel.Visible = true;
                        this.hsb1.Visible = true;
                        this.hsb2.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Warp Map Attribute";
                    }

                    break;
                case "item":
                {
                        if (this.hsb1.Value == 0)
                        {
                            this.Lbl1.Text = "Item 0: None";
                        }
                        else if (this.hsb1.Value > MaxInfo.MaxItems)
                        {
                            this.hsb1.Value = MaxInfo.MaxItems;
                        }
else
                        {
                            this.Lbl1.Text = "Item " + this.hsb1.Value + ": " + Items.ItemHelper.Items[this.hsb1.Value].Name;
                        }

                        this.lbl2.Text = "Value: " + this.hsb2.Value;
                        this.lbl2.Location = new Point(0, 78);
                        this.lbl2.Visible = true;
                        this.hsb1.Maximum = MaxInfo.MaxItems;
                        this.hsb2.Maximum = 32759;
                        this.hsb1.Location = new Point(9, 55);
                        this.hsb2.Location = new Point(9, 105);
                        this.chkTake.Location = new Point(9, 130);
                        this.chkTake.Text = "Sticky";
                        this.chkTake.Visible = true;
                        this.chkHidden.Location = new Point(9, 155);
                        this.chkHidden.Text = "Hidden";
                        this.chkHidden.Visible = true;
                        this.txt1.Location = new Point(9, 175);
                        this.txt1.Visible = true;
                        this.hsb1.Visible = true;
                        this.hsb2.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Item Attribute";
                    }

                    break;
                case "key":
                {
                        if (this.hsb1.Value == 0)
                        {
                            this.Lbl1.Text = "Item 0: None";
                        }
                        else if (this.hsb1.Value > MaxInfo.MaxItems)
                        {
                            this.hsb1.Value = MaxInfo.MaxItems;
                        }
else
                        {
                            this.Lbl1.Text = "Item " + this.hsb1.Value + ": " + Items.ItemHelper.Items[this.hsb1.Value].Name;
                        }

                        this.hsb1.Maximum = MaxInfo.MaxItems;
                        this.hsb1.Location = new Point(9, 55);
                        this.chkTake.Location = new Point(9, 80);
                        this.chkTake.Text = "Take away key upon use";
                        this.hsb1.Visible = true;
                        this.chkTake.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Key Attribute";
                    }

                    break;
                case "keyopen":
                {
                        if (this.hsb1.Value > 50)
                        {
                            this.hsb1.Value = 50;
                        }

                        if (this.hsb2.Value > 50)
                        {
                            this.hsb2.Value = 50;
                        }

                        this.Lbl1.Text = "X: " + this.hsb1.Value;
                        this.lbl2.Text = "Y: " + this.hsb2.Value;
                        this.lbl3.Text = "Key Message (Leave blank for default)";
                        this.hsb1.Maximum = 50;
                        this.hsb2.Maximum = 50;
                        this.hsb1.Location = new Point(9, 55);
                        this.hsb2.Location = new Point(9, 105);
                        this.txt1.Location = new Point(9, 143);
                        this.lbl2.Location = new Point(0, 78);
                        this.lbl3.Location = new Point(0, 119);
                        this.lbl2.Visible = true;
                        this.lbl3.Visible = true;
                        this.txt1.Visible = true;
                        this.hsb1.Visible = true;
                        this.hsb2.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Key Open Attribute";
                    }

                    break;
                case "sound":
                {
                        this.lstSound.Visible = true;
                        if (this.lstSound.SelectedItems.Count == 0)
                        {
                            this.lstSound.SelectItem(0);
                        }

                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Sound Attribute";
                    }

                    break;
                case "scripted":
                {
                        if (this.hsb1.Value > 100)
                        {
                            this.hsb1.Value = 100;
                        }

                        this.Lbl1.Text = "Script " + this.hsb1.Value + ": (Needs work)";
                        Messenger.SendScriptedTileInfoRequest(this.hsb1.Value);
                        this.lbl2.Text = "Param 1:";
                        this.lbl3.Text = "Param 2:";
                        this.lbl4.Text = "Param 3:";
                        this.hsb1.Maximum = 100;
                        this.txt1.Location = new Point(9, 105);
                        this.txt2.Location = new Point(9, 143);
                        this.txt3.Location = new Point(9, 181);
                        this.lbl2.Location = new Point(0, 78);
                        this.lbl3.Location = new Point(0, 119);
                        this.lbl4.Location = new Point(0, 160);
                        this.hsb1.Location = new Point(9, 55);
                        this.lbl2.Visible = true;
                        this.lbl3.Visible = true;
                        this.lbl4.Visible = true;
                        this.hsb1.Visible = true;
                        this.txt1.Visible = true;
                        this.txt2.Visible = true;
                        this.txt3.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Scripted Tile Attribute";
                    }

                    break;
                case "notice":
                {
                        this.Lbl1.Text = "Sound:";
                        this.txt1.Location = new Point(9, 220);
                        this.lbl2.Text = "Title:";
                        this.lbl2.Location = new Point(0, 200);
                        this.txt2.Location = new Point(9, 265);
                        this.lbl3.Text = "Text:";
                        this.lbl3.Location = new Point(0, 245);
                        this.lbl2.Visible = true;
                        this.lbl3.Visible = true;
                        this.txt1.Visible = true;
                        this.txt2.Visible = true;
                        this.lstSound.Visible = true;
                        if (this.lstSound.SelectedItems.Count == 0)
                        {
                            this.lstSound.SelectItem(0);
                        }

                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Notice Attribute";
                    }

                    break;
                case "linkshop":
                {
                        if (this.hsb1.Value == 0)
                        {
                            this.Lbl1.Text = "Item Paid: 0 (None)";
                        }
                        else if (this.hsb1.Value > MaxInfo.MaxItems)
                        {
                            this.hsb1.Value = MaxInfo.MaxItems;
                        }
else
                        {
                            this.Lbl1.Text = "Item Paid: " + this.hsb1.Value + " (" + Items.ItemHelper.Items[this.hsb1.Value].Name + ")";
                        }

                        this.lbl2.Text = "Value: " + this.hsb2.Value;
                        this.lbl2.Location = new Point(0, 78);
                        this.lbl2.Visible = true;
                        this.hsb1.Maximum = MaxInfo.MaxItems;
                        this.hsb2.Maximum = 32759;
                        this.hsb1.Location = new Point(9, 55);
                        this.hsb2.Location = new Point(9, 105);
                        this.chkTake.Location = new Point(9, 130);
                        this.hsb1.Visible = true;
                        this.hsb2.Visible = true;
                        this.chkTake.Text = "Pre-Evo Moves";
                        this.hsb1.Visible = true;
                        this.chkTake.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Link Shop Attribute";
                    }

                    break;
                case "sign":
                {
                        this.Lbl1.Text = "Line 1:";
                        this.lbl2.Text = "Line 2:";
                        this.lbl3.Text = "Line 3:";
                        this.lbl2.Location = new Point(0, 85);
                        this.lbl3.Location = new Point(0, 135);
                        this.txt1.Location = new Point(9, 55);
                        this.txt2.Location = new Point(9, 105);
                        this.txt3.Location = new Point(9, 155);
                        this.lbl2.Visible = true;
                        this.lbl3.Visible = true;
                        this.txt1.Visible = true;
                        this.txt2.Visible = true;
                        this.txt3.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Sign Attribute";
                    }

                    break;
                case "sprite":
                {
                        this.Lbl1.Text = "Sprite: " + this.hsb1.Value;
                        if (this.hsb1.Value > 1000)
                        {
                            this.hsb1.Value = 1000;
                        }

                        if (this.hsb2.Value > MaxInfo.MaxItems || this.hsb2.Value == 0)
                        {
                            this.lbl2.Text = "Item 0: None";
                        }
else
                        {
                            this.lbl2.Text = "Item " + this.hsb2.Value + ": " + Items.ItemHelper.Items[this.hsb2.Value].Name;
                        }

                        this.hsb1.Maximum = 721;
                        this.hsb1.Location = new Point(0, 55);
                        this.hsb2.Maximum = MaxInfo.MaxItems;
                        this.hsb2.Minimum = 0;
                        this.hsb2.Location = new Point(0, 105);
                        this.hsb3.Maximum = 30000;
                        this.hsb3.Location = new Point(0, 155);
                        this.lbl2.Location = new Point(0, 85);
                        this.lbl3.Text = "Value: " + this.hsb3.Value;
                        this.lbl3.Location = new Point(0, 135);
                        this.lbl2.Visible = true;
                        this.lbl3.Visible = true;
                        this.hsb1.Visible = true;
                        this.hsb2.Visible = true;
                        this.hsb3.Visible = true;
                        this.picSprite.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Sprite Change Attribute";
                    }

                    break;
                case "shop":
                {
                        if (this.hsb1.Value > MaxInfo.MaxShops || this.hsb1.Value == 0)
                        {
                            this.Lbl1.Text = "Shop num 0- None";
                        }
else
                        {
                            this.Lbl1.Text = "Shop num " + this.hsb1.Value + "- " + Shops.ShopHelper.Shops[this.hsb1.Value].Name;
                        }

                        this.hsb1.Maximum = MaxInfo.MaxShops;
                        this.hsb1.Location = new Point(9, 55);
                        this.hsb1.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Shop Attribute";
                    }

                    break;
                case "arena":
                {
                        this.Lbl1.Text = "Maps: " + this.hsb3.Value;
                        this.hsb1.Location = new Point(9, 99);
                        if (this.hsb1.Value > 50)
                        {
                            this.hsb1.Value = 50;
                        }

                        if (this.hsb2.Value > 50)
                        {
                            this.hsb2.Value = 50;
                        }

                        if (this.hsb3.Value > 2000)
                        {
                            this.hsb3.Value = 2000;
                        }

                        this.lbl2.Text = "X: " + this.hsb1.Value;
                        this.lbl2.Location = new Point(0, 78);
                        this.lbl3.Text = "Y: " + this.hsb2.Value;
                        this.lbl3.Location = new Point(0, 119);
                        this.hsb2.Location = new Point(9, 141);
                        this.nudStoryLevel.Minimum = 1;
                        this.nudStoryLevel.Maximum = 2000;
                        this.hsb1.Maximum = 50;
                        this.hsb2.Maximum = 50;
                        this.hsb3.Maximum = 2000;
                        this.hsb3.Location = new Point(9, 60);
                        this.lbl2.Visible = true;
                        this.lbl3.Visible = true;
                        this.hsb3.Visible = true;
                        this.hsb1.Visible = true;
                        this.hsb2.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Arena Attribute";
                    }

                    break;
                case "guildblock":
                {
                        this.Lbl1.Text = "Guild Name to Pass Block:";
                        this.txt1.Location = new Point(9, 55);
                        this.txt1.Visible = true;
                        this.txt1.Size = new Size(230, 18);
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Guild Block Attribute";
                    }

                    break;
                case "spriteblock":
                {
                        this.Lbl1.Text = "Allow Sprite: " + this.hsb1.Value;
                        this.lbl2.Text = "Allow Sprite: " + this.hsb2.Value;
                        this.hsb1.Maximum = 1000;
                        this.hsb1.Location = new Point(0, 55);
                        this.hsb2.Maximum = 1000;
                        this.hsb2.Minimum = 0;
                        this.hsb2.Location = new Point(0, 105);
                        this.lbl2.Location = new Point(0, 85);
                        this.lbl3.Text = "Sprite 1:";
                        this.lbl3.Location = new Point(138, 7);
                        this.lbl4.Text = "Sprite 2:";
                        this.lbl4.Location = new Point(0, 130);
                        this.lblMode.Visible = true;
                        this.optAllow.Checked = true;
                        this.optAllow.Visible = true;
                        this.optBlock.Visible = true;
                        this.lbl2.Visible = true;
                        this.lbl3.Visible = true;
                        this.lbl4.Visible = true;
                        this.hsb1.Visible = true;
                        this.hsb2.Visible = true;
                        this.picSprite.Visible = true;
                        this.picSprite2.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Sprite Block Attribute";
                    }

                    break;
                case "mobileblock":
                {
                        if (this.hsb1.Value > 31)
                        {
                            this.hsb1.Value = 31;
                        }

                        this.Lbl1.Text = "Mobility Flag #" + ": Loading..."; // Needs work
                        Messenger.SendMobilityInfoRequest(this.hsb1.Value);
                        this.hsb1.Maximum = 15;
                        this.hsb1.Location = new Point(9, 55);
                        this.hsb1.Visible = true;
                        this.chkHidden.Location = new Point(9, 85);
                        this.chkHidden.Text = "Blocked";
                        this.chkHidden.Visible = true;

                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Mobile Block Attribute";
                    }

                    break;
                case "levelblock":
                {
                        if (this.nudStoryLevel.Value > 100)
                        {
                            this.nudStoryLevel.Value = 100;
                        }

                        this.Lbl1.Text = "Select a level. All levels equal to or";
                        this.lbl2.Text = "under this level will be blocked.";
                        this.lbl2.Location = new Point(0, 53);
                        this.nudStoryLevel.Location = new Point(9, 71);
                        this.nudStoryLevel.Minimum = 1;
                        this.nudStoryLevel.Maximum = 100;
                        this.lbl2.Visible = true;
                        this.nudStoryLevel.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Level Block Attribute";
                    }

                    break;
                case "story":
                {
                        this.Lbl1.Text = "Chapter";
                        this.nudStoryLevel.Location = new Point(9, 55);
                        this.nudStoryLevel.Minimum = 1;
                        this.nudStoryLevel.Maximum = MaxInfo.MaxStories;
                        this.nudStoryLevel.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Story Attribute";
                    }

                    break;
                case "scriptedsign":
                {
                        if (this.hsb1.Value > 100)
                        {
                            this.hsb1.Value = 100;
                        }

                        this.Lbl1.Text = "Script " + this.hsb1.Value + ": (Needs work)"; // Needs work
                        Messenger.SendScriptedSignInfoRequest(this.hsb1.Value);
                        this.hsb1.Maximum = 100;
                        this.txt1.Location = new Point(9, 105);
                        this.txt2.Location = new Point(9, 143);
                        this.txt3.Location = new Point(9, 181);
                        this.lbl2.Location = new Point(0, 78);
                        this.lbl3.Location = new Point(0, 119);
                        this.lbl4.Location = new Point(0, 160);
                        this.hsb1.Location = new Point(9, 55);
                        this.lbl2.Text = "Param 1:";
                        this.lbl3.Text = "Param 2:";
                        this.lbl4.Text = "Param 3:";
                        this.lbl2.Visible = true;
                        this.lbl3.Visible = true;
                        this.lbl4.Visible = true;
                        this.hsb1.Visible = true;
                        this.txt1.Visible = true;
                        this.txt2.Visible = true;
                        this.txt3.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Scripted Sign Attribute";
                    }

                    break;
                case "slippery":
                {
                        if (this.hsb1.Value > 31)
                        {
                            this.hsb1.Value = 31;
                        }

                        this.Lbl1.Text = "Mobility Flag #" + ": Loading..."; // Needs work
                        Messenger.SendMobilityInfoRequest(this.hsb1.Value);
                        this.hsb1.Maximum = 15;
                        this.hsb1.Location = new Point(9, 55);
                        this.hsb1.Visible = true;
                        this.chkHidden.Location = new Point(9, 85);
                        this.chkHidden.Text = "Slip";
                        this.chkHidden.Visible = true;

                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Slippery Attribute";
                    }

                    break;
                case "slow":
                {
                        if (this.hsb1.Value > 15)
                        {
                            this.hsb1.Value = 15;
                        }

                        this.Lbl1.Text = "Mobility Flag #" + ": Loading..."; // Needs work
                        Messenger.SendMobilityInfoRequest(this.hsb1.Value);
                        this.hsb1.Maximum = 15;
                        this.hsb1.Location = new Point(9, 55);
                        this.hsb1.Visible = true;
                        this.chkHidden.Location = new Point(9, 85);
                        this.chkHidden.Text = "Slow";
                        this.chkHidden.Visible = true;

                        this.lbl2.Text = "Speed:";
                        this.lbl2.Location = new Point(0, 115);
                        this.lbl2.Visible = true;

                        if (this.hsb2.Value > 7)
                        {
                            this.hsb2.Value = 7;
                        }

                        this.hsb2.Maximum = 8;
                        this.hsb2.Location = new Point(9, 145);
                        this.hsb2.Visible = true;

                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Slow Attribute";
                    }

                    break;
                case "dropshop":
                {
                        if (this.hsb1.Value == 0)
                        {
                            this.Lbl1.Text = "Item 0: None";
                        }
                        else if (this.hsb1.Value > MaxInfo.MaxItems)
                        {
                            this.hsb1.Value = MaxInfo.MaxItems;
                        }
else
                        {
                            this.Lbl1.Text = "Item " + this.hsb1.Value + ": " + Items.ItemHelper.Items[this.hsb1.Value].Name;
                        }

                        this.lbl2.Text = "Value: " + this.hsb2.Value;
                        this.lbl2.Location = new Point(0, 78);
                        this.lbl2.Visible = true;
                        this.lbl3.Text = "Paid Value: " + this.hsb2.Value;
                        this.lbl3.Location = new Point(0, 210);
                        this.lbl3.Visible = true;
                        this.hsb1.Maximum = MaxInfo.MaxItems;
                        this.hsb2.Maximum = 32759;
                        this.hsb3.Maximum = 32759;
                        this.hsb1.Location = new Point(9, 55);
                        this.hsb2.Location = new Point(9, 105);
                        this.txt1.Location = new Point(9, 175);
                        this.hsb3.Location = new Point(9, 245);
                        this.txt1.Visible = true;
                        this.hsb1.Visible = true;
                        this.hsb2.Visible = true;
                        this.hsb3.Visible = true;
                        this.OpenOptionsPanel();
                        this.btnTitle.Text = "Drop Shop Attribute";
                    }

                    break;
            }
        }

        private void Hsb1_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.btnTitle.Text == "Sprite Change Attribute" || this.btnTitle.Text == "Sprite Block Attribute")
            {
                this.Lbl1.Text = "Sprite: " + this.hsb1.Value;
                this.picSprite.Size = new Size(32, 64);
                this.picSprite.BlitToBuffer(Graphic.GraphicsManager.GetSpriteSheet(this.hsb1.Value).GetSheet(Graphic.FrameType.Idle, Enums.Direction.Down), new Rectangle(96, 0, 32, 64));
            }
            else if (this.btnTitle.Text == "Warp Map Attribute" || this.btnTitle.Text == "Arena Attribute")
            {
                this.lbl2.Text = "X: " + this.hsb1.Value;
            }
            else if (this.btnTitle.Text == "Link Shop Attribute")
            {
                if (this.hsb1.Value == 0)
                {
                    this.Lbl1.Text = "Item Paid: 0 (None)";
                }
else
                {
                    this.Lbl1.Text = "Item Paid: " + this.hsb1.Value + " (" + Items.ItemHelper.Items[this.hsb1.Value].Name + ")";
                }
            }
            else if (this.btnTitle.Text == "Item Attribute" || this.btnTitle.Text == "Key Attribute" || this.btnTitle.Text == "Drop Shop Attribute")
            {
                if (this.hsb1.Value == 0)
                {
                    this.Lbl1.Text = "Item 0: None";
                }
else
                {
                    this.Lbl1.Text = "Item " + this.hsb1.Value + ": " + Items.ItemHelper.Items[this.hsb1.Value].Name;
                }
            }
            else if (this.btnTitle.Text == "Key Open Attribute")
            {
                this.Lbl1.Text = "X: " + this.hsb1.Value;
            }
            else if (this.btnTitle.Text == "Scripted Tile Attribute")
            {
                this.Lbl1.Text = "Script " + this.hsb1.Value + ": Loading..."; // something goes here.
                Messenger.SendScriptedTileInfoRequest(this.hsb1.Value);
            }
            else if (this.btnTitle.Text == "Scripted Sign Attribute")
            {
                this.Lbl1.Text = "Script " + this.hsb1.Value + ": Loading..."; // something goes here.
                Messenger.SendScriptedSignInfoRequest(this.hsb1.Value);
            }
            else if (this.btnTitle.Text == "Mobile Block Attribute" || this.btnTitle.Text == "Slippery Attribute" || this.btnTitle.Text == "Slow Attribute")
            {
                this.Lbl1.Text = "Mobility Flag #" + this.hsb1.Value + ": Loading..."; // something goes here.
                Messenger.SendMobilityInfoRequest(this.hsb1.Value);
                this.chkHidden.Checked = this.tempArrayForMobility[this.hsb1.Value];
            }
            else if (this.btnTitle.Text == "Shop Attribute")
            {
                if (this.hsb1.Value == 0)
                {
                    this.Lbl1.Text = "Shop num 0: None";
                }
else
                {
                    this.Lbl1.Text = "Shop num " + this.hsb1.Value + "- " + Shops.ShopHelper.Shops[this.hsb1.Value].Name;
                }
            }
        }

        private void Hsb2_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.btnTitle.Text == "Sprite Change Attribute")
            {
                if (this.hsb2.Value == 0)
                {
                    this.lbl2.Text = "Item 0: None";
                }
else
                {
                    this.lbl2.Text = "Item " + this.hsb2.Value + ": " + Items.ItemHelper.Items[this.hsb2.Value].Name;
                }
            }
            else if (this.btnTitle.Text == "Warp Map Attribute" || this.btnTitle.Text == "Arena Attribute")
            {
                this.lbl3.Text = "Y: " + this.hsb2.Value;
            }
            else if (this.btnTitle.Text == "Item Attribute" || this.btnTitle.Text == "Link Shop Attribute" || this.btnTitle.Text == "Drop Shop Attribute")
            {
                this.lbl2.Text = "Value: " + this.hsb2.Value;
            }
            else if (this.btnTitle.Text == "Key Open Attribute")
            {
                this.lbl2.Text = "Y: " + this.hsb2.Value;
            }
            else if (this.btnTitle.Text == "Sprite Block Attribute")
            {
                this.lbl2.Text = "Sprite: " + this.hsb2.Value;
                this.picSprite2.Size = new Size(32, 64);
                this.picSprite2.BlitToBuffer(Graphic.GraphicsManager.GetSpriteSheet(this.hsb2.Value).GetSheet(Graphic.FrameType.Idle, Enums.Direction.Down), new Rectangle(96, 0, 32, 64));
            }
            else if (this.btnTitle.Text == "Slow Attribute")
            {
                this.lbl2.Text = "Speed: " + (Enums.MovementSpeed)this.hsb2.Value;
            }
        }

        private void Hsb3_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.btnTitle.Text == "Sprite Change Attribute")
            {
                this.lbl3.Text = "Value: " + this.hsb3.Value;
            }
            else if (this.btnTitle.Text == "Arena Attribute")
            {
                this.Lbl1.Text = "Maps: " + this.hsb3.Value;
            }
            else if (this.btnTitle.Text == "Drop Shop Attribute")
            {
                this.lbl3.Text = "Given Value: " + this.hsb3.Value;
            }
        }

        private void OptAllow_CheckChanged(object sender, EventArgs e)
        {
            this.mode = 1;
        }

        private void OptBlock_CheckChanged(object sender, EventArgs e)
        {
            this.mode = 2;
        }

        private void ChkHidden_CheckChanged(object sender, EventArgs e)
        {
            if (this.btnTitle.Text == "Mobile Block Attribute" || this.btnTitle.Text == "Slippery Attribute" || this.btnTitle.Text == "Slow Attribute")
            {
                this.tempArrayForMobility[this.hsb1.Value] = this.chkHidden.Checked;
            }
        }

        private void OpenOptionsPanel()
        {
            this.pnlAttributes.Visible = false;
            this.pnlAttOptions.Visible = true;
            this.btnTerrain.Visible = false;
            this.btnAttributes.Visible = false;
            this.btnOK.Visible = true;

            // btnBack.Visible = true;
            this.btnTitle.Visible = true;
            this.btnMapping.Visible = false;
            this.btnLayers.Visible = false;
            this.btnTileset.Visible = false;
            this.btnSettings.Visible = false;
            this.txt1.Text = "1";
            this.txt2.Text = "1";
            this.txt3.Text = "1";
            if (this.optSound.Checked == false)
            {
                this.Lbl1.Visible = true;
            }
        }

        private void CloseOptionsPanel()
        {
            this.pnlAttOptions.Visible = false;
            this.pnlAttributes.Visible = true;
            this.btnTerrain.Visible = true;
            this.btnAttributes.Visible = true;
            this.btnOK.Visible = false;

            // btnBack.Visible = false;
            this.btnTitle.Text = string.Empty;
            this.btnTitle.Visible = false;
            this.btnMapping.Visible = true;
            this.btnLayers.Visible = true;
            this.btnTileset.Visible = true;
            this.btnSettings.Visible = true;
            this.Lbl1.Visible = false;
            this.txt1.Size = new Size(134, 18);
        }

        /*void btnBack_Click(object sender, MouseButtonEventArgs e){
    lbl1.Text = "";
    lbl1.Visible = false;
    lbl2.Text = "";
    lbl2.Visible = false;
    lbl3.Text = "";
    lbl3.Visible = false;
    lbl4.Text = "";
    lbl4.Visible = false;
    lblMode.Text = "";
    lblMode.Visible = false;
    txt1.Text = "";
    txt1.Visible = false;
    txt2.Text = "";
    txt2.Visible = false;
    txt3.Text = "";
    txt3.Visible = false;
    hsb1.Value = 0;
    hsb1.Visible = false;
    hsb2.Value = 0;
    hsb2.Visible = false;
    hsb3.Value = 0;
    hsb3.Visible = false;
    picSprite.Visible = false;
    picSprite2.Visible = false;
    lstSound.Visible = false;
    chkTake.Visible = false;
    optAllow.Visible = false;
    optBlock.Visible = false;
    nudStoryLevel.Value = 0;
    nudStoryLevel.Visible = false;
    CloseOptionsPanel();
    optBlocked.Checked = true;
}*/

        private void BtnOK_Click(object sender, MouseButtonEventArgs e)
        {
            this.Lbl1.Visible = false;
            this.lbl2.Visible = false;
            this.lbl3.Visible = false;
            this.lbl4.Visible = false;
            this.lblMode.Visible = false;
            this.txt1.Visible = false;
            this.txt2.Visible = false;
            this.txt3.Visible = false;
            this.hsb1.Visible = false;
            this.hsb2.Visible = false;
            this.hsb3.Visible = false;
            this.picSprite.Visible = false;
            this.picSprite2.Visible = false;
            this.lstSound.Visible = false;
            this.chkTake.Visible = false;
            this.chkHidden.Visible = false;
            this.optAllow.Visible = false;
            this.optBlock.Visible = false;
            this.nudStoryLevel.Visible = false;
            this.CloseOptionsPanel();
        }

        private Maps.Tile selectedTile;

        public bool InLiveMode { get => inLiveMode; set => inLiveMode = value; }

        private void Editor_mapViewer_Click(object sender, MouseButtonEventArgs e)
        {
            if ((Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Mapper) || Maps.MapHelper.ActiveMap.MapID.StartsWith("h-")) && (SdlDotNet.Input.Keyboard.IsKeyPressed(SdlDotNet.Input.Key.LeftShift) && e.MouseEventArgs.Button == SdlDotNet.Input.MouseButton.SecondaryButton))
            {
                int newX = (e.RelativePosition.X / Constants.TILEWIDTH) + Graphic.Renderers.Screen.ScreenRenderer.Camera.X;
                int newY = (e.RelativePosition.Y / Constants.TILEHEIGHT) + Graphic.Renderers.Screen.ScreenRenderer.Camera.Y;
                Messenger.WarpLoc(newX, newY);
                return;
            }

            if (this.InMapEditor)
            {
                switch (this.activeTool)
                {
                    case MappingTool.Editor:
                    {
                            if (this.mapMemento == null)
                            {
                                this.mapMemento = new List<Maps.Map>();
                            }

                            if (this.mementoIndex != this.mapMemento.Count - 1)
                            {
                                for (int i = this.mementoIndex + 1; i < this.mapMemento.Count; i++)
                                {
                                    this.mapMemento.RemoveAt(i);
                                }

                                this.mementoIndex = this.mapMemento.Count - 1;
                                this.mementoIndex = (this.mementoIndex < 0) ? 0 : this.mementoIndex;
                            }

                            // Point location = mapViewer.ScreenLocation;
                            // Point relPoint = new Point(e.Position.X - location.X, e.Position.Y - location.Y);
                            if (this.btnTerrain.Selected)
                            {
                                this.mapMemento.Add(this.mapViewer.ActiveMap);
                                this.PlaceLayer(e.RelativePosition, e.MouseEventArgs.Button);
                            }
                            else if (this.btnAttributes.Selected)
                            {
                                this.mapMemento.Add(this.mapViewer.ActiveMap);
                                this.PlaceAttribute(e.RelativePosition, e.MouseEventArgs.Button);
                            }

                            this.mementoIndex = this.mapMemento.Count - 1;
                        }

                        break;
                    case MappingTool.EyeDropper:
                    {
                            Point location = this.mapViewer.ScreenLocation;
                            Point relPoint = new Point(e.Position.X - location.X, e.Position.Y - location.Y);
                            int X = (relPoint.X / Constants.TILEWIDTH) + Graphic.Renderers.Screen.ScreenRenderer.Camera.X;
                            int Y = (relPoint.Y / Constants.TILEHEIGHT) + Graphic.Renderers.Screen.ScreenRenderer.Camera.Y;
                            if (X <= this.mapViewer.ActiveMap.MaxX && Y <= this.mapViewer.ActiveMap.MaxY && X >= 0 && Y >= 0)
                            {
                                if (this.btnAttributes.Selected)
                                {
                                    Maps.Tile tile = this.mapViewer.ActiveMap.Tile[X, Y];
                                    switch (tile.Type)
                                    {
                                        case Enums.TileType.Blocked:
                                        {
                                                this.optBlocked.Checked = true;
                                            }

                                            break;
                                        case Enums.TileType.Warp:
                                        {
                                                this.optWarp.Checked = true;
                                                this.nudStoryLevel.Value = tile.Data1;
                                                this.hsb1.Value = tile.Data2;
                                                this.hsb2.Value = tile.Data3;
                                            }

                                            break;
                                        case Enums.TileType.Item:
                                        {
                                                this.optItem.Checked = true;
                                                this.hsb1.Value = tile.Data1;
                                                this.hsb2.Value = tile.Data2;
                                                this.chkTake.Checked = tile.Data3.ToString().ToBool();
                                                this.chkHidden.Checked = tile.String1.ToBool();
                                                this.txt1.Text = tile.String2;
                                            }

                                            break;
                                        case Enums.TileType.NPCAvoid:
                                        {
                                                this.optNpcAvoid.Checked = true;
                                            }

                                            break;
                                        case Enums.TileType.Key:
                                        {
                                                this.optKey.Checked = true;
                                                this.hsb1.Value = tile.Data1;
                                                this.chkTake.Checked = tile.Data2.ToString().ToBool();
                                            }

                                            break;
                                        case Enums.TileType.KeyOpen:
                                        {
                                                this.optKeyOpen.Checked = true;
                                                this.hsb1.Value = tile.Data1;
                                                this.hsb2.Value = tile.Data2;
                                                this.txt1.Text = tile.String1;
                                            }

                                            break;
                                        case Enums.TileType.Heal:
                                        {
                                                this.optHeal.Checked = true;
                                            }

                                            break;
                                        case Enums.TileType.Kill:
                                        {
                                                this.optKill.Checked = true;
                                            }

                                            break;
                                        case Enums.TileType.Sound:
                                        {
                                                this.optSound.Checked = true;
                                                this.lstSound.SelectItem(tile.String1);
                                            }

                                            break;
                                        case Enums.TileType.Scripted:
                                        {
                                                this.optScripted.Checked = true;
                                                this.hsb1.Value = tile.Data1;
                                                this.txt1.Text = tile.String1;
                                                this.txt2.Text = tile.String2;
                                                this.txt3.Text = tile.String3;
                                            }

                                            break;
                                        case Enums.TileType.Notice:
                                        {
                                                this.optNotice.Checked = true;
                                                this.txt1.Text = tile.String1;
                                                this.txt2.Text = tile.String2;
                                                this.lstSound.SelectItem(tile.String3);
                                            }

                                            break;
                                        case Enums.TileType.LinkShop:
                                        {
                                                this.optLinkShop.Checked = true;
                                                this.hsb1.Value = tile.Data1;
                                                this.hsb2.Value = tile.Data2;
                                                this.chkTake.Checked = tile.Data3.ToString().ToBool();
                                            }

                                            break;
                                        case Enums.TileType.Door:
                                        {
                                                this.optDoor.Checked = true;
                                            }

                                            break;
                                        case Enums.TileType.Sign:
                                        {
                                                this.optSign.Checked = true;
                                                this.txt1.Text = tile.String1;
                                                this.txt2.Text = tile.String2;
                                                this.txt3.Text = tile.String3;
                                            }

                                            break;
                                        case Enums.TileType.SpriteChange:
                                        {
                                                this.optSpriteChange.Checked = true;
                                                this.hsb1.Value = tile.Data1;
                                                this.hsb2.Value = tile.Data2;
                                                this.hsb3.Value = tile.Data3;
                                            }

                                            break;
                                        case Enums.TileType.Shop:
                                        {
                                                this.optShop.Checked = true;
                                                this.hsb1.Value = tile.Data1;
                                            }

                                            break;
                                        case Enums.TileType.Arena:
                                        {
                                                this.optArena.Checked = true;
                                                this.hsb1.Value = tile.Data1;
                                                this.hsb2.Value = tile.Data2;
                                                this.hsb3.Value = tile.Data3;
                                            }

                                            break;
                                        case Enums.TileType.Bank:
                                        {
                                                this.optBank.Checked = true;
                                            }

                                            break;
                                        case Enums.TileType.LevelBlock:
                                        {
                                                this.optLevelBlock.Checked = true;
                                                this.nudStoryLevel.Value = tile.Data1;
                                            }

                                            break;
                                        case Enums.TileType.SpriteBlock:
                                        {
                                                this.optSpriteBlock.Checked = true;
                                                this.mode = tile.Data1;
                                                this.hsb1.Value = tile.Data2;
                                                this.hsb3.Value = tile.Data3;
                                            }

                                            break;
                                        case Enums.TileType.MobileBlock:
                                        {
                                                this.optMobileBlock.Checked = true;
                                                int mobility = tile.Data1;
                                                for (int i = 0; i < 16; i++)
                                                {
                                                    this.tempArrayForMobility[i] = (mobility % 2).ToString().ToBool();
                                                    mobility /= 2;
                                                }

                                                this.hsb1.Value = 0;
                                                this.chkHidden.Checked = this.tempArrayForMobility[0];

                                                // hsb3.Value = tile.Data3;
                                            }

                                            break;
                                        case Enums.TileType.Guild:
                                        {
                                                this.optGuildBlock.Checked = true;
                                            }

                                            break;
                                        case Enums.TileType.Assembly:
                                        {
                                                this.optAssembly.Checked = true;
                                            }

                                            break;
                                        case Enums.TileType.Evolution:
                                        {
                                                this.optEvolution.Checked = true;
                                            }

                                            break;
                                        case Enums.TileType.Story:
                                        {
                                                this.optStory.Checked = true;
                                            }

                                            break;
                                        case Enums.TileType.MissionBoard:
                                        {
                                                this.optMission.Checked = true;
                                            }

                                            break;
                                        case Enums.TileType.ScriptedSign:
                                        {
                                                this.optScriptedSign.Checked = true;
                                                this.hsb1.Value = tile.Data1;
                                                this.txt1.Text = tile.String1;
                                                this.txt2.Text = tile.String2;
                                                this.txt3.Text = tile.String3;
                                            }

                                            break;
                                        case Enums.TileType.Ambiguous:
                                        {
                                                this.optAmbiguous.Checked = true;
                                            }

                                            break;
                                        case Enums.TileType.Slippery:
                                        {
                                                this.optSlippery.Checked = true;
                                                int mobility = tile.Data1;
                                                for (int i = 0; i < 16; i++)
                                                {
                                                    this.tempArrayForMobility[i] = (mobility % 2).ToString().ToBool();
                                                    mobility /= 2;
                                                }

                                                this.hsb1.Value = 0;
                                                this.chkHidden.Checked = this.tempArrayForMobility[0];
                                            }

                                            break;
                                        case Enums.TileType.Slow:
                                        {
                                                this.optSlow.Checked = true;
                                                int mobility = tile.Data1;
                                                for (int i = 0; i < 16; i++)
                                                {
                                                    this.tempArrayForMobility[i] = (mobility % 2).ToString().ToBool();
                                                    mobility /= 2;
                                                }

                                                this.hsb1.Value = 0;
                                                this.chkHidden.Checked = this.tempArrayForMobility[0];
                                                this.hsb2.Value = tile.Data2;
                                            }

                                            break;
                                        case Enums.TileType.DropShop:
                                        {
                                                this.optDropShop.Checked = true;
                                                this.hsb1.Value = tile.Data2;
                                                this.hsb2.Value = tile.Data3;
                                                this.txt1.Text = tile.String2;
                                                this.hsb3.Value = tile.Data1;
                                            }

                                            break;
                                    }

                                    this.nudDungeonTileValue.Value = tile.RDungeonMapValue;
                                }
                                else
                                {
                                    int x = X;
                                    int y = Y;

                                    // Apparently the only way to get the tileset of the selected tile
                                    switch (this.GetActiveLayer())
                                    {
                                        case Enums.LayerType.Ground:
                                            this.tilesetViewer.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[this.mapViewer.ActiveMap.Tile[x, y].GroundSet];
                                            break;
                                        case Enums.LayerType.GroundAnim:
                                            this.tilesetViewer.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[this.mapViewer.ActiveMap.Tile[x, y].GroundAnimSet];
                                            break;
                                        case Enums.LayerType.Mask:
                                            this.tilesetViewer.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[this.mapViewer.ActiveMap.Tile[x, y].MaskSet];
                                            break;
                                        case Enums.LayerType.MaskAnim:
                                            this.tilesetViewer.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[this.mapViewer.ActiveMap.Tile[x, y].AnimSet];
                                            break;
                                        case Enums.LayerType.Mask2:
                                            this.tilesetViewer.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[this.mapViewer.ActiveMap.Tile[x, y].Mask2Set];
                                            break;
                                        case Enums.LayerType.Mask2Anim:
                                            this.tilesetViewer.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[this.mapViewer.ActiveMap.Tile[x, y].M2AnimSet];
                                            break;
                                        case Enums.LayerType.Fringe:
                                            this.tilesetViewer.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[this.mapViewer.ActiveMap.Tile[x, y].FringeSet];
                                            break;
                                        case Enums.LayerType.FringeAnim:
                                            this.tilesetViewer.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[this.mapViewer.ActiveMap.Tile[x, y].FAnimSet];
                                            break;
                                        case Enums.LayerType.Fringe2:
                                            this.tilesetViewer.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[this.mapViewer.ActiveMap.Tile[x, y].Fringe2Set];
                                            break;
                                        case Enums.LayerType.Fringe2Anim:
                                            this.tilesetViewer.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[this.mapViewer.ActiveMap.Tile[x, y].F2AnimSet];
                                            break;
                                        default:
                                            this.tilesetViewer.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[0];
                                            break;
                                    }

                                    // Formula to calcuate the point of the tile in the tileset from the linear integer stored by tiles
                                    int tileX;
                                    int tileY;
                                    {
                                        int linearTileNum = this.GetActiveLayerTile(X, Y);

                                        tileY = (int)Math.Floor((decimal)(linearTileNum / (this.tilesetViewer.ActiveTilesetSurface.Size.Width / Constants.TILEWIDTH)));
                                        tileX = linearTileNum - ((this.tilesetViewer.ActiveTilesetSurface.Size.Width / Constants.TILEWIDTH) * tileY);
                                    }

                                    // Attempt to get the tile
                                    this.tilesetViewer.SelectedTile = new Point(tileX, tileY);
                                }
                            }

                            break;
                        }
                }
            }
        }

        private void MapViewer_MouseMotion(object sender, SdlDotNet.Input.MouseMotionEventArgs e)
        {
            if (this.InMapEditor)
            {
                if (this.mapViewer.ActiveMap != null && this.mapViewer.ActiveMap.Loaded)
                {
                    Point location = this.mapViewer.ScreenLocation;
                    Point relPoint = new Point(e.Position.X - location.X, e.Position.Y - location.Y);
                    int x = (relPoint.X / Constants.TILEWIDTH) + Graphic.Renderers.Screen.ScreenRenderer.Camera.X;
                    int y = (relPoint.Y / Constants.TILEHEIGHT) + Graphic.Renderers.Screen.ScreenRenderer.Camera.Y;
                    if (x <= this.mapViewer.ActiveMap.MaxX && y <= this.mapViewer.ActiveMap.MaxY && x >= 0 && y >= 0)
                    {
                        // Graphic.Renderers.Screen.ScreenRenderer.Camera.X = X;
                        // Graphic.Renderers.Screen.ScreenRenderer.Camera.Y = Y;
                        // if (tabControl1.SelectedIndex == 1) {
                        //    ShowAttribInfo(X, Y);
                        // }
                        if (IO.Options.DragAndPlace && (SdlDotNet.Input.Mouse.IsButtonPressed(SdlDotNet.Input.MouseButton.PrimaryButton) || SdlDotNet.Input.Mouse.IsButtonPressed(SdlDotNet.Input.MouseButton.SecondaryButton)))
                        {
                            if (this.btnTerrain.Selected)
                            {
                                this.PlaceLayer(relPoint, e.Button);
                            }
                            else if (this.btnAttributes.Selected)
                            {
                                this.PlaceAttribute(relPoint, e.Button);
                            }
                        }
                    }
                }
            }
        }

        private Enums.LayerType GetActiveLayer()
        {
            if (this.optGround.Checked)
            {
                if (!this.chkAnim.Checked)
                {
                    return Enums.LayerType.Ground;
                }
else
                {
                    return Enums.LayerType.GroundAnim;
                }
            }
            else if (this.optMask.Checked)
            {
                if (!this.chkAnim.Checked)
                {
                    return Enums.LayerType.Mask;
                }
else
                {
                    return Enums.LayerType.MaskAnim;
                }
            }
            else if (this.optMask2.Checked)
            {
                if (!this.chkAnim.Checked)
                {
                    return Enums.LayerType.Mask2;
                }
else
                {
                    return Enums.LayerType.Mask2Anim;
                }
            }
            else if (this.optFringe.Checked)
            {
                if (!this.chkAnim.Checked)
                {
                    return Enums.LayerType.Fringe;
                }
else
                {
                    return Enums.LayerType.FringeAnim;
                }
            }
            else if (this.optFringe2.Checked)
            {
                if (!this.chkAnim.Checked)
                {
                    return Enums.LayerType.Fringe2;
                }
else
                {
                    return Enums.LayerType.Fringe2Anim;
                }
            }
else
            {
                return Enums.LayerType.None;
            }
        }

        private Enums.TileType GetActiveAttribute()
        {
            if (this.optBlocked.Checked)
            {
                return Enums.TileType.Blocked;
            }

            if (this.optWarp.Checked)
            {
                return Enums.TileType.Warp;
            }

            if (this.optItem.Checked)
            {
                return Enums.TileType.Item;
            }

            if (this.optNpcAvoid.Checked)
            {
                return Enums.TileType.NPCAvoid;
            }

            if (this.optKey.Checked)
            {
                return Enums.TileType.Key;
            }

            if (this.optKeyOpen.Checked)
            {
                return Enums.TileType.KeyOpen;
            }

            if (this.optHeal.Checked)
            {
                return Enums.TileType.Heal;
            }

            if (this.optKill.Checked)
            {
                return Enums.TileType.Kill;
            }

            if (this.optSound.Checked)
            {
                return Enums.TileType.Sound;
            }

            if (this.optScripted.Checked)
            {
                return Enums.TileType.Scripted;
            }

            if (this.optNotice.Checked)
            {
                return Enums.TileType.Notice;
            }

            if (this.optLinkShop.Checked)
            {
                return Enums.TileType.LinkShop;
            }

            if (this.optDoor.Checked)
            {
                return Enums.TileType.Door;
            }

            if (this.optSign.Checked)
            {
                return Enums.TileType.Sign;
            }

            if (this.optSpriteChange.Checked)
            {
                return Enums.TileType.SpriteChange;
            }

            if (this.optShop.Checked)
            {
                return Enums.TileType.Shop;
            }

            if (this.optArena.Checked)
            {
                return Enums.TileType.Arena;
            }

            if (this.optBank.Checked)
            {
                return Enums.TileType.Bank;
            }

            if (this.optGuildBlock.Checked)
            {
                return Enums.TileType.Guild;
            }

            if (this.optSpriteBlock.Checked)
            {
                return Enums.TileType.SpriteBlock;
            }

            if (this.optMobileBlock.Checked)
            {
                return Enums.TileType.MobileBlock;
            }

            if (this.optLevelBlock.Checked)
            {
                return Enums.TileType.LevelBlock;
            }

            if (this.optAssembly.Checked)
            {
                return Enums.TileType.Assembly;
            }

            if (this.optEvolution.Checked)
            {
                return Enums.TileType.Evolution;
            }

            if (this.optStory.Checked)
            {
                return Enums.TileType.Story;
            }

            if (this.optMission.Checked)
            {
                return Enums.TileType.MissionBoard;
            }

            if (this.optScriptedSign.Checked)
            {
                return Enums.TileType.ScriptedSign;
            }

            if (this.optAmbiguous.Checked)
            {
                return Enums.TileType.Ambiguous;
            }

            if (this.optSlippery.Checked)
            {
                return Enums.TileType.Slippery;
            }

            if (this.optSlow.Checked)
            {
                return Enums.TileType.Slow;
            }

            if (this.optDropShop.Checked)
            {
                return Enums.TileType.DropShop;
            }

            return Enums.TileType.Walkable;
        }

        public void SetMapLayer(int x, int y, Enums.LayerType layer, int set, int startX, int startY, int endX, int endY, int rotation, bool flipped)
        {
            int length = this.tilesetViewer.ActiveTilesetSurface.Size.Width / 32;

            for (int i = startX; i <= endX; i++)
            {
                for (int j = startY; j <= endY; j++)
                {
                    this.SetMapLayer(x + i - startX, y + j - startY, layer, set, (length * j) + i, rotation, flipped);
                }
            }
        }

        public void SetMapLayer(int x, int y, Enums.LayerType layer, int set, int tile, int rotation, bool flipped)
        {
            if (x < 0 || x > this.mapViewer.ActiveMap.MaxX || y < 0 || y > this.mapViewer.ActiveMap.MaxY)
            {
                return;
            }

            switch (layer)
            {
                case Enums.LayerType.Ground:
                    this.mapViewer.ActiveMap.Tile[x, y].GroundSet = set;
                    this.mapViewer.ActiveMap.Tile[x, y].Ground = tile;
                    this.mapViewer.ActiveMap.Tile[x, y].GroundRotation = rotation;
                    this.mapViewer.ActiveMap.Tile[x, y].GroundFlipped = flipped;
                    break;
                case Enums.LayerType.GroundAnim:
                    this.mapViewer.ActiveMap.Tile[x, y].GroundAnimSet = set;
                    this.mapViewer.ActiveMap.Tile[x, y].GroundAnim = tile;
                    this.mapViewer.ActiveMap.Tile[x, y].GroundAnimRotation = rotation;
                    this.mapViewer.ActiveMap.Tile[x, y].GroundAnimFlipped = flipped;
                    break;
                case Enums.LayerType.Mask:
                    this.mapViewer.ActiveMap.Tile[x, y].MaskSet = set;
                    this.mapViewer.ActiveMap.Tile[x, y].Mask = tile;
                    this.mapViewer.ActiveMap.Tile[x, y].MaskRotation = rotation;
                    this.mapViewer.ActiveMap.Tile[x, y].MaskFlipped = flipped;
                    break;
                case Enums.LayerType.MaskAnim:
                    this.mapViewer.ActiveMap.Tile[x, y].AnimSet = set;
                    this.mapViewer.ActiveMap.Tile[x, y].Anim = tile;
                    this.mapViewer.ActiveMap.Tile[x, y].AnimRotation = rotation;
                    this.mapViewer.ActiveMap.Tile[x, y].AnimFlipped = flipped;
                    break;
                case Enums.LayerType.Mask2:
                    this.mapViewer.ActiveMap.Tile[x, y].Mask2Set = set;
                    this.mapViewer.ActiveMap.Tile[x, y].Mask2 = tile;
                    this.mapViewer.ActiveMap.Tile[x, y].Mask2Rotation = rotation;
                    this.mapViewer.ActiveMap.Tile[x, y].Mask2Flipped = flipped;
                    break;
                case Enums.LayerType.Mask2Anim:
                    this.mapViewer.ActiveMap.Tile[x, y].M2AnimSet = set;
                    this.mapViewer.ActiveMap.Tile[x, y].M2Anim = tile;
                    this.mapViewer.ActiveMap.Tile[x, y].M2AnimRotation = rotation;
                    this.mapViewer.ActiveMap.Tile[x, y].M2AnimFlipped = flipped;
                    break;
                case Enums.LayerType.Fringe:
                    this.mapViewer.ActiveMap.Tile[x, y].FringeSet = set;
                    this.mapViewer.ActiveMap.Tile[x, y].Fringe = tile;
                    this.mapViewer.ActiveMap.Tile[x, y].FringeRotation = rotation;
                    this.mapViewer.ActiveMap.Tile[x, y].FringeFlipped = flipped;
                    break;
                case Enums.LayerType.FringeAnim:
                    this.mapViewer.ActiveMap.Tile[x, y].FAnimSet = set;
                    this.mapViewer.ActiveMap.Tile[x, y].FAnim = tile;
                    this.mapViewer.ActiveMap.Tile[x, y].FAnimRotation = rotation;
                    this.mapViewer.ActiveMap.Tile[x, y].FAnimFlipped = flipped;
                    break;
                case Enums.LayerType.Fringe2:
                    this.mapViewer.ActiveMap.Tile[x, y].Fringe2Set = set;
                    this.mapViewer.ActiveMap.Tile[x, y].Fringe2 = tile;
                    this.mapViewer.ActiveMap.Tile[x, y].Fringe2Rotation = rotation;
                    this.mapViewer.ActiveMap.Tile[x, y].Fringe2Flipped = flipped;
                    break;
                case Enums.LayerType.Fringe2Anim:
                    this.mapViewer.ActiveMap.Tile[x, y].F2AnimSet = set;
                    this.mapViewer.ActiveMap.Tile[x, y].F2Anim = tile;
                    this.mapViewer.ActiveMap.Tile[x, y].F2AnimRotation = rotation;
                    this.mapViewer.ActiveMap.Tile[x, y].F2AnimFlipped = flipped;
                    break;
            }
        }

        public int GetActiveLayerTileset(int x, int y)
        {
            switch (this.GetActiveLayer())
            {
                case Enums.LayerType.Ground:
                    return this.mapViewer.ActiveMap.Tile[x, y].GroundSet;
                case Enums.LayerType.Mask:
                    return this.mapViewer.ActiveMap.Tile[x, y].MaskSet;
                case Enums.LayerType.MaskAnim:
                    return this.mapViewer.ActiveMap.Tile[x, y].AnimSet;
                case Enums.LayerType.Mask2:
                    return this.mapViewer.ActiveMap.Tile[x, y].Mask2Set;
                case Enums.LayerType.Mask2Anim:
                    return this.mapViewer.ActiveMap.Tile[x, y].M2AnimSet;
                case Enums.LayerType.Fringe:
                    return this.mapViewer.ActiveMap.Tile[x, y].FringeSet;
                case Enums.LayerType.FringeAnim:
                    return this.mapViewer.ActiveMap.Tile[x, y].FAnimSet;
                case Enums.LayerType.Fringe2:
                    return this.mapViewer.ActiveMap.Tile[x, y].Fringe2Set;
                case Enums.LayerType.Fringe2Anim:
                    return this.mapViewer.ActiveMap.Tile[x, y].F2AnimSet;
                default:
                    return 0;
            }
        }

        public int GetActiveLayerTile(int x, int y)
        {
            switch (this.GetActiveLayer())
            {
                case Enums.LayerType.Ground:
                    return this.mapViewer.ActiveMap.Tile[x, y].Ground;
                case Enums.LayerType.GroundAnim:
                    return this.mapViewer.ActiveMap.Tile[x, y].GroundAnim;
                case Enums.LayerType.Mask:
                    return this.mapViewer.ActiveMap.Tile[x, y].Mask;
                case Enums.LayerType.MaskAnim:
                    return this.mapViewer.ActiveMap.Tile[x, y].Anim;
                case Enums.LayerType.Mask2:
                    return this.mapViewer.ActiveMap.Tile[x, y].Mask2;
                case Enums.LayerType.Mask2Anim:
                    return this.mapViewer.ActiveMap.Tile[x, y].M2Anim;
                case Enums.LayerType.Fringe:
                    return this.mapViewer.ActiveMap.Tile[x, y].Fringe;
                case Enums.LayerType.FringeAnim:
                    return this.mapViewer.ActiveMap.Tile[x, y].FAnim;
                case Enums.LayerType.Fringe2:
                    return this.mapViewer.ActiveMap.Tile[x, y].Fringe2;
                case Enums.LayerType.Fringe2Anim:
                    return this.mapViewer.ActiveMap.Tile[x, y].F2Anim;
                default:
                    return 0;
            }
        }

        public void SetMapAttribute(int x, int y, Enums.TileType tileType, int data1, int data2, int data3,
            string string1, string string2, string string3, int dungeonValue)
            {
            Maps.Tile tile = this.mapViewer.ActiveMap.Tile[x, y];

            tile.Type = tileType;
            tile.Data1 = data1;
            tile.Data2 = data2;
            tile.Data3 = data3;
            tile.String1 = string1;
            tile.String2 = string2;
            tile.String3 = string3;
            tile.RDungeonMapValue = dungeonValue;
        }

        private void PlaceLayer(Point relPoint, SdlDotNet.Input.MouseButton button)
        {
            if (!File.Exists(Path.Combine(IO.Paths.StartupPath + "TileCache.xml")))
            {
                File.Create(Path.Combine(IO.Paths.StartupPath + "TileCache.xml"));
            }

            int X = (relPoint.X / Constants.TILEWIDTH) + Graphic.Renderers.Screen.ScreenRenderer.Camera.X;
            int Y = (relPoint.Y / Constants.TILEHEIGHT) + Graphic.Renderers.Screen.ScreenRenderer.Camera.Y;
            if (X <= this.mapViewer.ActiveMap.MaxX && Y <= this.mapViewer.ActiveMap.MaxY && X >= 0 && Y >= 0)
            {
                if (button == SdlDotNet.Input.MouseButton.PrimaryButton)
                {
                    if (this.tilesetViewer.SelectedTile == this.tilesetViewer.EndTile)
                    {
                        this.SetMapLayer(X, Y, this.GetActiveLayer(), this.tilesetViewer.ActiveTilesetSurface.TilesetNumber, (this.tilesetViewer.SelectedTile.Y * (this.tilesetViewer.ActiveTilesetSurface.Size.Width / Constants.TILEWIDTH)) + this.tilesetViewer.SelectedTile.X, this.hRotationSelect.Value * 90, this.chkFlip.Checked);
                    }
                    else
                    {
                        this.SetMapLayer(X, Y, this.GetActiveLayer(), this.tilesetViewer.ActiveTilesetSurface.TilesetNumber, this.tilesetViewer.SelectedTile.X, this.tilesetViewer.SelectedTile.Y, this.tilesetViewer.EndTile.X, this.tilesetViewer.EndTile.Y, this.hRotationSelect.Value * 90, this.chkFlip.Checked);
                    }
                }
                else if (button == SdlDotNet.Input.MouseButton.SecondaryButton)
                {
                    this.SetMapLayer(X, Y, this.GetActiveLayer(), 0, 0, 0, false);
                }

                if (this.InLiveMode)
                {
                    if (this.tilesetViewer.SelectedTile == this.tilesetViewer.EndTile)
                    {
                        Messenger.SendTilePlacedData(X, Y, this.GetActiveLayer(), this.GetActiveLayerTileset(X, Y), this.GetActiveLayerTile(X, Y), this.hRotationSelect.Value * 90, this.chkFlip.Checked);
                    }
else
                    {
                        for (int x = this.tilesetViewer.SelectedTile.X; x <= this.tilesetViewer.EndTile.X; x++)
                        {
                            for (int y = this.tilesetViewer.SelectedTile.Y; y <= this.tilesetViewer.EndTile.Y; y++)
                            {
                                if (X + x - this.tilesetViewer.SelectedTile.X < 0 || X + x - this.tilesetViewer.SelectedTile.X > this.mapViewer.ActiveMap.MaxX ||
                                    Y + y - this.tilesetViewer.SelectedTile.Y < 0 || Y + y - this.tilesetViewer.SelectedTile.Y > this.mapViewer.ActiveMap.MaxY)
                                    {
                                }
else
                                {
                                    Messenger.SendTilePlacedData(X + x - this.tilesetViewer.SelectedTile.X, Y + y - this.tilesetViewer.SelectedTile.Y, this.GetActiveLayer(), this.GetActiveLayerTileset(X + x - this.tilesetViewer.SelectedTile.X, Y + y - this.tilesetViewer.SelectedTile.Y), this.GetActiveLayerTile(X + x - this.tilesetViewer.SelectedTile.X, Y + y - this.tilesetViewer.SelectedTile.Y), this.hRotationSelect.Value * 90, this.chkFlip.Checked);
                                }
                            }
                        }

                        // Messenger.SendTilePlacedData(X, Y, X + tilesetViewer.EndTile.X - tilesetViewer.SelectedTile.X, Y + tilesetViewer.EndTile.Y - tilesetViewer.SelectedTile.Y, GetActiveLayer(), GetActiveLayerTileset(X, Y), GetActiveLayerTile(X, Y));
                    }
                }
            }
        }

        private void FillAttributesFromSettings(bool sendLiveModeData)
        {
            Enums.TileType tileType = Enums.TileType.Walkable;
            int data1 = 0;
            int data2 = 0;
            int data3 = 0;
            string string1 = string.Empty;
            string string2 = string.Empty;
            string string3 = string.Empty;
            int dungeonValue = 0;
            switch (this.GetActiveAttribute())
            {
                case Enums.TileType.Blocked:
                {
                        tileType = Enums.TileType.Blocked;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Warp:
                {
                        tileType = Enums.TileType.Warp;
                        data1 = this.nudStoryLevel.Value;
                        data2 = this.hsb1.Value;
                        data3 = this.hsb2.Value;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Item:
                {
                        tileType = Enums.TileType.Item;
                        data1 = this.hsb1.Value;
                        data2 = this.hsb2.Value;
                        data3 = this.chkTake.Checked.ToIntString().ToInt();
                        string1 = this.chkHidden.Checked.ToIntString();
                        string2 = this.txt1.Text;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.NPCAvoid:
                {
                        tileType = Enums.TileType.NPCAvoid;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Key:
                {
                        tileType = Enums.TileType.Key;
                        data1 = this.hsb1.Value;
                        data2 = this.chkTake.Checked.ToIntString().ToInt();
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.KeyOpen:
                {
                        tileType = Enums.TileType.KeyOpen;
                        data1 = this.hsb1.Value;
                        data2 = this.hsb2.Value;
                        data3 = 0;
                        string1 = this.txt1.Text;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Heal:
                {
                        tileType = Enums.TileType.Heal;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Kill:
                {
                        tileType = Enums.TileType.Kill;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Sound:
                {
                        tileType = Enums.TileType.Sound;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = ((ListBoxTextItem)this.lstSound.SelectedItems[0]).Text;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Scripted:
                {
                        tileType = Enums.TileType.Scripted;
                        data1 = this.hsb1.Value;
                        data2 = 0;
                        data3 = 0;
                        string1 = this.txt1.Text;
                        string2 = this.txt2.Text;
                        string3 = this.txt3.Text;
                    }

                    break;
                case Enums.TileType.Notice:
                {
                        tileType = Enums.TileType.Notice;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = this.txt1.Text;
                        string2 = this.txt2.Text;
                        string3 = ((ListBoxTextItem)this.lstSound.SelectedItems[0]).Text;
                    }

                    break;
                case Enums.TileType.LinkShop:
                {
                        tileType = Enums.TileType.LinkShop;
                        data1 = this.hsb1.Value;
                        data2 = this.hsb2.Value;
                        data3 = this.chkTake.Checked.ToIntString().ToInt();
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Door:
                {
                        tileType = Enums.TileType.Door;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Sign:
                {
                        tileType = Enums.TileType.Sign;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = this.txt1.Text;
                        string2 = this.txt2.Text;
                        string3 = this.txt3.Text;
                    }

                    break;
                case Enums.TileType.SpriteChange:
                {
                        tileType = Enums.TileType.SpriteChange;
                        data1 = this.hsb1.Value;
                        data2 = this.hsb2.Value;
                        data3 = this.hsb3.Value;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Shop:
                {
                        tileType = Enums.TileType.Shop;
                        data1 = this.hsb1.Value;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Arena:
                {
                        tileType = Enums.TileType.Arena;
                        data1 = this.hsb1.Value;
                        data2 = this.hsb2.Value;
                        data3 = this.hsb3.Value;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Bank:
                {
                        tileType = Enums.TileType.Bank;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.LevelBlock:
                {
                        tileType = Enums.TileType.LevelBlock;
                        data1 = this.nudStoryLevel.Value;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.SpriteBlock:
                {
                        tileType = Enums.TileType.SpriteBlock;
                        data1 = this.mode;
                        data2 = this.hsb1.Value;
                        data3 = this.hsb2.Value;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.MobileBlock:
                {
                        tileType = Enums.TileType.MobileBlock;
                        int mobility = 0;
                        for (int i = 15; i >= 0; i--)
                        {
                            if (this.tempArrayForMobility[i])
                            {
                                mobility += (int)Math.Pow(2, i);
                            }
                        }

                        data1 = mobility;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Guild:
                {
                        tileType = Enums.TileType.Guild;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Assembly:
                {
                        tileType = Enums.TileType.Assembly;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Evolution:
                {
                        tileType = Enums.TileType.Evolution;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Story:
                {
                        tileType = Enums.TileType.Story;
                        data1 = this.nudStoryLevel.Value - 1;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.MissionBoard:
                {
                        tileType = Enums.TileType.MissionBoard;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.ScriptedSign:
                {
                        tileType = Enums.TileType.ScriptedSign;
                        data1 = this.hsb1.Value;
                        data2 = 0;
                        data3 = 0;
                        string1 = this.txt1.Text;
                        string2 = this.txt2.Text;
                        string3 = this.txt3.Text;
                    }

                    break;
                case Enums.TileType.Ambiguous:
                {
                        tileType = Enums.TileType.Ambiguous;
                        data1 = 0;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Slippery:
                {
                        tileType = Enums.TileType.Slippery;
                        int mobility = 0;
                        for (int i = 15; i >= 0; i--)
                        {
                            if (this.tempArrayForMobility[i])
                            {
                                mobility += (int)Math.Pow(2, i);
                            }
                        }

                        data1 = mobility;
                        data2 = 0;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.Slow:
                {
                        tileType = Enums.TileType.Slow;
                        int mobility = 0;
                        for (int i = 15; i >= 0; i--)
                        {
                            if (this.tempArrayForMobility[i])
                            {
                                mobility += (int)Math.Pow(2, i);
                            }
                        }

                        data1 = mobility;
                        data2 = this.hsb2.Value;
                        data3 = 0;
                        string1 = string.Empty;
                        string2 = string.Empty;
                        string3 = string.Empty;
                    }

                    break;
                case Enums.TileType.DropShop:
                {
                    tileType = Enums.TileType.DropShop;
                        data2 = this.hsb1.Value;
                        data3 = this.hsb2.Value;
                        data1 = this.hsb3.Value;
                        string2 = this.txt1.Text;
                        string3 = string.Empty;
                    }

                    break;
            }

            dungeonValue = this.nudDungeonTileValue.Value;
            this.FillAttributes(tileType, data1, data2, data3, string1, string2, string3, dungeonValue);
            if (sendLiveModeData)
            {
                if (this.InLiveMode)
                {
                    Messenger.SendAttributeFillData(tileType, data1, data2, data3, string1, string2, string3, dungeonValue);
                }
            }
        }

        public void FillAttributes(Enums.TileType tileType, int data1, int data2, int data3, string string1, string string2, string string3, int dungeonValue)
        {
            if (this.mapViewer.ActiveMap != null && this.mapViewer.ActiveMap.Loaded)
            {
                for (int x = 0; x <= this.mapViewer.ActiveMap.MaxX; x++)
                {
                    for (int y = 0; y <= this.mapViewer.ActiveMap.MaxY; y++)
                    {
                        this.SetMapAttribute(x, y, tileType, data1, data2, data3, string1, string2, string3, dungeonValue);
                    }
                }
            }
        }

        public void FillLayer(Enums.LayerType layer, int tileSet, int tileNum, int rotation, bool flipped)
        {
            if (this.mapViewer.ActiveMap != null && this.mapViewer.ActiveMap.Loaded)
            {
                for (int x = 0; x <= this.mapViewer.ActiveMap.MaxX; x++)
                {
                    for (int y = 0; y <= this.mapViewer.ActiveMap.MaxY; y++)
                    {
                        this.SetMapLayer(x, y, layer, tileSet, tileNum, rotation, flipped);
                    }
                }
            }
        }

        private void ClearLayer()
        {
            if (this.mapViewer.ActiveMap != null && this.mapViewer.ActiveMap.Loaded)
            {
                for (int x = 0; x <= this.mapViewer.ActiveMap.MaxX; x++)
                {
                    for (int y = 0; y <= this.mapViewer.ActiveMap.MaxY; y++)
                    {
                        if (this.btnTerrain.Selected)
                        {
                            switch (this.GetActiveLayer())
                            {
                                case Enums.LayerType.Ground:
                                    this.mapViewer.ActiveMap.Tile[x, y].GroundSet = 0;
                                    this.mapViewer.ActiveMap.Tile[x, y].Ground = 0;
                                    break;
                                case Enums.LayerType.GroundAnim:
                                    this.mapViewer.ActiveMap.Tile[x, y].GroundAnimSet = 0;
                                    this.mapViewer.ActiveMap.Tile[x, y].GroundAnim = 0;
                                    break;
                                case Enums.LayerType.Mask:
                                    this.mapViewer.ActiveMap.Tile[x, y].MaskSet = 0;
                                    this.mapViewer.ActiveMap.Tile[x, y].Mask = 0;
                                    break;
                                case Enums.LayerType.MaskAnim:
                                    this.mapViewer.ActiveMap.Tile[x, y].AnimSet = 0;
                                    this.mapViewer.ActiveMap.Tile[x, y].Anim = 0;
                                    break;
                                case Enums.LayerType.Mask2:
                                    this.mapViewer.ActiveMap.Tile[x, y].Mask2Set = 0;
                                    this.mapViewer.ActiveMap.Tile[x, y].Mask2 = 0;
                                    break;
                                case Enums.LayerType.Mask2Anim:
                                    this.mapViewer.ActiveMap.Tile[x, y].M2AnimSet = 0;
                                    this.mapViewer.ActiveMap.Tile[x, y].M2Anim = 0;
                                    break;
                                case Enums.LayerType.Fringe:
                                    this.mapViewer.ActiveMap.Tile[x, y].FringeSet = 0;
                                    this.mapViewer.ActiveMap.Tile[x, y].Fringe = 0;
                                    break;
                                case Enums.LayerType.FringeAnim:
                                    this.mapViewer.ActiveMap.Tile[x, y].FAnimSet = 0;
                                    this.mapViewer.ActiveMap.Tile[x, y].FAnim = 0;
                                    break;
                                case Enums.LayerType.Fringe2:
                                    this.mapViewer.ActiveMap.Tile[x, y].Fringe2Set = 0;
                                    this.mapViewer.ActiveMap.Tile[x, y].Fringe2 = 0;
                                    break;
                                case Enums.LayerType.Fringe2Anim:
                                    this.mapViewer.ActiveMap.Tile[x, y].F2AnimSet = 0;
                                    this.mapViewer.ActiveMap.Tile[x, y].F2Anim = 0;
                                    break;
                            }
                        }
                        else if (this.btnAttributes.Selected)
                        {
                            this.mapViewer.ActiveMap.Tile[x, y].Type = Enums.TileType.Walkable;
                            this.mapViewer.ActiveMap.Tile[x, y].Data1 = 0;
                            this.mapViewer.ActiveMap.Tile[x, y].Data2 = 0;
                            this.mapViewer.ActiveMap.Tile[x, y].Data3 = 0;
                            this.mapViewer.ActiveMap.Tile[x, y].String1 = string.Empty;
                            this.mapViewer.ActiveMap.Tile[x, y].String2 = string.Empty;
                            this.mapViewer.ActiveMap.Tile[x, y].String3 = string.Empty;
                            this.mapViewer.ActiveMap.Tile[x, y].RDungeonMapValue = 0;
                        }
                    }
                }
            }
        }

        private void PlaceAttribute(Point relPoint, SdlDotNet.Input.MouseButton button)
        {
            int x = (relPoint.X / Constants.TILEWIDTH) + Graphic.Renderers.Screen.ScreenRenderer.Camera.X;
            int y = (relPoint.Y / Constants.TILEHEIGHT) + Graphic.Renderers.Screen.ScreenRenderer.Camera.Y;
            if (x <= this.mapViewer.ActiveMap.MaxX && y <= this.mapViewer.ActiveMap.MaxY && x >= 0 && y >= 0)
            {
                Maps.Tile tile = this.mapViewer.ActiveMap.Tile[x, y];
                if (button == SdlDotNet.Input.MouseButton.PrimaryButton)
                {
                    switch (this.GetActiveAttribute())
                    {
                        case Enums.TileType.Blocked:
                        {
                                tile.Type = Enums.TileType.Blocked;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Warp:
                        {
                                tile.Type = Enums.TileType.Warp;
                                tile.Data1 = this.nudStoryLevel.Value;
                                tile.Data2 = this.hsb1.Value;
                                tile.Data3 = this.hsb2.Value;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Item:
                        {
                                tile.Type = Enums.TileType.Item;
                                tile.Data1 = this.hsb1.Value;
                                tile.Data2 = this.hsb2.Value;
                                tile.Data3 = this.chkTake.Checked.ToIntString().ToInt();
                                tile.String1 = this.chkHidden.Checked.ToIntString();
                                tile.String2 = this.txt1.Text;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.NPCAvoid:
                        {
                                tile.Type = Enums.TileType.NPCAvoid;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Key:
                        {
                                tile.Type = Enums.TileType.Key;
                                tile.Data1 = this.hsb1.Value;
                                tile.Data2 = this.chkTake.Checked.ToIntString().ToInt();
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.KeyOpen:
                        {
                                tile.Type = Enums.TileType.KeyOpen;
                                tile.Data1 = this.hsb1.Value;
                                tile.Data2 = this.hsb2.Value;
                                tile.Data3 = 0;
                                tile.String1 = this.txt1.Text;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Heal:
                        {
                                tile.Type = Enums.TileType.Heal;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Kill:
                        {
                                tile.Type = Enums.TileType.Kill;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Sound:
                        {
                                tile.Type = Enums.TileType.Sound;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = ((ListBoxTextItem)this.lstSound.SelectedItems[0]).Text;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Scripted:
                        {
                                tile.Type = Enums.TileType.Scripted;
                                tile.Data1 = this.hsb1.Value;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = this.txt1.Text;
                                tile.String2 = this.txt2.Text;
                                tile.String3 = this.txt3.Text;
                            }

                            break;
                        case Enums.TileType.Notice:
                        {
                                tile.Type = Enums.TileType.Notice;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = this.txt1.Text;
                                tile.String2 = this.txt2.Text;
                                tile.String3 = ((ListBoxTextItem)this.lstSound.SelectedItems[0]).Text;
                            }

                            break;
                        case Enums.TileType.LinkShop:
                        {
                                tile.Type = Enums.TileType.LinkShop;
                                tile.Data1 = this.hsb1.Value;
                                tile.Data2 = this.hsb2.Value;
                                tile.Data3 = this.chkTake.Checked.ToIntString().ToInt();
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Door:
                        {
                                tile.Type = Enums.TileType.Door;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Sign:
                        {
                                tile.Type = Enums.TileType.Sign;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = this.txt1.Text;
                                tile.String2 = this.txt2.Text;
                                tile.String3 = this.txt3.Text;
                            }

                            break;
                        case Enums.TileType.SpriteChange:
                        {
                                tile.Type = Enums.TileType.SpriteChange;
                                tile.Data1 = this.hsb1.Value;
                                tile.Data2 = this.hsb2.Value;
                                tile.Data3 = this.hsb3.Value;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Shop:
                        {
                                tile.Type = Enums.TileType.Shop;
                                tile.Data1 = this.hsb1.Value;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Arena:
                        {
                                tile.Type = Enums.TileType.Arena;
                                tile.Data1 = this.hsb1.Value;
                                tile.Data2 = this.hsb2.Value;
                                tile.Data3 = this.hsb3.Value;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Bank:
                        {
                                tile.Type = Enums.TileType.Bank;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.LevelBlock:
                        {
                                tile.Type = Enums.TileType.LevelBlock;
                                tile.Data1 = this.nudStoryLevel.Value;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.SpriteBlock:
                        {
                                tile.Type = Enums.TileType.SpriteBlock;
                                tile.Data1 = this.mode;
                                tile.Data2 = this.hsb1.Value;
                                tile.Data3 = this.hsb2.Value;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.MobileBlock:
                        {
                                tile.Type = Enums.TileType.MobileBlock;
                                int mobility = 0;
                                for (int i = 15; i >= 0; i--)
                                {
                                    if (this.tempArrayForMobility[i])
                                    {
                                        mobility += (int)Math.Pow(2, i);
                                    }
                                }

                                tile.Data1 = mobility;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Guild:
                        {
                                tile.Type = Enums.TileType.Guild;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Assembly:
                        {
                                tile.Type = Enums.TileType.Assembly;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Evolution:
                        {
                                tile.Type = Enums.TileType.Evolution;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Story:
                        {
                                tile.Type = Enums.TileType.Story;
                                tile.Data1 = this.nudStoryLevel.Value - 1;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.MissionBoard:
                        {
                                tile.Type = Enums.TileType.MissionBoard;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.ScriptedSign:
                        {
                                tile.Type = Enums.TileType.ScriptedSign;
                                tile.Data1 = this.hsb1.Value;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = this.txt1.Text;
                                tile.String2 = this.txt2.Text;
                                tile.String3 = this.txt3.Text;
                            }

                            break;
                        case Enums.TileType.Ambiguous:
                        {
                                tile.Type = Enums.TileType.Ambiguous;
                                tile.Data1 = 0;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Slippery:
                        {
                                tile.Type = Enums.TileType.Slippery;
                                int mobility = 0;
                                for (int i = 15; i >= 0; i--)
                                {
                                    if (this.tempArrayForMobility[i])
                                    {
                                        mobility += (int)Math.Pow(2, i);
                                    }
                                }

                                tile.Data1 = mobility;
                                tile.Data2 = 0;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.Slow:
                        {
                                tile.Type = Enums.TileType.Slow;
                                int mobility = 0;
                                for (int i = 15; i >= 0; i--)
                                {
                                    if (this.tempArrayForMobility[i])
                                    {
                                        mobility += (int)Math.Pow(2, i);
                                    }
                                }

                                tile.Data1 = mobility;
                                tile.Data2 = this.hsb2.Value;
                                tile.Data3 = 0;
                                tile.String1 = string.Empty;
                                tile.String2 = string.Empty;
                                tile.String3 = string.Empty;
                            }

                            break;
                        case Enums.TileType.DropShop:
                        {
                                tile.Type = Enums.TileType.DropShop;
                                tile.Data2 = this.hsb1.Value;
                                tile.Data3 = this.hsb2.Value;
                                tile.Data1 = this.hsb3.Value;
                                tile.String2 = this.txt1.Text;
                                tile.String3 = string.Empty;
                            }

                            break;
                    }

                    tile.RDungeonMapValue = this.nudDungeonTileValue.Value;
                }
                else if (button == SdlDotNet.Input.MouseButton.SecondaryButton)
                {
                    tile.Type = Enums.TileType.Walkable;
                    tile.Data1 = 0;
                    tile.Data2 = 0;
                    tile.Data3 = 0;
                    tile.String1 = string.Empty;
                    tile.String2 = string.Empty;
                    tile.String3 = string.Empty;
                    tile.RDungeonMapValue = 0;
                }

                if (this.InLiveMode)
                {
                    Messenger.SendAttributePlacedData(x, y, tile.Type, tile.Data1, tile.Data2, tile.Data3,
                        tile.String1, tile.String2, tile.String3, tile.RDungeonMapValue);
                }
            }
        }

        public int GetSelectedTileNumber()
        {
            return (this.tilesetViewer.SelectedTile.Y * (this.tilesetViewer.ActiveTilesetSurface.Size.Width / Constants.TILEWIDTH)) + this.tilesetViewer.SelectedTile.X;
        }
    }
}
