﻿// <copyright file="winProperties.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors.MapEditor
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;
    using Extensions;
    using Maps;
    using Network;

    public partial class WinProperties : Form
    {
        private static WinProperties instance;

        private readonly MapProperties properties;
        private bool drag = false; // determine if we should be moving the form
        private Point startPoint = new Point(0, 0); // also for the moving

        public WinProperties()
        {
            this.InitializeComponent();

            this.properties = MapHelper.ActiveMap.ExportToPropertiesClass();

            this.txtMapName.Text = this.properties.Name;
            this.nudNorth.Value = this.properties.Up;
            this.nudSouth.Value = this.properties.Down;
            this.nudEast.Value = this.properties.Right;
            this.nudWest.Value = this.properties.Left;
            this.chkIndoors.Checked = this.properties.Indoors;
            this.chkHunger.Checked = this.properties.Belly;
            this.chkExp.Checked = this.properties.Exp;
            this.chkInstanced.Checked = this.properties.Instanced;
            this.chkRecruit.Checked = this.properties.Recruit;

            this.cmbMapMorality.DataSource = Enum.GetValues(typeof(Enums.MapMoral));
            this.cmbMapMorality.SelectedIndex = (int)this.properties.Moral;

            this.cmbMapWeather.DataSource = Enum.GetValues(typeof(Enums.Weather));
            this.cmbMapWeather.SelectedIndex = (int)this.properties.Weather;

            this.nudDarkness.Value = this.properties.Darkness;
            this.nudTimeLimit.Maximum = int.MaxValue;
            this.nudTimeLimit.Value = this.properties.TimeLimit;
            this.nudNpcSpawnTime.Value = this.properties.NpcSpawnTime;

            this.lbxNpcs.Items.Clear();
            for (int npc = 0; npc < this.properties.Npcs.Count; npc++)
            {
                MapNpcSettings newNpc = new MapNpcSettings();
                newNpc.NpcNum = this.properties.Npcs[npc].NpcNum;
                newNpc.MinLevel = this.properties.Npcs[npc].MinLevel;
                newNpc.MaxLevel = this.properties.Npcs[npc].MaxLevel;
                newNpc.AppearanceRate = this.properties.Npcs[npc].AppearanceRate;
                newNpc.StartStatus = this.properties.Npcs[npc].StartStatus;

                // newNpc.StartStatusCounter = properties.Npcs[npc].StartStatusCounter;
                newNpc.StartStatusChance = this.properties.Npcs[npc].StartStatusChance;

                this.lbxNpcs.Items.Add((npc + 1) + ": "
                    + "(" + newNpc.AppearanceRate + "%) " + "Lv." + newNpc.MinLevel + "-" + newNpc.MaxLevel + " " + Npc.NpcHelper.Npcs[newNpc.NpcNum].Name
                    + " [" + newNpc.StartStatusChance + "% " + newNpc.StartStatus.ToString() + "]");
            }

            this.nudNpcMin.Maximum = MaxInfo.MAXMAPNPCS;
            this.nudNpcMin.Value = this.properties.MinNpcs;
            this.nudNpcMax.Maximum = MaxInfo.MAXMAPNPCS;
            this.nudNpcMax.Value = this.properties.MaxNpcs;

            this.nudNpcNum.Maximum = MaxInfo.MaxItems;
            this.nudNpcSpawnX.Maximum = this.properties.MaxX;
            this.nudNpcSpawnY.Maximum = this.properties.MaxY;

            this.cbNpcStartStatus.DataSource = Enum.GetValues(typeof(Enums.StatusAilment));

            this.nudStatusCounter.Minimum = int.MinValue;
            this.nudStatusCounter.Maximum = int.MaxValue;

            this.nudMaxX.Maximum = MaxInfo.MaxMapX;
            this.nudMaxX.Value = this.properties.MaxX;

            this.nudMaxY.Maximum = MaxInfo.MaxMapY;
            this.nudMaxY.Value = this.properties.MaxY;

            this.pbHeader.MouseDown += new MouseEventHandler(this.Title_MouseDown);
            this.pbHeader.MouseUp += new MouseEventHandler(this.Title_MouseUp);
            this.pbHeader.MouseMove += new MouseEventHandler(this.Title_MouseMove);
            this.nudNpcNum.ValueChanged += new EventHandler(this.NudNpcNum_ValueChanged);
            this.btnStop.Click += new EventHandler(this.BtnStop_Click);
            this.btnPlay.Click += new EventHandler(this.BtnPlay_Click);
            this.btnOk.Click += new EventHandler(this.BtnOk_Click);
            this.btnCancel.Click += new EventHandler(this.BtnCancel_Click);
            this.btnGeneral.Click += new EventHandler(this.BtnGeneral_Click);
            this.btnNPC.Click += new EventHandler(this.BtnNPC_Click);
            this.btnScroll.Click += new EventHandler(this.BtnScroll_Click);
            this.btnAddNpc.Click += new EventHandler(this.BtnAddNpc_Click);
            this.btnLoadNpc.Click += new EventHandler(this.BtnLoadNpc_Click);
            this.btnRemoveNpc.Click += new EventHandler(this.BtnRemoveNpc_Click);
            this.lbxNpcs.KeyUp += new KeyEventHandler(this.LbxNpcs_KeyUp);

            this.LoadMusic();
        }

        public static WinProperties Instance
        {
            get
            {
                return instance;
            }
        }

        protected override void OnShown(EventArgs e)
        {
            instance = this;

            base.OnShown(e);
        }

        private void LbxNpcs_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                if (this.lbxNpcs.SelectedIndex >= 0)
                {
                    this.lbxNpcs.Items.RemoveAt(this.lbxNpcs.SelectedIndex);
                }
            }
        }

        // void btnClearNpc_Click(object sender, EventArgs e) {
        //    int npcIndex = cmbNpcSelector.SelectedIndex;
        //    if (npcIndex > -1) {
        //        properties.Npcs[npcIndex].NpcNum = 0;
        //        properties.Npcs[npcIndex].SpawnX = 0;
        //        properties.Npcs[npcIndex].SpawnY = 0;
        // properties.Npcs[npcIndex].Level = 1;

        // nudNpcNum.Value = 0;
        //        nudNpcSpawnX.Value = 0;
        //        nudNpcSpawnY.Value = 0;
        //        nudNpcLevel.Value = 1;
        //    }
        // }

        // void btnSaveNpc_Click(object sender, EventArgs e) {
        //    int npcIndex = cmbNpcSelector.SelectedIndex;
        //    if (npcIndex > -1) {
        //        properties.Npcs[npcIndex].NpcNum = nudNpcNum.Value;
        //        properties.Npcs[npcIndex].SpawnX = nudNpcSpawnX.Value;
        //        properties.Npcs[npcIndex].SpawnY = nudNpcSpawnY.Value;
        // properties.Npcs[npcIndex].Level = nudNpcLevel.Value;
        //    }
        // }

        // void nudNpcNum_ValueChanged(object sender, ValueChangedEventArgs e) {
        //    if (e.NewValue != 0) {
        //        lblNpcNumInfo.Text = Npc.NpcHelper.Npcs[e.NewValue].Name;
        //    } else {
        //        lblNpcNumInfo.Text = "None";
        //    }
        // }

        // void cmbNpcSelector_ItemSelected(object sender, EventArgs e) {
        //    int npcIndex = cmbNpcSelector.SelectedIndex;
        //    if (npcIndex > -1) {
        //        nudNpcNum.Value = properties.Npcs[npcIndex].NpcNum;
        //        nudNpcSpawnX.Value = properties.Npcs[npcIndex].SpawnX;
        //        nudNpcSpawnY.Value = properties.Npcs[npcIndex].SpawnY;
        // nudNpcLevel.Value = properties.Npcs[npcIndex].Level;
        //    }
        // }
        private void NudNpcNum_ValueChanged(object sender, EventArgs e)
        {
            if (this.nudNpcNum.Value > 0 && this.nudNpcNum.Value < MaxInfo.MaxNpcs)
            {
                this.lblNpcNum.Text = Npc.NpcHelper.Npcs[(int)this.nudNpcNum.Value].Name;
            }
            else
            {
                this.lblNpcNum.Text = "NPC #";
            }
        }

        private void BtnAddNpc_Click(object sender, EventArgs e)
        {
            if (this.nudNpcNum.Value > 0)
            {
                MapNpcSettings newNpc = new MapNpcSettings();
                newNpc.NpcNum = (int)this.nudNpcNum.Value;
                newNpc.SpawnX = (int)this.nudNpcSpawnX.Value;
                newNpc.SpawnY = (int)this.nudNpcSpawnY.Value;
                newNpc.MinLevel = (int)this.nudMinLevel.Value;
                newNpc.MaxLevel = (int)this.nudMaxLevel.Value;
                newNpc.AppearanceRate = (int)this.nudNpcSpawnRate.Value;
                newNpc.StartStatus = (Enums.StatusAilment)this.cbNpcStartStatus.SelectedIndex;
                newNpc.StartStatusCounter = (int)this.nudStatusCounter.Value;
                newNpc.StartStatusChance = (int)this.nudStatusChance.Value;

                this.properties.Npcs.Add(newNpc);
                this.lbxNpcs.Items.Add(this.properties.Npcs.Count + ": "
                    + "(" + newNpc.AppearanceRate + "%) " + "Lv." + newNpc.MinLevel + "-" + newNpc.MaxLevel + " " + Npc.NpcHelper.Npcs[newNpc.NpcNum].Name
                    + " [" + newNpc.StartStatusChance + "% " + newNpc.StartStatus.ToString() + "]");
            }
        }

        private void BtnLoadNpc_Click(object sender, EventArgs e)
        {
            if (this.lbxNpcs.SelectedIndex > -1)
            {
                this.nudNpcNum.Value = this.properties.Npcs[this.lbxNpcs.SelectedIndex].NpcNum;
                this.nudNpcSpawnX.Value = this.properties.Npcs[this.lbxNpcs.SelectedIndex].SpawnX;
                this.nudNpcSpawnY.Value = this.properties.Npcs[this.lbxNpcs.SelectedIndex].SpawnY;
                this.nudMinLevel.Value = this.properties.Npcs[this.lbxNpcs.SelectedIndex].MinLevel;
                this.nudMaxLevel.Value = this.properties.Npcs[this.lbxNpcs.SelectedIndex].MaxLevel;
                this.nudNpcSpawnRate.Value = this.properties.Npcs[this.lbxNpcs.SelectedIndex].AppearanceRate;
                this.cbNpcStartStatus.SelectedIndex = (int)this.properties.Npcs[this.lbxNpcs.SelectedIndex].StartStatus;
                this.nudStatusCounter.Value = this.properties.Npcs[this.lbxNpcs.SelectedIndex].StartStatusCounter;
                this.nudStatusChance.Value = this.properties.Npcs[this.lbxNpcs.SelectedIndex].StartStatusChance;
            }
        }

        private void BtnRemoveNpc_Click(object sender, EventArgs e)
        {
            if (this.lbxNpcs.SelectedIndex > -1)
            {
                this.properties.Npcs.RemoveAt(this.lbxNpcs.SelectedIndex);

                this.lbxNpcs.Items.Clear();
                for (int npc = 0; npc < this.properties.Npcs.Count; npc++)
                {
                    this.lbxNpcs.Items.Add((npc + 1) + ": "
                    + "(" + this.properties.Npcs[npc].AppearanceRate + "%) " + "Lv." + this.properties.Npcs[npc].MinLevel + "-" + this.properties.Npcs[npc].MaxLevel + " "
                    + Npc.NpcHelper.Npcs[this.properties.Npcs[npc].NpcNum].Name
                    + " [" + this.properties.Npcs[npc].StartStatusChance + "% " + this.properties.Npcs[npc].StartStatus.ToString() + "]");
                }
            }
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            // Stop the music
            Music.Music.AudioPlayer.StopMusic();

            // Play the map music
            if (!string.IsNullOrEmpty(MapHelper.ActiveMap.Music))
            {
                // Music.Music.AudioPlayer.PlayMusic(MapHelper.ActiveMap.Music);
                ((Music.Bass.BassAudioPlayer)Music.Music.AudioPlayer).FadeToNext(MapHelper.ActiveMap.Music, 1000);
            }
        }

        private void BtnPlay_Click(object sender, EventArgs e)
        {
            string song = null;
            if (this.cmbMusic.SelectedIndex > -1)
            {
                song = this.cmbMusic.GetItemText(this.cmbMusic.SelectedItem);
            }

            if (!string.IsNullOrEmpty(song))
            {
                Music.Music.AudioPlayer.PlayMusic(song, -1, true, true);
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            bool maxDimensionsChanged = false;
            this.properties.Name = string.IsNullOrEmpty(this.txtMapName.Text) ? this.properties.Name : this.txtMapName.Text;
            this.properties.Right = (int)this.nudEast.Value;
            this.properties.Left = (int)this.nudWest.Value;
            this.properties.Up = (int)this.nudNorth.Value;
            this.properties.Down = (int)this.nudSouth.Value;
            this.properties.Music = (this.cmbMusic.SelectedItem == null || string.IsNullOrEmpty(this.cmbMusic.GetItemText(this.cmbMusic.SelectedItem))) ? this.properties.Music : this.cmbMusic.GetItemText(this.cmbMusic.SelectedItem);

            int oldMaxX = this.properties.MaxX;
            this.properties.MaxX = (int)this.nudMaxX.Value;
            if (this.properties.MaxX != oldMaxX)
            {
                maxDimensionsChanged = true;
            }

            int oldMaxY = this.properties.MaxY;
            this.properties.MaxY = (int)this.nudMaxY.Value;
            if (this.properties.MaxY != oldMaxY)
            {
                maxDimensionsChanged = true;
            }

            if (this.cmbMapMorality.SelectedIndex > -1)
            {
                this.properties.Moral = (Enums.MapMoral)this.cmbMapMorality.SelectedIndex;
            }

            this.properties.Darkness = (int)this.nudDarkness.Value;

            this.properties.TimeLimit = (int)this.nudTimeLimit.Value;

            if (this.cmbMapWeather.SelectedIndex > -1)
            {
                this.properties.Weather = (Enums.Weather)this.cmbMapWeather.SelectedIndex;
            }

            this.properties.Indoors = this.chkIndoors.Checked;
            this.properties.Belly = this.chkHunger.Checked;
            this.properties.Recruit = this.chkRecruit.Checked;
            this.properties.Exp = this.chkExp.Checked;
            this.properties.Instanced = this.chkInstanced.Checked;

            this.properties.NpcSpawnTime = (int)this.nudNpcSpawnTime.Value;
            this.properties.MinNpcs = (int)this.nudNpcMin.Value;
            this.properties.MaxNpcs = (int)this.nudNpcMax.Value;

            if (maxDimensionsChanged)
            {
                MapHelper.ActiveMap.LoadFromPropertiesClass(this.properties);
                Messenger.SendSaveMap(MapHelper.ActiveMap);
                this.Close();
            }
            else
            {
                MapHelper.ActiveMap.LoadFromPropertiesClass(this.properties);
                this.Close();
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnGeneral_Click(object sender, EventArgs e)
        {
            if (!this.pnlGeneral.Visible)
            {
                this.pnlGeneral.Show();
                this.pnlNPC.Hide();
                this.pnlScroll.Hide();
                this.Size = new Size(500, 500);
            }
        }

        private void BtnNPC_Click(object sender, EventArgs e)
        {
            if (!this.pnlNPC.Visible)
            {
                this.pnlGeneral.Hide();
                this.pnlNPC.Show();
                this.pnlScroll.Hide();
                this.Size = new Size(500, 500);
            }
        }

        private void BtnScroll_Click(object sender, EventArgs e)
        {
            if (!this.pnlScroll.Visible)
            {
                this.pnlGeneral.Hide();
                this.pnlNPC.Hide();
                this.pnlScroll.Show();
                this.Size = new Size(500, 150);
            }
        }

        private void LoadMusic()
        {
            System.Threading.Thread trackListDownloadThread = new System.Threading.Thread(new System.Threading.ThreadStart(this.LoadMusicBackground));
            trackListDownloadThread.IsBackground = true;
            trackListDownloadThread.Start();

            // SdlDotNet.Graphics.Font font = Logic.Graphic.FontManager.LoadFont("PMU", 18);
            // string[] musicFiles = System.IO.Directory.GetFiles(IO.Paths.MusicPath);
            // for (int i = 0; i < musicFiles.Length; i++)
            // {
            //    cmbMusic.Items.Add(new ListBoxTextItem(font, System.IO.Path.GetFileName(musicFiles[i])));
            // }
            // cmbMusic.SelectItem(properties.Music);
        }

        private void LoadMusicBackground()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(this.LoadMusicBackground), new object[] { });
                return;
            }

            WebClient client = new WebClient();
            using (MemoryStream musicTrackListXml = new MemoryStream(client.DownloadData(IO.Options.MusicAddress + "TrackList.xml")))
            {
                Music.TrackList trackList = new Music.TrackList();
                musicTrackListXml.Seek(0, SeekOrigin.Begin);
                using (XmlReader reader = XmlReader.Create(musicTrackListXml))
                {
                    trackList.Load(reader);
                }

                foreach (Music.TrackListEntry entry in trackList.Entries)
                {
                    this.cmbMusic.Items.Add(entry.TrackName);
                }

                // string[] musicFiles = System.IO.Directory.GetFiles(IO.Paths.MusicPath);
                // for (int i = 0; i < musicFiles.Length; i++) {
                //    cmbMusic.Items.Add(new ListBoxTextItem(font, System.IO.Path.GetFileName(musicFiles[i])));
                // }
                for (int i = 0; i < this.cmbMusic.Items.Count; i++)
                {
                    if (this.cmbMusic.GetItemText(this.cmbMusic.Items[i]) == this.properties.Music)
                    {
                        this.cmbMusic.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            if (File.Exists(Path.Combine(Application.StartupPath, "tmp.png")))
            {
                File.Delete(Path.Combine(Application.StartupPath, "tmp.png"));
            }

            instance = null;

            base.OnClosed(e);
        }

        private void PbMapNorth_Click(object sender, EventArgs e)
        {
            if (this.nudNorth.Value > 0)
            {
                Messenger.SendMapRequest((int)this.nudNorth.Value);
            }
        }

        private void PbMapSouth_Click(object sender, EventArgs e)
        {
            if (this.nudSouth.Value > 0)
            {
                Messenger.SendMapRequest((int)this.nudSouth.Value);
            }
        }

        private void PbMapEast_Click(object sender, EventArgs e)
        {
            if (this.nudEast.Value > 0)
            {
                Messenger.SendMapRequest((int)this.nudEast.Value);
            }
        }

        private void PbMapWest_Click(object sender, EventArgs e)
        {
            if (this.nudWest.Value > 0)
            {
                Messenger.SendMapRequest((int)this.nudWest.Value);
            }
        }

        private void Title_MouseUp(object sender, MouseEventArgs e)
        {
            this.drag = false;
        }

        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            this.startPoint = e.Location;
            this.drag = true;
        }

        private void Title_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.drag)
            { // if we should be dragging it, we need to figure out some movement
                Point p1 = new Point(e.X, e.Y);
                Point p2 = this.PointToScreen(p1);
                Point p3 = new Point(
                    p2.X - this.startPoint.X,
                                     p2.Y - this.startPoint.Y);
                this.Location = p3;
            }
        }
    }
}