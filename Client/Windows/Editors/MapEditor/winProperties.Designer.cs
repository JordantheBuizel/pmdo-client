﻿namespace Client.Logic.Windows.Editors.MapEditor
{
    partial class WinProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WinProperties));
            this.btnGeneral = new System.Windows.Forms.Button();
            this.btnNPC = new System.Windows.Forms.Button();
            this.btnScroll = new System.Windows.Forms.Button();
            this.pnlGeneral = new System.Windows.Forms.Panel();
            this.btnStop = new System.Windows.Forms.PictureBox();
            this.btnPlay = new System.Windows.Forms.PictureBox();
            this.chkInstanced = new Client.Logic.Extensions.CheckBoxImage();
            this.chkExp = new Client.Logic.Extensions.CheckBoxImage();
            this.chkRecruit = new Client.Logic.Extensions.CheckBoxImage();
            this.chkHunger = new Client.Logic.Extensions.CheckBoxImage();
            this.chkIndoors = new Client.Logic.Extensions.CheckBoxImage();
            this.cmbMapWeather = new System.Windows.Forms.ComboBox();
            this.cmbMusic = new System.Windows.Forms.ComboBox();
            this.cmbMapMorality = new System.Windows.Forms.ComboBox();
            this.pbMapWest = new System.Windows.Forms.PictureBox();
            this.pbMapEast = new System.Windows.Forms.PictureBox();
            this.pbMapSouth = new System.Windows.Forms.PictureBox();
            this.pbMapNorth = new System.Windows.Forms.PictureBox();
            this.nudWest = new System.Windows.Forms.NumericUpDown();
            this.nudEast = new System.Windows.Forms.NumericUpDown();
            this.nudSouth = new System.Windows.Forms.NumericUpDown();
            this.nudTimeLimit = new System.Windows.Forms.NumericUpDown();
            this.nudDarkness = new System.Windows.Forms.NumericUpDown();
            this.nudNorth = new System.Windows.Forms.NumericUpDown();
            this.lblWest = new System.Windows.Forms.Label();
            this.lblEast = new System.Windows.Forms.Label();
            this.lblSouth = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblIndoors = new System.Windows.Forms.Label();
            this.lblDarkness = new System.Windows.Forms.Label();
            this.lblWeather = new System.Windows.Forms.Label();
            this.lblInstanced = new System.Windows.Forms.Label();
            this.lblHunger = new System.Windows.Forms.Label();
            this.lblExp = new System.Windows.Forms.Label();
            this.lblRecruit = new System.Windows.Forms.Label();
            this.lblMoral = new System.Windows.Forms.Label();
            this.lblNorth = new System.Windows.Forms.Label();
            this.lblGlobal = new System.Windows.Forms.Label();
            this.lblMusic = new System.Windows.Forms.Label();
            this.lblMapSwitchover = new System.Windows.Forms.Label();
            this.lblMapName = new System.Windows.Forms.Label();
            this.txtMapName = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlNPC = new System.Windows.Forms.Panel();
            this.pbHeader = new System.Windows.Forms.PictureBox();
            this.lblNpcSpawnTime = new System.Windows.Forms.Label();
            this.nudNpcSpawnTime = new System.Windows.Forms.NumericUpDown();
            this.lblNpcMin = new System.Windows.Forms.Label();
            this.nudNpcMin = new System.Windows.Forms.NumericUpDown();
            this.lblNpcMax = new System.Windows.Forms.Label();
            this.nudNpcMax = new System.Windows.Forms.NumericUpDown();
            this.lblNpcNum = new System.Windows.Forms.Label();
            this.lblNpcSpawnX = new System.Windows.Forms.Label();
            this.lblNpcSpawnY = new System.Windows.Forms.Label();
            this.nudNpcNum = new System.Windows.Forms.NumericUpDown();
            this.nudNpcSpawnX = new System.Windows.Forms.NumericUpDown();
            this.nudNpcSpawnY = new System.Windows.Forms.NumericUpDown();
            this.lblNpcSpawnRate = new System.Windows.Forms.Label();
            this.lblMinLevel = new System.Windows.Forms.Label();
            this.lblMaxLevel = new System.Windows.Forms.Label();
            this.nudNpcSpawnRate = new System.Windows.Forms.NumericUpDown();
            this.nudMinLevel = new System.Windows.Forms.NumericUpDown();
            this.nudMaxLevel = new System.Windows.Forms.NumericUpDown();
            this.lblStatusCounter = new System.Windows.Forms.Label();
            this.lblStatusChance = new System.Windows.Forms.Label();
            this.nudStatusCounter = new System.Windows.Forms.NumericUpDown();
            this.nudStatusChance = new System.Windows.Forms.NumericUpDown();
            this.lbxNpcs = new System.Windows.Forms.ListBox();
            this.lblNpcStartStatus = new System.Windows.Forms.Label();
            this.cbNpcStartStatus = new System.Windows.Forms.ComboBox();
            this.btnAddNpc = new System.Windows.Forms.Button();
            this.btnRemoveNpc = new System.Windows.Forms.Button();
            this.btnLoadNpc = new System.Windows.Forms.Button();
            this.pnlScroll = new System.Windows.Forms.Panel();
            this.nudMaxY = new System.Windows.Forms.NumericUpDown();
            this.nudMaxX = new System.Windows.Forms.NumericUpDown();
            this.lblMaxX = new System.Windows.Forms.Label();
            this.lblMaxY = new System.Windows.Forms.Label();
            this.pnlGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPlay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInstanced)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecruit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHunger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIndoors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMapWest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMapEast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMapSouth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMapNorth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSouth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimeLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDarkness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNorth)).BeginInit();
            this.pnlNPC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcSpawnTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcSpawnX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcSpawnY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcSpawnRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStatusCounter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStatusChance)).BeginInit();
            this.pnlScroll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxX)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGeneral
            // 
            this.btnGeneral.BackgroundImage = global::Client.Logic.Properties.Resources.button;
            this.btnGeneral.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGeneral.FlatAppearance.BorderSize = 0;
            this.btnGeneral.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGeneral.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGeneral.Location = new System.Drawing.Point(61, 27);
            this.btnGeneral.Name = "btnGeneral";
            this.btnGeneral.Size = new System.Drawing.Size(123, 36);
            this.btnGeneral.TabIndex = 0;
            this.btnGeneral.Text = "General Options";
            this.btnGeneral.UseVisualStyleBackColor = true;
            this.btnGeneral.Click += new System.EventHandler(this.BtnGeneral_Click);
            // 
            // btnNPC
            // 
            this.btnNPC.BackgroundImage = global::Client.Logic.Properties.Resources.button;
            this.btnNPC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNPC.FlatAppearance.BorderSize = 0;
            this.btnNPC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNPC.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.btnNPC.Location = new System.Drawing.Point(190, 27);
            this.btnNPC.Name = "btnNPC";
            this.btnNPC.Size = new System.Drawing.Size(123, 36);
            this.btnNPC.TabIndex = 0;
            this.btnNPC.Text = "NPC";
            this.btnNPC.UseVisualStyleBackColor = true;
            this.btnNPC.Click += new System.EventHandler(this.BtnNPC_Click);
            // 
            // btnScroll
            // 
            this.btnScroll.BackgroundImage = global::Client.Logic.Properties.Resources.button;
            this.btnScroll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnScroll.FlatAppearance.BorderSize = 0;
            this.btnScroll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnScroll.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.btnScroll.Location = new System.Drawing.Point(319, 27);
            this.btnScroll.Name = "btnScroll";
            this.btnScroll.Size = new System.Drawing.Size(123, 36);
            this.btnScroll.TabIndex = 0;
            this.btnScroll.Text = "Scroll";
            this.btnScroll.UseVisualStyleBackColor = true;
            this.btnScroll.Click += new System.EventHandler(this.BtnScroll_Click);
            // 
            // pnlGeneral
            // 
            this.pnlGeneral.BackColor = System.Drawing.Color.Transparent;
            this.pnlGeneral.Controls.Add(this.btnStop);
            this.pnlGeneral.Controls.Add(this.btnPlay);
            this.pnlGeneral.Controls.Add(this.chkInstanced);
            this.pnlGeneral.Controls.Add(this.btnCancel);
            this.pnlGeneral.Controls.Add(this.btnOk);
            this.pnlGeneral.Controls.Add(this.chkExp);
            this.pnlGeneral.Controls.Add(this.chkRecruit);
            this.pnlGeneral.Controls.Add(this.chkHunger);
            this.pnlGeneral.Controls.Add(this.chkIndoors);
            this.pnlGeneral.Controls.Add(this.cmbMapWeather);
            this.pnlGeneral.Controls.Add(this.cmbMusic);
            this.pnlGeneral.Controls.Add(this.cmbMapMorality);
            this.pnlGeneral.Controls.Add(this.pbMapWest);
            this.pnlGeneral.Controls.Add(this.pbMapEast);
            this.pnlGeneral.Controls.Add(this.pbMapSouth);
            this.pnlGeneral.Controls.Add(this.pbMapNorth);
            this.pnlGeneral.Controls.Add(this.nudWest);
            this.pnlGeneral.Controls.Add(this.nudEast);
            this.pnlGeneral.Controls.Add(this.nudSouth);
            this.pnlGeneral.Controls.Add(this.nudTimeLimit);
            this.pnlGeneral.Controls.Add(this.nudDarkness);
            this.pnlGeneral.Controls.Add(this.nudNorth);
            this.pnlGeneral.Controls.Add(this.lblWest);
            this.pnlGeneral.Controls.Add(this.lblEast);
            this.pnlGeneral.Controls.Add(this.lblSouth);
            this.pnlGeneral.Controls.Add(this.lblTime);
            this.pnlGeneral.Controls.Add(this.lblIndoors);
            this.pnlGeneral.Controls.Add(this.lblDarkness);
            this.pnlGeneral.Controls.Add(this.lblWeather);
            this.pnlGeneral.Controls.Add(this.lblInstanced);
            this.pnlGeneral.Controls.Add(this.lblHunger);
            this.pnlGeneral.Controls.Add(this.lblExp);
            this.pnlGeneral.Controls.Add(this.lblRecruit);
            this.pnlGeneral.Controls.Add(this.lblMoral);
            this.pnlGeneral.Controls.Add(this.lblNorth);
            this.pnlGeneral.Controls.Add(this.lblGlobal);
            this.pnlGeneral.Controls.Add(this.lblMusic);
            this.pnlGeneral.Controls.Add(this.lblMapSwitchover);
            this.pnlGeneral.Controls.Add(this.lblMapName);
            this.pnlGeneral.Controls.Add(this.txtMapName);
            this.pnlGeneral.Location = new System.Drawing.Point(11, 69);
            this.pnlGeneral.Name = "pnlGeneral";
            this.pnlGeneral.Size = new System.Drawing.Size(476, 413);
            this.pnlGeneral.TabIndex = 1;
            // 
            // btnStop
            // 
            this.btnStop.Image = global::Client.Logic.Properties.Resources.stop;
            this.btnStop.Location = new System.Drawing.Point(80, 277);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(32, 32);
            this.btnStop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnStop.TabIndex = 8;
            this.btnStop.TabStop = false;
            // 
            // btnPlay
            // 
            this.btnPlay.Image = global::Client.Logic.Properties.Resources.play_xxl;
            this.btnPlay.Location = new System.Drawing.Point(30, 277);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(32, 32);
            this.btnPlay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPlay.TabIndex = 8;
            this.btnPlay.TabStop = false;
            // 
            // chkInstanced
            // 
            this.chkInstanced.Checked = false;
            this.chkInstanced.Image = ((System.Drawing.Image)(resources.GetObject("chkInstanced.Image")));
            this.chkInstanced.Location = new System.Drawing.Point(366, 338);
            this.chkInstanced.Name = "chkInstanced";
            this.chkInstanced.Size = new System.Drawing.Size(14, 14);
            this.chkInstanced.TabIndex = 7;
            this.chkInstanced.TabStop = false;
            // 
            // chkExp
            // 
            this.chkExp.Checked = false;
            this.chkExp.Image = ((System.Drawing.Image)(resources.GetObject("chkExp.Image")));
            this.chkExp.Location = new System.Drawing.Point(366, 306);
            this.chkExp.Name = "chkExp";
            this.chkExp.Size = new System.Drawing.Size(14, 14);
            this.chkExp.TabIndex = 7;
            this.chkExp.TabStop = false;
            // 
            // chkRecruit
            // 
            this.chkRecruit.Checked = false;
            this.chkRecruit.Image = ((System.Drawing.Image)(resources.GetObject("chkRecruit.Image")));
            this.chkRecruit.Location = new System.Drawing.Point(366, 277);
            this.chkRecruit.Name = "chkRecruit";
            this.chkRecruit.Size = new System.Drawing.Size(14, 14);
            this.chkRecruit.TabIndex = 7;
            this.chkRecruit.TabStop = false;
            // 
            // chkHunger
            // 
            this.chkHunger.Checked = false;
            this.chkHunger.Image = ((System.Drawing.Image)(resources.GetObject("chkHunger.Image")));
            this.chkHunger.Location = new System.Drawing.Point(366, 247);
            this.chkHunger.Name = "chkHunger";
            this.chkHunger.Size = new System.Drawing.Size(14, 14);
            this.chkHunger.TabIndex = 7;
            this.chkHunger.TabStop = false;
            // 
            // chkIndoors
            // 
            this.chkIndoors.Checked = false;
            this.chkIndoors.Image = ((System.Drawing.Image)(resources.GetObject("chkIndoors.Image")));
            this.chkIndoors.Location = new System.Drawing.Point(366, 218);
            this.chkIndoors.Name = "chkIndoors";
            this.chkIndoors.Size = new System.Drawing.Size(14, 14);
            this.chkIndoors.TabIndex = 7;
            this.chkIndoors.TabStop = false;
            // 
            // cmbMapWeather
            // 
            this.cmbMapWeather.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMapWeather.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.cmbMapWeather.FormattingEnabled = true;
            this.cmbMapWeather.Location = new System.Drawing.Point(308, 119);
            this.cmbMapWeather.Name = "cmbMapWeather";
            this.cmbMapWeather.Size = new System.Drawing.Size(121, 24);
            this.cmbMapWeather.TabIndex = 6;
            // 
            // cmbMusic
            // 
            this.cmbMusic.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMusic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.cmbMusic.FormattingEnabled = true;
            this.cmbMusic.Location = new System.Drawing.Point(30, 244);
            this.cmbMusic.Name = "cmbMusic";
            this.cmbMusic.Size = new System.Drawing.Size(309, 24);
            this.cmbMusic.TabIndex = 6;
            // 
            // cmbMapMorality
            // 
            this.cmbMapMorality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMapMorality.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.cmbMapMorality.FormattingEnabled = true;
            this.cmbMapMorality.Location = new System.Drawing.Point(308, 94);
            this.cmbMapMorality.Name = "cmbMapMorality";
            this.cmbMapMorality.Size = new System.Drawing.Size(121, 24);
            this.cmbMapMorality.TabIndex = 6;
            // 
            // pbMapWest
            // 
            this.pbMapWest.Image = global::Client.Logic.Properties.Resources.mapview;
            this.pbMapWest.Location = new System.Drawing.Point(179, 172);
            this.pbMapWest.Name = "pbMapWest";
            this.pbMapWest.Size = new System.Drawing.Size(22, 22);
            this.pbMapWest.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbMapWest.TabIndex = 5;
            this.pbMapWest.TabStop = false;
            this.pbMapWest.Click += new System.EventHandler(this.PbMapWest_Click);
            // 
            // pbMapEast
            // 
            this.pbMapEast.Image = global::Client.Logic.Properties.Resources.mapview;
            this.pbMapEast.Location = new System.Drawing.Point(179, 146);
            this.pbMapEast.Name = "pbMapEast";
            this.pbMapEast.Size = new System.Drawing.Size(22, 22);
            this.pbMapEast.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbMapEast.TabIndex = 5;
            this.pbMapEast.TabStop = false;
            this.pbMapEast.Click += new System.EventHandler(this.PbMapEast_Click);
            // 
            // pbMapSouth
            // 
            this.pbMapSouth.Image = global::Client.Logic.Properties.Resources.mapview;
            this.pbMapSouth.Location = new System.Drawing.Point(179, 120);
            this.pbMapSouth.Name = "pbMapSouth";
            this.pbMapSouth.Size = new System.Drawing.Size(22, 22);
            this.pbMapSouth.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbMapSouth.TabIndex = 5;
            this.pbMapSouth.TabStop = false;
            this.pbMapSouth.Click += new System.EventHandler(this.PbMapSouth_Click);
            // 
            // pbMapNorth
            // 
            this.pbMapNorth.Image = global::Client.Logic.Properties.Resources.mapview;
            this.pbMapNorth.Location = new System.Drawing.Point(179, 95);
            this.pbMapNorth.Name = "pbMapNorth";
            this.pbMapNorth.Size = new System.Drawing.Size(22, 22);
            this.pbMapNorth.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbMapNorth.TabIndex = 5;
            this.pbMapNorth.TabStop = false;
            this.pbMapNorth.Click += new System.EventHandler(this.PbMapNorth_Click);
            // 
            // nudWest
            // 
            this.nudWest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudWest.Location = new System.Drawing.Point(107, 172);
            this.nudWest.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudWest.Name = "nudWest";
            this.nudWest.Size = new System.Drawing.Size(60, 22);
            this.nudWest.TabIndex = 4;
            // 
            // nudEast
            // 
            this.nudEast.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudEast.Location = new System.Drawing.Point(107, 146);
            this.nudEast.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudEast.Name = "nudEast";
            this.nudEast.Size = new System.Drawing.Size(60, 22);
            this.nudEast.TabIndex = 4;
            // 
            // nudSouth
            // 
            this.nudSouth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudSouth.Location = new System.Drawing.Point(107, 120);
            this.nudSouth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudSouth.Name = "nudSouth";
            this.nudSouth.Size = new System.Drawing.Size(60, 22);
            this.nudSouth.TabIndex = 4;
            // 
            // nudTimeLimit
            // 
            this.nudTimeLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudTimeLimit.Location = new System.Drawing.Point(308, 172);
            this.nudTimeLimit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudTimeLimit.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nudTimeLimit.Name = "nudTimeLimit";
            this.nudTimeLimit.Size = new System.Drawing.Size(121, 22);
            this.nudTimeLimit.TabIndex = 4;
            // 
            // nudDarkness
            // 
            this.nudDarkness.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudDarkness.Location = new System.Drawing.Point(308, 146);
            this.nudDarkness.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudDarkness.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nudDarkness.Name = "nudDarkness";
            this.nudDarkness.Size = new System.Drawing.Size(121, 22);
            this.nudDarkness.TabIndex = 4;
            // 
            // nudNorth
            // 
            this.nudNorth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudNorth.Location = new System.Drawing.Point(107, 95);
            this.nudNorth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudNorth.Name = "nudNorth";
            this.nudNorth.Size = new System.Drawing.Size(60, 22);
            this.nudNorth.TabIndex = 4;
            // 
            // lblWest
            // 
            this.lblWest.AutoSize = true;
            this.lblWest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblWest.Location = new System.Drawing.Point(40, 174);
            this.lblWest.Name = "lblWest";
            this.lblWest.Size = new System.Drawing.Size(42, 16);
            this.lblWest.TabIndex = 3;
            this.lblWest.Text = "West:";
            // 
            // lblEast
            // 
            this.lblEast.AutoSize = true;
            this.lblEast.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblEast.Location = new System.Drawing.Point(40, 148);
            this.lblEast.Name = "lblEast";
            this.lblEast.Size = new System.Drawing.Size(38, 16);
            this.lblEast.TabIndex = 3;
            this.lblEast.Text = "East:";
            // 
            // lblSouth
            // 
            this.lblSouth.AutoSize = true;
            this.lblSouth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblSouth.Location = new System.Drawing.Point(40, 122);
            this.lblSouth.Name = "lblSouth";
            this.lblSouth.Size = new System.Drawing.Size(45, 16);
            this.lblSouth.TabIndex = 3;
            this.lblSouth.Text = "South:";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblTime.Location = new System.Drawing.Point(237, 174);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(42, 16);
            this.lblTime.TabIndex = 3;
            this.lblTime.Text = "Time:";
            // 
            // lblIndoors
            // 
            this.lblIndoors.AutoSize = true;
            this.lblIndoors.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblIndoors.Location = new System.Drawing.Point(386, 218);
            this.lblIndoors.Name = "lblIndoors";
            this.lblIndoors.Size = new System.Drawing.Size(53, 16);
            this.lblIndoors.TabIndex = 3;
            this.lblIndoors.Text = "Indoors";
            // 
            // lblDarkness
            // 
            this.lblDarkness.AutoSize = true;
            this.lblDarkness.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblDarkness.Location = new System.Drawing.Point(237, 148);
            this.lblDarkness.Name = "lblDarkness";
            this.lblDarkness.Size = new System.Drawing.Size(69, 16);
            this.lblDarkness.TabIndex = 3;
            this.lblDarkness.Text = "Darkness:";
            // 
            // lblWeather
            // 
            this.lblWeather.AutoSize = true;
            this.lblWeather.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblWeather.Location = new System.Drawing.Point(237, 122);
            this.lblWeather.Name = "lblWeather";
            this.lblWeather.Size = new System.Drawing.Size(62, 16);
            this.lblWeather.TabIndex = 3;
            this.lblWeather.Text = "Weather:";
            // 
            // lblInstanced
            // 
            this.lblInstanced.AutoSize = true;
            this.lblInstanced.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblInstanced.Location = new System.Drawing.Point(386, 338);
            this.lblInstanced.Name = "lblInstanced";
            this.lblInstanced.Size = new System.Drawing.Size(66, 16);
            this.lblInstanced.TabIndex = 3;
            this.lblInstanced.Text = "Instanced";
            // 
            // lblHunger
            // 
            this.lblHunger.AutoSize = true;
            this.lblHunger.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblHunger.Location = new System.Drawing.Point(386, 247);
            this.lblHunger.Name = "lblHunger";
            this.lblHunger.Size = new System.Drawing.Size(52, 16);
            this.lblHunger.TabIndex = 3;
            this.lblHunger.Text = "Hunger";
            // 
            // lblExp
            // 
            this.lblExp.AutoSize = true;
            this.lblExp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblExp.Location = new System.Drawing.Point(386, 306);
            this.lblExp.Name = "lblExp";
            this.lblExp.Size = new System.Drawing.Size(31, 16);
            this.lblExp.TabIndex = 3;
            this.lblExp.Text = "Exp";
            // 
            // lblRecruit
            // 
            this.lblRecruit.AutoSize = true;
            this.lblRecruit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblRecruit.Location = new System.Drawing.Point(386, 277);
            this.lblRecruit.Name = "lblRecruit";
            this.lblRecruit.Size = new System.Drawing.Size(50, 16);
            this.lblRecruit.TabIndex = 3;
            this.lblRecruit.Text = "Recruit";
            // 
            // lblMoral
            // 
            this.lblMoral.AutoSize = true;
            this.lblMoral.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblMoral.Location = new System.Drawing.Point(237, 97);
            this.lblMoral.Name = "lblMoral";
            this.lblMoral.Size = new System.Drawing.Size(45, 16);
            this.lblMoral.TabIndex = 3;
            this.lblMoral.Text = "Moral:";
            // 
            // lblNorth
            // 
            this.lblNorth.AutoSize = true;
            this.lblNorth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblNorth.Location = new System.Drawing.Point(40, 97);
            this.lblNorth.Name = "lblNorth";
            this.lblNorth.Size = new System.Drawing.Size(43, 16);
            this.lblNorth.TabIndex = 3;
            this.lblNorth.Text = "North:";
            // 
            // lblGlobal
            // 
            this.lblGlobal.AutoSize = true;
            this.lblGlobal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblGlobal.Location = new System.Drawing.Point(270, 67);
            this.lblGlobal.Name = "lblGlobal";
            this.lblGlobal.Size = new System.Drawing.Size(114, 20);
            this.lblGlobal.TabIndex = 2;
            this.lblGlobal.Text = "Global Options";
            // 
            // lblMusic
            // 
            this.lblMusic.AutoSize = true;
            this.lblMusic.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblMusic.Location = new System.Drawing.Point(46, 215);
            this.lblMusic.Name = "lblMusic";
            this.lblMusic.Size = new System.Drawing.Size(50, 20);
            this.lblMusic.TabIndex = 2;
            this.lblMusic.Text = "Music";
            // 
            // lblMapSwitchover
            // 
            this.lblMapSwitchover.AutoSize = true;
            this.lblMapSwitchover.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblMapSwitchover.Location = new System.Drawing.Point(46, 67);
            this.lblMapSwitchover.Name = "lblMapSwitchover";
            this.lblMapSwitchover.Size = new System.Drawing.Size(121, 20);
            this.lblMapSwitchover.TabIndex = 2;
            this.lblMapSwitchover.Text = "Map Switchover";
            // 
            // lblMapName
            // 
            this.lblMapName.AutoSize = true;
            this.lblMapName.Location = new System.Drawing.Point(27, 22);
            this.lblMapName.Name = "lblMapName";
            this.lblMapName.Size = new System.Drawing.Size(62, 13);
            this.lblMapName.TabIndex = 1;
            this.lblMapName.Text = "Map Name:";
            // 
            // txtMapName
            // 
            this.txtMapName.Location = new System.Drawing.Point(95, 19);
            this.txtMapName.Name = "txtMapName";
            this.txtMapName.Size = new System.Drawing.Size(296, 20);
            this.txtMapName.TabIndex = 0;
            // 
            // btnOk
            // 
            this.btnOk.BackgroundImage = global::Client.Logic.Properties.Resources.button;
            this.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOk.FlatAppearance.BorderSize = 0;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(13, 381);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(83, 29);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnGeneral_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackgroundImage = global::Client.Logic.Properties.Resources.button;
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(118, 381);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(83, 29);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnGeneral_Click);
            // 
            // pnlNPC
            // 
            this.pnlNPC.BackColor = System.Drawing.Color.Transparent;
            this.pnlNPC.Controls.Add(this.btnLoadNpc);
            this.pnlNPC.Controls.Add(this.btnRemoveNpc);
            this.pnlNPC.Controls.Add(this.btnAddNpc);
            this.pnlNPC.Controls.Add(this.cbNpcStartStatus);
            this.pnlNPC.Controls.Add(this.lbxNpcs);
            this.pnlNPC.Controls.Add(this.nudStatusChance);
            this.pnlNPC.Controls.Add(this.nudMaxLevel);
            this.pnlNPC.Controls.Add(this.nudNpcSpawnY);
            this.pnlNPC.Controls.Add(this.nudNpcMax);
            this.pnlNPC.Controls.Add(this.nudStatusCounter);
            this.pnlNPC.Controls.Add(this.nudMinLevel);
            this.pnlNPC.Controls.Add(this.nudNpcSpawnX);
            this.pnlNPC.Controls.Add(this.nudNpcMin);
            this.pnlNPC.Controls.Add(this.nudNpcSpawnRate);
            this.pnlNPC.Controls.Add(this.nudNpcNum);
            this.pnlNPC.Controls.Add(this.nudNpcSpawnTime);
            this.pnlNPC.Controls.Add(this.lblStatusChance);
            this.pnlNPC.Controls.Add(this.lblMaxLevel);
            this.pnlNPC.Controls.Add(this.lblNpcSpawnY);
            this.pnlNPC.Controls.Add(this.lblNpcMax);
            this.pnlNPC.Controls.Add(this.lblNpcStartStatus);
            this.pnlNPC.Controls.Add(this.lblStatusCounter);
            this.pnlNPC.Controls.Add(this.lblMinLevel);
            this.pnlNPC.Controls.Add(this.lblNpcSpawnX);
            this.pnlNPC.Controls.Add(this.lblNpcMin);
            this.pnlNPC.Controls.Add(this.lblNpcSpawnRate);
            this.pnlNPC.Controls.Add(this.lblNpcNum);
            this.pnlNPC.Controls.Add(this.lblNpcSpawnTime);
            this.pnlNPC.Location = new System.Drawing.Point(11, 69);
            this.pnlNPC.Name = "pnlNPC";
            this.pnlNPC.Size = new System.Drawing.Size(476, 413);
            this.pnlNPC.TabIndex = 1;
            this.pnlNPC.Visible = false;
            // 
            // pbHeader
            // 
            this.pbHeader.BackColor = System.Drawing.Color.Transparent;
            this.pbHeader.Location = new System.Drawing.Point(0, -1);
            this.pbHeader.Name = "pbHeader";
            this.pbHeader.Size = new System.Drawing.Size(501, 22);
            this.pbHeader.TabIndex = 2;
            this.pbHeader.TabStop = false;
            // 
            // lblNpcSpawnTime
            // 
            this.lblNpcSpawnTime.AutoSize = true;
            this.lblNpcSpawnTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblNpcSpawnTime.Location = new System.Drawing.Point(43, 35);
            this.lblNpcSpawnTime.Name = "lblNpcSpawnTime";
            this.lblNpcSpawnTime.Size = new System.Drawing.Size(86, 16);
            this.lblNpcSpawnTime.TabIndex = 4;
            this.lblNpcSpawnTime.Text = "Spawn Time:";
            // 
            // nudNpcSpawnTime
            // 
            this.nudNpcSpawnTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudNpcSpawnTime.Location = new System.Drawing.Point(43, 54);
            this.nudNpcSpawnTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNpcSpawnTime.Name = "nudNpcSpawnTime";
            this.nudNpcSpawnTime.Size = new System.Drawing.Size(86, 22);
            this.nudNpcSpawnTime.TabIndex = 5;
            this.nudNpcSpawnTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblNpcMin
            // 
            this.lblNpcMin.AutoSize = true;
            this.lblNpcMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblNpcMin.Location = new System.Drawing.Point(196, 35);
            this.lblNpcMin.Name = "lblNpcMin";
            this.lblNpcMin.Size = new System.Drawing.Size(67, 16);
            this.lblNpcMin.TabIndex = 4;
            this.lblNpcMin.Text = "Min Npcs:";
            // 
            // nudNpcMin
            // 
            this.nudNpcMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudNpcMin.Location = new System.Drawing.Point(196, 54);
            this.nudNpcMin.Name = "nudNpcMin";
            this.nudNpcMin.Size = new System.Drawing.Size(86, 22);
            this.nudNpcMin.TabIndex = 5;
            this.nudNpcMin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblNpcMax
            // 
            this.lblNpcMax.AutoSize = true;
            this.lblNpcMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblNpcMax.Location = new System.Drawing.Point(331, 35);
            this.lblNpcMax.Name = "lblNpcMax";
            this.lblNpcMax.Size = new System.Drawing.Size(71, 16);
            this.lblNpcMax.TabIndex = 4;
            this.lblNpcMax.Text = "Max Npcs:";
            // 
            // nudNpcMax
            // 
            this.nudNpcMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudNpcMax.Location = new System.Drawing.Point(331, 54);
            this.nudNpcMax.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNpcMax.Name = "nudNpcMax";
            this.nudNpcMax.Size = new System.Drawing.Size(86, 22);
            this.nudNpcMax.TabIndex = 5;
            this.nudNpcMax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblNpcNum
            // 
            this.lblNpcNum.AutoSize = true;
            this.lblNpcNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblNpcNum.Location = new System.Drawing.Point(43, 93);
            this.lblNpcNum.Name = "lblNpcNum";
            this.lblNpcNum.Size = new System.Drawing.Size(46, 16);
            this.lblNpcNum.TabIndex = 4;
            this.lblNpcNum.Text = "NPC #";
            // 
            // lblNpcSpawnX
            // 
            this.lblNpcSpawnX.AutoSize = true;
            this.lblNpcSpawnX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblNpcSpawnX.Location = new System.Drawing.Point(196, 93);
            this.lblNpcSpawnX.Name = "lblNpcSpawnX";
            this.lblNpcSpawnX.Size = new System.Drawing.Size(63, 16);
            this.lblNpcSpawnX.TabIndex = 4;
            this.lblNpcSpawnX.Text = "Spawn X:";
            // 
            // lblNpcSpawnY
            // 
            this.lblNpcSpawnY.AutoSize = true;
            this.lblNpcSpawnY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblNpcSpawnY.Location = new System.Drawing.Point(331, 93);
            this.lblNpcSpawnY.Name = "lblNpcSpawnY";
            this.lblNpcSpawnY.Size = new System.Drawing.Size(64, 16);
            this.lblNpcSpawnY.TabIndex = 4;
            this.lblNpcSpawnY.Text = "Spawn Y:";
            // 
            // nudNpcNum
            // 
            this.nudNpcNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudNpcNum.Location = new System.Drawing.Point(43, 112);
            this.nudNpcNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNpcNum.Name = "nudNpcNum";
            this.nudNpcNum.Size = new System.Drawing.Size(86, 22);
            this.nudNpcNum.TabIndex = 5;
            this.nudNpcNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudNpcSpawnX
            // 
            this.nudNpcSpawnX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudNpcSpawnX.Location = new System.Drawing.Point(196, 112);
            this.nudNpcSpawnX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nudNpcSpawnX.Name = "nudNpcSpawnX";
            this.nudNpcSpawnX.Size = new System.Drawing.Size(86, 22);
            this.nudNpcSpawnX.TabIndex = 5;
            // 
            // nudNpcSpawnY
            // 
            this.nudNpcSpawnY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudNpcSpawnY.Location = new System.Drawing.Point(331, 112);
            this.nudNpcSpawnY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nudNpcSpawnY.Name = "nudNpcSpawnY";
            this.nudNpcSpawnY.Size = new System.Drawing.Size(86, 22);
            this.nudNpcSpawnY.TabIndex = 5;
            // 
            // lblNpcSpawnRate
            // 
            this.lblNpcSpawnRate.AutoSize = true;
            this.lblNpcSpawnRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblNpcSpawnRate.Location = new System.Drawing.Point(43, 146);
            this.lblNpcSpawnRate.Name = "lblNpcSpawnRate";
            this.lblNpcSpawnRate.Size = new System.Drawing.Size(84, 16);
            this.lblNpcSpawnRate.TabIndex = 4;
            this.lblNpcSpawnRate.Text = "Spawn Rate:";
            // 
            // lblMinLevel
            // 
            this.lblMinLevel.AutoSize = true;
            this.lblMinLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblMinLevel.Location = new System.Drawing.Point(196, 146);
            this.lblMinLevel.Name = "lblMinLevel";
            this.lblMinLevel.Size = new System.Drawing.Size(68, 16);
            this.lblMinLevel.TabIndex = 4;
            this.lblMinLevel.Text = "Min Level:";
            // 
            // lblMaxLevel
            // 
            this.lblMaxLevel.AutoSize = true;
            this.lblMaxLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblMaxLevel.Location = new System.Drawing.Point(331, 146);
            this.lblMaxLevel.Name = "lblMaxLevel";
            this.lblMaxLevel.Size = new System.Drawing.Size(72, 16);
            this.lblMaxLevel.TabIndex = 4;
            this.lblMaxLevel.Text = "Max Level:";
            // 
            // nudNpcSpawnRate
            // 
            this.nudNpcSpawnRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudNpcSpawnRate.Location = new System.Drawing.Point(43, 165);
            this.nudNpcSpawnRate.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNpcSpawnRate.Name = "nudNpcSpawnRate";
            this.nudNpcSpawnRate.Size = new System.Drawing.Size(86, 22);
            this.nudNpcSpawnRate.TabIndex = 5;
            this.nudNpcSpawnRate.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudMinLevel
            // 
            this.nudMinLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudMinLevel.Location = new System.Drawing.Point(196, 165);
            this.nudMinLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMinLevel.Name = "nudMinLevel";
            this.nudMinLevel.Size = new System.Drawing.Size(86, 22);
            this.nudMinLevel.TabIndex = 5;
            this.nudMinLevel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudMaxLevel
            // 
            this.nudMaxLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudMaxLevel.Location = new System.Drawing.Point(331, 165);
            this.nudMaxLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMaxLevel.Name = "nudMaxLevel";
            this.nudMaxLevel.Size = new System.Drawing.Size(86, 22);
            this.nudMaxLevel.TabIndex = 5;
            this.nudMaxLevel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblStatusCounter
            // 
            this.lblStatusCounter.AutoSize = true;
            this.lblStatusCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblStatusCounter.Location = new System.Drawing.Point(196, 199);
            this.lblStatusCounter.Name = "lblStatusCounter";
            this.lblStatusCounter.Size = new System.Drawing.Size(97, 16);
            this.lblStatusCounter.TabIndex = 4;
            this.lblStatusCounter.Text = "Status Counter:";
            // 
            // lblStatusChance
            // 
            this.lblStatusChance.AutoSize = true;
            this.lblStatusChance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblStatusChance.Location = new System.Drawing.Point(331, 199);
            this.lblStatusChance.Name = "lblStatusChance";
            this.lblStatusChance.Size = new System.Drawing.Size(97, 16);
            this.lblStatusChance.TabIndex = 4;
            this.lblStatusChance.Text = "Status Chance:";
            // 
            // nudStatusCounter
            // 
            this.nudStatusCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudStatusCounter.Location = new System.Drawing.Point(196, 218);
            this.nudStatusCounter.Name = "nudStatusCounter";
            this.nudStatusCounter.Size = new System.Drawing.Size(86, 22);
            this.nudStatusCounter.TabIndex = 5;
            // 
            // nudStatusChance
            // 
            this.nudStatusChance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudStatusChance.Location = new System.Drawing.Point(331, 218);
            this.nudStatusChance.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudStatusChance.Name = "nudStatusChance";
            this.nudStatusChance.Size = new System.Drawing.Size(86, 22);
            this.nudStatusChance.TabIndex = 5;
            this.nudStatusChance.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbxNpcs
            // 
            this.lbxNpcs.BackColor = System.Drawing.SystemColors.Window;
            this.lbxNpcs.FormattingEnabled = true;
            this.lbxNpcs.Location = new System.Drawing.Point(3, 289);
            this.lbxNpcs.Name = "lbxNpcs";
            this.lbxNpcs.Size = new System.Drawing.Size(470, 121);
            this.lbxNpcs.TabIndex = 6;
            // 
            // lblNpcStartStatus
            // 
            this.lblNpcStartStatus.AutoSize = true;
            this.lblNpcStartStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblNpcStartStatus.Location = new System.Drawing.Point(43, 199);
            this.lblNpcStartStatus.Name = "lblNpcStartStatus";
            this.lblNpcStartStatus.Size = new System.Drawing.Size(96, 16);
            this.lblNpcStartStatus.TabIndex = 4;
            this.lblNpcStartStatus.Text = "Starting Status:";
            // 
            // cbNpcStartStatus
            // 
            this.cbNpcStartStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNpcStartStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.cbNpcStartStatus.FormattingEnabled = true;
            this.cbNpcStartStatus.Location = new System.Drawing.Point(43, 217);
            this.cbNpcStartStatus.Name = "cbNpcStartStatus";
            this.cbNpcStartStatus.Size = new System.Drawing.Size(86, 24);
            this.cbNpcStartStatus.TabIndex = 7;
            // 
            // btnAddNpc
            // 
            this.btnAddNpc.Location = new System.Drawing.Point(21, 260);
            this.btnAddNpc.Name = "btnAddNpc";
            this.btnAddNpc.Size = new System.Drawing.Size(75, 23);
            this.btnAddNpc.TabIndex = 8;
            this.btnAddNpc.Text = "Add Npc";
            this.btnAddNpc.UseVisualStyleBackColor = true;
            // 
            // btnRemoveNpc
            // 
            this.btnRemoveNpc.Location = new System.Drawing.Point(139, 260);
            this.btnRemoveNpc.Name = "btnRemoveNpc";
            this.btnRemoveNpc.Size = new System.Drawing.Size(80, 23);
            this.btnRemoveNpc.TabIndex = 8;
            this.btnRemoveNpc.Text = "Remove Npc";
            this.btnRemoveNpc.UseVisualStyleBackColor = true;
            // 
            // btnLoadNpc
            // 
            this.btnLoadNpc.Location = new System.Drawing.Point(264, 260);
            this.btnLoadNpc.Name = "btnLoadNpc";
            this.btnLoadNpc.Size = new System.Drawing.Size(75, 23);
            this.btnLoadNpc.TabIndex = 8;
            this.btnLoadNpc.Text = "Load Npc";
            this.btnLoadNpc.UseVisualStyleBackColor = true;
            // 
            // pnlScroll
            // 
            this.pnlScroll.BackColor = System.Drawing.Color.Transparent;
            this.pnlScroll.Controls.Add(this.nudMaxY);
            this.pnlScroll.Controls.Add(this.nudMaxX);
            this.pnlScroll.Controls.Add(this.lblMaxX);
            this.pnlScroll.Controls.Add(this.lblMaxY);
            this.pnlScroll.Location = new System.Drawing.Point(11, 69);
            this.pnlScroll.Name = "pnlScroll";
            this.pnlScroll.Size = new System.Drawing.Size(476, 413);
            this.pnlScroll.TabIndex = 1;
            this.pnlScroll.Visible = false;
            // 
            // nudMaxY
            // 
            this.nudMaxY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudMaxY.Location = new System.Drawing.Point(353, 28);
            this.nudMaxY.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudMaxY.Name = "nudMaxY";
            this.nudMaxY.Size = new System.Drawing.Size(86, 22);
            this.nudMaxY.TabIndex = 5;
            this.nudMaxY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudMaxX
            // 
            this.nudMaxX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.nudMaxX.Location = new System.Drawing.Point(46, 28);
            this.nudMaxX.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudMaxX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMaxX.Name = "nudMaxX";
            this.nudMaxX.Size = new System.Drawing.Size(86, 22);
            this.nudMaxX.TabIndex = 5;
            this.nudMaxX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblMaxX
            // 
            this.lblMaxX.AutoSize = true;
            this.lblMaxX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblMaxX.Location = new System.Drawing.Point(47, 9);
            this.lblMaxX.Name = "lblMaxX";
            this.lblMaxX.Size = new System.Drawing.Size(47, 16);
            this.lblMaxX.TabIndex = 4;
            this.lblMaxX.Text = "Max X:";
            // 
            // lblMaxY
            // 
            this.lblMaxY.AutoSize = true;
            this.lblMaxY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.lblMaxY.Location = new System.Drawing.Point(355, 9);
            this.lblMaxY.Name = "lblMaxY";
            this.lblMaxY.Size = new System.Drawing.Size(48, 16);
            this.lblMaxY.TabIndex = 4;
            this.lblMaxY.Text = "Max Y:";
            // 
            // WinProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Logic.Properties.Resources.menu_horizontal_border;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(500, 500);
            this.Controls.Add(this.pbHeader);
            this.Controls.Add(this.btnGeneral);
            this.Controls.Add(this.btnNPC);
            this.Controls.Add(this.btnScroll);
            this.Controls.Add(this.pnlGeneral);
            this.Controls.Add(this.pnlNPC);
            this.Controls.Add(this.pnlScroll);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WinProperties";
            this.Text = "winProperties";
            this.pnlGeneral.ResumeLayout(false);
            this.pnlGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPlay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInstanced)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecruit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHunger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIndoors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMapWest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMapEast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMapSouth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMapNorth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSouth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimeLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDarkness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNorth)).EndInit();
            this.pnlNPC.ResumeLayout(false);
            this.pnlNPC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcSpawnTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcSpawnX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcSpawnY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNpcSpawnRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStatusCounter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStatusChance)).EndInit();
            this.pnlScroll.ResumeLayout(false);
            this.pnlScroll.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxX)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGeneral;
        private System.Windows.Forms.Button btnNPC;
        private System.Windows.Forms.Button btnScroll;
        private System.Windows.Forms.Panel pnlGeneral;
        private System.Windows.Forms.Label lblMapName;
        private System.Windows.Forms.TextBox txtMapName;
        private System.Windows.Forms.NumericUpDown nudWest;
        private System.Windows.Forms.NumericUpDown nudEast;
        private System.Windows.Forms.NumericUpDown nudSouth;
        private System.Windows.Forms.NumericUpDown nudNorth;
        private System.Windows.Forms.Label lblWest;
        private System.Windows.Forms.Label lblEast;
        private System.Windows.Forms.Label lblSouth;
        private System.Windows.Forms.Label lblNorth;
        private System.Windows.Forms.Label lblGlobal;
        private System.Windows.Forms.Label lblMapSwitchover;
        private System.Windows.Forms.PictureBox pbMapNorth;
        private System.Windows.Forms.PictureBox pbMapWest;
        private System.Windows.Forms.PictureBox pbMapEast;
        private System.Windows.Forms.PictureBox pbMapSouth;
        private System.Windows.Forms.ComboBox cmbMapWeather;
        private System.Windows.Forms.ComboBox cmbMapMorality;
        private System.Windows.Forms.NumericUpDown nudTimeLimit;
        private System.Windows.Forms.NumericUpDown nudDarkness;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblDarkness;
        private System.Windows.Forms.Label lblWeather;
        private System.Windows.Forms.Label lblMoral;
        private Extensions.CheckBoxImage chkInstanced;
        private Extensions.CheckBoxImage chkExp;
        private Extensions.CheckBoxImage chkRecruit;
        private Extensions.CheckBoxImage chkHunger;
        private Extensions.CheckBoxImage chkIndoors;
        private System.Windows.Forms.ComboBox cmbMusic;
        private System.Windows.Forms.Label lblIndoors;
        private System.Windows.Forms.Label lblInstanced;
        private System.Windows.Forms.Label lblHunger;
        private System.Windows.Forms.Label lblExp;
        private System.Windows.Forms.Label lblRecruit;
        private System.Windows.Forms.Label lblMusic;
        private System.Windows.Forms.PictureBox btnPlay;
        private System.Windows.Forms.PictureBox btnStop;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Panel pnlNPC;
        private System.Windows.Forms.PictureBox pbHeader;
        private System.Windows.Forms.Label lblNpcSpawnTime;
        private System.Windows.Forms.NumericUpDown nudNpcSpawnTime;
        private System.Windows.Forms.Button btnLoadNpc;
        private System.Windows.Forms.Button btnRemoveNpc;
        private System.Windows.Forms.Button btnAddNpc;
        private System.Windows.Forms.ComboBox cbNpcStartStatus;
        private System.Windows.Forms.ListBox lbxNpcs;
        private System.Windows.Forms.NumericUpDown nudStatusChance;
        private System.Windows.Forms.NumericUpDown nudMaxLevel;
        private System.Windows.Forms.NumericUpDown nudNpcSpawnY;
        private System.Windows.Forms.NumericUpDown nudNpcMax;
        private System.Windows.Forms.NumericUpDown nudStatusCounter;
        private System.Windows.Forms.NumericUpDown nudMinLevel;
        private System.Windows.Forms.NumericUpDown nudNpcSpawnX;
        private System.Windows.Forms.NumericUpDown nudNpcMin;
        private System.Windows.Forms.NumericUpDown nudNpcSpawnRate;
        private System.Windows.Forms.NumericUpDown nudNpcNum;
        private System.Windows.Forms.Label lblStatusChance;
        private System.Windows.Forms.Label lblMaxLevel;
        private System.Windows.Forms.Label lblNpcSpawnY;
        private System.Windows.Forms.Label lblNpcMax;
        private System.Windows.Forms.Label lblNpcStartStatus;
        private System.Windows.Forms.Label lblStatusCounter;
        private System.Windows.Forms.Label lblMinLevel;
        private System.Windows.Forms.Label lblNpcSpawnX;
        private System.Windows.Forms.Label lblNpcMin;
        private System.Windows.Forms.Label lblNpcSpawnRate;
        private System.Windows.Forms.Label lblNpcNum;
        private System.Windows.Forms.Panel pnlScroll;
        private System.Windows.Forms.NumericUpDown nudMaxY;
        private System.Windows.Forms.NumericUpDown nudMaxX;
        private System.Windows.Forms.Label lblMaxX;
        private System.Windows.Forms.Label lblMaxY;
    }
}