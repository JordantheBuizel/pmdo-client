﻿// <copyright file="winMovePanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Graphic;
    using Graphic.Renderers.Moves;
    using Network;
    using PMU.Core;

    public partial class WinMovePanel : Form
    {
        private bool drag = false; // determine if we should be moving the form
        private Point startPoint = new Point(0, 0); // also for the moving

        private readonly Graphics graphics;
        private int moveNum = -1;
        private IMoveAnimation moveAnim;

        private List<string> moveList = new List<string>();

        private static WinMovePanel instance;

        public static WinMovePanel Instance
        {
            get
            {
                return instance;
            }
        }

        public WinMovePanel()
        {
            this.InitializeComponent();

            this.pbHeader.MouseDown += new MouseEventHandler(this.Title_MouseDown);
            this.pbHeader.MouseUp += new MouseEventHandler(this.Title_MouseUp);
            this.pbHeader.MouseMove += new MouseEventHandler(this.Title_MouseMove);

            this.nudMaxPP.Minimum = 0;
            this.nudMaxPP.Maximum = int.MaxValue;

            this.nudRange.Minimum = 0;
            this.nudRange.Maximum = int.MaxValue;

            this.nudKeyItem.Minimum = 0;
            this.nudKeyItem.Maximum = MaxInfo.MaxItems;

            this.nudData1.Minimum = int.MinValue;
            this.nudData1.Maximum = int.MaxValue;

            this.nudData2.Minimum = int.MinValue;
            this.nudData2.Maximum = int.MaxValue;

            this.nudData3.Minimum = int.MinValue;
            this.nudData3.Maximum = int.MaxValue;

            this.nudAccuracy.Minimum = -1;
            this.nudAccuracy.Maximum = int.MaxValue;

            this.nudHitTime.Minimum = 1;
            this.nudHitTime.Maximum = int.MaxValue;

            this.nudEffect1.Minimum = int.MinValue;
            this.nudEffect1.Maximum = int.MaxValue;

            this.nudEffect2.Minimum = int.MinValue;
            this.nudEffect2.Maximum = int.MaxValue;

            this.nudEffect3.Minimum = int.MinValue;
            this.nudEffect3.Maximum = int.MaxValue;

            this.nudSound.Minimum = 0;
            this.nudSound.Maximum = int.MaxValue;

            this.nudAtkAnimation.Minimum = int.MinValue;
            this.nudAtkAnimation.Maximum = int.MaxValue;

            this.nudAtkFrameTime.Minimum = 1;
            this.nudAtkFrameTime.Maximum = 1000;

            this.nudAtkCycles.Minimum = 1;
            this.nudAtkCycles.Maximum = 10;

            this.nudTrvAnimation.Minimum = int.MinValue;
            this.nudTrvAnimation.Maximum = int.MaxValue;

            this.nudTrvFrameTime.Minimum = 1;
            this.nudTrvFrameTime.Maximum = 1000;

            this.nudTrvCycles.Minimum = 1;
            this.nudTrvCycles.Maximum = 10;

            this.nudDefAnimation.Minimum = int.MinValue;
            this.nudDefAnimation.Maximum = int.MaxValue;

            this.nudDefFrameTime.Minimum = 1;
            this.nudDefFrameTime.Maximum = 1000;

            this.nudDefCycles.Minimum = 1;
            this.nudDefCycles.Maximum = 10;

            this.tbFind.KeyDown += new KeyEventHandler(this.TbFind_KeyDown);

            this.tmrPreview.Tick += new EventHandler(this.TmrPreview_Elapsed);

            this.cbxEffect.DataSource = Enum.GetValues(typeof(Enums.MoveType));
            this.cbxCategory.DataSource = Enum.GetValues(typeof(Enums.MoveCategory));
            this.cbxElement.DataSource = Enum.GetValues(typeof(Enums.PokemonType));
            this.cbxTarget.DataSource = Enum.GetValues(typeof(Enums.MoveTarget));
            this.cbxRangeType.DataSource = Enum.GetValues(typeof(Enums.MoveRange));
            this.cbxTrvType.DataSource = Enum.GetValues(typeof(Enums.MoveAnimationType));

            for (int i = 1; i < MaxInfo.MaxMoves; i++)
            {
                if (Moves.MoveHelper.Moves[i] != null)
                {
                    this.moveList.Add(i + ": " + Moves.MoveHelper.Moves[i].Name);
                }
                else
                {
                    this.moveList.Add(i + ": " + "-------");
                }
            }

            this.lbxMoveList.DataSource = this.moveList;

            this.graphics = this.pbPreview.CreateGraphics();

            this.Size = new Size(211, 304);
        }

        private void TbFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                int currentSelected = (this.lbxMoveList.SelectedItem != null) ? this.lbxMoveList.SelectedIndex : 0;

                for (int i = (currentSelected == 1) ? 1 : currentSelected + 1; i < this.lbxMoveList.Items.Count; i++)
                {
                    if (this.lbxMoveList.GetItemText(this.lbxMoveList.Items[i]).ToLower().Contains(this.tbFind.Text.ToLower()))
                    {
                        this.lbxMoveList.SetSelected(i, true);
                        break;
                    }
                }
            }
        }

        /// <inheritdoc/>
        protected override void OnLoad(EventArgs e)
        {
            instance = this;
            base.OnLoad(e);
        }

        /// <inheritdoc/>
        protected override void OnClosed(EventArgs e)
        {
            instance = null;
            base.OnClosed(e);
        }

        public void LoadMove(string[] parse)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<string[]>(this.LoadMove), new object[] { parse });
                return;
            }

            this.pnlMovesView.Visible = false;
            this.pnlMoveEdit.Visible = true;
            this.Size = new Size(564, 440);

            int currentIndex = 2;
            try
            {
                this.txtMoveName.Text = parse[2];
                currentIndex++;
                this.nudMaxPP.Value = parse[3].ToInt();
                currentIndex++;
                this.cbxEffect.SelectedIndex = parse[4].ToInt();
                currentIndex++;
                this.cbxElement.SelectedIndex = this.cbxElement.FindStringExact(Enum.GetName(typeof(Enums.PokemonType), parse[5].ToInt()));
                currentIndex++;
                this.cbxCategory.SelectedIndex = this.cbxCategory.FindStringExact(Enum.GetName(typeof(Enums.MoveCategory), parse[6].ToInt()));
                currentIndex++;
                this.cbxRangeType.SelectedIndex = this.cbxRangeType.FindStringExact(Enum.GetName(typeof(Enums.MoveRange), parse[7].ToInt()));
                currentIndex++;
                this.nudRange.Value = parse[8].ToInt();
                currentIndex++;
                this.cbxTarget.SelectedIndex = this.cbxTarget.FindStringExact(Enum.GetName(typeof(Enums.MoveTarget), parse[9].ToInt()));
                currentIndex++;
                this.nudData1.Value = parse[10].ToInt();
                currentIndex++;
                this.nudData2.Value = parse[11].ToInt();
                currentIndex++;
                this.nudData3.Value = parse[12].ToInt();
                currentIndex++;
                this.nudAccuracy.Value = parse[13].ToInt();
                currentIndex++;
                this.nudHitTime.Value = parse[14].ToInt();
                currentIndex++;
                this.cbiHitFreeze.Checked = parse[15].ToBool();
                currentIndex++;
                this.nudEffect1.Value = parse[16].ToInt();
                currentIndex++;
                this.nudEffect2.Value = parse[17].ToInt();
                currentIndex++;
                this.nudEffect3.Value = parse[18].ToInt();
                currentIndex++;
                this.cbiPerPlayer.Checked = parse[19].ToBool();
                currentIndex++;
                this.nudKeyItem.Value = parse[20].ToInt();
                currentIndex++;
                this.nudSound.Value = parse[21].ToInt();
                currentIndex += 2;

                // attackeranimationtype - 22
                this.nudAtkAnimation.Value = parse[23].ToInt();
                currentIndex++;
                this.nudAtkFrameTime.Value = parse[24].ToInt();
                currentIndex++;
                this.nudAtkCycles.Value = parse[25].ToInt();
                currentIndex++;

                this.cbxTrvType.SelectedIndex = this.cbxTrvType.FindStringExact(Enum.GetName(typeof(Enums.MoveAnimationType), parse[26].ToInt()));
                currentIndex++;
                this.nudTrvAnimation.Value = parse[27].ToInt();
                currentIndex++;
                this.nudTrvFrameTime.Value = parse[28].ToInt();
                currentIndex++;
                this.nudTrvCycles.Value = parse[29].ToInt();
                currentIndex += 2;

                // defenderanimationtype - 30
                this.nudDefAnimation.Value = parse[31].ToInt();
                currentIndex++;
                this.nudDefFrameTime.Value = parse[32].ToInt();
                currentIndex++;
                this.nudDefCycles.Value = parse[33].ToInt();
            }
            catch (ArgumentOutOfRangeException)
            {
                parse[currentIndex] = 1.ToString();
                this.LoadMove(parse);
            }

            this.RefreshMoveList();

            this.btnEdit.Text = "Edit";
        }

        private void RefreshMoveList()
        {
            this.moveList = new List<string>();

            for (int i = 1; i < MaxInfo.MaxMoves; i++)
            {
                if (Moves.MoveHelper.Moves[i] != null)
                {
                    this.moveList.Add(i + ": " + Moves.MoveHelper.Moves[i].Name);
                }
                else
                {
                    this.moveList.Add(i + ": " + "-------");
                }
            }

            this.lbxMoveList.DataSource = this.moveList;
        }

        private void ShowPreview()
        {
            if (this.pnlAttacker.Visible)
            {
                this.moveAnim = new NormalMoveAnimation(2, 2);
                this.moveAnim.AnimationIndex = (int)this.nudAtkAnimation.Value;
                this.moveAnim.MoveTime = (int)this.nudAtkFrameTime.Value;
                this.moveAnim.RenderLoops = (int)this.nudAtkCycles.Value;
                this.tmrPreview.Interval = (int)this.nudAtkFrameTime.Value;
            }
            else if (this.pnlDefender.Visible)
            {
                this.moveAnim = new NormalMoveAnimation(2, 2);
                this.moveAnim.AnimationIndex = (int)this.nudDefAnimation.Value;
                this.moveAnim.MoveTime = (int)this.nudDefFrameTime.Value;
                this.moveAnim.RenderLoops = (int)this.nudDefCycles.Value;
                this.tmrPreview.Interval = (int)this.nudDefFrameTime.Value;
            }
            else
            {
                switch ((Enums.MoveAnimationType)this.cbxTrvType.SelectedIndex)
                {
                    case Enums.MoveAnimationType.Normal:
                        {
                            this.moveAnim = new NormalMoveAnimation(2, 2);
                        }

                        break;
                    case Enums.MoveAnimationType.Overlay:
                        {
                            this.moveAnim = new OverlayMoveAnimation();
                        }

                        break;
                    case Enums.MoveAnimationType.Tile:
                        {
                            this.moveAnim = new TileMoveAnimation(2, 2, (Enums.MoveRange)this.cbxRangeType.SelectedIndex, Enums.Direction.Right, 3);
                        }

                        break;
                    case Enums.MoveAnimationType.Arrow:
                        {
                            this.moveAnim = new ArrowMoveAnimation(1, 2, Enums.Direction.Right, 4);
                        }

                        break;
                    case Enums.MoveAnimationType.Throw:
                        {
                            this.moveAnim = new ThrowMoveAnimation(1, 4, 3, -1);
                        }

                        break;
                    case Enums.MoveAnimationType.Beam:
                        {
                            this.moveAnim = new BeamMoveAnimation(1, 2, Enums.Direction.Right, 4);
                        }

                        break;
                    case Enums.MoveAnimationType.ItemArrow:
                        {
                            this.moveAnim = new ItemArrowMoveAnimation(1, 2, Enums.Direction.Right, 4);
                        }

                        break;
                    case Enums.MoveAnimationType.ItemThrow:
                        {
                            this.moveAnim = new ItemThrowMoveAnimation(1, 4, 3, -1);
                        }

                        break;
                }

                this.moveAnim.AnimationIndex = (int)this.nudTrvAnimation.Value;
                this.moveAnim.MoveTime = (int)this.nudTrvFrameTime.Value;
                this.moveAnim.RenderLoops = (int)this.nudTrvAnimation.Value;
                this.tmrPreview.Interval = (int)this.nudTrvFrameTime.Value;
            }

            Music.Music.AudioPlayer.PlaySoundEffect("magic" + this.nudSound.Value + ".wav");
            this.moveAnim.Active = true;
            this.moveAnim.Frame = 0;
            this.moveAnim.CompletedLoops = 0;
            this.RenderAnimationToPictureBox(this.moveAnim);
            this.tmrPreview.Start();
        }

        private void TmrPreview_Elapsed(object sender, EventArgs e)
        {
            // pbPreview.Image = new Bitmap(196, 128);
            this.RenderAnimationToPictureBox(this.moveAnim);
            if (!this.moveAnim.Active)
            {
                this.tmrPreview.Stop();
            }
        }

        private void RenderAnimationToPictureBox(IMoveAnimation animation)
        {
            if (animation.Active)
            {
                SdlDotNet.Graphics.Surface sdlSucks = new SdlDotNet.Graphics.Surface(196, 128);
                Graphic.Renderers.RendererDestinationData destData = new Graphic.Renderers.RendererDestinationData(sdlSucks, new Point(0, 0), sdlSucks.Size);
                MoveRenderer.RenderMoveAnimation(destData, animation, new Point(32 * animation.StartX, 32 * animation.StartY));

                this.graphics.DrawImage(sdlSucks.Bitmap, destData.Location);
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            if (this.lbxMoveList.SelectedItems.Count == 1)
            {
                string[] index = (this.lbxMoveList.SelectedValue as string).Split(':');
                if (index[0].IsNumeric())
                {
                    this.moveNum = index[0].ToInt();
                    this.btnEdit.Text = "Loading...";
                }

                Messenger.SendEditMove(this.moveNum);
            }
        }

        private void BtnViewerCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.moveNum = -1;
            this.pnlMoveEdit.Visible = false;
            this.pnlMovesView.Visible = true;
            this.Size = new Size(211, 304);
        }

        private void BtnAccept_Click(object sender, EventArgs e)
        {
            string[] moveToSend = new string[32];

            moveToSend[0] = this.txtMoveName.Text;
            moveToSend[1] = this.nudMaxPP.Value.ToString();

            moveToSend[2] = this.cbxEffect.SelectedIndex.ToString();
            moveToSend[3] = this.cbxElement.SelectedIndex.ToString();
            moveToSend[4] = this.cbxCategory.SelectedIndex.ToString();
            moveToSend[5] = this.cbxTarget.SelectedIndex.ToString();
            moveToSend[6] = this.cbxRangeType.SelectedIndex.ToString();

            moveToSend[7] = this.nudRange.Value.ToString();
            moveToSend[8] = this.nudData1.Value.ToString();
            moveToSend[9] = this.nudData2.Value.ToString();
            moveToSend[10] = this.nudData3.Value.ToString();
            moveToSend[11] = this.nudAccuracy.Value.ToString();
            moveToSend[12] = this.nudHitTime.Value.ToString();
            moveToSend[13] = this.cbiHitFreeze.Checked.ToIntString();
            moveToSend[14] = this.nudEffect1.Value.ToString();
            moveToSend[15] = this.nudEffect2.Value.ToString();
            moveToSend[16] = this.nudEffect3.Value.ToString();
            moveToSend[17] = this.cbiPerPlayer.Checked.ToIntString();

            moveToSend[18] = this.nudKeyItem.Value.ToString();
            moveToSend[19] = this.nudSound.Value.ToString();

            moveToSend[20] = string.Empty; // attacker animation type
            moveToSend[21] = this.nudAtkAnimation.Value.ToString();
            moveToSend[22] = this.nudAtkFrameTime.Value.ToString();
            moveToSend[23] = this.nudAtkCycles.Value.ToString();

            moveToSend[24] = this.cbxTrvType.SelectedIndex.ToString();
            moveToSend[25] = this.nudTrvAnimation.Value.ToString();
            moveToSend[26] = this.nudTrvFrameTime.Value.ToString();
            moveToSend[27] = this.nudTrvCycles.Value.ToString();

            moveToSend[28] = string.Empty; // defender animation type
            moveToSend[29] = this.nudDefAnimation.Value.ToString();
            moveToSend[30] = this.nudDefFrameTime.Value.ToString();
            moveToSend[31] = this.nudDefCycles.Value.ToString();

            Messenger.SendSaveMove(this.moveNum, moveToSend);
            this.moveNum = -1;
            this.pnlMoveEdit.Visible = false;
            this.pnlMovesView.Visible = true;
            this.Size = new Size(211, 304);
        }

        private void RenderMoveAnimation(IMoveAnimation animation, Point pinnedPoint)
        {
            int animTime = 400;
            switch (animation.AnimType)
            {
                case Enums.MoveAnimationType.Normal:
                    {
                        NormalMoveAnimation specifiedAnim = animation as NormalMoveAnimation;
                        if (specifiedAnim.CompletedLoops < specifiedAnim.RenderLoops)
                        {
                            SpellSheet spriteSheet = GraphicsManager.GetSpellSheet(Enums.StationaryAnimType.Spell, specifiedAnim.AnimationIndex, false);
                            Bitmap spriteToBlit = null;
                            if (spriteSheet != null)
                            {
                                spriteToBlit = spriteSheet.Sheet.Bitmap;
                            }
                            else
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                specifiedAnim.Frame * spriteToBlit.Height,
                                0, spriteToBlit.Height, spriteToBlit.Height);

                            pinnedPoint.X = pinnedPoint.X + (Constants.TILEWIDTH / 2) - (spriteToBlit.Height / 2);
                            pinnedPoint.Y = pinnedPoint.Y + (Constants.TILEHEIGHT / 2) - (spriteToBlit.Height / 2);

                            Point[] points = new Point[3]
                            {
                                pinnedPoint,
                                new Point(pinnedPoint.X, pinnedPoint.Y + sourceRec.Height),
                                new Point(pinnedPoint.X + sourceRec.Width, pinnedPoint.Y)
                            };

                            // blit
                            this.graphics.DrawImage(spriteToBlit, points, sourceRec, GraphicsUnit.Pixel);

                            if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength)
                            {
                                specifiedAnim.MoveTime = Globals.Tick;
                                specifiedAnim.Frame++;
                            }

                            if (specifiedAnim.Frame >= spriteToBlit.Width / spriteToBlit.Height)
                            {
                                specifiedAnim.CompletedLoops++;
                                specifiedAnim.Frame = 0;
                            }
                        }
                        else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.Arrow:
                    {
                        ArrowMoveAnimation specifiedAnim = animation as ArrowMoveAnimation;
                        int time = Globals.Tick - specifiedAnim.TotalMoveTime;
                        if (time < animTime)
                        {
                            SpellSheet spriteSheet = GraphicsManager.GetSpellSheet(Enums.StationaryAnimType.Arrow, specifiedAnim.AnimationIndex, false);
                            Bitmap spriteToBlit = null;
                            if (spriteSheet != null)
                            {
                                spriteToBlit = spriteSheet.Sheet.Bitmap;
                            }
                            else
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                specifiedAnim.Frame * spriteToBlit.Height / 8,
                                GraphicsManager.GetAnimDirInt(specifiedAnim.Direction) * spriteToBlit.Height / 8, spriteToBlit.Height / 8, spriteToBlit.Height / 8);

                            pinnedPoint.X = pinnedPoint.X + (Constants.TILEWIDTH / 2) - (spriteToBlit.Height / 2 / 8);
                            pinnedPoint.Y = pinnedPoint.Y + (Constants.TILEHEIGHT / 2) - (spriteToBlit.Height / 2 / 8);

                            switch (specifiedAnim.Direction)
                            {
                                case Enums.Direction.Up:
                                    {
                                        pinnedPoint.Y -= specifiedAnim.Distance * Constants.TILEHEIGHT * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.Down:
                                    {
                                        pinnedPoint.Y += specifiedAnim.Distance * Constants.TILEHEIGHT * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.Left:
                                    {
                                        pinnedPoint.X -= specifiedAnim.Distance * Constants.TILEWIDTH * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.Right:
                                    {
                                        pinnedPoint.X += specifiedAnim.Distance * Constants.TILEWIDTH * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.UpRight:
                                case Enums.Direction.DownRight:
                                case Enums.Direction.DownLeft:
                                case Enums.Direction.UpLeft:
                                    break;
                            }

                            Point[] points = new Point[3]
                            {
                                pinnedPoint,
                                new Point(pinnedPoint.X, pinnedPoint.Y + sourceRec.Height),
                                new Point(pinnedPoint.X + sourceRec.Width, pinnedPoint.Y)
                            };

                            // blit
                            this.graphics.DrawImage(spriteToBlit, points, sourceRec, GraphicsUnit.Pixel);

                            if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength)
                            {
                                specifiedAnim.MoveTime = Globals.Tick;
                                specifiedAnim.Frame++;
                            }

                            if (specifiedAnim.Frame >= spriteToBlit.Width / (spriteToBlit.Height / 8))
                            {
                                specifiedAnim.CompletedLoops++;
                                specifiedAnim.Frame = 0;
                            }
                        }
                        else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.Throw:
                    {
                        ThrowMoveAnimation specifiedAnim = animation as ThrowMoveAnimation;
                        int time = Globals.Tick - specifiedAnim.TotalMoveTime;
                        if (time < animTime)
                        {
                            SpellSheet spriteSheet = GraphicsManager.GetSpellSheet(Enums.StationaryAnimType.Spell, specifiedAnim.AnimationIndex, false);
                            Bitmap spriteToBlit = null;
                            if (spriteSheet != null)
                            {
                                spriteToBlit = spriteSheet.Sheet.Bitmap;
                            }
                            else
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                specifiedAnim.Frame * spriteToBlit.Height,
                                0, spriteToBlit.Height, spriteToBlit.Height);

                            double distance = Math.Sqrt(Math.Pow(specifiedAnim.XChange * Constants.TILEWIDTH, 2) + Math.Pow(specifiedAnim.YChange * Constants.TILEHEIGHT, 2));

                            int x = pinnedPoint.X + (specifiedAnim.XChange * Constants.TILEWIDTH * time / animTime);
                            int y = (int)(pinnedPoint.Y + (specifiedAnim.YChange * Constants.TILEHEIGHT * time / animTime) - ((((-4) * distance * Math.Pow(time, 2) / animTime) + (4 * distance * time)) / animTime));

                            x = x + (Constants.TILEWIDTH / 2) - (spriteToBlit.Height / 2);
                            y = y + (Constants.TILEHEIGHT / 2) - (spriteToBlit.Height / 2);

                            Point[] points = new Point[3]
                            {
                                new Point(x, y),
                                new Point(x, y + sourceRec.Height),
                                new Point(x + sourceRec.Width, y)
                            };

                            // blit
                            this.graphics.DrawImage(spriteToBlit, points, sourceRec, GraphicsUnit.Pixel);

                            if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength)
                            {
                                specifiedAnim.MoveTime = Globals.Tick;
                                specifiedAnim.Frame++;
                            }

                            if (specifiedAnim.Frame >= spriteToBlit.Width / spriteToBlit.Height)
                            {
                                specifiedAnim.CompletedLoops++;
                                specifiedAnim.Frame = 0;
                            }
                        }
                        else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.Beam:
                    {
                        BeamMoveAnimation specifiedAnim = animation as BeamMoveAnimation;
                        if (specifiedAnim.CompletedLoops < specifiedAnim.RenderLoops + specifiedAnim.Distance)
                        {
                            SpellSheet spriteSheet = GraphicsManager.GetSpellSheet(Enums.StationaryAnimType.Beam, specifiedAnim.AnimationIndex, false);
                            Bitmap spriteToBlit = null;
                            if (spriteSheet != null)
                            {
                                spriteToBlit = spriteSheet.Sheet.Bitmap;
                            }
                            else
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            int curDistance = specifiedAnim.Distance;
                            Rectangle sourceRec = default(Rectangle);
                            if (specifiedAnim.CompletedLoops < specifiedAnim.Distance)
                            {
                                curDistance = specifiedAnim.CompletedLoops;
                            }

                            for (int i = 0; i <= curDistance; i++)
                            {
                                if (i == 0)
                                {
                                    // draw beginning
                                    sourceRec = new Rectangle(
                                        specifiedAnim.Frame * spriteToBlit.Height / 32,
                                GraphicsManager.GetAnimDirInt(specifiedAnim.Direction) * 4 * spriteToBlit.Height / 32, spriteToBlit.Height / 32, spriteToBlit.Height / 32);
                                }
                                else if (i == curDistance)
                                {
                                    if (curDistance == specifiedAnim.Distance)
                                    {
                                        sourceRec = new Rectangle(
                                            specifiedAnim.Frame * spriteToBlit.Height / 32,
                                ((GraphicsManager.GetAnimDirInt(specifiedAnim.Direction) * 4) + 3) * spriteToBlit.Height / 32, spriteToBlit.Height / 32, spriteToBlit.Height / 32);
                                    }
                                    else
                                    {
                                        sourceRec = new Rectangle(
                                            specifiedAnim.Frame * spriteToBlit.Height / 32,
                                ((GraphicsManager.GetAnimDirInt(specifiedAnim.Direction) * 4) + 2) * spriteToBlit.Height / 32, spriteToBlit.Height / 32, spriteToBlit.Height / 32);
                                    }
                                }
                                else
                                {
                                    // draw body
                                    sourceRec = new Rectangle(
                                        specifiedAnim.Frame * spriteToBlit.Height / 32,
                                ((GraphicsManager.GetAnimDirInt(specifiedAnim.Direction) * 4) + 1) * spriteToBlit.Height / 32, spriteToBlit.Height / 32, spriteToBlit.Height / 32);
                                }

                                Point blitPoint = default(Point);

                                switch (specifiedAnim.Direction)
                                {
                                    case Enums.Direction.Up:
                                        {
                                            blitPoint = new Point(pinnedPoint.X, pinnedPoint.Y - (i * Constants.TILEHEIGHT));
                                        }

                                        break;
                                    case Enums.Direction.Down:
                                        {
                                            blitPoint = new Point(pinnedPoint.X, pinnedPoint.Y + (i * Constants.TILEHEIGHT));
                                        }

                                        break;
                                    case Enums.Direction.Left:
                                        {
                                            blitPoint = new Point(pinnedPoint.X - (i * Constants.TILEWIDTH), pinnedPoint.Y);
                                        }

                                        break;
                                    case Enums.Direction.Right:
                                        {
                                            blitPoint = new Point(pinnedPoint.X + (i * Constants.TILEWIDTH), pinnedPoint.Y);
                                        }

                                        break;
                                    case Enums.Direction.UpRight:
                                    case Enums.Direction.DownRight:
                                    case Enums.Direction.DownLeft:
                                    case Enums.Direction.UpLeft:
                                        break;
                                }

                                blitPoint.X = blitPoint.X + (Constants.TILEWIDTH / 2) - (spriteToBlit.Height / 2 / 32);
                                blitPoint.Y = blitPoint.Y + (Constants.TILEHEIGHT / 2) - (spriteToBlit.Height / 2 / 32);

                                Point[] points = new Point[3]
                            {
                                pinnedPoint,
                                new Point(pinnedPoint.X, pinnedPoint.Y + sourceRec.Height),
                                new Point(pinnedPoint.X + sourceRec.Width, pinnedPoint.Y)
                            };

                                // blit
                                this.graphics.DrawImage(spriteToBlit, points, sourceRec, GraphicsUnit.Pixel);
                            }

                            if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength)
                            {
                                specifiedAnim.MoveTime = Globals.Tick;
                                specifiedAnim.Frame++;
                            }

                            if (specifiedAnim.Frame >= spriteToBlit.Width / (spriteToBlit.Height / 32))
                            {
                                specifiedAnim.CompletedLoops++;
                                specifiedAnim.Frame = 0;
                            }
                        }
                        else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.Overlay:
                    {
                        OverlayMoveAnimation specifiedAnim = animation as OverlayMoveAnimation;
                        if (specifiedAnim.CompletedLoops < specifiedAnim.RenderLoops)
                        {
                            SpellSheet spriteSheet = GraphicsManager.GetSpellSheet(Enums.StationaryAnimType.Spell, specifiedAnim.AnimationIndex, true);
                            Bitmap spriteToBlit = null;
                            if (spriteSheet != null)
                            {
                                spriteToBlit = spriteSheet.Sheet.Bitmap;
                            }
                            else
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                specifiedAnim.Frame * spriteToBlit.Height,
                                0, spriteToBlit.Height, spriteToBlit.Height);

                            // blit
                            for (int y = 0; y < Constants.TILEHEIGHT * 15; y += spriteToBlit.Height)
                            {
                                for (int x = 0; x < Constants.TILEWIDTH * 20; x += spriteToBlit.Height)
                                {
                                    Point[] points = new Point[3]
                            {
                                pinnedPoint,
                                new Point(pinnedPoint.X, pinnedPoint.Y + sourceRec.Height),
                                new Point(pinnedPoint.X + sourceRec.Width, pinnedPoint.Y)
                            };

                                    // blit
                                    this.graphics.DrawImage(spriteToBlit, points, sourceRec, GraphicsUnit.Pixel);
                                }
                            }

                            if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength)
                            {
                                specifiedAnim.MoveTime = Globals.Tick;
                                specifiedAnim.Frame++;
                            }

                            if (specifiedAnim.Frame >= spriteToBlit.Width / spriteToBlit.Height)
                            {
                                specifiedAnim.CompletedLoops++;
                                specifiedAnim.Frame = 0;
                            }
                        }
                        else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.Tile:
                    {
                        TileMoveAnimation specifiedAnim = animation as TileMoveAnimation;
                        if (specifiedAnim.CompletedLoops < specifiedAnim.RenderLoops)
                        {
                            SpellSheet spriteSheet = GraphicsManager.GetSpellSheet(Enums.StationaryAnimType.Spell, specifiedAnim.AnimationIndex, false);
                            Bitmap spriteToBlit = null;
                            if (spriteSheet != null)
                            {
                                spriteToBlit = spriteSheet.Sheet.Bitmap;
                            }
                            else
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                specifiedAnim.Frame * spriteToBlit.Height,
                                0, spriteToBlit.Height, spriteToBlit.Height);

                            Point blitPoint = new Point(pinnedPoint.X + (Constants.TILEWIDTH / 2) - (spriteToBlit.Height / 2), pinnedPoint.Y + (Constants.TILEHEIGHT / 2) - (spriteToBlit.Height / 2));

                            // blit
                            switch (specifiedAnim.RangeType)
                            {
                                case Enums.MoveRange.FrontOfUserUntil:
                                case Enums.MoveRange.LineUntilHit:
                                    {
                                        switch (specifiedAnim.Direction)
                                        {
                                            case Enums.Direction.Up:
                                                {
                                                    int y = specifiedAnim.StartY;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        Point[] points = new Point[3]
                            {
                                new Point(blitPoint.X, blitPoint.Y - (Constants.TILEHEIGHT * i)),
                                new Point(blitPoint.X, blitPoint.Y - (Constants.TILEHEIGHT * i) + sourceRec.Height),
                                new Point(blitPoint.X + sourceRec.Width, blitPoint.Y - (Constants.TILEHEIGHT * i))
                            };

                                                        // blit
                                                        this.graphics.DrawImage(spriteToBlit, points, sourceRec, GraphicsUnit.Pixel);

                                                        if (MoveRenderer.IsRenderingTargetOnSprite(
                                                            Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX, specifiedAnim.StartY - i))
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }

                                                break;
                                            case Enums.Direction.Down:
                                                {
                                                    int y = specifiedAnim.StartY;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        Point[] points = new Point[3]
                            {
                                new Point(blitPoint.X, blitPoint.Y + (Constants.TILEHEIGHT * i)),
                                new Point(blitPoint.X, blitPoint.Y + (Constants.TILEHEIGHT * i) + sourceRec.Height),
                                new Point(blitPoint.X + sourceRec.Width, blitPoint.Y + (Constants.TILEHEIGHT * i))
                            };

                                                        // blit
                                                        this.graphics.DrawImage(spriteToBlit, points, sourceRec, GraphicsUnit.Pixel);

                                                        if (MoveRenderer.IsRenderingTargetOnSprite(
                                                            Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX, specifiedAnim.StartY + i))
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }

                                                break;
                                            case Enums.Direction.Left:
                                                {
                                                    int x = specifiedAnim.StartX;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        this.graphics.DrawImage(spriteToBlit, blitPoint.X - (Constants.TILEWIDTH * i), blitPoint.Y, sourceRec, GraphicsUnit.Pixel);

                                                        if (MoveRenderer.IsRenderingTargetOnSprite(
                                                            Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX - i, specifiedAnim.StartY))
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }

                                                break;
                                            case Enums.Direction.Right:
                                                {
                                                    int x = specifiedAnim.StartX;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        this.graphics.DrawImage(spriteToBlit, blitPoint.X + (Constants.TILEWIDTH * i), blitPoint.Y, sourceRec, GraphicsUnit.Pixel);

                                                        if (MoveRenderer.IsRenderingTargetOnSprite(
                                                            Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX + i, specifiedAnim.StartY))
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }

                                                break;
                                        }
                                    }

                                    break;
                                case Enums.MoveRange.StraightLine:
                                case Enums.MoveRange.FrontOfUser:
                                    {
                                        switch (specifiedAnim.Direction)
                                        {
                                            case Enums.Direction.Up:
                                                {
                                                    int y = specifiedAnim.StartY;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        this.graphics.DrawImage(spriteToBlit, blitPoint.X, blitPoint.Y - (Constants.TILEHEIGHT * i), sourceRec, GraphicsUnit.Pixel);
                                                    }
                                                }

                                                break;
                                            case Enums.Direction.Down:
                                                {
                                                    int y = specifiedAnim.StartY;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        this.graphics.DrawImage(spriteToBlit, blitPoint.X, blitPoint.Y + (Constants.TILEHEIGHT * i), sourceRec, GraphicsUnit.Pixel);
                                                    }
                                                }

                                                break;
                                            case Enums.Direction.Left:
                                                {
                                                    int x = specifiedAnim.StartX;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        this.graphics.DrawImage(spriteToBlit, blitPoint.X - (Constants.TILEWIDTH * i), blitPoint.Y, sourceRec, GraphicsUnit.Pixel);
                                                    }
                                                }

                                                break;
                                            case Enums.Direction.Right:
                                                {
                                                    int x = specifiedAnim.StartX;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        this.graphics.DrawImage(spriteToBlit, blitPoint.X + (Constants.TILEWIDTH * i), blitPoint.Y, sourceRec, GraphicsUnit.Pixel);
                                                    }
                                                }

                                                break;
                                        }
                                    }

                                    break;
                                case Enums.MoveRange.User:
                                case Enums.MoveRange.Special:
                                    {
                                        this.graphics.DrawImage(spriteToBlit, blitPoint.X, blitPoint.Y, sourceRec, GraphicsUnit.Pixel);
                                    }

                                    break;
                                case Enums.MoveRange.Floor:
                                    {
                                        for (int x = 0; x < 20; x++)
                                        {
                                            for (int y = 0; y < 15; y++)
                                            {
                                                this.graphics.DrawImage(spriteToBlit, (x * Constants.TILEWIDTH) + (Constants.TILEWIDTH / 2) - (spriteToBlit.Height / 2), (y * Constants.TILEHEIGHT) + (Constants.TILEHEIGHT / 2) - (spriteToBlit.Height / 2), sourceRec, GraphicsUnit.Pixel);
                                            }
                                        }
                                    }

                                    break;
                                case Enums.MoveRange.Room:
                                    {
                                        for (int x = -specifiedAnim.Range; x <= specifiedAnim.Range; x++)
                                        {
                                            for (int y = -specifiedAnim.Range; y <= specifiedAnim.Range; y++)
                                            {
                                                this.graphics.DrawImage(spriteToBlit, blitPoint.X + (x * Constants.TILEWIDTH), blitPoint.Y + (y * Constants.TILEHEIGHT), sourceRec, GraphicsUnit.Pixel);
                                            }
                                        }
                                    }

                                    break;
                                case Enums.MoveRange.FrontAndSides:
                                    {
                                        for (int r = 0; r <= specifiedAnim.Range; r++)
                                        {
                                            // check adjacent tiles
                                            switch (specifiedAnim.Direction)
                                            {
                                                case Enums.Direction.Right:
                                                    {
                                                        this.graphics.DrawImage(spriteToBlit, blitPoint.X + (r * Constants.TILEWIDTH), blitPoint.Y, sourceRec, GraphicsUnit.Pixel);

                                                        for (int s = 1; s <= r; s++)
                                                        {
                                                            this.graphics.DrawImage(spriteToBlit, blitPoint.X + (r * Constants.TILEWIDTH), blitPoint.Y - (s * Constants.TILEHEIGHT), sourceRec, GraphicsUnit.Pixel);

                                                            this.graphics.DrawImage(spriteToBlit, blitPoint.X + (r * Constants.TILEWIDTH), blitPoint.Y + (s * Constants.TILEHEIGHT), sourceRec, GraphicsUnit.Pixel);
                                                        }
                                                    }

                                                    break;
                                            }
                                        }
                                    }

                                    break;
                                case Enums.MoveRange.ArcThrow:
                                    {
                                        bool stopattile = false;

                                        for (int r = 0; r <= specifiedAnim.Range; r++)
                                        {
                                            // check adjacent tiles
                                            switch (specifiedAnim.Direction)
                                            {
                                                case Enums.Direction.Right:
                                                    {
                                                        this.graphics.DrawImage(spriteToBlit, blitPoint.X + (r * Constants.TILEWIDTH), blitPoint.Y, sourceRec, GraphicsUnit.Pixel);

                                                        if (MoveRenderer.IsRenderingTargetOnSprite(
                                                            Maps.MapHelper.ActiveMap,
                                                                specifiedAnim.StartX + r, specifiedAnim.StartY))
                                                        {
                                                            stopattile = true;
                                                        }

                                                        if (stopattile)
                                                        {
                                                            break;
                                                        }

                                                        for (int s = 1; s <= r; s++)
                                                        {
                                                            this.graphics.DrawImage(spriteToBlit, blitPoint.X + (r * Constants.TILEWIDTH), blitPoint.Y - (s * Constants.TILEHEIGHT), sourceRec, GraphicsUnit.Pixel);

                                                            if (MoveRenderer.IsRenderingTargetOnSprite(
                                                                Maps.MapHelper.ActiveMap,
                                                                specifiedAnim.StartX + r, specifiedAnim.StartY - s))
                                                            {
                                                                stopattile = true;
                                                            }

                                                            if (stopattile)
                                                            {
                                                                break;
                                                            }

                                                            this.graphics.DrawImage(spriteToBlit, blitPoint.X + (r * Constants.TILEWIDTH), blitPoint.Y + (s * Constants.TILEHEIGHT), sourceRec, GraphicsUnit.Pixel);

                                                            if (MoveRenderer.IsRenderingTargetOnSprite(
                                                                Maps.MapHelper.ActiveMap,
                                                                specifiedAnim.StartX + r, specifiedAnim.StartY + s))
                                                            {
                                                                stopattile = true;
                                                            }

                                                            if (stopattile)
                                                            {
                                                                break;
                                                            }
                                                        }
                                                    }

                                                    break;
                                            }

                                            if (stopattile)
                                            {
                                                break;
                                            }
                                        }
                                    }

                                    break;
                            }

                            // for (int y = specifiedAnim.StartY; y <= specifiedAnim.EndY; y++) {
                            //    for (int x = specifiedAnim.StartX; x <= specifiedAnim.EndX; x++) {

                            // Point blitPoint = Screen.ScreenRenderer.ToTilePoint(new Point(x, y), useScrolling);
                            //        blitPoint.X = blitPoint.X + Constants.TILE_WIDTH / 2 - spriteToBlit.Height / 2;
                            //        blitPoint.Y = blitPoint.Y + Constants.TILE_HEIGHT / 2 - spriteToBlit.Height / 2;

                            // destData.Blit(spriteToBlit, blitPoint, sourceRec);
                            //    }
                            // }
                            if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength)
                            {
                                specifiedAnim.MoveTime = Globals.Tick;
                                specifiedAnim.Frame++;
                            }

                            if (specifiedAnim.Frame >= spriteToBlit.Width / spriteToBlit.Height)
                            {
                                specifiedAnim.CompletedLoops++;
                                specifiedAnim.Frame = 0;
                            }
                        }
                        else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.ItemArrow:
                    {
                        ItemArrowMoveAnimation specifiedAnim = animation as ItemArrowMoveAnimation;
                        int time = Globals.Tick - specifiedAnim.TotalMoveTime;
                        if (time < animTime)
                        {
                            if (specifiedAnim.AnimationIndex < 0)
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                (specifiedAnim.AnimationIndex - ((specifiedAnim.AnimationIndex / 6) * 6)) * Constants.TILEWIDTH,
                                               (specifiedAnim.AnimationIndex / 6) * Constants.TILEHEIGHT, Constants.TILEWIDTH, Constants.TILEHEIGHT);

                            switch (specifiedAnim.Direction)
                            {
                                case Enums.Direction.Right:
                                    {
                                        pinnedPoint.X += specifiedAnim.Distance * Constants.TILEWIDTH * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.UpRight:
                                case Enums.Direction.DownRight:
                                case Enums.Direction.DownLeft:
                                case Enums.Direction.UpLeft:
                                    break;
                            }

                            // blit
                            this.graphics.DrawImage(GraphicsManager.Items.Bitmap, pinnedPoint.X, pinnedPoint.Y, sourceRec, GraphicsUnit.Pixel);

                            // if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength) {
                            //    specifiedAnim.MoveTime = Globals.Tick;
                            //    specifiedAnim.Frame++;
                            // }

                            // if (specifiedAnim.Frame >= spriteToBlit.Width / spriteToBlit.Height) {
                            //    specifiedAnim.CompletedLoops++;
                            //    specifiedAnim.Frame = 0;
                            // }
                        }
                        else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.ItemThrow:
                    {
                        ItemThrowMoveAnimation specifiedAnim = animation as ItemThrowMoveAnimation;
                        int time = Globals.Tick - specifiedAnim.TotalMoveTime;
                        if (time < animTime)
                        {
                            if (specifiedAnim.AnimationIndex < 0)
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                               (specifiedAnim.AnimationIndex - ((specifiedAnim.AnimationIndex / 6) * 6)) * Constants.TILEWIDTH,
                                               (specifiedAnim.AnimationIndex / 6) * Constants.TILEHEIGHT,
                                               Constants.TILEWIDTH,
                                               Constants.TILEHEIGHT);

                            double distance = Math.Sqrt(Math.Pow(specifiedAnim.XChange * Constants.TILEWIDTH, 2) + Math.Pow(specifiedAnim.YChange * Constants.TILEHEIGHT, 2));

                            int x = pinnedPoint.X + (specifiedAnim.XChange * Constants.TILEWIDTH * time / animTime);
                            int y = (int)(pinnedPoint.Y + (specifiedAnim.YChange * Constants.TILEHEIGHT * time / animTime) - ((((-4) * distance * Math.Pow(time, 2) / animTime) + (4 * distance * time)) / animTime));

                            // blit
                            this.graphics.DrawImage(GraphicsManager.Items.Bitmap, x, y, sourceRec, GraphicsUnit.Pixel);

                            // if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength) {
                            //    specifiedAnim.MoveTime = Globals.Tick;
                            //    specifiedAnim.Frame++;
                            // }

                            // if (specifiedAnim.Frame >= spriteToBlit.Width / spriteToBlit.Height) {
                            //    specifiedAnim.CompletedLoops++;
                            //    specifiedAnim.Frame = 0;
                            // }
                        }
                        else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
            }
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void NumericUpDown5_ValueChanged(object sender, EventArgs e)
        {
        }

        private void NumericUpDown6_ValueChanged(object sender, EventArgs e)
        {
        }

        private void NumericUpDown7_ValueChanged(object sender, EventArgs e)
        {
        }

        private void NumericUpDown8_ValueChanged(object sender, EventArgs e)
        {
        }

        private void NudFindMove_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Return)
            {
                e.Handled = e.SuppressKeyPress = true;
            }
        }

        private void BtnPreview_Click(object sender, EventArgs e)
        {
            this.ShowPreview();
        }

        private void BtnAttacker_Click(object sender, EventArgs e)
        {
            this.pnlAttacker.Show();
            this.pnlDefender.Hide();
            this.pnlTraveling.Hide();
        }

        private void BtnTraveling_Click(object sender, EventArgs e)
        {
            this.pnlAttacker.Hide();
            this.pnlDefender.Hide();
            this.pnlTraveling.Show();
        }

        private void BtnDefender_Click(object sender, EventArgs e)
        {
            this.pnlAttacker.Hide();
            this.pnlDefender.Show();
            this.pnlTraveling.Hide();
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
        }

        private void PictureBox3_Click(object sender, EventArgs e)
        {
        }

        private void NudSound_ValueChanged(object sender, EventArgs e)
        {
            this.ShowPreview();
        }

        private void Title_MouseUp(object sender, MouseEventArgs e)
        {
            this.drag = false;
        }

        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            this.startPoint = e.Location;
            this.drag = true;
        }

        private void Title_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.drag)
            { // if we should be dragging it, we need to figure out some movement
                Point p1 = new Point(e.X, e.Y);
                Point p2 = this.PointToScreen(p1);
                Point p3 = new Point(
                    p2.X - this.startPoint.X,
                                     p2.Y - this.startPoint.Y);
                this.Location = p3;
            }
        }
    }
}
