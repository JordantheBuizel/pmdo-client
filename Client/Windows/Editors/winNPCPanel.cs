﻿// <copyright file="winNPCPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Network;
    using PMU.Core;
    using PMU.Sockets;
    using SdlDotNet.Widgets;

    internal class WinNPCPanel : Core.WindowCore
    {
        private Panel pnlNPCEditor;

        private int itemNum = -1;
        private int currentTen = 0;
        private readonly Panel pnlNPCList;
        private readonly ScrollingListBox lbxNPCList;
        private ListBoxTextItem lbiArrow;
        private readonly Button btnBack;
        private readonly Button btnForward;
        private readonly Button btnCancel;
        private readonly Button btnEdit;
        private readonly Label lblSearch;
        private readonly NumericUpDown findTen;
        private readonly Button btnEditorCancel;
        private readonly Button btnEditorOK;
        private readonly Label lblName;
        private readonly TextBox txtName;
        private readonly Label lblAttackSay;
        private readonly TextBox txtAttackSay;
        private readonly Label lblForm;
        private readonly NumericUpDown nudForm;
        private readonly Label lblSpecies;
        private readonly NumericUpDown nudSpecies;
        private readonly Label lblRange;
        private readonly NumericUpDown nudShinyChance;
        private readonly CheckBox chkSpawnsAtDawn;
        private readonly CheckBox chkSpawnsAtDay;
        private readonly CheckBox chkSpawnsAtDusk;
        private readonly CheckBox chkSpawnsAtNight;
        private readonly Label lblBehaviour;
        private readonly ComboBox cmbBehaviour;
        private readonly Label lblRecruitRate;
        private readonly NumericUpDown nudRecruitRate;
        private readonly Label[] lblMove;
        private readonly Label[] lblMoveInfo;
        private readonly NumericUpDown[] nudMove;
        private readonly Label lblDropSelector;
        private readonly NumericUpDown nudDropSelector;
        private readonly Label lblDropItemNum;
        private readonly NumericUpDown nudDropItemNum;
        private readonly Label lblDropItemAmount;
        private readonly NumericUpDown nudDropItemAmount;
        private readonly Label lblDropItemChance;
        private readonly NumericUpDown nudDropItemChance;
        private readonly Label lblDropItemTag;
        private readonly TextBox txtDropItemTag;
        private Logic.Editors.NPCs.EditableNPC npc;

        public WinNPCPanel()
            : base("winNPCPanel")
        {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new Size(200, 270);
            this.Location = new Point(210, Windows.WindowSwitcher.GameWindow.ActiveTeam.Y + Windows.WindowSwitcher.GameWindow.ActiveTeam.Height + 0);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = true;
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "NPC Panel";

            this.pnlNPCList = new Panel("pnlNPCList");
            this.pnlNPCList.Size = new Size(200, 270);
            this.pnlNPCList.Location = new Point(0, 0);
            this.pnlNPCList.BackColor = Color.White;
            this.pnlNPCList.Visible = true;

            this.PnlNPCEditor = new Panel("pnlNPCEditor");
            this.PnlNPCEditor.Size = new Size(300, 420);
            this.PnlNPCEditor.Location = new Point(0, 0);
            this.PnlNPCEditor.BackColor = Color.White;
            this.PnlNPCEditor.Visible = false;
            this.lbxNPCList = new ScrollingListBox("lbxNPCList");
            this.lbxNPCList.Location = new Point(10, 10);
            this.lbxNPCList.Size = new Size(180, 140);
            for (int i = 0; i < 10; i++)
            {
                ListBoxTextItem lbiNPC = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (i + 1) + ": " + Npc.NpcHelper.Npcs[(i + 1) + (10 * this.currentTen)].Name);
                this.lbxNPCList.Items.Add(lbiNPC);
            }

            this.btnBack = new Button("btnBack");
            this.btnBack.Location = new Point(10, 160);
            this.btnBack.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnBack.Size = new Size(64, 16);
            this.btnBack.Visible = true;
            this.btnBack.Text = "<--";
            this.btnBack.Click += new EventHandler<MouseButtonEventArgs>(this.BtnBack_Click);

            this.btnForward = new Button("btnForward");
            this.btnForward.Location = new Point(126, 160);
            this.btnForward.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnForward.Size = new Size(64, 16);
            this.btnForward.Visible = true;
            this.btnForward.Text = "-->";
            this.btnForward.Click += new EventHandler<MouseButtonEventArgs>(this.BtnForward_Click);

            this.btnEdit = new Button("btnEdit");
            this.btnEdit.Location = new Point(10, 190);
            this.btnEdit.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEdit.Size = new Size(64, 16);
            this.btnEdit.Visible = true;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEdit_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(126, 190);
            this.btnCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnCancel.Size = new Size(64, 16);
            this.btnCancel.Visible = true;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.lblSearch = new Label("lblSearch");
            this.lblSearch.Location = new Point(11, 220);
            this.lblSearch.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblSearch.Text = "Find NPC Number:";
            this.lblSearch.AutoSize = true;

            this.findTen = new NumericUpDown("findTen");
            this.findTen.Location = new Point(126, 220);
            this.findTen.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.findTen.Size = new Size(64, 16);
            this.findTen.Visible = true;
            this.findTen.Minimum = 1;
            this.findTen.Maximum = MaxInfo.MaxNpcs;
            this.findTen.KeyUp += new EventHandler<SdlDotNet.Input.KeyboardEventArgs>(this.FindTen_KeyUp);
            this.lblName = new Label("lblName");
            this.lblName.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblName.Text = "NPC Name:";
            this.lblName.AutoSize = true;
            this.lblName.Location = new Point(10, 5);

            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(200, 15);
            this.txtName.Location = new Point(75, 5);
            this.txtName.Font = Graphic.FontManager.LoadFont("Tahoma", 12);
            this.txtName.Text = string.Empty;

            this.lblAttackSay = new Label("lblAttackSay");
            this.lblAttackSay.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblAttackSay.Text = "Attack Say:";
            this.lblAttackSay.AutoSize = true;
            this.lblAttackSay.Location = new Point(10, 25);

            this.txtAttackSay = new TextBox("txtAttackSay");
            this.txtAttackSay.Size = new Size(200, 15);
            this.txtAttackSay.Location = new Point(75, 25);
            this.txtAttackSay.Font = Graphic.FontManager.LoadFont("tahoma", 12);

            this.lblForm = new Label("lblForm");
            this.lblForm.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblForm.Text = "Form:";
            this.lblForm.AutoSize = true;
            this.lblForm.Location = new Point(10, 45);

            this.nudForm = new NumericUpDown("nudForm");
            this.nudForm.Maximum = 1000;
            this.nudForm.Location = new Point(75, 45);
            this.nudForm.Size = new Size(200, 15);
            this.nudForm.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudForm_ValueChanged);

            this.lblSpecies = new Label("lblSpecies");
            this.lblSpecies.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblSpecies.Text = "Species:";
            this.lblSpecies.AutoSize = true;
            this.lblSpecies.Location = new Point(10, 65);

            this.nudSpecies = new NumericUpDown("nudSpecies");
            this.nudSpecies.Location = new Point(75, 65);
            this.nudSpecies.Size = new Size(200, 15);
            this.nudSpecies.Minimum = -1;
            this.nudSpecies.Maximum = 721;
            this.nudForm.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudSpecies_ValueChanged);

            this.lblRange = new Label("lblRange");
            this.lblRange.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblRange.Text = "Shiny:";
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new Point(10, 85);

            this.nudShinyChance = new NumericUpDown("nudShinyChance");
            this.nudShinyChance.Location = new Point(75, 85);
            this.nudShinyChance.Size = new Size(200, 15);
            this.nudShinyChance.Maximum = int.MaxValue;
            this.nudShinyChance.Minimum = 0;

            this.chkSpawnsAtDawn = new CheckBox("chkSpawnsAtDawn");
            this.chkSpawnsAtDawn.Size = new Size(60, 17);
            this.chkSpawnsAtDawn.Location = new Point(75, 105);
            this.chkSpawnsAtDawn.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.chkSpawnsAtDawn.Text = "Dawn";

            this.chkSpawnsAtDay = new CheckBox("chkSpawnsAtDay");
            this.chkSpawnsAtDay.Size = new Size(60, 17);
            this.chkSpawnsAtDay.Location = new Point(175, 105);
            this.chkSpawnsAtDay.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.chkSpawnsAtDay.Text = "Day";

            this.chkSpawnsAtDusk = new CheckBox("chkSpawnsAtDusk");
            this.chkSpawnsAtDusk.Size = new Size(60, 17);
            this.chkSpawnsAtDusk.Location = new Point(75, 125);
            this.chkSpawnsAtDusk.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.chkSpawnsAtDusk.Text = "Dusk";

            this.chkSpawnsAtNight = new CheckBox("chkSpawnsAtNight");
            this.chkSpawnsAtNight.Size = new Size(60, 17);
            this.chkSpawnsAtNight.Location = new Point(175, 125);
            this.chkSpawnsAtNight.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.chkSpawnsAtNight.Text = "Night";

            this.lblBehaviour = new Label("lblBehaviour");
            this.lblBehaviour.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblBehaviour.Text = "Behaviour:";
            this.lblBehaviour.AutoSize = true;
            this.lblBehaviour.Location = new Point(10, this.lblRange.Y + 60);

            this.cmbBehaviour = new ComboBox("cmbBehaviour");
            this.cmbBehaviour.Location = new Point(75, this.lblBehaviour.Y);
            this.cmbBehaviour.Size = new Size(200, 15);
            for (int i = 0; i < 7; i++)
            {
                this.cmbBehaviour.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), Enum.GetName(typeof(Enums.NpcBehavior), i)));
            }

            this.lblRecruitRate = new Label("lblRecruitRate");
            this.lblRecruitRate.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblRecruitRate.Text = "Recruit Rate:";
            this.lblRecruitRate.AutoSize = true;
            this.lblRecruitRate.Location = new Point(10, this.lblBehaviour.Y + 20);

            this.nudRecruitRate = new NumericUpDown("nudRecruitRate");
            this.nudRecruitRate.Location = new Point(85, this.lblRecruitRate.Y);
            this.nudRecruitRate.Size = new Size(190, 15);
            this.nudRecruitRate.Minimum = -1000;
            this.nudRecruitRate.Maximum = 1000;

            this.lblMove = new Label[4];
            this.lblMoveInfo = new Label[4];
            this.nudMove = new NumericUpDown[4];
            for (int i = 0; i < this.lblMove.Length; i++)
            {
                this.lblMove[i] = new Label("lblMove" + i);
                this.lblMoveInfo[i] = new Label("lblMoveInfo" + i);
                this.nudMove[i] = new NumericUpDown("nudMove" + i);
                this.nudMove[i].ValueChanged += new EventHandler<ValueChangedEventArgs>(this.WinNPCPanel_ValueChanged);

                this.lblMove[i].Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.lblMove[i].Text = "Move " + (i + 1) + ":";
                this.lblMove[i].AutoSize = true;
                this.lblMove[i].Location = new Point(10, (this.lblRecruitRate.Y + 20) + (20 * i));

                this.nudMove[i].Location = new Point(75, (this.lblRecruitRate.Y + 20) + (20 * i));
                this.nudMove[i].Size = new Size(150, 15);
                this.nudMove[i].Minimum = -1;
                this.nudMove[i].Maximum = MaxInfo.MaxMoves;

                this.lblMoveInfo[i].Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.lblMoveInfo[i].AutoSize = true;
                this.lblMoveInfo[i].Location = new Point(230, (this.lblRecruitRate.Y + 20) + (20 * i));
            }

            this.lblDropSelector = new Label("lblDropSelector");
            this.lblDropSelector.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblDropSelector.AutoSize = true;
            this.lblDropSelector.Text = "Drop #:";
            this.lblDropSelector.Location = new Point(10, this.lblMove[this.lblMove.Length - 1].Y + 20);

            this.nudDropSelector = new NumericUpDown("nudDropSelector");
            this.nudDropSelector.Location = new Point(75, this.lblDropSelector.Y);
            this.nudDropSelector.Size = new Size(200, 15);
            this.nudDropSelector.Minimum = 1;
            this.nudDropSelector.Maximum = MaxInfo.MAXNPCDROPS;
            this.nudDropSelector.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudDropSelector_ValueChanged);

            this.lblDropItemNum = new Label("lblDropItemNum");
            this.lblDropItemNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblDropItemNum.AutoSize = true;
            this.lblDropItemNum.Text = "Drop Item #:";
            this.lblDropItemNum.Location = new Point(10, this.lblDropSelector.Y + 20);

            this.nudDropItemNum = new NumericUpDown("nudDropItemNum");
            this.nudDropItemNum.Location = new Point(75, this.lblDropItemNum.Y);
            this.nudDropItemNum.Size = new Size(200, 15);
            this.nudDropItemNum.Minimum = 0;
            this.nudDropItemNum.Maximum = MaxInfo.MaxItems;
            this.nudDropItemNum.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudDropItemNum_ValueChanged);

            this.lblDropItemAmount = new Label("lblDropItemAmount");
            this.lblDropItemAmount.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblDropItemAmount.AutoSize = true;
            this.lblDropItemAmount.Text = "Drop Amount:";
            this.lblDropItemAmount.Location = new Point(10, this.lblDropItemNum.Y + 20);

            this.nudDropItemAmount = new NumericUpDown("nudDropItemAmount");
            this.nudDropItemAmount.Location = new Point(85, this.lblDropItemAmount.Y);
            this.nudDropItemAmount.Size = new Size(190, 15);
            this.nudDropItemAmount.Minimum = 1;
            this.nudDropItemAmount.Maximum = int.MaxValue;
            this.nudDropItemAmount.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudDropItemAmount_ValueChanged);

            this.lblDropItemChance = new Label("lblDropItemChance");
            this.lblDropItemChance.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblDropItemChance.AutoSize = true;
            this.lblDropItemChance.Text = "Drop Chance:";
            this.lblDropItemChance.Location = new Point(10, this.lblDropItemAmount.Y + 20);

            this.nudDropItemChance = new NumericUpDown("nudDropItemChance");
            this.nudDropItemChance.Location = new Point(85, this.lblDropItemChance.Y);
            this.nudDropItemChance.Size = new Size(190, 15);
            this.nudDropItemChance.Minimum = 1;
            this.nudDropItemChance.Maximum = 100;
            this.nudDropItemChance.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudDropItemChance_ValueChanged);

            this.lblDropItemTag = new Label("lblDropItemTag");
            this.lblDropItemTag.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblDropItemTag.AutoSize = true;
            this.lblDropItemTag.Text = "Item Tag:";
            this.lblDropItemTag.Location = new Point(10, this.lblDropItemChance.Y + 20);

            this.txtDropItemTag = new TextBox("nudDropItemTag");
            this.txtDropItemTag.Location = new Point(85, this.lblDropItemTag.Y);
            this.txtDropItemTag.Size = new Size(190, 15);
            this.txtDropItemTag.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.txtDropItemTag.TextChanged += new EventHandler(this.TxtDropItemTag_TextChanged);
            this.btnEditorCancel = new Button("btnEditorCancel");
            this.btnEditorCancel.Location = new Point(20, this.lblDropItemTag.Y + 20);
            this.btnEditorCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorCancel.Size = new Size(64, 16);
            this.btnEditorCancel.Visible = true;
            this.btnEditorCancel.Text = "Cancel";
            this.btnEditorCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorCancel_Click);

            this.btnEditorOK = new Button("btnEditorOK");
            this.btnEditorOK.Location = new Point(120, this.lblDropItemTag.Y + 20);
            this.btnEditorOK.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorOK.Size = new Size(64, 16);
            this.btnEditorOK.Visible = true;
            this.btnEditorOK.Text = "OK";
            this.btnEditorOK.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorOK_Click);
            this.pnlNPCList.AddWidget(this.lbxNPCList);
            this.pnlNPCList.AddWidget(this.btnBack);
            this.pnlNPCList.AddWidget(this.btnForward);
            this.pnlNPCList.AddWidget(this.btnEdit);
            this.pnlNPCList.AddWidget(this.btnCancel);
            this.pnlNPCList.AddWidget(this.lblSearch);
            this.pnlNPCList.AddWidget(this.findTen);
            this.PnlNPCEditor.AddWidget(this.lblName);
            this.PnlNPCEditor.AddWidget(this.txtName);
            this.PnlNPCEditor.AddWidget(this.lblAttackSay);
            this.PnlNPCEditor.AddWidget(this.txtAttackSay);
            this.PnlNPCEditor.AddWidget(this.lblForm);
            this.PnlNPCEditor.AddWidget(this.nudForm);
            this.PnlNPCEditor.AddWidget(this.lblSpecies);
            this.PnlNPCEditor.AddWidget(this.nudSpecies);
            this.PnlNPCEditor.AddWidget(this.lblRange);
            this.PnlNPCEditor.AddWidget(this.nudShinyChance);
            this.PnlNPCEditor.AddWidget(this.chkSpawnsAtDawn);
            this.PnlNPCEditor.AddWidget(this.chkSpawnsAtDay);
            this.PnlNPCEditor.AddWidget(this.chkSpawnsAtDusk);
            this.PnlNPCEditor.AddWidget(this.chkSpawnsAtNight);
            this.PnlNPCEditor.AddWidget(this.lblBehaviour);
            this.PnlNPCEditor.AddWidget(this.cmbBehaviour);
            this.PnlNPCEditor.AddWidget(this.lblRecruitRate);
            this.PnlNPCEditor.AddWidget(this.nudRecruitRate);

            for (int i = 0; i < this.lblMove.Length; i++)
            {
                this.PnlNPCEditor.AddWidget(this.lblMove[i]);
                this.PnlNPCEditor.AddWidget(this.nudMove[i]);
                this.PnlNPCEditor.AddWidget(this.lblMoveInfo[i]);
            }

            this.PnlNPCEditor.AddWidget(this.lblDropSelector);
            this.PnlNPCEditor.AddWidget(this.nudDropSelector);
            this.PnlNPCEditor.AddWidget(this.lblDropItemNum);
            this.PnlNPCEditor.AddWidget(this.nudDropItemNum);
            this.PnlNPCEditor.AddWidget(this.lblDropItemAmount);
            this.PnlNPCEditor.AddWidget(this.nudDropItemAmount);
            this.PnlNPCEditor.AddWidget(this.lblDropItemChance);
            this.PnlNPCEditor.AddWidget(this.nudDropItemChance);
            this.PnlNPCEditor.AddWidget(this.lblDropItemTag);
            this.PnlNPCEditor.AddWidget(this.txtDropItemTag);
            this.PnlNPCEditor.AddWidget(this.btnEditorCancel);
            this.PnlNPCEditor.AddWidget(this.btnEditorOK);
            this.AddWidget(this.pnlNPCList);
            this.AddWidget(this.PnlNPCEditor);

            this.RefreshNPCList();
            this.LoadComplete();
        }

        public Panel PnlNPCEditor { get => this.pnlNPCEditor; set => this.pnlNPCEditor = value; }

        public void RefreshNPCList()
        {
            for (int i = 0; i < 10; i++)
            {
                if ((i + (this.currentTen * 10)) < MaxInfo.MaxNpcs)
                {
                    ((ListBoxTextItem)this.lbxNPCList.Items[i]).Text = (i + (10 * this.currentTen)) + 1 + ": " + Npc.NpcHelper.Npcs[(i + (10 * this.currentTen)) + 1].Name;
                }
                else
                {
                    ((ListBoxTextItem)this.lbxNPCList.Items[i]).Text = "---";
                }
            }
        }

        public void LoadNPC(string[] parse)
        {
            this.pnlNPCList.Visible = false;
            this.PnlNPCEditor.Visible = true;
            this.Size = this.PnlNPCEditor.Size;

            this.npc = new Logic.Editors.NPCs.EditableNPC();

            this.npc.Name = parse[1];
            this.npc.AttackSay = parse[2];
            this.npc.Form = parse[3].ToInt();
            this.npc.Species = parse[4].ToInt();
            this.npc.ShinyChance = parse[5].ToInt();
            this.npc.Behavior = (Enums.NpcBehavior)parse[6].ToInt();
            this.npc.RecruitRate = parse[7].ToInt();
            this.npc.AIScript = parse[8];
            this.npc.SpawnsAtDawn = parse[9].ToBool();
            this.npc.SpawnsAtDay = parse[10].ToBool();
            this.npc.SpawnsAtDusk = parse[11].ToBool();
            this.npc.SpawnsAtNight = parse[12].ToBool();

            int n = 13;

            // Load npc moves
            for (int i = 0; i < this.npc.Moves.Length; i++)
            {
                this.npc.Moves[i] = parse[n].ToInt();

                n += 1;
            }

            // Load npc drops
            for (int i = 0; i < this.npc.Drops.Length; i++)
            {
                this.npc.Drops[i] = new Logic.Editors.NPCs.EditableNpcDrop();
                this.npc.Drops[i].ItemNum = parse[n].ToInt();
                this.npc.Drops[i].ItemValue = parse[n + 1].ToInt();
                this.npc.Drops[i].Chance = parse[n + 2].ToInt();
                this.npc.Drops[i].Tag = parse[n + 3];

                n += 4;
            }

            this.txtName.Text = this.npc.Name;
            this.txtAttackSay.Text = this.npc.AttackSay;
            this.nudForm.Value = this.npc.Form;
            this.nudSpecies.Value = this.npc.Species;
            this.nudShinyChance.Value = this.npc.ShinyChance;
            this.chkSpawnsAtDawn.Checked = this.npc.SpawnsAtDawn;
            this.chkSpawnsAtDay.Checked = this.npc.SpawnsAtDay;
            this.chkSpawnsAtDusk.Checked = this.npc.SpawnsAtDusk;
            this.chkSpawnsAtNight.Checked = this.npc.SpawnsAtNight;
            this.cmbBehaviour.SelectItem(this.npc.Behavior.ToString());

            this.nudRecruitRate.Value = this.npc.RecruitRate;
            for (int i = 0; i < this.npc.Moves.Length; i++)
            {
                this.nudMove[i].Value = this.npc.Moves[i];
            }

            this.nudDropSelector.Value = 1;
            this.nudDropItemNum.Value = this.npc.Drops[0].ItemNum;
            this.nudDropItemAmount.Value = this.npc.Drops[0].ItemValue;
            this.nudDropItemChance.Value = this.npc.Drops[0].Chance;
            this.txtDropItemTag.Text = this.npc.Drops[0].Tag;

            this.btnEdit.Text = "Edit";
        }

        private void TxtDropItemTag_TextChanged(object sender, EventArgs e)
        {
            this.npc.Drops[this.nudDropSelector.Value - 1].Tag = this.txtDropItemTag.Text;
        }

        private void NudDropItemChance_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.npc.Drops[this.nudDropSelector.Value - 1].Chance = e.NewValue;
        }

        private void NudDropItemAmount_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.npc.Drops[this.nudDropSelector.Value - 1].ItemValue = e.NewValue;
        }

        private void NudDropItemNum_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.npc.Drops[this.nudDropSelector.Value - 1].ItemNum = e.NewValue;
        }

        private void NudDropSelector_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.nudDropItemNum.Value = this.npc.Drops[e.NewValue - 1].ItemNum;
            this.nudDropItemAmount.Value = this.npc.Drops[e.NewValue - 1].ItemValue;
            this.nudDropItemChance.Value = this.npc.Drops[e.NewValue - 1].Chance;
            this.txtDropItemTag.Text = this.npc.Drops[e.NewValue - 1].Tag;
        }

        private void WinNPCPanel_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            int index = Array.IndexOf(this.nudMove, sender);
            if (e.NewValue == 0)
            {
                this.lblMoveInfo[index].Text = "None";
            }
            else if (e.NewValue == -1)
            {
                this.lblMoveInfo[index].Text = "Auto";
            }
else
            {
                this.lblMoveInfo[index].Text = Moves.MoveHelper.Moves[e.NewValue].Name;
            }
        }

        private void FindTen_KeyUp(object sender, SdlDotNet.Input.KeyboardEventArgs e)
        {
            if (e.Key == SdlDotNet.Input.Key.Return)
            {
                int getTen = this.findTen.Value / 10;
                for (int i = 0; i < MaxInfo.MaxNpcs / 10; i++)
                {
                    if (i < getTen && i >= getTen - 1)
                    {
                        this.currentTen = i + 1;

                        /*bool evenTen = false;
                        string[] getselectedMove = new string[0];
                        try
                        {
                            getselectedMove = getTen.ToString().Split('.');
                        }
                        catch
                        {
                            evenTen = true;
                        }

                        if (evenTen == false && getselectedMove[1] != null)
                        {
                            lbxNPCList.SelectItem(Int32.Parse(getselectedMove[1]) - 1);
                        }
                        else
                        {
                            lbxNPCList.SelectItem(9);
                        }*/
                    }
                }

                this.RefreshNPCList();
            }
        }

        private void NudForm_ValueChanged(object sender, ValueChangedEventArgs e)
        {
        }

        private void NudSpecies_ValueChanged(object sender, ValueChangedEventArgs e)
        {
        }

        private void BtnBack_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen > 0)
            {
                this.currentTen--;
            }

            this.RefreshNPCList();
        }

        private void BtnForward_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen < ((MaxInfo.MaxNpcs - 1) / 10))
            {
                this.currentTen++;
            }

            this.RefreshNPCList();
        }

        private void BtnEdit_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxNPCList.SelectedItems.Count == 1)
            {
                string[] index = ((ListBoxTextItem)this.lbxNPCList.SelectedItems[0]).Text.Split(':');
                if (index[0].IsNumeric())
                {
                    this.itemNum = index[0].ToInt();
                    Messenger.SendEditNpc(this.itemNum);
                }
            }
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            return;
        }

        private void BtnEditorCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.itemNum = -1;
            this.PnlNPCEditor.Visible = false;
            this.pnlNPCList.Visible = true;
            this.Size = this.pnlNPCList.Size;
        }

        private void HsbPic_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            // pic.Image = Tools.CropImage(Logic.Graphic.GraphicsManager.Arrows, new Rectangle(0, hsbSpecies.Value * 32, 32, 32));
        }

        private void BtnEditorOK_Click(object sender, MouseButtonEventArgs e)
        {
            this.npc.Name = this.txtName.Text;
            this.npc.AttackSay = this.txtAttackSay.Text;
            this.npc.Form = this.nudForm.Value;
            this.npc.Species = this.nudSpecies.Value;
            this.npc.ShinyChance = this.nudShinyChance.Value;
            this.npc.SpawnsAtDawn = this.chkSpawnsAtDawn.Checked;
            this.npc.SpawnsAtDay = this.chkSpawnsAtDay.Checked;
            this.npc.SpawnsAtDusk = this.chkSpawnsAtDusk.Checked;
            this.npc.SpawnsAtNight = this.chkSpawnsAtNight.Checked;
            this.npc.Behavior = (Enums.NpcBehavior)this.cmbBehaviour.SelectedIndex;

            // 10 = spawn time...
            this.npc.RecruitRate = this.nudRecruitRate.Value;

            // Save npc moves
            for (int i = 0; i < this.npc.Moves.Length; i++)
            {
                this.npc.Moves[i] = this.nudMove[i].Value;
            }

            Messenger.SendSaveNpc(this.itemNum, this.npc);
            this.PnlNPCEditor.Visible = false;
            this.pnlNPCList.Visible = true;
            this.Size = this.pnlNPCList.Size;
        }
    }
}
