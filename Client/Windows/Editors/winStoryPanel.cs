﻿// <copyright file="winStoryPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Network;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class WinStoryPanel : Core.WindowCore
    {
        private int storyNum = -1;
        private int currentTen = 0;
        private Logic.Editors.Stories.EditableStory story;

        private readonly Panel pnlStoryList;
        private readonly Panel pnlEditorGeneral;
        private readonly Panel pnlEditorSegments;

        private readonly ScrollingListBox lbxStoryList;
        private readonly Button btnBack;
        private readonly Button btnForward;
        private readonly Button btnCancel;
        private readonly Button btnEdit;

        private readonly Button btnEditorCancel;
        private readonly Button btnEditorOK;
        private readonly Button btnSegments;
        private readonly Label lblName;
        private readonly TextBox txtName;
        private readonly Label lblStoryStart;
        private readonly NumericUpDown nudStoryStart;
        private readonly Label lblExitAndContinue;
        private readonly Button btnAddExitAndContinue;
        private readonly NumericUpDown nudExitAndContinueCheckpoint;
        private readonly Button btnRemoveExitAndContinue;
        private readonly ScrollingListBox lbxExitAndContinue;
        private readonly Button btnGeneral;

        // Label lblMaxSegments;
        // NumericUpDown nudMaxSegments;
        // Button btnSaveMaxSegments;
        // Label lblActiveSegment;
        // NumericUpDown nudActiveSegment;
        private readonly Button btnAddSegment;
        private readonly Button btnRemoveSegment;
        private readonly Button btnLoadSegment;
        private readonly Button btnSaveSegment;
        private readonly Label lblActions;
        private readonly ComboBox cmbSegmentTypes;
        private readonly ScrollingListBox lbxSegments;
        private Panel pnlSayAction;
        private Label lblSayText;
        private TextBox txtSayText;
        private Label lblSayMugshot;
        private NumericUpDown nudSayMugshot;
        private PictureBox pbxSayMugshot;
        private Label lblSaySpeed;
        private NumericUpDown nudSaySpeed;
        private Label lblSayPause;
        private NumericUpDown nudSayPause;
        private Panel pnlPauseAction;
        private Label lblPauseLength;
        private NumericUpDown nudPauseLength;
        private Panel pnlMapVisibilityAction;
        private CheckBox chkMapVisibilityVisible;
        private Panel pnlPadlockAction;
        private CheckBox chkPadlockState;
        private Panel pnlPlayMusicAction;
        private ScrollingListBox lbxPlayMusicPicker;
        private Button btnPlayMusicPlay;
        private Button btnPlayMusicStop;
        private CheckBox chkPlayMusicHonorSettings;
        private CheckBox chkPlayMusicLoop;
        private Panel pnlStopMusicAction;
        private Panel pnlShowImageAction;
        private ScrollingListBox lbxShowImageFiles;
        private Label lblShowImageID;
        private TextBox txtShowImageID;
        private Label lblShowImageX;
        private NumericUpDown nudShowImageX;
        private Label lblShowImageY;
        private NumericUpDown nudShowImageY;
        private Panel pnlHideImageAction;
        private Label lblHideImageID;
        private TextBox txtHideImageID;
        private Panel pnlWarpAction;
        private Label lblWarpMap;
        private TextBox txtWarpMap;
        private Label lblWarpX;
        private NumericUpDown nudWarpX;
        private Label lblWarpY;
        private NumericUpDown nudWarpY;
        private Panel pnlPlayerPadlockAction;
        private CheckBox chkPlayerPadlockState;
        private Panel pnlShowBackgroundAction;
        private ScrollingListBox lbxShowBackgroundFiles;
        private Panel pnlHideBackgroundAction;
        private Panel pnlCreateFNPCAction;
        private Label lblCreateFNPCID;
        private TextBox txtCreateFNPCID;
        private Label lblCreateFNPCMap;
        private TextBox txtCreateFNPCMap;
        private Label lblCreateFNPCX;
        private NumericUpDown nudCreateFNPCX;
        private Label lblCreateFNPCY;
        private NumericUpDown nudCreateFNPCY;
        private Label lblCreateFNPCSprite;
        private NumericUpDown nudCreateFNPCSprite;
        private PictureBox pbxCreateFNPCSprite;
        private Panel pnlMoveFNPCAction;
        private Label lblMoveFNPCID;
        private TextBox txtMoveFNPCID;
        private Label lblMoveFNPCX;
        private NumericUpDown nudMoveFNPCX;
        private Label lblMoveFNPCY;
        private NumericUpDown nudMoveFNPCY;
        private Label lblMoveFNPCSpeed;
        private ComboBox cbxMoveFNPCSpeed;
        private CheckBox chkMoveFNPCPause;
        private Panel pnlWarpFNPCAction;
        private Label lblWarpFNPCID;
        private TextBox txtWarpFNPCID;
        private Label lblWarpFNPCX;
        private NumericUpDown nudWarpFNPCX;
        private Label lblWarpFNPCY;
        private NumericUpDown nudWarpFNPCY;
        private Panel pnlChangeFNPCDirAction;
        private Label lblChangeFNPCDirID;
        private TextBox txtChangeFNPCDirID;
        private ComboBox cbxChangeFNPCDirDirection;
        private Panel pnlDeleteFNPCAction;
        private Label lblDeleteFNPCID;
        private TextBox txtDeleteFNPCID;
        private Panel pnlStoryScriptAction;
        private Label lblStoryScriptIndex;
        private NumericUpDown nudStoryScriptIndex;
        private Label lblStoryScriptParam1;
        private TextBox txtStoryScriptParam1;
        private Label lblStoryScriptParam2;
        private TextBox txtStoryScriptParam2;
        private Label lblStoryScriptParam3;
        private TextBox txtStoryScriptParam3;
        private CheckBox chkStoryScriptPause;
        private Panel pnlHidePlayersAction;
        private Panel pnlShowPlayersAction;
        private Panel pnlFNPCEmotionAction;
        private Label lblFNPCEmotionID;
        private TextBox txtFNPCEmotionID;
        private Label lblFNPCEmotionNum;
        private NumericUpDown nudFNPCEmotionNum;
        private Panel pnlWeatherAction;
        private Label lblWeatherType;
        private ComboBox cbxWeatherType;
        private Panel pnlHideNPCsAction;
        private Panel pnlShowNPCsAction;
        private Panel pnlWaitForMapAction;
        private Label lblWaitForMapMap;
        private TextBox txtWaitForMapMap;
        private Panel pnlWaitForLocAction;
        private Label lblWaitForLocMap;
        private TextBox txtWaitForLocMap;
        private Label lblWaitForLocX;
        private NumericUpDown nudWaitForLocX;
        private Label lblWaitForLocY;
        private NumericUpDown nudWaitForLocY;
        private Panel pnlAskQuestionAction;
        private Label lblAskQuestionQuestion;
        private TextBox txtAskQuestionQuestion;
        private Label lblAskQuestionSegmentOnYes;
        private NumericUpDown nudAskQuestionSegmentOnYes;
        private Label lblAskQuestionSegmentOnNo;
        private NumericUpDown nudAskQuestionSegmentOnNo;
        private Label lblAskQuestionMugshot;
        private NumericUpDown nudAskQuestionMugshot;
        private PictureBox pbxAskQuestionMugshot;
        private Panel pnlGoToSegmentAction;
        private Label lblGoToSegmentSegment;
        private NumericUpDown nudGoToSegmentSegment;
        private Panel pnlScrollCameraAction;
        private Label lblScrollCameraX;
        private NumericUpDown nudScrollCameraX;
        private Label lblScrollCameraY;
        private NumericUpDown nudScrollCameraY;
        private Label lblScrollCameraSpeed;
        private NumericUpDown nudScrollCameraSpeed;
        private CheckBox chkScrollCameraPause;
        private Panel pnlResetCameraAction;
        private Panel pnlMovePlayerAction;
        private Label lblMovePlayerX;
        private NumericUpDown nudMovePlayerX;
        private Label lblMovePlayerY;
        private NumericUpDown nudMovePlayerY;
        private Label lblMovePlayerSpeed;
        private ComboBox cbxMovePlayerSpeed;
        private CheckBox chkMovePlayerPause;
        private Panel pnlChangePlayerDirAction;
        private ComboBox cbxChangePlayerDirDirection;

        public WinStoryPanel()
            : base("winStoryPanel")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new Size(200, 230);
            this.Location = new Point(210, Windows.WindowSwitcher.GameWindow.ActiveTeam.Y + Windows.WindowSwitcher.GameWindow.ActiveTeam.Height + 0);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = true;
            this.TitleBar.Font = FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "Story Editor";
            this.pnlStoryList = new Panel("pnlStoryList");
            this.pnlStoryList.Size = new Size(200, 230);
            this.pnlStoryList.Location = new Point(0, 0);
            this.pnlStoryList.BackColor = Color.White;
            this.pnlStoryList.Visible = true;

            this.lbxStoryList = new ScrollingListBox("lbxStoryList");
            this.lbxStoryList.Location = new Point(10, 10);
            this.lbxStoryList.Size = new Size(180, 140);
            for (int i = 0; i < 10; i++)
            {
                ListBoxTextItem lbiItem = new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), (i + 1) + ": ");
                this.lbxStoryList.Items.Add(lbiItem);
            }

            this.lbxStoryList.SelectItem(0);

            this.btnBack = new Button("btnBack");
            this.btnBack.Location = new Point(10, 160);
            this.btnBack.Font = FontManager.LoadFont("tahoma", 10);
            this.btnBack.Size = new Size(64, 16);
            this.btnBack.Visible = true;
            this.btnBack.Text = "<--";
            this.btnBack.Click += new EventHandler<MouseButtonEventArgs>(this.BtnBack_Click);

            this.btnForward = new Button("btnForward");
            this.btnForward.Location = new Point(126, 160);
            this.btnForward.Font = FontManager.LoadFont("tahoma", 10);
            this.btnForward.Size = new Size(64, 16);
            this.btnForward.Visible = true;
            this.btnForward.Text = "-->";
            this.btnForward.Click += new EventHandler<MouseButtonEventArgs>(this.BtnForward_Click);

            this.btnEdit = new Button("btnEdit");
            this.btnEdit.Location = new Point(10, 190);
            this.btnEdit.Font = FontManager.LoadFont("tahoma", 10);
            this.btnEdit.Size = new Size(64, 16);
            this.btnEdit.Visible = true;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEdit_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(126, 190);
            this.btnCancel.Font = FontManager.LoadFont("tahoma", 10);
            this.btnCancel.Size = new Size(64, 16);
            this.btnCancel.Visible = true;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);
            this.pnlEditorGeneral = new Panel("pnlEditorGeneral");
            this.pnlEditorGeneral.Size = new Size(300, 400);
            this.pnlEditorGeneral.Location = new Point(0, 0);
            this.pnlEditorGeneral.BackColor = Color.White;
            this.pnlEditorGeneral.Visible = false;

            this.btnEditorCancel = new Button("btnEditorCancel");
            this.btnEditorCancel.Location = new Point(10, 334);
            this.btnEditorCancel.Font = FontManager.LoadFont("tahoma", 10);
            this.btnEditorCancel.Size = new Size(64, 16);
            this.btnEditorCancel.Visible = true;
            this.btnEditorCancel.Text = "Cancel";
            this.btnEditorCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorCancel_Click);

            this.btnEditorOK = new Button("btnEditorOK");
            this.btnEditorOK.Location = new Point(100, 334);
            this.btnEditorOK.Font = FontManager.LoadFont("tahoma", 10);
            this.btnEditorOK.Size = new Size(64, 16);
            this.btnEditorOK.Visible = true;
            this.btnEditorOK.Text = "OK";
            this.btnEditorOK.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorOK_Click);

            this.btnSegments = new Button("btnSegments");
            this.btnSegments.Location = new Point(215, 4);
            this.btnSegments.Font = FontManager.LoadFont("tahoma", 10);
            this.btnSegments.Size = new Size(64, 16);
            this.btnSegments.Text = "Segments";
            this.btnSegments.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSegments_Click);

            this.lblName = new Label("lblName");
            this.lblName.Font = FontManager.LoadFont("Tahoma", 10);
            this.lblName.Text = "Story Name:";
            this.lblName.AutoSize = true;
            this.lblName.Location = new Point(10, 4);

            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(200, 16);
            this.txtName.Location = new Point(10, 16);

            this.lblStoryStart = new Label("lblStoryStart");
            this.lblStoryStart.Font = FontManager.LoadFont("tahoma", 10);
            this.lblStoryStart.Text = "Only start if chapter is locked (0=disabled)";
            this.lblStoryStart.AutoSize = true;
            this.lblStoryStart.Location = new Point(10, 35);

            this.nudStoryStart = new NumericUpDown("nudStoryStart");
            this.nudStoryStart.Size = new Size(100, 16);
            this.nudStoryStart.Location = new Point(10, 47);
            this.nudStoryStart.Maximum = MaxInfo.MaxStories;

            this.lblExitAndContinue = new Label("lblExitAndContinue");
            this.lblExitAndContinue.Font = FontManager.LoadFont("tahoma", 10);
            this.lblExitAndContinue.Text = "Checkpoint Segments";
            this.lblExitAndContinue.AutoSize = true;
            this.lblExitAndContinue.Location = new Point(10, 65);

            this.lbxExitAndContinue = new ScrollingListBox("lbxExitAndContinue");
            this.lbxExitAndContinue.Location = new Point(10, 77);
            this.lbxExitAndContinue.Size = new Size(100, 100);
            this.lbxExitAndContinue.MultiSelect = false;

            this.btnAddExitAndContinue = new Button("btnAddExitAndContinue");
            this.btnAddExitAndContinue.Size = new Size(64, 16);
            this.btnAddExitAndContinue.Location = new Point(110, 77);
            this.btnAddExitAndContinue.Text = "Add";
            this.btnAddExitAndContinue.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddExitAndContinue_Click);

            this.nudExitAndContinueCheckpoint = new NumericUpDown("nudExitAndContinueCheckpoint");
            this.nudExitAndContinueCheckpoint.Location = new Point(110, 95);
            this.nudExitAndContinueCheckpoint.Maximum = 0;
            this.nudExitAndContinueCheckpoint.Size = new Size(64, 14);

            this.btnRemoveExitAndContinue = new Button("btnRemoveExitAndContinue");
            this.btnRemoveExitAndContinue.Location = new Point(110, 110);
            this.btnRemoveExitAndContinue.Size = new Size(64, 16);
            this.btnRemoveExitAndContinue.Text = "Remove";
            this.btnRemoveExitAndContinue.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveExitAndContinue_Click);
            this.pnlEditorSegments = new Panel("pnlEditorSegments");
            this.pnlEditorSegments.Size = new Size(300, 400);
            this.pnlEditorSegments.Location = new Point(0, 0);
            this.pnlEditorSegments.BackColor = Color.White;
            this.pnlEditorSegments.Visible = false;

            this.btnGeneral = new Button("btnGeneral");
            this.btnGeneral.Location = new Point(5, 5);
            this.btnGeneral.Size = new Size(65, 15);
            this.btnGeneral.Font = FontManager.LoadFont("tahoma", 10);
            this.btnGeneral.Text = "General";
            this.btnGeneral.Click += new EventHandler<MouseButtonEventArgs>(this.BtnGeneral_Click);

            // lblMaxSegments = new Label("lblMaxSegments");
            // lblMaxSegments.Location = new Point(75, 5);
            // lblMaxSegments.Font = Logic.Graphic.FontManager.LoadFont("tahoma", 10);
            // lblMaxSegments.AutoSize = true;
            // lblMaxSegments.Text = "Max Segments:";

            // nudMaxSegments = new NumericUpDown("nudMaxSegments");
            // nudMaxSegments.Location = new Point(lblMaxSegments.X + lblMaxSegments.Width + 5, 5);
            // nudMaxSegments.Size = new System.Drawing.Size(64, 14);
            // nudMaxSegments.Minimum = 1;

            // btnSaveMaxSegments = new Button("btnSaveMaxSegments");
            // btnSaveMaxSegments.Location = new Point(nudMaxSegments.X + nudMaxSegments.Width + 5, 25);
            // btnSaveMaxSegments.Size = new System.Drawing.Size(64, 16);
            // btnSaveMaxSegments.Font = Logic.Graphic.FontManager.LoadFont("tahoma", 10);
            // btnSaveMaxSegments.Text = "Save";
            // btnSaveMaxSegments.Click += new EventHandler<MouseButtonEventArgs>(btnSaveMaxSegments_Click);

            // lblActiveSegment = new Label("lblActiveSegment");
            // lblActiveSegment.Location = new Point(75, 5);
            // lblActiveSegment.Font = Logic.Graphic.FontManager.LoadFont("tahoma", 10);
            // lblActiveSegment.AutoSize = true;
            // lblActiveSegment.Text = "Segment:";

            // nudActiveSegment = new NumericUpDown("nudActiveSegment");
            // nudActiveSegment.Location = new Point(lblActiveSegment.X + lblActiveSegment.Width + 5, 25);
            // nudActiveSegment.Size = new System.Drawing.Size(64, 14);
            // nudActiveSegment.Minimum = 1;
            // nudActiveSegment.Maximum = 1;
            this.btnAddSegment = new Button("btnAddSegment");
            this.btnAddSegment.Size = new Size(64, 16);
            this.btnAddSegment.Font = FontManager.LoadFont("tahoma", 10);
            this.btnAddSegment.Location = new Point(5, 210);
            this.btnAddSegment.Text = "Add";
            this.btnAddSegment.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddSegment_Click);

            this.btnRemoveSegment = new Button("btnRemoveSegment");
            this.btnRemoveSegment.Size = new Size(64, 16);
            this.btnRemoveSegment.Font = FontManager.LoadFont("tahoma", 10);
            this.btnRemoveSegment.Location = new Point(this.btnAddSegment.X + this.btnAddSegment.Width + 5, 210);
            this.btnRemoveSegment.Text = "Remove";
            this.btnRemoveSegment.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveSegment_Click);

            this.btnLoadSegment = new Button("btnLoadSegment");
            this.btnLoadSegment.Size = new Size(64, 16);
            this.btnLoadSegment.Font = FontManager.LoadFont("tahoma", 10);
            this.btnLoadSegment.Location = new Point(this.btnRemoveSegment.X + this.btnRemoveSegment.Width + 5, 210);
            this.btnLoadSegment.Text = "Load";
            this.btnLoadSegment.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadSegment_Click);

            this.btnSaveSegment = new Button("btnSaveSegment");
            this.btnSaveSegment.Size = new Size(64, 16);
            this.btnSaveSegment.Font = FontManager.LoadFont("tahoma", 10);
            this.btnSaveSegment.Location = new Point(this.btnLoadSegment.X + this.btnLoadSegment.Width + 5, this.btnLoadSegment.Y);
            this.btnSaveSegment.Text = "Save";
            this.btnSaveSegment.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSaveSegment_Click);

            this.lblActions = new Label("lblActions");
            this.lblActions.Location = new Point(75, 5);
            this.lblActions.Font = FontManager.LoadFont("tahoma", 10);
            this.lblActions.AutoSize = true;
            this.lblActions.Text = "Action:";

            this.cmbSegmentTypes = new ComboBox("cmbSegmentTypes");
            this.cmbSegmentTypes.Location = new Point(this.lblActions.X + this.lblActions.Width + 5, 5);
            this.cmbSegmentTypes.Size = new Size(150, 16);
            string[] storySegmentActions = Enum.GetNames(typeof(Enums.StoryAction));
            for (int i = 0; i < storySegmentActions.Length; i++)
            {
                this.cmbSegmentTypes.Items.Add(new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), storySegmentActions[i]));
            }

            this.cmbSegmentTypes.ItemSelected += new EventHandler(this.CmbSegmentTypes_ItemSelected);
            this.cmbSegmentTypes.SelectItem(0);

            this.lbxSegments = new ScrollingListBox("lbxSegments");
            this.lbxSegments.Location = new Point(10, 230);
            this.lbxSegments.Size = new Size(280, 140);
            this.lbxSegments.MultiSelect = false;
            this.pnlStoryList.AddWidget(this.lbxStoryList);
            this.pnlStoryList.AddWidget(this.btnBack);
            this.pnlStoryList.AddWidget(this.btnForward);
            this.pnlStoryList.AddWidget(this.btnEdit);
            this.pnlStoryList.AddWidget(this.btnCancel);
            this.pnlEditorGeneral.AddWidget(this.btnSegments);
            this.pnlEditorGeneral.AddWidget(this.lblName);
            this.pnlEditorGeneral.AddWidget(this.txtName);
            this.pnlEditorGeneral.AddWidget(this.lblStoryStart);
            this.pnlEditorGeneral.AddWidget(this.nudStoryStart);
            this.pnlEditorGeneral.AddWidget(this.lblExitAndContinue);
            this.pnlEditorGeneral.AddWidget(this.lbxExitAndContinue);
            this.pnlEditorGeneral.AddWidget(this.btnAddExitAndContinue);
            this.pnlEditorGeneral.AddWidget(this.nudExitAndContinueCheckpoint);
            this.pnlEditorGeneral.AddWidget(this.btnRemoveExitAndContinue);

            this.pnlEditorGeneral.AddWidget(this.btnEditorCancel);
            this.pnlEditorGeneral.AddWidget(this.btnEditorOK);
            this.pnlEditorSegments.AddWidget(this.btnGeneral);

            // pnlEditorSegments.AddWidget(lblMaxSegments);
            // pnlEditorSegments.AddWidget(nudMaxSegments);
            // pnlEditorSegments.AddWidget(btnSaveMaxSegments);
            // pnlEditorSegments.AddWidget(lblActiveSegment);
            // pnlEditorSegments.AddWidget(nudActiveSegment);
            this.pnlEditorSegments.AddWidget(this.btnAddSegment);
            this.pnlEditorSegments.AddWidget(this.btnRemoveSegment);
            this.pnlEditorSegments.AddWidget(this.btnLoadSegment);
            this.pnlEditorSegments.AddWidget(this.btnSaveSegment);
            this.pnlEditorSegments.AddWidget(this.lblActions);
            this.pnlEditorSegments.AddWidget(this.cmbSegmentTypes);
            this.pnlEditorSegments.AddWidget(this.lbxSegments);
            this.AddWidget(this.pnlStoryList);
            this.AddWidget(this.pnlEditorGeneral);
            this.AddWidget(this.pnlEditorSegments);

            this.RefreshStoryList();
        }

        public void RefreshSegmentList()
        {
            this.lbxSegments.Items.Clear();
            for (int i = 0; i < this.story.Segments.Count; i++)
            {
                this.lbxSegments.Items.Add(new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), (i + 1) + ": " + this.story.Segments[i].Action.ToString()));
            }
        }

        public void RefreshStoryList()
        {
            for (int i = 0; i < 10; i++)
            {
                if ((i + (this.currentTen * 10)) < MaxInfo.MaxStories)
                {
                    ((ListBoxTextItem)this.lbxStoryList.Items[i]).Text = ((i + 1) + (10 * this.currentTen)) + ": " + Stories.StoryHelper.Stories[i + (10 * this.currentTen)].Name;
                }
                else
                {
                    ((ListBoxTextItem)this.lbxStoryList.Items[i]).Text = "---";
                }
            }
        }

        public void SaveSayOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.Say;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Text", this.txtSayText.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Mugshot", this.nudSayMugshot.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Speed", this.nudSaySpeed.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("PauseLocation", this.nudSayPause.Value.ToString());
        }

        public void LoadSayOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.txtSayText.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Text") ?? string.Empty;
            this.nudSayMugshot.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Mugshot").ToInt();
            this.nudSaySpeed.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Speed").ToInt();
            this.nudSayPause.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("PauseLocation").ToInt();
        }

        public void SwitchToPauseOptions()
        {
            if (this.pnlPauseAction == null)
            {
                this.pnlPauseAction = new Panel("pnlPauseAction");
                this.pnlPauseAction.Size = new Size(300, 180);
                this.pnlPauseAction.Location = new Point(0, 30);
                this.pnlPauseAction.BackColor = Color.Transparent;
                this.pnlPauseAction.Hide();

                this.lblPauseLength = new Label("lblPauseLength");
                this.lblPauseLength.Location = new Point(5, 5);
                this.lblPauseLength.Font = FontManager.LoadFont("tahoma", 10);
                this.lblPauseLength.AutoSize = true;
                this.lblPauseLength.Text = "Length:";

                this.nudPauseLength = new NumericUpDown("nudPauseLength");
                this.nudPauseLength.Location = new Point(75, 5);
                this.nudPauseLength.Size = new Size(100, 14);
                this.nudPauseLength.Maximum = int.MaxValue;

                this.pnlPauseAction.AddWidget(this.lblPauseLength);
                this.pnlPauseAction.AddWidget(this.nudPauseLength);

                this.pnlEditorSegments.AddWidget(this.pnlPauseAction);
            }

            this.HideAllOptionPanels();
            this.pnlPauseAction.Show();
        }

        public void SavePauseOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.Pause;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Length", this.nudPauseLength.Value.ToString());
        }

        public void LoadPauseOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.nudPauseLength.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Length").ToInt();
        }

        public void SwitchToMapVisibilityOptions()
        {
            if (this.pnlMapVisibilityAction == null)
            {
                this.pnlMapVisibilityAction = new Panel("pnlMapVisibilityAction");
                this.pnlMapVisibilityAction.Size = new Size(300, 180);
                this.pnlMapVisibilityAction.Location = new Point(0, 30);
                this.pnlMapVisibilityAction.BackColor = Color.Transparent;
                this.pnlMapVisibilityAction.Hide();

                this.chkMapVisibilityVisible = new CheckBox("chkMapVisibilityVisible");
                this.chkMapVisibilityVisible.Location = new Point(5, 5);
                this.chkMapVisibilityVisible.Size = new Size(100, 14);
                this.chkMapVisibilityVisible.Font = FontManager.LoadFont("tahoma", 10);
                this.chkMapVisibilityVisible.BackColor = Color.Transparent;
                this.chkMapVisibilityVisible.Text = "Visible";

                this.pnlMapVisibilityAction.AddWidget(this.chkMapVisibilityVisible);

                this.pnlEditorSegments.AddWidget(this.pnlMapVisibilityAction);
            }

            this.HideAllOptionPanels();
            this.pnlMapVisibilityAction.Show();
        }

        public void SaveMapVisibilityOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.MapVisibility;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Visible", this.chkMapVisibilityVisible.Checked.ToString());
        }

        public void LoadMapVisibilityOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.chkMapVisibilityVisible.Checked = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Visible").ToBool();
        }

        public void SwitchToPadlockOptions()
        {
            if (this.pnlPadlockAction == null)
            {
                this.pnlPadlockAction = new Panel("pnlPadlockAction");
                this.pnlPadlockAction.Size = new Size(300, 180);
                this.pnlPadlockAction.Location = new Point(0, 30);
                this.pnlPadlockAction.BackColor = Color.Transparent;
                this.pnlPadlockAction.Hide();

                this.chkPadlockState = new CheckBox("chkPadlockState");
                this.chkPadlockState.Location = new Point(5, 5);
                this.chkPadlockState.Size = new Size(100, 14);
                this.chkPadlockState.Font = FontManager.LoadFont("tahoma", 10);
                this.chkPadlockState.BackColor = Color.Transparent;
                this.chkPadlockState.Text = "Lock";

                this.pnlPadlockAction.AddWidget(this.chkPadlockState);

                this.pnlEditorSegments.AddWidget(this.pnlPadlockAction);
            }

            this.HideAllOptionPanels();
            this.pnlPadlockAction.Show();
        }

        public void SavePadlockOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.Padlock;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("State", this.chkPadlockState.Checked ? "Lock" : "Unlock");
        }

        public void LoadPadlockOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.chkPadlockState.Checked = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("State") == "Lock";
        }

        public void SwitchToPlayMusicOptions()
        {
            if (this.pnlPlayMusicAction == null)
            {
                this.pnlPlayMusicAction = new Panel("pnlPlayMusicAction");
                this.pnlPlayMusicAction.Size = new Size(300, 180);
                this.pnlPlayMusicAction.Location = new Point(0, 30);
                this.pnlPlayMusicAction.BackColor = Color.Transparent;
                this.pnlPlayMusicAction.Hide();

                this.lbxPlayMusicPicker = new ScrollingListBox("lbxPlayMusicPicker");
                this.lbxPlayMusicPicker.Location = new Point(5, 5);
                this.lbxPlayMusicPicker.Size = new Size(200, 100);
                SdlDotNet.Graphics.Font font = FontManager.LoadFont("tahoma", 10);
                string[] musicFiles = System.IO.Directory.GetFiles(IO.Paths.MusicPath);
                for (int i = 0; i < musicFiles.Length; i++)
                {
                    this.lbxPlayMusicPicker.Items.Add(new ListBoxTextItem(font, System.IO.Path.GetFileName(musicFiles[i])));
                }

                this.btnPlayMusicPlay = new Button("btnPlayMusicPlay");
                this.btnPlayMusicPlay.Location = new Point(210, 5);
                this.btnPlayMusicPlay.Size = new Size(64, 15);
                this.btnPlayMusicPlay.Font = FontManager.LoadFont("tahoma", 10);
                this.btnPlayMusicPlay.Text = "Play";
                this.btnPlayMusicPlay.Click += new EventHandler<MouseButtonEventArgs>(this.BtnPlayMusicPlay_Click);

                this.btnPlayMusicStop = new Button("btnPlayMusicStop");
                this.btnPlayMusicStop.Location = new Point(210, 25);
                this.btnPlayMusicStop.Size = new Size(64, 15);
                this.btnPlayMusicStop.Font = FontManager.LoadFont("tahoma", 10);
                this.btnPlayMusicStop.Text = "Stop";
                this.btnPlayMusicStop.Click += new EventHandler<MouseButtonEventArgs>(this.BtnPlayMusicStop_Click);

                this.chkPlayMusicHonorSettings = new CheckBox("chkPlayMusicHonorSettings");
                this.chkPlayMusicHonorSettings.Location = new Point(5, 105);
                this.chkPlayMusicHonorSettings.Size = new Size(100, 14);
                this.chkPlayMusicHonorSettings.Font = FontManager.LoadFont("tahoma", 10);
                this.chkPlayMusicHonorSettings.BackColor = Color.Transparent;
                this.chkPlayMusicHonorSettings.Text = "Honor Settings";

                this.chkPlayMusicLoop = new CheckBox("chkPlayMusicLoop");
                this.chkPlayMusicLoop.Location = new Point(5, 125);
                this.chkPlayMusicLoop.Size = new Size(100, 14);
                this.chkPlayMusicLoop.Font = FontManager.LoadFont("tahoma", 10);
                this.chkPlayMusicLoop.BackColor = Color.Transparent;
                this.chkPlayMusicLoop.Text = "Loop";

                this.pnlPlayMusicAction.AddWidget(this.lbxPlayMusicPicker);
                this.pnlPlayMusicAction.AddWidget(this.btnPlayMusicPlay);
                this.pnlPlayMusicAction.AddWidget(this.btnPlayMusicStop);
                this.pnlPlayMusicAction.AddWidget(this.chkPlayMusicHonorSettings);
                this.pnlPlayMusicAction.AddWidget(this.chkPlayMusicLoop);

                this.pnlEditorSegments.AddWidget(this.pnlPlayMusicAction);
            }

            this.HideAllOptionPanels();
            this.pnlPlayMusicAction.Show();
        }

        public void SavePlayMusicOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.PlayMusic;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            if (this.lbxPlayMusicPicker.SelectedItems.Count == 1)
            {
                this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("File", ((ListBoxTextItem)this.lbxPlayMusicPicker.SelectedItems[0]).Text);
            }
            else
            {
                this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("File", string.Empty);
            }

            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("HonorSettings", this.chkPlayMusicHonorSettings.Checked.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Loop", this.chkPlayMusicLoop.Checked.ToString());
        }

        public void LoadPlayMusicOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.lbxPlayMusicPicker.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("File"));
            this.chkPlayMusicHonorSettings.Checked = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("HonorSettings").ToBool();
            this.chkPlayMusicLoop.Checked = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Loop").ToBool();
        }

        public void SwitchToSayOptions()
        {
            if (this.pnlSayAction == null)
            {
                this.pnlSayAction = new Panel("pnlSayAction");
                this.pnlSayAction.Size = new Size(300, 180);
                this.pnlSayAction.Location = new Point(0, 30);
                this.pnlSayAction.BackColor = Color.Transparent;
                this.pnlSayAction.Hide();

                this.lblSayText = new Label("lblSayText");
                this.lblSayText.Location = new Point(5, 5);
                this.lblSayText.Font = FontManager.LoadFont("tahoma", 10);
                this.lblSayText.AutoSize = true;
                this.lblSayText.Text = "Text:";

                this.txtSayText = new TextBox("txtSayText");
                this.txtSayText.Location = new Point(75, 5);
                this.txtSayText.Size = new Size(200, 16);

                this.lblSayMugshot = new Label("lblSayMugshot");
                this.lblSayMugshot.Location = new Point(5, 25);
                this.lblSayMugshot.Font = FontManager.LoadFont("tahoma", 10);
                this.lblSayMugshot.AutoSize = true;
                this.lblSayMugshot.Text = "Mugshot:";

                this.nudSayMugshot = new NumericUpDown("nudSayMugshot");
                this.nudSayMugshot.Location = new Point(75, 25);
                this.nudSayMugshot.Size = new Size(100, 14);
                this.nudSayMugshot.Maximum = int.MaxValue;
                this.nudSayMugshot.Minimum = -1;
                this.nudSayMugshot.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudSayMugshot_ValueChanged);

                this.pbxSayMugshot = new PictureBox("pbxSayMugshot");
                this.pbxSayMugshot.Location = new Point(this.nudSayMugshot.X + this.nudSayMugshot.Width + 5, 25);
                this.pbxSayMugshot.Size = new Size(40, 40);

                this.lblSaySpeed = new Label("lblSaySpeed");
                this.lblSaySpeed.Location = new Point(5, 45);
                this.lblSaySpeed.Font = FontManager.LoadFont("tahoma", 10);
                this.lblSaySpeed.AutoSize = true;
                this.lblSaySpeed.Text = "Speed:";

                this.nudSaySpeed = new NumericUpDown("nudSaySpeed");
                this.nudSaySpeed.Location = new Point(75, 45);
                this.nudSaySpeed.Size = new Size(100, 14);
                this.nudSaySpeed.Maximum = int.MaxValue;

                this.lblSayPause = new Label("lblSayPause");
                this.lblSayPause.Location = new Point(5, 65);
                this.lblSayPause.Font = FontManager.LoadFont("tahoma", 10);
                this.lblSayPause.AutoSize = true;
                this.lblSayPause.Text = "Pause:";

                this.nudSayPause = new NumericUpDown("nudSayPause");
                this.nudSayPause.Location = new Point(75, 65);
                this.nudSayPause.Size = new Size(100, 14);
                this.nudSayPause.Maximum = int.MaxValue;

                this.pnlSayAction.AddWidget(this.lblSayText);
                this.pnlSayAction.AddWidget(this.txtSayText);
                this.pnlSayAction.AddWidget(this.lblSayMugshot);
                this.pnlSayAction.AddWidget(this.nudSayMugshot);
                this.pnlSayAction.AddWidget(this.pbxSayMugshot);
                this.pnlSayAction.AddWidget(this.lblSaySpeed);
                this.pnlSayAction.AddWidget(this.nudSaySpeed);
                this.pnlSayAction.AddWidget(this.lblSayPause);
                this.pnlSayAction.AddWidget(this.nudSayPause);

                this.pnlEditorSegments.AddWidget(this.pnlSayAction);
            }

            this.txtSayText.Text = string.Empty;
            this.HideAllOptionPanels();
            this.pnlSayAction.Show();
        }

        public void SwitchToStopMusicOptions()
        {
            if (this.pnlStopMusicAction == null)
            {
                this.pnlStopMusicAction = new Panel("pnlStopMusicAction");
                this.pnlStopMusicAction.Size = new Size(300, 180);
                this.pnlStopMusicAction.Location = new Point(0, 30);
                this.pnlStopMusicAction.BackColor = Color.Transparent;
                this.pnlStopMusicAction.Hide();

                this.pnlEditorSegments.AddWidget(this.pnlStopMusicAction);
            }

            this.HideAllOptionPanels();
            this.pnlStopMusicAction.Show();
        }

        public void SaveStopMusicOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.StopMusic;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
        }

        public void LoadStopMusicOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
        }

        public void SwitchToShowImageOptions()
        {
            if (this.pnlShowImageAction == null)
            {
                this.pnlShowImageAction = new Panel("pnlShowImageAction");
                this.pnlShowImageAction.Size = new Size(300, 180);
                this.pnlShowImageAction.Location = new Point(0, 30);
                this.pnlShowImageAction.BackColor = Color.Transparent;
                this.pnlShowImageAction.Hide();

                this.lbxShowImageFiles = new ScrollingListBox("lbxShowImageFiles");
                this.lbxShowImageFiles.Location = new Point(5, 5);
                this.lbxShowImageFiles.Size = new Size(200, 100);
                SdlDotNet.Graphics.Font font = FontManager.LoadFont("tahoma", 10);
                string[] imageFiles = System.IO.Directory.GetFiles(IO.Paths.StartupPath + "Story/Images/");
                for (int i = 0; i < imageFiles.Length; i++)
                {
                    this.lbxShowImageFiles.Items.Add(new ListBoxTextItem(font, System.IO.Path.GetFileName(imageFiles[i])));
                }

                this.lblShowImageID = new Label("lblShowImageID");
                this.lblShowImageID.Location = new Point(5, 105);
                this.lblShowImageID.Font = FontManager.LoadFont("tahoma", 10);
                this.lblShowImageID.AutoSize = true;
                this.lblShowImageID.Text = "Image ID:";

                this.txtShowImageID = new TextBox("txtShowImageID");
                this.txtShowImageID.Location = new Point(75, 105);
                this.txtShowImageID.Size = new Size(125, 15);

                this.lblShowImageX = new Label("lblShowImageX");
                this.lblShowImageX.Location = new Point(5, 125);
                this.lblShowImageX.Font = FontManager.LoadFont("tahoma", 10);
                this.lblShowImageX.AutoSize = true;
                this.lblShowImageX.Text = "Image X:";

                this.nudShowImageX = new NumericUpDown("nudShowImageX");
                this.nudShowImageX.Location = new Point(75, 125);
                this.nudShowImageX.Size = new Size(125, 15);
                this.nudShowImageX.Maximum = 640;

                this.lblShowImageY = new Label("lblShowImageY");
                this.lblShowImageY.Location = new Point(5, 145);
                this.lblShowImageY.Font = FontManager.LoadFont("tahoma", 10);
                this.lblShowImageY.AutoSize = true;
                this.lblShowImageY.Text = "Image Y:";

                this.nudShowImageY = new NumericUpDown("nudShowImageY");
                this.nudShowImageY.Location = new Point(75, 145);
                this.nudShowImageY.Size = new Size(125, 15);
                this.nudShowImageY.Maximum = 480;

                this.pnlShowImageAction.AddWidget(this.lbxShowImageFiles);
                this.pnlShowImageAction.AddWidget(this.lblShowImageID);
                this.pnlShowImageAction.AddWidget(this.txtShowImageID);
                this.pnlShowImageAction.AddWidget(this.lblShowImageX);
                this.pnlShowImageAction.AddWidget(this.nudShowImageX);
                this.pnlShowImageAction.AddWidget(this.lblShowImageY);
                this.pnlShowImageAction.AddWidget(this.nudShowImageY);

                this.pnlEditorSegments.AddWidget(this.pnlShowImageAction);
            }

            this.HideAllOptionPanels();
            this.pnlShowImageAction.Show();
        }

        public void SaveShowImageOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.ShowImage;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            if (this.lbxShowImageFiles.SelectedItems.Count == 1)
            {
                this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("File", ((ListBoxTextItem)this.lbxShowImageFiles.SelectedItems[0]).Text);
            }
            else
            {
                this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("File", string.Empty);
            }

            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ImageID", this.txtShowImageID.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("X", this.nudShowImageX.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Y", this.nudShowImageY.Value.ToString());
        }

        public void LoadShowImageOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.lbxShowImageFiles.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("File"));
            this.txtShowImageID.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ImageID") ?? string.Empty;
            this.nudShowImageX.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("X").ToInt();
            this.nudShowImageY.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Y").ToInt();
        }

        public void SwitchToHideImageOptions()
        {
            if (this.pnlHideImageAction == null)
            {
                this.pnlHideImageAction = new Panel("pnlHideImageAction");
                this.pnlHideImageAction.Size = new Size(300, 180);
                this.pnlHideImageAction.Location = new Point(0, 30);
                this.pnlHideImageAction.BackColor = Color.Transparent;
                this.pnlHideImageAction.Hide();

                this.lblHideImageID = new Label("lblHideImageID");
                this.lblHideImageID.Location = new Point(5, 5);
                this.lblHideImageID.Font = FontManager.LoadFont("tahoma", 10);
                this.lblHideImageID.AutoSize = true;
                this.lblHideImageID.Text = "Image ID:";

                this.txtHideImageID = new TextBox("txtHideImageID");
                this.txtHideImageID.Location = new Point(75, 5);
                this.txtHideImageID.Size = new Size(125, 15);

                this.pnlHideImageAction.AddWidget(this.lblHideImageID);
                this.pnlHideImageAction.AddWidget(this.txtHideImageID);

                this.pnlEditorSegments.AddWidget(this.pnlHideImageAction);
            }

            this.HideAllOptionPanels();
            this.pnlHideImageAction.Show();
        }

        public void SaveHideImageOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.HideImage;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ImageID", this.txtHideImageID.Text);
        }

        public void LoadHideImageOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.txtHideImageID.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ImageID") ?? string.Empty;
        }

        public void SwitchToWarpOptions()
        {
            if (this.pnlWarpAction == null)
            {
                this.pnlWarpAction = new Panel("pnlWarpAction");
                this.pnlWarpAction.Size = new Size(300, 180);
                this.pnlWarpAction.Location = new Point(0, 30);
                this.pnlWarpAction.BackColor = Color.Transparent;
                this.pnlWarpAction.Hide();

                this.lblWarpMap = new Label("lblWarpMap");
                this.lblWarpMap.Location = new Point(5, 5);
                this.lblWarpMap.Font = FontManager.LoadFont("tahoma", 10);
                this.lblWarpMap.AutoSize = true;
                this.lblWarpMap.Text = "Map:";

                this.txtWarpMap = new TextBox("txtWarpMap");
                this.txtWarpMap.Location = new Point(75, 5);
                this.txtWarpMap.Size = new Size(125, 15);

                this.lblWarpX = new Label("lblWarpX");
                this.lblWarpX.Location = new Point(5, 25);
                this.lblWarpX.Font = FontManager.LoadFont("tahoma", 10);
                this.lblWarpX.AutoSize = true;
                this.lblWarpX.Text = "X:";

                this.nudWarpX = new NumericUpDown("nudWarpX");
                this.nudWarpX.Location = new Point(75, 25);
                this.nudWarpX.Size = new Size(125, 15);
                this.nudWarpX.Maximum = 50;

                this.lblWarpY = new Label("lblWarpY");
                this.lblWarpY.Location = new Point(5, 45);
                this.lblWarpY.Font = FontManager.LoadFont("tahoma", 10);
                this.lblWarpY.AutoSize = true;
                this.lblWarpY.Text = "Y:";

                this.nudWarpY = new NumericUpDown("nudWarpY");
                this.nudWarpY.Location = new Point(75, 45);
                this.nudWarpY.Size = new Size(125, 15);
                this.nudWarpY.Maximum = 50;

                this.pnlWarpAction.AddWidget(this.lblWarpMap);
                this.pnlWarpAction.AddWidget(this.txtWarpMap);
                this.pnlWarpAction.AddWidget(this.lblWarpX);
                this.pnlWarpAction.AddWidget(this.nudWarpX);
                this.pnlWarpAction.AddWidget(this.lblWarpY);
                this.pnlWarpAction.AddWidget(this.nudWarpY);

                this.pnlEditorSegments.AddWidget(this.pnlWarpAction);
            }

            this.HideAllOptionPanels();
            this.pnlWarpAction.Show();
        }

        public void SaveWarpOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.Warp;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("MapID", this.txtWarpMap.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("X", this.nudWarpX.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Y", this.nudWarpY.Value.ToString());
        }

        public void LoadWarpOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.txtWarpMap.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("MapID") ?? string.Empty;
            this.nudWarpX.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("X").ToInt();
            this.nudWarpY.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Y").ToInt();
        }

        public void SwitchToPlayerPadlockOptions()
        {
            if (this.pnlPlayerPadlockAction == null)
            {
                this.pnlPlayerPadlockAction = new Panel("pnlPlayerPadlockAction");
                this.pnlPlayerPadlockAction.Size = new Size(300, 180);
                this.pnlPlayerPadlockAction.Location = new Point(0, 30);
                this.pnlPlayerPadlockAction.BackColor = Color.Transparent;
                this.pnlPlayerPadlockAction.Hide();

                this.chkPlayerPadlockState = new CheckBox("chkPlayerPadlockState");
                this.chkPlayerPadlockState.Location = new Point(5, 5);
                this.chkPlayerPadlockState.Size = new Size(100, 14);
                this.chkPlayerPadlockState.Font = FontManager.LoadFont("tahoma", 10);
                this.chkPlayerPadlockState.BackColor = Color.Transparent;
                this.chkPlayerPadlockState.Text = "Lock";

                this.pnlPlayerPadlockAction.AddWidget(this.chkPlayerPadlockState);

                this.pnlEditorSegments.AddWidget(this.pnlPlayerPadlockAction);
            }

            this.HideAllOptionPanels();
            this.pnlPlayerPadlockAction.Show();
        }

        public void SavePlayerPadlockOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.PlayerPadlock;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("MovementState", this.chkPlayerPadlockState.Checked ? "Lock" : "Unlock");
        }

        public void LoadPlayerPadlockOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.chkPlayerPadlockState.Checked = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("MovementState") == "Lock";
        }

        public void SwitchToShowBackgroundOptions()
        {
            if (this.pnlShowBackgroundAction == null)
            {
                this.pnlShowBackgroundAction = new Panel("pnlShowBackgroundAction");
                this.pnlShowBackgroundAction.Size = new Size(300, 180);
                this.pnlShowBackgroundAction.Location = new Point(0, 30);
                this.pnlShowBackgroundAction.BackColor = Color.Transparent;
                this.pnlShowBackgroundAction.Hide();

                this.lbxShowBackgroundFiles = new ScrollingListBox("lbxShowBackgroundFiles");
                this.lbxShowBackgroundFiles.Location = new Point(5, 5);
                this.lbxShowBackgroundFiles.Size = new Size(200, 100);
                SdlDotNet.Graphics.Font font = FontManager.LoadFont("tahoma", 10);
                string[] imageFiles = System.IO.Directory.GetFiles(IO.Paths.StartupPath + "Story/Backgrounds/");
                for (int i = 0; i < imageFiles.Length; i++)
                {
                    this.lbxShowBackgroundFiles.Items.Add(new ListBoxTextItem(font, System.IO.Path.GetFileName(imageFiles[i])));
                }

                this.pnlShowBackgroundAction.AddWidget(this.lbxShowImageFiles);

                this.pnlEditorSegments.AddWidget(this.pnlShowBackgroundAction);
            }

            this.HideAllOptionPanels();
            this.pnlShowBackgroundAction.Show();
        }

        public void SaveShowBackgroundOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.ShowBackground;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            if (this.lbxShowBackgroundFiles.SelectedItems.Count == 1)
            {
                this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("File", ((ListBoxTextItem)this.lbxShowBackgroundFiles.SelectedItems[0]).Text);
            }
            else
            {
                this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("File", string.Empty);
            }
        }

        public void LoadShowBackgroundOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.lbxShowBackgroundFiles.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("File"));
        }

        public void SwitchToHideBackgroundOptions()
        {
            if (this.pnlHideBackgroundAction == null)
            {
                this.pnlHideBackgroundAction = new Panel("pnlHideBackgroundAction");
                this.pnlHideBackgroundAction.Size = new Size(300, 180);
                this.pnlHideBackgroundAction.Location = new Point(0, 30);
                this.pnlHideBackgroundAction.BackColor = Color.Transparent;
                this.pnlHideBackgroundAction.Hide();

                this.pnlEditorSegments.AddWidget(this.pnlHideBackgroundAction);
            }

            this.HideAllOptionPanels();
            this.pnlHideBackgroundAction.Show();
        }

        public void SaveHideBackgroundOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.HideBackground;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
        }

        public void LoadHideBackgroundOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
        }

        public void SwitchToCreateFNPCOptions()
        {
            if (this.pnlCreateFNPCAction == null)
            {
                this.pnlCreateFNPCAction = new Panel("pnlCreateFNPCAction");
                this.pnlCreateFNPCAction.Size = new Size(300, 180);
                this.pnlCreateFNPCAction.Location = new Point(0, 30);
                this.pnlCreateFNPCAction.BackColor = Color.Transparent;
                this.pnlCreateFNPCAction.Hide();

                this.lblCreateFNPCID = new Label("lblCreateFNPCID");
                this.lblCreateFNPCID.Location = new Point(5, 5);
                this.lblCreateFNPCID.Font = FontManager.LoadFont("tahoma", 10);
                this.lblCreateFNPCID.AutoSize = true;
                this.lblCreateFNPCID.Text = "ID:";

                this.txtCreateFNPCID = new TextBox("txtCreateFNPCID");
                this.txtCreateFNPCID.Location = new Point(75, 5);
                this.txtCreateFNPCID.Size = new Size(125, 15);

                this.lblCreateFNPCMap = new Label("lblCreateFNPCMap");
                this.lblCreateFNPCMap.Location = new Point(5, 25);
                this.lblCreateFNPCMap.Font = FontManager.LoadFont("tahoma", 10);
                this.lblCreateFNPCMap.AutoSize = true;
                this.lblCreateFNPCMap.Text = "Map:";

                this.txtCreateFNPCMap = new TextBox("txtCreateFNPCMap");
                this.txtCreateFNPCMap.Location = new Point(75, 25);
                this.txtCreateFNPCMap.Size = new Size(125, 15);

                this.lblCreateFNPCX = new Label("lblCreateFNPCX");
                this.lblCreateFNPCX.Location = new Point(5, 45);
                this.lblCreateFNPCX.Font = FontManager.LoadFont("tahoma", 10);
                this.lblCreateFNPCX.AutoSize = true;
                this.lblCreateFNPCX.Text = "X:";

                this.nudCreateFNPCX = new NumericUpDown("nudCreateFNPCX");
                this.nudCreateFNPCX.Location = new Point(75, 45);
                this.nudCreateFNPCX.Size = new Size(125, 15);
                this.nudCreateFNPCX.Maximum = 50;

                this.lblCreateFNPCY = new Label("lblCreateFNPCY");
                this.lblCreateFNPCY.Location = new Point(5, 65);
                this.lblCreateFNPCY.Font = FontManager.LoadFont("tahoma", 10);
                this.lblCreateFNPCY.AutoSize = true;
                this.lblCreateFNPCY.Text = "Y:";

                this.nudCreateFNPCY = new NumericUpDown("nudCreateFNPCY");
                this.nudCreateFNPCY.Location = new Point(75, 65);
                this.nudCreateFNPCY.Size = new Size(125, 15);
                this.nudCreateFNPCY.Maximum = 50;

                this.lblCreateFNPCSprite = new Label("lblCreateFNPCSprite");
                this.lblCreateFNPCSprite.Location = new Point(5, 85);
                this.lblCreateFNPCSprite.Font = FontManager.LoadFont("tahoma", 10);
                this.lblCreateFNPCSprite.AutoSize = true;
                this.lblCreateFNPCSprite.Text = "Sprite:";

                this.nudCreateFNPCSprite = new NumericUpDown("nudCreateFNPCSprite");
                this.nudCreateFNPCSprite.Location = new Point(75, 85);
                this.nudCreateFNPCSprite.Size = new Size(125, 15);
                this.nudCreateFNPCSprite.Maximum = int.MaxValue;
                this.nudCreateFNPCSprite.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudCreateFNPCSprite_ValueChanged);
                this.nudCreateFNPCSprite.Minimum = 1;

                this.pbxCreateFNPCSprite = new PictureBox("pbxCreateFNPCSprite");
                this.pbxCreateFNPCSprite.Location = new Point(205, 85);

                this.pnlCreateFNPCAction.AddWidget(this.lblCreateFNPCID);
                this.pnlCreateFNPCAction.AddWidget(this.txtCreateFNPCID);
                this.pnlCreateFNPCAction.AddWidget(this.lblCreateFNPCMap);
                this.pnlCreateFNPCAction.AddWidget(this.txtCreateFNPCMap);
                this.pnlCreateFNPCAction.AddWidget(this.lblCreateFNPCX);
                this.pnlCreateFNPCAction.AddWidget(this.nudCreateFNPCX);
                this.pnlCreateFNPCAction.AddWidget(this.lblCreateFNPCY);
                this.pnlCreateFNPCAction.AddWidget(this.nudCreateFNPCY);
                this.pnlCreateFNPCAction.AddWidget(this.lblCreateFNPCSprite);
                this.pnlCreateFNPCAction.AddWidget(this.nudCreateFNPCSprite);
                this.pnlCreateFNPCAction.AddWidget(this.pbxCreateFNPCSprite);

                this.pnlEditorSegments.AddWidget(this.pnlCreateFNPCAction);
            }

            this.HideAllOptionPanels();
            this.pnlCreateFNPCAction.Show();
        }

        public void SaveCreateFNPCOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.CreateFNPC;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ID", this.txtCreateFNPCID.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ParentMapID", this.txtCreateFNPCMap.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("X", this.nudCreateFNPCX.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Y", this.nudCreateFNPCY.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Sprite", this.nudCreateFNPCSprite.Value.ToString());
        }

        public void LoadCreateFNPCOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.txtCreateFNPCID.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ID") ?? string.Empty;
            this.txtCreateFNPCMap.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ParentMapID") ?? string.Empty;
            this.nudCreateFNPCX.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("X").ToInt();
            this.nudCreateFNPCY.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Y").ToInt();
            this.nudCreateFNPCSprite.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Sprite").ToInt();
        }

        public void SwitchToMoveFNPCOptions()
        {
            if (this.pnlMoveFNPCAction == null)
            {
                this.pnlMoveFNPCAction = new Panel("pnlMoveFNPCAction");
                this.pnlMoveFNPCAction.Size = new Size(300, 180);
                this.pnlMoveFNPCAction.Location = new Point(0, 30);
                this.pnlMoveFNPCAction.BackColor = Color.Transparent;
                this.pnlMoveFNPCAction.Hide();

                this.lblMoveFNPCID = new Label("lblMoveFNPCID");
                this.lblMoveFNPCID.Location = new Point(5, 5);
                this.lblMoveFNPCID.Font = FontManager.LoadFont("tahoma", 10);
                this.lblMoveFNPCID.AutoSize = true;
                this.lblMoveFNPCID.Text = "ID:";

                this.txtMoveFNPCID = new TextBox("txtMoveFNPCID");
                this.txtMoveFNPCID.Location = new Point(75, 5);
                this.txtMoveFNPCID.Size = new Size(125, 15);

                this.lblMoveFNPCX = new Label("lblMoveFNPCX");
                this.lblMoveFNPCX.Location = new Point(5, 25);
                this.lblMoveFNPCX.Font = FontManager.LoadFont("tahoma", 10);
                this.lblMoveFNPCX.AutoSize = true;
                this.lblMoveFNPCX.Text = "Target X:";

                this.nudMoveFNPCX = new NumericUpDown("nudMoveFNPCX");
                this.nudMoveFNPCX.Location = new Point(75, 25);
                this.nudMoveFNPCX.Size = new Size(125, 15);
                this.nudMoveFNPCX.Maximum = 50;

                this.lblMoveFNPCY = new Label("lblMoveFNPCY");
                this.lblMoveFNPCY.Location = new Point(5, 45);
                this.lblMoveFNPCY.Font = FontManager.LoadFont("tahoma", 10);
                this.lblMoveFNPCY.AutoSize = true;
                this.lblMoveFNPCY.Text = "Target Y:";

                this.nudMoveFNPCY = new NumericUpDown("nudMoveFNPCY");
                this.nudMoveFNPCY.Location = new Point(75, 45);
                this.nudMoveFNPCY.Size = new Size(125, 15);
                this.nudMoveFNPCY.Maximum = 50;

                this.lblMoveFNPCSpeed = new Label("lblMoveFNPCSpeed");
                this.lblMoveFNPCSpeed.Location = new Point(5, 65);
                this.lblMoveFNPCSpeed.Font = FontManager.LoadFont("tahoma", 10);
                this.lblMoveFNPCSpeed.AutoSize = true;
                this.lblMoveFNPCSpeed.Text = "Speed:";

                this.cbxMoveFNPCSpeed = new ComboBox("cbxMoveFNPCSpeed");
                this.cbxMoveFNPCSpeed.Location = new Point(75, 65);
                this.cbxMoveFNPCSpeed.Size = new Size(125, 15);
                string[] values = Enum.GetNames(typeof(Enums.MovementSpeed));
                for (int i = 0; i < values.Length; i++)
                {
                    this.cbxMoveFNPCSpeed.Items.Add(new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), values[i]));
                }

                this.cbxMoveFNPCSpeed.SelectItem(0);

                this.chkMoveFNPCPause = new CheckBox("chkMoveFNPCPause");
                this.chkMoveFNPCPause.Location = new Point(5, 85);
                this.chkMoveFNPCPause.Size = new Size(200, 15);
                this.chkMoveFNPCPause.Font = FontManager.LoadFont("tahoma", 10);
                this.chkMoveFNPCPause.Text = "Pause until complete";

                this.pnlMoveFNPCAction.AddWidget(this.lblMoveFNPCID);
                this.pnlMoveFNPCAction.AddWidget(this.txtMoveFNPCID);
                this.pnlMoveFNPCAction.AddWidget(this.lblMoveFNPCX);
                this.pnlMoveFNPCAction.AddWidget(this.nudMoveFNPCX);
                this.pnlMoveFNPCAction.AddWidget(this.lblMoveFNPCY);
                this.pnlMoveFNPCAction.AddWidget(this.nudMoveFNPCY);
                this.pnlMoveFNPCAction.AddWidget(this.lblMoveFNPCSpeed);
                this.pnlMoveFNPCAction.AddWidget(this.cbxMoveFNPCSpeed);
                this.pnlMoveFNPCAction.AddWidget(this.chkMoveFNPCPause);

                this.pnlEditorSegments.AddWidget(this.pnlMoveFNPCAction);
            }

            this.HideAllOptionPanels();
            this.pnlMoveFNPCAction.Show();
        }

        public void SaveMoveFNPCOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.MoveFNPC;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ID", this.txtMoveFNPCID.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("X", this.nudMoveFNPCX.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Y", this.nudMoveFNPCY.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Speed", this.cbxMoveFNPCSpeed.SelectedIndex.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Pause", this.chkMoveFNPCPause.Checked.ToString());
        }

        public void LoadMoveFNPCOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.txtMoveFNPCID.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ID") ?? string.Empty;
            this.nudMoveFNPCX.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("X").ToInt();
            this.nudMoveFNPCY.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Y").ToInt();
            this.cbxMoveFNPCSpeed.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Speed").ToInt());
            this.chkMoveFNPCPause.Checked = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Pause").ToBool();
        }

        public void SwitchToWarpFNPCOptions()
        {
            if (this.pnlWarpFNPCAction == null)
            {
                this.pnlWarpFNPCAction = new Panel("pnlWarpFNPCAction");
                this.pnlWarpFNPCAction.Size = new Size(300, 180);
                this.pnlWarpFNPCAction.Location = new Point(0, 30);
                this.pnlWarpFNPCAction.BackColor = Color.Transparent;
                this.pnlWarpFNPCAction.Hide();

                this.lblWarpFNPCID = new Label("lblWarpFNPCID");
                this.lblWarpFNPCID.Location = new Point(5, 5);
                this.lblWarpFNPCID.Font = FontManager.LoadFont("tahoma", 10);
                this.lblWarpFNPCID.AutoSize = true;
                this.lblWarpFNPCID.Text = "ID:";

                this.txtWarpFNPCID = new TextBox("txtWarpFNPCID");
                this.txtWarpFNPCID.Location = new Point(75, 5);
                this.txtWarpFNPCID.Size = new Size(125, 15);

                this.lblWarpFNPCX = new Label("lblWarpFNPCX");
                this.lblWarpFNPCX.Location = new Point(5, 25);
                this.lblWarpFNPCX.Font = FontManager.LoadFont("tahoma", 10);
                this.lblWarpFNPCX.AutoSize = true;
                this.lblWarpFNPCX.Text = "Image X:";

                this.nudWarpFNPCX = new NumericUpDown("nudWarpFNPCX");
                this.nudWarpFNPCX.Location = new Point(75, 25);
                this.nudWarpFNPCX.Size = new Size(125, 15);
                this.nudWarpFNPCX.Maximum = 640;

                this.lblWarpFNPCY = new Label("lblWarpFNPCY");
                this.lblWarpFNPCY.Location = new Point(5, 45);
                this.lblWarpFNPCY.Font = FontManager.LoadFont("tahoma", 10);
                this.lblWarpFNPCY.AutoSize = true;
                this.lblWarpFNPCY.Text = "Image Y:";

                this.nudWarpFNPCY = new NumericUpDown("nudWarpFNPCY");
                this.nudWarpFNPCY.Location = new Point(75, 45);
                this.nudWarpFNPCY.Size = new Size(125, 15);
                this.nudWarpFNPCY.Maximum = 480;

                this.pnlWarpFNPCAction.AddWidget(this.lblWarpFNPCID);
                this.pnlWarpFNPCAction.AddWidget(this.txtWarpFNPCID);
                this.pnlWarpFNPCAction.AddWidget(this.lblWarpFNPCX);
                this.pnlWarpFNPCAction.AddWidget(this.nudWarpFNPCX);
                this.pnlWarpFNPCAction.AddWidget(this.lblWarpFNPCY);
                this.pnlWarpFNPCAction.AddWidget(this.nudWarpFNPCY);

                this.pnlEditorSegments.AddWidget(this.pnlWarpFNPCAction);
            }

            this.HideAllOptionPanels();
            this.pnlWarpFNPCAction.Show();
        }

        public void SaveWarpFNPCOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.WarpFNPC;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ID", this.txtWarpFNPCID.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("X", this.nudWarpFNPCX.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Y", this.nudWarpFNPCY.Value.ToString());
        }

        public void LoadWarpFNPCOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.txtShowImageID.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ID") ?? string.Empty;
            this.nudWarpFNPCX.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("X").ToInt();
            this.nudWarpFNPCY.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Y").ToInt();
        }

        public void SwitchToChangeFNPCDirOptions()
        {
            if (this.pnlChangeFNPCDirAction == null)
            {
                this.pnlChangeFNPCDirAction = new Panel("pnlChangeFNPCDirAction");
                this.pnlChangeFNPCDirAction.Size = new Size(300, 180);
                this.pnlChangeFNPCDirAction.Location = new Point(0, 30);
                this.pnlChangeFNPCDirAction.BackColor = Color.Transparent;
                this.pnlChangeFNPCDirAction.Hide();

                this.lblChangeFNPCDirID = new Label("lblChangeFNPCDirID");
                this.lblChangeFNPCDirID.Location = new Point(5, 5);
                this.lblChangeFNPCDirID.Font = FontManager.LoadFont("tahoma", 10);
                this.lblChangeFNPCDirID.AutoSize = true;
                this.lblChangeFNPCDirID.Text = "ID:";

                this.txtChangeFNPCDirID = new TextBox("txtChangeFNPCDirID");
                this.txtChangeFNPCDirID.Location = new Point(75, 5);
                this.txtChangeFNPCDirID.Size = new Size(125, 15);

                this.cbxChangeFNPCDirDirection = new ComboBox("cbxChangeFNPCDirDirection");
                this.cbxChangeFNPCDirDirection.Location = new Point(5, 25);
                this.cbxChangeFNPCDirDirection.Size = new Size(200, 15);
                string[] values = Enum.GetNames(typeof(Enums.Direction));
                for (int i = 0; i < values.Length; i++)
                {
                    this.cbxChangeFNPCDirDirection.Items.Add(new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), values[i]));
                }

                this.cbxChangeFNPCDirDirection.SelectItem(0);

                this.pnlChangeFNPCDirAction.AddWidget(this.lblChangeFNPCDirID);
                this.pnlChangeFNPCDirAction.AddWidget(this.txtChangeFNPCDirID);
                this.pnlChangeFNPCDirAction.AddWidget(this.cbxChangeFNPCDirDirection);

                this.pnlEditorSegments.AddWidget(this.pnlChangeFNPCDirAction);
            }

            this.HideAllOptionPanels();
            this.pnlChangeFNPCDirAction.Show();
        }

        public void SaveChangeFNPCDirOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.ChangeFNPCDir;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ID", this.txtChangeFNPCDirID.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Direction", this.cbxChangeFNPCDirDirection.SelectedIndex.ToString());
        }

        public void LoadChangeFNPCDirOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.txtChangeFNPCDirID.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ID") ?? string.Empty;
            this.cbxChangeFNPCDirDirection.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Direction").ToInt());
        }

        public void SwitchToDeleteFNPCOptions()
        {
            if (this.pnlDeleteFNPCAction == null)
            {
                this.pnlDeleteFNPCAction = new Panel("pnlDeleteFNPCAction");
                this.pnlDeleteFNPCAction.Size = new Size(300, 180);
                this.pnlDeleteFNPCAction.Location = new Point(0, 30);
                this.pnlDeleteFNPCAction.BackColor = Color.Transparent;
                this.pnlDeleteFNPCAction.Hide();

                this.lblDeleteFNPCID = new Label("lblDeleteFNPCID");
                this.lblDeleteFNPCID.Location = new Point(5, 5);
                this.lblDeleteFNPCID.Font = FontManager.LoadFont("tahoma", 10);
                this.lblDeleteFNPCID.AutoSize = true;
                this.lblDeleteFNPCID.Text = "ID:";

                this.txtDeleteFNPCID = new TextBox("txtDeleteFNPCID");
                this.txtDeleteFNPCID.Location = new Point(75, 5);
                this.txtDeleteFNPCID.Size = new Size(125, 15);

                this.pnlDeleteFNPCAction.AddWidget(this.lblDeleteFNPCID);
                this.pnlDeleteFNPCAction.AddWidget(this.txtDeleteFNPCID);

                this.pnlEditorSegments.AddWidget(this.pnlDeleteFNPCAction);
            }

            this.HideAllOptionPanels();
            this.pnlDeleteFNPCAction.Show();
        }

        public void SaveDeleteFNPCOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.DeleteFNPC;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ID", this.txtDeleteFNPCID.Text);
        }

        public void LoadDeleteFNPCOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.txtDeleteFNPCID.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ID") ?? string.Empty;
        }

        public void SwitchToStoryScriptOptions()
        {
            if (this.pnlStoryScriptAction == null)
            {
                this.pnlStoryScriptAction = new Panel("pnlStoryScriptAction");
                this.pnlStoryScriptAction.Size = new Size(300, 180);
                this.pnlStoryScriptAction.Location = new Point(0, 30);
                this.pnlStoryScriptAction.BackColor = Color.Transparent;
                this.pnlStoryScriptAction.Hide();

                this.lblStoryScriptIndex = new Label("lblStoryScriptIndex");
                this.lblStoryScriptIndex.Location = new Point(5, 5);
                this.lblStoryScriptIndex.Font = FontManager.LoadFont("tahoma", 10);
                this.lblStoryScriptIndex.AutoSize = true;
                this.lblStoryScriptIndex.Text = "Script:";

                this.nudStoryScriptIndex = new NumericUpDown("nudStoryScriptIndex");
                this.nudStoryScriptIndex.Location = new Point(75, 5);
                this.nudStoryScriptIndex.Size = new Size(100, 14);
                this.nudStoryScriptIndex.Maximum = int.MaxValue;

                this.lblStoryScriptParam1 = new Label("lblStoryScriptParam1");
                this.lblStoryScriptParam1.Location = new Point(5, 25);
                this.lblStoryScriptParam1.Font = FontManager.LoadFont("tahoma", 10);
                this.lblStoryScriptParam1.AutoSize = true;
                this.lblStoryScriptParam1.Text = "Script Parameter 1:";

                this.txtStoryScriptParam1 = new TextBox("txtStoryScriptParam1");
                this.txtStoryScriptParam1.Location = new Point(5, 40);
                this.txtStoryScriptParam1.Size = new Size(270, 16);

                this.lblStoryScriptParam2 = new Label("lblStoryScriptParam2");
                this.lblStoryScriptParam2.Location = new Point(5, 60);
                this.lblStoryScriptParam2.Font = FontManager.LoadFont("tahoma", 10);
                this.lblStoryScriptParam2.AutoSize = true;
                this.lblStoryScriptParam2.Text = "Script Parameter 2:";

                this.txtStoryScriptParam2 = new TextBox("txtStoryScriptParam2");
                this.txtStoryScriptParam2.Location = new Point(5, 75);
                this.txtStoryScriptParam2.Size = new Size(270, 16);

                this.lblStoryScriptParam3 = new Label("lblStoryScriptParam3");
                this.lblStoryScriptParam3.Location = new Point(5, 95);
                this.lblStoryScriptParam3.Font = FontManager.LoadFont("tahoma", 10);
                this.lblStoryScriptParam3.AutoSize = true;
                this.lblStoryScriptParam3.Text = "Script Parameter 3:";

                this.txtStoryScriptParam3 = new TextBox("txtStoryScriptParam3");
                this.txtStoryScriptParam3.Location = new Point(5, 110);
                this.txtStoryScriptParam3.Size = new Size(270, 16);

                this.chkStoryScriptPause = new CheckBox("chkStoryScriptPause");
                this.chkStoryScriptPause.Location = new Point(5, 135);
                this.chkStoryScriptPause.Size = new Size(200, 105);
                this.chkStoryScriptPause.Font = FontManager.LoadFont("tahoma", 10);
                this.chkStoryScriptPause.Text = "Pause until complete";

                this.pnlStoryScriptAction.AddWidget(this.lblStoryScriptIndex);
                this.pnlStoryScriptAction.AddWidget(this.nudStoryScriptIndex);
                this.pnlStoryScriptAction.AddWidget(this.lblStoryScriptParam1);
                this.pnlStoryScriptAction.AddWidget(this.txtStoryScriptParam1);
                this.pnlStoryScriptAction.AddWidget(this.lblStoryScriptParam2);
                this.pnlStoryScriptAction.AddWidget(this.txtStoryScriptParam2);
                this.pnlStoryScriptAction.AddWidget(this.lblStoryScriptParam3);
                this.pnlStoryScriptAction.AddWidget(this.txtStoryScriptParam3);
                this.pnlStoryScriptAction.AddWidget(this.chkStoryScriptPause);

                this.pnlEditorSegments.AddWidget(this.pnlStoryScriptAction);
            }

            this.HideAllOptionPanels();
            this.pnlStoryScriptAction.Show();
        }

        public void SaveStoryScriptOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.RunScript;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ScriptIndex", this.nudStoryScriptIndex.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ScriptParam1", this.txtStoryScriptParam1.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ScriptParam2", this.txtStoryScriptParam2.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ScriptParam3", this.txtStoryScriptParam3.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Pause", this.chkStoryScriptPause.Checked.ToString());
        }

        public void LoadStoryScriptOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.nudStoryScriptIndex.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ScriptIndex").ToInt();
            this.txtStoryScriptParam1.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ScriptParam1");
            this.txtStoryScriptParam2.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ScriptParam2");
            this.txtStoryScriptParam3.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ScriptParam3");
            this.chkStoryScriptPause.Checked = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Pause").ToBool();
        }

        public void SwitchToHidePlayersOptions()
        {
            if (this.pnlHidePlayersAction == null)
            {
                this.pnlHidePlayersAction = new Panel("pnlHidePlayersAction");
                this.pnlHidePlayersAction.Size = new Size(300, 180);
                this.pnlHidePlayersAction.Location = new Point(0, 30);
                this.pnlHidePlayersAction.BackColor = Color.Transparent;
                this.pnlHidePlayersAction.Hide();

                this.pnlEditorSegments.AddWidget(this.pnlHidePlayersAction);
            }

            this.HideAllOptionPanels();
            this.pnlHidePlayersAction.Show();
        }

        public void SaveHidePlayersOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.HidePlayers;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
        }

        public void LoadHidePlayersOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
        }

        public void SwitchToShowPlayersOptions()
        {
            if (this.pnlShowPlayersAction == null)
            {
                this.pnlShowPlayersAction = new Panel("pnlShowPlayersAction");
                this.pnlShowPlayersAction.Size = new Size(300, 180);
                this.pnlShowPlayersAction.Location = new Point(0, 30);
                this.pnlShowPlayersAction.BackColor = Color.Transparent;
                this.pnlShowPlayersAction.Hide();

                this.pnlEditorSegments.AddWidget(this.pnlShowPlayersAction);
            }

            this.HideAllOptionPanels();
            this.pnlShowPlayersAction.Show();
        }

        public void SaveShowPlayersOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.ShowPlayers;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
        }

        public void LoadShowPlayersOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
        }

        public void SwitchToFNPCEmotionOptions()
        {
            if (this.pnlFNPCEmotionAction == null)
            {
                this.pnlFNPCEmotionAction = new Panel("pnlFNPCEmotionAction");
                this.pnlFNPCEmotionAction.Size = new Size(300, 180);
                this.pnlFNPCEmotionAction.Location = new Point(0, 30);
                this.pnlFNPCEmotionAction.BackColor = Color.Transparent;
                this.pnlFNPCEmotionAction.Hide();

                this.lblFNPCEmotionID = new Label("lblFNPCEmotionID");
                this.lblFNPCEmotionID.Location = new Point(5, 5);
                this.lblFNPCEmotionID.Font = FontManager.LoadFont("tahoma", 10);
                this.lblFNPCEmotionID.AutoSize = true;
                this.lblFNPCEmotionID.Text = "ID:";

                this.txtFNPCEmotionID = new TextBox("txtFNPCEmotionID");
                this.txtFNPCEmotionID.Location = new Point(75, 5);
                this.txtFNPCEmotionID.Size = new Size(125, 15);

                this.lblFNPCEmotionNum = new Label("lblFNPCEmotionNum");
                this.lblFNPCEmotionNum.Location = new Point(5, 25);
                this.lblFNPCEmotionNum.Font = FontManager.LoadFont("tahoma", 10);
                this.lblFNPCEmotionNum.AutoSize = true;
                this.lblFNPCEmotionNum.Text = "Emotion:";

                this.nudFNPCEmotionNum = new NumericUpDown("nudFNPCEmotionNum");
                this.nudFNPCEmotionNum.Location = new Point(75, 25);
                this.nudFNPCEmotionNum.Size = new Size(125, 15);
                this.nudFNPCEmotionNum.Maximum = 10;

                this.pnlFNPCEmotionAction.AddWidget(this.lblFNPCEmotionID);
                this.pnlFNPCEmotionAction.AddWidget(this.txtFNPCEmotionID);
                this.pnlFNPCEmotionAction.AddWidget(this.lblFNPCEmotionNum);
                this.pnlFNPCEmotionAction.AddWidget(this.nudFNPCEmotionNum);

                this.pnlEditorSegments.AddWidget(this.pnlFNPCEmotionAction);
            }

            this.HideAllOptionPanels();
            this.pnlFNPCEmotionAction.Show();
        }

        public void SaveFNPCEmotionOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.FNPCEmotion;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("ID", this.txtFNPCEmotionID.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Emotion", this.nudFNPCEmotionNum.Value.ToString());
        }

        public void LoadFNPCEmotionOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.txtFNPCEmotionID.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("ID") ?? string.Empty;
            this.nudFNPCEmotionNum.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Emotion").ToInt();
        }

        public void SwitchToChangeWeatherOptions()
        {
            if (this.pnlWeatherAction == null)
            {
                this.pnlWeatherAction = new Panel("pnlWeatherAction");
                this.pnlWeatherAction.Size = new Size(300, 180);
                this.pnlWeatherAction.Location = new Point(0, 30);
                this.pnlWeatherAction.BackColor = Color.Transparent;
                this.pnlWeatherAction.Hide();

                this.lblWeatherType = new Label("lblWeatherType");
                this.lblWeatherType.Location = new Point(5, 5);
                this.lblWeatherType.Font = FontManager.LoadFont("tahoma", 10);
                this.lblWeatherType.AutoSize = true;
                this.lblWeatherType.Text = "Weather:";

                this.cbxWeatherType = new ComboBox("cbxWeatherType");
                this.cbxWeatherType.Location = new Point(75, 5);
                this.cbxWeatherType.Size = new Size(125, 15);
                string[] value = Enum.GetNames(typeof(Enums.Weather));
                for (int i = 0; i < value.Length; i++)
                {
                    this.cbxWeatherType.Items.Add(new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), value[i]));
                }

                this.cbxWeatherType.SelectItem(0);

                this.pnlWeatherAction.AddWidget(this.lblWeatherType);
                this.pnlWeatherAction.AddWidget(this.cbxWeatherType);

                this.pnlEditorSegments.AddWidget(this.pnlWeatherAction);
            }

            this.HideAllOptionPanels();
            this.pnlWeatherAction.Show();
        }

        public void SaveChangeWeatherOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.ChangeWeather;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Weather", this.cbxWeatherType.SelectedIndex.ToString());
        }

        public void LoadChangeWeatherOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.cbxWeatherType.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Weather").ToInt());
        }

        public void SwitchToHideNPCsOptions()
        {
            if (this.pnlHideNPCsAction == null)
            {
                this.pnlHideNPCsAction = new Panel("pnlHideNPCsAction");
                this.pnlHideNPCsAction.Size = new Size(300, 180);
                this.pnlHideNPCsAction.Location = new Point(0, 30);
                this.pnlHideNPCsAction.BackColor = Color.Transparent;
                this.pnlHideNPCsAction.Hide();

                this.pnlEditorSegments.AddWidget(this.pnlHideNPCsAction);
            }

            this.HideAllOptionPanels();
            this.pnlHideNPCsAction.Show();
        }

        public void SaveHideNPCsOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.HideNPCs;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
        }

        public void LoadHideNPCsOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
        }

        public void SwitchToShowNPCsOptions()
        {
            if (this.pnlShowNPCsAction == null)
            {
                this.pnlShowNPCsAction = new Panel("pnlShowNPCsAction");
                this.pnlShowNPCsAction.Size = new Size(300, 180);
                this.pnlShowNPCsAction.Location = new Point(0, 30);
                this.pnlShowNPCsAction.BackColor = Color.Transparent;
                this.pnlShowNPCsAction.Hide();

                this.pnlEditorSegments.AddWidget(this.pnlShowNPCsAction);
            }

            this.HideAllOptionPanels();
            this.pnlShowNPCsAction.Show();
        }

        public void SaveShowNPCsOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.ShowNPCs;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
        }

        public void LoadShowNPCsOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
        }

        public void SwitchToWaitForMapOptions()
        {
            if (this.pnlWaitForMapAction == null)
            {
                this.pnlWaitForMapAction = new Panel("pnlWaitForMapAction");
                this.pnlWaitForMapAction.Size = new Size(300, 180);
                this.pnlWaitForMapAction.Location = new Point(0, 30);
                this.pnlWaitForMapAction.BackColor = Color.Transparent;
                this.pnlWaitForMapAction.Hide();

                this.lblWaitForMapMap = new Label("lblWaitForMapMap");
                this.lblWaitForMapMap.Location = new Point(5, 5);
                this.lblWaitForMapMap.Font = FontManager.LoadFont("tahoma", 10);
                this.lblWaitForMapMap.AutoSize = true;
                this.lblWaitForMapMap.Text = "Map:";

                this.txtWaitForMapMap = new TextBox("txtWaitForMapMap");
                this.txtWaitForMapMap.Location = new Point(75, 5);
                this.txtWaitForMapMap.Size = new Size(125, 15);

                this.pnlWaitForMapAction.AddWidget(this.lblWaitForMapMap);
                this.pnlWaitForMapAction.AddWidget(this.txtWaitForMapMap);

                this.pnlEditorSegments.AddWidget(this.pnlWaitForMapAction);
            }

            this.HideAllOptionPanels();
            this.pnlWaitForMapAction.Show();
        }

        public void SaveWaitForMapOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.WaitForMap;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("MapID", this.txtWaitForMapMap.Text);
        }

        public void LoadWaitForMapOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.txtWaitForMapMap.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("MapID") ?? string.Empty;
        }

        public void SwitchToWaitForLocOptions()
        {
            if (this.pnlWaitForLocAction == null)
            {
                this.pnlWaitForLocAction = new Panel("pnlWaitForLocAction");
                this.pnlWaitForLocAction.Size = new Size(300, 180);
                this.pnlWaitForLocAction.Location = new Point(0, 30);
                this.pnlWaitForLocAction.BackColor = Color.Transparent;
                this.pnlWaitForLocAction.Hide();

                this.lblWaitForLocMap = new Label("lblWaitForLocMap");
                this.lblWaitForLocMap.Location = new Point(5, 5);
                this.lblWaitForLocMap.Font = FontManager.LoadFont("tahoma", 10);
                this.lblWaitForLocMap.AutoSize = true;
                this.lblWaitForLocMap.Text = "Map:";

                this.txtWaitForLocMap = new TextBox("txtWaitForLocMap");
                this.txtWaitForLocMap.Location = new Point(75, 5);
                this.txtWaitForLocMap.Size = new Size(125, 15);

                this.lblWaitForLocX = new Label("lblWaitForLocX");
                this.lblWaitForLocX.Location = new Point(5, 25);
                this.lblWaitForLocX.Font = FontManager.LoadFont("tahoma", 10);
                this.lblWaitForLocX.AutoSize = true;
                this.lblWaitForLocX.Text = "Target X:";

                this.nudWaitForLocX = new NumericUpDown("nudWaitForLocX");
                this.nudWaitForLocX.Location = new Point(75, 25);
                this.nudWaitForLocX.Size = new Size(125, 15);
                this.nudWaitForLocX.Maximum = 50;

                this.lblWaitForLocY = new Label("lblWaitForLocY");
                this.lblWaitForLocY.Location = new Point(5, 45);
                this.lblWaitForLocY.Font = FontManager.LoadFont("tahoma", 10);
                this.lblWaitForLocY.AutoSize = true;
                this.lblWaitForLocY.Text = "Target Y:";

                this.nudWaitForLocY = new NumericUpDown("nudWaitForLocY");
                this.nudWaitForLocY.Location = new Point(75, 45);
                this.nudWaitForLocY.Size = new Size(125, 15);
                this.nudWaitForLocY.Maximum = 50;

                this.pnlWaitForLocAction.AddWidget(this.lblWaitForLocMap);
                this.pnlWaitForLocAction.AddWidget(this.txtWaitForLocMap);
                this.pnlWaitForLocAction.AddWidget(this.lblWaitForLocX);
                this.pnlWaitForLocAction.AddWidget(this.nudWaitForLocX);
                this.pnlWaitForLocAction.AddWidget(this.lblWaitForLocY);
                this.pnlWaitForLocAction.AddWidget(this.nudWaitForLocY);

                this.pnlEditorSegments.AddWidget(this.pnlWaitForLocAction);
            }

            this.HideAllOptionPanels();
            this.pnlWaitForLocAction.Show();
        }

        public void SaveWaitForLocOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.WaitForLoc;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("MapID", this.txtWaitForLocMap.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("X", this.nudWaitForLocX.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Y", this.nudWaitForLocY.Value.ToString());
        }

        public void LoadWaitForLocOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.txtWaitForLocMap.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("MapID") ?? string.Empty;
            this.nudWaitForLocX.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("X").ToInt();
            this.nudWaitForLocY.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Y").ToInt();
        }

        public void SwitchToAskQuestionOptions()
        {
            if (this.pnlAskQuestionAction == null)
            {
                this.pnlAskQuestionAction = new Panel("pnlAskQuestionAction");
                this.pnlAskQuestionAction.Size = new Size(300, 180);
                this.pnlAskQuestionAction.Location = new Point(0, 30);
                this.pnlAskQuestionAction.BackColor = Color.Transparent;
                this.pnlAskQuestionAction.Hide();

                this.lblAskQuestionQuestion = new Label("lblAskQuestionQuestion");
                this.lblAskQuestionQuestion.Location = new Point(5, 5);
                this.lblAskQuestionQuestion.Font = FontManager.LoadFont("tahoma", 10);
                this.lblAskQuestionQuestion.AutoSize = true;
                this.lblAskQuestionQuestion.Text = "Question:";

                this.txtAskQuestionQuestion = new TextBox("txtAskQuestionQuestion");
                this.txtAskQuestionQuestion.Location = new Point(75, 5);
                this.txtAskQuestionQuestion.Size = new Size(125, 15);

                this.lblAskQuestionSegmentOnYes = new Label("lblAskQuestionSegmentOnYes");
                this.lblAskQuestionSegmentOnYes.Location = new Point(5, 25);
                this.lblAskQuestionSegmentOnYes.Font = FontManager.LoadFont("tahoma", 10);
                this.lblAskQuestionSegmentOnYes.AutoSize = true;
                this.lblAskQuestionSegmentOnYes.Text = "Segment On Yes:";

                this.nudAskQuestionSegmentOnYes = new NumericUpDown("nudAskQuestionSegmentOnYes");
                this.nudAskQuestionSegmentOnYes.Location = new Point(75, 25);
                this.nudAskQuestionSegmentOnYes.Size = new Size(125, 15);
                this.nudAskQuestionSegmentOnYes.Minimum = 1;
                this.nudAskQuestionSegmentOnYes.Maximum = this.story.Segments.Count;

                this.lblAskQuestionSegmentOnNo = new Label("lblAskQuestionSegmentOnNo");
                this.lblAskQuestionSegmentOnNo.Location = new Point(5, 45);
                this.lblAskQuestionSegmentOnNo.Font = FontManager.LoadFont("tahoma", 10);
                this.lblAskQuestionSegmentOnNo.AutoSize = true;
                this.lblAskQuestionSegmentOnNo.Text = "Segment On No:";

                this.nudAskQuestionSegmentOnNo = new NumericUpDown("nudAskQuestionSegmentOnNo");
                this.nudAskQuestionSegmentOnNo.Location = new Point(75, 45);
                this.nudAskQuestionSegmentOnNo.Size = new Size(125, 15);
                this.nudAskQuestionSegmentOnNo.Minimum = 1;
                this.nudAskQuestionSegmentOnNo.Maximum = this.story.Segments.Count;

                this.lblAskQuestionMugshot = new Label("lblAskQuestionMugshot");
                this.lblAskQuestionMugshot.Location = new Point(5, 65);
                this.lblAskQuestionMugshot.Font = FontManager.LoadFont("tahoma", 10);
                this.lblAskQuestionMugshot.AutoSize = true;
                this.lblAskQuestionMugshot.Text = "Mugshot:";

                this.nudAskQuestionMugshot = new NumericUpDown("nudAskQuestionMugshot");
                this.nudAskQuestionMugshot.Location = new Point(75, 65);
                this.nudAskQuestionMugshot.Size = new Size(100, 14);
                this.nudAskQuestionMugshot.Maximum = int.MaxValue;
                this.nudAskQuestionMugshot.Minimum = 1;
                this.nudAskQuestionMugshot.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudAskQuestionMugshot_ValueChanged);

                this.pbxAskQuestionMugshot = new PictureBox("pbxAskQuestionMugshot");
                this.pbxAskQuestionMugshot.Location = new Point(this.nudAskQuestionMugshot.X + this.nudAskQuestionMugshot.Width + 5, 65);
                this.pbxAskQuestionMugshot.Size = new Size(40, 40);

                this.pnlAskQuestionAction.AddWidget(this.lblAskQuestionQuestion);
                this.pnlAskQuestionAction.AddWidget(this.txtAskQuestionQuestion);
                this.pnlAskQuestionAction.AddWidget(this.lblAskQuestionSegmentOnYes);
                this.pnlAskQuestionAction.AddWidget(this.nudAskQuestionSegmentOnYes);
                this.pnlAskQuestionAction.AddWidget(this.lblAskQuestionSegmentOnNo);
                this.pnlAskQuestionAction.AddWidget(this.nudAskQuestionSegmentOnNo);
                this.pnlAskQuestionAction.AddWidget(this.lblAskQuestionMugshot);
                this.pnlAskQuestionAction.AddWidget(this.nudAskQuestionMugshot);
                this.pnlAskQuestionAction.AddWidget(this.pbxAskQuestionMugshot);

                this.pnlEditorSegments.AddWidget(this.pnlAskQuestionAction);
            }

            this.HideAllOptionPanels();
            this.pnlAskQuestionAction.Show();
        }

        public void SaveAskQuestionOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.AskQuestion;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Question", this.txtAskQuestionQuestion.Text);
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("SegmentOnYes", this.nudAskQuestionSegmentOnYes.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("SegmentOnNo", this.nudAskQuestionSegmentOnNo.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Mugshot", this.nudAskQuestionMugshot.Value.ToString());
        }

        public void LoadAskQuestionOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.txtAskQuestionQuestion.Text = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Question") ?? string.Empty;
            this.nudAskQuestionSegmentOnYes.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("SegmentOnYes").ToInt();
            this.nudAskQuestionSegmentOnNo.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("SegmentOnNo").ToInt();
            this.nudAskQuestionMugshot.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Mugshot").ToInt();
        }

        public void SwitchToGoToSegmentOptions()
        {
            if (this.pnlGoToSegmentAction == null)
            {
                this.pnlGoToSegmentAction = new Panel("pnlGoToSegmentAction");
                this.pnlGoToSegmentAction.Size = new Size(300, 180);
                this.pnlGoToSegmentAction.Location = new Point(0, 30);
                this.pnlGoToSegmentAction.BackColor = Color.Transparent;
                this.pnlGoToSegmentAction.Hide();

                this.lblGoToSegmentSegment = new Label("lblGoToSegmentSegment");
                this.lblGoToSegmentSegment.Location = new Point(5, 5);
                this.lblGoToSegmentSegment.Font = FontManager.LoadFont("tahoma", 10);
                this.lblGoToSegmentSegment.AutoSize = true;
                this.lblGoToSegmentSegment.Text = "Script:";

                this.nudGoToSegmentSegment = new NumericUpDown("nudGoToSegmentSegment");
                this.nudGoToSegmentSegment.Location = new Point(75, 5);
                this.nudGoToSegmentSegment.Size = new Size(100, 14);
                this.nudGoToSegmentSegment.Minimum = 1;
                this.nudGoToSegmentSegment.Maximum = this.story.Segments.Count;

                this.pnlGoToSegmentAction.AddWidget(this.lblGoToSegmentSegment);
                this.pnlGoToSegmentAction.AddWidget(this.nudGoToSegmentSegment);

                this.pnlEditorSegments.AddWidget(this.pnlGoToSegmentAction);
            }

            this.HideAllOptionPanels();
            this.pnlGoToSegmentAction.Show();
        }

        public void SaveGoToSegmentOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.GoToSegment;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Segment", this.nudGoToSegmentSegment.Value.ToString());
        }

        public void LoadGoToSegmentOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.nudGoToSegmentSegment.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Segment").ToInt();
        }

        public void SwitchToScrollCameraOptions()
        {
            if (this.pnlScrollCameraAction == null)
            {
                this.pnlScrollCameraAction = new Panel("pnlScrollCameraAction");
                this.pnlScrollCameraAction.Size = new Size(300, 180);
                this.pnlScrollCameraAction.Location = new Point(0, 30);
                this.pnlScrollCameraAction.BackColor = Color.Transparent;
                this.pnlScrollCameraAction.Hide();

                this.lblScrollCameraX = new Label("lblScrollCameraX");
                this.lblScrollCameraX.Location = new Point(5, 5);
                this.lblScrollCameraX.Font = FontManager.LoadFont("tahoma", 10);
                this.lblScrollCameraX.AutoSize = true;
                this.lblScrollCameraX.Text = "X:";

                this.nudScrollCameraX = new NumericUpDown("nudScrollCameraX");
                this.nudScrollCameraX.Location = new Point(75, 5);
                this.nudScrollCameraX.Size = new Size(100, 14);
                this.nudScrollCameraX.Maximum = 50;

                this.lblScrollCameraY = new Label("lblScrollCameraY");
                this.lblScrollCameraY.Location = new Point(5, 25);
                this.lblScrollCameraY.Font = FontManager.LoadFont("tahoma", 10);
                this.lblScrollCameraY.AutoSize = true;
                this.lblScrollCameraY.Text = "Y:";

                this.nudScrollCameraY = new NumericUpDown("nudScrollCameraY");
                this.nudScrollCameraY.Location = new Point(75, 25);
                this.nudScrollCameraY.Size = new Size(100, 14);
                this.nudScrollCameraY.Maximum = 50;

                this.lblScrollCameraSpeed = new Label("lblScrollCameraSpeed");
                this.lblScrollCameraSpeed.Location = new Point(5, 45);
                this.lblScrollCameraSpeed.Font = FontManager.LoadFont("tahoma", 10);
                this.lblScrollCameraSpeed.AutoSize = true;
                this.lblScrollCameraSpeed.Text = "Speed:";

                this.nudScrollCameraSpeed = new NumericUpDown("nudScrollCameraSpeed");
                this.nudScrollCameraSpeed.Location = new Point(75, 45);
                this.nudScrollCameraSpeed.Size = new Size(100, 14);
                this.nudScrollCameraSpeed.Maximum = int.MaxValue;

                this.chkScrollCameraPause = new CheckBox("chkScrollCameraPause");
                this.chkScrollCameraPause.Location = new Point(5, 65);
                this.chkScrollCameraPause.Size = new Size(100, 14);
                this.chkScrollCameraPause.Font = FontManager.LoadFont("tahoma", 10);
                this.chkScrollCameraPause.BackColor = Color.Transparent;
                this.chkScrollCameraPause.Text = "Pause until complete";

                this.pnlScrollCameraAction.AddWidget(this.lblScrollCameraX);
                this.pnlScrollCameraAction.AddWidget(this.nudScrollCameraX);
                this.pnlScrollCameraAction.AddWidget(this.lblScrollCameraY);
                this.pnlScrollCameraAction.AddWidget(this.nudScrollCameraY);
                this.pnlScrollCameraAction.AddWidget(this.lblScrollCameraSpeed);
                this.pnlScrollCameraAction.AddWidget(this.nudScrollCameraSpeed);
                this.pnlScrollCameraAction.AddWidget(this.chkScrollCameraPause);

                this.pnlEditorSegments.AddWidget(this.pnlScrollCameraAction);
            }

            this.HideAllOptionPanels();
            this.pnlScrollCameraAction.Show();
        }

        public void SaveScrollCameraOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.ScrollCamera;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("X", this.nudScrollCameraX.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Y", this.nudScrollCameraY.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Speed", this.nudScrollCameraSpeed.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Pause", this.chkScrollCameraPause.Checked.ToString());
        }

        public void LoadScrollCameraOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.nudScrollCameraX.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("X").ToInt();
            this.nudScrollCameraY.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Y").ToInt();
            this.nudScrollCameraSpeed.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Speed").ToInt();
            this.chkScrollCameraPause.Checked = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Pause").ToBool();
        }

        public void SwitchToResetCameraOptions()
        {
            if (this.pnlResetCameraAction == null)
            {
                this.pnlResetCameraAction = new Panel("pnlResetCameraAction");
                this.pnlResetCameraAction.Size = new Size(300, 180);
                this.pnlResetCameraAction.Location = new Point(0, 30);
                this.pnlResetCameraAction.BackColor = Color.Transparent;
                this.pnlResetCameraAction.Hide();

                this.pnlEditorSegments.AddWidget(this.pnlResetCameraAction);
            }

            this.HideAllOptionPanels();
            this.pnlResetCameraAction.Show();
        }

        public void SaveResetCameraOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.ResetCamera;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
        }

        public void LoadResetCameraOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
        }

        public void SwitchToMovePlayerOptions()
        {
            if (this.pnlMovePlayerAction == null)
            {
                this.pnlMovePlayerAction = new Panel("pnlMovePlayerAction");
                this.pnlMovePlayerAction.Size = new Size(300, 180);
                this.pnlMovePlayerAction.Location = new Point(0, 30);
                this.pnlMovePlayerAction.BackColor = Color.Transparent;
                this.pnlMovePlayerAction.Hide();

                this.lblMovePlayerX = new Label("lblMovePlayerX");
                this.lblMovePlayerX.Location = new Point(5, 5);
                this.lblMovePlayerX.Font = FontManager.LoadFont("tahoma", 10);
                this.lblMovePlayerX.AutoSize = true;
                this.lblMovePlayerX.Text = "Target X:";

                this.nudMovePlayerX = new NumericUpDown("nudMovePlayerX");
                this.nudMovePlayerX.Location = new Point(75, 5);
                this.nudMovePlayerX.Size = new Size(125, 15);
                this.nudMovePlayerX.Maximum = 50;

                this.lblMovePlayerY = new Label("lblMovePlayerY");
                this.lblMovePlayerY.Location = new Point(5, 25);
                this.lblMovePlayerY.Font = FontManager.LoadFont("tahoma", 10);
                this.lblMovePlayerY.AutoSize = true;
                this.lblMovePlayerY.Text = "Target Y:";

                this.nudMovePlayerY = new NumericUpDown("nudMovePlayerY");
                this.nudMovePlayerY.Location = new Point(75, 25);
                this.nudMovePlayerY.Size = new Size(125, 15);
                this.nudMovePlayerY.Maximum = 50;

                this.lblMovePlayerSpeed = new Label("lblMovePlayerSpeed");
                this.lblMovePlayerSpeed.Location = new Point(5, 45);
                this.lblMovePlayerSpeed.Font = FontManager.LoadFont("tahoma", 10);
                this.lblMovePlayerSpeed.AutoSize = true;
                this.lblMovePlayerSpeed.Text = "Speed:";

                this.cbxMovePlayerSpeed = new ComboBox("cbxMovePlayerSpeed");
                this.cbxMovePlayerSpeed.Location = new Point(75, 45);
                this.cbxMovePlayerSpeed.Size = new Size(125, 15);
                string[] values = Enum.GetNames(typeof(Enums.MovementSpeed));
                for (int i = 0; i < values.Length; i++)
                {
                    this.cbxMovePlayerSpeed.Items.Add(new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), values[i]));
                }

                this.cbxMovePlayerSpeed.SelectItem(0);

                this.chkMovePlayerPause = new CheckBox("chkMovePlayerPause");
                this.chkMovePlayerPause.Location = new Point(5, 65);
                this.chkMovePlayerPause.Size = new Size(200, 15);
                this.chkMovePlayerPause.Font = FontManager.LoadFont("tahoma", 10);
                this.chkMovePlayerPause.Text = "Pause until complete";

                this.pnlMovePlayerAction.AddWidget(this.lblMovePlayerX);
                this.pnlMovePlayerAction.AddWidget(this.nudMovePlayerX);
                this.pnlMovePlayerAction.AddWidget(this.lblMovePlayerY);
                this.pnlMovePlayerAction.AddWidget(this.nudMovePlayerY);
                this.pnlMovePlayerAction.AddWidget(this.lblMovePlayerSpeed);
                this.pnlMovePlayerAction.AddWidget(this.cbxMovePlayerSpeed);
                this.pnlMovePlayerAction.AddWidget(this.chkMovePlayerPause);

                this.pnlEditorSegments.AddWidget(this.pnlMovePlayerAction);
            }

            this.HideAllOptionPanels();
            this.pnlMovePlayerAction.Show();
        }

        public void SaveMovePlayerOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.MovePlayer;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("X", this.nudMovePlayerX.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Y", this.nudMovePlayerY.Value.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Speed", this.cbxMovePlayerSpeed.SelectedIndex.ToString());
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Pause", this.chkMovePlayerPause.Checked.ToString());
        }

        public void LoadMovePlayerOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.nudMovePlayerX.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("X").ToInt();
            this.nudMovePlayerY.Value = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Y").ToInt();
            this.cbxMovePlayerSpeed.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Speed").ToInt());
            this.chkMovePlayerPause.Checked = this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Pause").ToBool();
        }

        public void SwitchToChangePlayerDirOptions()
        {
            if (this.pnlChangePlayerDirAction == null)
            {
                this.pnlChangePlayerDirAction = new Panel("pnlChangePlayerDirAction");
                this.pnlChangePlayerDirAction.Size = new Size(300, 180);
                this.pnlChangePlayerDirAction.Location = new Point(0, 30);
                this.pnlChangePlayerDirAction.BackColor = Color.Transparent;
                this.pnlChangePlayerDirAction.Hide();

                this.cbxChangePlayerDirDirection = new ComboBox("cbxChangePlayerDirDirection");
                this.cbxChangePlayerDirDirection.Location = new Point(5, 5);
                this.cbxChangePlayerDirDirection.Size = new Size(200, 15);
                string[] values = Enum.GetNames(typeof(Enums.Direction));
                for (int i = 0; i < values.Length; i++)
                {
                    this.cbxChangePlayerDirDirection.Items.Add(new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), values[i]));
                }

                this.pnlChangePlayerDirAction.AddWidget(this.cbxChangePlayerDirDirection);

                this.pnlEditorSegments.AddWidget(this.pnlChangePlayerDirAction);
            }

            this.HideAllOptionPanels();
            this.pnlChangePlayerDirAction.Show();
        }

        public void SaveChangePlayerDirOptions()
        {
            this.story.Segments[this.lbxSegments.SelectedIndex].Action = Enums.StoryAction.ChangePlayerDir;
            this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.Clear();
            this.story.Segments[this.lbxSegments.SelectedIndex].AddParameter("Direction", this.cbxChangePlayerDirDirection.SelectedIndex.ToString());
        }

        public void LoadChangePlayerDirOptions()
        {
            this.cmbSegmentTypes.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Action.ToString());
            this.cbxChangePlayerDirDirection.SelectItem(this.story.Segments[this.lbxSegments.SelectedIndex].Parameters.GetValue("Direction").ToInt());
        }

        public void LoadStory(string[] parse)
        {
            this.pnlStoryList.Visible = false;
            this.pnlEditorGeneral.Visible = true;
            this.Size = new Size(this.pnlEditorGeneral.Width, this.pnlEditorGeneral.Height);

            this.story = new Logic.Editors.Stories.EditableStory();
            this.story.Name = parse[2];

            // parse[3] = revision
            this.story.StoryStart = parse[4].ToInt();

            // story.Segments = new Logic.Editors.Stories.EditableStorySegment[parse[5].ToInt()];
            int n = 6;
            for (int i = 0; i < parse[5].ToInt(); i++)
            {
                int paramCount = parse[n].ToInt();
                Logic.Editors.Stories.EditableStorySegment segment = new Logic.Editors.Stories.EditableStorySegment();
                segment.Action = (Enums.StoryAction)parse[n + 1].ToInt();
                n += 2;
                for (int z = 0; z < paramCount; z++)
                {
                    segment.AddParameter(parse[n], parse[n + 1]);

                    n += 2;
                }

                this.story.Segments.Add(segment);
            }

            int exitAndContinueCount = parse[n].ToInt();
            n++;
            for (int i = 0; i < exitAndContinueCount; i++)
            {
                this.story.ExitAndContinue.Add(parse[n].ToInt());

                n += 1;
            }

            this.RefreshSegmentList();

            // if (story.Segments.Length == 0) {
            //    story.Segments = new Logic.Editors.Stories.EditableStorySegment[1];
            //    story.Segments[0] = new Logic.Editors.Stories.EditableStorySegment();
            // }
            this.txtName.Text = this.story.Name;

            // nudMaxSegments.Value = story.Segments.Length;
            // nudActiveSegment.Maximum = nudMaxSegments.Value;
            this.nudExitAndContinueCheckpoint.Maximum = this.story.Segments.Count;
            this.lbxExitAndContinue.Items.Clear();
            for (int i = 0; i < this.story.ExitAndContinue.Count; i++)
            {
                this.lbxExitAndContinue.Items.Add(new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), this.story.ExitAndContinue[i].ToString()));
            }

            this.btnEdit.Text = "Edit";
        }

        private void BtnRemoveExitAndContinue_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxExitAndContinue.SelectedItems.Count == 1)
            {
                this.story.ExitAndContinue.RemoveAt(this.lbxExitAndContinue.SelectedIndex);
                this.lbxExitAndContinue.Items.RemoveAt(this.lbxExitAndContinue.SelectedIndex);
            }
        }

        private void BtnAddExitAndContinue_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.story.ExitAndContinue.Contains(this.nudExitAndContinueCheckpoint.Value) == false)
            {
                this.lbxExitAndContinue.Items.Add(new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), this.nudExitAndContinueCheckpoint.Value.ToString()));
                this.story.ExitAndContinue.Add(this.nudExitAndContinueCheckpoint.Value);
            }
        }

        private void BtnSaveMaxSegments_Click(object sender, MouseButtonEventArgs e)
        {
            // nudActiveSegment.Maximum = nudMaxSegments.Value;

            // Logic.Editors.Stories.EditableStorySegment[] segmentsOld = new Logic.Editors.Stories.EditableStorySegment[story.Segments.Length];
            // Array.Copy(story.Segments, segmentsOld, story.Segments.Length);
            // story.Segments = new Logic.Editors.Stories.EditableStorySegment[nudMaxSegments.Value];
            // Array.Copy(segmentsOld, story.Segments, System.Math.Min(segmentsOld.Length, story.Segments.Length));
            // for (int i = segmentsOld.Length; i < story.Segments.Length; i++) {
            //    story.Segments[i] = new Logic.Editors.Stories.EditableStorySegment();
            // }

            // nudExitAndContinueCheckpoint.Maximum = story.Segments.Length;
            // if (nudAskQuestionSegmentOnNo != null) {
            //    nudAskQuestionSegmentOnNo.Maximum = story.Segments.Length;
            // }
            // if (nudAskQuestionSegmentOnYes != null) {
            //    nudAskQuestionSegmentOnYes.Maximum = story.Segments.Length;
            // }
            // if (nudGoToSegmentSegment != null) {
            //    nudGoToSegmentSegment.Maximum = story.Segments.Length;
            // }
        }

        private void CmbSegmentTypes_ItemSelected(object sender, EventArgs e)
        {
            if (this.cmbSegmentTypes.SelectedIndex > -1)
            {
                Enums.StoryAction action = (Enums.StoryAction)Enum.Parse(typeof(Enums.StoryAction), this.cmbSegmentTypes.SelectedItem.TextIdentifier);
                switch (action)
                {
                    case Enums.StoryAction.Say:
                        this.SwitchToSayOptions();
                        break;
                    case Enums.StoryAction.Pause:
                        this.SwitchToPauseOptions();
                        break;
                    case Enums.StoryAction.MapVisibility:
                        this.SwitchToMapVisibilityOptions();
                        break;
                    case Enums.StoryAction.Padlock:
                        this.SwitchToPadlockOptions();
                        break;
                    case Enums.StoryAction.PlayMusic:
                        this.SwitchToPlayMusicOptions();
                        break;
                    case Enums.StoryAction.StopMusic:
                        this.SwitchToStopMusicOptions();
                        break;
                    case Enums.StoryAction.ShowImage:
                        this.SwitchToShowImageOptions();
                        break;
                    case Enums.StoryAction.HideImage:
                        this.SwitchToHideImageOptions();
                        break;
                    case Enums.StoryAction.Warp:
                        this.SwitchToWarpOptions();
                        break;
                    case Enums.StoryAction.PlayerPadlock:
                        this.SwitchToPlayerPadlockOptions();
                        break;
                    case Enums.StoryAction.ShowBackground:
                        this.SwitchToShowBackgroundOptions();
                        break;
                    case Enums.StoryAction.HideBackground:
                        this.SwitchToHideBackgroundOptions();
                        break;
                    case Enums.StoryAction.CreateFNPC:
                        this.SwitchToCreateFNPCOptions();
                        break;
                    case Enums.StoryAction.MoveFNPC:
                        this.SwitchToMoveFNPCOptions();
                        break;
                    case Enums.StoryAction.WarpFNPC:
                        this.SwitchToWarpFNPCOptions();
                        break;
                    case Enums.StoryAction.ChangeFNPCDir:
                        this.SwitchToChangeFNPCDirOptions();
                        break;
                    case Enums.StoryAction.DeleteFNPC:
                        this.SwitchToDeleteFNPCOptions();
                        break;
                    case Enums.StoryAction.RunScript:
                        this.SwitchToStoryScriptOptions();
                        break;
                    case Enums.StoryAction.HidePlayers:
                        this.SwitchToHidePlayersOptions();
                        break;
                    case Enums.StoryAction.ShowPlayers:
                        this.SwitchToShowPlayersOptions();
                        break;
                    case Enums.StoryAction.FNPCEmotion:
                        this.SwitchToFNPCEmotionOptions();
                        break;
                    case Enums.StoryAction.ChangeWeather:
                        this.SwitchToChangeWeatherOptions();
                        break;
                    case Enums.StoryAction.HideNPCs:
                        this.SwitchToHideNPCsOptions();
                        break;
                    case Enums.StoryAction.ShowNPCs:
                        this.SwitchToShowNPCsOptions();
                        break;
                    case Enums.StoryAction.WaitForMap:
                        this.SwitchToWaitForMapOptions();
                        break;
                    case Enums.StoryAction.WaitForLoc:
                        this.SwitchToWaitForLocOptions();
                        break;
                    case Enums.StoryAction.AskQuestion:
                        this.SwitchToAskQuestionOptions();
                        break;
                    case Enums.StoryAction.GoToSegment:
                        this.SwitchToGoToSegmentOptions();
                        break;
                    case Enums.StoryAction.ScrollCamera:
                        this.SwitchToScrollCameraOptions();
                        break;
                    case Enums.StoryAction.ResetCamera:
                        this.SwitchToResetCameraOptions();
                        break;
                    case Enums.StoryAction.MovePlayer:
                        this.SwitchToMovePlayerOptions();
                        break;
                    case Enums.StoryAction.ChangePlayerDir:
                        this.SwitchToChangePlayerDirOptions();
                        break;
                }
            }
        }

        private void BtnAddSegment_Click(object sender, MouseButtonEventArgs e)
        {
            int index = this.lbxSegments.SelectedIndex;

            if (index < 0 || index >= this.story.Segments.Count)
            {
                index = this.story.Segments.Count;
                this.story.Segments.Add(new Logic.Editors.Stories.EditableStorySegment());
                this.lbxSegments.Items.Add(new ListBoxTextItem(FontManager.LoadFont("tahoma", 10), "Adding..."));
                this.lbxSegments.SelectItem(index);
            }
            else
            {
                this.story.Segments.Insert(index, new Logic.Editors.Stories.EditableStorySegment());
            }

            Enums.StoryAction action = (Enums.StoryAction)Enum.Parse(typeof(Enums.StoryAction), this.cmbSegmentTypes.SelectedItem.TextIdentifier);
            switch (action)
            {
                case Enums.StoryAction.Say:
                    this.SaveSayOptions();
                    break;
                case Enums.StoryAction.Pause:
                    this.SavePauseOptions();
                    break;
                case Enums.StoryAction.MapVisibility:
                    this.SaveMapVisibilityOptions();
                    break;
                case Enums.StoryAction.Padlock:
                    this.SavePadlockOptions();
                    break;
                case Enums.StoryAction.PlayMusic:
                    this.SavePlayMusicOptions();
                    break;
                case Enums.StoryAction.StopMusic:
                    this.SaveStopMusicOptions();
                    break;
                case Enums.StoryAction.ShowImage:
                    this.SaveShowImageOptions();
                    break;
                case Enums.StoryAction.HideImage:
                    this.SaveHideImageOptions();
                    break;
                case Enums.StoryAction.Warp:
                    this.SaveWarpOptions();
                    break;
                case Enums.StoryAction.PlayerPadlock:
                    this.SavePlayerPadlockOptions();
                    break;
                case Enums.StoryAction.ShowBackground:
                    this.SaveShowBackgroundOptions();
                    break;
                case Enums.StoryAction.HideBackground:
                    this.SaveHideBackgroundOptions();
                    break;
                case Enums.StoryAction.CreateFNPC:
                    this.SaveCreateFNPCOptions();
                    break;
                case Enums.StoryAction.MoveFNPC:
                    this.SaveMoveFNPCOptions();
                    break;
                case Enums.StoryAction.WarpFNPC:
                    this.SaveWarpFNPCOptions();
                    break;
                case Enums.StoryAction.ChangeFNPCDir:
                    this.SaveChangeFNPCDirOptions();
                    break;
                case Enums.StoryAction.DeleteFNPC:
                    this.SaveDeleteFNPCOptions();
                    break;
                case Enums.StoryAction.RunScript:
                    this.SaveStoryScriptOptions();
                    break;
                case Enums.StoryAction.HidePlayers:
                    this.SaveHidePlayersOptions();
                    break;
                case Enums.StoryAction.ShowPlayers:
                    this.SaveShowPlayersOptions();
                    break;
                case Enums.StoryAction.FNPCEmotion:
                    this.SaveFNPCEmotionOptions();
                    break;
                case Enums.StoryAction.ChangeWeather:
                    this.SaveChangeWeatherOptions();
                    break;
                case Enums.StoryAction.HideNPCs:
                    this.SaveHideNPCsOptions();
                    break;
                case Enums.StoryAction.ShowNPCs:
                    this.SaveShowNPCsOptions();
                    break;
                case Enums.StoryAction.WaitForMap:
                    this.SaveWaitForMapOptions();
                    break;
                case Enums.StoryAction.WaitForLoc:
                    this.SaveWaitForLocOptions();
                    break;
                case Enums.StoryAction.AskQuestion:
                    this.SaveAskQuestionOptions();
                    break;
                case Enums.StoryAction.GoToSegment:
                    this.SaveGoToSegmentOptions();
                    break;
                case Enums.StoryAction.ScrollCamera:
                    this.SaveScrollCameraOptions();
                    break;
                case Enums.StoryAction.ResetCamera:
                    this.SaveResetCameraOptions();
                    break;
                case Enums.StoryAction.MovePlayer:
                    this.SaveMovePlayerOptions();
                    break;
                case Enums.StoryAction.ChangePlayerDir:
                    this.SaveChangePlayerDirOptions();
                    break;
            }

            this.RefreshSegmentList();
        }

        private void BtnRemoveSegment_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxSegments.SelectedIndex < 0 || this.lbxSegments.SelectedIndex >= this.story.Segments.Count)
            {
                return;
            }

            this.story.Segments.RemoveAt(this.lbxSegments.SelectedIndex);

            this.RefreshSegmentList();
        }

        private void BtnLoadSegment_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxSegments.SelectedIndex < 0 || this.lbxSegments.SelectedIndex >= this.story.Segments.Count)
            {
                return;
            }

            if (this.story.Segments[this.lbxSegments.SelectedIndex] == null)
            {
                this.story.Segments[this.lbxSegments.SelectedIndex] = new Logic.Editors.Stories.EditableStorySegment();
            }

            switch (this.story.Segments[this.lbxSegments.SelectedIndex].Action)
            {
                case Enums.StoryAction.Say:
                    this.LoadSayOptions();
                    break;
                case Enums.StoryAction.Pause:
                    this.LoadPauseOptions();
                    break;
                case Enums.StoryAction.MapVisibility:
                    this.LoadMapVisibilityOptions();
                    break;
                case Enums.StoryAction.Padlock:
                    this.LoadPadlockOptions();
                    break;
                case Enums.StoryAction.PlayMusic:
                    this.LoadPlayMusicOptions();
                    break;
                case Enums.StoryAction.StopMusic:
                    this.LoadStopMusicOptions();
                    break;
                case Enums.StoryAction.ShowImage:
                    this.LoadShowImageOptions();
                    break;
                case Enums.StoryAction.HideImage:
                    this.LoadHideImageOptions();
                    break;
                case Enums.StoryAction.Warp:
                    this.LoadWarpOptions();
                    break;
                case Enums.StoryAction.PlayerPadlock:
                    this.LoadPlayerPadlockOptions();
                    break;
                case Enums.StoryAction.ShowBackground:
                    this.LoadShowBackgroundOptions();
                    break;
                case Enums.StoryAction.HideBackground:
                    this.LoadHideBackgroundOptions();
                    break;
                case Enums.StoryAction.CreateFNPC:
                    this.LoadCreateFNPCOptions();
                    break;
                case Enums.StoryAction.MoveFNPC:
                    this.LoadMoveFNPCOptions();
                    break;
                case Enums.StoryAction.WarpFNPC:
                    this.LoadWarpFNPCOptions();
                    break;
                case Enums.StoryAction.ChangeFNPCDir:
                    this.LoadChangeFNPCDirOptions();
                    break;
                case Enums.StoryAction.DeleteFNPC:
                    this.LoadDeleteFNPCOptions();
                    break;
                case Enums.StoryAction.RunScript:
                    this.LoadStoryScriptOptions();
                    break;
                case Enums.StoryAction.HidePlayers:
                    this.LoadHidePlayersOptions();
                    break;
                case Enums.StoryAction.ShowPlayers:
                    this.LoadShowPlayersOptions();
                    break;
                case Enums.StoryAction.FNPCEmotion:
                    this.LoadFNPCEmotionOptions();
                    break;
                case Enums.StoryAction.ChangeWeather:
                    this.LoadChangeWeatherOptions();
                    break;
                case Enums.StoryAction.HideNPCs:
                    this.LoadHideNPCsOptions();
                    break;
                case Enums.StoryAction.ShowNPCs:
                    this.LoadShowNPCsOptions();
                    break;
                case Enums.StoryAction.WaitForMap:
                    this.LoadWaitForMapOptions();
                    break;
                case Enums.StoryAction.WaitForLoc:
                    this.LoadWaitForLocOptions();
                    break;
                case Enums.StoryAction.AskQuestion:
                    this.LoadAskQuestionOptions();
                    break;
                case Enums.StoryAction.GoToSegment:
                    this.LoadGoToSegmentOptions();
                    break;
                case Enums.StoryAction.ScrollCamera:
                    this.LoadScrollCameraOptions();
                    break;
                case Enums.StoryAction.ResetCamera:
                    this.LoadResetCameraOptions();
                    break;
                case Enums.StoryAction.MovePlayer:
                    this.LoadMovePlayerOptions();
                    break;
                case Enums.StoryAction.ChangePlayerDir:
                    this.LoadChangePlayerDirOptions();
                    break;
            }
        }

        private void BtnSaveSegment_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.cmbSegmentTypes.SelectedItem == null)
            {
                return;
            }

            if (this.lbxSegments.SelectedIndex < 0 || this.lbxSegments.SelectedIndex >= this.story.Segments.Count)
            {
                return;
            }

            if (this.story.Segments[this.lbxSegments.SelectedIndex] == null)
            {
                this.story.Segments[this.lbxSegments.SelectedIndex] = new Logic.Editors.Stories.EditableStorySegment();
            }

            Enums.StoryAction action = (Enums.StoryAction)Enum.Parse(typeof(Enums.StoryAction), this.cmbSegmentTypes.SelectedItem.TextIdentifier);
            switch (action)
            {
                case Enums.StoryAction.Say:
                    this.SaveSayOptions();
                    break;
                case Enums.StoryAction.Pause:
                    this.SavePauseOptions();
                    break;
                case Enums.StoryAction.MapVisibility:
                    this.SaveMapVisibilityOptions();
                    break;
                case Enums.StoryAction.Padlock:
                    this.SavePadlockOptions();
                    break;
                case Enums.StoryAction.PlayMusic:
                    this.SavePlayMusicOptions();
                    break;
                case Enums.StoryAction.StopMusic:
                    this.SaveStopMusicOptions();
                    break;
                case Enums.StoryAction.ShowImage:
                    this.SaveShowImageOptions();
                    break;
                case Enums.StoryAction.HideImage:
                    this.SaveHideImageOptions();
                    break;
                case Enums.StoryAction.Warp:
                    this.SaveWarpOptions();
                    break;
                case Enums.StoryAction.PlayerPadlock:
                    this.SavePlayerPadlockOptions();
                    break;
                case Enums.StoryAction.ShowBackground:
                    this.SaveShowBackgroundOptions();
                    break;
                case Enums.StoryAction.HideBackground:
                    this.SaveHideBackgroundOptions();
                    break;
                case Enums.StoryAction.CreateFNPC:
                    this.SaveCreateFNPCOptions();
                    break;
                case Enums.StoryAction.MoveFNPC:
                    this.SaveMoveFNPCOptions();
                    break;
                case Enums.StoryAction.WarpFNPC:
                    this.SaveWarpFNPCOptions();
                    break;
                case Enums.StoryAction.ChangeFNPCDir:
                    this.SaveChangeFNPCDirOptions();
                    break;
                case Enums.StoryAction.DeleteFNPC:
                    this.SaveDeleteFNPCOptions();
                    break;
                case Enums.StoryAction.RunScript:
                    this.SaveStoryScriptOptions();
                    break;
                case Enums.StoryAction.HidePlayers:
                    this.SaveHidePlayersOptions();
                    break;
                case Enums.StoryAction.ShowPlayers:
                    this.SaveShowPlayersOptions();
                    break;
                case Enums.StoryAction.FNPCEmotion:
                    this.SaveFNPCEmotionOptions();
                    break;
                case Enums.StoryAction.ChangeWeather:
                    this.SaveChangeWeatherOptions();
                    break;
                case Enums.StoryAction.HideNPCs:
                    this.SaveHideNPCsOptions();
                    break;
                case Enums.StoryAction.ShowNPCs:
                    this.SaveShowNPCsOptions();
                    break;
                case Enums.StoryAction.WaitForMap:
                    this.SaveWaitForMapOptions();
                    break;
                case Enums.StoryAction.WaitForLoc:
                    this.SaveWaitForLocOptions();
                    break;
                case Enums.StoryAction.AskQuestion:
                    this.SaveAskQuestionOptions();
                    break;
                case Enums.StoryAction.GoToSegment:
                    this.SaveGoToSegmentOptions();
                    break;
                case Enums.StoryAction.ScrollCamera:
                    this.SaveScrollCameraOptions();
                    break;
                case Enums.StoryAction.ResetCamera:
                    this.SaveResetCameraOptions();
                    break;
                case Enums.StoryAction.MovePlayer:
                    this.SaveMovePlayerOptions();
                    break;
                case Enums.StoryAction.ChangePlayerDir:
                    this.SaveChangePlayerDirOptions();
                    break;
            }

            this.RefreshSegmentList();
        }

        private void HideAllOptionPanels()
        {
            this.HideIfLoaded(this.pnlSayAction);
            this.HideIfLoaded(this.pnlPauseAction);
            this.HideIfLoaded(this.pnlMapVisibilityAction);
            this.HideIfLoaded(this.pnlPadlockAction);
            this.HideIfLoaded(this.pnlPlayMusicAction);
            this.HideIfLoaded(this.pnlStopMusicAction);
            this.HideIfLoaded(this.pnlShowImageAction);
            this.HideIfLoaded(this.pnlHideImageAction);
            this.HideIfLoaded(this.pnlWarpAction);
            this.HideIfLoaded(this.pnlPlayerPadlockAction);
            this.HideIfLoaded(this.pnlShowBackgroundAction);
            this.HideIfLoaded(this.pnlHideBackgroundAction);
            this.HideIfLoaded(this.pnlCreateFNPCAction);
            this.HideIfLoaded(this.pnlMoveFNPCAction);
            this.HideIfLoaded(this.pnlWarpFNPCAction);
            this.HideIfLoaded(this.pnlChangeFNPCDirAction);
            this.HideIfLoaded(this.pnlDeleteFNPCAction);
            this.HideIfLoaded(this.pnlStoryScriptAction);
            this.HideIfLoaded(this.pnlHidePlayersAction);
            this.HideIfLoaded(this.pnlShowPlayersAction);
            this.HideIfLoaded(this.pnlFNPCEmotionAction);
            this.HideIfLoaded(this.pnlWeatherAction);
            this.HideIfLoaded(this.pnlHideNPCsAction);
            this.HideIfLoaded(this.pnlShowNPCsAction);
            this.HideIfLoaded(this.pnlWaitForMapAction);
            this.HideIfLoaded(this.pnlWaitForLocAction);
            this.HideIfLoaded(this.pnlAskQuestionAction);
            this.HideIfLoaded(this.pnlGoToSegmentAction);
            this.HideIfLoaded(this.pnlScrollCameraAction);
            this.HideIfLoaded(this.pnlResetCameraAction);
            this.HideIfLoaded(this.pnlMovePlayerAction);
            this.HideIfLoaded(this.pnlChangePlayerDirAction);
        }

        private void NudSayMugshot_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (e.NewValue > 0)
            {
                Mugshot mugshot = GraphicsManager.GetMugshot(e.NewValue, string.Empty, 0, 0);
                if (mugshot != null)
                {
                    this.pbxSayMugshot.Image = mugshot.GetEmote(0);
                }
else
                {
                    this.pbxSayMugshot.Image = null;
                }
            }
else
            {
                this.pbxSayMugshot.Image = null;
            }
        }

        private void BtnPlayMusicPlay_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxPlayMusicPicker.SelectedItems.Count == 1)
            {
                Music.Music.AudioPlayer.PlayMusic(((ListBoxTextItem)this.lbxPlayMusicPicker.SelectedItems[0]).Text, 1, true, false);
            }
        }

        private void BtnPlayMusicStop_Click(object sender, MouseButtonEventArgs e)
        {
            Music.Music.AudioPlayer.StopMusic();
        }

        private void NudCreateFNPCSprite_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            SpriteSheet sheet = GraphicsManager.GetSpriteSheet(e.NewValue);
            if (sheet != null)
            {
                this.pbxCreateFNPCSprite.Size = new Size(32, 64);
                this.pbxCreateFNPCSprite.BlitToBuffer(sheet.GetSheet(FrameType.Idle, Enums.Direction.Down), new Rectangle(96, 0, 32, 64));
            }
        }

        private void NudAskQuestionMugshot_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Mugshot mugshot = GraphicsManager.GetMugshot(e.NewValue, string.Empty, 0, 0);
            if (mugshot != null)
            {
                this.pbxAskQuestionMugshot.Image = mugshot.GetEmote(0);
            }
        }

        // int GetActiveSegment() {
        //    return nudActiveSegment.Value - 1;
        // }
        private void HideIfLoaded(Panel pnl)
        {
            if (pnl != null)
            {
                pnl.Hide();
            }
        }

        private void BtnSegments_Click(object sender, MouseButtonEventArgs e)
        {
            this.pnlEditorGeneral.Hide();
            this.pnlEditorSegments.Show();
            this.Size = this.pnlEditorSegments.Size;
        }

        private void BtnGeneral_Click(object sender, MouseButtonEventArgs e)
        {
            this.pnlEditorGeneral.Show();
            this.pnlEditorSegments.Hide();
            this.Size = this.pnlEditorGeneral.Size;
        }

        private void BtnBack_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen > 0)
            {
                this.currentTen--;
            }

            this.RefreshStoryList();
        }

        private void BtnForward_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen < (MaxInfo.MaxMoves / 10))
            {
                this.currentTen++;
            }

            this.RefreshStoryList();
        }

        private void BtnEdit_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxStoryList.SelectedItems.Count == 1)
            {
                string[] index = ((ListBoxTextItem)this.lbxStoryList.SelectedItems[0]).Text.Split(':');
                if (index[0].IsNumeric())
                {
                    this.storyNum = index[0].ToInt() - 1;
                    this.btnEdit.Text = "Loading...";
                }

                Messenger.SendEditStory(this.storyNum);
            }
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            return;
        }

        private void BtnEditorCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.storyNum = -1;
            this.pnlEditorGeneral.Visible = false;
            this.pnlStoryList.Visible = true;
            this.Size = new Size(this.pnlStoryList.Width, this.pnlStoryList.Height);
        }

        private void BtnEditorOK_Click(object sender, MouseButtonEventArgs e)
        {
            this.story.Name = this.txtName.Text;
            this.story.StoryStart = this.nudStoryStart.Value;

            Messenger.SendSaveStory(this.storyNum, this.story);
            this.storyNum = -1;
            this.pnlEditorGeneral.Visible = false;
            this.pnlStoryList.Visible = true;
            this.Size = new Size(this.pnlStoryList.Width, this.pnlStoryList.Height);
        }
    }
}
