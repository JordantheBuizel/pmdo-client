﻿// <copyright file="winShopPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Network;
    using PMU.Core;
    using PMU.Sockets;
    using SdlDotNet.Widgets;

    /// <summary>
    /// Description of winShopPanel.
    /// </summary>
    internal class WinShopPanel : Core.WindowCore
    {
        private int shopNum = -1;
        private int currentTen = 0;

        private readonly Panel pnlShopList;
        private readonly Panel pnlShopEditor;

        private readonly ScrollingListBox lbxShopList;
        private readonly ListBoxTextItem lbiItem;
        private readonly Button btnBack;
        private readonly Button btnForward;
        private readonly Button btnCancel;
        private readonly Button btnEdit;

        private readonly Button btnEditorCancel;
        private readonly Button btnEditorOK;

        private readonly Label lblName;
        private readonly TextBox txtName;
        private readonly Label lblJoinSay;
        private readonly TextBox txtJoinSay;
        private readonly Label lblLeaveSay;
        private readonly TextBox txtLeaveSay;
        private readonly ScrollingListBox lbxShopItems;
        private readonly ListBoxTextItem lbiShopItem;
        private readonly Label lblGiveItem;
        private readonly NumericUpDown nudGiveItem;
        private readonly Label lblGiveAmount;
        private readonly NumericUpDown nudGiveAmount;
        private readonly Label lblGetItem;
        private readonly NumericUpDown nudGetItem;
        private readonly Button btnChange;
        private readonly Button btnShiftUp;
        private readonly Button btnShiftDown;

        private ListPair<int, Shops.ShopItem> shopItemList;

        public WinShopPanel()
            : base("winShopPanel")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new Size(200, 230);
            this.Location = new Point(210, Windows.WindowSwitcher.GameWindow.ActiveTeam.Y + Windows.WindowSwitcher.GameWindow.ActiveTeam.Height + 0);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = true;
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "Shop Panel";

            this.pnlShopList = new Panel("pnlShopList");
            this.pnlShopList.Size = new Size(200, 230);
            this.pnlShopList.Location = new Point(0, 0);
            this.pnlShopList.BackColor = Color.White;
            this.pnlShopList.Visible = true;

            this.pnlShopEditor = new Panel("pnlShopEditor");
            this.pnlShopEditor.Size = new Size(440, 380);
            this.pnlShopEditor.Location = new Point(0, 0);
            this.pnlShopEditor.BackColor = Color.White;
            this.pnlShopEditor.Visible = false;

            this.lbxShopList = new ScrollingListBox("lbxShopList");
            this.lbxShopList.Location = new Point(10, 10);
            this.lbxShopList.Size = new Size(180, 140);
            for (int i = 0; i < 10; i++)
            {
                this.lbiItem = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (i + 1) + ": " + Shops.ShopHelper.Shops[(i + 1) + (10 * this.currentTen)].Name);
                this.lbxShopList.Items.Add(this.lbiItem);
            }

            this.lbxShopList.SelectItem(0);

            this.btnBack = new Button("btnBack");
            this.btnBack.Location = new Point(10, 160);
            this.btnBack.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnBack.Size = new Size(64, 16);
            this.btnBack.Visible = true;
            this.btnBack.Text = "<--";
            this.btnBack.Click += new EventHandler<MouseButtonEventArgs>(this.BtnBack_Click);

            this.btnForward = new Button("btnForward");
            this.btnForward.Location = new Point(126, 160);
            this.btnForward.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnForward.Size = new Size(64, 16);
            this.btnForward.Visible = true;
            this.btnForward.Text = "-->";
            this.btnForward.Click += new EventHandler<MouseButtonEventArgs>(this.BtnForward_Click);

            this.btnEdit = new Button("btnEdit");
            this.btnEdit.Location = new Point(10, 190);
            this.btnEdit.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEdit.Size = new Size(64, 16);
            this.btnEdit.Visible = true;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEdit_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(126, 190);
            this.btnCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnCancel.Size = new Size(64, 16);
            this.btnCancel.Visible = true;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.btnEditorCancel = new Button("btnEditorCancel");
            this.btnEditorCancel.Location = new Point(340, 334);
            this.btnEditorCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorCancel.Size = new Size(64, 16);
            this.btnEditorCancel.Visible = true;
            this.btnEditorCancel.Text = "Cancel";
            this.btnEditorCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorCancel_Click);

            this.btnEditorOK = new Button("btnEditorOK");
            this.btnEditorOK.Location = new Point(250, 334);
            this.btnEditorOK.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorOK.Size = new Size(64, 16);
            this.btnEditorOK.Visible = true;
            this.btnEditorOK.Text = "OK";
            this.btnEditorOK.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorOK_Click);

            this.lblName = new Label("lblName");
            this.lblName.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblName.Text = "Shop Name:";
            this.lblName.AutoSize = true;
            this.lblName.Location = new Point(10, 4);

            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(420, 16);
            this.txtName.Location = new Point(10, 16);

            // txtName.Text = "Loading...";
            this.lblJoinSay = new Label("lblJoinSay");
            this.lblJoinSay.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblJoinSay.Text = "Join Say:";
            this.lblJoinSay.AutoSize = true;
            this.lblJoinSay.Location = new Point(10, 36);

            this.txtJoinSay = new TextBox("txtJoinSay");
            this.txtJoinSay.Size = new Size(420, 16);
            this.txtJoinSay.Location = new Point(10, 48);

            this.lblLeaveSay = new Label("lblLeaveSay");
            this.lblLeaveSay.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblLeaveSay.Text = "Leave Say:";
            this.lblLeaveSay.AutoSize = true;
            this.lblLeaveSay.Location = new Point(10, 68);

            this.txtLeaveSay = new TextBox("txtLeaveSay");
            this.txtLeaveSay.Size = new Size(420, 16);
            this.txtLeaveSay.Location = new Point(10, 80);

            this.lbxShopItems = new ScrollingListBox("lbxShopItems");
            this.lbxShopItems.Location = new Point(10, 100);
            this.lbxShopItems.Size = new Size(220, 240);
            this.lbxShopItems.ItemSelected += new EventHandler(this.LbxShopItems_ItemSelected);

            for (int i = 0; i < MaxInfo.MAXTRADES; i++)
            {
                this.lbiShopItem = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (i + 1) + ": ---");

                this.lbxShopItems.Items.Add(this.lbiShopItem);
            }

            // ListBoxTextItem lbiShopItem;
            this.lblGiveItem = new Label("lblGiveItem");
            this.lblGiveItem.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblGiveItem.Text = "Item Paid [---]:";
            this.lblGiveItem.AutoSize = true;
            this.lblGiveItem.Location = new Point(240, 100);

            this.nudGiveItem = new NumericUpDown("nudGiveItem");
            this.nudGiveItem.Size = new Size(100, 16);
            this.nudGiveItem.Location = new Point(240, 112);
            this.nudGiveItem.Maximum = MaxInfo.MaxItems;
            this.nudGiveItem.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudGiveItem_ValueChanged);

            this.lblGiveAmount = new Label("lblGiveAmount");
            this.lblGiveAmount.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblGiveAmount.Text = "Amount Paid: (integer)";
            this.lblGiveAmount.AutoSize = true;
            this.lblGiveAmount.Location = new Point(240, 132);

            this.nudGiveAmount = new NumericUpDown("nudGiveAmount");
            this.nudGiveAmount.Size = new Size(100, 16);
            this.nudGiveAmount.Maximum = 2147483647;
            this.nudGiveAmount.Location = new Point(240, 144);

            this.lblGetItem = new Label("lblGetItem");
            this.lblGetItem.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblGetItem.Text = "Item Bought [---]:";
            this.lblGetItem.AutoSize = true;
            this.lblGetItem.Location = new Point(240, 164);

            this.nudGetItem = new NumericUpDown("nudGetItem");
            this.nudGetItem.Size = new Size(100, 16);
            this.nudGetItem.Location = new Point(240, 176);
            this.nudGetItem.Maximum = MaxInfo.MaxItems;
            this.nudGetItem.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudGetItem_ValueChanged);

            this.btnShiftUp = new Button("btnShiftUp");
            this.btnShiftUp.Location = new Point(240, 206);
            this.btnShiftUp.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnShiftUp.Size = new Size(64, 16);
            this.btnShiftUp.Visible = true;
            this.btnShiftUp.Text = "Shift Up";
            this.btnShiftUp.Click += new EventHandler<MouseButtonEventArgs>(this.BtnShiftUp_Click);

            this.btnChange = new Button("btnChange");
            this.btnChange.Location = new Point(240, 226);
            this.btnChange.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnChange.Size = new Size(64, 16);
            this.btnChange.Visible = true;
            this.btnChange.Text = "Change";
            this.btnChange.Click += new EventHandler<MouseButtonEventArgs>(this.BtnChange_Click);

            this.btnShiftDown = new Button("btnShiftDown");
            this.btnShiftDown.Location = new Point(240, 246);
            this.btnShiftDown.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnShiftDown.Size = new Size(64, 16);
            this.btnShiftDown.Visible = true;
            this.btnShiftDown.Text = "Shift Down";
            this.btnShiftDown.Click += new EventHandler<MouseButtonEventArgs>(this.BtnShiftDown_Click);

            this.pnlShopList.AddWidget(this.lbxShopList);
            this.pnlShopList.AddWidget(this.btnBack);
            this.pnlShopList.AddWidget(this.btnForward);
            this.pnlShopList.AddWidget(this.btnEdit);
            this.pnlShopList.AddWidget(this.btnCancel);

            this.pnlShopEditor.AddWidget(this.lblName);
            this.pnlShopEditor.AddWidget(this.txtName);
            this.pnlShopEditor.AddWidget(this.lblJoinSay);
            this.pnlShopEditor.AddWidget(this.txtJoinSay);
            this.pnlShopEditor.AddWidget(this.lblLeaveSay);
            this.pnlShopEditor.AddWidget(this.txtLeaveSay);
            this.pnlShopEditor.AddWidget(this.lbxShopItems);

            // pnlShopEditor.AddWidget(lbiShopItem);
            this.pnlShopEditor.AddWidget(this.lblGiveItem);
            this.pnlShopEditor.AddWidget(this.nudGiveItem);
            this.pnlShopEditor.AddWidget(this.lblGiveAmount);
            this.pnlShopEditor.AddWidget(this.nudGiveAmount);
            this.pnlShopEditor.AddWidget(this.lblGetItem);
            this.pnlShopEditor.AddWidget(this.nudGetItem);
            this.pnlShopEditor.AddWidget(this.btnChange);
            this.pnlShopEditor.AddWidget(this.btnShiftUp);
            this.pnlShopEditor.AddWidget(this.btnShiftDown);

            this.pnlShopEditor.AddWidget(this.btnEditorCancel);
            this.pnlShopEditor.AddWidget(this.btnEditorOK);

            this.AddWidget(this.pnlShopList);
            this.AddWidget(this.pnlShopEditor);
        }

        public void LoadShop(string[] parse)
        {
            this.pnlShopList.Visible = false;
            this.pnlShopEditor.Visible = true;
            this.Size = new Size(this.pnlShopEditor.Width, this.pnlShopEditor.Height);

            this.txtName.Text = parse[2];
            this.txtJoinSay.Text = parse[3];
            this.txtLeaveSay.Text = parse[4];

            this.shopItemList = new ListPair<int, Shops.ShopItem>();

            for (int i = 0; i < MaxInfo.MAXTRADES; i++)
            {
                Shops.ShopItem shopItem = new Shops.ShopItem();
                shopItem.GiveItem = parse[5 + (i * 3)].ToInt();
                shopItem.GiveValue = parse[6 + (i * 3)].ToInt();
                shopItem.GetItem = parse[7 + (i * 3)].ToInt();

                this.shopItemList.Add(i, shopItem);
            }

            // if (lbxShopItems.SelectedItems.Count > 0) {

            // lbxShopItems.SelectItem(lbxShopItems.SelectedItems[0].TextIdentifier);

            // }
            this.RefreshShopItemList();
            this.lbxShopItems.SelectItem(0);

            this.btnEdit.Text = "Edit";
        }

        public void RefreshShopList()
        {
            for (int i = 0; i < 10; i++)
            {
                if ((i + (this.currentTen * 10)) < MaxInfo.MaxShops)
                {
                    ((ListBoxTextItem)this.lbxShopList.Items[i]).Text = ((i + 1) + (10 * this.currentTen)) + ": " + Shops.ShopHelper.Shops[(i + 1) + (10 * this.currentTen)].Name;
                }
                else
                {
                    ((ListBoxTextItem)this.lbxShopList.Items[i]).Text = "---";
                }
            }
        }

        private void BtnBack_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen > 0)
            {
                this.currentTen--;
            }

            this.RefreshShopList();
        }

        private void BtnForward_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen < (MaxInfo.MaxShops / 10))
            {
                this.currentTen++;
            }

            this.RefreshShopList();
        }

        private void RefreshShopItemList()
        {
            for (int i = 0; i < MaxInfo.MAXTRADES; i++)
            {
                if (this.shopItemList[i].GiveItem > 0 && this.shopItemList[i].GiveValue > 0 && this.shopItemList[i].GetItem > 0)
                {
                    ((ListBoxTextItem)this.lbxShopItems.Items[i]).Text = (i + 1) + ": " + this.shopItemList[i].GiveValue.ToString() + " " + Items.ItemHelper.Items[this.shopItemList[i].GiveItem].Name + " for " + Items.ItemHelper.Items[this.shopItemList[i].GetItem].Name;
                }
else
                {
                    ((ListBoxTextItem)this.lbxShopItems.Items[i]).Text = (i + 1) + ": ---";
                }
            }
        }

        private void RefreshTransactionData()
        {
            if (this.lbxShopItems.SelectedItems.Count == 1)
            {
                string[] index = ((ListBoxTextItem)this.lbxShopItems.SelectedItems[0]).Text.Split(':');
                if (index[0].IsNumeric())
                {
                    int itemNum = index[0].ToInt() - 1;
                    this.nudGiveItem.Value = this.shopItemList[itemNum].GiveItem;
                    this.nudGiveAmount.Value = this.shopItemList[itemNum].GiveValue;
                    this.nudGetItem.Value = this.shopItemList[itemNum].GetItem;
                }
            }
        }

        private void BtnEdit_Click(object sender, MouseButtonEventArgs e)
        {
            string[] index = ((ListBoxTextItem)this.lbxShopList.SelectedItems[0]).Text.Split(':');
            if (index[0].IsNumeric())
            {
                this.shopNum = index[0].ToInt();
                this.btnEdit.Text = "Loading...";
            }

            Messenger.SendEditShop(this.shopNum);
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void LbxShopItems_ItemSelected(object sender, EventArgs e)
        {
            this.RefreshTransactionData();
        }

        private void BtnShiftUp_Click(object sender, MouseButtonEventArgs e)
        {// change, then shift up
            bool validInput = true;

            if (this.nudGiveItem.Value < 0 || this.nudGiveItem.Value >= MaxInfo.MaxItems)
            {
                validInput = false;
                this.lblGiveItem.ForeColor = Color.Red;
            }
else
            {
                this.lblGiveItem.ForeColor = Color.Black;
            }

            if (this.nudGiveAmount.Value < 0)
            {
                validInput = false;
                this.lblGiveAmount.ForeColor = Color.Red;
            }
else
            {
                this.lblGiveAmount.ForeColor = Color.Black;
            }

            if (this.nudGetItem.Value < 0 || this.nudGetItem.Value >= MaxInfo.MaxItems)
            {
                validInput = false;
                this.lblGetItem.ForeColor = Color.Red;
            }
else
            {
                this.lblGetItem.ForeColor = Color.Black;
            }

            if (validInput && this.lbxShopItems.SelectedItems.Count == 1)
            {
                string[] index = ((ListBoxTextItem)this.lbxShopItems.SelectedItems[0]).Text.Split(':');
                if (index[0].IsNumeric())
                {
                    int itemNum = index[0].ToInt() - 1;
                    if (itemNum <= 0)
                    {
                        this.shopItemList[itemNum].GiveItem = this.nudGiveItem.Value;
                        this.shopItemList[itemNum].GiveValue = this.nudGiveAmount.Value;
                        this.shopItemList[itemNum].GetItem = this.nudGetItem.Value;

                        // lbxShopItems.SelectItem(itemNum);
                        this.RefreshShopItemList();
                    }
else
                    {
                        this.shopItemList[itemNum].GiveItem = this.shopItemList[itemNum - 1].GiveItem;
                        this.shopItemList[itemNum].GiveValue = this.shopItemList[itemNum - 1].GiveValue;
                        this.shopItemList[itemNum].GetItem = this.shopItemList[itemNum - 1].GetItem;
                        this.shopItemList[itemNum - 1].GiveItem = this.nudGiveItem.Value;
                        this.shopItemList[itemNum - 1].GiveValue = this.nudGiveAmount.Value;
                        this.shopItemList[itemNum - 1].GetItem = this.nudGetItem.Value;

                        this.lbxShopItems.SelectItem(itemNum - 1);
                        this.RefreshShopItemList();
                    }
                }
            }
        }

        private void BtnChange_Click(object sender, MouseButtonEventArgs e)
        {
            bool validInput = true;

            if (this.nudGiveItem.Value < 0 || this.nudGiveItem.Value >= MaxInfo.MaxItems)
            {
                validInput = false;
                this.lblGiveItem.ForeColor = Color.Red;
            }
else
            {
                this.lblGiveItem.ForeColor = Color.Black;
            }

            if (this.nudGiveAmount.Value < 0)
            {
                validInput = false;
                this.lblGiveAmount.ForeColor = Color.Red;
            }
else
            {
                this.lblGiveAmount.ForeColor = Color.Black;
            }

            if (this.nudGetItem.Value < 0 || this.nudGetItem.Value >= MaxInfo.MaxItems)
            {
                validInput = false;
                this.lblGetItem.ForeColor = Color.Red;
            }
else
            {
                this.lblGetItem.ForeColor = Color.Black;
            }

            if (validInput && this.lbxShopItems.SelectedItems.Count == 1)
            {
                string[] index = ((ListBoxTextItem)this.lbxShopItems.SelectedItems[0]).Text.Split(':');
                if (index[0].IsNumeric())
                {
                    int itemNum = index[0].ToInt() - 1;
                    this.shopItemList[itemNum].GiveItem = this.nudGiveItem.Value;
                    this.shopItemList[itemNum].GiveValue = this.nudGiveAmount.Value;
                    this.shopItemList[itemNum].GetItem = this.nudGetItem.Value;
                }

                this.RefreshShopItemList();
            }
        }

        private void BtnShiftDown_Click(object sender, MouseButtonEventArgs e)
        {// change, then shift down
            bool validInput = true;

            if (this.nudGiveItem.Value < 0 || this.nudGiveItem.Value >= MaxInfo.MaxItems)
            {
                validInput = false;
                this.lblGiveItem.ForeColor = Color.Red;
            }
else
            {
                this.lblGiveItem.ForeColor = Color.Black;
            }

            if (this.nudGiveAmount.Value < 0)
            {
                validInput = false;
                this.lblGiveAmount.ForeColor = Color.Red;
            }
else
            {
                this.lblGiveAmount.ForeColor = Color.Black;
            }

            if (this.nudGetItem.Value < 0 || this.nudGetItem.Value >= MaxInfo.MaxItems)
            {
                validInput = false;
                this.lblGetItem.ForeColor = Color.Red;
            }
else
            {
                this.lblGetItem.ForeColor = Color.Black;
            }

            if (validInput && this.lbxShopItems.SelectedItems.Count == 1)
            {
                string[] index = ((ListBoxTextItem)this.lbxShopItems.SelectedItems[0]).Text.Split(':');
                if (index[0].IsNumeric())
                {
                    int itemNum = index[0].ToInt() - 1;
                    if (itemNum >= MaxInfo.MAXTRADES - 1)
                    {
                        this.shopItemList[itemNum].GiveItem = this.nudGiveItem.Value;
                        this.shopItemList[itemNum].GiveValue = this.nudGiveAmount.Value;
                        this.shopItemList[itemNum].GetItem = this.nudGetItem.Value;

                        // lbxShopItems.SelectItem(itemNum);
                        this.RefreshShopItemList();
                    }
else
                    {
                        this.shopItemList[itemNum].GiveItem = this.shopItemList[itemNum + 1].GiveItem;
                        this.shopItemList[itemNum].GiveValue = this.shopItemList[itemNum + 1].GiveValue;
                        this.shopItemList[itemNum].GetItem = this.shopItemList[itemNum + 1].GetItem;
                        this.shopItemList[itemNum + 1].GiveItem = this.nudGiveItem.Value;
                        this.shopItemList[itemNum + 1].GiveValue = this.nudGiveAmount.Value;
                        this.shopItemList[itemNum + 1].GetItem = this.nudGetItem.Value;

                        this.lbxShopItems.SelectItem(itemNum + 1);
                        this.RefreshShopItemList();
                    }
                }
            }
        }

        private void NudGiveItem_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.nudGiveItem.Value > 0 && this.nudGiveItem.Value < MaxInfo.MaxItems)
            {
                this.lblGiveItem.Text = "Item Paid (" + Items.ItemHelper.Items[this.nudGiveItem.Value].Name + "):";
            }
else
            {
                this.lblGiveItem.Text = "Item Paid [---]:";
            }
        }

        private void NudGetItem_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.nudGetItem.Value > 0 && this.nudGetItem.Value < MaxInfo.MaxItems)
            {
                this.lblGetItem.Text = "Item Bought (" + Items.ItemHelper.Items[this.nudGetItem.Value].Name + "):";
            }
else
            {
                this.lblGetItem.Text = "Item Bought [---]:";
            }
        }

        private void BtnEditorCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.shopNum = -1;
            this.pnlShopEditor.Visible = false;
            this.pnlShopList.Visible = true;
            this.Size = new Size(this.pnlShopList.Width, this.pnlShopList.Height);
        }

        private void BtnEditorOK_Click(object sender, MouseButtonEventArgs e)
        {
            Shops.Shop shopToSend = new Shops.Shop();

            shopToSend.Name = this.txtName.Text;
            shopToSend.JoinSay = this.txtJoinSay.Text;
            shopToSend.LeaveSay = this.txtLeaveSay.Text;

            for (int j = 0; j < MaxInfo.MAXTRADES; j++)
            {
                shopToSend.Items[j].GiveItem = this.shopItemList[j].GiveItem;
                shopToSend.Items[j].GiveValue = this.shopItemList[j].GiveValue;
                shopToSend.Items[j].GetItem = this.shopItemList[j].GetItem;
            }

            Messenger.SendSaveShop(this.shopNum, shopToSend);

            this.pnlShopEditor.Visible = false;
            this.pnlShopList.Visible = true;
            this.Size = new Size(this.pnlShopList.Width, this.pnlShopList.Height);
        }
    }
}
