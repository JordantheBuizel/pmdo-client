﻿// <copyright file="winDungeonPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Network;
    using PMU.Core;
    using PMU.Sockets;
    using SdlDotNet.Widgets;

    internal class WinDungeonPanel : Core.WindowCore
    {
        private ExpKit.Modules.KitChat kitchat;
        private int dungeonNum = 0;
        private int currentTen = 0;
        private Logic.Editors.Dungeons.EditableDungeon dungeon;

        private readonly Panel pnlDungeonList;
        private readonly Panel pnlDungeonEditor;
        private readonly ScrollingListBox lbxDungeonList;
        private readonly Button btnAddNew;
        private readonly Button btnBack;
        private readonly Button btnForward;
        private readonly Button btnCancel;
        private readonly Button btnEdit;
        private readonly Button btnGeneral;
        private readonly Button btnScripts;
        private readonly Button btnStandardMaps;
        private readonly Button btnRandomMaps;
        private readonly Panel pnlDungeonGeneral;
        private readonly Panel pnlDungeonScripts;
        private readonly Panel pnlDungeonStandardMaps;
        private readonly Panel pnlDungeonRandomMaps;
        private readonly Label lblName;
        private readonly TextBox txtName;
        private readonly CheckBox chkRescue;
        private readonly Button btnEditorCancel;
        private readonly Button btnEditorOK;
        private readonly ScrollingListBox lbxDungeonScripts;
        private readonly Label lblScriptNum;
        private readonly NumericUpDown nudScriptNum;
        private readonly Label lblScriptParam;
        private readonly TextBox txtScriptParam;
        private readonly Button btnAddScript;
        private readonly Button btnLoadScript;
        private readonly Button btnRemoveScript;
        private readonly ScrollingListBox lbxDungeonSMaps;
        private readonly Label lblMapNum;
        private readonly NumericUpDown nudMapNum;
        private readonly Label lblSMapDifficulty;
        private readonly NumericUpDown nudSMapDifficulty;
        private readonly CheckBox chkSMapBad;
        private readonly Button btnAddSMap;
        private readonly Button btnLoadSMap;
        private readonly Button btnRemoveSMap;
        private readonly ScrollingListBox lbxDungeonRMaps;
        private readonly Label lblRDungeonIndex;
        private readonly NumericUpDown nudRDungeonIndex;
        private readonly Label lblRDungeonFloor;
        private readonly NumericUpDown nudRDungeonFloor;
        private readonly Label lblRDungeonFloorEnd;
        private readonly NumericUpDown nudRDungeonFloorEnd;
        private readonly Label lblRMapDifficulty;
        private readonly NumericUpDown nudRMapDifficulty;
        private readonly CheckBox chkRMapBad;
        private readonly Button btnAddRMap;
        private readonly Button btnLoadRMap;
        private readonly Button btnRemoveRMap;

        public WinDungeonPanel()
            : base("winDungeonPanel")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new Size(200, 230);
            this.Location = new Point(210, Windows.WindowSwitcher.GameWindow.ActiveTeam.Y + Windows.WindowSwitcher.GameWindow.ActiveTeam.Height + 0);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = true;
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "Dungeon Panel";
            this.pnlDungeonList = new Panel("pnlDungeonList");
            this.pnlDungeonList.Size = new Size(200, 230);
            this.pnlDungeonList.Location = new Point(0, 0);
            this.pnlDungeonList.BackColor = Color.White;
            this.pnlDungeonList.Visible = true;

            this.pnlDungeonEditor = new Panel("pnlDungeonEditor");
            this.pnlDungeonEditor.Size = new Size(320, 300);
            this.pnlDungeonEditor.Location = new Point(0, 0);
            this.pnlDungeonEditor.BackColor = Color.White;
            this.pnlDungeonEditor.Visible = false;

            this.pnlDungeonGeneral = new Panel("pnlDungeonGeneral");
            this.pnlDungeonGeneral.Size = new Size(320, 270);
            this.pnlDungeonGeneral.Location = new Point(0, 30);
            this.pnlDungeonGeneral.BackColor = Color.White;
            this.pnlDungeonGeneral.Visible = true;

            this.pnlDungeonScripts = new Panel("pnlDungeonScripts");
            this.pnlDungeonScripts.Size = new Size(320, 270);
            this.pnlDungeonScripts.Location = new Point(0, 30);
            this.pnlDungeonScripts.BackColor = Color.White;
            this.pnlDungeonScripts.Visible = false;

            this.pnlDungeonStandardMaps = new Panel("pnlDungeonStandardMaps");
            this.pnlDungeonStandardMaps.Size = new Size(320, 270);
            this.pnlDungeonStandardMaps.Location = new Point(0, 30);
            this.pnlDungeonStandardMaps.BackColor = Color.White;
            this.pnlDungeonStandardMaps.Visible = false;

            this.pnlDungeonRandomMaps = new Panel("pnlDungeonRandomMaps");
            this.pnlDungeonRandomMaps.Size = new Size(320, 270);
            this.pnlDungeonRandomMaps.Location = new Point(0, 30);
            this.pnlDungeonRandomMaps.BackColor = Color.White;
            this.pnlDungeonRandomMaps.Visible = false;
            this.lbxDungeonList = new ScrollingListBox("lbxDungeonList");
            this.lbxDungeonList.Location = new Point(10, 10);
            this.lbxDungeonList.Size = new Size(180, 140);
            for (int i = 0; i < 10; i++)
            {
                ListBoxTextItem lbiDungeon = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), i + ": ");
                this.lbxDungeonList.Items.Add(lbiDungeon);
            }

            this.btnBack = new Button("btnBack");
            this.btnBack.Location = new Point(10, 160);
            this.btnBack.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnBack.Size = new Size(64, 16);
            this.btnBack.Visible = true;
            this.btnBack.Text = "<--";
            this.btnBack.Click += new EventHandler<MouseButtonEventArgs>(this.BtnBack_Click);

            this.btnForward = new Button("btnForward");
            this.btnForward.Location = new Point(126, 160);
            this.btnForward.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnForward.Size = new Size(64, 16);
            this.btnForward.Visible = true;
            this.btnForward.Text = "-->";
            this.btnForward.Click += new EventHandler<MouseButtonEventArgs>(this.BtnForward_Click);

            this.btnEdit = new Button("btnEdit");
            this.btnEdit.Location = new Point(10, 190);
            this.btnEdit.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEdit.Size = new Size(64, 16);
            this.btnEdit.Visible = true;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEdit_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(126, 190);
            this.btnCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnCancel.Size = new Size(64, 16);
            this.btnCancel.Visible = true;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.btnAddNew = new Button("btnAddNew");
            this.btnAddNew.Location = new Point(76, 190);
            this.btnAddNew.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnAddNew.Size = new Size(48, 16);
            this.btnAddNew.Visible = true;
            this.btnAddNew.Text = "New";
            this.btnAddNew.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddNew_Click);
            this.btnGeneral = new Button("btnGeneral");
            this.btnGeneral.Location = new Point(5, 5);
            this.btnGeneral.Size = new Size(70, 20);
            this.btnGeneral.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnGeneral.Text = "General";
            this.btnGeneral.Click += new EventHandler<MouseButtonEventArgs>(this.BtnGeneral_Click);

            this.btnStandardMaps = new Button("btnStandardMaps");
            this.btnStandardMaps.Location = new Point(80, 5);
            this.btnStandardMaps.Size = new Size(70, 20);
            this.btnStandardMaps.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnStandardMaps.Text = "Standard Maps";
            this.btnStandardMaps.Click += new EventHandler<MouseButtonEventArgs>(this.BtnStandardMaps_Click);

            this.btnRandomMaps = new Button("btnRandomMaps");
            this.btnRandomMaps.Location = new Point(155, 5);
            this.btnRandomMaps.Size = new Size(70, 20);
            this.btnRandomMaps.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnRandomMaps.Text = "Random Maps";
            this.btnRandomMaps.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRandomMaps_Click);

            this.btnScripts = new Button("btnScripts");
            this.btnScripts.Location = new Point(230, 5);
            this.btnScripts.Size = new Size(70, 20);
            this.btnScripts.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnScripts.Text = "Scripts";
            this.btnScripts.Click += new EventHandler<MouseButtonEventArgs>(this.BtnScripts_Click);
            this.lblName = new Label("lblName");
            this.lblName.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblName.Text = "Name:";
            this.lblName.AutoSize = true;
            this.lblName.Location = new Point(10, 4);

            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(200, 16);
            this.txtName.Location = new Point(10, 24);
            this.txtName.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.txtName.KeyDown += new EventHandler<SdlDotNet.Input.KeyboardEventArgs>(this.TxtName_KeyDown);

            this.chkRescue = new CheckBox("chkRescue");
            this.chkRescue.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.chkRescue.Size = new Size(140, 14);
            this.chkRescue.Location = new Point(10, 48);
            this.chkRescue.Text = "Allow Rescue";

            this.btnEditorCancel = new Button("btnEditorCancel");
            this.btnEditorCancel.Location = new Point(120, 75);
            this.btnEditorCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorCancel.Size = new Size(64, 16);
            this.btnEditorCancel.Visible = true;
            this.btnEditorCancel.Text = "Cancel";
            this.btnEditorCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorCancel_Click);

            this.btnEditorOK = new Button("btnEditorOK");
            this.btnEditorOK.Location = new Point(20, 75);
            this.btnEditorOK.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorOK.Size = new Size(64, 16);
            this.btnEditorOK.Visible = true;
            this.btnEditorOK.Text = "OK";
            this.btnEditorOK.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorOK_Click);
            this.lblScriptNum = new Label("lblScriptNum");
            this.lblScriptNum.AutoSize = true;
            this.lblScriptNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblScriptNum.Location = new Point(10, 0);
            this.lblScriptNum.Text = "Num:";

            this.nudScriptNum = new NumericUpDown("nudScriptNum");
            this.nudScriptNum.Size = new Size(70, 20);
            this.nudScriptNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudScriptNum.Maximum = int.MaxValue;
            this.nudScriptNum.Minimum = int.MinValue;
            this.nudScriptNum.Location = new Point(10, 14);

            this.lblScriptParam = new Label("lblScriptParam");
            this.lblScriptParam.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblScriptParam.Text = "Parameter String:";
            this.lblScriptParam.AutoSize = true;
            this.lblScriptParam.Location = new Point(10, 34);

            this.txtScriptParam = new TextBox("txtScriptParam");
            this.txtScriptParam.Size = new Size(200, 16);
            this.txtScriptParam.Location = new Point(10, 46);
            this.txtScriptParam.Font = Graphic.FontManager.LoadFont("tahoma", 12);

            this.btnAddScript = new Button("btnAddScript");
            this.btnAddScript.Size = new Size(70, 16);
            this.btnAddScript.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnAddScript.Location = new Point(5, 72);
            this.btnAddScript.Text = "Add";
            this.btnAddScript.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddScript_Click);

            this.btnRemoveScript = new Button("btnRemoveScript");
            this.btnRemoveScript.Size = new Size(70, 16);
            this.btnRemoveScript.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnRemoveScript.Location = new Point(80, 72);
            this.btnRemoveScript.Text = "Remove";
            this.btnRemoveScript.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveScript_Click);

            this.btnLoadScript = new Button("btnLoadScript");
            this.btnLoadScript.Size = new Size(70, 16);
            this.btnLoadScript.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnLoadScript.Location = new Point(155, 72);
            this.btnLoadScript.Text = "Load";
            this.btnLoadScript.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadScript_Click);

            this.lbxDungeonScripts = new ScrollingListBox("lbxDungeonScripts");
            this.lbxDungeonScripts.Location = new Point(10, 90);
            this.lbxDungeonScripts.Size = new Size(this.pnlDungeonStandardMaps.Size.Width - 20, this.pnlDungeonStandardMaps.Size.Height - 120);
            this.lbxDungeonScripts.MultiSelect = false;
            this.lblMapNum = new Label("lblMapNum");
            this.lblMapNum.AutoSize = true;
            this.lblMapNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblMapNum.Location = new Point(10, 0);
            this.lblMapNum.Text = "Num:";

            this.nudMapNum = new NumericUpDown("nudMapNum");
            this.nudMapNum.Size = new Size(70, 20);
            this.nudMapNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudMapNum.Maximum = 2000;
            this.nudMapNum.Minimum = 1;
            this.nudMapNum.Location = new Point(10, 14);

            this.lblSMapDifficulty = new Label("lblSMapDifficulty");
            this.lblSMapDifficulty.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblSMapDifficulty.Text = "Difficulty:";
            this.lblSMapDifficulty.AutoSize = true;
            this.lblSMapDifficulty.Location = new Point(10, 34);

            this.nudSMapDifficulty = new NumericUpDown("nudSMapDifficulty");
            this.nudSMapDifficulty.Size = new Size(50, 20);
            this.nudSMapDifficulty.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudSMapDifficulty.Location = new Point(10, 48);
            this.nudSMapDifficulty.Minimum = 1;
            this.nudSMapDifficulty.Maximum = 16;

            this.chkSMapBad = new CheckBox("chkSMap");
            this.chkSMapBad.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.chkSMapBad.Size = new Size(120, 14);
            this.chkSMapBad.Location = new Point(100, 40);
            this.chkSMapBad.Text = "Boss Goal Map";

            this.btnAddSMap = new Button("btnAddSMap");
            this.btnAddSMap.Size = new Size(70, 16);
            this.btnAddSMap.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnAddSMap.Location = new Point(5, 72);
            this.btnAddSMap.Text = "Add";
            this.btnAddSMap.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddSMap_Click);

            this.btnRemoveSMap = new Button("btnRemoveSMap");
            this.btnRemoveSMap.Size = new Size(70, 16);
            this.btnRemoveSMap.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnRemoveSMap.Location = new Point(80, 72);
            this.btnRemoveSMap.Text = "Remove";
            this.btnRemoveSMap.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveSMap_Click);

            this.btnLoadSMap = new Button("btnLoadSMap");
            this.btnLoadSMap.Size = new Size(70, 16);
            this.btnLoadSMap.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnLoadSMap.Location = new Point(155, 72);
            this.btnLoadSMap.Text = "Load";
            this.btnLoadSMap.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadSMap_Click);

            this.lbxDungeonSMaps = new ScrollingListBox("lbxDungeonSMaps");
            this.lbxDungeonSMaps.Location = new Point(10, 90);
            this.lbxDungeonSMaps.Size = new Size(this.pnlDungeonStandardMaps.Size.Width - 20, this.pnlDungeonStandardMaps.Size.Height - 120);
            this.lbxDungeonSMaps.MultiSelect = false;
            this.lblRDungeonIndex = new Label("lblRDungeonIndex");
            this.lblRDungeonIndex.AutoSize = true;
            this.lblRDungeonIndex.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblRDungeonIndex.Location = new Point(2, 0);
            this.lblRDungeonIndex.Text = "Dungeon #:";

            this.nudRDungeonIndex = new NumericUpDown("nudRDungeonIndex");
            this.nudRDungeonIndex.Size = new Size(70, 20);
            this.nudRDungeonIndex.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudRDungeonIndex.Maximum = MaxInfo.MaxRDungeons;
            this.nudRDungeonIndex.Minimum = 1;
            this.nudRDungeonIndex.Location = new Point(10, 14);
            this.nudRDungeonIndex.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudRDungeonIndex_ValueChanged);

            this.lblRDungeonFloor = new Label("lblRDungeonFloor");
            this.lblRDungeonFloor.AutoSize = true;
            this.lblRDungeonFloor.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblRDungeonFloor.Location = new Point(240, 0);
            this.lblRDungeonFloor.Text = "From Floor:";

            this.nudRDungeonFloor = new NumericUpDown("nudRDungeonFloor");
            this.nudRDungeonFloor.Size = new Size(70, 20);
            this.nudRDungeonFloor.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudRDungeonFloor.Maximum = int.MaxValue;
            this.nudRDungeonFloor.Minimum = 1;
            this.nudRDungeonFloor.Location = new Point(240, 14);
            this.nudRDungeonFloor.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudRDungeonFloor_ValueChanged);

            this.lblRDungeonFloorEnd = new Label("lblRDungeonFloorEnd");
            this.lblRDungeonFloorEnd.AutoSize = true;
            this.lblRDungeonFloorEnd.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblRDungeonFloorEnd.Location = new Point(240, 34);
            this.lblRDungeonFloorEnd.Text = "To Floor:";

            this.nudRDungeonFloorEnd = new NumericUpDown("nudRDungeonFloorEnd");
            this.nudRDungeonFloorEnd.Size = new Size(70, 20);
            this.nudRDungeonFloorEnd.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudRDungeonFloorEnd.Maximum = int.MaxValue;
            this.nudRDungeonFloorEnd.Minimum = 1;
            this.nudRDungeonFloorEnd.Location = new Point(240, 48);
            this.nudRDungeonFloorEnd.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudRDungeonFloorEnd_ValueChanged);

            this.lblRMapDifficulty = new Label("lblRMapDifficulty");
            this.lblRMapDifficulty.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblRMapDifficulty.Text = "Difficulty:";
            this.lblRMapDifficulty.AutoSize = true;
            this.lblRMapDifficulty.Location = new Point(10, 34);

            this.nudRMapDifficulty = new NumericUpDown("nudRMapDifficulty");
            this.nudRMapDifficulty.Size = new Size(50, 20);
            this.nudRMapDifficulty.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.nudRMapDifficulty.Location = new Point(10, 48);
            this.nudRMapDifficulty.Minimum = 1;
            this.nudRMapDifficulty.Maximum = 16;

            this.chkRMapBad = new CheckBox("chkRMap");
            this.chkRMapBad.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.chkRMapBad.Size = new Size(120, 14);
            this.chkRMapBad.Location = new Point(100, 50);
            this.chkRMapBad.Text = "Boss Goal Map";

            this.btnAddRMap = new Button("btnAddRMap");
            this.btnAddRMap.Size = new Size(70, 16);
            this.btnAddRMap.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnAddRMap.Location = new Point(5, 72);
            this.btnAddRMap.Text = "Add";
            this.btnAddRMap.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddRMap_Click);

            this.btnRemoveRMap = new Button("btnRemoveRMap");
            this.btnRemoveRMap.Size = new Size(70, 16);
            this.btnRemoveRMap.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnRemoveRMap.Location = new Point(80, 72);
            this.btnRemoveRMap.Text = "Remove";
            this.btnRemoveRMap.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveRMap_Click);

            this.btnLoadRMap = new Button("btnLoadRMap");
            this.btnLoadRMap.Size = new Size(70, 16);
            this.btnLoadRMap.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnLoadRMap.Location = new Point(155, 72);
            this.btnLoadRMap.Text = "Load";
            this.btnLoadRMap.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadRMap_Click);

            this.lbxDungeonRMaps = new ScrollingListBox("lbxDungeonRMaps");
            this.lbxDungeonRMaps.Location = new Point(10, 90);
            this.lbxDungeonRMaps.Size = new Size(this.pnlDungeonRandomMaps.Size.Width - 20, this.pnlDungeonRandomMaps.Size.Height - 120);
            this.lbxDungeonRMaps.MultiSelect = false;



            // Dungeon List
            this.pnlDungeonList.AddWidget(this.lbxDungeonList);
            this.pnlDungeonList.AddWidget(this.btnBack);
            this.pnlDungeonList.AddWidget(this.btnForward);
            this.pnlDungeonList.AddWidget(this.btnAddNew);
            this.pnlDungeonList.AddWidget(this.btnEdit);
            this.pnlDungeonList.AddWidget(this.btnCancel);

            // General
            this.pnlDungeonGeneral.AddWidget(this.lblName);
            this.pnlDungeonGeneral.AddWidget(this.txtName);
            this.pnlDungeonGeneral.AddWidget(this.chkRescue);
            this.pnlDungeonGeneral.AddWidget(this.btnEditorCancel);
            this.pnlDungeonGeneral.AddWidget(this.btnEditorOK);

            // Scripts
            this.pnlDungeonScripts.AddWidget(this.lblScriptNum);
            this.pnlDungeonScripts.AddWidget(this.nudScriptNum);
            this.pnlDungeonScripts.AddWidget(this.lblScriptParam);
            this.pnlDungeonScripts.AddWidget(this.txtScriptParam);
            this.pnlDungeonScripts.AddWidget(this.btnAddScript);
            this.pnlDungeonScripts.AddWidget(this.btnRemoveScript);
            this.pnlDungeonScripts.AddWidget(this.btnLoadScript);
            this.pnlDungeonScripts.AddWidget(this.lbxDungeonScripts);

            // Standard Maps
            this.pnlDungeonStandardMaps.AddWidget(this.lblMapNum);
            this.pnlDungeonStandardMaps.AddWidget(this.nudMapNum);
            this.pnlDungeonStandardMaps.AddWidget(this.lblSMapDifficulty);
            this.pnlDungeonStandardMaps.AddWidget(this.nudSMapDifficulty);
            this.pnlDungeonStandardMaps.AddWidget(this.chkSMapBad);
            this.pnlDungeonStandardMaps.AddWidget(this.btnAddSMap);
            this.pnlDungeonStandardMaps.AddWidget(this.btnRemoveSMap);
            this.pnlDungeonStandardMaps.AddWidget(this.btnLoadSMap);
            this.pnlDungeonStandardMaps.AddWidget(this.lbxDungeonSMaps);

            // RDungeon Maps
            this.pnlDungeonRandomMaps.AddWidget(this.lblRDungeonIndex);
            this.pnlDungeonRandomMaps.AddWidget(this.nudRDungeonIndex);
            this.pnlDungeonRandomMaps.AddWidget(this.lblRDungeonFloor);
            this.pnlDungeonRandomMaps.AddWidget(this.nudRDungeonFloor);
            this.pnlDungeonRandomMaps.AddWidget(this.lblRDungeonFloorEnd);
            this.pnlDungeonRandomMaps.AddWidget(this.nudRDungeonFloorEnd);
            this.pnlDungeonRandomMaps.AddWidget(this.lblRMapDifficulty);
            this.pnlDungeonRandomMaps.AddWidget(this.nudRMapDifficulty);
            this.pnlDungeonRandomMaps.AddWidget(this.chkRMapBad);
            this.pnlDungeonRandomMaps.AddWidget(this.btnAddRMap);
            this.pnlDungeonRandomMaps.AddWidget(this.btnRemoveRMap);
            this.pnlDungeonRandomMaps.AddWidget(this.btnLoadRMap);
            this.pnlDungeonRandomMaps.AddWidget(this.lbxDungeonRMaps);

            // Editor panel
            this.pnlDungeonEditor.AddWidget(this.btnGeneral);
            this.pnlDungeonEditor.AddWidget(this.btnStandardMaps);
            this.pnlDungeonEditor.AddWidget(this.btnRandomMaps);
            this.pnlDungeonEditor.AddWidget(this.btnScripts);
            this.pnlDungeonEditor.AddWidget(this.pnlDungeonGeneral);
            this.pnlDungeonEditor.AddWidget(this.pnlDungeonStandardMaps);
            this.pnlDungeonEditor.AddWidget(this.pnlDungeonRandomMaps);
            this.pnlDungeonEditor.AddWidget(this.pnlDungeonScripts);

            // This
            this.AddWidget(this.pnlDungeonList);
            this.AddWidget(this.pnlDungeonEditor);
            this.RefreshDungeonList();
        }

        private void TxtName_KeyDown(object sender, SdlDotNet.Input.KeyboardEventArgs e)
        {
            if (e.Key == SdlDotNet.Input.Key.End)
            {
                try
                {
                    System.Windows.Forms.Clipboard.SetText(this.txtName.Text);
                    this.txtName.Text = string.Empty;
                }
                catch
                {
                }
            }

            if (e.Key == SdlDotNet.Input.Key.PageUp)
            {
                try
                {
                    System.Windows.Forms.Clipboard.SetText(this.txtName.Text);
                }
                catch
                {
                }
            }

            if (e.Key == SdlDotNet.Input.Key.PageDown)
            {
                try
                {
                    this.txtName.Text = this.txtName.Text + System.Windows.Forms.Clipboard.GetText();
                }
                catch
                {
                    this.kitchat.AppendChat("The paste text must be only standard text (abc123,./!$% etc.)", Color.Red);
                }
            }
        }

        private void BtnBack_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen > 0)
            {
                this.currentTen--;
            }

            this.RefreshDungeonList();
        }

        private void BtnForward_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen < (MaxInfo.MaxDungeons / 10))
            {
                this.currentTen++;
            }

            this.RefreshDungeonList();
        }

        private void BtnAddNew_Click(object sender, MouseButtonEventArgs e)
        {
            Messenger.SendAddDungeon();
        }

        private void BtnEdit_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxDungeonList.SelectedItems.Count == 1)
            {
                string[] index = ((ListBoxTextItem)this.lbxDungeonList.SelectedItems[0]).Text.Split(':');
                if (index[0].IsNumeric())
                {
                    this.dungeonNum = index[0].ToInt() - 1;
                    this.btnEdit.Text = "Loading...";

                    Messenger.SendEditDungeon(this.dungeonNum);
                }
            }
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            return;
        }

        public void RefreshDungeonList()
        {
            for (int i = 0; i < 10; i++)
            {
                if ((i + (this.currentTen * 10)) < MaxInfo.MaxDungeons)
                {
                    ((ListBoxTextItem)this.lbxDungeonList.Items[i]).Text = ((i + 1) + (10 * this.currentTen)) + ": " + Dungeons.DungeonHelper.Dungeons[i + (10 * this.currentTen)].Name;
                }
else
                {
                    ((ListBoxTextItem)this.lbxDungeonList.Items[i]).Text = "---";
                }
            }
        }

        public void LoadDungeon(string[] parse)
        {
            this.Size = this.pnlDungeonEditor.Size;
            this.pnlDungeonList.Visible = false;
            this.pnlDungeonEditor.Visible = true;
            this.BtnGeneral_Click(null, null);
            this.lbxDungeonSMaps.Items.Clear();
            this.lbxDungeonRMaps.Items.Clear();
            this.lbxDungeonScripts.Items.Clear();

            // this.Size = new System.Drawing.Size(pnlDungeonGeneral.Width, pnlDungeonGeneral.Height);
            this.dungeon = new Logic.Editors.Dungeons.EditableDungeon();

            this.dungeon.Name = parse[2];
            this.txtName.Text = this.dungeon.Name;
            this.dungeon.AllowsRescue = parse[3].ToBool();
            this.chkRescue.Checked = this.dungeon.AllowsRescue;
            int scriptCount = parse[4].ToInt();
            int n = 5;
            for (int i = 0; i < scriptCount; i++)
            {
                this.dungeon.ScriptList.Add(parse[n].ToInt(), parse[n + 1]);

                n += 2;

                ListBoxTextItem lbiScript = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), this.dungeon.ScriptList.KeyByIndex(i) + ": " + this.dungeon.ScriptList.ValueByIndex(i));
                this.lbxDungeonScripts.Items.Add(lbiScript);
            }

            int mapCount = parse[n].ToInt();
            n++;

            for (int i = 0; i < mapCount; i++)
            {
                Logic.Editors.Dungeons.EditableStandardDungeonMap map = new Logic.Editors.Dungeons.EditableStandardDungeonMap();
                map.Difficulty = (Enums.JobDifficulty)parse[n].ToInt();
                map.IsBadGoalMap = parse[n + 1].ToBool();
                map.MapNum = parse[n + 2].ToInt();

                this.dungeon.StandardMaps.Add(map);

                string mapText;
                if (map.IsBadGoalMap)
                {
                    mapText = (i + 1) + ": (Boss)[" + Missions.MissionManager.DifficultyToString(map.Difficulty) + "] Map #" + map.MapNum;
                }
else
                {
                    mapText = (i + 1) + ": [" + Missions.MissionManager.DifficultyToString(map.Difficulty) + "] Map #" + map.MapNum;
                }

                ListBoxTextItem lbiMap = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), mapText);
                this.lbxDungeonSMaps.Items.Add(lbiMap);

                n += 3;
            }

            mapCount = parse[n].ToInt();
            n++;
            for (int i = 0; i < mapCount; i++)
            {
                Logic.Editors.Dungeons.EditableRandomDungeonMap map = new Logic.Editors.Dungeons.EditableRandomDungeonMap();
                map.Difficulty = (Enums.JobDifficulty)parse[n].ToInt();
                map.IsBadGoalMap = parse[n + 1].ToBool();
                map.RDungeonIndex = parse[n + 2].ToInt();
                map.RDungeonFloor = parse[n + 3].ToInt();
                this.dungeon.RandomMaps.Add(map);

                n += 4;
                string mapText;
                if (map.IsBadGoalMap)
                {
                    mapText = (i + 1) + ": (Boss)[" + Missions.MissionManager.DifficultyToString(map.Difficulty) + "] Dun #" + (map.RDungeonIndex + 1) + " (" + RDungeons.RDungeonHelper.RDungeons[map.RDungeonIndex].Name + ") " + (map.RDungeonFloor + 1) + "F";
                }
else
                {
                    mapText = (i + 1) + ": [" + Missions.MissionManager.DifficultyToString(map.Difficulty) + "] Dun #" + (map.RDungeonIndex + 1) + " (" + RDungeons.RDungeonHelper.RDungeons[map.RDungeonIndex].Name + ") " + (map.RDungeonFloor + 1) + "F";
                }

                ListBoxTextItem lbiMap = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), mapText);
                this.lbxDungeonRMaps.Items.Add(lbiMap);
            }

            this.btnEdit.Text = "Edit";
        }

        private void BtnGeneral_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnGeneral.Selected)
            {
                this.btnGeneral.Selected = true;
                this.btnStandardMaps.Selected = false;
                this.btnRandomMaps.Selected = false;
                this.btnScripts.Selected = false;
                this.pnlDungeonGeneral.Visible = true;
                this.pnlDungeonStandardMaps.Visible = false;
                this.pnlDungeonRandomMaps.Visible = false;
                this.pnlDungeonScripts.Visible = false;
                this.TitleBar.Text = "Dungeon Editor - General";
            }
        }

        private void BtnStandardMaps_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnStandardMaps.Selected)
            {
                this.btnGeneral.Selected = false;
                this.btnStandardMaps.Selected = true;
                this.btnRandomMaps.Selected = false;
                this.btnScripts.Selected = false;
                this.pnlDungeonGeneral.Visible = false;
                this.pnlDungeonStandardMaps.Visible = true;
                this.pnlDungeonRandomMaps.Visible = false;
                this.pnlDungeonScripts.Visible = false;
                this.TitleBar.Text = "Dungeon Editor - Standard Maps";
            }
        }

        private void BtnRandomMaps_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnRandomMaps.Selected)
            {
                this.btnGeneral.Selected = false;
                this.btnStandardMaps.Selected = false;
                this.btnRandomMaps.Selected = true;
                this.btnScripts.Selected = false;
                this.pnlDungeonGeneral.Visible = false;
                this.pnlDungeonStandardMaps.Visible = false;
                this.pnlDungeonRandomMaps.Visible = true;
                this.pnlDungeonScripts.Visible = false;
                this.TitleBar.Text = "Dungeon Editor - Random Maps";
            }
        }

        private void BtnScripts_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnScripts.Selected)
            {
                this.btnGeneral.Selected = false;
                this.btnStandardMaps.Selected = false;
                this.btnRandomMaps.Selected = false;
                this.btnScripts.Selected = true;
                this.pnlDungeonGeneral.Visible = false;
                this.pnlDungeonStandardMaps.Visible = false;
                this.pnlDungeonRandomMaps.Visible = false;
                this.pnlDungeonScripts.Visible = true;
                this.TitleBar.Text = "Dungeon Editor - Scripts";
            }
        }

        private void BtnEditorCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.dungeonNum = -1;
            this.pnlDungeonEditor.Visible = false;
            this.pnlDungeonList.Visible = true;
            this.Size = new Size(this.pnlDungeonList.Width, this.pnlDungeonList.Height);
            this.TitleBar.Text = "Dungeon Panel";
        }

        private void BtnEditorOK_Click(object sender, MouseButtonEventArgs e)
        {
            this.dungeon.Name = this.txtName.Text;
            this.dungeon.AllowsRescue = this.chkRescue.Checked;
            Messenger.SendSaveDungeon(this.dungeonNum, this.dungeon);

            this.dungeonNum = -1;
            this.pnlDungeonEditor.Visible = false;
            this.pnlDungeonList.Visible = true;
            this.Size = new Size(this.pnlDungeonList.Width, this.pnlDungeonList.Height);
        }

        private void BtnRemoveScript_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxDungeonScripts.SelectedIndex > -1)
            {
                this.dungeon.ScriptList.RemoveAt(this.lbxDungeonScripts.SelectedIndex);
                this.lbxDungeonScripts.Items.Clear();
                for (int scripts = 0; scripts < this.dungeon.ScriptList.Count; scripts++)
                {
                    ListBoxTextItem lbiScript = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), this.dungeon.ScriptList.KeyByIndex(scripts) + ": " + this.dungeon.ScriptList.ValueByIndex(scripts));
                    this.lbxDungeonScripts.Items.Add(lbiScript);
                }
            }
        }

        private void BtnAddScript_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.dungeon.ScriptList.ContainsKey(this.nudScriptNum.Value))
            {
                this.dungeon.ScriptList.RemoveAtKey(this.nudScriptNum.Value);
            }

            this.dungeon.ScriptList.Add(this.nudScriptNum.Value, this.txtScriptParam.Text);
            this.lbxDungeonScripts.Items.Clear();
            for (int scripts = 0; scripts < this.dungeon.ScriptList.Count; scripts++)
            {
                ListBoxTextItem lbiScript = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), this.dungeon.ScriptList.KeyByIndex(scripts) + ": " + this.dungeon.ScriptList.ValueByIndex(scripts));
                this.lbxDungeonScripts.Items.Add(lbiScript);
            }
        }

        private void BtnLoadScript_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxDungeonScripts.SelectedIndex > -1)
            {
                this.nudScriptNum.Value = this.dungeon.ScriptList.KeyByIndex(this.lbxDungeonScripts.SelectedIndex);
                this.txtScriptParam.Text = this.dungeon.ScriptList.ValueByIndex(this.lbxDungeonScripts.SelectedIndex);
            }
        }

        private void BtnRemoveSMap_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxDungeonSMaps.SelectedIndex > -1)
            {
                this.dungeon.StandardMaps.RemoveAt(this.lbxDungeonSMaps.SelectedIndex);
                this.lbxDungeonSMaps.Items.Clear();
                for (int maps = 0; maps < this.dungeon.StandardMaps.Count; maps++)
                {
                    string mapText;
                    if (this.dungeon.StandardMaps[maps].IsBadGoalMap)
                    {
                        mapText = (maps + 1) + ": (Boss)[" + Missions.MissionManager.DifficultyToString(this.dungeon.StandardMaps[maps].Difficulty) + "] Map #" + this.dungeon.StandardMaps[maps].MapNum;
                    }
else
                    {
                        mapText = (maps + 1) + ": [" + Missions.MissionManager.DifficultyToString(this.dungeon.StandardMaps[maps].Difficulty) + "] Map #" + this.dungeon.StandardMaps[maps].MapNum;
                    }

                    ListBoxTextItem lbiMap = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), mapText);
                    this.lbxDungeonSMaps.Items.Add(lbiMap);
                }
            }
        }

        private void BtnAddSMap_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.MapIndex(this.nudMapNum.Value) > -1)
            {
                this.dungeon.StandardMaps.RemoveAt(this.MapIndex(this.nudMapNum.Value));
                this.lbxDungeonSMaps.Items.Clear();
                for (int maps = 0; maps < this.dungeon.StandardMaps.Count; maps++)
                {
                    string mapText2;
                    if (this.dungeon.StandardMaps[maps].IsBadGoalMap)
                    {
                        mapText2 = (maps + 1) + ": (Boss)[" + Missions.MissionManager.DifficultyToString(this.dungeon.StandardMaps[maps].Difficulty) + "] Map #" + this.dungeon.StandardMaps[maps].MapNum;
                    }
                    else
                    {
                        mapText2 = (maps + 1) + ": [" + Missions.MissionManager.DifficultyToString(this.dungeon.StandardMaps[maps].Difficulty) + "] Map #" + this.dungeon.StandardMaps[maps].MapNum;
                    }

                    ListBoxTextItem lbiMap2 = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), mapText2);
                    this.lbxDungeonSMaps.Items.Add(lbiMap2);
                }
            }

            Logic.Editors.Dungeons.EditableStandardDungeonMap map = new Logic.Editors.Dungeons.EditableStandardDungeonMap();
            map.MapNum = this.nudMapNum.Value;
            map.Difficulty = (Enums.JobDifficulty)this.nudSMapDifficulty.Value;
            map.IsBadGoalMap = this.chkSMapBad.Checked;

            this.dungeon.StandardMaps.Add(map);

            string mapText;
            if (map.IsBadGoalMap)
            {
                mapText = (this.lbxDungeonSMaps.Items.Count + 1) + ": (Boss)[" + Missions.MissionManager.DifficultyToString(map.Difficulty) + "] Map #" + map.MapNum;
            }
else
            {
                mapText = (this.lbxDungeonSMaps.Items.Count + 1) + ": [" + Missions.MissionManager.DifficultyToString(map.Difficulty) + "] Map #" + map.MapNum;
            }

            ListBoxTextItem lbiMap = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), mapText);
            this.lbxDungeonSMaps.Items.Add(lbiMap);
        }

        private void BtnLoadSMap_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxDungeonSMaps.SelectedIndex > -1)
            {
                this.nudMapNum.Value = this.dungeon.StandardMaps[this.lbxDungeonSMaps.SelectedIndex].MapNum;
                this.nudSMapDifficulty.Value = (int)this.dungeon.StandardMaps[this.lbxDungeonSMaps.SelectedIndex].Difficulty;
                this.chkSMapBad.Checked = this.dungeon.StandardMaps[this.lbxDungeonSMaps.SelectedIndex].IsBadGoalMap;
            }
        }

        private int MapIndex(int mapNum)
        {
            for (int i = 0; i < this.dungeon.StandardMaps.Count; i++)
            {
                if (this.dungeon.StandardMaps[i].MapNum == mapNum)
                {
                    return i;
                }
            }

            return -1;
        }

        private void BtnRemoveRMap_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxDungeonRMaps.SelectedIndex > -1)
            {
                this.dungeon.RandomMaps.RemoveAt(this.lbxDungeonRMaps.SelectedIndex);
                this.lbxDungeonRMaps.Items.Clear();
                for (int maps = 0; maps < this.dungeon.RandomMaps.Count; maps++)
                {
                    string mapText;
                    if (this.dungeon.RandomMaps[maps].IsBadGoalMap)
                    {
                        mapText = (maps + 1) + ": (Boss)[" + Missions.MissionManager.DifficultyToString(this.dungeon.RandomMaps[maps].Difficulty) + "] Dun #" + (this.dungeon.RandomMaps[maps].RDungeonIndex + 1) + " (" + RDungeons.RDungeonHelper.RDungeons[this.dungeon.RandomMaps[maps].RDungeonIndex].Name + ") " + (this.dungeon.RandomMaps[maps].RDungeonFloor + 1) + "F";
                    }
                    else
                    {
                        mapText = (maps + 1) + ": [" + Missions.MissionManager.DifficultyToString(this.dungeon.RandomMaps[maps].Difficulty) + "] Dun #" + (this.dungeon.RandomMaps[maps].RDungeonIndex + 1) + " (" + RDungeons.RDungeonHelper.RDungeons[this.dungeon.RandomMaps[maps].RDungeonIndex].Name + ") " + (this.dungeon.RandomMaps[maps].RDungeonFloor + 1) + "F";
                    }

                    ListBoxTextItem lbiMap = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), mapText);
                    this.lbxDungeonRMaps.Items.Add(lbiMap);
                }
            }
        }

        private void BtnAddRMap_Click(object sender, MouseButtonEventArgs e)
        {
            for (int i = this.nudRDungeonFloor.Value; i <= this.nudRDungeonFloorEnd.Value; i++)
            {
                int index = this.DungeonFloorIndex(this.nudRDungeonIndex.Value - 1, i - 1);
                if (index > -1)
                {
                    this.dungeon.RandomMaps.RemoveAt(index);
                }
            }

            this.lbxDungeonRMaps.Items.Clear();
            for (int maps = 0; maps < this.dungeon.RandomMaps.Count; maps++)
            {
                string mapText;
                if (this.dungeon.RandomMaps[maps].IsBadGoalMap)
                {
                    mapText = (maps + 1) + ": (Boss)[" + Missions.MissionManager.DifficultyToString(this.dungeon.RandomMaps[maps].Difficulty) + "] Dun #" + (this.dungeon.RandomMaps[maps].RDungeonIndex + 1) + " (" + RDungeons.RDungeonHelper.RDungeons[this.dungeon.RandomMaps[maps].RDungeonIndex].Name + ") " + (this.dungeon.RandomMaps[maps].RDungeonFloor + 1) + "F";
                }
                else
                {
                    mapText = (maps + 1) + ": [" + Missions.MissionManager.DifficultyToString(this.dungeon.RandomMaps[maps].Difficulty) + "] Dun #" + (this.dungeon.RandomMaps[maps].RDungeonIndex + 1) + " (" + RDungeons.RDungeonHelper.RDungeons[this.dungeon.RandomMaps[maps].RDungeonIndex].Name + ") " + (this.dungeon.RandomMaps[maps].RDungeonFloor + 1) + "F";
                }

                ListBoxTextItem lbiMap = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), mapText);
                this.lbxDungeonRMaps.Items.Add(lbiMap);
            }

            for (int i = this.nudRDungeonFloor.Value; i <= this.nudRDungeonFloorEnd.Value; i++)
            {
                Logic.Editors.Dungeons.EditableRandomDungeonMap map = new Logic.Editors.Dungeons.EditableRandomDungeonMap();
                map.RDungeonIndex = this.nudRDungeonIndex.Value - 1;
                map.RDungeonFloor = i - 1;
                map.Difficulty = (Enums.JobDifficulty)this.nudRMapDifficulty.Value;
                map.IsBadGoalMap = this.chkRMapBad.Checked;

                this.dungeon.RandomMaps.Add(map);

                string mapText;
                if (map.IsBadGoalMap)
                {
                    mapText = (this.lbxDungeonRMaps.Items.Count + 1) + ": (Boss)[" + Missions.MissionManager.DifficultyToString(map.Difficulty) + "] Dun #" + (map.RDungeonIndex + 1) + " (" + RDungeons.RDungeonHelper.RDungeons[map.RDungeonIndex].Name + ") " + (map.RDungeonFloor + 1) + "F";
                }
                else
                {
                    mapText = (this.lbxDungeonRMaps.Items.Count + 1) + ": [" + Missions.MissionManager.DifficultyToString(map.Difficulty) + "] Dun #" + (map.RDungeonIndex + 1) + " (" + RDungeons.RDungeonHelper.RDungeons[map.RDungeonIndex].Name + ") " + (map.RDungeonFloor + 1) + "F";
                }

                ListBoxTextItem lbiMap = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), mapText);
                this.lbxDungeonRMaps.Items.Add(lbiMap);
            }
        }

        private void BtnLoadRMap_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxDungeonRMaps.SelectedIndex > -1)
            {
                this.nudRDungeonIndex.Value = this.dungeon.RandomMaps[this.lbxDungeonRMaps.SelectedIndex].RDungeonIndex + 1;
                this.nudRDungeonFloor.Value = this.dungeon.RandomMaps[this.lbxDungeonRMaps.SelectedIndex].RDungeonFloor + 1;
                this.nudRMapDifficulty.Value = (int)this.dungeon.RandomMaps[this.lbxDungeonRMaps.SelectedIndex].Difficulty;
                this.chkRMapBad.Checked = this.dungeon.RandomMaps[this.lbxDungeonRMaps.SelectedIndex].IsBadGoalMap;
            }
        }

        private void NudRDungeonIndex_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.lblRDungeonIndex.Text = RDungeons.RDungeonHelper.RDungeons[this.nudRDungeonIndex.Value - 1].Name;
        }

        private void NudRDungeonFloor_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.nudRDungeonFloorEnd.Value < this.nudRDungeonFloor.Value)
            {
                this.nudRDungeonFloorEnd.Value = this.nudRDungeonFloor.Value;
            }
        }

        private void NudRDungeonFloorEnd_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.nudRDungeonFloorEnd.Value < this.nudRDungeonFloor.Value)
            {
                this.nudRDungeonFloor.Value = this.nudRDungeonFloorEnd.Value;
            }
        }

        private int DungeonFloorIndex(int dungeonIndex, int dungeonFloor)
        {
            for (int i = 0; i < this.dungeon.RandomMaps.Count; i++)
            {
                if (this.dungeon.RandomMaps[i].RDungeonIndex == dungeonIndex && this.dungeon.RandomMaps[i].RDungeonFloor == dungeonFloor)
                {
                    return i;
                }
            }

            return -1;
        }
    }
}
