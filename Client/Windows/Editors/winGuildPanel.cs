﻿// <copyright file="winGuildPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Network;
    using SdlDotNet.Widgets;

    internal class WinGuildPanel : Core.WindowCore
    {
        private readonly Label lblPlayer;
        private readonly Label lblGuild;
        private readonly Label lblCreate;

        private readonly TextBox txtPlayer;
        private readonly TextBox txtGuild;

        public WinGuildPanel()
            : base("winGuildPanel")
        {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new Size(174, 196);
            this.Location = new Point(210, Windows.WindowSwitcher.GameWindow.ActiveTeam.Y + Windows.WindowSwitcher.GameWindow.ActiveTeam.Height + 0);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = true;
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "Guild Panel";

            this.lblPlayer = new Label("lblPlayer");
            this.lblPlayer.Location = new Point(20, 0);
            this.lblPlayer.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblPlayer.AutoSize = true;

            this.lblPlayer.Text = "Player Name:";

            this.txtPlayer = new TextBox("txtPlayer");
            this.txtPlayer.Location = new Point(20, 20);
            this.txtPlayer.Size = new Size(120, 20);
            this.txtPlayer.Font = Graphic.FontManager.LoadFont("PMU", 16);

            this.lblGuild = new Label("lblGuild");
            this.lblGuild.Location = new Point(20, 60);
            this.lblGuild.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblGuild.AutoSize = true;
            this.lblGuild.Text = "Guild Name:";

            this.txtGuild = new TextBox("txtGuild");
            this.txtGuild.Location = new Point(20, 80);
            this.txtGuild.Size = new Size(120, 20);
            this.txtGuild.Font = Graphic.FontManager.LoadFont("PMU", 16);

            this.lblCreate = new Label("lblCreate");
            this.lblCreate.Location = new Point(40, 140);
            this.lblCreate.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblCreate.AutoSize = true;
            this.lblCreate.Text = "Create";
            this.lblCreate.Click += new EventHandler<MouseButtonEventArgs>(this.LblCreate_Click);

            this.AddWidget(this.lblPlayer);
            this.AddWidget(this.lblGuild);
            this.AddWidget(this.txtPlayer);
            this.AddWidget(this.txtGuild);
            this.AddWidget(this.lblCreate);

            this.LoadComplete();
        }

        private void LblCreate_Click(object sender, MouseButtonEventArgs e)
        {
            // Messenger.MakeGuild(txtPlayer.Text, txtGuild.Text);
        }
    }
}
