﻿// <copyright file="frmScriptEditor.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors.ScriptEditor
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Text;
    using System.Windows.Forms;
    using Network;
    using PMU.Sockets;

    internal partial class FrmScriptEditor : Form
    {
        public FrmScriptEditor()
        {
            this.InitializeComponent();
        }

        private delegate void AddDocumentTabDelegate(ScriptFileTab tab);

        public void AddDocumentTab(ScriptFileTab tab)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new AddDocumentTabDelegate(this.AddDocumentTab), tab);
            }
else
            {
                TabPage page = new TabPage(tab.File);
                tab.AddToTabPage(page);
                this.tabControl1.TabPages.Add(page);
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Messenger.SendPacket(TcpPacket.CreatePacket("requesteditscriptfile", (string)this.comboBox1.SelectedItem));
        }

        private delegate void SetFileListDelegate(List<string> files);

        public void SetFileList(List<string> files)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new SetFileListDelegate(this.SetFileList), files);
            }
else
            {
                this.comboBox1.Items.Clear();
                for (int i = 0; i < files.Count; i++)
                {
                    this.comboBox1.Items.Add(files[i]);
                }
            }
        }

        private delegate void SetClassesListDelegate(List<string> files);

        public void SetClassesList(List<string> files)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new SetClassesListDelegate(this.SetClassesList), files);
            }
else
            {
                this.comboBox2.Items.Clear();
                for (int i = 0; i < files.Count; i++)
                {
                    this.comboBox2.Items.Add(files[i]);
                }
            }
        }

        private delegate void SetMethodsListDelegate(List<string> files);

        public void SetMethodsList(List<string> files)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new SetMethodsListDelegate(this.SetMethodsList), files);
            }
else
            {
                this.comboBox3.Items.Clear();
                for (int i = 0; i < files.Count; i++)
                {
                    this.comboBox3.Items.Add(files[i]);
                }
            }
        }

        private delegate void SetErrorsListDelegate(List<string> errors);

        public void SetErrorsList(List<string> errors)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new SetErrorsListDelegate(this.SetErrorsList), errors);
            }
else
            {
                this.ScrollingListBox1.Items.Clear();
                for (int i = 0; i < errors.Count; i++)
                {
                    this.ScrollingListBox1.Items.Add(errors[i]);
                }
            }
        }

        private delegate void SetScriptParameterInfoDelegate(string info);

        public void SetScriptParameterInfo(string info)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new SetScriptParameterInfoDelegate(this.SetScriptParameterInfo), info);
            }
else
            {
                this.lblParameters.Text = "Parameters: " + info;
            }
        }

        private void SaveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ScriptFileTab tab = this.tabControl1.SelectedTab.Tag as ScriptFileTab;
            if (tab != null)
            {
                Messenger.SendPacket(TcpPacket.CreatePacket("savescript", tab.File.Replace(".cs", string.Empty),
                    tab.SyntaxBox.Document.Text.Replace(TcpPacket.SEP_CHAR, '/')));
            }
        }

        private void ReloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Messenger.SendPacket(TcpPacket.CreatePacket("reloadscripts"));
        }

        private void FinalizeScriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Messenger.SendPacket(TcpPacket.CreatePacket("finalizescript"));
        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Messenger.SendPacket(TcpPacket.CreatePacket("getscriptmethods", (string)this.comboBox2.SelectedItem));
        }

        private void ComboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            Messenger.SendPacket(TcpPacket.CreatePacket("getscriptparam", (string)this.comboBox3.Items[0], (string)this.comboBox3.SelectedItem, this.numericUpDown1.Value.ToString()));
        }

        private void NumericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            Messenger.SendPacket(TcpPacket.CreatePacket("getscriptparam", (string)this.comboBox3.Items[0], (string)this.comboBox3.SelectedItem, this.numericUpDown1.Value.ToString()));
        }

        private void AddToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Messenger.SendPacket(TcpPacket.CreatePacket("addnewclass", this.toolStripTextBox1.Text));
        }

        private void ScrollingListBox1_MouseLeave(object sender, EventArgs e)
        {
            this.ScrollingListBox1.Hide();
            this.pnlOptions.Show();
            this.panel2.Size = new Size(968, 42);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.pnlOptions.Hide();
            this.ScrollingListBox1.Show();
            this.panel2.Size = new Size(968, 186);
        }

        private void FrmScriptEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            Messenger.SendPacket(TcpPacket.CreatePacket("scripteditexit"));
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
