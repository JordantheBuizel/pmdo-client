﻿// <copyright file="ScriptFileTab.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors.ScriptEditor
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Forms;

    internal class ScriptFileTab
    {
        private Alsing.Windows.Forms.SyntaxBoxControl syntaxBox;
        private string file;

        public string File
        {
            get
            {
                return this.file;
            }
        }

        public Alsing.Windows.Forms.SyntaxBoxControl SyntaxBox
        {
            get { return this.syntaxBox; }
        }

        public ScriptFileTab()
        {
            this.Load();
        }

        private delegate void LoadDelegate();

        private void Load()
        {
            if (EditorManager.ScriptEditor.InvokeRequired)
            {
                EditorManager.ScriptEditor.Invoke(new LoadDelegate(this.Load));
            }
else
            {
                this.syntaxBox = new Alsing.Windows.Forms.SyntaxBoxControl();
                this.syntaxBox.AllowDrop = false;
                this.syntaxBox.Location = new System.Drawing.Point(0, 0);
                this.syntaxBox.Dock = DockStyle.Fill;
            }
        }

        public void AddToTabPage(TabPage tabPage)
        {
            tabPage.Controls.Add(this.syntaxBox);
            tabPage.Tag = this;
        }

        public void SetDocumentFile(string file)
        {
            this.file = file;
        }

        private delegate void SetDocumentDelegate(string text);

        public void SetDocumentText(string text)
        {
            if (EditorManager.ScriptEditor.InvokeRequired)
            {
                EditorManager.ScriptEditor.Invoke(new SetDocumentDelegate(this.SetDocumentText), text);
            }
else
            {
                this.syntaxBox.Document.SyntaxFile = IO.Paths.StartupPath + "Script/CSharp.syn";
                this.syntaxBox.Document.Text = text;
                this.syntaxBox.Document.ReParse();
            }
        }
    }
}
