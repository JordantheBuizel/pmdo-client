﻿// <copyright file="winAdminPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Network;
    using PMU.Core;
    using PMU.Sockets;
    using SdlDotNet.Widgets;

    internal class WinAdminPanel : Core.WindowCore
    {
        private readonly Panel pnlWeather;
        private readonly Panel pnlPlayerInfo;
        private readonly Panel pnlCommands;
        private readonly Button btnWeather;
        private readonly Button btnPlayerInfo;
        private readonly Button btnCommands;
        private readonly ComboBox cbWeather;
        private readonly Button btnApplyWeather;
        private readonly Button btnBanish;
        private readonly Button btnWarpTo;
        private readonly Button btnKick;
        private readonly Button btnSetAccess;
        private readonly ComboBox cbAccessLevel;
        private readonly Button btnRespawn;
        private readonly Button btnSetSprite;
        private readonly Button btnWarpMeTo;
        private readonly Button btnSetPlayerSprite;
        private readonly Button btnWarpToMe;
        private readonly Label lblPlayerName;
        private readonly TextBox txtPlayerName;
        private readonly Label lblSpriteNumber;
        private readonly TextBox txtSpriteNumber;
        private readonly Label lblAccessLevel;
        private readonly Label lblMapNumber;
        private readonly TextBox txtMapNumber;
        private readonly Button btnMapEditor;
        private readonly Button btnMapReport;
        private readonly Button btnSpells;
        private readonly Button btnItems;
        private readonly Button btnShops;
        private readonly Button btnEvolutions;
        private readonly Button btnStories; // And Why isn't this available for Mappers?
        private readonly Button btnNPC;

        // Button btnArrows;
        private readonly Button btnEmotion;
        private readonly Button btnRDungeons;
        private readonly Button btnMissions;
        private readonly Button btnDungeons;

        public WinAdminPanel()
            : base("winAdminPanel")
            {
            // this.Location = Graphics.DrawingSupport.GetCenter(this.Size);
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new Size(354, 220);
            this.Location = new Point(210, Windows.WindowSwitcher.GameWindow.ActiveTeam.Y + Windows.WindowSwitcher.GameWindow.ActiveTeam.Height + 0);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = true;
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "Administration Panel";

            this.pnlWeather = new Panel("pnlWeather");
            this.pnlWeather.Size = new Size(354, 220);
            this.pnlWeather.Location = new Point(0, 34);
            this.pnlWeather.BackColor = Color.White;
            this.pnlWeather.Visible = true;

            this.pnlPlayerInfo = new Panel("pnlPlayerInfo");
            this.pnlPlayerInfo.Size = new Size(410, 500);
            this.pnlPlayerInfo.Location = new Point(0, 34);
            this.pnlPlayerInfo.BackColor = Color.White;
            this.pnlPlayerInfo.Visible = false;

            this.pnlCommands = new Panel("pnlCommands");
            this.pnlCommands.Size = new Size(410, 348);
            this.pnlCommands.Location = new Point(0, 34);
            this.pnlCommands.BackColor = Color.White;
            this.pnlCommands.Visible = false;
            this.btnWeather = new Button("btnWeather");
            this.btnWeather.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnWeather.Location = new Point(27, 0);
            this.btnWeather.Size = new Size(100, 32);
            this.btnWeather.Text = "Weather";
            this.btnWeather.Selected = true;
            this.btnWeather.Click += new EventHandler<MouseButtonEventArgs>(this.BtnWeather_Click);

            this.btnPlayerInfo = new Button("btnPlayerInfo");
            this.btnPlayerInfo.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnPlayerInfo.Location = new Point(127, 0);
            this.btnPlayerInfo.Size = new Size(100, 32);
            this.btnPlayerInfo.Text = "Player Info";
            this.btnPlayerInfo.Selected = false;
            this.btnPlayerInfo.Click += new EventHandler<MouseButtonEventArgs>(this.BtnPlayerInfo_Click);

            this.btnCommands = new Button("btnCommands");
            this.btnCommands.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnCommands.Location = new Point(227, 0);
            this.btnCommands.Size = new Size(100, 32);
            this.btnCommands.Text = "Commands";
            this.btnCommands.Selected = false;
            this.btnCommands.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCommands_Click);

            this.cbWeather = new ComboBox("cbWeather");
            this.cbWeather.Location = new Point(110, 20);
            this.cbWeather.Size = new Size(134, 16);
            this.cbWeather.BackColor = Color.DarkGray;
            for (int i = 0; i < 13; i++)
            {
                ListBoxTextItem item = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), Enum.GetName(typeof(Enums.Weather), i));
                this.cbWeather.Items.Add(item);
            }

            this.cbWeather.SelectItem(0);

            this.btnApplyWeather = new Button("btnApplyWeather");
            this.btnApplyWeather.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnApplyWeather.Location = new Point(110, 54);
            this.btnApplyWeather.Size = new Size(134, 32);
            this.btnApplyWeather.Visible = true;
            this.btnApplyWeather.Text = "Apply";
            this.btnApplyWeather.Click += new EventHandler<MouseButtonEventArgs>(this.BtnApplyWeather_Click);

            this.btnBanish = new Button("btnBanish");
            this.btnBanish.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnBanish.Location = new Point(20, 20);
            this.btnBanish.Size = new Size(134, 32);
            this.btnBanish.Visible = true;
            this.btnBanish.Text = "Ban";
            this.btnBanish.Click += new EventHandler<MouseButtonEventArgs>(this.BtnBanish_Click);

            this.btnWarpTo = new Button("btnWarpTo");
            this.btnWarpTo.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnWarpTo.Location = new Point(200, 20);
            this.btnWarpTo.Size = new Size(134, 32);
            this.btnWarpTo.Visible = true;
            this.btnWarpTo.Text = "Warp to Map";
            this.btnWarpTo.Click += new EventHandler<MouseButtonEventArgs>(this.BtnWarpTo_Click);

            this.btnKick = new Button("btnKick");
            this.btnKick.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnKick.Location = new Point(20, 54);
            this.btnKick.Size = new Size(134, 32);
            this.btnKick.Visible = true;
            this.btnKick.Text = "Kick";
            this.btnKick.Click += new EventHandler<MouseButtonEventArgs>(this.BtnKick_Click);

            this.btnSetAccess = new Button("btnSetAccess");
            this.btnSetAccess.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnSetAccess.Location = new Point(200, 54);
            this.btnSetAccess.Size = new Size(134, 32);
            this.btnSetAccess.Visible = true;
            this.btnSetAccess.Text = "Set Access";
            this.btnSetAccess.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSetAccess_Click);

            this.cbAccessLevel = new ComboBox("cbAccessLevel");
            this.cbAccessLevel.Location = new Point(174, 274);
            this.cbAccessLevel.Size = new Size(134, 18);
            this.cbAccessLevel.BackColor = Color.DarkGray;
            for (int i = 0; i < 7; i++)
            {
                ListBoxTextItem item = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), Enum.GetName(typeof(Enums.Rank), i));
                this.cbAccessLevel.Items.Add(item);
            }

            this.cbAccessLevel.SelectItem(0);

            this.btnRespawn = new Button("btnRespawn");
            this.btnRespawn.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnRespawn.Location = new Point(20, 88);
            this.btnRespawn.Size = new Size(134, 32);
            this.btnRespawn.Visible = true;
            this.btnRespawn.Text = "Respawn";
            this.btnRespawn.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRespawn_Click);

            this.btnSetSprite = new Button("btnSetSprite");
            this.btnSetSprite.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnSetSprite.Location = new Point(200, 88);
            this.btnSetSprite.Size = new Size(134, 32);
            this.btnSetSprite.Visible = true;
            this.btnSetSprite.Text = "Set Species";
            this.btnSetSprite.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSetSprite_Click);

            this.btnWarpMeTo = new Button("btnWarpMeTo");
            this.btnWarpMeTo.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnWarpMeTo.Location = new Point(20, 122);
            this.btnWarpMeTo.Size = new Size(134, 32);
            this.btnWarpMeTo.Visible = true;
            this.btnWarpMeTo.Text = "Warp Me to";
            this.btnWarpMeTo.Click += new EventHandler<MouseButtonEventArgs>(this.BtnWarpMeTo_Click);

            this.btnSetPlayerSprite = new Button("btnSetPlayerSprite");
            this.btnSetPlayerSprite.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnSetPlayerSprite.Location = new Point(200, 122);
            this.btnSetPlayerSprite.Size = new Size(134, 32);
            this.btnSetPlayerSprite.Visible = true;
            this.btnSetPlayerSprite.Text = "Set Player Species";
            this.btnSetPlayerSprite.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSetPlayerSprite_Click);

            this.btnWarpToMe = new Button("btnWarpToMe");
            this.btnWarpToMe.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnWarpToMe.Location = new Point(110, 156);
            this.btnWarpToMe.Size = new Size(134, 32);
            this.btnWarpToMe.Visible = true;
            this.btnWarpToMe.Text = "Warp to Me";
            this.btnWarpToMe.Click += new EventHandler<MouseButtonEventArgs>(this.BtnWarpToMe_Click);

            this.lblPlayerName = new Label("lblPlayerName");
            this.lblPlayerName.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblPlayerName.Location = new Point(20, 210);
            this.lblPlayerName.Size = new Size(134, 32);
            this.lblPlayerName.Text = "Player Name:";
            this.lblPlayerName.Visible = true;

            this.txtPlayerName = new TextBox("txtPlayerName");
            this.txtPlayerName.Location = new Point(174, 219);
            this.txtPlayerName.Size = new Size(134, 18);
            this.txtPlayerName.Visible = true;

            this.lblSpriteNumber = new Label("lblSpriteNumber");
            this.lblSpriteNumber.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblSpriteNumber.Location = new Point(20, 242);
            this.lblSpriteNumber.Size = new Size(134, 32);
            this.lblSpriteNumber.Text = "Species Number:";
            this.lblSpriteNumber.Visible = true;

            this.txtSpriteNumber = new TextBox("txtSpriteNumber");
            this.txtSpriteNumber.Location = new Point(174, 251);
            this.txtSpriteNumber.Size = new Size(134, 18);
            this.txtSpriteNumber.Visible = true;

            this.lblAccessLevel = new Label("lblAccessLevel");
            this.lblAccessLevel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblAccessLevel.Location = new Point(20, 274);
            this.lblAccessLevel.Size = new Size(134, 32);
            this.lblAccessLevel.Text = "Access Level:";
            this.lblAccessLevel.Visible = true;

            // boxAccessLevel = new DropBox("boxAccessLevel");
            // boxAccessLevel.Location = new Point(174, 274);
            // boxAccessLevel.visible = false;
            this.lblMapNumber = new Label("lblMapNumber");
            this.lblMapNumber.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblMapNumber.Location = new Point(20, 306);
            this.lblMapNumber.Size = new Size(134, 32);
            this.lblMapNumber.Text = "Map Number:";
            this.lblMapNumber.Visible = true;

            this.txtMapNumber = new TextBox("txtMapNumber");
            this.txtMapNumber.Location = new Point(174, 315);
            this.txtMapNumber.Size = new Size(134, 18);
            this.txtMapNumber.Visible = true;

            this.btnMapEditor = new Button("btnMapEditor");
            this.btnMapEditor.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnMapEditor.Location = new Point(20, 20);
            this.btnMapEditor.Size = new Size(134, 32);
            this.btnMapEditor.Visible = true;
            this.btnMapEditor.Text = "Map Editor";
            this.btnMapEditor.Click += new EventHandler<MouseButtonEventArgs>(this.BtnMapEditor_Click);

            this.btnMapReport = new Button("btnMapReport");
            this.btnMapReport.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnMapReport.Location = new Point(200, 20);
            this.btnMapReport.Size = new Size(134, 32);
            this.btnMapReport.Visible = true;
            this.btnMapReport.Text = "Map Report";
            this.btnMapReport.Click += new EventHandler<MouseButtonEventArgs>(this.BtnMapReport_Click);

            this.btnSpells = new Button("btnSpells");
            this.btnSpells.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnSpells.Location = new Point(20, 54);
            this.btnSpells.Size = new Size(134, 32);
            this.btnSpells.Visible = true;
            this.btnSpells.Text = "Edit Moves";
            this.btnSpells.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSpells_Click);

            this.btnItems = new Button("btnItems");
            this.btnItems.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnItems.Location = new Point(200, 54);
            this.btnItems.Size = new Size(134, 32);
            this.btnItems.Visible = true;
            this.btnItems.Text = "Edit Items";
            this.btnItems.Click += new EventHandler<MouseButtonEventArgs>(this.BtnItems_Click);

            this.btnShops = new Button("btnShops");
            this.btnShops.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnShops.Location = new Point(20, 88);
            this.btnShops.Size = new Size(134, 32);
            this.btnShops.Visible = true;
            this.btnShops.Text = "Edit Shops";
            this.btnShops.Click += new EventHandler<MouseButtonEventArgs>(this.BtnShops_Click);

            this.btnEvolutions = new Button("btnEvolutions");
            this.btnEvolutions.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEvolutions.Location = new Point(200, 88);
            this.btnEvolutions.Size = new Size(134, 32);
            this.btnEvolutions.Visible = true;
            this.btnEvolutions.Text = "Edit Evolutions";
            this.btnEvolutions.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEvolutions_Click);

            this.btnStories = new Button("btnStories");
            this.btnStories.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnStories.Location = new Point(20, 122);
            this.btnStories.Size = new Size(134, 32);
            this.btnStories.Visible = true;
            this.btnStories.Text = "Story Editor";
            this.btnStories.Click += new EventHandler<MouseButtonEventArgs>(this.BtnStories_Click);

            this.btnNPC = new Button("btnNPC");
            this.btnNPC.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnNPC.Location = new Point(200, 122);
            this.btnNPC.Size = new Size(134, 32);
            this.btnNPC.Visible = true;
            this.btnNPC.Text = "NPC Editor";
            this.btnNPC.Click += new EventHandler<MouseButtonEventArgs>(this.BtnNPC_Click);

            // btnArrows = new Button("btnArrows");
            // btnArrows.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            // btnArrows.Location = new Point(20, 156);
            // btnArrows.Size = new System.Drawing.Size(134, 32);
            // btnArrows.Visible = true;
            // btnArrows.Text = "Edit Arrows";
            // btnArrows.Click += new EventHandler<MouseButtonEventArgs>(btnArrows_Click);
            this.btnEmotion = new Button("btnEmotion");
            this.btnEmotion.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEmotion.Location = new Point(200, 156);
            this.btnEmotion.Size = new Size(134, 32);
            this.btnEmotion.Visible = true;
            this.btnEmotion.Text = "Edit Emotions";
            this.btnEmotion.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEmotion_Click);

            this.btnRDungeons = new Button("btnRDungeons");
            this.btnRDungeons.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnRDungeons.Location = new Point(20, 156);
            this.btnRDungeons.Size = new Size(134, 32);
            this.btnRDungeons.Visible = true;
            this.btnRDungeons.Text = "Edit Random Dungeons";
            this.btnRDungeons.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRDungeons_Click);

            this.btnMissions = new Button("btnMissions");
            this.btnMissions.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnMissions.Location = new Point(20, 190);
            this.btnMissions.Size = new Size(134, 32);
            this.btnMissions.Visible = true;
            this.btnMissions.Text = "Edit Missions";
            this.btnMissions.Click += new EventHandler<MouseButtonEventArgs>(this.BtnMissions_Click);

            this.btnDungeons = new Button("btnDungeons");
            this.btnDungeons.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnDungeons.Location = new Point(200, 190);
            this.btnDungeons.Size = new Size(134, 32);
            this.btnDungeons.Visible = true;
            this.btnDungeons.Text = "Edit Dungeons";
            this.btnDungeons.Click += new EventHandler<MouseButtonEventArgs>(this.BtnDungeons_Click);
            this.pnlWeather.AddWidget(this.cbWeather);
            this.pnlWeather.AddWidget(this.btnApplyWeather);
            this.pnlPlayerInfo.AddWidget(this.btnBanish);
            this.pnlPlayerInfo.AddWidget(this.btnWarpTo);
            this.pnlPlayerInfo.AddWidget(this.btnKick);
            this.pnlPlayerInfo.AddWidget(this.btnSetAccess);
            this.pnlPlayerInfo.AddWidget(this.btnRespawn);
            this.pnlPlayerInfo.AddWidget(this.btnSetSprite);
            this.pnlPlayerInfo.AddWidget(this.btnWarpMeTo);
            this.pnlPlayerInfo.AddWidget(this.btnSetPlayerSprite);
            this.pnlPlayerInfo.AddWidget(this.btnWarpToMe);
            this.pnlPlayerInfo.AddWidget(this.lblPlayerName);
            this.pnlPlayerInfo.AddWidget(this.txtPlayerName);
            this.pnlPlayerInfo.AddWidget(this.lblSpriteNumber);
            this.pnlPlayerInfo.AddWidget(this.txtSpriteNumber);
            this.pnlPlayerInfo.AddWidget(this.lblAccessLevel);
            this.pnlPlayerInfo.AddWidget(this.cbAccessLevel);
            this.pnlPlayerInfo.AddWidget(this.lblMapNumber);
            this.pnlPlayerInfo.AddWidget(this.txtMapNumber);
            this.pnlCommands.AddWidget(this.btnMapEditor);
            this.pnlCommands.AddWidget(this.btnMapReport);
            this.pnlCommands.AddWidget(this.btnSpells);
            this.pnlCommands.AddWidget(this.btnItems);
            this.pnlCommands.AddWidget(this.btnShops);
            this.pnlCommands.AddWidget(this.btnEvolutions);
            this.pnlCommands.AddWidget(this.btnStories);
            this.pnlCommands.AddWidget(this.btnNPC);

            // pnlCommands.AddWidget(btnArrows);
            this.pnlCommands.AddWidget(this.btnEmotion);
            this.pnlCommands.AddWidget(this.btnRDungeons);
            this.pnlCommands.AddWidget(this.btnMissions);
            this.pnlCommands.AddWidget(this.btnDungeons);

            this.AddWidget(this.pnlWeather);
            this.AddWidget(this.btnWeather);
            this.AddWidget(this.pnlPlayerInfo);
            this.AddWidget(this.btnPlayerInfo);
            this.AddWidget(this.pnlCommands);
            this.AddWidget(this.btnCommands);

            this.LoadComplete();
        }

        private void BtnWeather_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnWeather.Selected)
            {
                this.btnWeather.Selected = true;
                this.btnPlayerInfo.Selected = false;
                this.btnCommands.Selected = false;
                this.pnlWeather.Visible = true;
                this.pnlPlayerInfo.Visible = false;
                this.pnlCommands.Visible = false;
                this.btnPlayerInfo.Location = new Point(127, 0);
                this.btnCommands.Location = new Point(227, 0);
                this.Size = new Size(354, 220);
                this.TitleBar.Text = "Administration Panel - Weather";
            }
        }

        private void BtnPlayerInfo_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnPlayerInfo.Selected)
            {
                this.btnWeather.Selected = false;
                this.btnPlayerInfo.Selected = true;
                this.btnCommands.Selected = false;
                this.pnlWeather.Visible = false;
                this.pnlPlayerInfo.Visible = true;
                this.pnlCommands.Visible = false;
                this.btnWeather.Location = new Point(27, 0);
                this.btnCommands.Location = new Point(227, 0);
                this.Size = new Size(354, 410);
                this.TitleBar.Text = "Administration Panel - Player Info";
            }
        }

        private void BtnCommands_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.btnCommands.Selected)
            {
                this.btnWeather.Selected = false;
                this.btnPlayerInfo.Selected = false;
                this.btnCommands.Selected = true;
                this.pnlWeather.Visible = false;
                this.pnlPlayerInfo.Visible = false;
                this.pnlCommands.Visible = true;
                this.btnWeather.Location = new Point(27, 0);
                this.btnPlayerInfo.Location = new Point(127, 0);
                this.Size = new Size(354, 290);
                this.TitleBar.Text = "Administration Panel - Developer Commands";
            }
        }

        private void BtnApplyWeather_Click(object sender, MouseButtonEventArgs e)
        {
                Messenger.Weather(this.cbWeather.SelectedIndex);
        }

        private void BtnBanish_Click(object sender, MouseButtonEventArgs e)
        {
            // Used with txtPlayerName
            Messenger.BanPlayer(this.txtPlayerName.Text);
        }

        private void BtnWarpTo_Click(object sender, MouseButtonEventArgs e)
        {
            // Used with txtMapNumber
            if (this.txtMapNumber.Text.IsNumeric())
            {
            Messenger.WarpTo(this.txtMapNumber.Text.ToInt());
            }
        }

        private void BtnKick_Click(object sender, MouseButtonEventArgs e)
        {
            // Used with txtPlayerName
            Messenger.KickPlayer(this.txtPlayerName.Text);
        }

        private void BtnSetAccess_Click(object sender, MouseButtonEventArgs e)
        {
            // To be used with BoxAccessLevel and txtPlayerName
            Messenger.SetAccess(this.txtPlayerName.Text, this.cbAccessLevel.SelectedIndex);
        }

        private void BtnRespawn_Click(object sender, MouseButtonEventArgs e)
        {
            // null
            Messenger.MapRespawn();
        }

        private void BtnSetSprite_Click(object sender, MouseButtonEventArgs e)
        {
            // something to do with txtSpriteNumber
            Messenger.SetSprite(this.txtSpriteNumber.Text.ToInt());
        }

        private void BtnWarpMeTo_Click(object sender, MouseButtonEventArgs e)
        {
            // Used with txtPlayerName
            Messenger.WarpMeTo(this.txtPlayerName.Text);
        }

        private void BtnSetPlayerSprite_Click(object sender, MouseButtonEventArgs e)
        {
            // Used with txtPlayerName and txtSpriteNumber
            Messenger.SetPlayerSprite(this.txtPlayerName.Text, this.txtSpriteNumber.Text.ToInt());
        }

        private void BtnWarpToMe_Click(object sender, MouseButtonEventArgs e)
        {
            // Used with txtPlayerName
            Messenger.WarpToMe(this.txtPlayerName.Text);
        }

        private void BtnMapEditor_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendPacket(TcpPacket.CreatePacket("requesteditmap"));
        }

        private void BtnMapReport_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            if (Windows.WindowSwitcher.ExpKit.KitContainer.ModuleSwitcher.IsModuleAvailable(Enums.ExpKitModules.MapReport))
            {
                Windows.WindowSwitcher.ExpKit.KitContainer.SetActiveModule(Enums.ExpKitModules.MapReport);
            }
        }

        private void BtnSpells_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendPacket(TcpPacket.CreatePacket("requesteditmove"));
        }

        private void BtnItems_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendPacket(TcpPacket.CreatePacket("requestedititem"));
            return;
        }

        private void BtnShops_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendPacket(TcpPacket.CreatePacket("requesteditshop"));
            return;
        }

        private void BtnEvolutions_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendPacket(TcpPacket.CreatePacket("requesteditevo"));
            return;
        }

        private void BtnStories_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendPacket(TcpPacket.CreatePacket("requesteditstory"));
            return;
        }

        private void BtnNPC_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendPacket(TcpPacket.CreatePacket("requesteditnpc"));
            return;
        }

        private void BtnArrows_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendPacket(TcpPacket.CreatePacket("requesteditarrow"));
            return;
        }

        private void BtnEmotion_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendPacket(TcpPacket.CreatePacket("requesteditemoticon"));
            return;
        }

        private void BtnRDungeons_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendPacket(TcpPacket.CreatePacket("requesteditrdungeon"));
            return;
        }

        private void BtnDungeons_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendPacket(TcpPacket.CreatePacket("requesteditdungeon"));
            return;
        }

        private void BtnMissions_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendPacket(TcpPacket.CreatePacket("requesteditmission"));
            return;
        }
    }
}
