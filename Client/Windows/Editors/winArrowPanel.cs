﻿// <copyright file="winArrowPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Network;
    using PMU.Core;
    using PMU.Sockets;
    using SdlDotNet.Widgets;

    internal class WinArrowPanel : Core.WindowCore
    {
        private int itemNum = -1;
        private int currentTen = 0;

        private readonly Panel pnlArrowList;
        private readonly Panel pnlArrowEditor;

        private readonly ScrollingListBox lbxArrowList;
        private readonly ListBoxTextItem lbiArrow;

        // Button btnAddNew; (Can implement later...)
        private readonly Button btnBack;
        private readonly Button btnForward;
        private readonly Button btnCancel;
        private readonly Button btnEdit;

        private readonly Button btnEditorCancel;
        private readonly Button btnEditorOK;

        private readonly Label lblName;
        private readonly TextBox txtName;
        private readonly Label lblSprite;
        private readonly HScrollBar hsbPic;
        private readonly PictureBox pic;
        private readonly Label lblRange;
        private readonly HScrollBar hsbRange;
        private readonly Label lblAmount;
        private readonly HScrollBar hsbAmount;

        public WinArrowPanel()
            : base("winArrowPanel")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new Size(200, 230);
            this.Location = new Point(210, Windows.WindowSwitcher.GameWindow.ActiveTeam.Y + Windows.WindowSwitcher.GameWindow.ActiveTeam.Height + 0);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = true;
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "Arrow Panel";

            this.pnlArrowList = new Panel("pnlArrowList");
            this.pnlArrowList.Size = new Size(200, 230);
            this.pnlArrowList.Location = new Point(0, 0);
            this.pnlArrowList.BackColor = Color.White;
            this.pnlArrowList.Visible = true;

            this.pnlArrowEditor = new Panel("pnlArrowEditor");
            this.pnlArrowEditor.Size = new Size(230, 261);
            this.pnlArrowEditor.Location = new Point(0, 0);
            this.pnlArrowEditor.BackColor = Color.White;
            this.pnlArrowEditor.Visible = false;

            this.lbxArrowList = new ScrollingListBox("lbxArrowList");
            this.lbxArrowList.Location = new Point(10, 10);
            this.lbxArrowList.Size = new Size(180, 140);
            for (int i = 0; i < 10; i++)
            {
                this.lbiArrow = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (i + 1) + ": " + Arrows.ArrowHelper.Arrows[(i + 1) + (10 * this.currentTen)].Name);
                this.lbxArrowList.Items.Add(this.lbiArrow);
            }

            this.btnBack = new Button("btnBack");
            this.btnBack.Location = new Point(10, 160);
            this.btnBack.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnBack.Size = new Size(64, 16);
            this.btnBack.Visible = true;
            this.btnBack.Text = "<--";
            this.btnBack.Click += new EventHandler<MouseButtonEventArgs>(this.BtnBack_Click);

            this.btnForward = new Button("btnForward");
            this.btnForward.Location = new Point(126, 160);
            this.btnForward.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnForward.Size = new Size(64, 16);
            this.btnForward.Visible = true;
            this.btnForward.Text = "-->";
            this.btnForward.Click += new EventHandler<MouseButtonEventArgs>(this.BtnForward_Click);

            this.btnEdit = new Button("btnEdit");
            this.btnEdit.Location = new Point(10, 190);
            this.btnEdit.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEdit.Size = new Size(64, 16);
            this.btnEdit.Visible = true;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEdit_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(126, 190);
            this.btnCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnCancel.Size = new Size(64, 16);
            this.btnCancel.Visible = true;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            // btnAddNew = new Button("btnAddNew");
            // btnAddNew.Location = new Point();
            // btnAddNew.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            // btnAddNew.Size = new System.Drawing.Size(64, 16);
            // btnAddNew.Visible = true;
            // btnAddNew.Text = "Add New";
            // btnAddNew.Click += new EventHandler<MouseButtonEventArgs>(btnAddNew_Click);
            this.btnEditorCancel = new Button("btnEditorCancel");
            this.btnEditorCancel.Location = new Point(120, 215);
            this.btnEditorCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorCancel.Size = new Size(64, 16);
            this.btnEditorCancel.Visible = true;
            this.btnEditorCancel.Text = "Cancel";
            this.btnEditorCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorCancel_Click);

            this.btnEditorOK = new Button("btnEditorOK");
            this.btnEditorOK.Location = new Point(20, 215);
            this.btnEditorOK.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorOK.Size = new Size(64, 16);
            this.btnEditorOK.Visible = true;
            this.btnEditorOK.Text = "OK";
            this.btnEditorOK.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorOK_Click);

            this.lblName = new Label("lblName");
            this.lblName.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblName.Text = "Arrow Name:";
            this.lblName.AutoSize = true;
            this.lblName.Location = new Point(10, 4);

            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(200, 16);
            this.txtName.Location = new Point(10, 16);
            this.txtName.Font = Graphic.FontManager.LoadFont("Tahoma", 12);

            this.lblSprite = new Label("lblSprite");
            this.lblSprite.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblSprite.Text = "Arrow Sprite:";
            this.lblSprite.AutoSize = true;
            this.lblSprite.Location = new Point(10, 36);

            this.pic = new PictureBox("pic");
            this.pic.Location = new Point(10, 48);
            this.pic.Size = new Size(32, 32);

            this.hsbPic = new HScrollBar("hsbPic");
            this.hsbPic.Maximum = MaxInfo.MAXARROWS;
            this.hsbPic.Location = new Point(10, 90);
            this.hsbPic.Size = new Size(200, 12);
            this.hsbPic.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.HsbPic_ValueChanged);

            this.lblRange = new Label("lblRange");
            this.lblRange.AutoSize = true;
            this.lblRange.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblRange.Location = new Point(10, 115);
            this.lblRange.Text = "Range: -1";

            this.hsbRange = new HScrollBar("hsbRange");
            this.hsbRange.Maximum = 50;
            this.hsbRange.Location = new Point(10, 140);
            this.hsbRange.Size = new Size(200, 12);
            this.hsbRange.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.HsbRange_ValueChanged);

            this.lblAmount = new Label("lblAmount");
            this.lblAmount.AutoSize = true;
            this.lblAmount.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblAmount.Location = new Point(10, 165);
            this.lblAmount.Text = "Amount: -1";

            this.hsbAmount = new HScrollBar("hsbAmount");
            this.hsbAmount.Maximum = 15;
            this.hsbAmount.Location = new Point(10, 190);
            this.hsbAmount.Size = new Size(200, 12);
            this.hsbAmount.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.HsbAmount_ValueChanged);

            this.pnlArrowList.AddWidget(this.lbxArrowList);
            this.pnlArrowList.AddWidget(this.btnBack);
            this.pnlArrowList.AddWidget(this.btnForward);

            // pnlArrowList.AddWidget(btnAddNew);
            this.pnlArrowList.AddWidget(this.btnEdit);
            this.pnlArrowList.AddWidget(this.btnCancel);

            this.pnlArrowEditor.AddWidget(this.lblName);
            this.pnlArrowEditor.AddWidget(this.txtName);
            this.pnlArrowEditor.AddWidget(this.lblSprite);
            this.pnlArrowEditor.AddWidget(this.pic);
            this.pnlArrowEditor.AddWidget(this.hsbPic);
            this.pnlArrowEditor.AddWidget(this.lblRange);
            this.pnlArrowEditor.AddWidget(this.hsbRange);
            this.pnlArrowEditor.AddWidget(this.lblAmount);
            this.pnlArrowEditor.AddWidget(this.hsbAmount);
            this.pnlArrowEditor.AddWidget(this.btnEditorCancel);
            this.pnlArrowEditor.AddWidget(this.btnEditorOK);

            this.AddWidget(this.pnlArrowList);
            this.AddWidget(this.pnlArrowEditor);

            this.RefreshArrowList();
            this.LoadComplete();
        }

        private void BtnBack_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen > 0)
            {
                this.currentTen--;
            }

            this.RefreshArrowList();
        }

        private void BtnForward_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen < ((MaxInfo.MAXARROWS - 1) / 10))
            {
                this.currentTen++;
            }

            this.RefreshArrowList();
        }

        public void RefreshArrowList()
        {
            for (int i = 0; i < 10; i++)
            {
                if ((i + (this.currentTen * 10)) < MaxInfo.MAXARROWS)
                {
                    ((ListBoxTextItem)this.lbxArrowList.Items[i]).Text = (i + (10 * this.currentTen)) + 1 + ": " + Arrows.ArrowHelper.Arrows[i + (10 * this.currentTen)].Name;
                }
else
                {
                    ((ListBoxTextItem)this.lbxArrowList.Items[i]).Text = "---";
                }
            }
        }

        private void BtnEdit_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxArrowList.SelectedItems.Count == 1)
            {
                string[] index = ((ListBoxTextItem)this.lbxArrowList.SelectedItems[0]).Text.Split(':');
                if (index[0].IsNumeric())
                {
                    this.itemNum = index[0].ToInt() - 1;
                    Messenger.SendEditArrow(this.itemNum);
                    this.pnlArrowList.Visible = false;
                }
            }
        }

        public void DisplayArrowData()
        {
            // First, get the arrow instance based on the stored arrow index
            Arrows.Arrow arrow = Arrows.ArrowHelper.Arrows[this.itemNum];

            // Update the widgets with the arrow data
            this.txtName.Text = arrow.Name;

            // pic.Image = Tools.CropImage(Logic.Graphic.GraphicsManager.Arrows, new Rectangle(0, arrow.Pic * 32, 32, 32));
            this.hsbAmount.Value = arrow.Amount;
            this.hsbRange.Value = arrow.Range;

            this.pnlArrowEditor.Visible = true;
            this.Size = new Size(this.pnlArrowEditor.Width, this.pnlArrowEditor.Height);
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            return;
        }

        private void BtnEditorCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.itemNum = -1;
            this.pnlArrowEditor.Visible = false;
            this.pnlArrowList.Visible = true;
            this.Size = new Size(this.pnlArrowList.Width, this.pnlArrowList.Height);
        }

        private void HsbRange_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.lblRange.Text != "Range: " + e.NewValue.ToString())
            {
                this.lblRange.Text = "Range: " + e.NewValue.ToString();
            }
        }

        private void HsbAmount_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.lblAmount.Text != "Amount: " + e.NewValue.ToString())
            {
                this.lblAmount.Text = "Amount: " + e.NewValue.ToString();
            }
        }

        private void HsbPic_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            // pic.Image = Tools.CropImage(Logic.Graphic.GraphicsManager.Arrows, new Rectangle(0, hsbPic.Value * 32, 32, 32));
        }

        private void BtnEditorOK_Click(object sender, MouseButtonEventArgs e)
        {
            Arrows.Arrow arrowToSend = new Arrows.Arrow();
            arrowToSend.Name = this.txtName.Text;
            arrowToSend.Pic = this.hsbPic.Value;
            arrowToSend.Range = this.hsbRange.Value;
            arrowToSend.Amount = this.hsbAmount.Value;
            Messenger.SendSaveArrow(this.itemNum, arrowToSend);
            this.pnlArrowEditor.Visible = false;
            this.pnlArrowList.Visible = true;
            this.Size = new Size(this.pnlArrowList.Width, this.pnlArrowList.Height);
        }
    }
}
