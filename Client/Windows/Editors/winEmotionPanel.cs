﻿// <copyright file="winEmotionPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Network;
    using PMU.Core;
    using PMU.Sockets;
    using SdlDotNet.Widgets;

    internal class WinEmotionPanel : Core.WindowCore
    {
        private int itemNum = 0;

        private readonly Panel pnlEmoteList;
        private readonly Panel pnlEmoteEditor;

        private readonly ScrollingListBox lbxEmotionList;
        private readonly ListBoxTextItem lbiEmote;

        // Button btnAddNew; (Can implement later... Needed?)
        private readonly Button btnCancel;
        private readonly Button btnEdit;

        private readonly Button btnEditorCancel;
        private readonly Button btnEditorOK;

        private readonly HScrollBar hsbENum;
        private readonly Label lblENum;
        private readonly PictureBox picEmote;
        private readonly Label lblCommand;
        private readonly TextBox txtCommand;

        public WinEmotionPanel()
            : base("winEmotionPanel")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new Size(200, 230);
            this.Location = new Point(210, Windows.WindowSwitcher.GameWindow.ActiveTeam.Y + Windows.WindowSwitcher.GameWindow.ActiveTeam.Height + 0);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = true;
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "Emotion Panel";

            this.pnlEmoteList = new Panel("pnlEmoteList");
            this.pnlEmoteList.Size = new Size(200, 230);
            this.pnlEmoteList.Location = new Point(0, 0);
            this.pnlEmoteList.BackColor = Color.White;
            this.pnlEmoteList.Visible = true;

            this.pnlEmoteEditor = new Panel("pnlEmoteEditor");
            this.pnlEmoteEditor.Size = new Size(230, 166);
            this.pnlEmoteEditor.Location = new Point(0, 0);
            this.pnlEmoteEditor.BackColor = Color.White;
            this.pnlEmoteEditor.Visible = false;

            this.lbxEmotionList = new ScrollingListBox("lbxEmotionList");
            this.lbxEmotionList.Location = new Point(10, 10);
            this.lbxEmotionList.Size = new Size(180, 140);
                for (int i = 0; i < 10; i++)
                {
                this.lbiEmote = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), i + ": ---");
                this.lbxEmotionList.Items.Add(this.lbiEmote);
                }

            this.btnEdit = new Button("btnEdit");
            this.btnEdit.Location = new Point(10, 190);
            this.btnEdit.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEdit.Size = new Size(64, 16);
            this.btnEdit.Visible = true;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEdit_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Location = new Point(126, 190);
            this.btnCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnCancel.Size = new Size(64, 16);
            this.btnCancel.Visible = true;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            // btnAddNew = new Button("btnAddNew");
            // btnAddNew.Location = new Point();
            // btnAddNew.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            // btnAddNew.Size = new System.Drawing.Size(64, 16);
            // btnAddNew.Visible = true;
            // btnAddNew.Text = "Add New";
            // btnAddNew.Click += new EventHandler<MouseButtonEventArgs>(btnAddNew_Click);
            this.btnEditorCancel = new Button("btnEditorCancel");
            this.btnEditorCancel.Location = new Point(120, 125);
            this.btnEditorCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorCancel.Size = new Size(64, 16);
            this.btnEditorCancel.Visible = true;
            this.btnEditorCancel.Text = "Cancel";
            this.btnEditorCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorCancel_Click);

            this.btnEditorOK = new Button("btnEditorOK");
            this.btnEditorOK.Location = new Point(20, 125);
            this.btnEditorOK.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.btnEditorOK.Size = new Size(64, 16);
            this.btnEditorOK.Visible = true;
            this.btnEditorOK.Text = "OK";
            this.btnEditorOK.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorOK_Click);

            this.lblENum = new Label("lblENum");
            this.lblENum.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblENum.Text = "Emoticon: ";
            this.lblENum.AutoSize = true;
            this.lblENum.Location = new Point(10, 4);

            this.txtCommand = new TextBox("txtCommand");
            this.txtCommand.Size = new Size(200, 16);
            this.txtCommand.Location = new Point(10, 94);
            this.txtCommand.Font = Graphic.FontManager.LoadFont("Tahoma", 12);

            this.lblCommand = new Label("lblCommand");
            this.lblCommand.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
            this.lblCommand.Text = "Command:";
            this.lblCommand.AutoSize = true;
            this.lblCommand.Location = new Point(10, 80);

            this.picEmote = new PictureBox("picEmote");
            this.picEmote.Location = new Point(10, 18);
            this.picEmote.Size = new Size(32, 32);

            this.hsbENum = new HScrollBar("hsbPic");
            this.hsbENum.Maximum = MaxInfo.MaxEmoticons;
            this.hsbENum.Location = new Point(10, 54);
            this.hsbENum.Size = new Size(200, 12);
            this.hsbENum.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.HsbENum_ValueChanged);

            this.pnlEmoteList.AddWidget(this.lbxEmotionList);

            // pnlEmoteList.AddWidget(btnAddNew); Needed?
            this.pnlEmoteList.AddWidget(this.btnEdit);
            this.pnlEmoteList.AddWidget(this.btnCancel);

            this.pnlEmoteEditor.AddWidget(this.hsbENum);
            this.pnlEmoteEditor.AddWidget(this.lblENum);
            this.pnlEmoteEditor.AddWidget(this.picEmote);
            this.pnlEmoteEditor.AddWidget(this.lblCommand);
            this.pnlEmoteEditor.AddWidget(this.txtCommand);
            this.pnlEmoteEditor.AddWidget(this.btnEditorCancel);
            this.pnlEmoteEditor.AddWidget(this.btnEditorOK);

            this.AddWidget(this.pnlEmoteList);
            this.AddWidget(this.pnlEmoteEditor);

            this.RefreshEmoteList();
            this.LoadComplete();
        }

        public void RefreshEmoteList()
        {
            for (int i = 0; i < MaxInfo.MaxEmoticons; i++)
            {
                try
                {
                    if (i < MaxInfo.MaxEmoticons)
                    {
                        ((ListBoxTextItem)this.lbxEmotionList.Items[i]).Text = i + ": " + Emotions.EmotionHelper.Emotions[i].Command;
                    }
else
                    {
                        ((ListBoxTextItem)this.lbxEmotionList.Items[i]).Text = i + ": ---";
                    }
                }
                catch
                {
                    ((ListBoxTextItem)this.lbxEmotionList.Items[i]).Text = i + ": ---";
                }
            }
        }

        private void BtnEdit_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxEmotionList.SelectedItems.Count == 1)
            {
                string[] index = ((ListBoxTextItem)this.lbxEmotionList.SelectedItems[0]).Text.Split(':');
                if (index[0].IsNumeric())
                {
                    this.itemNum = index[0].ToInt();
                    Messenger.SendEditEmotion(this.itemNum);
                    this.pnlEmoteList.Visible = false;
                }
            }
        }

        public void DisplayEmotionData()
        {
            // First, get the emote instance based on the stored emote index
            Emotions.Emotion emote = Emotions.EmotionHelper.Emotions[this.itemNum];

            // Update the widgets with the emote data
            this.txtCommand.Text = emote.Command;

            // picEmote.Image = Tools.CropImage(Logic.Graphic.GraphicsManager.Emoticons, new Rectangle(0, emote.Pic * 32, 32, 32));
            this.hsbENum.Value = emote.Pic;

            this.pnlEmoteEditor.Visible = true;
            this.Size = new Size(this.pnlEmoteEditor.Width, this.pnlEmoteEditor.Height);
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            return;
        }

        private void BtnEditorCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.itemNum = 0;
            this.pnlEmoteEditor.Visible = false;
            this.pnlEmoteList.Visible = true;
            this.Size = new Size(this.pnlEmoteList.Width, this.pnlEmoteList.Height);
        }

        private void HsbENum_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.lblENum.Text != "Emoticon: " + e.NewValue.ToString())
            {
                this.lblENum.Text = "Emoticon: " + e.NewValue.ToString();

                // picEmote.Image = Tools.CropImage(Logic.Graphic.GraphicsManager.Emoticons, new Rectangle(0, hsbENum.Value * 32, 32, 32));
            }
        }

        private void BtnEditorOK_Click(object sender, MouseButtonEventArgs e)
        {
            Emotions.Emotion emoticonToSend = new Emotions.Emotion();
            emoticonToSend.Command = this.txtCommand.Text;
            emoticonToSend.Pic = this.hsbENum.Value;
            Messenger.SendSaveEmotion(this.itemNum, emoticonToSend);
            this.pnlEmoteEditor.Visible = false;
            this.pnlEmoteList.Visible = true;
            this.Size = new Size(this.pnlEmoteList.Width, this.pnlEmoteList.Height);
        }
    }
}
