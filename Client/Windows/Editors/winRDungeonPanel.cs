﻿// <copyright file="winRDungeonPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Logic.Editors.RDungeons;
    using Extensions;
    using Maps;
    using Network;
    using PMU.Core;
    using PMU.Sockets;
    using SdlDotNet.Widgets;

    internal class WinRDungeonPanel : Core.WindowCore
    {
        // int itemNum = 0;
        private int currentTen = 0;
        private int editedTile = -1;
        private EditableRDungeon rdungeon;
        private Panel pnlRDungeonList;
        private Panel pnlRDungeonGeneral;
        private Panel pnlRDungeonFloors;
        private Panel pnlRDungeonFloorSettingSelection;
        private Panel pnlRDungeonStructure;
        private Panel pnlRDungeonLandTiles;
        private Panel pnlRDungeonLandAltTiles;
        private Panel pnlRDungeonWaterTiles;
        private Panel pnlRDungeonWaterAnimTiles;
        private Panel pnlRDungeonAttributes;
        private Panel pnlRDungeonItems;
        private Panel pnlRDungeonNpcs;
        private Panel pnlRDungeonTraps;
        private Panel pnlRDungeonWeather;
        private Panel pnlRDungeonGoal;
        private Panel pnlRDungeonChambers;
        private Panel pnlRDungeonMisc;
        private Panel pnlTileSelector;

        private bool listLoaded;
        private bool generalLoaded;
        private bool floorsLoaded;
        private bool floorSettingSelectionLoaded;
        private bool structureLoaded;
        private bool landTilesLoaded;
        private bool landAltTilesLoaded;
        private bool waterTilesLoaded;
        private bool waterAnimTilesLoaded;
        private bool attributesLoaded;
        private bool itemsLoaded;
        private bool npcsLoaded;
        private bool trapsLoaded;
        private bool weatherLoaded;
        private bool goalLoaded;
        private bool chambersLoaded;
        private bool miscLoaded;
        private bool tileSelectorLoaded;
        private ScrollingListBox lbxRDungeonList;

        // ListBoxTextItem lbiRDungeon;
        private Button btnBack;
        private Button btnForward;
        private Button btnAddNew;

        // Implement btnAddNew to certain editors when Ready
        private Button btnCancel;
        private Button btnEdit;
        private Label lblGeneral;
        private Button btnFloors;
        private Label lblDungeonName;
        private TextBox txtDungeonName;
        private Label lblDirection;
        private RadioButton optUp;
        private RadioButton optDown;
        private Label lblMaxFloors;
        private NumericUpDown nudMaxFloors;
        private CheckBox chkRecruiting;
        private CheckBox chkEXPGain;
        private Label lblWindTimer;
        private NumericUpDown nudWindTimer;
        private Button btnEditorCancel;
        private Button btnSave;
        private Label lblFloors;
        private Button btnGeneral;
        private Label lblFromFloorNumber;
        private Label lblToFloorNumber;
        private NumericUpDown nudFirstFloor;
        private NumericUpDown nudLastFloor;
        private Button btnSettingsMenu;
        private Button btnSaveFloor;
        private Button btnLoadFloor;
        private Label lblSaveLoadMessage;
        private Button btnStructure;
        private Button btnLandTiles;
        private Button btnWaterTiles;
        private Button btnAttributes;
        private Button btnItems;
        private Button btnNpcs;
        private Button btnTraps;
        private Button btnWeather;
        private Button btnGoal;
        private Button btnChambers;
        private Button btnMisc;
        private Label lblStructure;
        private Label lblTrapMin;
        private NumericUpDown nudTrapMin;
        private Label lblTrapMax;
        private NumericUpDown nudTrapMax;
        private Label lblItemMin;
        private NumericUpDown nudItemMin;
        private Label lblItemMax;
        private NumericUpDown nudItemMax;
        private Label lblRoomWidthMin;
        private NumericUpDown nudRoomWidthMin;
        private Label lblRoomWidthMax;
        private NumericUpDown nudRoomWidthMax;
        private Label lblRoomLengthMin;
        private NumericUpDown nudRoomLengthMin;
        private Label lblRoomLengthMax;
        private NumericUpDown nudRoomLengthMax;
        private Label lblHallTurnMin;
        private NumericUpDown nudHallTurnMin;
        private Label lblHallTurnMax;
        private NumericUpDown nudHallTurnMax;
        private Label lblHallVarMin;
        private NumericUpDown nudHallVarMin;
        private Label lblHallVarMax;
        private NumericUpDown nudHallVarMax;
        private Label lblWaterFrequency;
        private NumericUpDown nudWaterFrequency;
        private Label lblCraters;
        private NumericUpDown nudCraters;
        private Label lblCraterMinLength;
        private NumericUpDown nudCraterMinLength;
        private Label lblCraterMaxLength;
        private NumericUpDown nudCraterMaxLength;
        private CheckBox chkCraterFuzzy;
        private Label lblLandTiles;

        private Label[] lblLandTileset;
        private PictureBox[] picLandTileset;
        private int[,] landTileNumbers;
        private Button btnLandAltSwitch;

        /*
        Label lblStairs;
        Label lblGround;
        Label lblTopLeft;
        Label lblTopCenter;
        Label lblTopRight;
        Label lblCenterLeft;
        Label lblCenterCenter;
        Label lblCenterRight;
        Label lblBottomLeft;
        Label lblBottomCenter;
        Label lblBottomRight;
        Label lblInnerTopLeft;
        Label lblInnerTopRight;
        Label lblInnerBottomLeft;
        Label lblInnerBottomRight;
        Label lblIsolatedWall;
        Label lblColumnTop;
        Label lblColumnCenter;
        Label lblColumnBottom;
        Label lblRowLeft;
        Label lblRowCenter;
        Label lblRowRight;

        */
        private Label lblLandAltTiles;

        private Label[] lblLandAltTileset;
        private PictureBox[] picLandAltTileset;
        private int[,] landAltTileNumbers;
        private Button btnLandSwitch;

        /*
        Label lblStairsAlt;
        Label lblGroundAlt;
        Label lblTopLeftAlt;
        Label lblTopCenterAlt;
        Label lblTopRightAlt;
        Label lblCenterLeftAlt;
        Label lblCenterCenterAlt;
        Label lblCenterRightAlt;
        Label lblBottomLeftAlt;
        Label lblBottomCenterAlt;
        Label lblBottomRightAlt;
        Label lblInnerTopLeftAlt;
        Label lblInnerTopRightAlt;
        Label lblInnerBottomLeftAlt;
        Label lblInnerBottomRightAlt;
        Label lblIsolatedWallAlt;
        Label lblColumnTopAlt;
        Label lblColumnCenterAlt;
        Label lblColumnBottomAlt;
        Label lblRowLeftAlt;
        Label lblRowCenterAlt;
        Label lblRowRightAlt;

        */
        private Label lblWaterTiles;

        private Label[] lblWaterTileset;
        private PictureBox[] picWaterTileset;
        private int[,] waterTileNumbers;
        private Button btnWaterAnimSwitch;

        /*
        Label lblShoreSurrounded;
        Label lblShoreInnerTopLeft;
        Label lblShoreTop;
        Label lblShoreInnerTopRight;
        Label lblShoreLeft;
        Label lblWater;
        Label lblShoreRight;
        Label lblShoreInnerBottomLeft;
        Label lblShoreBottom;
        Label lblShoreInnerBottomRight;
        Label lblShoreTopLeft;
        Label lblShoreTopRight;
        Label lblShoreBottomLeft;
        Label lblShoreBottomRight;
        Label lblShoreDiagonalForward;
        Label lblShoreDiagonalBack;
        Label lblShoreInnerTop;
        Label lblShoreVertical;
        Label lblShoreInnerBottom;
        Label lblShoreInnerLeft;
        Label lblShoreHorizontal;
        Label lblShoreInnerRight;

        */

        private Label lblWaterAnimTiles;

        private Label[] lblWaterAnimTileset;
        private PictureBox[] picWaterAnimTileset;
        private int[,] waterAnimTileNumbers;
        private Button btnWaterSwitch;

        /*
        Label lblShoreSurroundedAnim;
        Label lblShoreInnerTopLeftAnim;
        Label lblShoreTopAnim;
        Label lblShoreInnerTopRightAnim;
        Label lblShoreLeftAnim;
        Label lblWaterAnim;
        Label lblShoreRightAnim;
        Label lblShoreInnerBottomLeftAnim;
        Label lblShoreBottomAnim;
        Label lblShoreInnerBottomRightAnim;
        Label lblShoreTopLeftAnim;
        Label lblShoreTopRightAnim;
        Label lblShoreBottomLeftAnim;
        Label lblShoreBottomRightAnim;
        Label lblShoreDiagonalForwardAnim;
        Label lblShoreDiagonalBackAnim;
        Label lblShoreInnerTopAnim;
        Label lblShoreVerticalAnim;
        Label lblShoreInnerBottomAnim;
        Label lblShoreInnerLeftAnim;
        Label lblShoreHorizontalAnim;
        Label lblShoreInnerRightAnim;

        */

        private Label lblAttributes;

        private ComboBox cbGroundType;
        private Label lblGroundData1;
        private NumericUpDown nudGroundData1;
        private Label lblGroundData2;
        private NumericUpDown nudGroundData2;
        private Label lblGroundData3;
        private NumericUpDown nudGroundData3;

        private Label lblGroundString1;
        private TextBox txtGroundString1;
        private Label lblGroundString2;
        private TextBox txtGroundString2;
        private Label lblGroundString3;
        private TextBox txtGroundString3;

        private ComboBox cbHallType;
        private Label lblHallData1;
        private NumericUpDown nudHallData1;
        private Label lblHallData2;
        private NumericUpDown nudHallData2;
        private Label lblHallData3;
        private NumericUpDown nudHallData3;

        private Label lblHallString1;
        private TextBox txtHallString1;
        private Label lblHallString2;
        private TextBox txtHallString2;
        private Label lblHallString3;
        private TextBox txtHallString3;

        private ComboBox cbWaterType;
        private Label lblWaterData1;
        private NumericUpDown nudWaterData1;
        private Label lblWaterData2;
        private NumericUpDown nudWaterData2;
        private Label lblWaterData3;
        private NumericUpDown nudWaterData3;

        private Label lblWaterString1;
        private TextBox txtWaterString1;
        private Label lblWaterString2;
        private TextBox txtWaterString2;
        private Label lblWaterString3;
        private TextBox txtWaterString3;

        private ComboBox cbWallType;
        private Label lblWallData1;
        private NumericUpDown nudWallData1;
        private Label lblWallData2;
        private NumericUpDown nudWallData2;
        private Label lblWallData3;
        private NumericUpDown nudWallData3;

        private Label lblWallString1;
        private TextBox txtWallString1;
        private Label lblWallString2;
        private TextBox txtWallString2;
        private Label lblWallString3;
        private TextBox txtWallString3;
        private Label lblItems;

        private ScrollingListBox lbxItems;
        private List<EditableRDungeonItem> itemList;

        private Label lblItemNum;
        private NumericUpDown nudItemNum;
        private Label lblMinValue;
        private NumericUpDown nudMinValue;
        private Label lblMaxValue;
        private NumericUpDown nudMaxValue;
        private Label lblItemSpawnRate;
        private NumericUpDown nudItemSpawnRate;
        private Label lblStickyRate;
        private NumericUpDown nudStickyRate;
        private Label lblTag;
        private TextBox txtTag;
        private CheckBox chkHidden;
        private CheckBox chkOnGround;
        private CheckBox chkOnWater;
        private CheckBox chkOnWall;
        private Button btnAddItem;
        private Button btnRemoveItem;
        private Button btnLoadItem;
        private Button btnChangeItem;
        private CheckBox chkBulkItem;
        private Label lblNpcs;

        private Label lblNpcSpawnTime;
        private NumericUpDown nudNpcSpawnTime;

        private Label lblNpcMin;
        private NumericUpDown nudNpcMin;
        private Label lblNpcMax;
        private NumericUpDown nudNpcMax;

        private ScrollingListBox lbxNpcs;
        private List<MapNpcSettings> npcList;

        private Label lblNpcNum;
        private NumericUpDown nudNpcNum;
        private Label lblMinLevel;
        private NumericUpDown nudMinLevel;
        private Label lblMaxLevel;
        private NumericUpDown nudMaxLevel;
        private Label lblNpcSpawnRate;
        private NumericUpDown nudNpcSpawnRate;
        private Label lblNpcStartStatus;
        private ComboBox cbNpcStartStatus;
        private Label lblStatusCounter;
        private NumericUpDown nudStatusCounter;
        private Label lblStatusChance;
        private NumericUpDown nudStatusChance;
        private Button btnAddNpc;
        private Button btnRemoveNpc;
        private Button btnLoadNpc;
        private Button btnChangeNpc;
        private CheckBox chkBulkNpc;
        private Label lblTraps;

        private ScrollingListBox lbxTraps;
        private List<EditableRDungeonTrap> trapList;
        private Label[] lblTrapTileset;
        private PictureBox[] picTrapTileset;
        private int[,] trapTileNumbers;

        private ComboBox cbTrapType;
        private Label lblTrapData1;
        private NumericUpDown nudTrapData1;
        private Label lblTrapData2;
        private NumericUpDown nudTrapData2;
        private Label lblTrapData3;
        private NumericUpDown nudTrapData3;
        private Label lblTrapString1;
        private TextBox txtTrapString1;
        private Label lblTrapString2;
        private TextBox txtTrapString2;
        private Label lblTrapString3;
        private TextBox txtTrapString3;

        private Label lblTrapChance;
        private NumericUpDown nudTrapChance;

        private Button btnAddTrap;
        private Button btnRemoveTrap;
        private Button btnLoadTrap;
        private Button btnChangeTrap;
        private Label lblWeather;

        private ScrollingListBox lbxWeather;
        private ComboBox cbWeather;
        private Button btnAddWeather;
        private Button btnRemoveWeather;
        private Label lblGoal;
        private RadioButton optNextFloor;
        private RadioButton optMap;
        private RadioButton optScripted;
        private Label lblData1;
        private Label lblData2;
        private Label lblData3;
        private NumericUpDown nudData1;
        private NumericUpDown nudData2;
        private NumericUpDown nudData3;
        private Label lblChambers;

        private ScrollingListBox lbxChambers;
        private List<EditableRDungeonChamber> chamberList;
        private Label lblChamberNum;
        private NumericUpDown nudChamber;
        private Label lblChamberString1;
        private TextBox txtChamberString1;
        private Label lblChamberString2;
        private TextBox txtChamberString2;
        private Label lblChamberString3;
        private TextBox txtChamberString3;
        private Button btnAddChamber;
        private Button btnRemoveChamber;
        private Label lblMisc;
        private Label lblMusic;
        private ScrollingListBox lbxMusic;
        private Button btnPlayMusic;
        private Label lblDarkness;
        private NumericUpDown nudDarkness;
        private Widges.TilesetViewer tileSelector;

        private Label lblTileSet;
        private NumericUpDown nudTileSet;
        private Button btnTileSetOK;
        private Button btnTileSetCancel;

        public WinRDungeonPanel()
            : base("winRDungeonPanel")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new Size(200, 230);
            this.Location = new Point(210, Windows.WindowSwitcher.GameWindow.ActiveTeam.Y + Windows.WindowSwitcher.GameWindow.ActiveTeam.Height + 0);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = true;
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "Random Dungeon Panel";

            this.LoadpnlRDungeonList();

            this.LoadComplete();
        }

        public void RefreshRDungeonList()
        {
            for (int i = 0; i < 10; i++)
            {
                if ((i + (this.currentTen * 10)) < MaxInfo.MaxRDungeons)
                {
                    ((ListBoxTextItem)this.lbxRDungeonList.Items[i]).Text = ((i + 1) + (10 * this.currentTen)) + ": " + RDungeons.RDungeonHelper.RDungeons[i + (10 * this.currentTen)].Name;
                }
                else
                {
                    ((ListBoxTextItem)this.lbxRDungeonList.Items[i]).Text = "---";
                }
            }
        }

        public void LoadRDungeon(string[] parse)
        {
            // load Rdungeon from packet
            this.rdungeon = new EditableRDungeon(parse[1].ToInt());
            this.rdungeon.DungeonName = parse[2];
            this.rdungeon.Direction = (Enums.Direction)parse[3].ToInt();
            this.rdungeon.MaxFloors = parse[4].ToInt();
            this.rdungeon.Recruitment = parse[5].ToBool();
            this.rdungeon.Exp = parse[6].ToBool();
            this.rdungeon.WindTimer = parse[7].ToInt();
            this.rdungeon.DungeonIndex = parse[8].ToInt();
            int n = 9;
            for (int i = 0; i < this.rdungeon.MaxFloors; i++)
            {
                this.rdungeon.Floors.Add(new EditableRDungeonFloor());
                this.rdungeon.Floors[i].TrapMin = parse[n].ToInt();
                this.rdungeon.Floors[i].TrapMax = parse[n + 1].ToInt();
                this.rdungeon.Floors[i].ItemMin = parse[n + 2].ToInt();
                this.rdungeon.Floors[i].ItemMax = parse[n + 3].ToInt();
                this.rdungeon.Floors[i].Intricacy = parse[n + 4].ToInt();
                this.rdungeon.Floors[i].RoomWidthMin = parse[n + 5].ToInt();
                this.rdungeon.Floors[i].RoomWidthMax = parse[n + 6].ToInt();
                this.rdungeon.Floors[i].RoomLengthMin = parse[n + 7].ToInt();
                this.rdungeon.Floors[i].RoomLengthMax = parse[n + 8].ToInt();
                this.rdungeon.Floors[i].HallTurnMin = parse[n + 9].ToInt();
                this.rdungeon.Floors[i].HallTurnMax = parse[n + 10].ToInt();
                this.rdungeon.Floors[i].HallVarMin = parse[n + 11].ToInt();
                this.rdungeon.Floors[i].HallVarMax = parse[n + 12].ToInt();
                this.rdungeon.Floors[i].WaterFrequency = parse[n + 13].ToInt();
                this.rdungeon.Floors[i].Craters = parse[n + 14].ToInt();
                this.rdungeon.Floors[i].CraterMinLength = parse[n + 15].ToInt();
                this.rdungeon.Floors[i].CraterMaxLength = parse[n + 16].ToInt();
                this.rdungeon.Floors[i].CraterFuzzy = parse[n + 17].ToBool();
                this.rdungeon.Floors[i].MinChambers = parse[n + 18].ToInt();
                this.rdungeon.Floors[i].MaxChambers = parse[n + 19].ToInt();
                this.rdungeon.Floors[i].Darkness = parse[n + 20].ToInt();
                this.rdungeon.Floors[i].GoalType = (Enums.RFloorGoalType)parse[n + 21].ToInt();
                this.rdungeon.Floors[i].GoalMap = parse[n + 22].ToInt();
                this.rdungeon.Floors[i].GoalX = parse[n + 23].ToInt();
                this.rdungeon.Floors[i].GoalY = parse[n + 24].ToInt();
                this.rdungeon.Floors[i].Music = parse[n + 25];

                n += 26;

                this.rdungeon.Floors[i].StairsX = parse[n].ToInt();
                this.rdungeon.Floors[i].StairsSheet = parse[n + 1].ToInt();

                this.rdungeon.Floors[i].MGroundX = parse[n + 2].ToInt();
                this.rdungeon.Floors[i].MGroundSheet = parse[n + 3].ToInt();

                this.rdungeon.Floors[i].MTopLeftX = parse[n + 4].ToInt();
                this.rdungeon.Floors[i].MTopLeftSheet = parse[n + 5].ToInt();
                this.rdungeon.Floors[i].MTopCenterX = parse[n + 6].ToInt();
                this.rdungeon.Floors[i].MTopCenterSheet = parse[n + 7].ToInt();
                this.rdungeon.Floors[i].MTopRightX = parse[n + 8].ToInt();
                this.rdungeon.Floors[i].MTopRightSheet = parse[n + 9].ToInt();

                this.rdungeon.Floors[i].MCenterLeftX = parse[n + 10].ToInt();
                this.rdungeon.Floors[i].MCenterLeftSheet = parse[n + 11].ToInt();
                this.rdungeon.Floors[i].MCenterCenterX = parse[n + 12].ToInt();
                this.rdungeon.Floors[i].MCenterCenterSheet = parse[n + 13].ToInt();
                this.rdungeon.Floors[i].MCenterRightX = parse[n + 14].ToInt();
                this.rdungeon.Floors[i].MCenterRightSheet = parse[n + 15].ToInt();

                this.rdungeon.Floors[i].MBottomLeftX = parse[n + 16].ToInt();
                this.rdungeon.Floors[i].MBottomLeftSheet = parse[n + 17].ToInt();
                this.rdungeon.Floors[i].MBottomCenterX = parse[n + 18].ToInt();
                this.rdungeon.Floors[i].MBottomCenterSheet = parse[n + 19].ToInt();
                this.rdungeon.Floors[i].MBottomRightX = parse[n + 20].ToInt();
                this.rdungeon.Floors[i].MBottomRightSheet = parse[n + 21].ToInt();

                this.rdungeon.Floors[i].MInnerTopLeftX = parse[n + 22].ToInt();
                this.rdungeon.Floors[i].MInnerTopLeftSheet = parse[n + 23].ToInt();
                this.rdungeon.Floors[i].MInnerBottomLeftX = parse[n + 24].ToInt();
                this.rdungeon.Floors[i].MInnerBottomLeftSheet = parse[n + 25].ToInt();
                this.rdungeon.Floors[i].MInnerTopRightX = parse[n + 26].ToInt();
                this.rdungeon.Floors[i].MInnerTopRightSheet = parse[n + 27].ToInt();
                this.rdungeon.Floors[i].MInnerBottomRightX = parse[n + 28].ToInt();
                this.rdungeon.Floors[i].MInnerBottomRightSheet = parse[n + 29].ToInt();

                this.rdungeon.Floors[i].MIsolatedWallX = parse[n + 30].ToInt();
                this.rdungeon.Floors[i].MIsolatedWallSheet = parse[n + 31].ToInt();
                this.rdungeon.Floors[i].MColumnTopX = parse[n + 32].ToInt();
                this.rdungeon.Floors[i].MColumnTopSheet = parse[n + 33].ToInt();
                this.rdungeon.Floors[i].MColumnCenterX = parse[n + 34].ToInt();
                this.rdungeon.Floors[i].MColumnCenterSheet = parse[n + 35].ToInt();
                this.rdungeon.Floors[i].MColumnBottomX = parse[n + 36].ToInt();
                this.rdungeon.Floors[i].MColumnBottomSheet = parse[n + 37].ToInt();

                this.rdungeon.Floors[i].MRowLeftX = parse[n + 38].ToInt();
                this.rdungeon.Floors[i].MRowLeftSheet = parse[n + 39].ToInt();
                this.rdungeon.Floors[i].MRowCenterX = parse[n + 40].ToInt();
                this.rdungeon.Floors[i].MRowCenterSheet = parse[n + 41].ToInt();
                this.rdungeon.Floors[i].MRowRightX = parse[n + 42].ToInt();
                this.rdungeon.Floors[i].MRowRightSheet = parse[n + 43].ToInt();

                this.rdungeon.Floors[i].MGroundAltX = parse[n + 44].ToInt();
                this.rdungeon.Floors[i].MGroundAltSheet = parse[n + 45].ToInt();
                this.rdungeon.Floors[i].MGroundAlt2X = parse[n + 46].ToInt();
                this.rdungeon.Floors[i].MGroundAlt2Sheet = parse[n + 47].ToInt();

                this.rdungeon.Floors[i].MTopLeftAltX = parse[n + 48].ToInt();
                this.rdungeon.Floors[i].MTopLeftAltSheet = parse[n + 49].ToInt();
                this.rdungeon.Floors[i].MTopCenterAltX = parse[n + 50].ToInt();
                this.rdungeon.Floors[i].MTopCenterAltSheet = parse[n + 51].ToInt();
                this.rdungeon.Floors[i].MTopRightAltX = parse[n + 52].ToInt();
                this.rdungeon.Floors[i].MTopRightAltSheet = parse[n + 53].ToInt();

                this.rdungeon.Floors[i].MCenterLeftAltX = parse[n + 54].ToInt();
                this.rdungeon.Floors[i].MCenterLeftAltSheet = parse[n + 55].ToInt();
                this.rdungeon.Floors[i].MCenterCenterAltX = parse[n + 56].ToInt();
                this.rdungeon.Floors[i].MCenterCenterAltSheet = parse[n + 57].ToInt();
                this.rdungeon.Floors[i].MCenterCenterAlt2X = parse[n + 58].ToInt();
                this.rdungeon.Floors[i].MCenterCenterAlt2Sheet = parse[n + 59].ToInt();
                this.rdungeon.Floors[i].MCenterRightAltX = parse[n + 60].ToInt();
                this.rdungeon.Floors[i].MCenterRightAltSheet = parse[n + 61].ToInt();

                this.rdungeon.Floors[i].MBottomLeftAltX = parse[n + 62].ToInt();
                this.rdungeon.Floors[i].MBottomLeftAltSheet = parse[n + 63].ToInt();
                this.rdungeon.Floors[i].MBottomCenterAltX = parse[n + 64].ToInt();
                this.rdungeon.Floors[i].MBottomCenterAltSheet = parse[n + 65].ToInt();
                this.rdungeon.Floors[i].MBottomRightAltX = parse[n + 66].ToInt();
                this.rdungeon.Floors[i].MBottomRightAltSheet = parse[n + 67].ToInt();

                this.rdungeon.Floors[i].MInnerTopLeftAltX = parse[n + 68].ToInt();
                this.rdungeon.Floors[i].MInnerTopLeftAltSheet = parse[n + 69].ToInt();
                this.rdungeon.Floors[i].MInnerBottomLeftAltX = parse[n + 70].ToInt();
                this.rdungeon.Floors[i].MInnerBottomLeftAltSheet = parse[n + 71].ToInt();
                this.rdungeon.Floors[i].MInnerTopRightAltX = parse[n + 72].ToInt();
                this.rdungeon.Floors[i].MInnerTopRightAltSheet = parse[n + 73].ToInt();
                this.rdungeon.Floors[i].MInnerBottomRightAltX = parse[n + 74].ToInt();
                this.rdungeon.Floors[i].MInnerBottomRightAltSheet = parse[n + 75].ToInt();

                this.rdungeon.Floors[i].MIsolatedWallAltX = parse[n + 76].ToInt();
                this.rdungeon.Floors[i].MIsolatedWallAltSheet = parse[n + 77].ToInt();
                this.rdungeon.Floors[i].MColumnTopAltX = parse[n + 78].ToInt();
                this.rdungeon.Floors[i].MColumnTopAltSheet = parse[n + 79].ToInt();
                this.rdungeon.Floors[i].MColumnCenterAltX = parse[n + 80].ToInt();
                this.rdungeon.Floors[i].MColumnCenterAltSheet = parse[n + 81].ToInt();
                this.rdungeon.Floors[i].MColumnBottomAltX = parse[n + 82].ToInt();
                this.rdungeon.Floors[i].MColumnBottomAltSheet = parse[n + 83].ToInt();

                this.rdungeon.Floors[i].MRowLeftAltX = parse[n + 84].ToInt();
                this.rdungeon.Floors[i].MRowLeftAltSheet = parse[n + 85].ToInt();
                this.rdungeon.Floors[i].MRowCenterAltX = parse[n + 86].ToInt();
                this.rdungeon.Floors[i].MRowCenterAltSheet = parse[n + 87].ToInt();
                this.rdungeon.Floors[i].MRowRightAltX = parse[n + 88].ToInt();
                this.rdungeon.Floors[i].MRowRightAltSheet = parse[n + 89].ToInt();

                n += 90;

                this.rdungeon.Floors[i].MWaterX = parse[n].ToInt();
                this.rdungeon.Floors[i].MWaterSheet = parse[n + 1].ToInt();
                this.rdungeon.Floors[i].MWaterAnimX = parse[n + 2].ToInt();
                this.rdungeon.Floors[i].MWaterAnimSheet = parse[n + 3].ToInt();

                this.rdungeon.Floors[i].MShoreTopLeftX = parse[n + 4].ToInt();
                this.rdungeon.Floors[i].MShoreTopLeftSheet = parse[n + 5].ToInt();
                this.rdungeon.Floors[i].MShoreTopRightX = parse[n + 6].ToInt();
                this.rdungeon.Floors[i].MShoreTopRightSheet = parse[n + 7].ToInt();
                this.rdungeon.Floors[i].MShoreBottomRightX = parse[n + 8].ToInt();
                this.rdungeon.Floors[i].MShoreBottomRightSheet = parse[n + 9].ToInt();
                this.rdungeon.Floors[i].MShoreBottomLeftX = parse[n + 10].ToInt();
                this.rdungeon.Floors[i].MShoreBottomLeftSheet = parse[n + 11].ToInt();

                this.rdungeon.Floors[i].MShoreDiagonalForwardX = parse[n + 12].ToInt();
                this.rdungeon.Floors[i].MShoreDiagonalForwardSheet = parse[n + 13].ToInt();
                this.rdungeon.Floors[i].MShoreDiagonalBackX = parse[n + 14].ToInt();
                this.rdungeon.Floors[i].MShoreDiagonalBackSheet = parse[n + 15].ToInt();

                this.rdungeon.Floors[i].MShoreTopX = parse[n + 16].ToInt();
                this.rdungeon.Floors[i].MShoreTopSheet = parse[n + 17].ToInt();
                this.rdungeon.Floors[i].MShoreRightX = parse[n + 18].ToInt();
                this.rdungeon.Floors[i].MShoreRightSheet = parse[n + 19].ToInt();
                this.rdungeon.Floors[i].MShoreBottomX = parse[n + 20].ToInt();
                this.rdungeon.Floors[i].MShoreBottomSheet = parse[n + 21].ToInt();
                this.rdungeon.Floors[i].MShoreLeftX = parse[n + 22].ToInt();
                this.rdungeon.Floors[i].MShoreLeftSheet = parse[n + 23].ToInt();

                this.rdungeon.Floors[i].MShoreVerticalX = parse[n + 24].ToInt();
                this.rdungeon.Floors[i].MShoreVerticalSheet = parse[n + 25].ToInt();
                this.rdungeon.Floors[i].MShoreHorizontalX = parse[n + 26].ToInt();
                this.rdungeon.Floors[i].MShoreHorizontalSheet = parse[n + 27].ToInt();

                this.rdungeon.Floors[i].MShoreInnerTopLeftX = parse[n + 28].ToInt();
                this.rdungeon.Floors[i].MShoreInnerTopLeftSheet = parse[n + 29].ToInt();
                this.rdungeon.Floors[i].MShoreInnerTopRightX = parse[n + 30].ToInt();
                this.rdungeon.Floors[i].MShoreInnerTopRightSheet = parse[n + 31].ToInt();
                this.rdungeon.Floors[i].MShoreInnerBottomRightX = parse[n + 32].ToInt();
                this.rdungeon.Floors[i].MShoreInnerBottomRightSheet = parse[n + 33].ToInt();
                this.rdungeon.Floors[i].MShoreInnerBottomLeftX = parse[n + 34].ToInt();
                this.rdungeon.Floors[i].MShoreInnerBottomLeftSheet = parse[n + 35].ToInt();

                this.rdungeon.Floors[i].MShoreInnerTopX = parse[n + 36].ToInt();
                this.rdungeon.Floors[i].MShoreInnerTopSheet = parse[n + 37].ToInt();
                this.rdungeon.Floors[i].MShoreInnerRightX = parse[n + 38].ToInt();
                this.rdungeon.Floors[i].MShoreInnerRightSheet = parse[n + 39].ToInt();
                this.rdungeon.Floors[i].MShoreInnerBottomX = parse[n + 40].ToInt();
                this.rdungeon.Floors[i].MShoreInnerBottomSheet = parse[n + 41].ToInt();
                this.rdungeon.Floors[i].MShoreInnerLeftX = parse[n + 42].ToInt();
                this.rdungeon.Floors[i].MShoreInnerLeftSheet = parse[n + 43].ToInt();
                this.rdungeon.Floors[i].MShoreSurroundedX = parse[n + 44].ToInt();
                this.rdungeon.Floors[i].MShoreSurroundedSheet = parse[n + 45].ToInt();

                this.rdungeon.Floors[i].MShoreTopLeftAnimX = parse[n + 46].ToInt();
                this.rdungeon.Floors[i].MShoreTopLeftAnimSheet = parse[n + 47].ToInt();
                this.rdungeon.Floors[i].MShoreTopRightAnimX = parse[n + 48].ToInt();
                this.rdungeon.Floors[i].MShoreTopRightAnimSheet = parse[n + 49].ToInt();
                this.rdungeon.Floors[i].MShoreBottomRightAnimX = parse[n + 50].ToInt();
                this.rdungeon.Floors[i].MShoreBottomRightAnimSheet = parse[n + 51].ToInt();
                this.rdungeon.Floors[i].MShoreBottomLeftAnimX = parse[n + 52].ToInt();
                this.rdungeon.Floors[i].MShoreBottomLeftAnimSheet = parse[n + 53].ToInt();

                this.rdungeon.Floors[i].MShoreDiagonalForwardAnimX = parse[n + 54].ToInt();
                this.rdungeon.Floors[i].MShoreDiagonalForwardAnimSheet = parse[n + 55].ToInt();
                this.rdungeon.Floors[i].MShoreDiagonalBackAnimX = parse[n + 56].ToInt();
                this.rdungeon.Floors[i].MShoreDiagonalBackAnimSheet = parse[n + 57].ToInt();

                this.rdungeon.Floors[i].MShoreTopAnimX = parse[n + 58].ToInt();
                this.rdungeon.Floors[i].MShoreTopAnimSheet = parse[n + 59].ToInt();
                this.rdungeon.Floors[i].MShoreRightAnimX = parse[n + 60].ToInt();
                this.rdungeon.Floors[i].MShoreRightAnimSheet = parse[n + 61].ToInt();
                this.rdungeon.Floors[i].MShoreBottomAnimX = parse[n + 62].ToInt();
                this.rdungeon.Floors[i].MShoreBottomAnimSheet = parse[n + 63].ToInt();
                this.rdungeon.Floors[i].MShoreLeftAnimX = parse[n + 64].ToInt();
                this.rdungeon.Floors[i].MShoreLeftAnimSheet = parse[n + 65].ToInt();

                this.rdungeon.Floors[i].MShoreVerticalAnimX = parse[n + 66].ToInt();
                this.rdungeon.Floors[i].MShoreVerticalAnimSheet = parse[n + 67].ToInt();
                this.rdungeon.Floors[i].MShoreHorizontalAnimX = parse[n + 68].ToInt();
                this.rdungeon.Floors[i].MShoreHorizontalAnimSheet = parse[n + 69].ToInt();

                this.rdungeon.Floors[i].MShoreInnerTopLeftAnimX = parse[n + 70].ToInt();
                this.rdungeon.Floors[i].MShoreInnerTopLeftAnimSheet = parse[n + 71].ToInt();
                this.rdungeon.Floors[i].MShoreInnerTopRightAnimX = parse[n + 72].ToInt();
                this.rdungeon.Floors[i].MShoreInnerTopRightAnimSheet = parse[n + 73].ToInt();
                this.rdungeon.Floors[i].MShoreInnerBottomRightAnimX = parse[n + 74].ToInt();
                this.rdungeon.Floors[i].MShoreInnerBottomRightAnimSheet = parse[n + 75].ToInt();
                this.rdungeon.Floors[i].MShoreInnerBottomLeftAnimX = parse[n + 76].ToInt();
                this.rdungeon.Floors[i].MShoreInnerBottomLeftAnimSheet = parse[n + 77].ToInt();

                this.rdungeon.Floors[i].MShoreInnerTopAnimX = parse[n + 78].ToInt();
                this.rdungeon.Floors[i].MShoreInnerTopAnimSheet = parse[n + 79].ToInt();
                this.rdungeon.Floors[i].MShoreInnerRightAnimX = parse[n + 80].ToInt();
                this.rdungeon.Floors[i].MShoreInnerRightAnimSheet = parse[n + 81].ToInt();
                this.rdungeon.Floors[i].MShoreInnerBottomAnimX = parse[n + 82].ToInt();
                this.rdungeon.Floors[i].MShoreInnerBottomAnimSheet = parse[n + 83].ToInt();
                this.rdungeon.Floors[i].MShoreInnerLeftAnimX = parse[n + 84].ToInt();
                this.rdungeon.Floors[i].MShoreInnerLeftAnimSheet = parse[n + 85].ToInt();

                this.rdungeon.Floors[i].MShoreSurroundedAnimX = parse[n + 86].ToInt();
                this.rdungeon.Floors[i].MShoreSurroundedAnimSheet = parse[n + 87].ToInt();

                n += 88;

                this.rdungeon.Floors[i].GroundTile.Type = (Enums.TileType)parse[n].ToInt();
                this.rdungeon.Floors[i].GroundTile.Data1 = parse[n + 1].ToInt();
                this.rdungeon.Floors[i].GroundTile.Data2 = parse[n + 2].ToInt();
                this.rdungeon.Floors[i].GroundTile.Data3 = parse[n + 3].ToInt();
                this.rdungeon.Floors[i].GroundTile.String1 = parse[n + 4];
                this.rdungeon.Floors[i].GroundTile.String2 = parse[n + 5];
                this.rdungeon.Floors[i].GroundTile.String3 = parse[n + 6];

                this.rdungeon.Floors[i].HallTile.Type = (Enums.TileType)parse[n + 7].ToInt();
                this.rdungeon.Floors[i].HallTile.Data1 = parse[n + 8].ToInt();
                this.rdungeon.Floors[i].HallTile.Data2 = parse[n + 9].ToInt();
                this.rdungeon.Floors[i].HallTile.Data3 = parse[n + 10].ToInt();
                this.rdungeon.Floors[i].HallTile.String1 = parse[n + 11];
                this.rdungeon.Floors[i].HallTile.String2 = parse[n + 12];
                this.rdungeon.Floors[i].HallTile.String3 = parse[n + 13];

                this.rdungeon.Floors[i].WaterTile.Type = (Enums.TileType)parse[n + 14].ToInt();
                this.rdungeon.Floors[i].WaterTile.Data1 = parse[n + 15].ToInt();
                this.rdungeon.Floors[i].WaterTile.Data2 = parse[n + 16].ToInt();
                this.rdungeon.Floors[i].WaterTile.Data3 = parse[n + 17].ToInt();
                this.rdungeon.Floors[i].WaterTile.String1 = parse[n + 18];
                this.rdungeon.Floors[i].WaterTile.String2 = parse[n + 19];
                this.rdungeon.Floors[i].WaterTile.String3 = parse[n + 20];

                this.rdungeon.Floors[i].WallTile.Type = (Enums.TileType)parse[n + 21].ToInt();
                this.rdungeon.Floors[i].WallTile.Data1 = parse[n + 22].ToInt();
                this.rdungeon.Floors[i].WallTile.Data2 = parse[n + 23].ToInt();
                this.rdungeon.Floors[i].WallTile.Data3 = parse[n + 24].ToInt();
                this.rdungeon.Floors[i].WallTile.String1 = parse[n + 25];
                this.rdungeon.Floors[i].WallTile.String2 = parse[n + 26];
                this.rdungeon.Floors[i].WallTile.String3 = parse[n + 27];

                this.rdungeon.Floors[i].NpcSpawnTime = parse[n + 28].ToInt();
                this.rdungeon.Floors[i].NpcMin = parse[n + 29].ToInt();
                this.rdungeon.Floors[i].NpcMax = parse[n + 30].ToInt();

                n += 31;

                for (int item = 0; item < parse[n].ToInt(); item++)
                {
                    EditableRDungeonItem newItem = new EditableRDungeonItem();
                    newItem.ItemNum = parse[n + (item * 10) + 1].ToInt();
                    newItem.MinAmount = parse[n + (item * 10) + 2].ToInt();
                    newItem.MaxAmount = parse[n + (item * 10) + 3].ToInt();
                    newItem.AppearanceRate = parse[n + (item * 10) + 4].ToInt();
                    newItem.StickyRate = parse[n + (item * 10) + 5].ToInt();
                    newItem.Tag = parse[n + (item * 10) + 6];
                    newItem.Hidden = parse[n + (item * 10) + 7].ToBool();
                    newItem.OnGround = parse[n + (item * 10) + 8].ToBool();
                    newItem.OnWater = parse[n + (item * 10) + 9].ToBool();
                    newItem.OnWall = parse[n + (item * 10) + 10].ToBool();

                    this.rdungeon.Floors[i].Items.Add(newItem);
                }

                n += (this.rdungeon.Floors[i].Items.Count * 10) + 1;

                for (int npc = 0; npc < parse[n].ToInt(); npc++)
                {
                    MapNpcSettings newNpc = new MapNpcSettings();
                    newNpc.NpcNum = parse[n + (npc * 7) + 1].ToInt();
                    newNpc.MinLevel = parse[n + (npc * 7) + 2].ToInt();
                    newNpc.MaxLevel = parse[n + (npc * 7) + 3].ToInt();
                    newNpc.AppearanceRate = parse[n + (npc * 7) + 4].ToInt();
                    newNpc.StartStatus = (Enums.StatusAilment)parse[n + (npc * 7) + 5].ToInt();
                    newNpc.StartStatusCounter = parse[n + (npc * 7) + 6].ToInt();
                    newNpc.StartStatusChance = parse[n + (npc * 7) + 7].ToInt();

                    this.rdungeon.Floors[i].Npcs.Add(newNpc);
                }

                n += (this.rdungeon.Floors[i].Npcs.Count * 7) + 1;

                for (int traps = 0; traps < parse[n].ToInt(); traps++)
                {
                    EditableRDungeonTrap newTile = new EditableRDungeonTrap();
                    newTile.SpecialTile.Type = (Enums.TileType)parse[n + (traps * 29) + 1].ToInt();
                    newTile.SpecialTile.Data1 = parse[n + (traps * 29) + 2].ToInt();
                    newTile.SpecialTile.Data2 = parse[n + (traps * 29) + 3].ToInt();
                    newTile.SpecialTile.Data3 = parse[n + (traps * 29) + 4].ToInt();
                    newTile.SpecialTile.String1 = parse[n + (traps * 29) + 5];
                    newTile.SpecialTile.String2 = parse[n + (traps * 29) + 6];
                    newTile.SpecialTile.String3 = parse[n + (traps * 29) + 7];
                    newTile.SpecialTile.Ground = parse[n + (traps * 29) + 8].ToInt();
                    newTile.SpecialTile.GroundSet = parse[n + (traps * 29) + 9].ToInt();
                    newTile.SpecialTile.GroundAnim = parse[n + (traps * 29) + 10].ToInt();
                    newTile.SpecialTile.GroundAnimSet = parse[n + (traps * 29) + 11].ToInt();
                    newTile.SpecialTile.Mask = parse[n + (traps * 29) + 12].ToInt();
                    newTile.SpecialTile.MaskSet = parse[n + (traps * 29) + 13].ToInt();
                    newTile.SpecialTile.Anim = parse[n + (traps * 29) + 14].ToInt();
                    newTile.SpecialTile.AnimSet = parse[n + (traps * 29) + 15].ToInt();
                    newTile.SpecialTile.Mask2 = parse[n + (traps * 29) + 16].ToInt();
                    newTile.SpecialTile.Mask2Set = parse[n + (traps * 29) + 17].ToInt();
                    newTile.SpecialTile.M2Anim = parse[n + (traps * 29) + 18].ToInt();
                    newTile.SpecialTile.M2AnimSet = parse[n + (traps * 29) + 19].ToInt();
                    newTile.SpecialTile.Fringe = parse[n + (traps * 29) + 20].ToInt();
                    newTile.SpecialTile.FringeSet = parse[n + (traps * 29) + 21].ToInt();
                    newTile.SpecialTile.FAnim = parse[n + (traps * 29) + 22].ToInt();
                    newTile.SpecialTile.FAnimSet = parse[n + (traps * 29) + 23].ToInt();
                    newTile.SpecialTile.Fringe2 = parse[n + (traps * 29) + 24].ToInt();
                    newTile.SpecialTile.Fringe2Set = parse[n + (traps * 29) + 25].ToInt();
                    newTile.SpecialTile.F2Anim = parse[n + (traps * 29) + 26].ToInt();
                    newTile.SpecialTile.F2AnimSet = parse[n + (traps * 29) + 27].ToInt();
                    newTile.SpecialTile.RDungeonMapValue = parse[n + (traps * 29) + 28].ToInt();
                    newTile.AppearanceRate = parse[n + (traps * 29) + 29].ToInt();

                    this.rdungeon.Floors[i].SpecialTiles.Add(newTile);
                }

                n += (this.rdungeon.Floors[i].SpecialTiles.Count * 29) + 1;

                for (int weather = 0; weather < parse[n].ToInt(); weather++)
                {
                    this.rdungeon.Floors[i].Weather.Add((Enums.Weather)parse[n + 1 + weather].ToInt());
                }

                n += this.rdungeon.Floors[i].Weather.Count + 1;

                for (int chamber = 0; chamber < parse[n].ToInt(); chamber++)
                {
                    EditableRDungeonChamber newChamber = new EditableRDungeonChamber();
                    newChamber.ChamberNum = parse[n + (chamber * 4) + 1].ToInt();
                    newChamber.String1 = parse[n + (chamber * 4) + 2];
                    newChamber.String2 = parse[n + (chamber * 4) + 3];
                    newChamber.String3 = parse[n + (chamber * 4) + 4];
                    this.rdungeon.Floors[i].Chambers.Add(newChamber);
                }

                n += (this.rdungeon.Floors[i].Chambers.Count * 4) + 1;
            }

            this.pnlRDungeonList.Visible = false;
            this.btnEdit.Text = "Edit";

            this.txtDungeonName.Text = this.rdungeon.DungeonName;
            if (this.rdungeon.Direction == Enums.Direction.Up)
            {
                this.optUp.Checked = true;
            }
            else
            {
                this.optDown.Checked = true;
            }

            this.nudMaxFloors.Value = this.rdungeon.MaxFloors;
            this.chkRecruiting.Checked = this.rdungeon.Recruitment;
            this.chkEXPGain.Checked = this.rdungeon.Exp;
            this.nudWindTimer.Value = this.rdungeon.WindTimer;

            this.pnlRDungeonGeneral.Visible = true;
            this.Size = new Size(this.pnlRDungeonGeneral.Width, this.pnlRDungeonGeneral.Height);

            // btnGeneral.Visible = true;
            // btnFloors.Visible = true;
            // btnTerrain.Visible = true;
            // btnGeneral.Size = new Size((pnlRDungeonGeneral.Width - 21) / 3, 32);
            // btnFloors.Location = new Point((pnlRDungeonGeneral.Width - 21) / 3 + 10, 0);
            // btnFloors.Size = new Size((pnlRDungeonGeneral.Width - 21) / 3, 32);
            // btnFloors.Visible = true;
            // btnTerrain.Location = new Point((pnlRDungeonGeneral.Width - 21) / 3 * 2 + 10, 0);
            // btnTerrain.Size = new Size((pnlRDungeonGeneral.Width - 21) / 3, 32);
            // btnGeneral.Selected = true;

            // this.TitleBar.Text = "General Random Dungeon Options";
        }

        private void LoadpnlRDungeonList()
        {
            if (!this.listLoaded)
            {
                this.pnlRDungeonList = new Panel("pnlRDungeonList");
                this.pnlRDungeonList.Size = this.Size;
                this.pnlRDungeonList.Location = new Point(0, 0);
                this.pnlRDungeonList.BackColor = Color.White;
                this.pnlRDungeonList.Visible = true;
                this.lbxRDungeonList = new ScrollingListBox("lbxRDungeonList");
                this.lbxRDungeonList.Location = new Point(10, 10);
                this.lbxRDungeonList.Size = new Size(180, 140);
                for (int i = 0; i < 10; i++)
                {
                    this.lbxRDungeonList.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (i + 1) + ": " + RDungeons.RDungeonHelper.RDungeons[i].Name));
                }

                this.btnBack = new Button("btnBack");
                this.btnBack.Location = new Point(10, 160);
                this.btnBack.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnBack.Size = new Size(64, 16);
                this.btnBack.Visible = true;
                this.btnBack.Text = "<--";
                this.btnBack.Click += new EventHandler<MouseButtonEventArgs>(this.BtnBack_Click);

                this.btnForward = new Button("btnForward");
                this.btnForward.Location = new Point(126, 160);
                this.btnForward.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnForward.Size = new Size(64, 16);
                this.btnForward.Visible = true;
                this.btnForward.Text = "-->";
                this.btnForward.Click += new EventHandler<MouseButtonEventArgs>(this.BtnForward_Click);

                this.btnEdit = new Button("btnEdit");
                this.btnEdit.Location = new Point(10, 190);
                this.btnEdit.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnEdit.Size = new Size(48, 16);
                this.btnEdit.Visible = true;
                this.btnEdit.Text = "Edit";
                this.btnEdit.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEdit_Click);

                this.btnCancel = new Button("btnCancel");
                this.btnCancel.Location = new Point(142, 190);
                this.btnCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnCancel.Size = new Size(48, 16);
                this.btnCancel.Visible = true;
                this.btnCancel.Text = "Cancel";
                this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

                this.btnAddNew = new Button("btnAddNew");
                this.btnAddNew.Location = new Point(76, 190);
                this.btnAddNew.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnAddNew.Size = new Size(48, 16);
                this.btnAddNew.Visible = true;
                this.btnAddNew.Text = "New";
                this.btnAddNew.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddNew_Click);
                this.pnlRDungeonList.AddWidget(this.lbxRDungeonList);
                this.pnlRDungeonList.AddWidget(this.btnBack);
                this.pnlRDungeonList.AddWidget(this.btnForward);
                this.pnlRDungeonList.AddWidget(this.btnAddNew);
                this.pnlRDungeonList.AddWidget(this.btnEdit);
                this.pnlRDungeonList.AddWidget(this.btnCancel);
                this.AddWidget(this.pnlRDungeonList);
                this.listLoaded = true;
            }
        }

        private void LoadpnlRDungeonGeneral()
        {
            if (!this.generalLoaded)
            {
                this.pnlRDungeonGeneral = new Panel("pnlRDungeonGeneral");
                this.pnlRDungeonGeneral.Size = new Size(250, 220);
                this.pnlRDungeonGeneral.Location = new Point(0, 0);
                this.pnlRDungeonGeneral.BackColor = Color.White;
                this.pnlRDungeonGeneral.Visible = false;
                this.lblGeneral = new Label("lblGeneral");
                this.lblGeneral.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblGeneral.Text = "General Settings";
                this.lblGeneral.AutoSize = true;
                this.lblGeneral.Location = new Point(10, 4);

                this.btnFloors = new Button("btnFloors");
                this.btnFloors.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.btnFloors.Size = new Size(64, 16);
                this.btnFloors.Text = "Floors ->";
                this.btnFloors.Location = new Point(158, 4);
                this.btnFloors.Click += new EventHandler<MouseButtonEventArgs>(this.BtnFloors_Click);

                this.lblDungeonName = new Label("lblDungeonName");
                this.lblDungeonName.AutoSize = true;
                this.lblDungeonName.Location = new Point(10, 34);
                this.lblDungeonName.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.lblDungeonName.Text = "Dungeon Name";

                this.txtDungeonName = new TextBox("txtDungeonName");
                this.txtDungeonName.Size = new Size(210, 18);
                this.txtDungeonName.Location = new Point(10, 48);

                this.lblDirection = new Label("lblDirection");
                this.lblDirection.AutoSize = true;
                this.lblDirection.Location = new Point(10, 72);
                this.lblDirection.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.lblDirection.Text = "Direction";

                this.optUp = new RadioButton("optUp");
                this.optUp.BackColor = Color.Transparent;
                this.optUp.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.optUp.Location = new Point(10, 84);
                this.optUp.Size = new Size(95, 17);
                this.optUp.Text = "Up";

                this.optDown = new RadioButton("optDown");
                this.optDown.BackColor = Color.Transparent;
                this.optDown.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.optDown.Location = new Point(63, 84);
                this.optDown.Size = new Size(95, 17);
                this.optDown.Text = "Down";

                this.lblMaxFloors = new Label("lblMaxFloors");
                this.lblMaxFloors.AutoSize = true;
                this.lblMaxFloors.Location = new Point(128, 72);
                this.lblMaxFloors.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.lblMaxFloors.Text = "Max Floors";

                this.nudMaxFloors = new NumericUpDown("nudMaxFloors");
                this.nudMaxFloors.Minimum = 1;
                this.nudMaxFloors.Maximum = int.MaxValue;
                this.nudMaxFloors.Size = new Size(80, 20);
                this.nudMaxFloors.Location = new Point(134, 84);
                this.nudMaxFloors.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.chkRecruiting = new CheckBox("chkRecruiting");
                this.chkRecruiting.Size = new Size(100, 17);
                this.chkRecruiting.Location = new Point(10, 114);
                this.chkRecruiting.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.chkRecruiting.Text = "Recruiting";

                this.chkEXPGain = new CheckBox("chkEXPGain");
                this.chkEXPGain.Size = new Size(100, 17);
                this.chkEXPGain.Location = new Point(10, 134);
                this.chkEXPGain.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.chkEXPGain.Text = "EXP Gained";

                this.lblWindTimer = new Label("lblWindTimer");
                this.lblWindTimer.AutoSize = true;
                this.lblWindTimer.Location = new Point(128, 118);
                this.lblWindTimer.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.lblWindTimer.Text = "Time Limit";

                this.nudWindTimer = new NumericUpDown("nudWindTimer");
                this.nudWindTimer.Minimum = -1;
                this.nudWindTimer.Maximum = int.MaxValue;
                this.nudWindTimer.Size = new Size(80, 20);
                this.nudWindTimer.Location = new Point(134, 130);
                this.nudWindTimer.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.btnEditorCancel = new Button("btnEditorCancel");
                this.btnEditorCancel.Location = new Point(120, 170);
                this.btnEditorCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnEditorCancel.Size = new Size(64, 16);
                this.btnEditorCancel.Visible = true;
                this.btnEditorCancel.Text = "Cancel";
                this.btnEditorCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnEditorCancel_Click);

                this.btnSave = new Button("btnSave");
                this.btnSave.Location = new Point(20, 170);
                this.btnSave.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnSave.Size = new Size(64, 16);
                this.btnSave.Visible = true;
                this.btnSave.Text = "OK";
                this.btnSave.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSave_Click);
                this.pnlRDungeonGeneral.AddWidget(this.lblGeneral);
                this.pnlRDungeonGeneral.AddWidget(this.btnFloors);
                this.pnlRDungeonGeneral.AddWidget(this.btnEditorCancel);
                this.pnlRDungeonGeneral.AddWidget(this.btnSave);
                this.pnlRDungeonGeneral.AddWidget(this.lblDungeonName);
                this.pnlRDungeonGeneral.AddWidget(this.txtDungeonName);
                this.pnlRDungeonGeneral.AddWidget(this.lblDirection);
                this.pnlRDungeonGeneral.AddWidget(this.optUp);
                this.pnlRDungeonGeneral.AddWidget(this.optDown);
                this.pnlRDungeonGeneral.AddWidget(this.lblMaxFloors);
                this.pnlRDungeonGeneral.AddWidget(this.nudMaxFloors);
                this.pnlRDungeonGeneral.AddWidget(this.chkRecruiting);
                this.pnlRDungeonGeneral.AddWidget(this.chkEXPGain);
                this.pnlRDungeonGeneral.AddWidget(this.lblWindTimer);
                this.pnlRDungeonGeneral.AddWidget(this.nudWindTimer);
                this.AddWidget(this.pnlRDungeonGeneral);
                this.generalLoaded = true;
            }
        }

        private void LoadpnlRDungeonFloors()
        {
            if (!this.floorsLoaded)
            {
                this.pnlRDungeonFloors = new Panel("pnlRDungeonFloors");
                this.pnlRDungeonFloors.Size = new Size(600, 400);
                this.pnlRDungeonFloors.Location = new Point(0, 0);
                this.pnlRDungeonFloors.BackColor = Color.White;
                this.pnlRDungeonFloors.Visible = false;
                this.lblFloors = new Label("lblFloors");
                this.lblFloors.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblFloors.Text = "Floors Settings";
                this.lblFloors.AutoSize = true;
                this.lblFloors.Location = new Point(100, 4);

                this.btnGeneral = new Button("btnGeneral");
                this.btnGeneral.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnGeneral.Size = new Size(64, 16);
                this.btnGeneral.Location = new Point(10, 0);
                this.btnGeneral.Text = "<- General";
                this.btnGeneral.Click += new EventHandler<MouseButtonEventArgs>(this.BtnGeneral_Click);

                this.lblFromFloorNumber = new Label("lblFromFloorNumber");
                this.lblFromFloorNumber.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblFromFloorNumber.Text = "From Floor #";
                this.lblFromFloorNumber.AutoSize = true;
                this.lblFromFloorNumber.Location = new Point(10, 24);

                this.lblToFloorNumber = new Label("lblToFloorNumber");
                this.lblToFloorNumber.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblToFloorNumber.Text = "To Floor #";
                this.lblToFloorNumber.AutoSize = true;
                this.lblToFloorNumber.Location = new Point(160, 24);

                this.nudFirstFloor = new NumericUpDown("nudFirstFloor");
                this.nudFirstFloor.Minimum = 1;
                this.nudFirstFloor.Maximum = int.MaxValue;
                this.nudFirstFloor.Size = new Size(80, 20);
                this.nudFirstFloor.Location = new Point(80, 24);
                this.nudFirstFloor.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.nudFirstFloor.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudFirstFloor_ValueChanged);

                this.nudLastFloor = new NumericUpDown("nudLastFloor");
                this.nudLastFloor.Minimum = 1;
                this.nudLastFloor.Maximum = int.MaxValue;
                this.nudLastFloor.Size = new Size(80, 20);
                this.nudLastFloor.Location = new Point(220, 24);
                this.nudLastFloor.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.nudLastFloor.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudLastFloor_ValueChanged);

                this.btnSettingsMenu = new Button("btnSettingsMenu");
                this.btnSettingsMenu.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnSettingsMenu.Size = new Size(80, 24);
                this.btnSettingsMenu.Location = new Point(10, 52);
                this.btnSettingsMenu.Text = "Settings Menu";
                this.btnSettingsMenu.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSettingsMenu_Click);

                this.btnSaveFloor = new Button("btnSaveFloor");
                this.btnSaveFloor.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnSaveFloor.Size = new Size(150, 24);
                this.btnSaveFloor.Location = new Point(100, 52);
                this.btnSaveFloor.Text = "Save All Settings to Floor(s)";
                this.btnSaveFloor.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSaveFloor_Click);

                this.btnLoadFloor = new Button("btnLoadFloor");
                this.btnLoadFloor.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnLoadFloor.Size = new Size(150, 24);
                this.btnLoadFloor.Location = new Point(260, 52);
                this.btnLoadFloor.Text = "Load All Settings from Floor";
                this.btnLoadFloor.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadFloor_Click);

                this.lblSaveLoadMessage = new Label("lblSaveLoadMessage");
                this.lblSaveLoadMessage.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblSaveLoadMessage.Text = "SaveLoadMessage Here";
                this.lblSaveLoadMessage.AutoSize = true;
                this.lblSaveLoadMessage.Location = new Point(420, 60);
                this.pnlRDungeonFloors.AddWidget(this.lblFloors);
                this.pnlRDungeonFloors.AddWidget(this.btnGeneral);
                this.pnlRDungeonFloors.AddWidget(this.lblFromFloorNumber);
                this.pnlRDungeonFloors.AddWidget(this.lblToFloorNumber);
                this.pnlRDungeonFloors.AddWidget(this.nudFirstFloor);
                this.pnlRDungeonFloors.AddWidget(this.nudLastFloor);
                this.pnlRDungeonFloors.AddWidget(this.btnSettingsMenu);
                this.pnlRDungeonFloors.AddWidget(this.btnSaveFloor);
                this.pnlRDungeonFloors.AddWidget(this.btnLoadFloor);
                this.pnlRDungeonFloors.AddWidget(this.lblSaveLoadMessage);
                this.AddWidget(this.pnlRDungeonFloors);
                this.floorsLoaded = true;
            }
        }

        private void LoadpnlRDungeonFloorSettingSelection()
        {
            if (!this.floorSettingSelectionLoaded)
            {
                this.pnlRDungeonFloorSettingSelection = new Panel("pnlRDungeonFloorSettingSelection");
                this.pnlRDungeonFloorSettingSelection.Size = new Size(600, 320);
                this.pnlRDungeonFloorSettingSelection.Location = new Point(0, 80);
                this.pnlRDungeonFloorSettingSelection.BackColor = Color.LightGray;
                this.pnlRDungeonFloorSettingSelection.Visible = false;
                this.btnStructure = new Button("btnStructure");
                this.btnStructure.Location = new Point(10, 10);
                this.btnStructure.Font = Graphic.FontManager.LoadFont("tahoma", 18);
                this.btnStructure.Size = new Size(100, 32);
                this.btnStructure.Text = "Structure";
                this.btnStructure.Click += new EventHandler<MouseButtonEventArgs>(this.BtnStructure_Click);

                this.btnLandTiles = new Button("btnLandTiles");
                this.btnLandTiles.Location = new Point(200, 10);
                this.btnLandTiles.Font = Graphic.FontManager.LoadFont("tahoma", 18);
                this.btnLandTiles.Size = new Size(100, 32);
                this.btnLandTiles.Text = "LandTiles";
                this.btnLandTiles.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLandTiles_Click);

                this.btnWaterTiles = new Button("btnWaterTiles");
                this.btnWaterTiles.Location = new Point(390, 10);
                this.btnWaterTiles.Font = Graphic.FontManager.LoadFont("tahoma", 18);
                this.btnWaterTiles.Size = new Size(100, 32);
                this.btnWaterTiles.Text = "WaterTiles";
                this.btnWaterTiles.Click += new EventHandler<MouseButtonEventArgs>(this.BtnWaterTiles_Click);

                this.btnAttributes = new Button("btnAttributes");
                this.btnAttributes.Location = new Point(10, 50);
                this.btnAttributes.Font = Graphic.FontManager.LoadFont("tahoma", 18);
                this.btnAttributes.Size = new Size(100, 32);
                this.btnAttributes.Text = "Attributes";
                this.btnAttributes.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAttributes_Click);

                this.btnItems = new Button("btnItems");
                this.btnItems.Location = new Point(200, 50);
                this.btnItems.Font = Graphic.FontManager.LoadFont("tahoma", 18);
                this.btnItems.Size = new Size(100, 32);
                this.btnItems.Text = "Items";
                this.btnItems.Click += new EventHandler<MouseButtonEventArgs>(this.BtnItems_Click);

                this.btnNpcs = new Button("btnNpcs");
                this.btnNpcs.Location = new Point(390, 50);
                this.btnNpcs.Font = Graphic.FontManager.LoadFont("tahoma", 18);
                this.btnNpcs.Size = new Size(100, 32);
                this.btnNpcs.Text = "Npcs";
                this.btnNpcs.Click += new EventHandler<MouseButtonEventArgs>(this.BtnNpcs_Click);

                this.btnTraps = new Button("btnTraps");
                this.btnTraps.Location = new Point(10, 90);
                this.btnTraps.Font = Graphic.FontManager.LoadFont("tahoma", 18);
                this.btnTraps.Size = new Size(100, 32);
                this.btnTraps.Text = "Traps";
                this.btnTraps.Click += new EventHandler<MouseButtonEventArgs>(this.BtnTraps_Click);

                this.btnWeather = new Button("btnWeather");
                this.btnWeather.Location = new Point(200, 90);
                this.btnWeather.Font = Graphic.FontManager.LoadFont("tahoma", 18);
                this.btnWeather.Size = new Size(100, 32);
                this.btnWeather.Text = "Weather";
                this.btnWeather.Click += new EventHandler<MouseButtonEventArgs>(this.BtnWeather_Click);

                this.btnGoal = new Button("btnGoal");
                this.btnGoal.Location = new Point(390, 90);
                this.btnGoal.Font = Graphic.FontManager.LoadFont("tahoma", 18);
                this.btnGoal.Size = new Size(100, 32);
                this.btnGoal.Text = "Goal";
                this.btnGoal.Click += new EventHandler<MouseButtonEventArgs>(this.BtnGoal_Click);

                this.btnChambers = new Button("btnChambers");
                this.btnChambers.Location = new Point(10, 130);
                this.btnChambers.Font = Graphic.FontManager.LoadFont("tahoma", 18);
                this.btnChambers.Size = new Size(100, 32);
                this.btnChambers.Text = "Chambers";
                this.btnChambers.Click += new EventHandler<MouseButtonEventArgs>(this.BtnChambers_Click);

                this.btnMisc = new Button("btnMisc");
                this.btnMisc.Location = new Point(200, 130);
                this.btnMisc.Font = Graphic.FontManager.LoadFont("tahoma", 18);
                this.btnMisc.Size = new Size(100, 32);
                this.btnMisc.Text = "Misc";
                this.btnMisc.Click += new EventHandler<MouseButtonEventArgs>(this.BtnMisc_Click);
                this.pnlRDungeonFloorSettingSelection.AddWidget(this.btnStructure);
                this.pnlRDungeonFloorSettingSelection.AddWidget(this.btnLandTiles);
                this.pnlRDungeonFloorSettingSelection.AddWidget(this.btnWaterTiles);
                this.pnlRDungeonFloorSettingSelection.AddWidget(this.btnAttributes);
                this.pnlRDungeonFloorSettingSelection.AddWidget(this.btnItems);
                this.pnlRDungeonFloorSettingSelection.AddWidget(this.btnNpcs);
                this.pnlRDungeonFloorSettingSelection.AddWidget(this.btnTraps);
                this.pnlRDungeonFloorSettingSelection.AddWidget(this.btnWeather);
                this.pnlRDungeonFloorSettingSelection.AddWidget(this.btnGoal);
                this.pnlRDungeonFloorSettingSelection.AddWidget(this.btnChambers);
                this.pnlRDungeonFloorSettingSelection.AddWidget(this.btnMisc);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonFloorSettingSelection);
                this.floorSettingSelectionLoaded = true;
            }
        }

        private void LoadpnlRDungeonStructure()
        {
            if (!this.structureLoaded)
            {
                this.pnlRDungeonStructure = new Panel("pnlRDungeonStructure");
                this.pnlRDungeonStructure.Size = new Size(600, 320);
                this.pnlRDungeonStructure.Location = new Point(0, 80);
                this.pnlRDungeonStructure.BackColor = Color.LightGray;
                this.pnlRDungeonStructure.Visible = false;
                this.lblStructure = new Label("lblStructure");
                this.lblStructure.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblStructure.Text = "Structure Settings";
                this.lblStructure.AutoSize = true;
                this.lblStructure.Location = new Point(10, 4);

                this.lblTrapMin = new Label("lblTrapMin");
                this.lblTrapMin.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblTrapMin.Text = "Minimum Traps:";
                this.lblTrapMin.AutoSize = true;
                this.lblTrapMin.Location = new Point(10, 28);

                this.nudTrapMin = new NumericUpDown("nudTrapMin");
                this.nudTrapMin.Minimum = 0;
                this.nudTrapMin.Maximum = int.MaxValue;
                this.nudTrapMin.Size = new Size(80, 20);
                this.nudTrapMin.Location = new Point(10, 42);
                this.nudTrapMin.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblTrapMax = new Label("lblTrapMax");
                this.lblTrapMax.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblTrapMax.Text = "Maximum Traps:";
                this.lblTrapMax.AutoSize = true;
                this.lblTrapMax.Location = new Point(10, 72);

                this.nudTrapMax = new NumericUpDown("nudTrapMax");
                this.nudTrapMax.Minimum = 0;
                this.nudTrapMax.Maximum = int.MaxValue;
                this.nudTrapMax.Size = new Size(80, 20);
                this.nudTrapMax.Location = new Point(10, 86);
                this.nudTrapMax.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblItemMin = new Label("lblItemMin");
                this.lblItemMin.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblItemMin.Text = "Minimum Items:";
                this.lblItemMin.AutoSize = true;
                this.lblItemMin.Location = new Point(10, 116);

                this.nudItemMin = new NumericUpDown("nudItemMin");
                this.nudItemMin.Minimum = 0;
                this.nudItemMin.Maximum = 255;
                this.nudItemMin.Size = new Size(80, 20);
                this.nudItemMin.Location = new Point(10, 130);
                this.nudItemMin.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblItemMax = new Label("lblItemMax");
                this.lblItemMax.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblItemMax.Text = "Maximum Items:";
                this.lblItemMax.AutoSize = true;
                this.lblItemMax.Location = new Point(10, 160);

                this.nudItemMax = new NumericUpDown("nudItemMax");
                this.nudItemMax.Minimum = 0;
                this.nudItemMax.Maximum = 255;
                this.nudItemMax.Size = new Size(80, 20);
                this.nudItemMax.Location = new Point(10, 174);
                this.nudItemMax.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblRoomWidthMin = new Label("lblRoomWidthMin");
                this.lblRoomWidthMin.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblRoomWidthMin.Text = "Min Room Width:";
                this.lblRoomWidthMin.AutoSize = true;
                this.lblRoomWidthMin.Location = new Point(140, 28);

                this.nudRoomWidthMin = new NumericUpDown("nudRoomWidthMin");
                this.nudRoomWidthMin.Minimum = 0;
                this.nudRoomWidthMin.Maximum = 48;
                this.nudRoomWidthMin.Size = new Size(80, 20);
                this.nudRoomWidthMin.Location = new Point(140, 42);
                this.nudRoomWidthMin.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblRoomWidthMax = new Label("lblRoomWidthMax");
                this.lblRoomWidthMax.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblRoomWidthMax.Text = "Max Room Width:";
                this.lblRoomWidthMax.AutoSize = true;
                this.lblRoomWidthMax.Location = new Point(140, 72);

                this.nudRoomWidthMax = new NumericUpDown("nudRoomWidthMax");
                this.nudRoomWidthMax.Minimum = 0;
                this.nudRoomWidthMax.Maximum = 48;
                this.nudRoomWidthMax.Size = new Size(80, 20);
                this.nudRoomWidthMax.Location = new Point(140, 86);
                this.nudRoomWidthMax.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblRoomLengthMin = new Label("lblRoomLengthMin");
                this.lblRoomLengthMin.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblRoomLengthMin.Text = "Min Room Length:";
                this.lblRoomLengthMin.AutoSize = true;
                this.lblRoomLengthMin.Location = new Point(140, 116);

                this.nudRoomLengthMin = new NumericUpDown("nudRoomLengthMin");
                this.nudRoomLengthMin.Minimum = 0;
                this.nudRoomLengthMin.Maximum = 48;
                this.nudRoomLengthMin.Size = new Size(80, 20);
                this.nudRoomLengthMin.Location = new Point(140, 130);
                this.nudRoomLengthMin.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblRoomLengthMax = new Label("lblRoomLengthMax");
                this.lblRoomLengthMax.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblRoomLengthMax.Text = "Max Room Length:";
                this.lblRoomLengthMax.AutoSize = true;
                this.lblRoomLengthMax.Location = new Point(140, 160);

                this.nudRoomLengthMax = new NumericUpDown("nudRoomLengthMax");
                this.nudRoomLengthMax.Minimum = 0;
                this.nudRoomLengthMax.Maximum = 48;
                this.nudRoomLengthMax.Size = new Size(80, 20);
                this.nudRoomLengthMax.Location = new Point(140, 174);
                this.nudRoomLengthMax.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblHallTurnMin = new Label("lblHallTurnMin");
                this.lblHallTurnMin.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblHallTurnMin.Text = "Min Hall Turns:";
                this.lblHallTurnMin.AutoSize = true;
                this.lblHallTurnMin.Location = new Point(270, 28);

                this.nudHallTurnMin = new NumericUpDown("nudHallTurnMin");
                this.nudHallTurnMin.Minimum = 0;
                this.nudHallTurnMin.Maximum = 48;
                this.nudHallTurnMin.Size = new Size(80, 20);
                this.nudHallTurnMin.Location = new Point(270, 42);
                this.nudHallTurnMin.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblHallTurnMax = new Label("lblHallTurnMax");
                this.lblHallTurnMax.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblHallTurnMax.Text = "Max Hall Turns:";
                this.lblHallTurnMax.AutoSize = true;
                this.lblHallTurnMax.Location = new Point(270, 72);

                this.nudHallTurnMax = new NumericUpDown("nudHallTurnMax");
                this.nudHallTurnMax.Minimum = 0;
                this.nudHallTurnMax.Maximum = 48;
                this.nudHallTurnMax.Size = new Size(80, 20);
                this.nudHallTurnMax.Location = new Point(270, 86);
                this.nudHallTurnMax.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblHallVarMin = new Label("lblHallVarMin");
                this.lblHallVarMin.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblHallVarMin.Text = "Min Hall Variation:";
                this.lblHallVarMin.AutoSize = true;
                this.lblHallVarMin.Location = new Point(270, 116);

                this.nudHallVarMin = new NumericUpDown("nudHallVarMin");
                this.nudHallVarMin.Minimum = 0;
                this.nudHallVarMin.Maximum = 48;
                this.nudHallVarMin.Size = new Size(80, 20);
                this.nudHallVarMin.Location = new Point(270, 130);
                this.nudHallVarMin.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblHallVarMax = new Label("lblHallVarMax");
                this.lblHallVarMax.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblHallVarMax.Text = "Max Hall Variation:";
                this.lblHallVarMax.AutoSize = true;
                this.lblHallVarMax.Location = new Point(270, 160);

                this.nudHallVarMax = new NumericUpDown("nudHallVarMax");
                this.nudHallVarMax.Minimum = 0;
                this.nudHallVarMax.Maximum = 48;
                this.nudHallVarMax.Size = new Size(80, 20);
                this.nudHallVarMax.Location = new Point(270, 174);
                this.nudHallVarMax.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblWaterFrequency = new Label("lblWaterFrequency");
                this.lblWaterFrequency.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWaterFrequency.Text = "Water Frequency:";
                this.lblWaterFrequency.AutoSize = true;
                this.lblWaterFrequency.Location = new Point(400, 28);

                this.nudWaterFrequency = new NumericUpDown("nudWaterFrequency");
                this.nudWaterFrequency.Minimum = 0;
                this.nudWaterFrequency.Maximum = 100;
                this.nudWaterFrequency.Size = new Size(80, 20);
                this.nudWaterFrequency.Location = new Point(400, 42);
                this.nudWaterFrequency.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblCraters = new Label("lblCraters");
                this.lblCraters.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblCraters.Text = "Craters:";
                this.lblCraters.AutoSize = true;
                this.lblCraters.Location = new Point(400, 72);

                this.nudCraters = new NumericUpDown("nudCraters");
                this.nudCraters.Minimum = 0;
                this.nudCraters.Maximum = 20;
                this.nudCraters.Size = new Size(80, 20);
                this.nudCraters.Location = new Point(400, 86);
                this.nudCraters.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblCraterMinLength = new Label("lblCraterMinLength");
                this.lblCraterMinLength.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblCraterMinLength.Text = "Min Crater Length:";
                this.lblCraterMinLength.AutoSize = true;
                this.lblCraterMinLength.Location = new Point(400, 116);

                this.nudCraterMinLength = new NumericUpDown("nudCraterMinLength");
                this.nudCraterMinLength.Minimum = 0;
                this.nudCraterMinLength.Maximum = 100;
                this.nudCraterMinLength.Size = new Size(80, 20);
                this.nudCraterMinLength.Location = new Point(400, 130);
                this.nudCraterMinLength.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblCraterMaxLength = new Label("lblCraterMaxLength");
                this.lblCraterMaxLength.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblCraterMaxLength.Text = "Max Crater Length:";
                this.lblCraterMaxLength.AutoSize = true;
                this.lblCraterMaxLength.Location = new Point(400, 160);

                this.nudCraterMaxLength = new NumericUpDown("nudCraterMaxLength");
                this.nudCraterMaxLength.Minimum = 0;
                this.nudCraterMaxLength.Maximum = 100;
                this.nudCraterMaxLength.Size = new Size(80, 20);
                this.nudCraterMaxLength.Location = new Point(400, 174);
                this.nudCraterMaxLength.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.chkCraterFuzzy = new CheckBox("chkCraterFuzzy");
                this.chkCraterFuzzy.Size = new Size(100, 17);
                this.chkCraterFuzzy.Location = new Point(400, 204);
                this.chkCraterFuzzy.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.chkCraterFuzzy.Text = "Fuzzy Craters";
                this.pnlRDungeonStructure.AddWidget(this.lblStructure);
                this.pnlRDungeonStructure.AddWidget(this.lblTrapMin);
                this.pnlRDungeonStructure.AddWidget(this.nudTrapMin);
                this.pnlRDungeonStructure.AddWidget(this.lblTrapMax);
                this.pnlRDungeonStructure.AddWidget(this.nudTrapMax);
                this.pnlRDungeonStructure.AddWidget(this.lblItemMin);
                this.pnlRDungeonStructure.AddWidget(this.nudItemMin);
                this.pnlRDungeonStructure.AddWidget(this.lblItemMax);
                this.pnlRDungeonStructure.AddWidget(this.nudItemMax);
                this.pnlRDungeonStructure.AddWidget(this.lblRoomWidthMin);
                this.pnlRDungeonStructure.AddWidget(this.nudRoomWidthMin);
                this.pnlRDungeonStructure.AddWidget(this.lblRoomWidthMax);
                this.pnlRDungeonStructure.AddWidget(this.nudRoomWidthMax);
                this.pnlRDungeonStructure.AddWidget(this.lblRoomLengthMin);
                this.pnlRDungeonStructure.AddWidget(this.nudRoomLengthMin);
                this.pnlRDungeonStructure.AddWidget(this.lblRoomLengthMax);
                this.pnlRDungeonStructure.AddWidget(this.nudRoomLengthMax);
                this.pnlRDungeonStructure.AddWidget(this.lblHallTurnMin);
                this.pnlRDungeonStructure.AddWidget(this.nudHallTurnMin);
                this.pnlRDungeonStructure.AddWidget(this.lblHallTurnMax);
                this.pnlRDungeonStructure.AddWidget(this.nudHallTurnMax);
                this.pnlRDungeonStructure.AddWidget(this.lblHallVarMin);
                this.pnlRDungeonStructure.AddWidget(this.nudHallVarMin);
                this.pnlRDungeonStructure.AddWidget(this.lblHallVarMax);
                this.pnlRDungeonStructure.AddWidget(this.nudHallVarMax);
                this.pnlRDungeonStructure.AddWidget(this.lblWaterFrequency);
                this.pnlRDungeonStructure.AddWidget(this.nudWaterFrequency);
                this.pnlRDungeonStructure.AddWidget(this.lblCraters);
                this.pnlRDungeonStructure.AddWidget(this.nudCraters);
                this.pnlRDungeonStructure.AddWidget(this.lblCraterMinLength);
                this.pnlRDungeonStructure.AddWidget(this.nudCraterMinLength);
                this.pnlRDungeonStructure.AddWidget(this.lblCraterMaxLength);
                this.pnlRDungeonStructure.AddWidget(this.nudCraterMaxLength);
                this.pnlRDungeonStructure.AddWidget(this.chkCraterFuzzy);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonStructure);
                this.structureLoaded = true;
            }
        }

        private void LoadpnlRDungeonLandTiles()
        {
            if (!this.landTilesLoaded)
            {
                this.pnlRDungeonLandTiles = new Panel("pnlRDungeonLandTiles");
                this.pnlRDungeonLandTiles.Size = new Size(600, 320);
                this.pnlRDungeonLandTiles.Location = new Point(0, 80);
                this.pnlRDungeonLandTiles.BackColor = Color.LightGray;
                this.pnlRDungeonLandTiles.Visible = false;

                // Label lblLandTiles;
                this.lblLandTiles = new Label("lblLandTiles");
                this.lblLandTiles.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblLandTiles.Text = "Land Tiles Settings";
                this.lblLandTiles.AutoSize = true;
                this.lblLandTiles.Location = new Point(10, 4);

                this.lblLandTileset = new Label[22];
                this.picLandTileset = new PictureBox[22];
                for (int i = 0; i < 22; i++)
                {
                    this.lblLandTileset[i] = new Label("lblLandTileset" + i);
                    this.lblLandTileset[i].Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                    this.lblLandTileset[i].AutoSize = true;
                    if (i < 16)
                    {
                        this.lblLandTileset[i].Location = new Point(2 + (70 * (i / 4)), 30 + (70 * (i % 4)));
                    }
else
                    {
                        this.lblLandTileset[i].Location = new Point(352 + (70 * ((i - 16) / 3)), 30 + (70 * ((i - 16) % 3)));
                    }

                    this.picLandTileset[i] = new PictureBox("picLandTileset" + i);
                    this.picLandTileset[i].Size = new Size(32, 32);
                    this.picLandTileset[i].BackColor = Color.Transparent;
                    this.picLandTileset[i].Image = new SdlDotNet.Graphics.Surface(32, 32);
                    this.picLandTileset[i].Click += new EventHandler<MouseButtonEventArgs>(this.PicLandTileset_Click);

                    if (i < 16)
                    {
                        this.picLandTileset[i].Location = new Point(10 + (70 * (i / 4)), 46 + (70 * (i % 4)));
                    }
else
                    {
                        this.picLandTileset[i].Location = new Point(360 + (70 * ((i - 16) / 3)), 46 + (70 * ((i - 16) % 3)));
                    }

                    this.pnlRDungeonLandTiles.AddWidget(this.lblLandTileset[i]);
                    this.pnlRDungeonLandTiles.AddWidget(this.picLandTileset[i]);
                }

                this.lblLandTileset[19].Text = "Stairs";
                this.lblLandTileset[16].Text = "Ground";
                this.lblLandTileset[10].Text = "Top Left";
                this.lblLandTileset[6].Text = "Top Center";
                this.lblLandTileset[2].Text = "Top Right";
                this.lblLandTileset[9].Text = "Center Left";
                this.lblLandTileset[5].Text = "Center Center";
                this.lblLandTileset[1].Text = "Center Right";
                this.lblLandTileset[8].Text = "Bottom Left";
                this.lblLandTileset[4].Text = "Bottom Center";
                this.lblLandTileset[0].Text = "Bottom Right";
                this.lblLandTileset[17].Text = "Inner Top Left";
                this.lblLandTileset[20].Text = "Inner Top Right";
                this.lblLandTileset[18].Text = "Inner Bottom Left";
                this.lblLandTileset[21].Text = "Inner Bottom Right";
                this.lblLandTileset[15].Text = "Isolated Wall";
                this.lblLandTileset[12].Text = "Column Top";
                this.lblLandTileset[13].Text = "Column Center";
                this.lblLandTileset[14].Text = "Column Bottom";
                this.lblLandTileset[3].Text = "Row Left";
                this.lblLandTileset[7].Text = "Row Center";
                this.lblLandTileset[11].Text = "Row Right";

                this.btnLandAltSwitch = new Button("btnLandAltSwitch");
                this.btnLandAltSwitch.Location = new Point(520, 280);
                this.btnLandAltSwitch.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnLandAltSwitch.Size = new Size(64, 16);
                this.btnLandAltSwitch.Text = "Alt ->";
                this.btnLandAltSwitch.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLandAltSwitch_Click);
                this.pnlRDungeonLandTiles.AddWidget(this.lblLandTiles);
                this.landTileNumbers = new int[2, 22];
                this.pnlRDungeonLandTiles.AddWidget(this.btnLandAltSwitch);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonLandTiles);
                this.landTilesLoaded = true;
            }
        }

        private void LoadpnlRDungeonLandAltTiles()
        {
            if (!this.landAltTilesLoaded)
            {
                this.pnlRDungeonLandAltTiles = new Panel("pnlRDungeonLandAltTiles");
                this.pnlRDungeonLandAltTiles.Size = new Size(600, 320);
                this.pnlRDungeonLandAltTiles.Location = new Point(0, 80);
                this.pnlRDungeonLandAltTiles.BackColor = Color.LightGray;
                this.pnlRDungeonLandAltTiles.Visible = false;

                // Label lblLandTiles;
                this.lblLandAltTiles = new Label("lblLandAltTiles");
                this.lblLandAltTiles.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblLandAltTiles.Text = "Land Alt Tiles Settings";
                this.lblLandAltTiles.AutoSize = true;
                this.lblLandAltTiles.Location = new Point(10, 4);

                this.lblLandAltTileset = new Label[23];
                this.picLandAltTileset = new PictureBox[23];
                for (int i = 0; i < 23; i++)
                {
                    this.lblLandAltTileset[i] = new Label("lblLandAltTileset" + i);
                    this.lblLandAltTileset[i].Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                    this.lblLandAltTileset[i].AutoSize = true;
                    if (i == 5)
                    {
                        this.lblLandAltTileset[i].Location = new Point(352, 240);
                    }
                    else if (i == 22)
                    {
                        this.lblLandAltTileset[i].Location = new Point(422, 240);
                    }
                    else if (i < 16)
                    {
                        this.lblLandAltTileset[i].Location = new Point(2 + (70 * (i / 4)), 30 + (70 * (i % 4)));
                    }
else
                    {
                        this.lblLandAltTileset[i].Location = new Point(352 + (70 * ((i - 16) / 3)), 30 + (70 * ((i - 16) % 3)));
                    }

                    this.picLandAltTileset[i] = new PictureBox("picLandAltTileset" + i);
                    this.picLandAltTileset[i].Size = new Size(32, 32);
                    this.picLandAltTileset[i].BackColor = Color.Transparent;
                    this.picLandAltTileset[i].Image = new SdlDotNet.Graphics.Surface(32, 32);
                    this.picLandAltTileset[i].Click += new EventHandler<MouseButtonEventArgs>(this.PicLandAltTileset_Click);

                    if (i == 5)
                    {
                        this.picLandAltTileset[i].Location = new Point(360, 256);
                    }
                    else if (i == 22)
                    {
                        this.picLandAltTileset[i].Location = new Point(430, 256);
                    }
                    else if (i < 16)
                    {
                        this.picLandAltTileset[i].Location = new Point(10 + (70 * (i / 4)), 46 + (70 * (i % 4)));
                    }
else
                    {
                        this.picLandAltTileset[i].Location = new Point(360 + (70 * ((i - 16) / 3)), 46 + (70 * ((i - 16) % 3)));
                    }

                    this.pnlRDungeonLandAltTiles.AddWidget(this.lblLandAltTileset[i]);
                    this.pnlRDungeonLandAltTiles.AddWidget(this.picLandAltTileset[i]);
                }

                this.lblLandAltTileset[19].Text = "Ground2";
                this.lblLandAltTileset[16].Text = "Ground";
                this.lblLandAltTileset[10].Text = "Top Left";
                this.lblLandAltTileset[6].Text = "Top Center";
                this.lblLandAltTileset[2].Text = "Top Right";
                this.lblLandAltTileset[9].Text = "Center Left";
                this.lblLandAltTileset[5].Text = "Center Center";
                this.lblLandAltTileset[22].Text = "Center Center2";
                this.lblLandAltTileset[1].Text = "Center Right";
                this.lblLandAltTileset[8].Text = "Bottom Left";
                this.lblLandAltTileset[4].Text = "Bottom Center";
                this.lblLandAltTileset[0].Text = "Bottom Right";
                this.lblLandAltTileset[17].Text = "Inner Top Left";
                this.lblLandAltTileset[20].Text = "Inner Top Right";
                this.lblLandAltTileset[18].Text = "Inner Bottom Left";
                this.lblLandAltTileset[21].Text = "Inner Bottom Right";
                this.lblLandAltTileset[15].Text = "Isolated Wall";
                this.lblLandAltTileset[12].Text = "Column Top";
                this.lblLandAltTileset[13].Text = "Column Center";
                this.lblLandAltTileset[14].Text = "Column Bottom";
                this.lblLandAltTileset[3].Text = "Row Left";
                this.lblLandAltTileset[7].Text = "Row Center";
                this.lblLandAltTileset[11].Text = "Row Right";

                this.btnLandSwitch = new Button("btnLandSwitch");
                this.btnLandSwitch.Location = new Point(520, 280);
                this.btnLandSwitch.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnLandSwitch.Size = new Size(64, 16);
                this.btnLandSwitch.Text = "Normal ->";
                this.btnLandSwitch.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLandSwitch_Click);
                this.pnlRDungeonLandAltTiles.AddWidget(this.lblLandAltTiles);
                this.landAltTileNumbers = new int[2, 23];
                this.pnlRDungeonLandAltTiles.AddWidget(this.btnLandSwitch);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonLandAltTiles);
                this.landAltTilesLoaded = true;
            }
        }

        private void LoadpnlRDungeonWaterTiles()
        {
            if (!this.waterTilesLoaded)
            {
                this.pnlRDungeonWaterTiles = new Panel("pnlRDungeonWaterTiles");
                this.pnlRDungeonWaterTiles.Size = new Size(600, 320);
                this.pnlRDungeonWaterTiles.Location = new Point(0, 80);
                this.pnlRDungeonWaterTiles.BackColor = Color.LightGray;
                this.pnlRDungeonWaterTiles.Visible = false;

                // Label lblWaterTiles;
                this.lblWaterTiles = new Label("lblWaterTiles");
                this.lblWaterTiles.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWaterTiles.Text = "WaterTiles Settings";
                this.lblWaterTiles.AutoSize = true;
                this.lblWaterTiles.Location = new Point(10, 4);

                this.lblWaterTileset = new Label[22];
                this.picWaterTileset = new PictureBox[22];
                for (int i = 0; i < 22; i++)
                {
                    this.lblWaterTileset[i] = new Label("lblWaterTileset" + i);
                    this.lblWaterTileset[i].Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                    this.lblWaterTileset[i].AutoSize = true;
                    if (i < 16)
                    {
                        this.lblWaterTileset[i].Location = new Point(2 + (70 * (i / 4)), 30 + (70 * (i % 4)));
                    }
else
                    {
                        this.lblWaterTileset[i].Location = new Point(352 + (70 * ((i - 16) / 3)), 30 + (70 * ((i - 16) % 3)));
                    }

                    this.picWaterTileset[i] = new PictureBox("picWaterTileset" + i);
                    this.picWaterTileset[i].Size = new Size(32, 32);
                    this.picWaterTileset[i].BackColor = Color.Transparent;
                    this.picWaterTileset[i].Image = new SdlDotNet.Graphics.Surface(32, 32);
                    this.picWaterTileset[i].Click += new EventHandler<MouseButtonEventArgs>(this.PicWaterTileset_Click);

                    if (i < 16)
                    {
                        this.picWaterTileset[i].Location = new Point(10 + (70 * (i / 4)), 46 + (70 * (i % 4)));
                    }
else
                    {
                        this.picWaterTileset[i].Location = new Point(360 + (70 * ((i - 16) / 3)), 46 + (70 * ((i - 16) % 3)));
                    }

                    this.pnlRDungeonWaterTiles.AddWidget(this.lblWaterTileset[i]);
                    this.pnlRDungeonWaterTiles.AddWidget(this.picWaterTileset[i]);
                }

                this.lblWaterTileset[15].Text = "Surrounded";
                this.lblWaterTileset[0].Text = "InnerTopLeft";
                this.lblWaterTileset[4].Text = "Top";
                this.lblWaterTileset[8].Text = "InnerTopRight";
                this.lblWaterTileset[1].Text = "Left";
                this.lblWaterTileset[5].Text = "Water";
                this.lblWaterTileset[9].Text = "Right";
                this.lblWaterTileset[2].Text = "InnerBottomLeft";
                this.lblWaterTileset[6].Text = "Bottom";
                this.lblWaterTileset[10].Text = "InnerBottomRight";
                this.lblWaterTileset[16].Text = "TopLeft";
                this.lblWaterTileset[19].Text = "TopRight";
                this.lblWaterTileset[17].Text = "BottomLeft";
                this.lblWaterTileset[20].Text = "BottomRight";
                this.lblWaterTileset[18].Text = "DiagonalForward";
                this.lblWaterTileset[21].Text = "DiagonalBack";
                this.lblWaterTileset[12].Text = "InnerTop";
                this.lblWaterTileset[13].Text = "Vertical";
                this.lblWaterTileset[14].Text = "InnerBottom";
                this.lblWaterTileset[3].Text = "InnerLeft";
                this.lblWaterTileset[7].Text = "Horizontal";
                this.lblWaterTileset[11].Text = "InnerRight";

                this.btnWaterAnimSwitch = new Button("btnWaterAnimSwitch");
                this.btnWaterAnimSwitch.Location = new Point(520, 280);
                this.btnWaterAnimSwitch.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnWaterAnimSwitch.Size = new Size(64, 16);
                this.btnWaterAnimSwitch.Text = "Anim ->";
                this.btnWaterAnimSwitch.Click += new EventHandler<MouseButtonEventArgs>(this.BtnWaterAnimSwitch_Click);
                this.pnlRDungeonWaterTiles.AddWidget(this.lblWaterTiles);
                this.waterTileNumbers = new int[2, 22];
                this.pnlRDungeonWaterTiles.AddWidget(this.btnWaterAnimSwitch);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonWaterTiles);
                this.waterTilesLoaded = true;
            }
        }

        private void LoadpnlRDungeonWaterAnimTiles()
        {
            if (!this.waterAnimTilesLoaded)
            {
                this.pnlRDungeonWaterAnimTiles = new Panel("pnlRDungeonWaterAnimTiles");
                this.pnlRDungeonWaterAnimTiles.Size = new Size(600, 320);
                this.pnlRDungeonWaterAnimTiles.Location = new Point(0, 80);
                this.pnlRDungeonWaterAnimTiles.BackColor = Color.LightGray;
                this.pnlRDungeonWaterAnimTiles.Visible = false;

                // Label lblWaterAnimTiles;
                this.lblWaterAnimTiles = new Label("lblWaterAnimTiles");
                this.lblWaterAnimTiles.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWaterAnimTiles.Text = "WaterAnimTiles Settings";
                this.lblWaterAnimTiles.AutoSize = true;
                this.lblWaterAnimTiles.Location = new Point(10, 4);

                this.lblWaterAnimTileset = new Label[22];
                this.picWaterAnimTileset = new PictureBox[22];
                for (int i = 0; i < 22; i++)
                {
                    this.lblWaterAnimTileset[i] = new Label("lblWaterAnimTileset" + i);
                    this.lblWaterAnimTileset[i].Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                    this.lblWaterAnimTileset[i].AutoSize = true;
                    if (i < 16)
                    {
                        this.lblWaterAnimTileset[i].Location = new Point(2 + (70 * (i / 4)), 30 + (70 * (i % 4)));
                    }
else
                    {
                        this.lblWaterAnimTileset[i].Location = new Point(352 + (70 * ((i - 16) / 3)), 30 + (70 * ((i - 16) % 3)));
                    }

                    this.picWaterAnimTileset[i] = new PictureBox("picWaterAnimTileset" + i);
                    this.picWaterAnimTileset[i].Size = new Size(32, 32);
                    this.picWaterAnimTileset[i].BackColor = Color.Transparent;
                    this.picWaterAnimTileset[i].Image = new SdlDotNet.Graphics.Surface(32, 32);
                    this.picWaterAnimTileset[i].Click += new EventHandler<MouseButtonEventArgs>(this.PicWaterAnimTileset_Click);

                    if (i < 16)
                    {
                        this.picWaterAnimTileset[i].Location = new Point(10 + (70 * (i / 4)), 46 + (70 * (i % 4)));
                    }
else
                    {
                        this.picWaterAnimTileset[i].Location = new Point(360 + (70 * ((i - 16) / 3)), 46 + (70 * ((i - 16) % 3)));
                    }

                    this.pnlRDungeonWaterAnimTiles.AddWidget(this.lblWaterAnimTileset[i]);
                    this.pnlRDungeonWaterAnimTiles.AddWidget(this.picWaterAnimTileset[i]);
                }

                this.lblWaterAnimTileset[15].Text = "Surrounded";
                this.lblWaterAnimTileset[0].Text = "InnerTopLeft";
                this.lblWaterAnimTileset[4].Text = "Top";
                this.lblWaterAnimTileset[8].Text = "InnerTopRight";
                this.lblWaterAnimTileset[1].Text = "Left";
                this.lblWaterAnimTileset[5].Text = "Water";
                this.lblWaterAnimTileset[9].Text = "Right";
                this.lblWaterAnimTileset[2].Text = "InnerBottomLeft";
                this.lblWaterAnimTileset[6].Text = "Bottom";
                this.lblWaterAnimTileset[10].Text = "InnerBottomRight";
                this.lblWaterAnimTileset[16].Text = "TopLeft";
                this.lblWaterAnimTileset[19].Text = "TopRight";
                this.lblWaterAnimTileset[17].Text = "BottomLeft";
                this.lblWaterAnimTileset[20].Text = "BottomRight";
                this.lblWaterAnimTileset[18].Text = "DiagonalForward";
                this.lblWaterAnimTileset[21].Text = "DiagonalBack";
                this.lblWaterAnimTileset[12].Text = "InnerTop";
                this.lblWaterAnimTileset[13].Text = "Vertical";
                this.lblWaterAnimTileset[14].Text = "InnerBottom";
                this.lblWaterAnimTileset[3].Text = "InnerLeft";
                this.lblWaterAnimTileset[7].Text = "Horizontal";
                this.lblWaterAnimTileset[11].Text = "InnerRight";

                this.btnWaterSwitch = new Button("btnWaterSwitch");
                this.btnWaterSwitch.Location = new Point(520, 280);
                this.btnWaterSwitch.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnWaterSwitch.Size = new Size(64, 16);
                this.btnWaterSwitch.Text = "Normal ->";
                this.btnWaterSwitch.Click += new EventHandler<MouseButtonEventArgs>(this.BtnWaterSwitch_Click);
                this.pnlRDungeonWaterAnimTiles.AddWidget(this.lblWaterAnimTiles);
                this.waterAnimTileNumbers = new int[2, 22];
                this.pnlRDungeonWaterAnimTiles.AddWidget(this.btnWaterSwitch);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonWaterAnimTiles);
                this.waterAnimTilesLoaded = true;
            }
        }

        private void LoadpnlRDungeonAttributes()
        {
            if (!this.attributesLoaded)
            {
                this.pnlRDungeonAttributes = new Panel("pnlRDungeonAttributes");
                this.pnlRDungeonAttributes.Size = new Size(600, 320);
                this.pnlRDungeonAttributes.Location = new Point(0, 80);
                this.pnlRDungeonAttributes.BackColor = Color.LightGray;
                this.pnlRDungeonAttributes.Visible = false;
                this.lblAttributes = new Label("lblAttributes");
                this.lblAttributes.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblAttributes.Text = "Attributes Settings";
                this.lblAttributes.AutoSize = true;
                this.lblAttributes.Location = new Point(10, 4);

                this.cbGroundType = new ComboBox("cbGroundType");
                this.cbGroundType.Location = new Point(10, 26);
                this.cbGroundType.Size = new Size(100, 16);
                for (int i = 0; i < 40; i++)
                {
                    this.cbGroundType.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), i + ": " + Enum.GetName(typeof(Enums.TileType), i)));
                }

                this.cbGroundType.SelectItem(0);

                this.lblGroundData1 = new Label("lblGroundData1");
                this.lblGroundData1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblGroundData1.Text = "Ground Data1:";
                this.lblGroundData1.AutoSize = true;
                this.lblGroundData1.Location = new Point(10, 52);

                this.nudGroundData1 = new NumericUpDown("nudGroundData1");
                this.nudGroundData1.Minimum = int.MinValue;
                this.nudGroundData1.Maximum = int.MaxValue;
                this.nudGroundData1.Size = new Size(80, 20);
                this.nudGroundData1.Location = new Point(10, 66);
                this.nudGroundData1.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblGroundData2 = new Label("lblGroundData2");
                this.lblGroundData2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblGroundData2.Text = "Ground Data2:";
                this.lblGroundData2.AutoSize = true;
                this.lblGroundData2.Location = new Point(10, 92);

                this.nudGroundData2 = new NumericUpDown("nudGroundData2");
                this.nudGroundData2.Minimum = int.MinValue;
                this.nudGroundData2.Maximum = int.MaxValue;
                this.nudGroundData2.Size = new Size(80, 20);
                this.nudGroundData2.Location = new Point(10, 106);
                this.nudGroundData2.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblGroundData3 = new Label("lblGroundData3");
                this.lblGroundData3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblGroundData3.Text = "Ground Data3:";
                this.lblGroundData3.AutoSize = true;
                this.lblGroundData3.Location = new Point(10, 132);

                this.nudGroundData3 = new NumericUpDown("nudGroundData3");
                this.nudGroundData3.Minimum = int.MinValue;
                this.nudGroundData3.Maximum = int.MaxValue;
                this.nudGroundData3.Size = new Size(80, 20);
                this.nudGroundData3.Location = new Point(10, 146);
                this.nudGroundData3.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblGroundString1 = new Label("lblGroundString1");
                this.lblGroundString1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblGroundString1.Text = "Ground String1:";
                this.lblGroundString1.AutoSize = true;
                this.lblGroundString1.Location = new Point(10, 172);

                this.txtGroundString1 = new TextBox("txtGroundString1");
                this.txtGroundString1.BackColor = Color.White;
                this.txtGroundString1.Size = new Size(80, 20);
                this.txtGroundString1.Location = new Point(10, 186);
                this.txtGroundString1.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblGroundString2 = new Label("lblGroundString2");
                this.lblGroundString2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblGroundString2.Text = "Ground String2:";
                this.lblGroundString2.AutoSize = true;
                this.lblGroundString2.Location = new Point(10, 212);

                this.txtGroundString2 = new TextBox("txtGroundString2");
                this.txtGroundString2.BackColor = Color.White;
                this.txtGroundString2.Size = new Size(80, 20);
                this.txtGroundString2.Location = new Point(10, 226);
                this.txtGroundString2.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblGroundString3 = new Label("lblGroundString3");
                this.lblGroundString3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblGroundString3.Text = "Ground String3:";
                this.lblGroundString3.AutoSize = true;
                this.lblGroundString3.Location = new Point(10, 252);

                this.txtGroundString3 = new TextBox("txtGroundString3");
                this.txtGroundString3.BackColor = Color.White;
                this.txtGroundString3.Size = new Size(80, 20);
                this.txtGroundString3.Location = new Point(10, 266);
                this.txtGroundString3.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.cbHallType = new ComboBox("cbHallType");
                this.cbHallType.Location = new Point(120, 26);
                this.cbHallType.Size = new Size(100, 16);
                for (int i = 0; i < 40; i++)
                {
                    this.cbHallType.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), i + ": " + Enum.GetName(typeof(Enums.TileType), i)));
                }

                this.cbHallType.SelectItem(0);

                this.lblHallData1 = new Label("lblHallData1");
                this.lblHallData1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblHallData1.Text = "Hall Data1:";
                this.lblHallData1.AutoSize = true;
                this.lblHallData1.Location = new Point(120, 52);

                this.nudHallData1 = new NumericUpDown("nudHallData1");
                this.nudHallData1.Minimum = int.MinValue;
                this.nudHallData1.Maximum = int.MaxValue;
                this.nudHallData1.Size = new Size(80, 20);
                this.nudHallData1.Location = new Point(120, 66);
                this.nudHallData1.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblHallData2 = new Label("lblHallData2");
                this.lblHallData2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblHallData2.Text = "Hall Data2:";
                this.lblHallData2.AutoSize = true;
                this.lblHallData2.Location = new Point(120, 92);

                this.nudHallData2 = new NumericUpDown("nudHallData2");
                this.nudHallData2.Minimum = int.MinValue;
                this.nudHallData2.Maximum = int.MaxValue;
                this.nudHallData2.Size = new Size(80, 20);
                this.nudHallData2.Location = new Point(120, 106);
                this.nudHallData2.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblHallData3 = new Label("lblHallData3");
                this.lblHallData3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblHallData3.Text = "Hall Data3:";
                this.lblHallData3.AutoSize = true;
                this.lblHallData3.Location = new Point(120, 132);

                this.nudHallData3 = new NumericUpDown("nudHallData3");
                this.nudHallData3.Minimum = int.MinValue;
                this.nudHallData3.Maximum = int.MaxValue;
                this.nudHallData3.Size = new Size(80, 20);
                this.nudHallData3.Location = new Point(120, 146);
                this.nudHallData3.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblHallString1 = new Label("lblHallString1");
                this.lblHallString1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblHallString1.Text = "Hall String1:";
                this.lblHallString1.AutoSize = true;
                this.lblHallString1.Location = new Point(120, 172);

                this.txtHallString1 = new TextBox("txtHallString1");
                this.txtHallString1.BackColor = Color.White;
                this.txtHallString1.Size = new Size(80, 20);
                this.txtHallString1.Location = new Point(120, 186);
                this.txtHallString1.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblHallString2 = new Label("lblHallString2");
                this.lblHallString2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblHallString2.Text = "Hall String2:";
                this.lblHallString2.AutoSize = true;
                this.lblHallString2.Location = new Point(120, 212);

                this.txtHallString2 = new TextBox("txtHallString2");
                this.txtHallString2.BackColor = Color.White;
                this.txtHallString2.Size = new Size(80, 20);
                this.txtHallString2.Location = new Point(120, 226);
                this.txtHallString2.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblHallString3 = new Label("lblHallString3");
                this.lblHallString3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblHallString3.Text = "Hall String3:";
                this.lblHallString3.AutoSize = true;
                this.lblHallString3.Location = new Point(120, 252);

                this.txtHallString3 = new TextBox("txtHallString3");
                this.txtHallString3.BackColor = Color.White;
                this.txtHallString3.Size = new Size(80, 20);
                this.txtHallString3.Location = new Point(120, 266);
                this.txtHallString3.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.cbWaterType = new ComboBox("cbWaterType");
                this.cbWaterType.Location = new Point(230, 26);
                this.cbWaterType.Size = new Size(100, 16);
                for (int i = 0; i < 40; i++)
                {
                    this.cbWaterType.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), i + ": " + Enum.GetName(typeof(Enums.TileType), i)));
                }

                this.cbWaterType.SelectItem(0);

                this.lblWaterData1 = new Label("lblWaterData1");
                this.lblWaterData1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWaterData1.Text = "Water Data1:";
                this.lblWaterData1.AutoSize = true;
                this.lblWaterData1.Location = new Point(230, 52);

                this.nudWaterData1 = new NumericUpDown("nudWaterData1");
                this.nudWaterData1.Minimum = int.MinValue;
                this.nudWaterData1.Maximum = int.MaxValue;
                this.nudWaterData1.Size = new Size(80, 20);
                this.nudWaterData1.Location = new Point(230, 66);
                this.nudWaterData1.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblWaterData2 = new Label("lblWaterData2");
                this.lblWaterData2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWaterData2.Text = "Water Data2:";
                this.lblWaterData2.AutoSize = true;
                this.lblWaterData2.Location = new Point(230, 92);

                this.nudWaterData2 = new NumericUpDown("nudWaterData2");
                this.nudWaterData2.Minimum = int.MinValue;
                this.nudWaterData2.Maximum = int.MaxValue;
                this.nudWaterData2.Size = new Size(80, 20);
                this.nudWaterData2.Location = new Point(230, 106);
                this.nudWaterData2.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblWaterData3 = new Label("lblWaterData3");
                this.lblWaterData3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWaterData3.Text = "Water Data3:";
                this.lblWaterData3.AutoSize = true;
                this.lblWaterData3.Location = new Point(230, 132);

                this.nudWaterData3 = new NumericUpDown("nudWaterData3");
                this.nudWaterData3.Minimum = int.MinValue;
                this.nudWaterData3.Maximum = int.MaxValue;
                this.nudWaterData3.Size = new Size(80, 20);
                this.nudWaterData3.Location = new Point(230, 146);
                this.nudWaterData3.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblWaterString1 = new Label("lblWaterString1");
                this.lblWaterString1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWaterString1.Text = "Water String1:";
                this.lblWaterString1.AutoSize = true;
                this.lblWaterString1.Location = new Point(230, 172);

                this.txtWaterString1 = new TextBox("txtWaterString1");
                this.txtWaterString1.BackColor = Color.White;
                this.txtWaterString1.Size = new Size(80, 20);
                this.txtWaterString1.Location = new Point(230, 186);
                this.txtWaterString1.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblWaterString2 = new Label("lblWaterString2");
                this.lblWaterString2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWaterString2.Text = "Water String2:";
                this.lblWaterString2.AutoSize = true;
                this.lblWaterString2.Location = new Point(230, 212);

                this.txtWaterString2 = new TextBox("txtWaterString2");
                this.txtWaterString2.BackColor = Color.White;
                this.txtWaterString2.Size = new Size(80, 20);
                this.txtWaterString2.Location = new Point(230, 226);
                this.txtWaterString2.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblWaterString3 = new Label("lblWaterString3");
                this.lblWaterString3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWaterString3.Text = "Water String3:";
                this.lblWaterString3.AutoSize = true;
                this.lblWaterString3.Location = new Point(230, 252);

                this.txtWaterString3 = new TextBox("txtWaterString3");
                this.txtWaterString3.BackColor = Color.White;
                this.txtWaterString3.Size = new Size(80, 20);
                this.txtWaterString3.Location = new Point(230, 266);
                this.txtWaterString3.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.cbWallType = new ComboBox("cbWallType");
                this.cbWallType.Location = new Point(340, 26);
                this.cbWallType.Size = new Size(100, 16);
                for (int i = 0; i < 40; i++)
                {
                    this.cbWallType.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), i + ": " + Enum.GetName(typeof(Enums.TileType), i)));
                }

                this.cbWallType.SelectItem(0);

                this.lblWallData1 = new Label("lblWallData1");
                this.lblWallData1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWallData1.Text = "Wall Data1:";
                this.lblWallData1.AutoSize = true;
                this.lblWallData1.Location = new Point(340, 52);

                this.nudWallData1 = new NumericUpDown("nudWallData1");
                this.nudWallData1.Minimum = int.MinValue;
                this.nudWallData1.Maximum = int.MaxValue;
                this.nudWallData1.Size = new Size(80, 20);
                this.nudWallData1.Location = new Point(340, 66);
                this.nudWallData1.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblWallData2 = new Label("lblWallData2");
                this.lblWallData2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWallData2.Text = "Wall Data2:";
                this.lblWallData2.AutoSize = true;
                this.lblWallData2.Location = new Point(340, 92);

                this.nudWallData2 = new NumericUpDown("nudWallData2");
                this.nudWallData2.Minimum = int.MinValue;
                this.nudWallData2.Maximum = int.MaxValue;
                this.nudWallData2.Size = new Size(80, 20);
                this.nudWallData2.Location = new Point(340, 106);
                this.nudWallData2.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblWallData3 = new Label("lblWallData3");
                this.lblWallData3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWallData3.Text = "Wall Data3:";
                this.lblWallData3.AutoSize = true;
                this.lblWallData3.Location = new Point(340, 132);

                this.nudWallData3 = new NumericUpDown("nudWallData3");
                this.nudWallData3.Minimum = int.MinValue;
                this.nudWallData3.Maximum = int.MaxValue;
                this.nudWallData3.Size = new Size(80, 20);
                this.nudWallData3.Location = new Point(340, 146);
                this.nudWallData3.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblWallString1 = new Label("lblWallString1");
                this.lblWallString1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWallString1.Text = "Wall String1:";
                this.lblWallString1.AutoSize = true;
                this.lblWallString1.Location = new Point(340, 172);

                this.txtWallString1 = new TextBox("txtWallString1");
                this.txtWallString1.BackColor = Color.White;
                this.txtWallString1.Size = new Size(80, 20);
                this.txtWallString1.Location = new Point(340, 186);
                this.txtWallString1.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblWallString2 = new Label("lblWallString2");
                this.lblWallString2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWallString2.Text = "Wall String2:";
                this.lblWallString2.AutoSize = true;
                this.lblWallString2.Location = new Point(340, 212);

                this.txtWallString2 = new TextBox("txtWallString2");
                this.txtWallString2.BackColor = Color.White;
                this.txtWallString2.Size = new Size(80, 20);
                this.txtWallString2.Location = new Point(340, 226);
                this.txtWallString2.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblWallString3 = new Label("lblWallString3");
                this.lblWallString3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWallString3.Text = "Wall String3:";
                this.lblWallString3.AutoSize = true;
                this.lblWallString3.Location = new Point(340, 252);

                this.txtWallString3 = new TextBox("txtWallString3");
                this.txtWallString3.BackColor = Color.White;
                this.txtWallString3.Size = new Size(80, 20);
                this.txtWallString3.Location = new Point(340, 266);
                this.txtWallString3.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.pnlRDungeonAttributes.AddWidget(this.lblAttributes);

                this.pnlRDungeonAttributes.AddWidget(this.cbGroundType);
                this.pnlRDungeonAttributes.AddWidget(this.lblGroundData1);
                this.pnlRDungeonAttributes.AddWidget(this.nudGroundData1);
                this.pnlRDungeonAttributes.AddWidget(this.lblGroundData2);
                this.pnlRDungeonAttributes.AddWidget(this.nudGroundData2);
                this.pnlRDungeonAttributes.AddWidget(this.lblGroundData3);
                this.pnlRDungeonAttributes.AddWidget(this.nudGroundData3);

                this.pnlRDungeonAttributes.AddWidget(this.lblGroundString1);
                this.pnlRDungeonAttributes.AddWidget(this.txtGroundString1);
                this.pnlRDungeonAttributes.AddWidget(this.lblGroundString2);
                this.pnlRDungeonAttributes.AddWidget(this.txtGroundString2);
                this.pnlRDungeonAttributes.AddWidget(this.lblGroundString3);
                this.pnlRDungeonAttributes.AddWidget(this.txtGroundString3);

                this.pnlRDungeonAttributes.AddWidget(this.cbHallType);
                this.pnlRDungeonAttributes.AddWidget(this.lblHallData1);
                this.pnlRDungeonAttributes.AddWidget(this.nudHallData1);
                this.pnlRDungeonAttributes.AddWidget(this.lblHallData2);
                this.pnlRDungeonAttributes.AddWidget(this.nudHallData2);
                this.pnlRDungeonAttributes.AddWidget(this.lblHallData3);
                this.pnlRDungeonAttributes.AddWidget(this.nudHallData3);

                this.pnlRDungeonAttributes.AddWidget(this.lblHallString1);
                this.pnlRDungeonAttributes.AddWidget(this.txtHallString1);
                this.pnlRDungeonAttributes.AddWidget(this.lblHallString2);
                this.pnlRDungeonAttributes.AddWidget(this.txtHallString2);
                this.pnlRDungeonAttributes.AddWidget(this.lblHallString3);
                this.pnlRDungeonAttributes.AddWidget(this.txtHallString3);

                this.pnlRDungeonAttributes.AddWidget(this.cbWaterType);
                this.pnlRDungeonAttributes.AddWidget(this.lblWaterData1);
                this.pnlRDungeonAttributes.AddWidget(this.nudWaterData1);
                this.pnlRDungeonAttributes.AddWidget(this.lblWaterData2);
                this.pnlRDungeonAttributes.AddWidget(this.nudWaterData2);
                this.pnlRDungeonAttributes.AddWidget(this.lblWaterData3);
                this.pnlRDungeonAttributes.AddWidget(this.nudWaterData3);

                this.pnlRDungeonAttributes.AddWidget(this.lblWaterString1);
                this.pnlRDungeonAttributes.AddWidget(this.txtWaterString1);
                this.pnlRDungeonAttributes.AddWidget(this.lblWaterString2);
                this.pnlRDungeonAttributes.AddWidget(this.txtWaterString2);
                this.pnlRDungeonAttributes.AddWidget(this.lblWaterString3);
                this.pnlRDungeonAttributes.AddWidget(this.txtWaterString3);

                this.pnlRDungeonAttributes.AddWidget(this.cbWallType);
                this.pnlRDungeonAttributes.AddWidget(this.lblWallData1);
                this.pnlRDungeonAttributes.AddWidget(this.nudWallData1);
                this.pnlRDungeonAttributes.AddWidget(this.lblWallData2);
                this.pnlRDungeonAttributes.AddWidget(this.nudWallData2);
                this.pnlRDungeonAttributes.AddWidget(this.lblWallData3);
                this.pnlRDungeonAttributes.AddWidget(this.nudWallData3);

                this.pnlRDungeonAttributes.AddWidget(this.lblWallString1);
                this.pnlRDungeonAttributes.AddWidget(this.txtWallString1);
                this.pnlRDungeonAttributes.AddWidget(this.lblWallString2);
                this.pnlRDungeonAttributes.AddWidget(this.txtWallString2);
                this.pnlRDungeonAttributes.AddWidget(this.lblWallString3);
                this.pnlRDungeonAttributes.AddWidget(this.txtWallString3);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonAttributes);
                this.attributesLoaded = true;
            }
        }

        private void LoadpnlRDungeonItems()
        {
            if (!this.itemsLoaded)
            {
                this.pnlRDungeonItems = new Panel("pnlRDungeonItems");
                this.pnlRDungeonItems.Size = new Size(600, 320);
                this.pnlRDungeonItems.Location = new Point(0, 80);
                this.pnlRDungeonItems.BackColor = Color.LightGray;
                this.pnlRDungeonItems.Visible = false;

                // Label lblItems;
                this.lblItems = new Label("lblItems");
                this.lblItems.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblItems.Text = "Items Settings";
                this.lblItems.AutoSize = true;
                this.lblItems.Location = new Point(10, 4);

                // ScrollingListBox lbxItems;
                this.lbxItems = new ScrollingListBox("lbxItems");
                this.lbxItems.Location = new Point(10, 130);
                this.lbxItems.Size = new Size(580, 160);

                // Label lblItemNum;
                this.lblItemNum = new Label("lblItemNum");
                this.lblItemNum.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblItemNum.Text = "Item #";
                this.lblItemNum.AutoSize = true;
                this.lblItemNum.Location = new Point(10, 26);

                // NumericUpDown nudItemNum;
                this.nudItemNum = new NumericUpDown("nudItemNum");
                this.nudItemNum.Minimum = 1;
                this.nudItemNum.Maximum = MaxInfo.MaxItems;
                this.nudItemNum.Size = new Size(80, 20);
                this.nudItemNum.Location = new Point(10, 40);
                this.nudItemNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.nudItemNum.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudItemNum_ValueChanged);

                // Label lblItemSpawnRate;
                this.lblItemSpawnRate = new Label("lblItemSpawnRate");
                this.lblItemSpawnRate.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblItemSpawnRate.Text = "Spawn Rate:";
                this.lblItemSpawnRate.AutoSize = true;
                this.lblItemSpawnRate.Location = new Point(10, 66);

                // NumericUpDown nudItemSpawnRate;
                this.nudItemSpawnRate = new NumericUpDown("nudItemSpawnRate");
                this.nudItemSpawnRate.Minimum = 1;
                this.nudItemSpawnRate.Maximum = 100;
                this.nudItemSpawnRate.Size = new Size(80, 20);
                this.nudItemSpawnRate.Location = new Point(10, 80);
                this.nudItemSpawnRate.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblMinValue = new Label("lblMinValue");
                this.lblMinValue.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblMinValue.Text = "Min Value";
                this.lblMinValue.AutoSize = true;
                this.lblMinValue.Location = new Point(120, 26);

                // NumericUpDown nudMinValue;
                this.nudMinValue = new NumericUpDown("nudMinValue");
                this.nudMinValue.Minimum = 1;
                this.nudMinValue.Maximum = int.MaxValue;
                this.nudMinValue.Size = new Size(80, 20);
                this.nudMinValue.Location = new Point(120, 40);
                this.nudMinValue.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // Label lblMaxValue;
                this.lblMaxValue = new Label("lblMaxValue");
                this.lblMaxValue.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblMaxValue.Text = "Max Value";
                this.lblMaxValue.AutoSize = true;
                this.lblMaxValue.Location = new Point(120, 66);

                // NumericUpDown nudMaxValue;
                this.nudMaxValue = new NumericUpDown("nudMaxValue");
                this.nudMaxValue.Minimum = 1;
                this.nudMaxValue.Maximum = int.MaxValue;
                this.nudMaxValue.Size = new Size(80, 20);
                this.nudMaxValue.Location = new Point(120, 80);
                this.nudMaxValue.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // Label lblStickyRate;
                this.lblStickyRate = new Label("lblStickyRate");
                this.lblStickyRate.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblStickyRate.Text = "Sticky Rate";
                this.lblStickyRate.AutoSize = true;
                this.lblStickyRate.Location = new Point(230, 26);

                // NumericUpDown nudStickyRate;
                this.nudStickyRate = new NumericUpDown("nudStickyRate");
                this.nudStickyRate.Minimum = 0;
                this.nudStickyRate.Maximum = 100;
                this.nudStickyRate.Size = new Size(80, 20);
                this.nudStickyRate.Location = new Point(230, 40);
                this.nudStickyRate.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // Label lblTag;
                this.lblTag = new Label("lblTag");
                this.lblTag.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblTag.Text = "Tag";
                this.lblTag.AutoSize = true;
                this.lblTag.Location = new Point(230, 66);

                // NumericUpDown txtTag;
                this.txtTag = new TextBox("nudTag");
                this.txtTag.BackColor = Color.White;
                this.txtTag.Size = new Size(80, 20);
                this.txtTag.Location = new Point(230, 80);
                this.txtTag.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.chkHidden = new CheckBox("chkHidden");
                this.chkHidden.Size = new Size(100, 17);
                this.chkHidden.Location = new Point(340, 26);
                this.chkHidden.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.chkHidden.Text = "Hidden";

                this.chkOnGround = new CheckBox("chkOnGround");
                this.chkOnGround.Size = new Size(100, 17);
                this.chkOnGround.Location = new Point(340, 66);
                this.chkOnGround.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.chkOnGround.Text = "On Ground";

                this.chkOnWater = new CheckBox("chkOnWater");
                this.chkOnWater.Size = new Size(100, 17);
                this.chkOnWater.Location = new Point(450, 26);
                this.chkOnWater.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.chkOnWater.Text = "On Water";

                this.chkOnWall = new CheckBox("chkOnWall");
                this.chkOnWall.Size = new Size(100, 17);
                this.chkOnWall.Location = new Point(450, 66);
                this.chkOnWall.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.chkOnWall.Text = "On Wall";

                // Button btnAddItem;
                this.btnAddItem = new Button("btnAddItem");
                this.btnAddItem.Location = new Point(10, 110);
                this.btnAddItem.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnAddItem.Size = new Size(64, 16);
                this.btnAddItem.Visible = true;
                this.btnAddItem.Text = "Add Item";
                this.btnAddItem.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddItem_Click);

                // Button btnRemoveItem;
                this.btnRemoveItem = new Button("btnRemoveItem");
                this.btnRemoveItem.Location = new Point(110, 110);
                this.btnRemoveItem.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnRemoveItem.Size = new Size(64, 16);
                this.btnRemoveItem.Visible = true;
                this.btnRemoveItem.Text = "Remove Item";
                this.btnRemoveItem.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveItem_Click);

                // Button btnLoadItem;
                this.btnLoadItem = new Button("btnLoadItem");
                this.btnLoadItem.Location = new Point(210, 110);
                this.btnLoadItem.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnLoadItem.Size = new Size(64, 16);
                this.btnLoadItem.Visible = true;
                this.btnLoadItem.Text = "Load Item";
                this.btnLoadItem.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadItem_Click);

                // Button btnChangeItem;
                this.btnChangeItem = new Button("btnChangeItem");
                this.btnChangeItem.Location = new Point(310, 110);
                this.btnChangeItem.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnChangeItem.Size = new Size(64, 16);
                this.btnChangeItem.Visible = true;
                this.btnChangeItem.Text = "Change Item";
                this.btnChangeItem.Click += new EventHandler<MouseButtonEventArgs>(this.BtnChangeItem_Click);

                this.chkBulkItem = new CheckBox("chkBulkItem");
                this.chkBulkItem.Size = new Size(100, 17);
                this.chkBulkItem.Location = new Point(410, 110);
                this.chkBulkItem.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.chkBulkItem.Text = "Bulk Edit";
                this.pnlRDungeonItems.AddWidget(this.lblItems);

                this.itemList = new List<EditableRDungeonItem>();
                this.pnlRDungeonItems.AddWidget(this.lbxItems);

                this.pnlRDungeonItems.AddWidget(this.lblItemNum);
                this.pnlRDungeonItems.AddWidget(this.nudItemNum);
                this.pnlRDungeonItems.AddWidget(this.lblMinValue);
                this.pnlRDungeonItems.AddWidget(this.nudMinValue);
                this.pnlRDungeonItems.AddWidget(this.lblMaxValue);
                this.pnlRDungeonItems.AddWidget(this.nudMaxValue);
                this.pnlRDungeonItems.AddWidget(this.lblItemSpawnRate);
                this.pnlRDungeonItems.AddWidget(this.nudItemSpawnRate);
                this.pnlRDungeonItems.AddWidget(this.lblStickyRate);
                this.pnlRDungeonItems.AddWidget(this.nudStickyRate);
                this.pnlRDungeonItems.AddWidget(this.lblTag);
                this.pnlRDungeonItems.AddWidget(this.txtTag);
                this.pnlRDungeonItems.AddWidget(this.chkHidden);
                this.pnlRDungeonItems.AddWidget(this.chkOnGround);
                this.pnlRDungeonItems.AddWidget(this.chkOnWater);
                this.pnlRDungeonItems.AddWidget(this.chkOnWall);
                this.pnlRDungeonItems.AddWidget(this.btnAddItem);
                this.pnlRDungeonItems.AddWidget(this.btnRemoveItem);
                this.pnlRDungeonItems.AddWidget(this.btnLoadItem);
                this.pnlRDungeonItems.AddWidget(this.btnChangeItem);
                this.pnlRDungeonItems.AddWidget(this.chkBulkItem);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonItems);
                this.itemsLoaded = true;
            }
        }

        private void LoadpnlRDungeonNpcs()
        {
            if (!this.npcsLoaded)
            {
                this.pnlRDungeonNpcs = new Panel("pnlRDungeonNpcs");
                this.pnlRDungeonNpcs.Size = new Size(600, 320);
                this.pnlRDungeonNpcs.Location = new Point(0, 80);
                this.pnlRDungeonNpcs.BackColor = Color.LightGray;
                this.pnlRDungeonNpcs.Visible = false;

                // Label lblNpcs;
                this.lblNpcs = new Label("lblNpcs");
                this.lblNpcs.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblNpcs.Text = "Npcs Settings";
                this.lblNpcs.AutoSize = true;
                this.lblNpcs.Location = new Point(10, 4);

                // ScrollingListBox lbxItems;
                this.lbxNpcs = new ScrollingListBox("lbxNpcs");
                this.lbxNpcs.Location = new Point(10, 150);
                this.lbxNpcs.Size = new Size(580, 140);

                // Label lblNpcSpawnTime;
                this.lblNpcSpawnTime = new Label("lblNpcSpawnTime");
                this.lblNpcSpawnTime.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblNpcSpawnTime.Text = "Spawn Time:";
                this.lblNpcSpawnTime.AutoSize = true;
                this.lblNpcSpawnTime.Location = new Point(100, 22);

                // NumericUpDown nudNpcSpawnTime;
                this.nudNpcSpawnTime = new NumericUpDown("nudNpcSpawnTime");
                this.nudNpcSpawnTime.Minimum = 1;
                this.nudNpcSpawnTime.Maximum = int.MaxValue;
                this.nudNpcSpawnTime.Size = new Size(80, 20);
                this.nudNpcSpawnTime.Location = new Point(100, 36);
                this.nudNpcSpawnTime.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblNpcMin = new Label("lblNpcMin");
                this.lblNpcMin.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblNpcMin.Text = "Min Npcs";
                this.lblNpcMin.AutoSize = true;
                this.lblNpcMin.Location = new Point(250, 20);

                // NumericUpDown nudNpcMin;
                this.nudNpcMin = new NumericUpDown("nudNpcMin");
                this.nudNpcMin.Minimum = 0;
                this.nudNpcMin.Maximum = MaxInfo.MAXMAPNPCS;
                this.nudNpcMin.Size = new Size(80, 20);
                this.nudNpcMin.Location = new Point(250, 36);
                this.nudNpcMin.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // Label lblNpcMax;
                this.lblNpcMax = new Label("lblNpcMax");
                this.lblNpcMax.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblNpcMax.Text = "Max Npcs";
                this.lblNpcMax.AutoSize = true;
                this.lblNpcMax.Location = new Point(400, 20);

                // NumericUpDown nudNpcMax;
                this.nudNpcMax = new NumericUpDown("nudNpcMax");
                this.nudNpcMax.Minimum = 0;
                this.nudNpcMax.Maximum = MaxInfo.MAXMAPNPCS;
                this.nudNpcMax.Size = new Size(80, 20);
                this.nudNpcMax.Location = new Point(400, 36);
                this.nudNpcMax.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // Label lblNpcNum;
                this.lblNpcNum = new Label("lblNpcNum");
                this.lblNpcNum.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblNpcNum.Text = "NPC #";
                this.lblNpcNum.AutoSize = true;
                this.lblNpcNum.Location = new Point(50, 60);

                // NumericUpDown nudNpcNum;
                this.nudNpcNum = new NumericUpDown("nudNpcNum");
                this.nudNpcNum.Minimum = 1;
                this.nudNpcNum.Maximum = MaxInfo.MaxItems;
                this.nudNpcNum.Size = new Size(80, 20);
                this.nudNpcNum.Location = new Point(50, 74);
                this.nudNpcNum.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.nudNpcNum.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudNpcNum_ValueChanged);

                // Label lblNpcSpawnRate;
                this.lblNpcSpawnRate = new Label("lblNpcSpawnRate");
                this.lblNpcSpawnRate.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblNpcSpawnRate.Text = "Spawn Rate:";
                this.lblNpcSpawnRate.AutoSize = true;
                this.lblNpcSpawnRate.Location = new Point(190, 60);

                // NumericUpDown nudNpcSpawnRate;
                this.nudNpcSpawnRate = new NumericUpDown("nudNpcSpawnRate");
                this.nudNpcSpawnRate.Minimum = 1;
                this.nudNpcSpawnRate.Maximum = 100;
                this.nudNpcSpawnRate.Size = new Size(80, 20);
                this.nudNpcSpawnRate.Location = new Point(190, 74);
                this.nudNpcSpawnRate.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblMinLevel = new Label("lblMinLevel");
                this.lblMinLevel.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblMinLevel.Text = "Min Level";
                this.lblMinLevel.AutoSize = true;
                this.lblMinLevel.Location = new Point(320, 60);

                // NumericUpDown nudMinLevel;
                this.nudMinLevel = new NumericUpDown("nudMinLevel");
                this.nudMinLevel.Minimum = 1;
                this.nudMinLevel.Maximum = 100;
                this.nudMinLevel.Size = new Size(80, 20);
                this.nudMinLevel.Location = new Point(320, 74);
                this.nudMinLevel.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // Label lblMaxLevel;
                this.lblMaxLevel = new Label("lblMaxLevel");
                this.lblMaxLevel.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblMaxLevel.Text = "Max Level";
                this.lblMaxLevel.AutoSize = true;
                this.lblMaxLevel.Location = new Point(450, 60);

                // NumericUpDown nudMaxLevel;
                this.nudMaxLevel = new NumericUpDown("nudMaxLevel");
                this.nudMaxLevel.Minimum = 1;
                this.nudMaxLevel.Maximum = 100;
                this.nudMaxLevel.Size = new Size(80, 20);
                this.nudMaxLevel.Location = new Point(450, 74);
                this.nudMaxLevel.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblNpcStartStatus = new Label("lblNpcStartStatus");
                this.lblNpcStartStatus.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblNpcStartStatus.AutoSize = true;
                this.lblNpcStartStatus.Location = new Point(190, 94);
                this.lblNpcStartStatus.Text = "Start Status:";

                this.cbNpcStartStatus = new ComboBox("cbNpcStartStatus");
                this.cbNpcStartStatus.Size = new Size(80, 20);
                this.cbNpcStartStatus.Location = new Point(190, 108);
                for (int i = 0; i < 6; i++)
                {
                    this.cbNpcStartStatus.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("Tahoma", 10), ((Enums.StatusAilment)i).ToString()));
                }

                this.cbNpcStartStatus.SelectItem(0);

                this.lblStatusCounter = new Label("lblStatusCounter");
                this.lblStatusCounter.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblStatusCounter.Text = "Status Counter";
                this.lblStatusCounter.AutoSize = true;
                this.lblStatusCounter.Location = new Point(320, 94);

                // NumericUpDown nudStatusCounter;
                this.nudStatusCounter = new NumericUpDown("nudStatusCounter");
                this.nudStatusCounter.Minimum = int.MinValue;
                this.nudStatusCounter.Maximum = int.MaxValue;
                this.nudStatusCounter.Size = new Size(80, 20);
                this.nudStatusCounter.Location = new Point(320, 108);
                this.nudStatusCounter.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // Label lblStatusChance;
                this.lblStatusChance = new Label("lblStatusChance");
                this.lblStatusChance.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblStatusChance.Text = "Status Chance";
                this.lblStatusChance.AutoSize = true;
                this.lblStatusChance.Location = new Point(450, 94);

                // NumericUpDown nudStatusChance;
                this.nudStatusChance = new NumericUpDown("nudStatusChance");
                this.nudStatusChance.Minimum = 1;
                this.nudStatusChance.Maximum = 100;
                this.nudStatusChance.Size = new Size(80, 20);
                this.nudStatusChance.Location = new Point(450, 108);
                this.nudStatusChance.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // Button btnAddNpc;
                this.btnAddNpc = new Button("btnAddNpc");
                this.btnAddNpc.Location = new Point(10, 130);
                this.btnAddNpc.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnAddNpc.Size = new Size(64, 16);
                this.btnAddNpc.Visible = true;
                this.btnAddNpc.Text = "Add Npc";
                this.btnAddNpc.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddNpc_Click);

                // Button btnRemoveNpc;
                this.btnRemoveNpc = new Button("btnRemoveNpc");
                this.btnRemoveNpc.Location = new Point(110, 130);
                this.btnRemoveNpc.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnRemoveNpc.Size = new Size(64, 16);
                this.btnRemoveNpc.Visible = true;
                this.btnRemoveNpc.Text = "Remove Npc";
                this.btnRemoveNpc.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveNpc_Click);

                // Button btnLoadNpc;
                this.btnLoadNpc = new Button("btnLoadNpc");
                this.btnLoadNpc.Location = new Point(210, 130);
                this.btnLoadNpc.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnLoadNpc.Size = new Size(64, 16);
                this.btnLoadNpc.Visible = true;
                this.btnLoadNpc.Text = "Load Npc";
                this.btnLoadNpc.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadNpc_Click);

                // Button btnChangeNpc;
                this.btnChangeNpc = new Button("btnChangeNpc");
                this.btnChangeNpc.Location = new Point(310, 130);
                this.btnChangeNpc.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnChangeNpc.Size = new Size(64, 16);
                this.btnChangeNpc.Visible = true;
                this.btnChangeNpc.Text = "Change Npc";
                this.btnChangeNpc.Click += new EventHandler<MouseButtonEventArgs>(this.BtnChangeNpc_Click);

                this.chkBulkNpc = new CheckBox("chkBulkNpc");
                this.chkBulkNpc.Size = new Size(100, 17);
                this.chkBulkNpc.Location = new Point(450, 130);
                this.chkBulkNpc.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.chkBulkNpc.Text = "Bulk Edit";
                this.pnlRDungeonNpcs.AddWidget(this.lblNpcs);

                this.pnlRDungeonNpcs.AddWidget(this.lblNpcSpawnTime);
                this.pnlRDungeonNpcs.AddWidget(this.nudNpcSpawnTime);

                this.pnlRDungeonNpcs.AddWidget(this.lblNpcMin);
                this.pnlRDungeonNpcs.AddWidget(this.nudNpcMin);
                this.pnlRDungeonNpcs.AddWidget(this.lblNpcMax);
                this.pnlRDungeonNpcs.AddWidget(this.nudNpcMax);

                this.npcList = new List<MapNpcSettings>();
                this.pnlRDungeonNpcs.AddWidget(this.lbxNpcs);

                this.pnlRDungeonNpcs.AddWidget(this.lblNpcNum);
                this.pnlRDungeonNpcs.AddWidget(this.nudNpcNum);
                this.pnlRDungeonNpcs.AddWidget(this.lblMinLevel);
                this.pnlRDungeonNpcs.AddWidget(this.nudMinLevel);
                this.pnlRDungeonNpcs.AddWidget(this.lblMaxLevel);
                this.pnlRDungeonNpcs.AddWidget(this.nudMaxLevel);
                this.pnlRDungeonNpcs.AddWidget(this.lblNpcSpawnRate);
                this.pnlRDungeonNpcs.AddWidget(this.nudNpcSpawnRate);
                this.pnlRDungeonNpcs.AddWidget(this.lblNpcStartStatus);
                this.pnlRDungeonNpcs.AddWidget(this.cbNpcStartStatus);
                this.pnlRDungeonNpcs.AddWidget(this.lblStatusCounter);
                this.pnlRDungeonNpcs.AddWidget(this.nudStatusCounter);
                this.pnlRDungeonNpcs.AddWidget(this.lblStatusChance);
                this.pnlRDungeonNpcs.AddWidget(this.nudStatusChance);
                this.pnlRDungeonNpcs.AddWidget(this.btnAddNpc);
                this.pnlRDungeonNpcs.AddWidget(this.btnRemoveNpc);
                this.pnlRDungeonNpcs.AddWidget(this.btnLoadNpc);
                this.pnlRDungeonNpcs.AddWidget(this.btnChangeNpc);
                this.pnlRDungeonNpcs.AddWidget(this.chkBulkNpc);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonNpcs);
                this.npcsLoaded = true;
            }
        }

        private void LoadpnlRDungeonTraps()
        {
            if (!this.trapsLoaded)
            {
                this.pnlRDungeonTraps = new Panel("pnlRDungeonTraps");
                this.pnlRDungeonTraps.Size = new Size(600, 320);
                this.pnlRDungeonTraps.Location = new Point(0, 80);
                this.pnlRDungeonTraps.BackColor = Color.LightGray;
                this.pnlRDungeonTraps.Visible = false;

                // Label lblTraps;
                this.lblTraps = new Label("lblTraps");
                this.lblTraps.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblTraps.Text = "Traps Settings";
                this.lblTraps.AutoSize = true;
                this.lblTraps.Location = new Point(10, 4);

                // ScrollingListBox lbxTraps;
                this.lbxTraps = new ScrollingListBox("lbxTraps");
                this.lbxTraps.Location = new Point(10, 170);
                this.lbxTraps.Size = new Size(580, 120);

                this.lblTrapTileset = new Label[10];
                this.picTrapTileset = new PictureBox[10];
                for (int i = 0; i < 10; i++)
                {
                    this.lblTrapTileset[i] = new Label("lblTrapTileset" + i);
                    this.lblTrapTileset[i].Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                    this.lblTrapTileset[i].AutoSize = true;

                    this.lblTrapTileset[i].Location = new Point(2 + (i * 60), 22);

                    this.picTrapTileset[i] = new PictureBox("picTrapTileset" + i);
                    this.picTrapTileset[i].Size = new Size(32, 32);
                    this.picTrapTileset[i].BackColor = Color.Transparent;
                    this.picTrapTileset[i].Image = new SdlDotNet.Graphics.Surface(32, 32);
                    this.picTrapTileset[i].Click += new EventHandler<MouseButtonEventArgs>(this.PicTrapTileset_Click);

                    this.picTrapTileset[i].Location = new Point(10 + (i * 60), 38);

                    this.pnlRDungeonTraps.AddWidget(this.lblTrapTileset[i]);
                    this.pnlRDungeonTraps.AddWidget(this.picTrapTileset[i]);
                }

                this.lblTrapTileset[0].Text = "Ground";
                this.lblTrapTileset[1].Text = "GAnim";
                this.lblTrapTileset[2].Text = "Mask";
                this.lblTrapTileset[3].Text = "MAnim";
                this.lblTrapTileset[4].Text = "Mask2";
                this.lblTrapTileset[5].Text = "M2Anim";
                this.lblTrapTileset[6].Text = "Fringe";
                this.lblTrapTileset[7].Text = "FAnim";
                this.lblTrapTileset[8].Text = "Fringe2";
                this.lblTrapTileset[9].Text = "F2Anim";

                this.cbTrapType = new ComboBox("cbTrapType");
                this.cbTrapType.Location = new Point(10, 80);
                this.cbTrapType.Size = new Size(120, 16);
                for (int i = 0; i < 40; i++)
                {
                    this.cbTrapType.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), i + ": " + Enum.GetName(typeof(Enums.TileType), i)));
                }

                this.cbTrapType.SelectItem(0);

                this.lblTrapChance = new Label("lblTrapChance");
                this.lblTrapChance.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblTrapChance.Text = "Trap Chance:";
                this.lblTrapChance.AutoSize = true;
                this.lblTrapChance.Location = new Point(10, 106);

                this.nudTrapChance = new NumericUpDown("nudTrapChance");
                this.nudTrapChance.Minimum = 1;
                this.nudTrapChance.Maximum = 100;
                this.nudTrapChance.Size = new Size(80, 20);
                this.nudTrapChance.Location = new Point(10, 120);
                this.nudTrapChance.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblTrapData1 = new Label("lblTrapData1");
                this.lblTrapData1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblTrapData1.Text = "Trap Data1:";
                this.lblTrapData1.AutoSize = true;
                this.lblTrapData1.Location = new Point(310, 76);

                this.nudTrapData1 = new NumericUpDown("nudTrapData1");
                this.nudTrapData1.Minimum = int.MinValue;
                this.nudTrapData1.Maximum = int.MaxValue;
                this.nudTrapData1.Size = new Size(80, 20);
                this.nudTrapData1.Location = new Point(310, 90);
                this.nudTrapData1.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblTrapData2 = new Label("lblTrapData2");
                this.lblTrapData2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblTrapData2.Text = "Trap Data2:";
                this.lblTrapData2.AutoSize = true;
                this.lblTrapData2.Location = new Point(410, 76);

                this.nudTrapData2 = new NumericUpDown("nudTrapData2");
                this.nudTrapData2.Minimum = int.MinValue;
                this.nudTrapData2.Maximum = int.MaxValue;
                this.nudTrapData2.Size = new Size(80, 20);
                this.nudTrapData2.Location = new Point(410, 90);
                this.nudTrapData2.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblTrapData3 = new Label("lblTrapData3");
                this.lblTrapData3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblTrapData3.Text = "Trap Data3:";
                this.lblTrapData3.AutoSize = true;
                this.lblTrapData3.Location = new Point(510, 76);

                this.nudTrapData3 = new NumericUpDown("nudTrapData3");
                this.nudTrapData3.Minimum = int.MinValue;
                this.nudTrapData3.Maximum = int.MaxValue;
                this.nudTrapData3.Size = new Size(80, 20);
                this.nudTrapData3.Location = new Point(510, 90);
                this.nudTrapData3.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblTrapString1 = new Label("lblTrapString1");
                this.lblTrapString1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblTrapString1.Text = "Trap String1:";
                this.lblTrapString1.AutoSize = true;
                this.lblTrapString1.Location = new Point(310, 120);

                this.txtTrapString1 = new TextBox("txtTrapString1");
                this.txtTrapString1.BackColor = Color.White;
                this.txtTrapString1.Size = new Size(80, 20);
                this.txtTrapString1.Location = new Point(310, 134);
                this.txtTrapString1.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblTrapString2 = new Label("lblTrapString2");
                this.lblTrapString2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblTrapString2.Text = "Trap String2:";
                this.lblTrapString2.AutoSize = true;
                this.lblTrapString2.Location = new Point(410, 120);

                this.txtTrapString2 = new TextBox("txtTrapString2");
                this.txtTrapString2.BackColor = Color.White;
                this.txtTrapString2.Size = new Size(80, 20);
                this.txtTrapString2.Location = new Point(410, 134);
                this.txtTrapString2.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.lblTrapString3 = new Label("lblTrapString3");
                this.lblTrapString3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblTrapString3.Text = "Trap String3:";
                this.lblTrapString3.AutoSize = true;
                this.lblTrapString3.Location = new Point(510, 120);

                this.txtTrapString3 = new TextBox("txtTrapString3");
                this.txtTrapString3.BackColor = Color.White;
                this.txtTrapString3.Size = new Size(80, 20);
                this.txtTrapString3.Location = new Point(510, 134);
                this.txtTrapString3.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // Button btnAddTrap;
                this.btnAddTrap = new Button("btnAddTrap");
                this.btnAddTrap.Location = new Point(220, 150);
                this.btnAddTrap.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnAddTrap.Size = new Size(64, 16);
                this.btnAddTrap.Visible = true;
                this.btnAddTrap.Text = "Add Trap";
                this.btnAddTrap.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddTrap_Click);

                // Button btnRemoveTrap;
                this.btnRemoveTrap = new Button("btnRemoveTrap");
                this.btnRemoveTrap.Location = new Point(150, 150);
                this.btnRemoveTrap.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnRemoveTrap.Size = new Size(64, 16);
                this.btnRemoveTrap.Visible = true;
                this.btnRemoveTrap.Text = "Remove Trap";
                this.btnRemoveTrap.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveTrap_Click);

                // Button btnLoadTrap;
                this.btnLoadTrap = new Button("btnLoadTrap");
                this.btnLoadTrap.Location = new Point(80, 150);
                this.btnLoadTrap.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnLoadTrap.Size = new Size(64, 16);
                this.btnLoadTrap.Visible = true;
                this.btnLoadTrap.Text = "Load Trap";
                this.btnLoadTrap.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadTrap_Click);

                // Button btnChangeTrap;
                this.btnChangeTrap = new Button("btnChangeTrap");
                this.btnChangeTrap.Location = new Point(10, 150);
                this.btnChangeTrap.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnChangeTrap.Size = new Size(64, 16);
                this.btnChangeTrap.Visible = true;
                this.btnChangeTrap.Text = "Change Trap";
                this.btnChangeTrap.Click += new EventHandler<MouseButtonEventArgs>(this.BtnChangeTrap_Click);
                this.pnlRDungeonTraps.AddWidget(this.lblTraps);

                this.trapList = new List<EditableRDungeonTrap>();
                this.pnlRDungeonTraps.AddWidget(this.lbxTraps);
                this.trapTileNumbers = new int[2, 10];

                this.pnlRDungeonTraps.AddWidget(this.cbTrapType);
                this.pnlRDungeonTraps.AddWidget(this.lblTrapData1);
                this.pnlRDungeonTraps.AddWidget(this.nudTrapData1);
                this.pnlRDungeonTraps.AddWidget(this.lblTrapData2);
                this.pnlRDungeonTraps.AddWidget(this.nudTrapData2);
                this.pnlRDungeonTraps.AddWidget(this.lblTrapData3);
                this.pnlRDungeonTraps.AddWidget(this.nudTrapData3);
                this.pnlRDungeonTraps.AddWidget(this.lblTrapString1);
                this.pnlRDungeonTraps.AddWidget(this.txtTrapString1);
                this.pnlRDungeonTraps.AddWidget(this.lblTrapString2);
                this.pnlRDungeonTraps.AddWidget(this.txtTrapString2);
                this.pnlRDungeonTraps.AddWidget(this.lblTrapString3);
                this.pnlRDungeonTraps.AddWidget(this.txtTrapString3);

                this.pnlRDungeonTraps.AddWidget(this.lblTrapChance);
                this.pnlRDungeonTraps.AddWidget(this.nudTrapChance);

                this.pnlRDungeonTraps.AddWidget(this.btnAddTrap);
                this.pnlRDungeonTraps.AddWidget(this.btnRemoveTrap);
                this.pnlRDungeonTraps.AddWidget(this.btnLoadTrap);
                this.pnlRDungeonTraps.AddWidget(this.btnChangeTrap);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonTraps);
                this.trapsLoaded = true;
            }
        }

        private void LoadpnlRDungeonWeather()
        {
            if (!this.weatherLoaded)
            {
                this.pnlRDungeonWeather = new Panel("pnlRDungeonWeather");
                this.pnlRDungeonWeather.Size = new Size(600, 320);
                this.pnlRDungeonWeather.Location = new Point(0, 80);
                this.pnlRDungeonWeather.BackColor = Color.LightGray;
                this.pnlRDungeonWeather.Visible = false;

                // Label lblWeather;
                this.lblWeather = new Label("lblWeather");
                this.lblWeather.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblWeather.Text = "Weather Settings";
                this.lblWeather.AutoSize = true;
                this.lblWeather.Location = new Point(10, 4);

                // ScrollingListBox lbxWeather;
                this.lbxWeather = new ScrollingListBox("lbxWeather");
                this.lbxWeather.Location = new Point(300, 26);
                this.lbxWeather.Size = new Size(260, 260);

                this.cbWeather = new ComboBox("cbWeather");
                this.cbWeather.Location = new Point(10, 26);
                this.cbWeather.Size = new Size(200, 16);

                // ComboBox cbWeather;
                for (int i = 0; i < 13; i++)
                {
                    // lbiItem = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), );
                    this.cbWeather.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), i + ": " + Enum.GetName(typeof(Enums.Weather), i)));
                }

                this.cbWeather.SelectItem(0);

                this.btnAddWeather = new Button("btnAddWeather");
                this.btnAddWeather.Location = new Point(10, 70);
                this.btnAddWeather.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnAddWeather.Size = new Size(80, 16);
                this.btnAddWeather.Visible = true;
                this.btnAddWeather.Text = "Add Weather";
                this.btnAddWeather.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddWeather_Click);

                this.btnRemoveWeather = new Button("btnRemoveWeather");
                this.btnRemoveWeather.Location = new Point(120, 70);
                this.btnRemoveWeather.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnRemoveWeather.Size = new Size(80, 16);
                this.btnRemoveWeather.Visible = true;
                this.btnRemoveWeather.Text = "Remove Weather";
                this.btnRemoveWeather.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveWeather_Click);

                // ComboBox cbWeather;
                // Button btnAddWeather;
                // Button btnRemoveWeather;
                this.pnlRDungeonWeather.AddWidget(this.lblWeather);
                this.pnlRDungeonWeather.AddWidget(this.lbxWeather);
                this.pnlRDungeonWeather.AddWidget(this.cbWeather);
                this.pnlRDungeonWeather.AddWidget(this.btnAddWeather);
                this.pnlRDungeonWeather.AddWidget(this.btnRemoveWeather);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonWeather);
                this.weatherLoaded = true;
            }
        }

        private void LoadpnlRDungeonGoal()
        {
            if (!this.goalLoaded)
            {
                this.pnlRDungeonGoal = new Panel("pnlRDungeonGoal");
                this.pnlRDungeonGoal.Size = new Size(600, 320);
                this.pnlRDungeonGoal.Location = new Point(0, 80);
                this.pnlRDungeonGoal.BackColor = Color.LightGray;
                this.pnlRDungeonGoal.Visible = false;

                // Label lblGoal;
                this.lblGoal = new Label("lblGoal");
                this.lblGoal.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblGoal.Text = "Goal Settings";
                this.lblGoal.AutoSize = true;
                this.lblGoal.Location = new Point(10, 4);

                // RadioButton optNextFloor;
                this.optNextFloor = new RadioButton("optNextFloor");
                this.optNextFloor.BackColor = Color.Transparent;
                this.optNextFloor.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.optNextFloor.Location = new Point(10, 24);
                this.optNextFloor.Size = new Size(95, 17);
                this.optNextFloor.Text = "Next Floor";
                this.optNextFloor.CheckChanged += new EventHandler(this.OptNextFloor_Checked);

                // RadioButton optMap;
                this.optMap = new RadioButton("optMap");
                this.optMap.BackColor = Color.Transparent;
                this.optMap.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.optMap.Location = new Point(10, 44);
                this.optMap.Size = new Size(95, 17);
                this.optMap.Text = "Map";
                this.optMap.CheckChanged += new EventHandler(this.OptMap_Checked);

                // RadioButton optScripted;
                this.optScripted = new RadioButton("optScripted");
                this.optScripted.BackColor = Color.Transparent;
                this.optScripted.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.optScripted.Location = new Point(10, 64);
                this.optScripted.Size = new Size(95, 17);
                this.optScripted.Text = "Script";
                this.optScripted.CheckChanged += new EventHandler(this.OptScripted_Checked);

                // Label lblData1;
                this.lblData1 = new Label("lblData1");
                this.lblData1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblData1.Text = "Data1:";
                this.lblData1.AutoSize = true;
                this.lblData1.Location = new Point(10, 94);

                // Label lblData2;
                this.lblData2 = new Label("lblData2");
                this.lblData2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblData2.Text = "Data2:";
                this.lblData2.AutoSize = true;
                this.lblData2.Location = new Point(10, 114);

                // Label lblData3;
                this.lblData3 = new Label("lblData3");
                this.lblData3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblData3.Text = "Data3:";
                this.lblData3.AutoSize = true;
                this.lblData3.Location = new Point(10, 134);

                // NumericUpDown nudData1;
                this.nudData1 = new NumericUpDown("nudData1");
                this.nudData1.Minimum = int.MinValue;
                this.nudData1.Maximum = int.MaxValue;
                this.nudData1.Size = new Size(80, 20);
                this.nudData1.Location = new Point(100, 94);
                this.nudData1.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // NumericUpDown nudData2;
                this.nudData2 = new NumericUpDown("nudData2");
                this.nudData2.Minimum = int.MinValue;
                this.nudData2.Maximum = int.MaxValue;
                this.nudData2.Size = new Size(80, 20);
                this.nudData2.Location = new Point(100, 114);
                this.nudData2.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // NumericUpDown nudData3;
                this.nudData3 = new NumericUpDown("nudData3");
                this.nudData3.Minimum = int.MinValue;
                this.nudData3.Maximum = int.MaxValue;
                this.nudData3.Size = new Size(80, 20);
                this.nudData3.Location = new Point(100, 134);
                this.nudData3.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.pnlRDungeonGoal.AddWidget(this.lblGoal);
                this.pnlRDungeonGoal.AddWidget(this.optNextFloor);
                this.pnlRDungeonGoal.AddWidget(this.optMap);
                this.pnlRDungeonGoal.AddWidget(this.optScripted);
                this.pnlRDungeonGoal.AddWidget(this.lblData1);
                this.pnlRDungeonGoal.AddWidget(this.lblData2);
                this.pnlRDungeonGoal.AddWidget(this.lblData3);
                this.pnlRDungeonGoal.AddWidget(this.nudData1);
                this.pnlRDungeonGoal.AddWidget(this.nudData2);
                this.pnlRDungeonGoal.AddWidget(this.nudData3);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonGoal);
                this.goalLoaded = true;
            }
        }

        private void LoadpnlRDungeonChambers()
        {
            if (!this.chambersLoaded)
            {
                this.pnlRDungeonChambers = new Panel("pnlRDungeonChambers");
                this.pnlRDungeonChambers.Size = new Size(600, 320);
                this.pnlRDungeonChambers.Location = new Point(0, 80);
                this.pnlRDungeonChambers.BackColor = Color.LightGray;
                this.pnlRDungeonChambers.Visible = false;

                // Label lblChambers;
                this.lblChambers = new Label("lblChambers");
                this.lblChambers.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblChambers.Text = "Chambers Settings";
                this.lblChambers.AutoSize = true;
                this.lblChambers.Location = new Point(10, 4);

                // ScrollingListBox lbxChambers;
                this.lbxChambers = new ScrollingListBox("lbxChambers");
                this.lbxChambers.Location = new Point(300, 26);
                this.lbxChambers.Size = new Size(260, 260);

                // lblChamberNum
                this.lblChamberNum = new Label("lblChamberNum");
                this.lblChamberNum.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblChamberNum.Text = "Chamber Num:";
                this.lblChamberNum.AutoSize = true;
                this.lblChamberNum.Location = new Point(10, 22);

                // NumericUpDown Chamber;
                this.nudChamber = new NumericUpDown("nudChamber");
                this.nudChamber.Minimum = int.MinValue;
                this.nudChamber.Maximum = int.MaxValue;
                this.nudChamber.Size = new Size(200, 16);
                this.nudChamber.Location = new Point(10, 36);
                this.nudChamber.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // lblChamberString1
                this.lblChamberString1 = new Label("lblChamberString1");
                this.lblChamberString1.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblChamberString1.Text = "Chamber String 1:";
                this.lblChamberString1.AutoSize = true;
                this.lblChamberString1.Location = new Point(10, 58);

                // Textbox ChamberString1;
                this.txtChamberString1 = new TextBox("txtChamberString1");
                this.txtChamberString1.BackColor = Color.White;
                this.txtChamberString1.Size = new Size(80, 20);
                this.txtChamberString1.Location = new Point(10, 72);
                this.txtChamberString1.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // lblChamberString2
                this.lblChamberString2 = new Label("lblChamberString2");
                this.lblChamberString2.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblChamberString2.Text = "Chamber String 2";
                this.lblChamberString2.AutoSize = true;
                this.lblChamberString2.Location = new Point(100, 58);

                // Textbox ChamberString2;
                this.txtChamberString2 = new TextBox("txtChamberString2");
                this.txtChamberString2.BackColor = Color.White;
                this.txtChamberString2.Size = new Size(80, 20);
                this.txtChamberString2.Location = new Point(100, 72);
                this.txtChamberString2.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // lblChamberString3
                this.lblChamberString3 = new Label("lblChamberString3");
                this.lblChamberString3.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblChamberString3.Text = "Chamber String 3:";
                this.lblChamberString3.AutoSize = true;
                this.lblChamberString3.Location = new Point(190, 58);

                // Textbox ChamberString3;
                this.txtChamberString3 = new TextBox("txtChamberString3");
                this.txtChamberString3.BackColor = Color.White;
                this.txtChamberString3.Size = new Size(80, 20);
                this.txtChamberString3.Location = new Point(190, 72);
                this.txtChamberString3.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                this.btnAddChamber = new Button("btnAddChamber");
                this.btnAddChamber.Location = new Point(10, 120);
                this.btnAddChamber.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnAddChamber.Size = new Size(80, 16);
                this.btnAddChamber.Visible = true;
                this.btnAddChamber.Text = "Add Chamber";
                this.btnAddChamber.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAddChamber_Click);

                this.btnRemoveChamber = new Button("btnRemoveChamber");
                this.btnRemoveChamber.Location = new Point(120, 120);
                this.btnRemoveChamber.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnRemoveChamber.Size = new Size(80, 16);
                this.btnRemoveChamber.Visible = true;
                this.btnRemoveChamber.Text = "Remove Chamber";
                this.btnRemoveChamber.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRemoveChamber_Click);

                // ComboBox cbChamber;
                // Button btnAddChamber;
                // Button btnRemoveChamber;
                this.pnlRDungeonChambers.AddWidget(this.lblChambers);

                this.chamberList = new List<EditableRDungeonChamber>();
                this.pnlRDungeonChambers.AddWidget(this.lbxChambers);
                this.pnlRDungeonChambers.AddWidget(this.lblChamberNum);
                this.pnlRDungeonChambers.AddWidget(this.nudChamber);
                this.pnlRDungeonChambers.AddWidget(this.lblChamberString1);
                this.pnlRDungeonChambers.AddWidget(this.txtChamberString1);
                this.pnlRDungeonChambers.AddWidget(this.lblChamberString2);
                this.pnlRDungeonChambers.AddWidget(this.txtChamberString2);
                this.pnlRDungeonChambers.AddWidget(this.lblChamberString3);
                this.pnlRDungeonChambers.AddWidget(this.txtChamberString3);
                this.pnlRDungeonChambers.AddWidget(this.btnAddChamber);
                this.pnlRDungeonChambers.AddWidget(this.btnRemoveChamber);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonChambers);
                this.chambersLoaded = true;
            }
        }

        private void LoadpnlRDungeonMisc()
        {
            if (!this.miscLoaded)
            {
                this.pnlRDungeonMisc = new Panel("pnlRDungeonMisc");
                this.pnlRDungeonMisc.Size = new Size(600, 320);
                this.pnlRDungeonMisc.Location = new Point(0, 80);
                this.pnlRDungeonMisc.BackColor = Color.LightGray;
                this.pnlRDungeonMisc.Visible = false;

                // Label lblMisc;
                this.lblMisc = new Label("lblMisc");
                this.lblMisc.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblMisc.Text = "Misc Settings";
                this.lblMisc.AutoSize = true;
                this.lblMisc.Location = new Point(10, 4);

                // Label lblDarkness;
                this.lblDarkness = new Label("lblDarkness");
                this.lblDarkness.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblDarkness.Text = "Darkness Settings";
                this.lblDarkness.AutoSize = true;
                this.lblDarkness.Location = new Point(10, 30);

                // NumericUpDown nudDarkness;
                this.nudDarkness = new NumericUpDown("nudDarkness");
                this.nudDarkness.Minimum = int.MinValue;
                this.nudDarkness.Maximum = int.MaxValue;
                this.nudDarkness.Size = new Size(80, 20);
                this.nudDarkness.Location = new Point(100, 30);
                this.nudDarkness.Font = Graphic.FontManager.LoadFont("tahoma", 10);

                // Label lblMusic;
                this.lblMusic = new Label("lblMusic");
                this.lblMusic.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblMusic.Text = "Music";
                this.lblMusic.AutoSize = true;
                this.lblMusic.Location = new Point(10, 60);

                // ScrollingListBox lbxMusic;
                this.lbxMusic = new ScrollingListBox("lbxMusic");
                this.lbxMusic.Location = new Point(10, 76);
                this.lbxMusic.Size = new Size(260, 220);
                this.lbxMusic.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("Tahoma", 10), "None"));
                string[] songs = System.IO.Directory.GetFiles(IO.Paths.MusicPath);
                for (int i = 0; i < songs.Length; i++)
                {
                    this.lbxMusic.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("Tahoma", 10), System.IO.Path.GetFileName(songs[i])));
                }

                // Button btnPlayMusic;
                this.btnPlayMusic = new Button("btnPlayMusic");
                this.btnPlayMusic.Location = new Point(90, 60);
                this.btnPlayMusic.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnPlayMusic.Size = new Size(80, 16);
                this.btnPlayMusic.Visible = true;
                this.btnPlayMusic.Text = "Play Song";
                this.btnPlayMusic.Click += new EventHandler<MouseButtonEventArgs>(this.BtnPlayMusic_Click);
                this.pnlRDungeonMisc.AddWidget(this.lblMisc);
                this.pnlRDungeonMisc.AddWidget(this.lblMusic);
                this.pnlRDungeonMisc.AddWidget(this.lbxMusic);
                this.pnlRDungeonMisc.AddWidget(this.btnPlayMusic);
                this.pnlRDungeonMisc.AddWidget(this.lblDarkness);
                this.pnlRDungeonMisc.AddWidget(this.nudDarkness);
                this.pnlRDungeonFloors.AddWidget(this.pnlRDungeonMisc);
                this.miscLoaded = true;
            }
        }

        private void LoadpnlTileSelector()
        {
            if (!this.tileSelectorLoaded)
            {
                this.pnlTileSelector = new Panel("pnlTileSelector");
                this.pnlTileSelector.Size = new Size(600, 400);
                this.pnlTileSelector.Location = new Point(0, 0);
                this.pnlTileSelector.BackColor = Color.LightGray;
                this.pnlTileSelector.Visible = false;
                this.tileSelector = new Widges.TilesetViewer("TileSelector");
                this.tileSelector.Location = new Point(0, 32);
                this.tileSelector.Size = new Size(458, 350);
                this.tileSelector.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[0];

                this.lblTileSet = new Label("lblTileSet");
                this.lblTileSet.Font = Graphic.FontManager.LoadFont("Tahoma", 10);
                this.lblTileSet.Text = "TileSet Settings";
                this.lblTileSet.AutoSize = true;
                this.lblTileSet.Location = new Point(10, 4);

                this.nudTileSet = new NumericUpDown("nudTileSet");
                this.nudTileSet.Minimum = 0;
                this.nudTileSet.Maximum = 10;
                this.nudTileSet.Size = new Size(80, 20);
                this.nudTileSet.Location = new Point(100, 4);
                this.nudTileSet.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.nudTileSet.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.NudTileSet_ValueChanged);

                this.btnTileSetOK = new Button("btnTileSetOK");
                this.btnTileSetOK.Location = new Point(200, 4);
                this.btnTileSetOK.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnTileSetOK.Size = new Size(80, 16);
                this.btnTileSetOK.Visible = true;
                this.btnTileSetOK.Text = "OK";
                this.btnTileSetOK.Click += new EventHandler<MouseButtonEventArgs>(this.BtnTileSetOK_Click);

                this.btnTileSetCancel = new Button("btnTileSetCancel");
                this.btnTileSetCancel.Location = new Point(300, 4);
                this.btnTileSetCancel.Font = Graphic.FontManager.LoadFont("tahoma", 10);
                this.btnTileSetCancel.Size = new Size(80, 16);
                this.btnTileSetCancel.Visible = true;
                this.btnTileSetCancel.Text = "Cancel";
                this.btnTileSetCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnTileSetCancel_Click);
                this.pnlTileSelector.AddWidget(this.tileSelector);
                this.pnlTileSelector.AddWidget(this.lblTileSet);
                this.pnlTileSelector.AddWidget(this.nudTileSet);
                this.pnlTileSelector.AddWidget(this.btnTileSetOK);
                this.pnlTileSelector.AddWidget(this.btnTileSetCancel);
                this.pnlRDungeonFloors.AddWidget(this.pnlTileSelector);
                this.tileSelectorLoaded = true;
            }
        }

        private void BtnBack_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen > 0)
            {
                this.currentTen--;
            }

            this.RefreshRDungeonList();
        }

        private void BtnForward_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.currentTen < (MaxInfo.MaxRDungeons / 10))
            {
                this.currentTen++;
            }

            this.RefreshRDungeonList();
        }

        private void BtnEdit_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxRDungeonList.SelectedItems.Count == 1)
            {
                string[] index = ((ListBoxTextItem)this.lbxRDungeonList.SelectedItems[0]).Text.Split(':');
                if (index[0].IsNumeric())
                {
                    // Messenger.SendEditRDungeon(index[0].ToInt());
                    this.btnEdit.Text = "Loading...";

                    this.LoadpnlRDungeonGeneral();

                    Messenger.SendEditRDungeon(index[0].ToInt() - 1);
                }
            }
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            return;
        }

        private void BtnAddNew_Click(object sender, MouseButtonEventArgs e)
        {
            Messenger.SendAddRDungeon();
        }

        private void BtnFloors_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonFloors();
            this.LoadpnlRDungeonFloorSettingSelection();

            this.pnlRDungeonGeneral.Visible = false;
            this.pnlRDungeonFloors.Visible = true;
            if (this.pnlRDungeonStructure != null)
            {
                this.pnlRDungeonStructure.Visible = false;
            }

            if (this.pnlRDungeonLandTiles != null)
            {
                this.pnlRDungeonLandTiles.Visible = false;
            }

            if (this.pnlRDungeonLandAltTiles != null)
            {
                this.pnlRDungeonLandAltTiles.Visible = false;
            }

            if (this.pnlRDungeonWaterTiles != null)
            {
                this.pnlRDungeonWaterTiles.Visible = false;
            }

            if (this.pnlRDungeonWaterAnimTiles != null)
            {
                this.pnlRDungeonWaterAnimTiles.Visible = false;
            }

            if (this.pnlRDungeonAttributes != null)
            {
                this.pnlRDungeonAttributes.Visible = false;
            }

            if (this.pnlRDungeonItems != null)
            {
                this.pnlRDungeonItems.Visible = false;
            }

            if (this.pnlRDungeonNpcs != null)
            {
                this.pnlRDungeonNpcs.Visible = false;
            }

            if (this.pnlRDungeonTraps != null)
            {
                this.pnlRDungeonTraps.Visible = false;
            }

            if (this.pnlRDungeonWeather != null)
            {
                this.pnlRDungeonWeather.Visible = false;
            }

            if (this.pnlRDungeonGoal != null)
            {
                this.pnlRDungeonGoal.Visible = false;
            }

            if (this.pnlRDungeonChambers != null)
            {
                this.pnlRDungeonChambers.Visible = false;
            }

            if (this.pnlRDungeonMisc != null)
            {
                this.pnlRDungeonMisc.Visible = false;
            }

            if (this.pnlTileSelector != null)
            {
                this.pnlTileSelector.Visible = false;
            }

            this.pnlRDungeonFloorSettingSelection.Visible = true;
            this.btnSaveFloor.Text = "Save All Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load All Settings from Floor";

            this.Size = new Size(this.pnlRDungeonFloors.Width, this.pnlRDungeonFloors.Height);
            this.TitleBar.Text = "Random Dungeon Floor Options";
        }

        private void BtnEditorCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.rdungeon = null;
            this.pnlRDungeonGeneral.Visible = false;
            this.pnlRDungeonList.Visible = true;
            this.Size = new Size(this.pnlRDungeonList.Width, this.pnlRDungeonList.Height);
            this.TitleBar.Text = "Random Dungeon Panel";
        }

        private void BtnSave_Click(object sender, MouseButtonEventArgs e)
        {
            this.rdungeon.DungeonName = this.txtDungeonName.Text;
            if (this.optUp.Checked)
            {
                this.rdungeon.Direction = Enums.Direction.Up;
            }
else
            {
                this.rdungeon.Direction = Enums.Direction.Down;
            }

            this.rdungeon.MaxFloors = this.nudMaxFloors.Value;
            this.rdungeon.Recruitment = this.chkRecruiting.Checked;
            this.rdungeon.Exp = this.chkEXPGain.Checked;
            this.rdungeon.WindTimer = this.nudWindTimer.Value;

            Messenger.SendSaveRDungeon(this.rdungeon);

            this.rdungeon = null;
            this.pnlRDungeonGeneral.Visible = false;
            this.pnlRDungeonList.Visible = true;
            this.Size = new Size(this.pnlRDungeonList.Width, this.pnlRDungeonList.Height);
            this.TitleBar.Text = "Random Dungeon Panel";
        }

        private void BtnGeneral_Click(object sender, MouseButtonEventArgs e)
        {
            // if (!btnGeneral.Selected) {
            // btnGeneral.Selected = true;
            // btnFloors.Selected = false;
            // btnTerrain.Selected = false;
            // btnGeneral.Size = new Size((pnlRDungeonGeneral.Width - 21) / 3, 32);
            // btnFloors.Location = new Point((pnlRDungeonGeneral.Width - 21) / 3 + 10, 0);
            // btnFloors.Size = new Size((pnlRDungeonGeneral.Width - 21) / 3, 32);
            // btnTerrain.Location = new Point((pnlRDungeonGeneral.Width - 21) / 3 * 2 + 10, 0);
            // btnTerrain.Size = new Size((pnlRDungeonGeneral.Width - 21) / 3, 32);
            this.pnlRDungeonGeneral.Visible = true;
            this.pnlRDungeonFloors.Visible = false;

            // pnlRDungeonTerrain.Visible = false;
            this.Size = new Size(this.pnlRDungeonGeneral.Width, this.pnlRDungeonGeneral.Height + 32);
            this.TitleBar.Text = "General Random Dungeon Options";

            // }
        }

        private void NudFirstFloor_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.nudFirstFloor.Value > this.nudLastFloor.Value)
            {
                this.nudLastFloor.Value = this.nudFirstFloor.Value;
            }
        }

        private void NudLastFloor_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.nudFirstFloor.Value > this.nudLastFloor.Value)
            {
                this.nudFirstFloor.Value = this.nudLastFloor.Value;
            }
        }

        private void BtnSettingsMenu_Click(object sender, MouseButtonEventArgs e)
        {
            this.pnlRDungeonGeneral.Visible = false;
            this.pnlRDungeonFloors.Visible = true;
            if (this.pnlRDungeonStructure != null)
            {
                this.pnlRDungeonStructure.Visible = false;
            }

            if (this.pnlRDungeonLandTiles != null)
            {
                this.pnlRDungeonLandTiles.Visible = false;
            }

            if (this.pnlRDungeonLandAltTiles != null)
            {
                this.pnlRDungeonLandAltTiles.Visible = false;
            }

            if (this.pnlRDungeonWaterTiles != null)
            {
                this.pnlRDungeonWaterTiles.Visible = false;
            }

            if (this.pnlRDungeonWaterAnimTiles != null)
            {
                this.pnlRDungeonWaterAnimTiles.Visible = false;
            }

            if (this.pnlRDungeonAttributes != null)
            {
                this.pnlRDungeonAttributes.Visible = false;
            }

            if (this.pnlRDungeonItems != null)
            {
                this.pnlRDungeonItems.Visible = false;
            }

            if (this.pnlRDungeonNpcs != null)
            {
                this.pnlRDungeonNpcs.Visible = false;
            }

            if (this.pnlRDungeonTraps != null)
            {
                this.pnlRDungeonTraps.Visible = false;
            }

            if (this.pnlRDungeonWeather != null)
            {
                this.pnlRDungeonWeather.Visible = false;
            }

            if (this.pnlRDungeonGoal != null)
            {
                this.pnlRDungeonGoal.Visible = false;
            }

            if (this.pnlRDungeonChambers != null)
            {
                this.pnlRDungeonChambers.Visible = false;
            }

            if (this.pnlRDungeonMisc != null)
            {
                this.pnlRDungeonMisc.Visible = false;
            }

            if (this.pnlTileSelector != null)
            {
                this.pnlTileSelector.Visible = false;
            }

            this.pnlRDungeonFloorSettingSelection.Visible = true;
            this.btnSaveFloor.Text = "Save All Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load All Settings from Floor";
        }

        private void BtnSaveFloor_Click(object sender, MouseButtonEventArgs e)
        {
            for (int i = this.rdungeon.Floors.Count; i < this.nudMaxFloors.Value; i++)
            {
                this.rdungeon.Floors.Add(new EditableRDungeonFloor());
            }

            if (this.nudFirstFloor.Value > this.nudMaxFloors.Value || this.nudLastFloor.Value > this.nudMaxFloors.Value)
            {
                this.lblSaveLoadMessage.Text = "Cannot save floor(s) above the maximum.";
                return;
            }

            if (this.pnlRDungeonFloorSettingSelection.Visible == true)
            {
                this.LoadpnlRDungeonStructure();
                this.LoadpnlRDungeonLandTiles();
                this.LoadpnlRDungeonLandAltTiles();
                this.LoadpnlRDungeonWaterTiles();
                this.LoadpnlRDungeonWaterAnimTiles();
                this.LoadpnlRDungeonAttributes();
                this.LoadpnlRDungeonItems();
                this.LoadpnlRDungeonNpcs();
                this.LoadpnlRDungeonTraps();
                this.LoadpnlRDungeonWeather();
                this.LoadpnlRDungeonGoal();
                this.LoadpnlRDungeonChambers();
                this.LoadpnlRDungeonMisc();

                // Structure
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].TrapMin = this.nudTrapMin.Value;
                    this.rdungeon.Floors[floor].TrapMax = this.nudTrapMax.Value;
                    this.rdungeon.Floors[floor].ItemMin = this.nudItemMin.Value;
                    this.rdungeon.Floors[floor].ItemMax = this.nudItemMax.Value;
                    this.rdungeon.Floors[floor].RoomWidthMin = this.nudRoomWidthMin.Value;
                    this.rdungeon.Floors[floor].RoomWidthMax = this.nudRoomWidthMax.Value;
                    this.rdungeon.Floors[floor].RoomLengthMin = this.nudRoomLengthMin.Value;
                    this.rdungeon.Floors[floor].RoomLengthMax = this.nudRoomLengthMax.Value;
                    this.rdungeon.Floors[floor].HallTurnMin = this.nudHallTurnMin.Value;
                    this.rdungeon.Floors[floor].HallTurnMax = this.nudHallTurnMax.Value;
                    this.rdungeon.Floors[floor].HallVarMin = this.nudHallVarMin.Value;
                    this.rdungeon.Floors[floor].HallVarMax = this.nudHallVarMax.Value;
                    this.rdungeon.Floors[floor].WaterFrequency = this.nudWaterFrequency.Value;
                    this.rdungeon.Floors[floor].Craters = this.nudCraters.Value;
                    this.rdungeon.Floors[floor].CraterMinLength = this.nudCraterMinLength.Value;
                    this.rdungeon.Floors[floor].CraterMaxLength = this.nudCraterMaxLength.Value;
                    this.rdungeon.Floors[floor].CraterFuzzy = this.chkCraterFuzzy.Checked;

                    // Land Tiles
                    this.rdungeon.Floors[floor].StairsSheet = this.landTileNumbers[0, 19];
                    this.rdungeon.Floors[floor].MGroundSheet = this.landTileNumbers[0, 16];
                    this.rdungeon.Floors[floor].MTopLeftSheet = this.landTileNumbers[0, 10];
                    this.rdungeon.Floors[floor].MTopCenterSheet = this.landTileNumbers[0, 6];
                    this.rdungeon.Floors[floor].MTopRightSheet = this.landTileNumbers[0, 2];
                    this.rdungeon.Floors[floor].MCenterLeftSheet = this.landTileNumbers[0, 9];
                    this.rdungeon.Floors[floor].MCenterCenterSheet = this.landTileNumbers[0, 5];
                    this.rdungeon.Floors[floor].MCenterRightSheet = this.landTileNumbers[0, 1];
                    this.rdungeon.Floors[floor].MBottomLeftSheet = this.landTileNumbers[0, 8];
                    this.rdungeon.Floors[floor].MBottomCenterSheet = this.landTileNumbers[0, 4];
                    this.rdungeon.Floors[floor].MBottomRightSheet = this.landTileNumbers[0, 0];
                    this.rdungeon.Floors[floor].MInnerTopLeftSheet = this.landTileNumbers[0, 17];
                    this.rdungeon.Floors[floor].MInnerTopRightSheet = this.landTileNumbers[0, 20];
                    this.rdungeon.Floors[floor].MInnerBottomLeftSheet = this.landTileNumbers[0, 18];
                    this.rdungeon.Floors[floor].MInnerBottomRightSheet = this.landTileNumbers[0, 21];
                    this.rdungeon.Floors[floor].MIsolatedWallSheet = this.landTileNumbers[0, 15];
                    this.rdungeon.Floors[floor].MColumnTopSheet = this.landTileNumbers[0, 12];
                    this.rdungeon.Floors[floor].MColumnCenterSheet = this.landTileNumbers[0, 13];
                    this.rdungeon.Floors[floor].MColumnBottomSheet = this.landTileNumbers[0, 14];
                    this.rdungeon.Floors[floor].MRowLeftSheet = this.landTileNumbers[0, 3];
                    this.rdungeon.Floors[floor].MRowCenterSheet = this.landTileNumbers[0, 7];
                    this.rdungeon.Floors[floor].MRowRightSheet = this.landTileNumbers[0, 11];

                    this.rdungeon.Floors[floor].StairsX = this.landTileNumbers[1, 19];
                    this.rdungeon.Floors[floor].MGroundX = this.landTileNumbers[1, 16];
                    this.rdungeon.Floors[floor].MTopLeftX = this.landTileNumbers[1, 10];
                    this.rdungeon.Floors[floor].MTopCenterX = this.landTileNumbers[1, 6];
                    this.rdungeon.Floors[floor].MTopRightX = this.landTileNumbers[1, 2];
                    this.rdungeon.Floors[floor].MCenterLeftX = this.landTileNumbers[1, 9];
                    this.rdungeon.Floors[floor].MCenterCenterX = this.landTileNumbers[1, 5];
                    this.rdungeon.Floors[floor].MCenterRightX = this.landTileNumbers[1, 1];
                    this.rdungeon.Floors[floor].MBottomLeftX = this.landTileNumbers[1, 8];
                    this.rdungeon.Floors[floor].MBottomCenterX = this.landTileNumbers[1, 4];
                    this.rdungeon.Floors[floor].MBottomRightX = this.landTileNumbers[1, 0];
                    this.rdungeon.Floors[floor].MInnerTopLeftX = this.landTileNumbers[1, 17];
                    this.rdungeon.Floors[floor].MInnerTopRightX = this.landTileNumbers[1, 20];
                    this.rdungeon.Floors[floor].MInnerBottomLeftX = this.landTileNumbers[1, 18];
                    this.rdungeon.Floors[floor].MInnerBottomRightX = this.landTileNumbers[1, 21];
                    this.rdungeon.Floors[floor].MIsolatedWallX = this.landTileNumbers[1, 15];
                    this.rdungeon.Floors[floor].MColumnTopX = this.landTileNumbers[1, 12];
                    this.rdungeon.Floors[floor].MColumnCenterX = this.landTileNumbers[1, 13];
                    this.rdungeon.Floors[floor].MColumnBottomX = this.landTileNumbers[1, 14];
                    this.rdungeon.Floors[floor].MRowLeftX = this.landTileNumbers[1, 3];
                    this.rdungeon.Floors[floor].MRowCenterX = this.landTileNumbers[1, 7];
                    this.rdungeon.Floors[floor].MRowRightX = this.landTileNumbers[1, 11];

                    // Land Alt Tiles
                    this.rdungeon.Floors[floor].MGroundAlt2Sheet = this.landAltTileNumbers[0, 19];
                    this.rdungeon.Floors[floor].MGroundAltSheet = this.landAltTileNumbers[0, 16];
                    this.rdungeon.Floors[floor].MTopLeftAltSheet = this.landAltTileNumbers[0, 10];
                    this.rdungeon.Floors[floor].MTopCenterAltSheet = this.landAltTileNumbers[0, 6];
                    this.rdungeon.Floors[floor].MTopRightAltSheet = this.landAltTileNumbers[0, 2];
                    this.rdungeon.Floors[floor].MCenterLeftAltSheet = this.landAltTileNumbers[0, 9];
                    this.rdungeon.Floors[floor].MCenterCenterAltSheet = this.landAltTileNumbers[0, 5];
                    this.rdungeon.Floors[floor].MCenterCenterAlt2Sheet = this.landAltTileNumbers[0, 22];
                    this.rdungeon.Floors[floor].MCenterRightAltSheet = this.landAltTileNumbers[0, 1];
                    this.rdungeon.Floors[floor].MBottomLeftAltSheet = this.landAltTileNumbers[0, 8];
                    this.rdungeon.Floors[floor].MBottomCenterAltSheet = this.landAltTileNumbers[0, 4];
                    this.rdungeon.Floors[floor].MBottomRightAltSheet = this.landAltTileNumbers[0, 0];
                    this.rdungeon.Floors[floor].MInnerTopLeftAltSheet = this.landAltTileNumbers[0, 17];
                    this.rdungeon.Floors[floor].MInnerTopRightAltSheet = this.landAltTileNumbers[0, 20];
                    this.rdungeon.Floors[floor].MInnerBottomLeftAltSheet = this.landAltTileNumbers[0, 18];
                    this.rdungeon.Floors[floor].MInnerBottomRightAltSheet = this.landAltTileNumbers[0, 21];
                    this.rdungeon.Floors[floor].MIsolatedWallAltSheet = this.landAltTileNumbers[0, 15];
                    this.rdungeon.Floors[floor].MColumnTopAltSheet = this.landAltTileNumbers[0, 12];
                    this.rdungeon.Floors[floor].MColumnCenterAltSheet = this.landAltTileNumbers[0, 13];
                    this.rdungeon.Floors[floor].MColumnBottomAltSheet = this.landAltTileNumbers[0, 14];
                    this.rdungeon.Floors[floor].MRowLeftAltSheet = this.landAltTileNumbers[0, 3];
                    this.rdungeon.Floors[floor].MRowCenterAltSheet = this.landAltTileNumbers[0, 7];
                    this.rdungeon.Floors[floor].MRowRightAltSheet = this.landAltTileNumbers[0, 11];

                    this.rdungeon.Floors[floor].MGroundAlt2X = this.landAltTileNumbers[1, 19];
                    this.rdungeon.Floors[floor].MGroundAltX = this.landAltTileNumbers[1, 16];
                    this.rdungeon.Floors[floor].MTopLeftAltX = this.landAltTileNumbers[1, 10];
                    this.rdungeon.Floors[floor].MTopCenterAltX = this.landAltTileNumbers[1, 6];
                    this.rdungeon.Floors[floor].MTopRightAltX = this.landAltTileNumbers[1, 2];
                    this.rdungeon.Floors[floor].MCenterLeftAltX = this.landAltTileNumbers[1, 9];
                    this.rdungeon.Floors[floor].MCenterCenterAltX = this.landAltTileNumbers[1, 5];
                    this.rdungeon.Floors[floor].MCenterCenterAlt2X = this.landAltTileNumbers[1, 22];
                    this.rdungeon.Floors[floor].MCenterRightAltX = this.landAltTileNumbers[1, 1];
                    this.rdungeon.Floors[floor].MBottomLeftAltX = this.landAltTileNumbers[1, 8];
                    this.rdungeon.Floors[floor].MBottomCenterAltX = this.landAltTileNumbers[1, 4];
                    this.rdungeon.Floors[floor].MBottomRightAltX = this.landAltTileNumbers[1, 0];
                    this.rdungeon.Floors[floor].MInnerTopLeftAltX = this.landAltTileNumbers[1, 17];
                    this.rdungeon.Floors[floor].MInnerTopRightAltX = this.landAltTileNumbers[1, 20];
                    this.rdungeon.Floors[floor].MInnerBottomLeftAltX = this.landAltTileNumbers[1, 18];
                    this.rdungeon.Floors[floor].MInnerBottomRightAltX = this.landAltTileNumbers[1, 21];
                    this.rdungeon.Floors[floor].MIsolatedWallAltX = this.landAltTileNumbers[1, 15];
                    this.rdungeon.Floors[floor].MColumnTopAltX = this.landAltTileNumbers[1, 12];
                    this.rdungeon.Floors[floor].MColumnCenterAltX = this.landAltTileNumbers[1, 13];
                    this.rdungeon.Floors[floor].MColumnBottomAltX = this.landAltTileNumbers[1, 14];
                    this.rdungeon.Floors[floor].MRowLeftAltX = this.landAltTileNumbers[1, 3];
                    this.rdungeon.Floors[floor].MRowCenterAltX = this.landAltTileNumbers[1, 7];
                    this.rdungeon.Floors[floor].MRowRightAltX = this.landAltTileNumbers[1, 11];

                    // Water Tiles
                    this.rdungeon.Floors[floor].MShoreSurroundedSheet = this.waterTileNumbers[0, 15];
                    this.rdungeon.Floors[floor].MShoreInnerTopLeftSheet = this.waterTileNumbers[0, 0];
                    this.rdungeon.Floors[floor].MShoreTopSheet = this.waterTileNumbers[0, 4];
                    this.rdungeon.Floors[floor].MShoreInnerTopRightSheet = this.waterTileNumbers[0, 8];
                    this.rdungeon.Floors[floor].MShoreLeftSheet = this.waterTileNumbers[0, 1];
                    this.rdungeon.Floors[floor].MWaterSheet = this.waterTileNumbers[0, 5];
                    this.rdungeon.Floors[floor].MShoreRightSheet = this.waterTileNumbers[0, 9];
                    this.rdungeon.Floors[floor].MShoreInnerBottomLeftSheet = this.waterTileNumbers[0, 2];
                    this.rdungeon.Floors[floor].MShoreBottomSheet = this.waterTileNumbers[0, 6];
                    this.rdungeon.Floors[floor].MShoreInnerBottomRightSheet = this.waterTileNumbers[0, 10];
                    this.rdungeon.Floors[floor].MShoreTopLeftSheet = this.waterTileNumbers[0, 16];
                    this.rdungeon.Floors[floor].MShoreTopRightSheet = this.waterTileNumbers[0, 19];
                    this.rdungeon.Floors[floor].MShoreBottomLeftSheet = this.waterTileNumbers[0, 17];
                    this.rdungeon.Floors[floor].MShoreBottomRightSheet = this.waterTileNumbers[0, 20];
                    this.rdungeon.Floors[floor].MShoreDiagonalForwardSheet = this.waterTileNumbers[0, 18];
                    this.rdungeon.Floors[floor].MShoreDiagonalBackSheet = this.waterTileNumbers[0, 21];
                    this.rdungeon.Floors[floor].MShoreInnerTopSheet = this.waterTileNumbers[0, 12];
                    this.rdungeon.Floors[floor].MShoreVerticalSheet = this.waterTileNumbers[0, 13];
                    this.rdungeon.Floors[floor].MShoreInnerBottomSheet = this.waterTileNumbers[0, 14];
                    this.rdungeon.Floors[floor].MShoreInnerLeftSheet = this.waterTileNumbers[0, 3];
                    this.rdungeon.Floors[floor].MShoreHorizontalSheet = this.waterTileNumbers[0, 7];
                    this.rdungeon.Floors[floor].MShoreInnerRightSheet = this.waterTileNumbers[0, 11];

                    this.rdungeon.Floors[floor].MShoreSurroundedX = this.waterTileNumbers[1, 15];
                    this.rdungeon.Floors[floor].MShoreInnerTopLeftX = this.waterTileNumbers[1, 0];
                    this.rdungeon.Floors[floor].MShoreTopX = this.waterTileNumbers[1, 4];
                    this.rdungeon.Floors[floor].MShoreInnerTopRightX = this.waterTileNumbers[1, 8];
                    this.rdungeon.Floors[floor].MShoreLeftX = this.waterTileNumbers[1, 1];
                    this.rdungeon.Floors[floor].MWaterX = this.waterTileNumbers[1, 5];
                    this.rdungeon.Floors[floor].MShoreRightX = this.waterTileNumbers[1, 9];
                    this.rdungeon.Floors[floor].MShoreInnerBottomLeftX = this.waterTileNumbers[1, 2];
                    this.rdungeon.Floors[floor].MShoreBottomX = this.waterTileNumbers[1, 6];
                    this.rdungeon.Floors[floor].MShoreInnerBottomRightX = this.waterTileNumbers[1, 10];
                    this.rdungeon.Floors[floor].MShoreTopLeftX = this.waterTileNumbers[1, 16];
                    this.rdungeon.Floors[floor].MShoreTopRightX = this.waterTileNumbers[1, 19];
                    this.rdungeon.Floors[floor].MShoreBottomLeftX = this.waterTileNumbers[1, 17];
                    this.rdungeon.Floors[floor].MShoreBottomRightX = this.waterTileNumbers[1, 20];
                    this.rdungeon.Floors[floor].MShoreDiagonalForwardX = this.waterTileNumbers[1, 18];
                    this.rdungeon.Floors[floor].MShoreDiagonalBackX = this.waterTileNumbers[1, 21];
                    this.rdungeon.Floors[floor].MShoreInnerTopX = this.waterTileNumbers[1, 12];
                    this.rdungeon.Floors[floor].MShoreVerticalX = this.waterTileNumbers[1, 13];
                    this.rdungeon.Floors[floor].MShoreInnerBottomX = this.waterTileNumbers[1, 14];
                    this.rdungeon.Floors[floor].MShoreInnerLeftX = this.waterTileNumbers[1, 3];
                    this.rdungeon.Floors[floor].MShoreHorizontalX = this.waterTileNumbers[1, 7];
                    this.rdungeon.Floors[floor].MShoreInnerRightX = this.waterTileNumbers[1, 11];

                    // Water Anim Tiles
                    this.rdungeon.Floors[floor].MShoreSurroundedAnimSheet = this.waterAnimTileNumbers[0, 15];
                    this.rdungeon.Floors[floor].MShoreInnerTopLeftAnimSheet = this.waterAnimTileNumbers[0, 0];
                    this.rdungeon.Floors[floor].MShoreTopAnimSheet = this.waterAnimTileNumbers[0, 4];
                    this.rdungeon.Floors[floor].MShoreInnerTopRightAnimSheet = this.waterAnimTileNumbers[0, 8];
                    this.rdungeon.Floors[floor].MShoreLeftAnimSheet = this.waterAnimTileNumbers[0, 1];
                    this.rdungeon.Floors[floor].MWaterAnimSheet = this.waterAnimTileNumbers[0, 5];
                    this.rdungeon.Floors[floor].MShoreRightAnimSheet = this.waterAnimTileNumbers[0, 9];
                    this.rdungeon.Floors[floor].MShoreInnerBottomLeftAnimSheet = this.waterAnimTileNumbers[0, 2];
                    this.rdungeon.Floors[floor].MShoreBottomAnimSheet = this.waterAnimTileNumbers[0, 6];
                    this.rdungeon.Floors[floor].MShoreInnerBottomRightAnimSheet = this.waterAnimTileNumbers[0, 10];
                    this.rdungeon.Floors[floor].MShoreTopLeftAnimSheet = this.waterAnimTileNumbers[0, 16];
                    this.rdungeon.Floors[floor].MShoreTopRightAnimSheet = this.waterAnimTileNumbers[0, 19];
                    this.rdungeon.Floors[floor].MShoreBottomLeftAnimSheet = this.waterAnimTileNumbers[0, 17];
                    this.rdungeon.Floors[floor].MShoreBottomRightAnimSheet = this.waterAnimTileNumbers[0, 20];
                    this.rdungeon.Floors[floor].MShoreDiagonalForwardAnimSheet = this.waterAnimTileNumbers[0, 18];
                    this.rdungeon.Floors[floor].MShoreDiagonalBackAnimSheet = this.waterAnimTileNumbers[0, 21];
                    this.rdungeon.Floors[floor].MShoreInnerTopAnimSheet = this.waterAnimTileNumbers[0, 12];
                    this.rdungeon.Floors[floor].MShoreVerticalAnimSheet = this.waterAnimTileNumbers[0, 13];
                    this.rdungeon.Floors[floor].MShoreInnerBottomAnimSheet = this.waterAnimTileNumbers[0, 14];
                    this.rdungeon.Floors[floor].MShoreInnerLeftAnimSheet = this.waterAnimTileNumbers[0, 3];
                    this.rdungeon.Floors[floor].MShoreHorizontalAnimSheet = this.waterAnimTileNumbers[0, 7];
                    this.rdungeon.Floors[floor].MShoreInnerRightAnimSheet = this.waterAnimTileNumbers[0, 11];

                    this.rdungeon.Floors[floor].MShoreSurroundedAnimX = this.waterAnimTileNumbers[1, 15];
                    this.rdungeon.Floors[floor].MShoreInnerTopLeftAnimX = this.waterAnimTileNumbers[1, 0];
                    this.rdungeon.Floors[floor].MShoreTopAnimX = this.waterAnimTileNumbers[1, 4];
                    this.rdungeon.Floors[floor].MShoreInnerTopRightAnimX = this.waterAnimTileNumbers[1, 8];
                    this.rdungeon.Floors[floor].MShoreLeftAnimX = this.waterAnimTileNumbers[1, 1];
                    this.rdungeon.Floors[floor].MWaterAnimX = this.waterAnimTileNumbers[1, 5];
                    this.rdungeon.Floors[floor].MShoreRightAnimX = this.waterAnimTileNumbers[1, 9];
                    this.rdungeon.Floors[floor].MShoreInnerBottomLeftAnimX = this.waterAnimTileNumbers[1, 2];
                    this.rdungeon.Floors[floor].MShoreBottomAnimX = this.waterAnimTileNumbers[1, 6];
                    this.rdungeon.Floors[floor].MShoreInnerBottomRightAnimX = this.waterAnimTileNumbers[1, 10];
                    this.rdungeon.Floors[floor].MShoreTopLeftAnimX = this.waterAnimTileNumbers[1, 16];
                    this.rdungeon.Floors[floor].MShoreTopRightAnimX = this.waterAnimTileNumbers[1, 19];
                    this.rdungeon.Floors[floor].MShoreBottomLeftAnimX = this.waterAnimTileNumbers[1, 17];
                    this.rdungeon.Floors[floor].MShoreBottomRightAnimX = this.waterAnimTileNumbers[1, 20];
                    this.rdungeon.Floors[floor].MShoreDiagonalForwardAnimX = this.waterAnimTileNumbers[1, 18];
                    this.rdungeon.Floors[floor].MShoreDiagonalBackAnimX = this.waterAnimTileNumbers[1, 21];
                    this.rdungeon.Floors[floor].MShoreInnerTopAnimX = this.waterAnimTileNumbers[1, 12];
                    this.rdungeon.Floors[floor].MShoreVerticalAnimX = this.waterAnimTileNumbers[1, 13];
                    this.rdungeon.Floors[floor].MShoreInnerBottomAnimX = this.waterAnimTileNumbers[1, 14];
                    this.rdungeon.Floors[floor].MShoreInnerLeftAnimX = this.waterAnimTileNumbers[1, 3];
                    this.rdungeon.Floors[floor].MShoreHorizontalAnimX = this.waterAnimTileNumbers[1, 7];
                    this.rdungeon.Floors[floor].MShoreInnerRightAnimX = this.waterAnimTileNumbers[1, 11];

                    // Attributes
                    this.rdungeon.Floors[floor].GroundTile.Type = (Enums.TileType)this.cbGroundType.SelectedIndex;
                    this.rdungeon.Floors[floor].GroundTile.Data1 = this.nudGroundData1.Value;
                    this.rdungeon.Floors[floor].GroundTile.Data2 = this.nudGroundData2.Value;
                    this.rdungeon.Floors[floor].GroundTile.Data3 = this.nudGroundData3.Value;
                    this.rdungeon.Floors[floor].GroundTile.String1 = this.txtGroundString1.Text;
                    this.rdungeon.Floors[floor].GroundTile.String2 = this.txtGroundString2.Text;
                    this.rdungeon.Floors[floor].GroundTile.String3 = this.txtGroundString3.Text;

                    this.rdungeon.Floors[floor].HallTile.Type = (Enums.TileType)this.cbHallType.SelectedIndex;
                    this.rdungeon.Floors[floor].HallTile.Data1 = this.nudHallData1.Value;
                    this.rdungeon.Floors[floor].HallTile.Data2 = this.nudHallData2.Value;
                    this.rdungeon.Floors[floor].HallTile.Data3 = this.nudHallData3.Value;
                    this.rdungeon.Floors[floor].HallTile.String1 = this.txtHallString1.Text;
                    this.rdungeon.Floors[floor].HallTile.String2 = this.txtHallString2.Text;
                    this.rdungeon.Floors[floor].HallTile.String3 = this.txtHallString3.Text;

                    this.rdungeon.Floors[floor].WaterTile.Type = (Enums.TileType)this.cbWaterType.SelectedIndex;
                    this.rdungeon.Floors[floor].WaterTile.Data1 = this.nudWaterData1.Value;
                    this.rdungeon.Floors[floor].WaterTile.Data2 = this.nudWaterData2.Value;
                    this.rdungeon.Floors[floor].WaterTile.Data3 = this.nudWaterData3.Value;
                    this.rdungeon.Floors[floor].WaterTile.String1 = this.txtWaterString1.Text;
                    this.rdungeon.Floors[floor].WaterTile.String2 = this.txtWaterString2.Text;
                    this.rdungeon.Floors[floor].WaterTile.String3 = this.txtWaterString3.Text;

                    this.rdungeon.Floors[floor].WallTile.Type = (Enums.TileType)this.cbWallType.SelectedIndex;
                    this.rdungeon.Floors[floor].WallTile.Data1 = this.nudWallData1.Value;
                    this.rdungeon.Floors[floor].WallTile.Data2 = this.nudWallData2.Value;
                    this.rdungeon.Floors[floor].WallTile.Data3 = this.nudWallData3.Value;
                    this.rdungeon.Floors[floor].WallTile.String1 = this.txtWallString1.Text;
                    this.rdungeon.Floors[floor].WallTile.String2 = this.txtWallString2.Text;
                    this.rdungeon.Floors[floor].WallTile.String3 = this.txtWallString3.Text;

                    // Items
                    this.rdungeon.Floors[floor].Items.Clear();
                    for (int item = 0; item < this.itemList.Count; item++)
                    {
                        EditableRDungeonItem newItem = new EditableRDungeonItem();
                        newItem.ItemNum = this.itemList[item].ItemNum;
                        newItem.MinAmount = this.itemList[item].MinAmount;
                        newItem.MaxAmount = this.itemList[item].MaxAmount;
                        newItem.AppearanceRate = this.itemList[item].AppearanceRate;
                        newItem.StickyRate = this.itemList[item].StickyRate;
                        newItem.Tag = this.itemList[item].Tag;
                        newItem.Hidden = this.itemList[item].Hidden;
                        newItem.OnGround = this.itemList[item].OnGround;
                        newItem.OnWater = this.itemList[item].OnWater;
                        newItem.OnWall = this.itemList[item].OnWall;

                        this.rdungeon.Floors[floor].Items.Add(newItem);
                    }

                    // Npcs
                    this.rdungeon.Floors[floor].NpcSpawnTime = this.nudNpcSpawnTime.Value;
                    this.rdungeon.Floors[floor].NpcMin = this.nudNpcMin.Value;
                    this.rdungeon.Floors[floor].NpcMax = this.nudNpcMax.Value;

                    this.rdungeon.Floors[floor].Npcs.Clear();
                    for (int npc = 0; npc < this.npcList.Count; npc++)
                    {
                        MapNpcSettings newNpc = new MapNpcSettings();
                        newNpc.NpcNum = this.npcList[npc].NpcNum;
                        newNpc.MinLevel = this.npcList[npc].MinLevel;
                        newNpc.MaxLevel = this.npcList[npc].MaxLevel;
                        newNpc.AppearanceRate = this.npcList[npc].AppearanceRate;
                        newNpc.StartStatus = this.npcList[npc].StartStatus;
                        newNpc.StartStatusCounter = this.npcList[npc].StartStatusCounter;
                        newNpc.StartStatusChance = this.npcList[npc].StartStatusChance;

                        this.rdungeon.Floors[floor].Npcs.Add(newNpc);
                    }

                    // Traps
                    this.rdungeon.Floors[floor].SpecialTiles.Clear();
                    for (int traps = 0; traps < this.trapList.Count; traps++)
                    {
                        EditableRDungeonTrap newTile = new EditableRDungeonTrap();

                        newTile.SpecialTile.Ground = this.trapList[traps].SpecialTile.Ground;
                        newTile.SpecialTile.GroundAnim = this.trapList[traps].SpecialTile.GroundAnim;
                        newTile.SpecialTile.Mask = this.trapList[traps].SpecialTile.Mask;
                        newTile.SpecialTile.Anim = this.trapList[traps].SpecialTile.Anim;
                        newTile.SpecialTile.Mask2 = this.trapList[traps].SpecialTile.Mask2;
                        newTile.SpecialTile.M2Anim = this.trapList[traps].SpecialTile.M2Anim;
                        newTile.SpecialTile.Fringe = this.trapList[traps].SpecialTile.Fringe;
                        newTile.SpecialTile.FAnim = this.trapList[traps].SpecialTile.FAnim;
                        newTile.SpecialTile.Fringe2 = this.trapList[traps].SpecialTile.Fringe2;
                        newTile.SpecialTile.F2Anim = this.trapList[traps].SpecialTile.F2Anim;

                        newTile.SpecialTile.GroundSet = this.trapList[traps].SpecialTile.GroundSet;
                        newTile.SpecialTile.GroundAnimSet = this.trapList[traps].SpecialTile.GroundAnimSet;
                        newTile.SpecialTile.MaskSet = this.trapList[traps].SpecialTile.MaskSet;
                        newTile.SpecialTile.AnimSet = this.trapList[traps].SpecialTile.AnimSet;
                        newTile.SpecialTile.Mask2Set = this.trapList[traps].SpecialTile.Mask2Set;
                        newTile.SpecialTile.M2AnimSet = this.trapList[traps].SpecialTile.M2AnimSet;
                        newTile.SpecialTile.FringeSet = this.trapList[traps].SpecialTile.FringeSet;
                        newTile.SpecialTile.FAnimSet = this.trapList[traps].SpecialTile.FAnimSet;
                        newTile.SpecialTile.Fringe2Set = this.trapList[traps].SpecialTile.Fringe2Set;
                        newTile.SpecialTile.F2AnimSet = this.trapList[traps].SpecialTile.F2AnimSet;

                        newTile.SpecialTile.Type = this.trapList[traps].SpecialTile.Type;
                        newTile.SpecialTile.Data1 = this.trapList[traps].SpecialTile.Data1;
                        newTile.SpecialTile.Data2 = this.trapList[traps].SpecialTile.Data2;
                        newTile.SpecialTile.Data3 = this.trapList[traps].SpecialTile.Data3;
                        newTile.SpecialTile.String1 = this.trapList[traps].SpecialTile.String1;
                        newTile.SpecialTile.String2 = this.trapList[traps].SpecialTile.String2;
                        newTile.SpecialTile.String3 = this.trapList[traps].SpecialTile.String3;

                        newTile.AppearanceRate = this.trapList[traps].AppearanceRate;

                        this.rdungeon.Floors[floor].SpecialTiles.Add(newTile);
                    }

                    // Weather
                    this.rdungeon.Floors[floor].Weather.Clear();
                    for (int weather = 0; weather < this.lbxWeather.Items.Count; weather++)
                    {
                        string[] weatherindex = this.lbxWeather.Items[weather].TextIdentifier.Split(':');
                        if (weatherindex[1].IsNumeric())
                        {
                            this.rdungeon.Floors[floor].Weather.Add((Enums.Weather)weatherindex[1].ToInt());
                        }
                    }

                    // Goal
                    if (this.optNextFloor.Checked)
                    {
                        this.rdungeon.Floors[floor].GoalType = Enums.RFloorGoalType.NextFloor;
                    }
                    else if (this.optMap.Checked)
                    {
                        this.rdungeon.Floors[floor].GoalType = Enums.RFloorGoalType.Map;
                    }
                    else if (this.optScripted.Checked)
                    {
                        this.rdungeon.Floors[floor].GoalType = Enums.RFloorGoalType.Scripted;
                    }
else
                    {
                        this.rdungeon.Floors[floor].GoalType = Enums.RFloorGoalType.NextFloor;
                    }

                    this.rdungeon.Floors[floor].GoalMap = this.nudData1.Value;
                    this.rdungeon.Floors[floor].GoalX = this.nudData2.Value;
                    this.rdungeon.Floors[floor].GoalY = this.nudData3.Value;

                    // chambers
                    this.rdungeon.Floors[floor].Chambers.Clear();
                    for (int chamber = 0; chamber < this.chamberList.Count; chamber++)
                    {
                        EditableRDungeonChamber addedChamber = new EditableRDungeonChamber();
                        addedChamber.ChamberNum = this.chamberList[chamber].ChamberNum;
                        addedChamber.String1 = this.chamberList[chamber].String1;
                        addedChamber.String2 = this.chamberList[chamber].String2;
                        addedChamber.String3 = this.chamberList[chamber].String3;
                        this.rdungeon.Floors[floor].Chambers.Add(addedChamber);
                    }

                    // Misc
                    this.rdungeon.Floors[floor].Darkness = this.nudDarkness.Value;
                    if (this.lbxMusic.SelectedItems.Count != 1 || this.lbxMusic.Items[0].Selected)
                    {
                        this.rdungeon.Floors[floor].Music = string.Empty;
                    }
else
                    {
                        this.rdungeon.Floors[floor].Music = this.lbxMusic.SelectedItems[0].TextIdentifier;
                    }
                }

                this.lblSaveLoadMessage.Text = "All settings saved to floor(s)";
            }
            else if (this.pnlRDungeonStructure != null && this.pnlRDungeonStructure.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].TrapMin = this.nudTrapMin.Value;
                    this.rdungeon.Floors[floor].TrapMax = this.nudTrapMax.Value;
                    this.rdungeon.Floors[floor].ItemMin = this.nudItemMin.Value;
                    this.rdungeon.Floors[floor].ItemMax = this.nudItemMax.Value;
                    this.rdungeon.Floors[floor].RoomWidthMin = this.nudRoomWidthMin.Value;
                    this.rdungeon.Floors[floor].RoomWidthMax = this.nudRoomWidthMax.Value;
                    this.rdungeon.Floors[floor].RoomLengthMin = this.nudRoomLengthMin.Value;
                    this.rdungeon.Floors[floor].RoomLengthMax = this.nudRoomLengthMax.Value;
                    this.rdungeon.Floors[floor].HallTurnMin = this.nudHallTurnMin.Value;
                    this.rdungeon.Floors[floor].HallTurnMax = this.nudHallTurnMax.Value;
                    this.rdungeon.Floors[floor].HallVarMin = this.nudHallVarMin.Value;
                    this.rdungeon.Floors[floor].HallVarMax = this.nudHallVarMax.Value;
                    this.rdungeon.Floors[floor].WaterFrequency = this.nudWaterFrequency.Value;
                    this.rdungeon.Floors[floor].Craters = this.nudCraters.Value;
                    this.rdungeon.Floors[floor].CraterMinLength = this.nudCraterMinLength.Value;
                    this.rdungeon.Floors[floor].CraterMaxLength = this.nudCraterMaxLength.Value;
                    this.rdungeon.Floors[floor].CraterFuzzy = this.chkCraterFuzzy.Checked;
                }

                this.lblSaveLoadMessage.Text = "Structure settings saved to floor(s)";
            }
            else if (this.pnlRDungeonLandTiles != null && this.pnlRDungeonLandTiles.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].StairsSheet = this.landTileNumbers[0, 19];
                    this.rdungeon.Floors[floor].MGroundSheet = this.landTileNumbers[0, 16];
                    this.rdungeon.Floors[floor].MTopLeftSheet = this.landTileNumbers[0, 10];
                    this.rdungeon.Floors[floor].MTopCenterSheet = this.landTileNumbers[0, 6];
                    this.rdungeon.Floors[floor].MTopRightSheet = this.landTileNumbers[0, 2];
                    this.rdungeon.Floors[floor].MCenterLeftSheet = this.landTileNumbers[0, 9];
                    this.rdungeon.Floors[floor].MCenterCenterSheet = this.landTileNumbers[0, 5];
                    this.rdungeon.Floors[floor].MCenterRightSheet = this.landTileNumbers[0, 1];
                    this.rdungeon.Floors[floor].MBottomLeftSheet = this.landTileNumbers[0, 8];
                    this.rdungeon.Floors[floor].MBottomCenterSheet = this.landTileNumbers[0, 4];
                    this.rdungeon.Floors[floor].MBottomRightSheet = this.landTileNumbers[0, 0];
                    this.rdungeon.Floors[floor].MInnerTopLeftSheet = this.landTileNumbers[0, 17];
                    this.rdungeon.Floors[floor].MInnerTopRightSheet = this.landTileNumbers[0, 20];
                    this.rdungeon.Floors[floor].MInnerBottomLeftSheet = this.landTileNumbers[0, 18];
                    this.rdungeon.Floors[floor].MInnerBottomRightSheet = this.landTileNumbers[0, 21];
                    this.rdungeon.Floors[floor].MIsolatedWallSheet = this.landTileNumbers[0, 15];
                    this.rdungeon.Floors[floor].MColumnTopSheet = this.landTileNumbers[0, 12];
                    this.rdungeon.Floors[floor].MColumnCenterSheet = this.landTileNumbers[0, 13];
                    this.rdungeon.Floors[floor].MColumnBottomSheet = this.landTileNumbers[0, 14];
                    this.rdungeon.Floors[floor].MRowLeftSheet = this.landTileNumbers[0, 3];
                    this.rdungeon.Floors[floor].MRowCenterSheet = this.landTileNumbers[0, 7];
                    this.rdungeon.Floors[floor].MRowRightSheet = this.landTileNumbers[0, 11];

                    this.rdungeon.Floors[floor].StairsX = this.landTileNumbers[1, 19];
                    this.rdungeon.Floors[floor].MGroundX = this.landTileNumbers[1, 16];
                    this.rdungeon.Floors[floor].MTopLeftX = this.landTileNumbers[1, 10];
                    this.rdungeon.Floors[floor].MTopCenterX = this.landTileNumbers[1, 6];
                    this.rdungeon.Floors[floor].MTopRightX = this.landTileNumbers[1, 2];
                    this.rdungeon.Floors[floor].MCenterLeftX = this.landTileNumbers[1, 9];
                    this.rdungeon.Floors[floor].MCenterCenterX = this.landTileNumbers[1, 5];
                    this.rdungeon.Floors[floor].MCenterRightX = this.landTileNumbers[1, 1];
                    this.rdungeon.Floors[floor].MBottomLeftX = this.landTileNumbers[1, 8];
                    this.rdungeon.Floors[floor].MBottomCenterX = this.landTileNumbers[1, 4];
                    this.rdungeon.Floors[floor].MBottomRightX = this.landTileNumbers[1, 0];
                    this.rdungeon.Floors[floor].MInnerTopLeftX = this.landTileNumbers[1, 17];
                    this.rdungeon.Floors[floor].MInnerTopRightX = this.landTileNumbers[1, 20];
                    this.rdungeon.Floors[floor].MInnerBottomLeftX = this.landTileNumbers[1, 18];
                    this.rdungeon.Floors[floor].MInnerBottomRightX = this.landTileNumbers[1, 21];
                    this.rdungeon.Floors[floor].MIsolatedWallX = this.landTileNumbers[1, 15];
                    this.rdungeon.Floors[floor].MColumnTopX = this.landTileNumbers[1, 12];
                    this.rdungeon.Floors[floor].MColumnCenterX = this.landTileNumbers[1, 13];
                    this.rdungeon.Floors[floor].MColumnBottomX = this.landTileNumbers[1, 14];
                    this.rdungeon.Floors[floor].MRowLeftX = this.landTileNumbers[1, 3];
                    this.rdungeon.Floors[floor].MRowCenterX = this.landTileNumbers[1, 7];
                    this.rdungeon.Floors[floor].MRowRightX = this.landTileNumbers[1, 11];
                }

                this.lblSaveLoadMessage.Text = "Land Tile settings saved to floor(s)";
            }
            else if (this.pnlRDungeonLandAltTiles != null && this.pnlRDungeonLandAltTiles.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].MGroundAlt2Sheet = this.landAltTileNumbers[0, 19];
                    this.rdungeon.Floors[floor].MGroundAltSheet = this.landAltTileNumbers[0, 16];
                    this.rdungeon.Floors[floor].MTopLeftAltSheet = this.landAltTileNumbers[0, 10];
                    this.rdungeon.Floors[floor].MTopCenterAltSheet = this.landAltTileNumbers[0, 6];
                    this.rdungeon.Floors[floor].MTopRightAltSheet = this.landAltTileNumbers[0, 2];
                    this.rdungeon.Floors[floor].MCenterLeftAltSheet = this.landAltTileNumbers[0, 9];
                    this.rdungeon.Floors[floor].MCenterCenterAltSheet = this.landAltTileNumbers[0, 5];
                    this.rdungeon.Floors[floor].MCenterCenterAlt2Sheet = this.landAltTileNumbers[0, 22];
                    this.rdungeon.Floors[floor].MCenterRightAltSheet = this.landAltTileNumbers[0, 1];
                    this.rdungeon.Floors[floor].MBottomLeftAltSheet = this.landAltTileNumbers[0, 8];
                    this.rdungeon.Floors[floor].MBottomCenterAltSheet = this.landAltTileNumbers[0, 4];
                    this.rdungeon.Floors[floor].MBottomRightAltSheet = this.landAltTileNumbers[0, 0];
                    this.rdungeon.Floors[floor].MInnerTopLeftAltSheet = this.landAltTileNumbers[0, 17];
                    this.rdungeon.Floors[floor].MInnerTopRightAltSheet = this.landAltTileNumbers[0, 20];
                    this.rdungeon.Floors[floor].MInnerBottomLeftAltSheet = this.landAltTileNumbers[0, 18];
                    this.rdungeon.Floors[floor].MInnerBottomRightAltSheet = this.landAltTileNumbers[0, 21];
                    this.rdungeon.Floors[floor].MIsolatedWallAltSheet = this.landAltTileNumbers[0, 15];
                    this.rdungeon.Floors[floor].MColumnTopAltSheet = this.landAltTileNumbers[0, 12];
                    this.rdungeon.Floors[floor].MColumnCenterAltSheet = this.landAltTileNumbers[0, 13];
                    this.rdungeon.Floors[floor].MColumnBottomAltSheet = this.landAltTileNumbers[0, 14];
                    this.rdungeon.Floors[floor].MRowLeftAltSheet = this.landAltTileNumbers[0, 3];
                    this.rdungeon.Floors[floor].MRowCenterAltSheet = this.landAltTileNumbers[0, 7];
                    this.rdungeon.Floors[floor].MRowRightAltSheet = this.landAltTileNumbers[0, 11];

                    this.rdungeon.Floors[floor].MGroundAlt2X = this.landAltTileNumbers[1, 19];
                    this.rdungeon.Floors[floor].MGroundAltX = this.landAltTileNumbers[1, 16];
                    this.rdungeon.Floors[floor].MTopLeftAltX = this.landAltTileNumbers[1, 10];
                    this.rdungeon.Floors[floor].MTopCenterAltX = this.landAltTileNumbers[1, 6];
                    this.rdungeon.Floors[floor].MTopRightAltX = this.landAltTileNumbers[1, 2];
                    this.rdungeon.Floors[floor].MCenterLeftAltX = this.landAltTileNumbers[1, 9];
                    this.rdungeon.Floors[floor].MCenterCenterAltX = this.landAltTileNumbers[1, 5];
                    this.rdungeon.Floors[floor].MCenterCenterAlt2X = this.landAltTileNumbers[1, 22];
                    this.rdungeon.Floors[floor].MCenterRightAltX = this.landAltTileNumbers[1, 1];
                    this.rdungeon.Floors[floor].MBottomLeftAltX = this.landAltTileNumbers[1, 8];
                    this.rdungeon.Floors[floor].MBottomCenterAltX = this.landAltTileNumbers[1, 4];
                    this.rdungeon.Floors[floor].MBottomRightAltX = this.landAltTileNumbers[1, 0];
                    this.rdungeon.Floors[floor].MInnerTopLeftAltX = this.landAltTileNumbers[1, 17];
                    this.rdungeon.Floors[floor].MInnerTopRightAltX = this.landAltTileNumbers[1, 20];
                    this.rdungeon.Floors[floor].MInnerBottomLeftAltX = this.landAltTileNumbers[1, 18];
                    this.rdungeon.Floors[floor].MInnerBottomRightAltX = this.landAltTileNumbers[1, 21];
                    this.rdungeon.Floors[floor].MIsolatedWallAltX = this.landAltTileNumbers[1, 15];
                    this.rdungeon.Floors[floor].MColumnTopAltX = this.landAltTileNumbers[1, 12];
                    this.rdungeon.Floors[floor].MColumnCenterAltX = this.landAltTileNumbers[1, 13];
                    this.rdungeon.Floors[floor].MColumnBottomAltX = this.landAltTileNumbers[1, 14];
                    this.rdungeon.Floors[floor].MRowLeftAltX = this.landAltTileNumbers[1, 3];
                    this.rdungeon.Floors[floor].MRowCenterAltX = this.landAltTileNumbers[1, 7];
                    this.rdungeon.Floors[floor].MRowRightAltX = this.landAltTileNumbers[1, 11];
                }

                this.lblSaveLoadMessage.Text = "Land Alt Tile settings saved to floor(s)";
            }
            else if (this.pnlRDungeonWaterTiles != null && this.pnlRDungeonWaterTiles.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].MShoreSurroundedSheet = this.waterTileNumbers[0, 15];
                    this.rdungeon.Floors[floor].MShoreInnerTopLeftSheet = this.waterTileNumbers[0, 0];
                    this.rdungeon.Floors[floor].MShoreTopSheet = this.waterTileNumbers[0, 4];
                    this.rdungeon.Floors[floor].MShoreInnerTopRightSheet = this.waterTileNumbers[0, 8];
                    this.rdungeon.Floors[floor].MShoreLeftSheet = this.waterTileNumbers[0, 1];
                    this.rdungeon.Floors[floor].MWaterSheet = this.waterTileNumbers[0, 5];
                    this.rdungeon.Floors[floor].MShoreRightSheet = this.waterTileNumbers[0, 9];
                    this.rdungeon.Floors[floor].MShoreInnerBottomLeftSheet = this.waterTileNumbers[0, 2];
                    this.rdungeon.Floors[floor].MShoreBottomSheet = this.waterTileNumbers[0, 6];
                    this.rdungeon.Floors[floor].MShoreInnerBottomRightSheet = this.waterTileNumbers[0, 10];
                    this.rdungeon.Floors[floor].MShoreTopLeftSheet = this.waterTileNumbers[0, 16];
                    this.rdungeon.Floors[floor].MShoreTopRightSheet = this.waterTileNumbers[0, 19];
                    this.rdungeon.Floors[floor].MShoreBottomLeftSheet = this.waterTileNumbers[0, 17];
                    this.rdungeon.Floors[floor].MShoreBottomRightSheet = this.waterTileNumbers[0, 20];
                    this.rdungeon.Floors[floor].MShoreDiagonalForwardSheet = this.waterTileNumbers[0, 18];
                    this.rdungeon.Floors[floor].MShoreDiagonalBackSheet = this.waterTileNumbers[0, 21];
                    this.rdungeon.Floors[floor].MShoreInnerTopSheet = this.waterTileNumbers[0, 12];
                    this.rdungeon.Floors[floor].MShoreVerticalSheet = this.waterTileNumbers[0, 13];
                    this.rdungeon.Floors[floor].MShoreInnerBottomSheet = this.waterTileNumbers[0, 14];
                    this.rdungeon.Floors[floor].MShoreInnerLeftSheet = this.waterTileNumbers[0, 3];
                    this.rdungeon.Floors[floor].MShoreHorizontalSheet = this.waterTileNumbers[0, 7];
                    this.rdungeon.Floors[floor].MShoreInnerRightSheet = this.waterTileNumbers[0, 11];

                    this.rdungeon.Floors[floor].MShoreSurroundedX = this.waterTileNumbers[1, 15];
                    this.rdungeon.Floors[floor].MShoreInnerTopLeftX = this.waterTileNumbers[1, 0];
                    this.rdungeon.Floors[floor].MShoreTopX = this.waterTileNumbers[1, 4];
                    this.rdungeon.Floors[floor].MShoreInnerTopRightX = this.waterTileNumbers[1, 8];
                    this.rdungeon.Floors[floor].MShoreLeftX = this.waterTileNumbers[1, 1];
                    this.rdungeon.Floors[floor].MWaterX = this.waterTileNumbers[1, 5];
                    this.rdungeon.Floors[floor].MShoreRightX = this.waterTileNumbers[1, 9];
                    this.rdungeon.Floors[floor].MShoreInnerBottomLeftX = this.waterTileNumbers[1, 2];
                    this.rdungeon.Floors[floor].MShoreBottomX = this.waterTileNumbers[1, 6];
                    this.rdungeon.Floors[floor].MShoreInnerBottomRightX = this.waterTileNumbers[1, 10];
                    this.rdungeon.Floors[floor].MShoreTopLeftX = this.waterTileNumbers[1, 16];
                    this.rdungeon.Floors[floor].MShoreTopRightX = this.waterTileNumbers[1, 19];
                    this.rdungeon.Floors[floor].MShoreBottomLeftX = this.waterTileNumbers[1, 17];
                    this.rdungeon.Floors[floor].MShoreBottomRightX = this.waterTileNumbers[1, 20];
                    this.rdungeon.Floors[floor].MShoreDiagonalForwardX = this.waterTileNumbers[1, 18];
                    this.rdungeon.Floors[floor].MShoreDiagonalBackX = this.waterTileNumbers[1, 21];
                    this.rdungeon.Floors[floor].MShoreInnerTopX = this.waterTileNumbers[1, 12];
                    this.rdungeon.Floors[floor].MShoreVerticalX = this.waterTileNumbers[1, 13];
                    this.rdungeon.Floors[floor].MShoreInnerBottomX = this.waterTileNumbers[1, 14];
                    this.rdungeon.Floors[floor].MShoreInnerLeftX = this.waterTileNumbers[1, 3];
                    this.rdungeon.Floors[floor].MShoreHorizontalX = this.waterTileNumbers[1, 7];
                    this.rdungeon.Floors[floor].MShoreInnerRightX = this.waterTileNumbers[1, 11];
                }

                this.lblSaveLoadMessage.Text = "Water Tile settings saved to floor(s)";
            }
            else if (this.pnlRDungeonWaterAnimTiles != null && this.pnlRDungeonWaterAnimTiles.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].MShoreSurroundedAnimSheet = this.waterAnimTileNumbers[0, 15];
                    this.rdungeon.Floors[floor].MShoreInnerTopLeftAnimSheet = this.waterAnimTileNumbers[0, 0];
                    this.rdungeon.Floors[floor].MShoreTopAnimSheet = this.waterAnimTileNumbers[0, 4];
                    this.rdungeon.Floors[floor].MShoreInnerTopRightAnimSheet = this.waterAnimTileNumbers[0, 8];
                    this.rdungeon.Floors[floor].MShoreLeftAnimSheet = this.waterAnimTileNumbers[0, 1];
                    this.rdungeon.Floors[floor].MWaterAnimSheet = this.waterAnimTileNumbers[0, 5];
                    this.rdungeon.Floors[floor].MShoreRightAnimSheet = this.waterAnimTileNumbers[0, 9];
                    this.rdungeon.Floors[floor].MShoreInnerBottomLeftAnimSheet = this.waterAnimTileNumbers[0, 2];
                    this.rdungeon.Floors[floor].MShoreBottomAnimSheet = this.waterAnimTileNumbers[0, 6];
                    this.rdungeon.Floors[floor].MShoreInnerBottomRightAnimSheet = this.waterAnimTileNumbers[0, 10];
                    this.rdungeon.Floors[floor].MShoreTopLeftAnimSheet = this.waterAnimTileNumbers[0, 16];
                    this.rdungeon.Floors[floor].MShoreTopRightAnimSheet = this.waterAnimTileNumbers[0, 19];
                    this.rdungeon.Floors[floor].MShoreBottomLeftAnimSheet = this.waterAnimTileNumbers[0, 17];
                    this.rdungeon.Floors[floor].MShoreBottomRightAnimSheet = this.waterAnimTileNumbers[0, 20];
                    this.rdungeon.Floors[floor].MShoreDiagonalForwardAnimSheet = this.waterAnimTileNumbers[0, 18];
                    this.rdungeon.Floors[floor].MShoreDiagonalBackAnimSheet = this.waterAnimTileNumbers[0, 21];
                    this.rdungeon.Floors[floor].MShoreInnerTopAnimSheet = this.waterAnimTileNumbers[0, 12];
                    this.rdungeon.Floors[floor].MShoreVerticalAnimSheet = this.waterAnimTileNumbers[0, 13];
                    this.rdungeon.Floors[floor].MShoreInnerBottomAnimSheet = this.waterAnimTileNumbers[0, 14];
                    this.rdungeon.Floors[floor].MShoreInnerLeftAnimSheet = this.waterAnimTileNumbers[0, 3];
                    this.rdungeon.Floors[floor].MShoreHorizontalAnimSheet = this.waterAnimTileNumbers[0, 7];
                    this.rdungeon.Floors[floor].MShoreInnerRightAnimSheet = this.waterAnimTileNumbers[0, 11];

                    this.rdungeon.Floors[floor].MShoreSurroundedAnimX = this.waterAnimTileNumbers[1, 15];
                    this.rdungeon.Floors[floor].MShoreInnerTopLeftAnimX = this.waterAnimTileNumbers[1, 0];
                    this.rdungeon.Floors[floor].MShoreTopAnimX = this.waterAnimTileNumbers[1, 4];
                    this.rdungeon.Floors[floor].MShoreInnerTopRightAnimX = this.waterAnimTileNumbers[1, 8];
                    this.rdungeon.Floors[floor].MShoreLeftAnimX = this.waterAnimTileNumbers[1, 1];
                    this.rdungeon.Floors[floor].MWaterAnimX = this.waterAnimTileNumbers[1, 5];
                    this.rdungeon.Floors[floor].MShoreRightAnimX = this.waterAnimTileNumbers[1, 9];
                    this.rdungeon.Floors[floor].MShoreInnerBottomLeftAnimX = this.waterAnimTileNumbers[1, 2];
                    this.rdungeon.Floors[floor].MShoreBottomAnimX = this.waterAnimTileNumbers[1, 6];
                    this.rdungeon.Floors[floor].MShoreInnerBottomRightAnimX = this.waterAnimTileNumbers[1, 10];
                    this.rdungeon.Floors[floor].MShoreTopLeftAnimX = this.waterAnimTileNumbers[1, 16];
                    this.rdungeon.Floors[floor].MShoreTopRightAnimX = this.waterAnimTileNumbers[1, 19];
                    this.rdungeon.Floors[floor].MShoreBottomLeftAnimX = this.waterAnimTileNumbers[1, 17];
                    this.rdungeon.Floors[floor].MShoreBottomRightAnimX = this.waterAnimTileNumbers[1, 20];
                    this.rdungeon.Floors[floor].MShoreDiagonalForwardAnimX = this.waterAnimTileNumbers[1, 18];
                    this.rdungeon.Floors[floor].MShoreDiagonalBackAnimX = this.waterAnimTileNumbers[1, 21];
                    this.rdungeon.Floors[floor].MShoreInnerTopAnimX = this.waterAnimTileNumbers[1, 12];
                    this.rdungeon.Floors[floor].MShoreVerticalAnimX = this.waterAnimTileNumbers[1, 13];
                    this.rdungeon.Floors[floor].MShoreInnerBottomAnimX = this.waterAnimTileNumbers[1, 14];
                    this.rdungeon.Floors[floor].MShoreInnerLeftAnimX = this.waterAnimTileNumbers[1, 3];
                    this.rdungeon.Floors[floor].MShoreHorizontalAnimX = this.waterAnimTileNumbers[1, 7];
                    this.rdungeon.Floors[floor].MShoreInnerRightAnimX = this.waterAnimTileNumbers[1, 11];
                }

                this.lblSaveLoadMessage.Text = "Water Anim Tile settings saved to floor(s)";
            }
            else if (this.pnlRDungeonAttributes != null && this.pnlRDungeonAttributes.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].GroundTile.Type = (Enums.TileType)this.cbGroundType.SelectedIndex;
                    this.rdungeon.Floors[floor].GroundTile.Data1 = this.nudGroundData1.Value;
                    this.rdungeon.Floors[floor].GroundTile.Data2 = this.nudGroundData2.Value;
                    this.rdungeon.Floors[floor].GroundTile.Data3 = this.nudGroundData3.Value;
                    this.rdungeon.Floors[floor].GroundTile.String1 = this.txtGroundString1.Text;
                    this.rdungeon.Floors[floor].GroundTile.String2 = this.txtGroundString2.Text;
                    this.rdungeon.Floors[floor].GroundTile.String3 = this.txtGroundString3.Text;

                    this.rdungeon.Floors[floor].HallTile.Type = (Enums.TileType)this.cbHallType.SelectedIndex;
                    this.rdungeon.Floors[floor].HallTile.Data1 = this.nudHallData1.Value;
                    this.rdungeon.Floors[floor].HallTile.Data2 = this.nudHallData2.Value;
                    this.rdungeon.Floors[floor].HallTile.Data3 = this.nudHallData3.Value;
                    this.rdungeon.Floors[floor].HallTile.String1 = this.txtHallString1.Text;
                    this.rdungeon.Floors[floor].HallTile.String2 = this.txtHallString2.Text;
                    this.rdungeon.Floors[floor].HallTile.String3 = this.txtHallString3.Text;

                    this.rdungeon.Floors[floor].WaterTile.Type = (Enums.TileType)this.cbWaterType.SelectedIndex;
                    this.rdungeon.Floors[floor].WaterTile.Data1 = this.nudWaterData1.Value;
                    this.rdungeon.Floors[floor].WaterTile.Data2 = this.nudWaterData2.Value;
                    this.rdungeon.Floors[floor].WaterTile.Data3 = this.nudWaterData3.Value;
                    this.rdungeon.Floors[floor].WaterTile.String1 = this.txtWaterString1.Text;
                    this.rdungeon.Floors[floor].WaterTile.String2 = this.txtWaterString2.Text;
                    this.rdungeon.Floors[floor].WaterTile.String3 = this.txtWaterString3.Text;

                    this.rdungeon.Floors[floor].WallTile.Type = (Enums.TileType)this.cbWallType.SelectedIndex;
                    this.rdungeon.Floors[floor].WallTile.Data1 = this.nudWallData1.Value;
                    this.rdungeon.Floors[floor].WallTile.Data2 = this.nudWallData2.Value;
                    this.rdungeon.Floors[floor].WallTile.Data3 = this.nudWallData3.Value;
                    this.rdungeon.Floors[floor].WallTile.String1 = this.txtWallString1.Text;
                    this.rdungeon.Floors[floor].WallTile.String2 = this.txtWallString2.Text;
                    this.rdungeon.Floors[floor].WallTile.String3 = this.txtWallString3.Text;
                }

                this.lblSaveLoadMessage.Text = "Attribute settings saved to floor(s)";
            }
            else if (this.pnlRDungeonItems != null && this.pnlRDungeonItems.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].Items.Clear();
                    for (int item = 0; item < this.itemList.Count; item++)
                    {
                        EditableRDungeonItem newItem = new EditableRDungeonItem();
                        newItem.ItemNum = this.itemList[item].ItemNum;
                        newItem.MinAmount = this.itemList[item].MinAmount;
                        newItem.MaxAmount = this.itemList[item].MaxAmount;
                        newItem.AppearanceRate = this.itemList[item].AppearanceRate;
                        newItem.StickyRate = this.itemList[item].StickyRate;
                        newItem.Tag = this.itemList[item].Tag;
                        newItem.Hidden = this.itemList[item].Hidden;
                        newItem.OnGround = this.itemList[item].OnGround;
                        newItem.OnWater = this.itemList[item].OnWater;
                        newItem.OnWall = this.itemList[item].OnWall;

                        this.rdungeon.Floors[floor].Items.Add(newItem);
                    }
                }

                this.lblSaveLoadMessage.Text = "Item settings saved to floor(s)";
            }
            else if (this.pnlRDungeonNpcs != null && this.pnlRDungeonNpcs.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].NpcSpawnTime = this.nudNpcSpawnTime.Value;
                    this.rdungeon.Floors[floor].NpcMin = this.nudNpcMin.Value;
                    this.rdungeon.Floors[floor].NpcMax = this.nudNpcMax.Value;

                    this.rdungeon.Floors[floor].Npcs.Clear();
                    for (int npc = 0; npc < this.npcList.Count; npc++)
                    {
                        MapNpcSettings newNpc = new MapNpcSettings();
                        newNpc.NpcNum = this.npcList[npc].NpcNum;
                        newNpc.MinLevel = this.npcList[npc].MinLevel;
                        newNpc.MaxLevel = this.npcList[npc].MaxLevel;
                        newNpc.AppearanceRate = this.npcList[npc].AppearanceRate;
                        newNpc.StartStatus = this.npcList[npc].StartStatus;
                        newNpc.StartStatusCounter = this.npcList[npc].StartStatusCounter;
                        newNpc.StartStatusChance = this.npcList[npc].StartStatusChance;

                        this.rdungeon.Floors[floor].Npcs.Add(newNpc);
                    }
                }

                this.lblSaveLoadMessage.Text = "Npc settings saved to floor(s)";
            }
            else if (this.pnlRDungeonTraps != null && this.pnlRDungeonTraps.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].SpecialTiles.Clear();
                    for (int traps = 0; traps < this.trapList.Count; traps++)
                    {
                        EditableRDungeonTrap newTile = new EditableRDungeonTrap();

                        newTile.SpecialTile.Ground = this.trapList[traps].SpecialTile.Ground;
                        newTile.SpecialTile.GroundAnim = this.trapList[traps].SpecialTile.GroundAnim;
                        newTile.SpecialTile.Mask = this.trapList[traps].SpecialTile.Mask;
                        newTile.SpecialTile.Anim = this.trapList[traps].SpecialTile.Anim;
                        newTile.SpecialTile.Mask2 = this.trapList[traps].SpecialTile.Mask2;
                        newTile.SpecialTile.M2Anim = this.trapList[traps].SpecialTile.M2Anim;
                        newTile.SpecialTile.Fringe = this.trapList[traps].SpecialTile.Fringe;
                        newTile.SpecialTile.FAnim = this.trapList[traps].SpecialTile.FAnim;
                        newTile.SpecialTile.Fringe2 = this.trapList[traps].SpecialTile.Fringe2;
                        newTile.SpecialTile.F2Anim = this.trapList[traps].SpecialTile.F2Anim;

                        newTile.SpecialTile.GroundSet = this.trapList[traps].SpecialTile.GroundSet;
                        newTile.SpecialTile.GroundAnimSet = this.trapList[traps].SpecialTile.GroundAnimSet;
                        newTile.SpecialTile.MaskSet = this.trapList[traps].SpecialTile.MaskSet;
                        newTile.SpecialTile.AnimSet = this.trapList[traps].SpecialTile.AnimSet;
                        newTile.SpecialTile.Mask2Set = this.trapList[traps].SpecialTile.Mask2Set;
                        newTile.SpecialTile.M2AnimSet = this.trapList[traps].SpecialTile.M2AnimSet;
                        newTile.SpecialTile.FringeSet = this.trapList[traps].SpecialTile.FringeSet;
                        newTile.SpecialTile.FAnimSet = this.trapList[traps].SpecialTile.FAnimSet;
                        newTile.SpecialTile.Fringe2Set = this.trapList[traps].SpecialTile.Fringe2Set;
                        newTile.SpecialTile.F2AnimSet = this.trapList[traps].SpecialTile.F2AnimSet;

                        newTile.SpecialTile.Type = this.trapList[traps].SpecialTile.Type;
                        newTile.SpecialTile.Data1 = this.trapList[traps].SpecialTile.Data1;
                        newTile.SpecialTile.Data2 = this.trapList[traps].SpecialTile.Data2;
                        newTile.SpecialTile.Data3 = this.trapList[traps].SpecialTile.Data3;
                        newTile.SpecialTile.String1 = this.trapList[traps].SpecialTile.String1;
                        newTile.SpecialTile.String2 = this.trapList[traps].SpecialTile.String2;
                        newTile.SpecialTile.String3 = this.trapList[traps].SpecialTile.String3;

                        newTile.AppearanceRate = this.trapList[traps].AppearanceRate;

                        this.rdungeon.Floors[floor].SpecialTiles.Add(newTile);
                    }
                }

                this.lblSaveLoadMessage.Text = "Trap settings saved to floor(s)";
            }
            else if (this.pnlRDungeonWeather != null && this.pnlRDungeonWeather.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].Weather.Clear();
                    for (int weather = 0; weather < this.lbxWeather.Items.Count; weather++)
                    {
                        string[] weatherindex = this.lbxWeather.Items[weather].TextIdentifier.Split(':');
                        if (weatherindex[1].IsNumeric())
                        {
                            this.rdungeon.Floors[floor].Weather.Add((Enums.Weather)weatherindex[1].ToInt());
                        }
                    }
                }

                this.lblSaveLoadMessage.Text = "Weather settings saved to floor(s)";
            }
            else if (this.pnlRDungeonGoal != null && this.pnlRDungeonGoal.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    if (this.optNextFloor.Checked)
                    {
                        this.rdungeon.Floors[floor].GoalType = Enums.RFloorGoalType.NextFloor;
                    }
                    else if (this.optMap.Checked)
                    {
                        this.rdungeon.Floors[floor].GoalType = Enums.RFloorGoalType.Map;
                    }
                    else if (this.optScripted.Checked)
                    {
                        this.rdungeon.Floors[floor].GoalType = Enums.RFloorGoalType.Scripted;
                    }
else
                    {
                        this.rdungeon.Floors[floor].GoalType = Enums.RFloorGoalType.NextFloor;
                    }

                    this.rdungeon.Floors[floor].GoalMap = this.nudData1.Value;
                    this.rdungeon.Floors[floor].GoalX = this.nudData2.Value;
                    this.rdungeon.Floors[floor].GoalY = this.nudData3.Value;
                }

                this.lblSaveLoadMessage.Text = "Goal settings saved to floor(s)";
            }
            else if (this.pnlRDungeonChambers != null && this.pnlRDungeonChambers.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].Chambers.Clear();
                    for (int chamber = 0; chamber < this.chamberList.Count; chamber++)
                    {
                        EditableRDungeonChamber addedChamber = new EditableRDungeonChamber();
                        addedChamber.ChamberNum = this.chamberList[chamber].ChamberNum;
                        addedChamber.String1 = this.chamberList[chamber].String1;
                        addedChamber.String2 = this.chamberList[chamber].String2;
                        addedChamber.String3 = this.chamberList[chamber].String3;
                        this.rdungeon.Floors[floor].Chambers.Add(addedChamber);
                    }
                }

                this.lblSaveLoadMessage.Text = "Chamber settings saved to floor(s)";
            }
            else if (this.pnlRDungeonMisc != null && this.pnlRDungeonMisc.Visible == true)
            {
                for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                {
                    this.rdungeon.Floors[floor].Darkness = this.nudDarkness.Value;
                    if (this.lbxMusic.SelectedItems.Count != 1 || this.lbxMusic.Items[0].Selected)
                    {
                        this.rdungeon.Floors[floor].Music = string.Empty;
                    }
else
                    {
                        this.rdungeon.Floors[floor].Music = this.lbxMusic.SelectedItems[0].TextIdentifier;
                    }
                }

                this.lblSaveLoadMessage.Text = "Misc settings saved to floor(s)";
            }
        }

        private void BtnLoadFloor_Click(object sender, MouseButtonEventArgs e)
        {
            for (int i = this.rdungeon.Floors.Count; i < this.nudMaxFloors.Value; i++)
            {
                this.rdungeon.Floors.Add(new EditableRDungeonFloor());
            }

            if (this.nudFirstFloor.Value > this.nudMaxFloors.Value)
            {
                this.lblSaveLoadMessage.Text = "Cannot load floor above the maximum.";
                return;
            }

            EditableRDungeonFloor loadingfloor = this.rdungeon.Floors[this.nudFirstFloor.Value - 1];

            if (this.pnlRDungeonFloorSettingSelection.Visible == true)
            {
                this.LoadpnlRDungeonStructure();
                this.LoadpnlRDungeonLandTiles();
                this.LoadpnlRDungeonLandAltTiles();
                this.LoadpnlRDungeonWaterTiles();
                this.LoadpnlRDungeonWaterAnimTiles();
                this.LoadpnlRDungeonAttributes();
                this.LoadpnlRDungeonItems();
                this.LoadpnlRDungeonNpcs();
                this.LoadpnlRDungeonTraps();
                this.LoadpnlRDungeonWeather();
                this.LoadpnlRDungeonGoal();
                this.LoadpnlRDungeonChambers();
                this.LoadpnlRDungeonMisc();

                // Structure
                this.nudTrapMin.Value = loadingfloor.TrapMin;
                this.nudTrapMax.Value = loadingfloor.TrapMax;
                this.nudItemMin.Value = loadingfloor.ItemMin;
                this.nudItemMax.Value = loadingfloor.ItemMax;
                this.nudRoomWidthMin.Value = loadingfloor.RoomWidthMin;
                this.nudRoomWidthMax.Value = loadingfloor.RoomWidthMax;
                this.nudRoomLengthMin.Value = loadingfloor.RoomLengthMin;
                this.nudRoomLengthMax.Value = loadingfloor.RoomLengthMax;
                this.nudHallTurnMin.Value = loadingfloor.HallTurnMin;
                this.nudHallTurnMax.Value = loadingfloor.HallTurnMax;
                this.nudHallVarMin.Value = loadingfloor.HallVarMin;
                this.nudHallVarMax.Value = loadingfloor.HallVarMax;
                this.nudWaterFrequency.Value = loadingfloor.WaterFrequency;
                this.nudCraters.Value = loadingfloor.Craters;
                this.nudCraterMinLength.Value = loadingfloor.CraterMinLength;
                this.nudCraterMaxLength.Value = loadingfloor.CraterMaxLength;
                this.chkCraterFuzzy.Checked = loadingfloor.CraterFuzzy;

                // Land Tiles
                this.landTileNumbers[0, 19] = loadingfloor.StairsSheet;
                this.landTileNumbers[0, 16] = loadingfloor.MGroundSheet;
                this.landTileNumbers[0, 10] = loadingfloor.MTopLeftSheet;
                this.landTileNumbers[0, 6] = loadingfloor.MTopCenterSheet;
                this.landTileNumbers[0, 2] = loadingfloor.MTopRightSheet;
                this.landTileNumbers[0, 9] = loadingfloor.MCenterLeftSheet;
                this.landTileNumbers[0, 5] = loadingfloor.MCenterCenterSheet;
                this.landTileNumbers[0, 1] = loadingfloor.MCenterRightSheet;
                this.landTileNumbers[0, 8] = loadingfloor.MBottomLeftSheet;
                this.landTileNumbers[0, 4] = loadingfloor.MBottomCenterSheet;
                this.landTileNumbers[0, 0] = loadingfloor.MBottomRightSheet;
                this.landTileNumbers[0, 17] = loadingfloor.MInnerTopLeftSheet;
                this.landTileNumbers[0, 20] = loadingfloor.MInnerTopRightSheet;
                this.landTileNumbers[0, 18] = loadingfloor.MInnerBottomLeftSheet;
                this.landTileNumbers[0, 21] = loadingfloor.MInnerBottomRightSheet;
                this.landTileNumbers[0, 15] = loadingfloor.MIsolatedWallSheet;
                this.landTileNumbers[0, 12] = loadingfloor.MColumnTopSheet;
                this.landTileNumbers[0, 13] = loadingfloor.MColumnCenterSheet;
                this.landTileNumbers[0, 14] = loadingfloor.MColumnBottomSheet;
                this.landTileNumbers[0, 3] = loadingfloor.MRowLeftSheet;
                this.landTileNumbers[0, 7] = loadingfloor.MRowCenterSheet;
                this.landTileNumbers[0, 11] = loadingfloor.MRowRightSheet;

                this.landTileNumbers[1, 19] = loadingfloor.StairsX;
                this.landTileNumbers[1, 16] = loadingfloor.MGroundX;
                this.landTileNumbers[1, 10] = loadingfloor.MTopLeftX;
                this.landTileNumbers[1, 6] = loadingfloor.MTopCenterX;
                this.landTileNumbers[1, 2] = loadingfloor.MTopRightX;
                this.landTileNumbers[1, 9] = loadingfloor.MCenterLeftX;
                this.landTileNumbers[1, 5] = loadingfloor.MCenterCenterX;
                this.landTileNumbers[1, 1] = loadingfloor.MCenterRightX;
                this.landTileNumbers[1, 8] = loadingfloor.MBottomLeftX;
                this.landTileNumbers[1, 4] = loadingfloor.MBottomCenterX;
                this.landTileNumbers[1, 0] = loadingfloor.MBottomRightX;
                this.landTileNumbers[1, 17] = loadingfloor.MInnerTopLeftX;
                this.landTileNumbers[1, 20] = loadingfloor.MInnerTopRightX;
                this.landTileNumbers[1, 18] = loadingfloor.MInnerBottomLeftX;
                this.landTileNumbers[1, 21] = loadingfloor.MInnerBottomRightX;
                this.landTileNumbers[1, 15] = loadingfloor.MIsolatedWallX;
                this.landTileNumbers[1, 12] = loadingfloor.MColumnTopX;
                this.landTileNumbers[1, 13] = loadingfloor.MColumnCenterX;
                this.landTileNumbers[1, 14] = loadingfloor.MColumnBottomX;
                this.landTileNumbers[1, 3] = loadingfloor.MRowLeftX;
                this.landTileNumbers[1, 7] = loadingfloor.MRowCenterX;
                this.landTileNumbers[1, 11] = loadingfloor.MRowRightX;

                for (int i = 0; i < 22; i++)
                {
                    this.picLandTileset[i].Image = Graphic.GraphicsManager.Tiles[this.landTileNumbers[0, i]][this.landTileNumbers[1, i]];
                }

                // Land Alt Tiles
                this.landAltTileNumbers[0, 19] = loadingfloor.MGroundAlt2Sheet;
                this.landAltTileNumbers[0, 16] = loadingfloor.MGroundAltSheet;
                this.landAltTileNumbers[0, 10] = loadingfloor.MTopLeftAltSheet;
                this.landAltTileNumbers[0, 6] = loadingfloor.MTopCenterAltSheet;
                this.landAltTileNumbers[0, 2] = loadingfloor.MTopRightAltSheet;
                this.landAltTileNumbers[0, 9] = loadingfloor.MCenterLeftAltSheet;
                this.landAltTileNumbers[0, 5] = loadingfloor.MCenterCenterAltSheet;
                this.landAltTileNumbers[0, 22] = loadingfloor.MCenterCenterAlt2Sheet;
                this.landAltTileNumbers[0, 1] = loadingfloor.MCenterRightAltSheet;
                this.landAltTileNumbers[0, 8] = loadingfloor.MBottomLeftAltSheet;
                this.landAltTileNumbers[0, 4] = loadingfloor.MBottomCenterAltSheet;
                this.landAltTileNumbers[0, 0] = loadingfloor.MBottomRightAltSheet;
                this.landAltTileNumbers[0, 17] = loadingfloor.MInnerTopLeftAltSheet;
                this.landAltTileNumbers[0, 20] = loadingfloor.MInnerTopRightAltSheet;
                this.landAltTileNumbers[0, 18] = loadingfloor.MInnerBottomLeftAltSheet;
                this.landAltTileNumbers[0, 21] = loadingfloor.MInnerBottomRightAltSheet;
                this.landAltTileNumbers[0, 15] = loadingfloor.MIsolatedWallAltSheet;
                this.landAltTileNumbers[0, 12] = loadingfloor.MColumnTopAltSheet;
                this.landAltTileNumbers[0, 13] = loadingfloor.MColumnCenterAltSheet;
                this.landAltTileNumbers[0, 14] = loadingfloor.MColumnBottomAltSheet;
                this.landAltTileNumbers[0, 3] = loadingfloor.MRowLeftAltSheet;
                this.landAltTileNumbers[0, 7] = loadingfloor.MRowCenterAltSheet;
                this.landAltTileNumbers[0, 11] = loadingfloor.MRowRightAltSheet;

                this.landAltTileNumbers[1, 19] = loadingfloor.MGroundAlt2X;
                this.landAltTileNumbers[1, 16] = loadingfloor.MGroundAltX;
                this.landAltTileNumbers[1, 10] = loadingfloor.MTopLeftAltX;
                this.landAltTileNumbers[1, 6] = loadingfloor.MTopCenterAltX;
                this.landAltTileNumbers[1, 2] = loadingfloor.MTopRightAltX;
                this.landAltTileNumbers[1, 9] = loadingfloor.MCenterLeftAltX;
                this.landAltTileNumbers[1, 5] = loadingfloor.MCenterCenterAltX;
                this.landAltTileNumbers[1, 22] = loadingfloor.MCenterCenterAlt2X;
                this.landAltTileNumbers[1, 1] = loadingfloor.MCenterRightAltX;
                this.landAltTileNumbers[1, 8] = loadingfloor.MBottomLeftAltX;
                this.landAltTileNumbers[1, 4] = loadingfloor.MBottomCenterAltX;
                this.landAltTileNumbers[1, 0] = loadingfloor.MBottomRightAltX;
                this.landAltTileNumbers[1, 17] = loadingfloor.MInnerTopLeftAltX;
                this.landAltTileNumbers[1, 20] = loadingfloor.MInnerTopRightAltX;
                this.landAltTileNumbers[1, 18] = loadingfloor.MInnerBottomLeftAltX;
                this.landAltTileNumbers[1, 21] = loadingfloor.MInnerBottomRightAltX;
                this.landAltTileNumbers[1, 15] = loadingfloor.MIsolatedWallAltX;
                this.landAltTileNumbers[1, 12] = loadingfloor.MColumnTopAltX;
                this.landAltTileNumbers[1, 13] = loadingfloor.MColumnCenterAltX;
                this.landAltTileNumbers[1, 14] = loadingfloor.MColumnBottomAltX;
                this.landAltTileNumbers[1, 3] = loadingfloor.MRowLeftAltX;
                this.landAltTileNumbers[1, 7] = loadingfloor.MRowCenterAltX;
                this.landAltTileNumbers[1, 11] = loadingfloor.MRowRightAltX;

                for (int i = 0; i < 23; i++)
                {
                    this.picLandAltTileset[i].Image = Graphic.GraphicsManager.Tiles[this.landAltTileNumbers[0, i]][this.landAltTileNumbers[1, i]];
                }

                // Water Tiles
                this.waterTileNumbers[0, 15] = loadingfloor.MShoreSurroundedSheet;
                this.waterTileNumbers[0, 0] = loadingfloor.MShoreInnerTopLeftSheet;
                this.waterTileNumbers[0, 4] = loadingfloor.MShoreTopSheet;
                this.waterTileNumbers[0, 8] = loadingfloor.MShoreInnerTopRightSheet;
                this.waterTileNumbers[0, 1] = loadingfloor.MShoreLeftSheet;
                this.waterTileNumbers[0, 5] = loadingfloor.MWaterSheet;
                this.waterTileNumbers[0, 9] = loadingfloor.MShoreRightSheet;
                this.waterTileNumbers[0, 2] = loadingfloor.MShoreInnerBottomLeftSheet;
                this.waterTileNumbers[0, 6] = loadingfloor.MShoreBottomSheet;
                this.waterTileNumbers[0, 10] = loadingfloor.MShoreInnerBottomRightSheet;
                this.waterTileNumbers[0, 16] = loadingfloor.MShoreTopLeftSheet;
                this.waterTileNumbers[0, 19] = loadingfloor.MShoreTopRightSheet;
                this.waterTileNumbers[0, 17] = loadingfloor.MShoreBottomLeftSheet;
                this.waterTileNumbers[0, 20] = loadingfloor.MShoreBottomRightSheet;
                this.waterTileNumbers[0, 18] = loadingfloor.MShoreDiagonalForwardSheet;
                this.waterTileNumbers[0, 21] = loadingfloor.MShoreDiagonalBackSheet;
                this.waterTileNumbers[0, 12] = loadingfloor.MShoreInnerTopSheet;
                this.waterTileNumbers[0, 13] = loadingfloor.MShoreVerticalSheet;
                this.waterTileNumbers[0, 14] = loadingfloor.MShoreInnerBottomSheet;
                this.waterTileNumbers[0, 3] = loadingfloor.MShoreInnerLeftSheet;
                this.waterTileNumbers[0, 7] = loadingfloor.MShoreHorizontalSheet;
                this.waterTileNumbers[0, 11] = loadingfloor.MShoreInnerRightSheet;

                this.waterTileNumbers[1, 15] = loadingfloor.MShoreSurroundedX;
                this.waterTileNumbers[1, 0] = loadingfloor.MShoreInnerTopLeftX;
                this.waterTileNumbers[1, 4] = loadingfloor.MShoreTopX;
                this.waterTileNumbers[1, 8] = loadingfloor.MShoreInnerTopRightX;
                this.waterTileNumbers[1, 1] = loadingfloor.MShoreLeftX;
                this.waterTileNumbers[1, 5] = loadingfloor.MWaterX;
                this.waterTileNumbers[1, 9] = loadingfloor.MShoreRightX;
                this.waterTileNumbers[1, 2] = loadingfloor.MShoreInnerBottomLeftX;
                this.waterTileNumbers[1, 6] = loadingfloor.MShoreBottomX;
                this.waterTileNumbers[1, 10] = loadingfloor.MShoreInnerBottomRightX;
                this.waterTileNumbers[1, 16] = loadingfloor.MShoreTopLeftX;
                this.waterTileNumbers[1, 19] = loadingfloor.MShoreTopRightX;
                this.waterTileNumbers[1, 17] = loadingfloor.MShoreBottomLeftX;
                this.waterTileNumbers[1, 20] = loadingfloor.MShoreBottomRightX;
                this.waterTileNumbers[1, 18] = loadingfloor.MShoreDiagonalForwardX;
                this.waterTileNumbers[1, 21] = loadingfloor.MShoreDiagonalBackX;
                this.waterTileNumbers[1, 12] = loadingfloor.MShoreInnerTopX;
                this.waterTileNumbers[1, 13] = loadingfloor.MShoreVerticalX;
                this.waterTileNumbers[1, 14] = loadingfloor.MShoreInnerBottomX;
                this.waterTileNumbers[1, 3] = loadingfloor.MShoreInnerLeftX;
                this.waterTileNumbers[1, 7] = loadingfloor.MShoreHorizontalX;
                this.waterTileNumbers[1, 11] = loadingfloor.MShoreInnerRightX;

                for (int i = 0; i < 22; i++)
                {
                    this.picWaterTileset[i].Image = Graphic.GraphicsManager.Tiles[this.waterTileNumbers[0, i]][this.waterTileNumbers[1, i]];
                }

                // Water Anim Tiles
                this.waterAnimTileNumbers[0, 15] = loadingfloor.MShoreSurroundedAnimSheet;
                this.waterAnimTileNumbers[0, 0] = loadingfloor.MShoreInnerTopLeftAnimSheet;
                this.waterAnimTileNumbers[0, 4] = loadingfloor.MShoreTopAnimSheet;
                this.waterAnimTileNumbers[0, 8] = loadingfloor.MShoreInnerTopRightAnimSheet;
                this.waterAnimTileNumbers[0, 1] = loadingfloor.MShoreLeftAnimSheet;
                this.waterAnimTileNumbers[0, 5] = loadingfloor.MWaterAnimSheet;
                this.waterAnimTileNumbers[0, 9] = loadingfloor.MShoreRightAnimSheet;
                this.waterAnimTileNumbers[0, 2] = loadingfloor.MShoreInnerBottomLeftAnimSheet;
                this.waterAnimTileNumbers[0, 6] = loadingfloor.MShoreBottomAnimSheet;
                this.waterAnimTileNumbers[0, 10] = loadingfloor.MShoreInnerBottomRightAnimSheet;
                this.waterAnimTileNumbers[0, 16] = loadingfloor.MShoreTopLeftAnimSheet;
                this.waterAnimTileNumbers[0, 19] = loadingfloor.MShoreTopRightAnimSheet;
                this.waterAnimTileNumbers[0, 17] = loadingfloor.MShoreBottomLeftAnimSheet;
                this.waterAnimTileNumbers[0, 20] = loadingfloor.MShoreBottomRightAnimSheet;
                this.waterAnimTileNumbers[0, 18] = loadingfloor.MShoreDiagonalForwardAnimSheet;
                this.waterAnimTileNumbers[0, 21] = loadingfloor.MShoreDiagonalBackAnimSheet;
                this.waterAnimTileNumbers[0, 12] = loadingfloor.MShoreInnerTopAnimSheet;
                this.waterAnimTileNumbers[0, 13] = loadingfloor.MShoreVerticalAnimSheet;
                this.waterAnimTileNumbers[0, 14] = loadingfloor.MShoreInnerBottomAnimSheet;
                this.waterAnimTileNumbers[0, 3] = loadingfloor.MShoreInnerLeftAnimSheet;
                this.waterAnimTileNumbers[0, 7] = loadingfloor.MShoreHorizontalAnimSheet;
                this.waterAnimTileNumbers[0, 11] = loadingfloor.MShoreInnerRightAnimSheet;

                this.waterAnimTileNumbers[1, 15] = loadingfloor.MShoreSurroundedAnimX;
                this.waterAnimTileNumbers[1, 0] = loadingfloor.MShoreInnerTopLeftAnimX;
                this.waterAnimTileNumbers[1, 4] = loadingfloor.MShoreTopAnimX;
                this.waterAnimTileNumbers[1, 8] = loadingfloor.MShoreInnerTopRightAnimX;
                this.waterAnimTileNumbers[1, 1] = loadingfloor.MShoreLeftAnimX;
                this.waterAnimTileNumbers[1, 5] = loadingfloor.MWaterAnimX;
                this.waterAnimTileNumbers[1, 9] = loadingfloor.MShoreRightAnimX;
                this.waterAnimTileNumbers[1, 2] = loadingfloor.MShoreInnerBottomLeftAnimX;
                this.waterAnimTileNumbers[1, 6] = loadingfloor.MShoreBottomAnimX;
                this.waterAnimTileNumbers[1, 10] = loadingfloor.MShoreInnerBottomRightAnimX;
                this.waterAnimTileNumbers[1, 16] = loadingfloor.MShoreTopLeftAnimX;
                this.waterAnimTileNumbers[1, 19] = loadingfloor.MShoreTopRightAnimX;
                this.waterAnimTileNumbers[1, 17] = loadingfloor.MShoreBottomLeftAnimX;
                this.waterAnimTileNumbers[1, 20] = loadingfloor.MShoreBottomRightAnimX;
                this.waterAnimTileNumbers[1, 18] = loadingfloor.MShoreDiagonalForwardAnimX;
                this.waterAnimTileNumbers[1, 21] = loadingfloor.MShoreDiagonalBackAnimX;
                this.waterAnimTileNumbers[1, 12] = loadingfloor.MShoreInnerTopAnimX;
                this.waterAnimTileNumbers[1, 13] = loadingfloor.MShoreVerticalAnimX;
                this.waterAnimTileNumbers[1, 14] = loadingfloor.MShoreInnerBottomAnimX;
                this.waterAnimTileNumbers[1, 3] = loadingfloor.MShoreInnerLeftAnimX;
                this.waterAnimTileNumbers[1, 7] = loadingfloor.MShoreHorizontalAnimX;
                this.waterAnimTileNumbers[1, 11] = loadingfloor.MShoreInnerRightAnimX;

                for (int i = 0; i < 22; i++)
                {
                    this.picWaterAnimTileset[i].Image = Graphic.GraphicsManager.Tiles[this.waterAnimTileNumbers[0, i]][this.waterAnimTileNumbers[1, i]];
                }

                // Attributes
                this.cbGroundType.SelectItem((int)loadingfloor.GroundTile.Type);
                this.nudGroundData1.Value = loadingfloor.GroundTile.Data1;
                this.nudGroundData2.Value = loadingfloor.GroundTile.Data2;
                this.nudGroundData3.Value = loadingfloor.GroundTile.Data3;
                this.txtGroundString1.Text = loadingfloor.GroundTile.String1;
                this.txtGroundString2.Text = loadingfloor.GroundTile.String2;
                this.txtGroundString3.Text = loadingfloor.GroundTile.String3;

                this.cbHallType.SelectItem((int)loadingfloor.HallTile.Type);
                this.nudHallData1.Value = loadingfloor.HallTile.Data1;
                this.nudHallData2.Value = loadingfloor.HallTile.Data2;
                this.nudHallData3.Value = loadingfloor.HallTile.Data3;
                this.txtHallString1.Text = loadingfloor.HallTile.String1;
                this.txtHallString2.Text = loadingfloor.HallTile.String2;
                this.txtHallString3.Text = loadingfloor.HallTile.String3;

                this.cbWaterType.SelectItem((int)loadingfloor.WaterTile.Type);
                this.nudWaterData1.Value = loadingfloor.WaterTile.Data1;
                this.nudWaterData2.Value = loadingfloor.WaterTile.Data2;
                this.nudWaterData3.Value = loadingfloor.WaterTile.Data3;
                this.txtWaterString1.Text = loadingfloor.WaterTile.String1;
                this.txtWaterString2.Text = loadingfloor.WaterTile.String2;
                this.txtWaterString3.Text = loadingfloor.WaterTile.String3;

                this.cbWallType.SelectItem((int)loadingfloor.WallTile.Type);
                this.nudWallData1.Value = loadingfloor.WallTile.Data1;
                this.nudWallData2.Value = loadingfloor.WallTile.Data2;
                this.nudWallData3.Value = loadingfloor.WallTile.Data3;
                this.txtWallString1.Text = loadingfloor.WallTile.String1;
                this.txtWallString2.Text = loadingfloor.WallTile.String2;
                this.txtWallString3.Text = loadingfloor.WallTile.String3;

                // Items
                this.itemList.Clear();
                this.lbxItems.Items.Clear();
                for (int item = 0; item < loadingfloor.Items.Count; item++)
                {
                    EditableRDungeonItem newItem = new EditableRDungeonItem();
                    newItem.ItemNum = loadingfloor.Items[item].ItemNum;
                    newItem.MinAmount = loadingfloor.Items[item].MinAmount;
                    newItem.MaxAmount = loadingfloor.Items[item].MaxAmount;
                    newItem.AppearanceRate = loadingfloor.Items[item].AppearanceRate;
                    newItem.StickyRate = loadingfloor.Items[item].StickyRate;
                    newItem.Tag = loadingfloor.Items[item].Tag;
                    newItem.Hidden = loadingfloor.Items[item].Hidden;
                    newItem.OnGround = loadingfloor.Items[item].OnGround;
                    newItem.OnWater = loadingfloor.Items[item].OnWater;
                    newItem.OnWall = loadingfloor.Items[item].OnWall;

                    this.itemList.Add(newItem);
                    this.lbxItems.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (item + 1) + ": (" + newItem.AppearanceRate + "%) " + newItem.MinAmount + "-" + newItem.MaxAmount + " " + Items.ItemHelper.Items[newItem.ItemNum].Name + " (" + newItem.StickyRate + "% Sticky)"));
                }

                // Npcs
                this.nudNpcSpawnTime.Value = loadingfloor.NpcSpawnTime;
                this.nudNpcMin.Value = loadingfloor.NpcMin;
                this.nudNpcMax.Value = loadingfloor.NpcMax;

                this.npcList.Clear();
                this.lbxNpcs.Items.Clear();
                for (int npc = 0; npc < loadingfloor.Npcs.Count; npc++)
                {
                    MapNpcSettings newNpc = new MapNpcSettings();
                    newNpc.NpcNum = loadingfloor.Npcs[npc].NpcNum;
                    newNpc.MinLevel = loadingfloor.Npcs[npc].MinLevel;
                    newNpc.MaxLevel = loadingfloor.Npcs[npc].MaxLevel;
                    newNpc.AppearanceRate = loadingfloor.Npcs[npc].AppearanceRate;
                    newNpc.StartStatus = loadingfloor.Npcs[npc].StartStatus;
                    newNpc.StartStatusCounter = loadingfloor.Npcs[npc].StartStatusCounter;
                    newNpc.StartStatusChance = loadingfloor.Npcs[npc].StartStatusChance;

                    this.npcList.Add(newNpc);
                    this.lbxNpcs.Items.Add(new ListBoxTextItem(
                        Graphic.FontManager.LoadFont("tahoma", 10),
                        (npc + 1) + ": (" + newNpc.AppearanceRate + "%) " + "Lv." + newNpc.MinLevel + "-" + newNpc.MaxLevel + " " + Npc.NpcHelper.Npcs[newNpc.NpcNum].Name + " [" + newNpc.StartStatusChance + "% " + newNpc.StartStatus.ToString() + "]"));
                }

                // Traps
                this.trapList.Clear();
                this.lbxTraps.Items.Clear();
                for (int traps = 0; traps < loadingfloor.SpecialTiles.Count; traps++)
                {
                    EditableRDungeonTrap newTile = new EditableRDungeonTrap();

                    newTile.SpecialTile.Ground = loadingfloor.SpecialTiles[traps].SpecialTile.Ground;
                    newTile.SpecialTile.GroundAnim = loadingfloor.SpecialTiles[traps].SpecialTile.GroundAnim;
                    newTile.SpecialTile.Mask = loadingfloor.SpecialTiles[traps].SpecialTile.Mask;
                    newTile.SpecialTile.Anim = loadingfloor.SpecialTiles[traps].SpecialTile.Anim;
                    newTile.SpecialTile.Mask2 = loadingfloor.SpecialTiles[traps].SpecialTile.Mask2;
                    newTile.SpecialTile.M2Anim = loadingfloor.SpecialTiles[traps].SpecialTile.M2Anim;
                    newTile.SpecialTile.Fringe = loadingfloor.SpecialTiles[traps].SpecialTile.Fringe;
                    newTile.SpecialTile.FAnim = loadingfloor.SpecialTiles[traps].SpecialTile.FAnim;
                    newTile.SpecialTile.Fringe2 = loadingfloor.SpecialTiles[traps].SpecialTile.Fringe2;
                    newTile.SpecialTile.F2Anim = loadingfloor.SpecialTiles[traps].SpecialTile.F2Anim;

                    newTile.SpecialTile.GroundSet = loadingfloor.SpecialTiles[traps].SpecialTile.GroundSet;
                    newTile.SpecialTile.GroundAnimSet = loadingfloor.SpecialTiles[traps].SpecialTile.GroundAnimSet;
                    newTile.SpecialTile.MaskSet = loadingfloor.SpecialTiles[traps].SpecialTile.MaskSet;
                    newTile.SpecialTile.AnimSet = loadingfloor.SpecialTiles[traps].SpecialTile.AnimSet;
                    newTile.SpecialTile.Mask2Set = loadingfloor.SpecialTiles[traps].SpecialTile.Mask2Set;
                    newTile.SpecialTile.M2AnimSet = loadingfloor.SpecialTiles[traps].SpecialTile.M2AnimSet;
                    newTile.SpecialTile.FringeSet = loadingfloor.SpecialTiles[traps].SpecialTile.FringeSet;
                    newTile.SpecialTile.FAnimSet = loadingfloor.SpecialTiles[traps].SpecialTile.FAnimSet;
                    newTile.SpecialTile.Fringe2Set = loadingfloor.SpecialTiles[traps].SpecialTile.Fringe2Set;
                    newTile.SpecialTile.F2AnimSet = loadingfloor.SpecialTiles[traps].SpecialTile.F2AnimSet;

                    newTile.SpecialTile.Type = loadingfloor.SpecialTiles[traps].SpecialTile.Type;
                    newTile.SpecialTile.Data1 = loadingfloor.SpecialTiles[traps].SpecialTile.Data1;
                    newTile.SpecialTile.Data2 = loadingfloor.SpecialTiles[traps].SpecialTile.Data2;
                    newTile.SpecialTile.Data3 = loadingfloor.SpecialTiles[traps].SpecialTile.Data3;
                    newTile.SpecialTile.String1 = loadingfloor.SpecialTiles[traps].SpecialTile.String1;
                    newTile.SpecialTile.String2 = loadingfloor.SpecialTiles[traps].SpecialTile.String2;
                    newTile.SpecialTile.String3 = loadingfloor.SpecialTiles[traps].SpecialTile.String3;

                    newTile.AppearanceRate = loadingfloor.SpecialTiles[traps].AppearanceRate;

                    this.trapList.Add(newTile);
                    this.lbxTraps.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (traps + 1) + ": " + newTile.SpecialTile.Type + "/" + newTile.SpecialTile.Data1 + "/" + newTile.SpecialTile.Data2 + "/" + newTile.SpecialTile.Data3));
                }

                // Weather
                this.lbxWeather.Items.Clear();
                for (int weather = 0; weather < loadingfloor.Weather.Count; weather++)
                {
                    this.lbxWeather.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (weather + 1) + ":" + (int)loadingfloor.Weather[weather] + ": " + Enum.GetName(typeof(Enums.Weather), loadingfloor.Weather[weather])));
                }

                // Goal
                switch (loadingfloor.GoalType)
                {
                    case Enums.RFloorGoalType.NextFloor:
                    {
                            this.optNextFloor.Checked = true;
                        }

                        break;
                    case Enums.RFloorGoalType.Map:
                    {
                            this.optMap.Checked = true;
                        }

                        break;
                    case Enums.RFloorGoalType.Scripted:
                    {
                            this.optScripted.Checked = true;
                        }

                        break;
                    default:
                    {
                            this.optNextFloor.Checked = true;
                        }

                        break;
                }

                this.nudData1.Value = loadingfloor.GoalMap;
                this.nudData2.Value = loadingfloor.GoalX;
                this.nudData3.Value = loadingfloor.GoalY;

                // chambers
                this.chamberList.Clear();
                this.lbxChambers.Items.Clear();
                for (int chamber = 0; chamber < loadingfloor.Chambers.Count; chamber++)
                {
                    EditableRDungeonChamber newChamber = new EditableRDungeonChamber();
                    newChamber.ChamberNum = loadingfloor.Chambers[chamber].ChamberNum;
                    newChamber.String1 = loadingfloor.Chambers[chamber].String1;
                    newChamber.String2 = loadingfloor.Chambers[chamber].String2;
                    newChamber.String3 = loadingfloor.Chambers[chamber].String3;
                    this.chamberList.Add(newChamber);
                    this.lbxChambers.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), "#" + loadingfloor.Chambers[chamber].ChamberNum + "/" + loadingfloor.Chambers[chamber].String1 + "/" + loadingfloor.Chambers[chamber].String2 + "/" + loadingfloor.Chambers[chamber].String3));
                }

                // Misc
                this.nudDarkness.Value = loadingfloor.Darkness;
                if (loadingfloor.Music == string.Empty)
                {
                    this.lbxMusic.SelectItem(0);
                }
else
                {
                    for (int i = 0; i < this.lbxMusic.Items.Count; i++)
                    {
                        if (this.lbxMusic.Items[i].TextIdentifier == loadingfloor.Music)
                        {
                            this.lbxMusic.SelectItem(i);
                        }
                    }
                }

                this.lblSaveLoadMessage.Text = "All settings loaded";
            }
            else if (this.pnlRDungeonStructure != null && this.pnlRDungeonStructure.Visible == true)
            {
                this.nudTrapMin.Value = loadingfloor.TrapMin;
                this.nudTrapMax.Value = loadingfloor.TrapMax;
                this.nudItemMin.Value = loadingfloor.ItemMin;
                this.nudItemMax.Value = loadingfloor.ItemMax;
                this.nudRoomWidthMin.Value = loadingfloor.RoomWidthMin;
                this.nudRoomWidthMax.Value = loadingfloor.RoomWidthMax;
                this.nudRoomLengthMin.Value = loadingfloor.RoomLengthMin;
                this.nudRoomLengthMax.Value = loadingfloor.RoomLengthMax;
                this.nudHallTurnMin.Value = loadingfloor.HallTurnMin;
                this.nudHallTurnMax.Value = loadingfloor.HallTurnMax;
                this.nudHallVarMin.Value = loadingfloor.HallVarMin;
                this.nudHallVarMax.Value = loadingfloor.HallVarMax;
                this.nudWaterFrequency.Value = loadingfloor.WaterFrequency;
                this.nudCraters.Value = loadingfloor.Craters;
                this.nudCraterMinLength.Value = loadingfloor.CraterMinLength;
                this.nudCraterMaxLength.Value = loadingfloor.CraterMaxLength;
                this.chkCraterFuzzy.Checked = loadingfloor.CraterFuzzy;

                this.lblSaveLoadMessage.Text = "Structure settings loaded";
            }
            else if (this.pnlRDungeonLandTiles != null && this.pnlRDungeonLandTiles.Visible == true)
            {
                this.landTileNumbers[0, 19] = loadingfloor.StairsSheet;
                this.landTileNumbers[0, 16] = loadingfloor.MGroundSheet;
                this.landTileNumbers[0, 10] = loadingfloor.MTopLeftSheet;
                this.landTileNumbers[0, 6] = loadingfloor.MTopCenterSheet;
                this.landTileNumbers[0, 2] = loadingfloor.MTopRightSheet;
                this.landTileNumbers[0, 9] = loadingfloor.MCenterLeftSheet;
                this.landTileNumbers[0, 5] = loadingfloor.MCenterCenterSheet;
                this.landTileNumbers[0, 1] = loadingfloor.MCenterRightSheet;
                this.landTileNumbers[0, 8] = loadingfloor.MBottomLeftSheet;
                this.landTileNumbers[0, 4] = loadingfloor.MBottomCenterSheet;
                this.landTileNumbers[0, 0] = loadingfloor.MBottomRightSheet;
                this.landTileNumbers[0, 17] = loadingfloor.MInnerTopLeftSheet;
                this.landTileNumbers[0, 20] = loadingfloor.MInnerTopRightSheet;
                this.landTileNumbers[0, 18] = loadingfloor.MInnerBottomLeftSheet;
                this.landTileNumbers[0, 21] = loadingfloor.MInnerBottomRightSheet;
                this.landTileNumbers[0, 15] = loadingfloor.MIsolatedWallSheet;
                this.landTileNumbers[0, 12] = loadingfloor.MColumnTopSheet;
                this.landTileNumbers[0, 13] = loadingfloor.MColumnCenterSheet;
                this.landTileNumbers[0, 14] = loadingfloor.MColumnBottomSheet;
                this.landTileNumbers[0, 3] = loadingfloor.MRowLeftSheet;
                this.landTileNumbers[0, 7] = loadingfloor.MRowCenterSheet;
                this.landTileNumbers[0, 11] = loadingfloor.MRowRightSheet;

                this.landTileNumbers[1, 19] = loadingfloor.StairsX;
                this.landTileNumbers[1, 16] = loadingfloor.MGroundX;
                this.landTileNumbers[1, 10] = loadingfloor.MTopLeftX;
                this.landTileNumbers[1, 6] = loadingfloor.MTopCenterX;
                this.landTileNumbers[1, 2] = loadingfloor.MTopRightX;
                this.landTileNumbers[1, 9] = loadingfloor.MCenterLeftX;
                this.landTileNumbers[1, 5] = loadingfloor.MCenterCenterX;
                this.landTileNumbers[1, 1] = loadingfloor.MCenterRightX;
                this.landTileNumbers[1, 8] = loadingfloor.MBottomLeftX;
                this.landTileNumbers[1, 4] = loadingfloor.MBottomCenterX;
                this.landTileNumbers[1, 0] = loadingfloor.MBottomRightX;
                this.landTileNumbers[1, 17] = loadingfloor.MInnerTopLeftX;
                this.landTileNumbers[1, 20] = loadingfloor.MInnerTopRightX;
                this.landTileNumbers[1, 18] = loadingfloor.MInnerBottomLeftX;
                this.landTileNumbers[1, 21] = loadingfloor.MInnerBottomRightX;
                this.landTileNumbers[1, 15] = loadingfloor.MIsolatedWallX;
                this.landTileNumbers[1, 12] = loadingfloor.MColumnTopX;
                this.landTileNumbers[1, 13] = loadingfloor.MColumnCenterX;
                this.landTileNumbers[1, 14] = loadingfloor.MColumnBottomX;
                this.landTileNumbers[1, 3] = loadingfloor.MRowLeftX;
                this.landTileNumbers[1, 7] = loadingfloor.MRowCenterX;
                this.landTileNumbers[1, 11] = loadingfloor.MRowRightX;

                for (int i = 0; i < 22; i++)
                {
                    this.picLandTileset[i].Image = Graphic.GraphicsManager.Tiles[this.landTileNumbers[0, i]][this.landTileNumbers[1, i]];
                }

                this.lblSaveLoadMessage.Text = "Land Tile settings loaded";
            }
            else if (this.pnlRDungeonLandAltTiles != null && this.pnlRDungeonLandAltTiles.Visible == true)
            {
                this.landAltTileNumbers[0, 19] = loadingfloor.MGroundAlt2Sheet;
                this.landAltTileNumbers[0, 16] = loadingfloor.MGroundAltSheet;
                this.landAltTileNumbers[0, 10] = loadingfloor.MTopLeftAltSheet;
                this.landAltTileNumbers[0, 6] = loadingfloor.MTopCenterAltSheet;
                this.landAltTileNumbers[0, 2] = loadingfloor.MTopRightAltSheet;
                this.landAltTileNumbers[0, 9] = loadingfloor.MCenterLeftAltSheet;
                this.landAltTileNumbers[0, 5] = loadingfloor.MCenterCenterAltSheet;
                this.landAltTileNumbers[0, 22] = loadingfloor.MCenterCenterAlt2Sheet;
                this.landAltTileNumbers[0, 1] = loadingfloor.MCenterRightAltSheet;
                this.landAltTileNumbers[0, 8] = loadingfloor.MBottomLeftAltSheet;
                this.landAltTileNumbers[0, 4] = loadingfloor.MBottomCenterAltSheet;
                this.landAltTileNumbers[0, 0] = loadingfloor.MBottomRightAltSheet;
                this.landAltTileNumbers[0, 17] = loadingfloor.MInnerTopLeftAltSheet;
                this.landAltTileNumbers[0, 20] = loadingfloor.MInnerTopRightAltSheet;
                this.landAltTileNumbers[0, 18] = loadingfloor.MInnerBottomLeftAltSheet;
                this.landAltTileNumbers[0, 21] = loadingfloor.MInnerBottomRightAltSheet;
                this.landAltTileNumbers[0, 15] = loadingfloor.MIsolatedWallAltSheet;
                this.landAltTileNumbers[0, 12] = loadingfloor.MColumnTopAltSheet;
                this.landAltTileNumbers[0, 13] = loadingfloor.MColumnCenterAltSheet;
                this.landAltTileNumbers[0, 14] = loadingfloor.MColumnBottomAltSheet;
                this.landAltTileNumbers[0, 3] = loadingfloor.MRowLeftAltSheet;
                this.landAltTileNumbers[0, 7] = loadingfloor.MRowCenterAltSheet;
                this.landAltTileNumbers[0, 11] = loadingfloor.MRowRightAltSheet;

                this.landAltTileNumbers[1, 19] = loadingfloor.MGroundAlt2X;
                this.landAltTileNumbers[1, 16] = loadingfloor.MGroundAltX;
                this.landAltTileNumbers[1, 10] = loadingfloor.MTopLeftAltX;
                this.landAltTileNumbers[1, 6] = loadingfloor.MTopCenterAltX;
                this.landAltTileNumbers[1, 2] = loadingfloor.MTopRightAltX;
                this.landAltTileNumbers[1, 9] = loadingfloor.MCenterLeftAltX;
                this.landAltTileNumbers[1, 5] = loadingfloor.MCenterCenterAltX;
                this.landAltTileNumbers[1, 22] = loadingfloor.MCenterCenterAlt2X;
                this.landAltTileNumbers[1, 1] = loadingfloor.MCenterRightAltX;
                this.landAltTileNumbers[1, 8] = loadingfloor.MBottomLeftAltX;
                this.landAltTileNumbers[1, 4] = loadingfloor.MBottomCenterAltX;
                this.landAltTileNumbers[1, 0] = loadingfloor.MBottomRightAltX;
                this.landAltTileNumbers[1, 17] = loadingfloor.MInnerTopLeftAltX;
                this.landAltTileNumbers[1, 20] = loadingfloor.MInnerTopRightAltX;
                this.landAltTileNumbers[1, 18] = loadingfloor.MInnerBottomLeftAltX;
                this.landAltTileNumbers[1, 21] = loadingfloor.MInnerBottomRightAltX;
                this.landAltTileNumbers[1, 15] = loadingfloor.MIsolatedWallAltX;
                this.landAltTileNumbers[1, 12] = loadingfloor.MColumnTopAltX;
                this.landAltTileNumbers[1, 13] = loadingfloor.MColumnCenterAltX;
                this.landAltTileNumbers[1, 14] = loadingfloor.MColumnBottomAltX;
                this.landAltTileNumbers[1, 3] = loadingfloor.MRowLeftAltX;
                this.landAltTileNumbers[1, 7] = loadingfloor.MRowCenterAltX;
                this.landAltTileNumbers[1, 11] = loadingfloor.MRowRightAltX;

                for (int i = 0; i < 23; i++)
                {
                    this.picLandAltTileset[i].Image = Graphic.GraphicsManager.Tiles[this.landAltTileNumbers[0, i]][this.landAltTileNumbers[1, i]];
                }

                this.lblSaveLoadMessage.Text = "Land Alt Tile settings loaded";
            }
            else if (this.pnlRDungeonWaterTiles != null && this.pnlRDungeonWaterTiles.Visible == true)
            {
                this.waterTileNumbers[0, 15] = loadingfloor.MShoreSurroundedSheet;
                this.waterTileNumbers[0, 0] = loadingfloor.MShoreInnerTopLeftSheet;
                this.waterTileNumbers[0, 4] = loadingfloor.MShoreTopSheet;
                this.waterTileNumbers[0, 8] = loadingfloor.MShoreInnerTopRightSheet;
                this.waterTileNumbers[0, 1] = loadingfloor.MShoreLeftSheet;
                this.waterTileNumbers[0, 5] = loadingfloor.MWaterSheet;
                this.waterTileNumbers[0, 9] = loadingfloor.MShoreRightSheet;
                this.waterTileNumbers[0, 2] = loadingfloor.MShoreInnerBottomLeftSheet;
                this.waterTileNumbers[0, 6] = loadingfloor.MShoreBottomSheet;
                this.waterTileNumbers[0, 10] = loadingfloor.MShoreInnerBottomRightSheet;
                this.waterTileNumbers[0, 16] = loadingfloor.MShoreTopLeftSheet;
                this.waterTileNumbers[0, 19] = loadingfloor.MShoreTopRightSheet;
                this.waterTileNumbers[0, 17] = loadingfloor.MShoreBottomLeftSheet;
                this.waterTileNumbers[0, 20] = loadingfloor.MShoreBottomRightSheet;
                this.waterTileNumbers[0, 18] = loadingfloor.MShoreDiagonalForwardSheet;
                this.waterTileNumbers[0, 21] = loadingfloor.MShoreDiagonalBackSheet;
                this.waterTileNumbers[0, 12] = loadingfloor.MShoreInnerTopSheet;
                this.waterTileNumbers[0, 13] = loadingfloor.MShoreVerticalSheet;
                this.waterTileNumbers[0, 14] = loadingfloor.MShoreInnerBottomSheet;
                this.waterTileNumbers[0, 3] = loadingfloor.MShoreInnerLeftSheet;
                this.waterTileNumbers[0, 7] = loadingfloor.MShoreHorizontalSheet;
                this.waterTileNumbers[0, 11] = loadingfloor.MShoreInnerRightSheet;

                this.waterTileNumbers[1, 15] = loadingfloor.MShoreSurroundedX;
                this.waterTileNumbers[1, 0] = loadingfloor.MShoreInnerTopLeftX;
                this.waterTileNumbers[1, 4] = loadingfloor.MShoreTopX;
                this.waterTileNumbers[1, 8] = loadingfloor.MShoreInnerTopRightX;
                this.waterTileNumbers[1, 1] = loadingfloor.MShoreLeftX;
                this.waterTileNumbers[1, 5] = loadingfloor.MWaterX;
                this.waterTileNumbers[1, 9] = loadingfloor.MShoreRightX;
                this.waterTileNumbers[1, 2] = loadingfloor.MShoreInnerBottomLeftX;
                this.waterTileNumbers[1, 6] = loadingfloor.MShoreBottomX;
                this.waterTileNumbers[1, 10] = loadingfloor.MShoreInnerBottomRightX;
                this.waterTileNumbers[1, 16] = loadingfloor.MShoreTopLeftX;
                this.waterTileNumbers[1, 19] = loadingfloor.MShoreTopRightX;
                this.waterTileNumbers[1, 17] = loadingfloor.MShoreBottomLeftX;
                this.waterTileNumbers[1, 20] = loadingfloor.MShoreBottomRightX;
                this.waterTileNumbers[1, 18] = loadingfloor.MShoreDiagonalForwardX;
                this.waterTileNumbers[1, 21] = loadingfloor.MShoreDiagonalBackX;
                this.waterTileNumbers[1, 12] = loadingfloor.MShoreInnerTopX;
                this.waterTileNumbers[1, 13] = loadingfloor.MShoreVerticalX;
                this.waterTileNumbers[1, 14] = loadingfloor.MShoreInnerBottomX;
                this.waterTileNumbers[1, 3] = loadingfloor.MShoreInnerLeftX;
                this.waterTileNumbers[1, 7] = loadingfloor.MShoreHorizontalX;
                this.waterTileNumbers[1, 11] = loadingfloor.MShoreInnerRightX;

                for (int i = 0; i < 22; i++)
                {
                    this.picWaterTileset[i].Image = Graphic.GraphicsManager.Tiles[this.waterTileNumbers[0, i]][this.waterTileNumbers[1, i]];
                }

                this.lblSaveLoadMessage.Text = "Water Tile settings loaded";
            }
            else if (this.pnlRDungeonWaterAnimTiles != null && this.pnlRDungeonWaterAnimTiles.Visible == true)
            {
                this.waterAnimTileNumbers[0, 15] = loadingfloor.MShoreSurroundedAnimSheet;
                this.waterAnimTileNumbers[0, 0] = loadingfloor.MShoreInnerTopLeftAnimSheet;
                this.waterAnimTileNumbers[0, 4] = loadingfloor.MShoreTopAnimSheet;
                this.waterAnimTileNumbers[0, 8] = loadingfloor.MShoreInnerTopRightAnimSheet;
                this.waterAnimTileNumbers[0, 1] = loadingfloor.MShoreLeftAnimSheet;
                this.waterAnimTileNumbers[0, 5] = loadingfloor.MWaterAnimSheet;
                this.waterAnimTileNumbers[0, 9] = loadingfloor.MShoreRightAnimSheet;
                this.waterAnimTileNumbers[0, 2] = loadingfloor.MShoreInnerBottomLeftAnimSheet;
                this.waterAnimTileNumbers[0, 6] = loadingfloor.MShoreBottomAnimSheet;
                this.waterAnimTileNumbers[0, 10] = loadingfloor.MShoreInnerBottomRightAnimSheet;
                this.waterAnimTileNumbers[0, 16] = loadingfloor.MShoreTopLeftAnimSheet;
                this.waterAnimTileNumbers[0, 19] = loadingfloor.MShoreTopRightAnimSheet;
                this.waterAnimTileNumbers[0, 17] = loadingfloor.MShoreBottomLeftAnimSheet;
                this.waterAnimTileNumbers[0, 20] = loadingfloor.MShoreBottomRightAnimSheet;
                this.waterAnimTileNumbers[0, 18] = loadingfloor.MShoreDiagonalForwardAnimSheet;
                this.waterAnimTileNumbers[0, 21] = loadingfloor.MShoreDiagonalBackAnimSheet;
                this.waterAnimTileNumbers[0, 12] = loadingfloor.MShoreInnerTopAnimSheet;
                this.waterAnimTileNumbers[0, 13] = loadingfloor.MShoreVerticalAnimSheet;
                this.waterAnimTileNumbers[0, 14] = loadingfloor.MShoreInnerBottomAnimSheet;
                this.waterAnimTileNumbers[0, 3] = loadingfloor.MShoreInnerLeftAnimSheet;
                this.waterAnimTileNumbers[0, 7] = loadingfloor.MShoreHorizontalAnimSheet;
                this.waterAnimTileNumbers[0, 11] = loadingfloor.MShoreInnerRightAnimSheet;

                this.waterAnimTileNumbers[1, 15] = loadingfloor.MShoreSurroundedAnimX;
                this.waterAnimTileNumbers[1, 0] = loadingfloor.MShoreInnerTopLeftAnimX;
                this.waterAnimTileNumbers[1, 4] = loadingfloor.MShoreTopAnimX;
                this.waterAnimTileNumbers[1, 8] = loadingfloor.MShoreInnerTopRightAnimX;
                this.waterAnimTileNumbers[1, 1] = loadingfloor.MShoreLeftAnimX;
                this.waterAnimTileNumbers[1, 5] = loadingfloor.MWaterAnimX;
                this.waterAnimTileNumbers[1, 9] = loadingfloor.MShoreRightAnimX;
                this.waterAnimTileNumbers[1, 2] = loadingfloor.MShoreInnerBottomLeftAnimX;
                this.waterAnimTileNumbers[1, 6] = loadingfloor.MShoreBottomAnimX;
                this.waterAnimTileNumbers[1, 10] = loadingfloor.MShoreInnerBottomRightAnimX;
                this.waterAnimTileNumbers[1, 16] = loadingfloor.MShoreTopLeftAnimX;
                this.waterAnimTileNumbers[1, 19] = loadingfloor.MShoreTopRightAnimX;
                this.waterAnimTileNumbers[1, 17] = loadingfloor.MShoreBottomLeftAnimX;
                this.waterAnimTileNumbers[1, 20] = loadingfloor.MShoreBottomRightAnimX;
                this.waterAnimTileNumbers[1, 18] = loadingfloor.MShoreDiagonalForwardAnimX;
                this.waterAnimTileNumbers[1, 21] = loadingfloor.MShoreDiagonalBackAnimX;
                this.waterAnimTileNumbers[1, 12] = loadingfloor.MShoreInnerTopAnimX;
                this.waterAnimTileNumbers[1, 13] = loadingfloor.MShoreVerticalAnimX;
                this.waterAnimTileNumbers[1, 14] = loadingfloor.MShoreInnerBottomAnimX;
                this.waterAnimTileNumbers[1, 3] = loadingfloor.MShoreInnerLeftAnimX;
                this.waterAnimTileNumbers[1, 7] = loadingfloor.MShoreHorizontalAnimX;
                this.waterAnimTileNumbers[1, 11] = loadingfloor.MShoreInnerRightAnimX;

                for (int i = 0; i < 22; i++)
                {
                    this.picWaterAnimTileset[i].Image = Graphic.GraphicsManager.Tiles[this.waterAnimTileNumbers[0, i]][this.waterAnimTileNumbers[1, i]];
                }

                this.lblSaveLoadMessage.Text = "Water Anim Tile settings loaded";
            }
            else if (this.pnlRDungeonAttributes != null && this.pnlRDungeonAttributes.Visible == true)
            {
                this.cbGroundType.SelectItem((int)loadingfloor.GroundTile.Type);
                this.nudGroundData1.Value = loadingfloor.GroundTile.Data1;
                this.nudGroundData2.Value = loadingfloor.GroundTile.Data2;
                this.nudGroundData3.Value = loadingfloor.GroundTile.Data3;
                this.txtGroundString1.Text = loadingfloor.GroundTile.String1;
                this.txtGroundString2.Text = loadingfloor.GroundTile.String2;
                this.txtGroundString3.Text = loadingfloor.GroundTile.String3;

                this.cbHallType.SelectItem((int)loadingfloor.HallTile.Type);
                this.nudHallData1.Value = loadingfloor.HallTile.Data1;
                this.nudHallData2.Value = loadingfloor.HallTile.Data2;
                this.nudHallData3.Value = loadingfloor.HallTile.Data3;
                this.txtHallString1.Text = loadingfloor.HallTile.String1;
                this.txtHallString2.Text = loadingfloor.HallTile.String2;
                this.txtHallString3.Text = loadingfloor.HallTile.String3;

                this.cbWaterType.SelectItem((int)loadingfloor.WaterTile.Type);
                this.nudWaterData1.Value = loadingfloor.WaterTile.Data1;
                this.nudWaterData2.Value = loadingfloor.WaterTile.Data2;
                this.nudWaterData3.Value = loadingfloor.WaterTile.Data3;
                this.txtWaterString1.Text = loadingfloor.WaterTile.String1;
                this.txtWaterString2.Text = loadingfloor.WaterTile.String2;
                this.txtWaterString3.Text = loadingfloor.WaterTile.String3;

                this.cbWallType.SelectItem((int)loadingfloor.WallTile.Type);
                this.nudWallData1.Value = loadingfloor.WallTile.Data1;
                this.nudWallData2.Value = loadingfloor.WallTile.Data2;
                this.nudWallData3.Value = loadingfloor.WallTile.Data3;
                this.txtWallString1.Text = loadingfloor.WallTile.String1;
                this.txtWallString2.Text = loadingfloor.WallTile.String2;
                this.txtWallString3.Text = loadingfloor.WallTile.String3;

                this.lblSaveLoadMessage.Text = "Attribute settings loaded";
            }
            else if (this.pnlRDungeonItems != null && this.pnlRDungeonItems.Visible == true)
            {
                this.itemList.Clear();
                this.lbxItems.Items.Clear();
                for (int item = 0; item < loadingfloor.Items.Count; item++)
                {
                    EditableRDungeonItem newItem = new EditableRDungeonItem();
                    newItem.ItemNum = loadingfloor.Items[item].ItemNum;
                    newItem.MinAmount = loadingfloor.Items[item].MinAmount;
                    newItem.MaxAmount = loadingfloor.Items[item].MaxAmount;
                    newItem.AppearanceRate = loadingfloor.Items[item].AppearanceRate;
                    newItem.StickyRate = loadingfloor.Items[item].StickyRate;
                    newItem.Tag = loadingfloor.Items[item].Tag;
                    newItem.Hidden = loadingfloor.Items[item].Hidden;
                    newItem.OnGround = loadingfloor.Items[item].OnGround;
                    newItem.OnWater = loadingfloor.Items[item].OnWater;
                    newItem.OnWall = loadingfloor.Items[item].OnWall;

                    this.itemList.Add(newItem);
                    this.lbxItems.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (item + 1) + ": (" + newItem.AppearanceRate + "%) " + newItem.MinAmount + "-" + newItem.MaxAmount + " " + Items.ItemHelper.Items[newItem.ItemNum].Name + " (" + newItem.StickyRate + "% Sticky)"));
                }

                this.lblSaveLoadMessage.Text = "Item settings loaded";
            }
            else if (this.pnlRDungeonNpcs != null && this.pnlRDungeonNpcs.Visible == true)
            {
                this.nudNpcSpawnTime.Value = loadingfloor.NpcSpawnTime;
                this.nudNpcMin.Value = loadingfloor.NpcMin;
                this.nudNpcMax.Value = loadingfloor.NpcMax;

                this.npcList.Clear();
                this.lbxNpcs.Items.Clear();
                for (int npc = 0; npc < loadingfloor.Npcs.Count; npc++)
                {
                    MapNpcSettings newNpc = new MapNpcSettings();
                    newNpc.NpcNum = loadingfloor.Npcs[npc].NpcNum;
                    newNpc.MinLevel = loadingfloor.Npcs[npc].MinLevel;
                    newNpc.MaxLevel = loadingfloor.Npcs[npc].MaxLevel;
                    newNpc.AppearanceRate = loadingfloor.Npcs[npc].AppearanceRate;
                    newNpc.StartStatus = loadingfloor.Npcs[npc].StartStatus;
                    newNpc.StartStatusCounter = loadingfloor.Npcs[npc].StartStatusCounter;
                    newNpc.StartStatusChance = loadingfloor.Npcs[npc].StartStatusChance;

                    this.npcList.Add(newNpc);
                    this.lbxNpcs.Items.Add(new ListBoxTextItem(
                        Graphic.FontManager.LoadFont("tahoma", 10),
                        (npc + 1) + ": (" + newNpc.AppearanceRate + "%) " + "Lv." + newNpc.MinLevel + "-" + newNpc.MaxLevel + " " + Npc.NpcHelper.Npcs[newNpc.NpcNum].Name + " [" + newNpc.StartStatusChance + "% " + newNpc.StartStatus.ToString() + "]"));
                }

                this.lblSaveLoadMessage.Text = "Npc settings loaded";
            }
            else if (this.pnlRDungeonTraps != null && this.pnlRDungeonTraps.Visible == true)
            {
                this.trapList.Clear();
                this.lbxTraps.Items.Clear();
                for (int traps = 0; traps < loadingfloor.SpecialTiles.Count; traps++)
                {
                    EditableRDungeonTrap newTile = new EditableRDungeonTrap();

                    newTile.SpecialTile.Ground = loadingfloor.SpecialTiles[traps].SpecialTile.Ground;
                    newTile.SpecialTile.GroundAnim = loadingfloor.SpecialTiles[traps].SpecialTile.GroundAnim;
                    newTile.SpecialTile.Mask = loadingfloor.SpecialTiles[traps].SpecialTile.Mask;
                    newTile.SpecialTile.Anim = loadingfloor.SpecialTiles[traps].SpecialTile.Anim;
                    newTile.SpecialTile.Mask2 = loadingfloor.SpecialTiles[traps].SpecialTile.Mask2;
                    newTile.SpecialTile.M2Anim = loadingfloor.SpecialTiles[traps].SpecialTile.M2Anim;
                    newTile.SpecialTile.Fringe = loadingfloor.SpecialTiles[traps].SpecialTile.Fringe;
                    newTile.SpecialTile.FAnim = loadingfloor.SpecialTiles[traps].SpecialTile.FAnim;
                    newTile.SpecialTile.Fringe2 = loadingfloor.SpecialTiles[traps].SpecialTile.Fringe2;
                    newTile.SpecialTile.F2Anim = loadingfloor.SpecialTiles[traps].SpecialTile.F2Anim;

                    newTile.SpecialTile.GroundSet = loadingfloor.SpecialTiles[traps].SpecialTile.GroundSet;
                    newTile.SpecialTile.GroundAnimSet = loadingfloor.SpecialTiles[traps].SpecialTile.GroundAnimSet;
                    newTile.SpecialTile.MaskSet = loadingfloor.SpecialTiles[traps].SpecialTile.MaskSet;
                    newTile.SpecialTile.AnimSet = loadingfloor.SpecialTiles[traps].SpecialTile.AnimSet;
                    newTile.SpecialTile.Mask2Set = loadingfloor.SpecialTiles[traps].SpecialTile.Mask2Set;
                    newTile.SpecialTile.M2AnimSet = loadingfloor.SpecialTiles[traps].SpecialTile.M2AnimSet;
                    newTile.SpecialTile.FringeSet = loadingfloor.SpecialTiles[traps].SpecialTile.FringeSet;
                    newTile.SpecialTile.FAnimSet = loadingfloor.SpecialTiles[traps].SpecialTile.FAnimSet;
                    newTile.SpecialTile.Fringe2Set = loadingfloor.SpecialTiles[traps].SpecialTile.Fringe2Set;
                    newTile.SpecialTile.F2AnimSet = loadingfloor.SpecialTiles[traps].SpecialTile.F2AnimSet;

                    newTile.SpecialTile.Type = loadingfloor.SpecialTiles[traps].SpecialTile.Type;
                    newTile.SpecialTile.Data1 = loadingfloor.SpecialTiles[traps].SpecialTile.Data1;
                    newTile.SpecialTile.Data2 = loadingfloor.SpecialTiles[traps].SpecialTile.Data2;
                    newTile.SpecialTile.Data3 = loadingfloor.SpecialTiles[traps].SpecialTile.Data3;
                    newTile.SpecialTile.String1 = loadingfloor.SpecialTiles[traps].SpecialTile.String1;
                    newTile.SpecialTile.String2 = loadingfloor.SpecialTiles[traps].SpecialTile.String2;
                    newTile.SpecialTile.String3 = loadingfloor.SpecialTiles[traps].SpecialTile.String3;

                    newTile.AppearanceRate = loadingfloor.SpecialTiles[traps].AppearanceRate;

                    this.trapList.Add(newTile);
                    this.lbxTraps.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (traps + 1) + ": " + newTile.SpecialTile.Type + "/" + newTile.SpecialTile.Data1 + "/" + newTile.SpecialTile.Data2 + "/" + newTile.SpecialTile.Data3));
                }

                this.lblSaveLoadMessage.Text = "Trap settings loaded";
            }
            else if (this.pnlRDungeonWeather != null && this.pnlRDungeonWeather.Visible == true)
            {
                this.lbxWeather.Items.Clear();
                for (int weather = 0; weather < loadingfloor.Weather.Count; weather++)
                {
                    this.lbxWeather.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (weather + 1) + ":" + (int)loadingfloor.Weather[weather] + ": " + Enum.GetName(typeof(Enums.Weather), loadingfloor.Weather[weather])));
                }

                this.lblSaveLoadMessage.Text = "Weather settings loaded";
            }
            else if (this.pnlRDungeonGoal != null && this.pnlRDungeonGoal.Visible == true)
            {
                switch (loadingfloor.GoalType)
                {
                    case Enums.RFloorGoalType.NextFloor:
                    {
                            this.optNextFloor.Checked = true;
                        }

                        break;
                    case Enums.RFloorGoalType.Map:
                    {
                            this.optMap.Checked = true;
                        }

                        break;
                    case Enums.RFloorGoalType.Scripted:
                    {
                            this.optScripted.Checked = true;
                        }

                        break;
                    default:
                    {
                            this.optNextFloor.Checked = true;
                        }

                        break;
                }

                this.nudData1.Value = loadingfloor.GoalMap;
                this.nudData2.Value = loadingfloor.GoalX;
                this.nudData3.Value = loadingfloor.GoalY;

                this.lblSaveLoadMessage.Text = "Goal settings loaded";
            }
            else if (this.pnlRDungeonChambers != null && this.pnlRDungeonChambers.Visible == true)
            {
                this.chamberList.Clear();
                this.lbxChambers.Items.Clear();
                for (int chamber = 0; chamber < loadingfloor.Chambers.Count; chamber++)
                {
                    EditableRDungeonChamber newChamber = new EditableRDungeonChamber();
                    newChamber.ChamberNum = loadingfloor.Chambers[chamber].ChamberNum;
                    newChamber.String1 = loadingfloor.Chambers[chamber].String1;
                    newChamber.String2 = loadingfloor.Chambers[chamber].String2;
                    newChamber.String3 = loadingfloor.Chambers[chamber].String3;
                    this.chamberList.Add(newChamber);
                    this.lbxChambers.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), "#" + loadingfloor.Chambers[chamber].ChamberNum + "/" + loadingfloor.Chambers[chamber].String1 + "/" + loadingfloor.Chambers[chamber].String2 + "/" + loadingfloor.Chambers[chamber].String3));
                }

                this.lblSaveLoadMessage.Text = "Chamber settings loaded";
            }
            else if (this.pnlRDungeonMisc != null && this.pnlRDungeonMisc.Visible == true)
            {
                this.nudDarkness.Value = loadingfloor.Darkness;
                if (loadingfloor.Music == string.Empty)
                {
                    this.lbxMusic.SelectItem(0);
                }
else
                {
                    for (int i = 0; i < this.lbxMusic.Items.Count; i++)
                    {
                        if (this.lbxMusic.Items[i].TextIdentifier == loadingfloor.Music)
                        {
                            this.lbxMusic.SelectItem(i);
                        }
                    }
                }

                this.lblSaveLoadMessage.Text = "Misc settings loaded";
            }
        }

        private void BtnStructure_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonStructure();

            this.pnlRDungeonFloorSettingSelection.Visible = false;
            this.pnlRDungeonStructure.Visible = true;
            this.btnSaveFloor.Text = "Save These Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load These Settings from Floor";
        }

        private void BtnLandTiles_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonLandTiles();

            this.pnlRDungeonFloorSettingSelection.Visible = false;
            this.pnlRDungeonLandTiles.Visible = true;
            this.btnSaveFloor.Text = "Save These Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load These Settings from Floor";
        }

        private void BtnWaterTiles_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonWaterTiles();

            this.pnlRDungeonFloorSettingSelection.Visible = false;
            this.pnlRDungeonWaterTiles.Visible = true;
            this.btnSaveFloor.Text = "Save These Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load These Settings from Floor";
        }

        private void BtnAttributes_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonAttributes();

            this.pnlRDungeonFloorSettingSelection.Visible = false;
            this.pnlRDungeonAttributes.Visible = true;
            this.btnSaveFloor.Text = "Save These Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load These Settings from Floor";
        }

        private void BtnItems_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonItems();

            this.pnlRDungeonFloorSettingSelection.Visible = false;
            this.pnlRDungeonItems.Visible = true;
            this.btnSaveFloor.Text = "Save These Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load These Settings from Floor";
        }

        private void BtnNpcs_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonNpcs();

            this.pnlRDungeonFloorSettingSelection.Visible = false;
            this.pnlRDungeonNpcs.Visible = true;
            this.btnSaveFloor.Text = "Save These Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load These Settings from Floor";
        }

        private void BtnTraps_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonTraps();

            this.pnlRDungeonFloorSettingSelection.Visible = false;
            this.pnlRDungeonTraps.Visible = true;
            this.btnSaveFloor.Text = "Save These Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load These Settings from Floor";
        }

        private void BtnWeather_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonWeather();

            this.pnlRDungeonFloorSettingSelection.Visible = false;
            this.pnlRDungeonWeather.Visible = true;
            this.btnSaveFloor.Text = "Save These Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load These Settings from Floor";
        }

        private void BtnGoal_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonGoal();

            this.pnlRDungeonFloorSettingSelection.Visible = false;
            this.pnlRDungeonGoal.Visible = true;
            this.btnSaveFloor.Text = "Save These Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load These Settings from Floor";
        }

        private void BtnChambers_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonChambers();

            this.pnlRDungeonFloorSettingSelection.Visible = false;
            this.pnlRDungeonChambers.Visible = true;
            this.btnSaveFloor.Text = "Save These Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load These Settings from Floor";
        }

        private void BtnMisc_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonMisc();

            this.pnlRDungeonFloorSettingSelection.Visible = false;
            this.pnlRDungeonMisc.Visible = true;
            this.btnSaveFloor.Text = "Save These Settings to Floor(s)";
            this.btnLoadFloor.Text = "Load These Settings from Floor";
        }

        private void PicLandTileset_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlTileSelector();
            this.editedTile = Array.IndexOf(this.picLandTileset, sender);
            this.pnlRDungeonLandTiles.Visible = false;
            this.pnlTileSelector.Visible = true;
        }

        private void BtnLandAltSwitch_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonLandAltTiles();

            this.pnlRDungeonLandTiles.Visible = false;
            this.pnlRDungeonLandAltTiles.Visible = true;
        }

        private void PicLandAltTileset_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlTileSelector();
            this.editedTile = Array.IndexOf(this.picLandAltTileset, sender) + 44;
            this.pnlRDungeonLandAltTiles.Visible = false;
            this.pnlTileSelector.Visible = true;
        }

        private void BtnLandSwitch_Click(object sender, MouseButtonEventArgs e)
        {
            this.pnlRDungeonLandAltTiles.Visible = false;
            this.pnlRDungeonLandTiles.Visible = true;
        }

        private void PicWaterTileset_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlTileSelector();
            this.editedTile = Array.IndexOf(this.picWaterTileset, sender) + 22;
            this.pnlRDungeonWaterTiles.Visible = false;
            this.pnlTileSelector.Visible = true;
        }

        private void BtnWaterAnimSwitch_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlRDungeonWaterAnimTiles();

            this.pnlRDungeonWaterTiles.Visible = false;
            this.pnlRDungeonWaterAnimTiles.Visible = true;
        }

        private void PicWaterAnimTileset_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlTileSelector();
            this.editedTile = Array.IndexOf(this.picWaterAnimTileset, sender) + 67;
            this.pnlRDungeonWaterAnimTiles.Visible = false;
            this.pnlTileSelector.Visible = true;
        }

        private void BtnWaterSwitch_Click(object sender, MouseButtonEventArgs e)
        {
            this.pnlRDungeonWaterAnimTiles.Visible = false;
            this.pnlRDungeonWaterTiles.Visible = true;
        }

        private void NudItemNum_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.nudItemNum.Value > 0 && this.nudItemNum.Value < MaxInfo.MaxItems)
            {
                this.lblItemNum.Text = Items.ItemHelper.Items[this.nudItemNum.Value].Name;
            }
else
            {
                this.lblItemNum.Text = "Item # ";
            }
        }

        private void BtnAddItem_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.nudItemNum.Value > 0)
            {
                EditableRDungeonItem newItem = new EditableRDungeonItem();
                newItem.ItemNum = this.nudItemNum.Value;
                newItem.MinAmount = this.nudMinValue.Value;
                newItem.MaxAmount = this.nudMaxValue.Value;
                newItem.AppearanceRate = this.nudItemSpawnRate.Value;
                newItem.StickyRate = this.nudStickyRate.Value;
                newItem.Tag = this.txtTag.Text;
                newItem.Hidden = this.chkHidden.Checked;
                newItem.OnGround = this.chkOnGround.Checked;
                newItem.OnWater = this.chkOnWater.Checked;
                newItem.OnWall = this.chkOnWall.Checked;

                if (this.chkBulkItem.Checked)
                {
                    for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                    {
                        if (this.lbxItems.SelectedIndex >= 0 && this.lbxItems.SelectedIndex < this.rdungeon.Floors[floor].Items.Count)
                        {
                            this.rdungeon.Floors[floor].Items.Insert(this.lbxItems.SelectedIndex, newItem);
                        }
else
                        {
                            this.rdungeon.Floors[floor].Items.Add(newItem);
                        }
                    }

                    this.itemList.Clear();
                    this.lbxItems.Items.Clear();
                    EditableRDungeonFloor loadingfloor = this.rdungeon.Floors[this.nudFirstFloor.Value - 1];
                    for (int item = 0; item < loadingfloor.Items.Count; item++)
                    {
                        EditableRDungeonItem newLoadItem = new EditableRDungeonItem();
                        newLoadItem.ItemNum = loadingfloor.Items[item].ItemNum;
                        newLoadItem.MinAmount = loadingfloor.Items[item].MinAmount;
                        newLoadItem.MaxAmount = loadingfloor.Items[item].MaxAmount;
                        newLoadItem.AppearanceRate = loadingfloor.Items[item].AppearanceRate;
                        newLoadItem.StickyRate = loadingfloor.Items[item].StickyRate;
                        newLoadItem.Tag = loadingfloor.Items[item].Tag;
                        newLoadItem.Hidden = loadingfloor.Items[item].Hidden;
                        newLoadItem.OnGround = loadingfloor.Items[item].OnGround;
                        newLoadItem.OnWater = loadingfloor.Items[item].OnWater;
                        newLoadItem.OnWall = loadingfloor.Items[item].OnWall;

                        this.itemList.Add(newLoadItem);
                        this.lbxItems.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (item + 1) + ": (" + newLoadItem.AppearanceRate + "%) " + newLoadItem.MinAmount + "-" + newLoadItem.MaxAmount + " " + Items.ItemHelper.Items[newLoadItem.ItemNum].Name + " (" + newLoadItem.StickyRate + "% Sticky)"));
                    }
                }
else
                {
                    if (this.lbxItems.SelectedIndex >= 0 && this.lbxItems.SelectedIndex < this.lbxItems.Items.Count)
                    {
                        this.itemList.Insert(this.lbxItems.SelectedIndex, newItem);
                    }
else
                    {
                        this.itemList.Add(newItem);
                    }

                    this.lbxItems.Items.Clear();
                    for (int item = 0; item < this.itemList.Count; item++)
                    {
                        this.lbxItems.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (item + 1) + ": (" + this.itemList[item].AppearanceRate + "%) " + this.itemList[item].MinAmount + "-" + this.itemList[item].MaxAmount + " " + Items.ItemHelper.Items[this.itemList[item].ItemNum].Name + " (" + this.itemList[item].StickyRate + "% Sticky)"));
                    }
                }
            }
        }

        private void BtnLoadItem_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxItems.SelectedIndex > -1)
            {
                this.nudItemNum.Value = this.itemList[this.lbxItems.SelectedIndex].ItemNum;
                this.nudMinValue.Value = this.itemList[this.lbxItems.SelectedIndex].MinAmount;
                this.nudMaxValue.Value = this.itemList[this.lbxItems.SelectedIndex].MaxAmount;
                this.nudItemSpawnRate.Value = this.itemList[this.lbxItems.SelectedIndex].AppearanceRate;
                this.nudStickyRate.Value = this.itemList[this.lbxItems.SelectedIndex].StickyRate;
                this.txtTag.Text = this.itemList[this.lbxItems.SelectedIndex].Tag;
                this.chkHidden.Checked = this.itemList[this.lbxItems.SelectedIndex].Hidden;
                this.chkOnGround.Checked = this.itemList[this.lbxItems.SelectedIndex].OnGround;
                this.chkOnWater.Checked = this.itemList[this.lbxItems.SelectedIndex].OnWater;
                this.chkOnWall.Checked = this.itemList[this.lbxItems.SelectedIndex].OnWall;
            }
        }

        private void BtnChangeItem_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxItems.SelectedIndex > -1)
            {
                EditableRDungeonItem newItem = new EditableRDungeonItem();
                newItem.ItemNum = this.nudItemNum.Value;
                newItem.MinAmount = this.nudMinValue.Value;
                newItem.MaxAmount = this.nudMaxValue.Value;
                newItem.AppearanceRate = this.nudItemSpawnRate.Value;
                newItem.StickyRate = this.nudStickyRate.Value;
                newItem.Tag = this.txtTag.Text;
                newItem.Hidden = this.chkHidden.Checked;
                newItem.OnGround = this.chkOnGround.Checked;
                newItem.OnWater = this.chkOnWater.Checked;
                newItem.OnWall = this.chkOnWall.Checked;

                if (this.chkBulkItem.Checked)
                {
                    for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                    {
                        for (int j = 0; j < this.rdungeon.Floors[floor].Items.Count; j++)
                        {
                            if (this.itemList[this.lbxItems.SelectedIndex].Equals(this.rdungeon.Floors[floor].Items[j]))
                            {
                                this.rdungeon.Floors[floor].Items[j] = newItem;
                            }
                        }
                    }

                    this.itemList.Clear();
                    this.lbxItems.Items.Clear();
                    EditableRDungeonFloor loadingfloor = this.rdungeon.Floors[this.nudFirstFloor.Value - 1];
                    for (int item = 0; item < loadingfloor.Items.Count; item++)
                    {
                        EditableRDungeonItem newLoadItem = new EditableRDungeonItem();
                        newLoadItem.ItemNum = loadingfloor.Items[item].ItemNum;
                        newLoadItem.MinAmount = loadingfloor.Items[item].MinAmount;
                        newLoadItem.MaxAmount = loadingfloor.Items[item].MaxAmount;
                        newLoadItem.AppearanceRate = loadingfloor.Items[item].AppearanceRate;
                        newLoadItem.StickyRate = loadingfloor.Items[item].StickyRate;
                        newLoadItem.Tag = loadingfloor.Items[item].Tag;
                        newLoadItem.Hidden = loadingfloor.Items[item].Hidden;
                        newLoadItem.OnGround = loadingfloor.Items[item].OnGround;
                        newLoadItem.OnWater = loadingfloor.Items[item].OnWater;
                        newLoadItem.OnWall = loadingfloor.Items[item].OnWall;

                        this.itemList.Add(newLoadItem);
                        this.lbxItems.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (item + 1) + ": (" + newLoadItem.AppearanceRate + "%) " + newLoadItem.MinAmount + "-" + newLoadItem.MaxAmount + " " + Items.ItemHelper.Items[newLoadItem.ItemNum].Name + " (" + newLoadItem.StickyRate + "% Sticky)"));
                    }
                }
else
                {
                    this.itemList[this.lbxItems.SelectedIndex] = newItem;

                    this.lbxItems.Items.Clear();
                    for (int item = 0; item < this.itemList.Count; item++)
                    {
                        this.lbxItems.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (item + 1) + ": (" + this.itemList[item].AppearanceRate + "%) " + this.itemList[item].MinAmount + "-" + this.itemList[item].MaxAmount + " " + Items.ItemHelper.Items[this.itemList[item].ItemNum].Name + " (" + this.itemList[item].StickyRate + "% Sticky)"));
                    }
                }
            }
        }

        private void BtnRemoveItem_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxItems.SelectedIndex > -1)
            {
                if (this.chkBulkItem.Checked)
                {
                    for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                    {
                        for (int j = this.rdungeon.Floors[floor].Items.Count - 1; j >= 0; j--)
                        {
                            if (this.itemList[this.lbxItems.SelectedIndex].Equals(this.rdungeon.Floors[floor].Items[j]))
                            {
                                this.rdungeon.Floors[floor].Items.RemoveAt(j);
                            }
                        }
                    }

                    this.itemList.Clear();
                    this.lbxItems.Items.Clear();
                    EditableRDungeonFloor loadingfloor = this.rdungeon.Floors[this.nudFirstFloor.Value - 1];
                    for (int item = 0; item < loadingfloor.Items.Count; item++)
                    {
                        EditableRDungeonItem newLoadItem = new EditableRDungeonItem();
                        newLoadItem.ItemNum = loadingfloor.Items[item].ItemNum;
                        newLoadItem.MinAmount = loadingfloor.Items[item].MinAmount;
                        newLoadItem.MaxAmount = loadingfloor.Items[item].MaxAmount;
                        newLoadItem.AppearanceRate = loadingfloor.Items[item].AppearanceRate;
                        newLoadItem.StickyRate = loadingfloor.Items[item].StickyRate;
                        newLoadItem.Tag = loadingfloor.Items[item].Tag;
                        newLoadItem.Hidden = loadingfloor.Items[item].Hidden;
                        newLoadItem.OnGround = loadingfloor.Items[item].OnGround;
                        newLoadItem.OnWater = loadingfloor.Items[item].OnWater;
                        newLoadItem.OnWall = loadingfloor.Items[item].OnWall;

                        this.itemList.Add(newLoadItem);
                        this.lbxItems.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (item + 1) + ": (" + newLoadItem.AppearanceRate + "%) " + newLoadItem.MinAmount + "-" + newLoadItem.MaxAmount + " " + Items.ItemHelper.Items[newLoadItem.ItemNum].Name + " (" + newLoadItem.StickyRate + "% Sticky)"));
                    }
                }
else
                {
                    this.itemList.RemoveAt(this.lbxItems.SelectedIndex);
                    this.lbxItems.Items.Clear();
                    for (int item = 0; item < this.itemList.Count; item++)
                    {
                        this.lbxItems.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (item + 1) + ": (" + this.itemList[item].AppearanceRate + "%) " + this.itemList[item].MinAmount + "-" + this.itemList[item].MaxAmount + " " + Items.ItemHelper.Items[this.itemList[item].ItemNum].Name + " (" + this.itemList[item].StickyRate + "% Sticky)"));
                    }
                }
            }
        }

        private void NudNpcNum_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (this.nudNpcNum.Value > 0 && this.nudNpcNum.Value < MaxInfo.MaxNpcs)
            {
                this.lblNpcNum.Text = Npc.NpcHelper.Npcs[this.nudNpcNum.Value].Name;
            }
else
            {
                this.lblNpcNum.Text = "NPC #";
            }
        }

        private void BtnAddNpc_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.nudNpcNum.Value > 0)
            {
                MapNpcSettings newNpc = new MapNpcSettings();
                newNpc.NpcNum = this.nudNpcNum.Value;
                newNpc.MinLevel = this.nudMinLevel.Value;
                newNpc.MaxLevel = this.nudMaxLevel.Value;
                newNpc.AppearanceRate = this.nudNpcSpawnRate.Value;
                newNpc.StartStatus = (Enums.StatusAilment)this.cbNpcStartStatus.SelectedIndex;
                newNpc.StartStatusCounter = this.nudStatusCounter.Value;
                newNpc.StartStatusChance = this.nudStatusChance.Value;

                if (this.chkBulkNpc.Checked)
                {
                    for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                    {
                        if (this.lbxNpcs.SelectedIndex >= 0 && this.lbxNpcs.SelectedIndex < this.rdungeon.Floors[floor].Npcs.Count)
                        {
                            this.rdungeon.Floors[floor].Npcs.Insert(this.lbxNpcs.SelectedIndex, newNpc);
                        }
else
                        {
                            this.rdungeon.Floors[floor].Npcs.Add(newNpc);
                        }
                    }

                    this.npcList.Clear();
                    this.lbxNpcs.Items.Clear();
                    EditableRDungeonFloor loadingfloor = this.rdungeon.Floors[this.nudFirstFloor.Value - 1];
                    for (int npc = 0; npc < loadingfloor.Npcs.Count; npc++)
                    {
                        MapNpcSettings newLoadNpc = new MapNpcSettings();
                        newLoadNpc.NpcNum = loadingfloor.Npcs[npc].NpcNum;
                        newLoadNpc.MinLevel = loadingfloor.Npcs[npc].MinLevel;
                        newLoadNpc.MaxLevel = loadingfloor.Npcs[npc].MaxLevel;
                        newLoadNpc.AppearanceRate = loadingfloor.Npcs[npc].AppearanceRate;
                        newLoadNpc.StartStatus = loadingfloor.Npcs[npc].StartStatus;
                        newLoadNpc.StartStatusCounter = loadingfloor.Npcs[npc].StartStatusCounter;
                        newLoadNpc.StartStatusChance = loadingfloor.Npcs[npc].StartStatusChance;

                        this.npcList.Add(newLoadNpc);
                        this.lbxNpcs.Items.Add(new ListBoxTextItem(
                            Graphic.FontManager.LoadFont("tahoma", 10),
                            (npc + 1) + ": (" + newLoadNpc.AppearanceRate + "%) " + "Lv." + newLoadNpc.MinLevel + "-" + newLoadNpc.MaxLevel + " " + Npc.NpcHelper.Npcs[newLoadNpc.NpcNum].Name + " [" + newLoadNpc.StartStatusChance + "% " + newLoadNpc.StartStatus.ToString() + "]"));
                    }
                }
else
                {
                    if (this.lbxNpcs.SelectedIndex >= 0 && this.lbxNpcs.SelectedIndex < this.lbxNpcs.Items.Count)
                    {
                        this.npcList.Insert(this.lbxNpcs.SelectedIndex, newNpc);
                    }
else
                    {
                        this.npcList.Add(newNpc);
                    }

                    this.lbxNpcs.Items.Clear();
                    for (int npc = 0; npc < this.npcList.Count; npc++)
                    {
                        this.lbxNpcs.Items.Add(new ListBoxTextItem(
                            Graphic.FontManager.LoadFont("tahoma", 10),
                            (npc + 1) + ": (" + this.npcList[npc].AppearanceRate + "%) " + "Lv." + this.npcList[npc].MinLevel + "-" + this.npcList[npc].MaxLevel + " " + Npc.NpcHelper.Npcs[this.npcList[npc].NpcNum].Name + " [" + this.npcList[npc].StartStatusChance + "% " + this.npcList[npc].StartStatus.ToString() + "]"));
                    }
                }
            }
        }

        private void BtnLoadNpc_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxNpcs.SelectedIndex > -1)
            {
                this.nudNpcNum.Value = this.npcList[this.lbxNpcs.SelectedIndex].NpcNum;
                this.nudMinLevel.Value = this.npcList[this.lbxNpcs.SelectedIndex].MinLevel;
                this.nudMaxLevel.Value = this.npcList[this.lbxNpcs.SelectedIndex].MaxLevel;
                this.nudNpcSpawnRate.Value = this.npcList[this.lbxNpcs.SelectedIndex].AppearanceRate;
                this.cbNpcStartStatus.SelectItem((int)this.npcList[this.lbxNpcs.SelectedIndex].StartStatus);
                this.nudStatusCounter.Value = this.npcList[this.lbxNpcs.SelectedIndex].StartStatusCounter;
                this.nudStatusChance.Value = this.npcList[this.lbxNpcs.SelectedIndex].StartStatusChance;
            }
        }

        private void BtnChangeNpc_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.nudNpcNum.Value > 0)
            {
                MapNpcSettings newNpc = new MapNpcSettings();
                newNpc.NpcNum = this.nudNpcNum.Value;
                newNpc.MinLevel = this.nudMinLevel.Value;
                newNpc.MaxLevel = this.nudMaxLevel.Value;
                newNpc.AppearanceRate = this.nudNpcSpawnRate.Value;
                newNpc.StartStatus = (Enums.StatusAilment)this.cbNpcStartStatus.SelectedIndex;
                newNpc.StartStatusCounter = this.nudStatusCounter.Value;
                newNpc.StartStatusChance = this.nudStatusChance.Value;

                if (this.chkBulkNpc.Checked)
                {
                    for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                    {
                        for (int j = 0; j < this.rdungeon.Floors[floor].Npcs.Count; j++)
                        {
                            if (this.npcList[this.lbxNpcs.SelectedIndex].Equals(this.rdungeon.Floors[floor].Npcs[j]))
                            {
                                this.rdungeon.Floors[floor].Npcs[j] = newNpc;
                            }
                        }
                    }

                    this.npcList.Clear();
                    this.lbxNpcs.Items.Clear();
                    EditableRDungeonFloor loadingfloor = this.rdungeon.Floors[this.nudFirstFloor.Value - 1];
                    for (int npc = 0; npc < loadingfloor.Npcs.Count; npc++)
                    {
                        MapNpcSettings newLoadNpc = new MapNpcSettings();
                        newLoadNpc.NpcNum = loadingfloor.Npcs[npc].NpcNum;
                        newLoadNpc.MinLevel = loadingfloor.Npcs[npc].MinLevel;
                        newLoadNpc.MaxLevel = loadingfloor.Npcs[npc].MaxLevel;
                        newLoadNpc.AppearanceRate = loadingfloor.Npcs[npc].AppearanceRate;
                        newLoadNpc.StartStatus = loadingfloor.Npcs[npc].StartStatus;
                        newLoadNpc.StartStatusCounter = loadingfloor.Npcs[npc].StartStatusCounter;
                        newLoadNpc.StartStatusChance = loadingfloor.Npcs[npc].StartStatusChance;

                        this.npcList.Add(newLoadNpc);
                        this.lbxNpcs.Items.Add(new ListBoxTextItem(
                            Graphic.FontManager.LoadFont("tahoma", 10),
                            (npc + 1) + ": (" + newLoadNpc.AppearanceRate + "%) " + "Lv." + newLoadNpc.MinLevel + "-" + newLoadNpc.MaxLevel + " " + Npc.NpcHelper.Npcs[newLoadNpc.NpcNum].Name + " [" + newLoadNpc.StartStatusChance + "% " + newLoadNpc.StartStatus.ToString() + "]"));
                    }
                }
else
                {
                    this.npcList[this.lbxNpcs.SelectedIndex] = newNpc;

                    this.lbxNpcs.Items.Clear();
                    for (int npc = 0; npc < this.npcList.Count; npc++)
                    {
                        this.lbxNpcs.Items.Add(new ListBoxTextItem(
                            Graphic.FontManager.LoadFont("tahoma", 10),
                            (npc + 1) + ": (" + this.npcList[npc].AppearanceRate + "%) " + "Lv." + this.npcList[npc].MinLevel + "-" + this.npcList[npc].MaxLevel + " " + Npc.NpcHelper.Npcs[this.npcList[npc].NpcNum].Name + " [" + this.npcList[npc].StartStatusChance + "% " + this.npcList[npc].StartStatus.ToString() + "]"));
                    }
                }
            }
        }

        private void BtnRemoveNpc_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxNpcs.SelectedIndex > -1)
            {
                if (this.chkBulkNpc.Checked)
                {
                    for (int floor = this.nudFirstFloor.Value - 1; floor < this.nudLastFloor.Value; floor++)
                    {
                        for (int j = this.rdungeon.Floors[floor].Npcs.Count - 1; j >= 0; j--)
                        {
                            if (this.npcList[this.lbxNpcs.SelectedIndex].Equals(this.rdungeon.Floors[floor].Npcs[j]))
                            {
                                this.rdungeon.Floors[floor].Npcs.RemoveAt(j);
                            }
                        }
                    }

                    this.npcList.Clear();
                    this.lbxNpcs.Items.Clear();
                    EditableRDungeonFloor loadingfloor = this.rdungeon.Floors[this.nudFirstFloor.Value - 1];
                    for (int npc = 0; npc < loadingfloor.Npcs.Count; npc++)
                    {
                        MapNpcSettings newLoadNpc = new MapNpcSettings();
                        newLoadNpc.NpcNum = loadingfloor.Npcs[npc].NpcNum;
                        newLoadNpc.MinLevel = loadingfloor.Npcs[npc].MinLevel;
                        newLoadNpc.MaxLevel = loadingfloor.Npcs[npc].MaxLevel;
                        newLoadNpc.AppearanceRate = loadingfloor.Npcs[npc].AppearanceRate;
                        newLoadNpc.StartStatus = loadingfloor.Npcs[npc].StartStatus;
                        newLoadNpc.StartStatusCounter = loadingfloor.Npcs[npc].StartStatusCounter;
                        newLoadNpc.StartStatusChance = loadingfloor.Npcs[npc].StartStatusChance;

                        this.npcList.Add(newLoadNpc);
                        this.lbxNpcs.Items.Add(new ListBoxTextItem(
                            Graphic.FontManager.LoadFont("tahoma", 10),
                            (npc + 1) + ": (" + newLoadNpc.AppearanceRate + "%) " + "Lv." + newLoadNpc.MinLevel + "-" + newLoadNpc.MaxLevel + " " + Npc.NpcHelper.Npcs[newLoadNpc.NpcNum].Name + " [" + newLoadNpc.StartStatusChance + "% " + newLoadNpc.StartStatus.ToString() + "]"));
                    }
                }
else
                {
                    this.npcList.RemoveAt(this.lbxNpcs.SelectedIndex);

                    this.lbxNpcs.Items.Clear();
                    for (int npc = 0; npc < this.npcList.Count; npc++)
                    {
                        this.lbxNpcs.Items.Add(new ListBoxTextItem(
                            Graphic.FontManager.LoadFont("tahoma", 10),
                            (npc + 1) + ": (" + this.npcList[npc].AppearanceRate + "%) " + "Lv." + this.npcList[npc].MinLevel + "-" + this.npcList[npc].MaxLevel + " " + Npc.NpcHelper.Npcs[this.npcList[npc].NpcNum].Name + " [" + this.npcList[npc].StartStatusChance + "% " + this.npcList[npc].StartStatus.ToString() + "]"));
                    }
                }
            }
        }

        private void BtnAddTrap_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.cbTrapType.SelectedIndex > -1)
            {
                EditableRDungeonTrap newTile = new EditableRDungeonTrap();
                newTile.SpecialTile.Ground = this.trapTileNumbers[1, 0];
                newTile.SpecialTile.GroundAnim = this.trapTileNumbers[1, 1];
                newTile.SpecialTile.Mask = this.trapTileNumbers[1, 2];
                newTile.SpecialTile.Anim = this.trapTileNumbers[1, 3];
                newTile.SpecialTile.Mask2 = this.trapTileNumbers[1, 4];
                newTile.SpecialTile.M2Anim = this.trapTileNumbers[1, 5];
                newTile.SpecialTile.Fringe = this.trapTileNumbers[1, 6];
                newTile.SpecialTile.FAnim = this.trapTileNumbers[1, 7];
                newTile.SpecialTile.Fringe2 = this.trapTileNumbers[1, 8];
                newTile.SpecialTile.F2Anim = this.trapTileNumbers[1, 9];

                newTile.SpecialTile.GroundSet = this.trapTileNumbers[0, 0];
                newTile.SpecialTile.GroundAnimSet = this.trapTileNumbers[0, 1];
                newTile.SpecialTile.MaskSet = this.trapTileNumbers[0, 2];
                newTile.SpecialTile.AnimSet = this.trapTileNumbers[0, 3];
                newTile.SpecialTile.Mask2Set = this.trapTileNumbers[0, 4];
                newTile.SpecialTile.M2AnimSet = this.trapTileNumbers[0, 5];
                newTile.SpecialTile.FringeSet = this.trapTileNumbers[0, 6];
                newTile.SpecialTile.FAnimSet = this.trapTileNumbers[0, 7];
                newTile.SpecialTile.Fringe2Set = this.trapTileNumbers[0, 8];
                newTile.SpecialTile.F2AnimSet = this.trapTileNumbers[0, 9];

                newTile.SpecialTile.Type = (Enums.TileType)this.cbTrapType.SelectedIndex;

                newTile.SpecialTile.Data1 = this.nudTrapData1.Value;
                newTile.SpecialTile.Data2 = this.nudTrapData2.Value;
                newTile.SpecialTile.Data3 = this.nudTrapData3.Value;
                newTile.SpecialTile.String1 = this.txtTrapString1.Text;
                newTile.SpecialTile.String2 = this.txtTrapString2.Text;
                newTile.SpecialTile.String3 = this.txtTrapString3.Text;

                newTile.AppearanceRate = this.nudTrapChance.Value;

                if (this.lbxTraps.SelectedIndex >= 0 && this.lbxTraps.SelectedIndex < this.lbxTraps.Items.Count)
                {
                    this.trapList.Insert(this.lbxTraps.SelectedIndex, newTile);
                }
else
                {
                    this.trapList.Add(newTile);
                }

                this.lbxTraps.Items.Clear();
                for (int traps = 0; traps < this.trapList.Count; traps++)
                {
                    this.lbxTraps.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (traps + 1) + ": " + this.trapList[traps].SpecialTile.Type + "/" + this.trapList[traps].SpecialTile.Data1 + "/" + this.trapList[traps].SpecialTile.Data2 + "/" + this.trapList[traps].SpecialTile.Data3));
                }
            }
        }

        private void BtnLoadTrap_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxTraps.SelectedIndex > -1)
            {
                this.trapTileNumbers[1, 0] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.Ground;
                this.trapTileNumbers[1, 1] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.GroundAnim;
                this.trapTileNumbers[1, 2] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.Mask;
                this.trapTileNumbers[1, 3] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.Anim;
                this.trapTileNumbers[1, 4] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.Mask2;
                this.trapTileNumbers[1, 5] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.M2Anim;
                this.trapTileNumbers[1, 6] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.Fringe;
                this.trapTileNumbers[1, 7] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.FAnim;
                this.trapTileNumbers[1, 8] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.Fringe2;
                this.trapTileNumbers[1, 9] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.F2Anim;

                this.trapTileNumbers[0, 0] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.GroundSet;
                this.trapTileNumbers[0, 1] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.GroundAnimSet;
                this.trapTileNumbers[0, 2] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.MaskSet;
                this.trapTileNumbers[0, 3] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.AnimSet;
                this.trapTileNumbers[0, 4] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.Mask2Set;
                this.trapTileNumbers[0, 5] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.M2AnimSet;
                this.trapTileNumbers[0, 6] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.FringeSet;
                this.trapTileNumbers[0, 7] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.FAnimSet;
                this.trapTileNumbers[0, 8] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.Fringe2Set;
                this.trapTileNumbers[0, 9] = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.F2AnimSet;

                for (int i = 0; i < 10; i++)
                {
                    this.picTrapTileset[i].Image = Graphic.GraphicsManager.Tiles[this.trapTileNumbers[0, i]][this.trapTileNumbers[1, i]];
                }

                this.cbTrapType.SelectItem((int)this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.Type);

                this.nudTrapData1.Value = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.Data1;
                this.nudTrapData2.Value = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.Data2;
                this.nudTrapData3.Value = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.Data3;
                this.txtTrapString1.Text = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.String1;
                this.txtTrapString2.Text = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.String2;
                this.txtTrapString3.Text = this.trapList[this.lbxTraps.SelectedIndex].SpecialTile.String3;
                this.nudTrapChance.Value = this.trapList[this.lbxTraps.SelectedIndex].AppearanceRate;
            }
        }

        private void BtnChangeTrap_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxTraps.SelectedIndex > -1)
            {
                EditableRDungeonTrap newTile = new EditableRDungeonTrap();
                newTile.SpecialTile.Ground = this.trapTileNumbers[1, 0];
                newTile.SpecialTile.GroundAnim = this.trapTileNumbers[1, 1];
                newTile.SpecialTile.Mask = this.trapTileNumbers[1, 2];
                newTile.SpecialTile.Anim = this.trapTileNumbers[1, 3];
                newTile.SpecialTile.Mask2 = this.trapTileNumbers[1, 4];
                newTile.SpecialTile.M2Anim = this.trapTileNumbers[1, 5];
                newTile.SpecialTile.Fringe = this.trapTileNumbers[1, 6];
                newTile.SpecialTile.FAnim = this.trapTileNumbers[1, 7];
                newTile.SpecialTile.Fringe2 = this.trapTileNumbers[1, 8];
                newTile.SpecialTile.F2Anim = this.trapTileNumbers[1, 9];

                newTile.SpecialTile.GroundSet = this.trapTileNumbers[0, 0];
                newTile.SpecialTile.GroundAnimSet = this.trapTileNumbers[0, 1];
                newTile.SpecialTile.MaskSet = this.trapTileNumbers[0, 2];
                newTile.SpecialTile.AnimSet = this.trapTileNumbers[0, 3];
                newTile.SpecialTile.Mask2Set = this.trapTileNumbers[0, 4];
                newTile.SpecialTile.M2AnimSet = this.trapTileNumbers[0, 5];
                newTile.SpecialTile.FringeSet = this.trapTileNumbers[0, 6];
                newTile.SpecialTile.FAnimSet = this.trapTileNumbers[0, 7];
                newTile.SpecialTile.Fringe2Set = this.trapTileNumbers[0, 8];
                newTile.SpecialTile.F2AnimSet = this.trapTileNumbers[0, 9];

                newTile.SpecialTile.Type = (Enums.TileType)this.cbTrapType.SelectedIndex;
                newTile.AppearanceRate = this.nudTrapChance.Value;

                newTile.SpecialTile.Data1 = this.nudTrapData1.Value;
                newTile.SpecialTile.Data2 = this.nudTrapData2.Value;
                newTile.SpecialTile.Data3 = this.nudTrapData3.Value;
                newTile.SpecialTile.String1 = this.txtTrapString1.Text;
                newTile.SpecialTile.String2 = this.txtTrapString2.Text;
                newTile.SpecialTile.String3 = this.txtTrapString3.Text;

                this.trapList[this.lbxTraps.SelectedIndex] = newTile;

                this.lbxTraps.Items.Clear();
                for (int traps = 0; traps < this.trapList.Count; traps++)
                {
                    this.lbxTraps.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (traps + 1) + ": " + this.trapList[traps].SpecialTile.Type + "/" + this.trapList[traps].SpecialTile.Data1 + "/" + this.trapList[traps].SpecialTile.Data2 + "/" + this.trapList[traps].SpecialTile.Data3));
                }
            }
        }

        private void PicTrapTileset_Click(object sender, MouseButtonEventArgs e)
        {
            this.LoadpnlTileSelector();

            this.editedTile = Array.IndexOf(this.picTrapTileset, sender) + 89;
            this.pnlRDungeonTraps.Visible = false;
            this.pnlTileSelector.Visible = true;
        }

        private void BtnRemoveTrap_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxTraps.SelectedIndex > -1)
            {
                this.trapList.RemoveAt(this.lbxTraps.SelectedIndex);

                this.lbxTraps.Items.Clear();
                for (int traps = 0; traps < this.trapList.Count; traps++)
                {
                    this.lbxTraps.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (traps + 1) + ": " + this.trapList[traps].SpecialTile.Type + "/" + this.trapList[traps].SpecialTile.Data1 + "/" + this.trapList[traps].SpecialTile.Data2 + "/" + this.trapList[traps].SpecialTile.Data3));
                }
            }
        }

        private void BtnAddWeather_Click(object sender, MouseButtonEventArgs e)
        {
            this.lbxWeather.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (this.lbxWeather.Items.Count + 1) + ":" + this.cbWeather.SelectedIndex + ": " + Enum.GetName(typeof(Enums.Weather), this.cbWeather.SelectedIndex)));
        }

        private void BtnRemoveWeather_Click(object sender, MouseButtonEventArgs e)
        {
            for (int weather = this.lbxWeather.SelectedIndex + 1; weather < this.lbxWeather.Items.Count; weather++)
            {
                string[] weatherindex = this.lbxWeather.Items[weather].TextIdentifier.Split(':');
                this.lbxWeather.Items[weather].TextIdentifier = weather + ":" + weatherindex[1] + ":" + weatherindex[2];
            }

            this.lbxWeather.Items.RemoveAt(this.lbxWeather.SelectedIndex);
        }

        private void OptNextFloor_Checked(object sender, EventArgs e)
        {
            this.lblData1.Text = "[No Parameter]";
            this.lblData2.Text = "[No Parameter]";
            this.lblData3.Text = "[No Parameter]";
        }

        private void OptMap_Checked(object sender, EventArgs e)
        {
            this.lblData1.Text = "Map Number:";
            this.lblData2.Text = "X:";
            this.lblData3.Text = "Y:";
        }

        private void OptScripted_Checked(object sender, EventArgs e)
        {
            this.lblData1.Text = "Script Number:";
            this.lblData2.Text = "[No Parameter]";
            this.lblData3.Text = "[No Parameter]";
        }

        private void BtnAddChamber_Click(object sender, MouseButtonEventArgs e)
        {
            EditableRDungeonChamber chamber = new EditableRDungeonChamber();
            chamber.ChamberNum = this.nudChamber.Value;
            chamber.String1 = this.txtChamberString1.Text;
            chamber.String2 = this.txtChamberString2.Text;
            chamber.String3 = this.txtChamberString3.Text;
            this.chamberList.Add(chamber);
            this.lbxChambers.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), "#" + chamber.ChamberNum + "/" + chamber.String1 + "/" + chamber.String2 + "/" + chamber.String3));
        }

        private void BtnRemoveChamber_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.lbxChambers.SelectedIndex > -1)
            {
                this.chamberList.RemoveAt(this.lbxChambers.SelectedIndex);
                this.lbxChambers.Items.RemoveAt(this.lbxChambers.SelectedIndex);
            }
        }

        private void BtnPlayMusic_Click(object sender, MouseButtonEventArgs e)
        {
            string song = null;
            if (this.lbxMusic.SelectedIndex > 0)
            {
                song = ((ListBoxTextItem)this.lbxMusic.SelectedItems[0]).Text;
            }
            else if (this.lbxMusic.SelectedIndex == 0)
            {
                Music.Music.AudioPlayer.StopMusic();
            }

            if (!string.IsNullOrEmpty(song))
            {
                Music.Music.AudioPlayer.PlayMusic(song, -1, true, true);
            }
        }

        private void NudTileSet_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.tileSelector.ActiveTilesetSurface = Graphic.GraphicsManager.Tiles[this.nudTileSet.Value];
        }

        private void BtnTileSetOK_Click(object sender, MouseButtonEventArgs e)
        {
            this.pnlTileSelector.Visible = false;
            if (this.editedTile < 22)
            {
                this.pnlRDungeonLandTiles.Visible = true;
                this.landTileNumbers[0, this.editedTile] = this.nudTileSet.Value;
                this.landTileNumbers[1, this.editedTile] = (this.tileSelector.SelectedTile.Y * (this.tileSelector.ActiveTilesetSurface.Size.Width / Constants.TILEWIDTH)) + this.tileSelector.SelectedTile.X;
                this.picLandTileset[this.editedTile].Image = this.tileSelector.ActiveTilesetSurface[this.landTileNumbers[1, this.editedTile]];
            }
            else if (this.editedTile < 44)
            {
                this.pnlRDungeonWaterTiles.Visible = true;
                this.waterTileNumbers[0, this.editedTile - 22] = this.nudTileSet.Value;
                this.waterTileNumbers[1, this.editedTile - 22] = (this.tileSelector.SelectedTile.Y * (this.tileSelector.ActiveTilesetSurface.Size.Width / Constants.TILEWIDTH)) + this.tileSelector.SelectedTile.X;
                this.picWaterTileset[this.editedTile - 22].Image = this.tileSelector.ActiveTilesetSurface[this.waterTileNumbers[1, this.editedTile - 22]];
            }
            else if (this.editedTile < 67)
            {
                this.pnlRDungeonLandAltTiles.Visible = true;
                this.landAltTileNumbers[0, this.editedTile - 44] = this.nudTileSet.Value;
                this.landAltTileNumbers[1, this.editedTile - 44] = (this.tileSelector.SelectedTile.Y * (this.tileSelector.ActiveTilesetSurface.Size.Width / Constants.TILEWIDTH)) + this.tileSelector.SelectedTile.X;
                this.picLandAltTileset[this.editedTile - 44].Image = this.tileSelector.ActiveTilesetSurface[this.landAltTileNumbers[1, this.editedTile - 44]];
            }
            else if (this.editedTile < 89)
            {
                this.pnlRDungeonWaterAnimTiles.Visible = true;
                this.waterAnimTileNumbers[0, this.editedTile - 67] = this.nudTileSet.Value;
                this.waterAnimTileNumbers[1, this.editedTile - 67] = (this.tileSelector.SelectedTile.Y * (this.tileSelector.ActiveTilesetSurface.Size.Width / Constants.TILEWIDTH)) + this.tileSelector.SelectedTile.X;
                this.picWaterAnimTileset[this.editedTile - 67].Image = this.tileSelector.ActiveTilesetSurface[this.waterAnimTileNumbers[1, this.editedTile - 67]];
            }
            else if (this.editedTile < 99)
            {
                this.pnlRDungeonTraps.Visible = true;
                this.trapTileNumbers[0, this.editedTile - 89] = this.nudTileSet.Value;
                this.trapTileNumbers[1, this.editedTile - 89] = (this.tileSelector.SelectedTile.Y * (this.tileSelector.ActiveTilesetSurface.Size.Width / Constants.TILEWIDTH)) + this.tileSelector.SelectedTile.X;
                this.picTrapTileset[this.editedTile - 89].Image = this.tileSelector.ActiveTilesetSurface[this.trapTileNumbers[1, this.editedTile - 89]];
            }

            this.editedTile = -1;
        }

        private void BtnTileSetCancel_Click(object sender, MouseButtonEventArgs e)
        {
            this.pnlTileSelector.Visible = false;
            if (this.editedTile < 22)
            {
                this.pnlRDungeonLandTiles.Visible = true;
            }
            else if (this.editedTile < 44)
            {
                this.pnlRDungeonWaterTiles.Visible = true;
            }
            else if (this.editedTile < 67)
            {
                this.pnlRDungeonLandAltTiles.Visible = true;
            }
            else if (this.editedTile < 89)
            {
                this.pnlRDungeonWaterAnimTiles.Visible = true;
            }
            else if (this.editedTile < 99)
            {
                this.pnlRDungeonTraps.Visible = true;
            }

            this.editedTile = -1;
        }
    }
}
