﻿// <copyright file="winGame.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Network;
    using Widges;
    using SdlDotNet.Input;
    using SdlDotNet.Widgets;
    using Widges;

    internal partial class WinGame : Core.WindowCore
    {
        private static WinGame instance;

        private bool inMapEditor;
        private readonly StatLabel lblStats;
        private readonly ActiveTeamPanel pnlTeam;
        private readonly MapViewer mapViewer;
        private readonly Menus.Core.MenuManager menuManager;
        private readonly ShortcutBar shortcutBar;
        private readonly BattleLog battleLog;
        private int menuManagerKeyDownTick;
        private int tickSinceLastPing;
        private Point startingLocation;
        private int lastSaveTick = 0;

        public WinGame()
          : base("winMainMenu")
        {
            {
                this.startingLocation = this.ScreenLocation;

                // this.Windowed = false;
                // this.Size = SdlDotNet.Graphics.Video.Screen.Size;
                // this.Location = new System.Drawing.Point(0, 0);
                // this.BackColor = Color.SteelBlue;
                this.pnlTeam = new ActiveTeamPanel("pnlTeam");
                this.pnlTeam.Size = new Size(SdlDotNet.Graphics.Video.Screen.Width/* - 50*/, 100);
                this.pnlTeam.Location = new Point(DrawingSupport.GetCenter(SdlDotNet.Graphics.Video.Screen.Size, this.pnlTeam.Size).X, 0);
                this.LocationChanged += new EventHandler(this.WinGame_LocationChanged);

                this.lblStats = new StatLabel("lblStats");
                this.lblStats.Size = new Size(400, 30);
                this.lblStats.Location = new Point(this.pnlTeam.X + 10, 75);

                this.mapViewer = new MapViewer("mapViewer");
                this.mapViewer.Size = new Size(640, 480);
                this.mapViewer.Location = new Point(SdlDotNet.Graphics.Video.Screen.Width - this.mapViewer.Width, this.pnlTeam.Y + this.pnlTeam.Height + 1);
                this.mapViewer.ActiveMap = null;
                this.mapViewer.BackColor = Color.Gray;
                this.mapViewer.KeyRepeatInterval = 1;

                // mapViewer.BeforeKeyDown += new EventHandler<SdlDotNet.Widgets.BeforeKeyDownEventArgs>(mapViewer_BeforeKeyDown);
                this.mapViewer.KeyDown += new EventHandler<KeyboardEventArgs>(this.MapViewer_KeyDown);
                this.mapViewer.KeyUp += new EventHandler<KeyboardEventArgs>(this.MapViewer_KeyUp);
                this.mapViewer.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(this.MapViewer_Click);
                this.mapViewer.MouseMotion += new EventHandler<MouseMotionEventArgs>(this.MapViewer_MouseMotion);

                this.menuManager = new Menus.Core.MenuManager("menuManager", this.mapViewer);
                this.menuManager.Location = this.mapViewer.Location;
                this.menuManager.Size = this.mapViewer.Size;
                this.menuManager.Visible = false;
                this.menuManager.KeyDown += new EventHandler<KeyboardEventArgs>(this.MenuManager_KeyDown);
                this.menuManager.KeyUp += new EventHandler<KeyboardEventArgs>(this.MenuManager_KeyUp);
                this.menuManager.KeyRepeatInterval = 0;
                this.menuManager.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(this.MenuManager_Click);

                this.battleLog = new BattleLog("battleLog");
                this.battleLog.Size = new Size(this.mapViewer.Width, 100);
                this.battleLog.Location = new Point(this.mapViewer.X, this.mapViewer.Y + this.mapViewer.Height - this.battleLog.Height);
                this.battleLog.Visible = false;

                this.shortcutBar = new ShortcutBar("shortcutBar");
                this.shortcutBar.Location = new Point(SdlDotNet.Graphics.Video.Screen.Width - this.shortcutBar.Width, this.mapViewer.Y + this.mapViewer.Height);
                ShortcutBarBuilder.AssembleShortcutBarButtons(this.shortcutBar);

                this.pnlTeam.Hide();
                this.lblStats.Hide();
                this.mapViewer.Hide();
                this.menuManager.Hide();
                this.shortcutBar.Hide();

                Screen.AddWidget(this.pnlTeam);
                Screen.AddWidget(this.mapViewer);
                Screen.AddWidget(this.lblStats);
                Screen.AddWidget(this.battleLog);
                Screen.AddWidget(this.menuManager);
                Screen.AddWidget(this.shortcutBar);

                this.InitMapEditorWidgets();

                // Add the events
                this.pnlTeam.ActiveRecruitChanged += new EventHandler<Events.ActiveRecruitChangedEventArgs>(this.PnlTeam_ActiveRecruitChanged);

                // this.LoadComplete();
                SdlDotNet.Core.Events.Tick += new EventHandler<SdlDotNet.Core.TickEventArgs>(this.WinGame_Tick);

                // base.Tick += new EventHandler<SdlDotNet.Core.TickEventArgs>(winGame_Tick);
                instance = this;
            }
        }

        public static WinGame Instance
        {
            get { return instance; }
        }

        public ActiveTeamPanel ActiveTeam
        {
            get { return this.pnlTeam; }
        }

        public StatLabel StatLabel
        {
            get { return this.lblStats; }
        }

        public MapViewer MapViewer
        {
            get { return this.mapViewer; }
        }

        public Menus.Core.MenuManager MenuManager
        {
            get { return this.menuManager; }
        }

        public BattleLog BattleLog
        {
            get { return this.battleLog; }
        }

        public Point LocationDifference
        {
            get
            {
                return new Point(this.ScreenLocation.X - this.startingLocation.X, this.ScreenLocation.Y - this.startingLocation.Y);
            }
        }

        public bool InMapEditor { get => this.inMapEditor; set => this.inMapEditor = value; }

        public void EnableMapEditorWidgets(Enums.MapEditorLimitTypes limitType, bool liveMode)
        {
            this.limiter = limitType;
            this.InLiveMode = liveMode;
            this.mapEditorMenu.Visible = true;
            this.btnAttributes.Visible = true;
            this.btnTerrain.Visible = true;
            this.tilesetViewer.Visible = true;
            this.EnforceMapEditorLimits();
        }

        public void DisableMapEditorWidgets()
        {
            this.mapEditorMenu.Visible = false;
            this.pnlAttributes.Visible = false;
            this.btnAttributes.Visible = false;
            this.btnTerrain.Visible = false;
            this.tilesetViewer.Visible = false;
            this.InLiveMode = false;
            this.HideAutoHiddenPanels();
            Messenger.SendExitMapEditor();
        }

        public void AddToBattleLog(string text, Color color)
        {
            if (!this.battleLog.Visible && !this.menuManager.Visible && this.InMapEditor == false)
            {
                this.battleLog.Visible = true;
            }

            this.battleLog.TmrHide.Start();
            this.battleLog.AddLog(text, color);
        }

        public void LoseFocus()
        {
            /////////
        }

        public new void Close()
        {
            instance = null;
            base.Close();
        }

        public void ShowWidgets()
        {
            this.pnlTeam.Show();
            this.lblStats.Show();
            this.mapViewer.Show();
            this.shortcutBar.Show();
        }

        private void WinGame_LocationChanged(object sender, EventArgs e)
        {
            // throw new NotImplementedException();
        }

        private void WinGame_Tick(object sender, SdlDotNet.Core.TickEventArgs e)
        {
            if (Globals.InGame)
            {
                if (IO.Options.AutoSaveSpeed > 0)
                {
                    if (e.Tick > this.lastSaveTick + (100 * 60000 / IO.Options.AutoSaveSpeed))
                    {
                        Messenger.SendPacket(PMU.Sockets.TcpPacket.CreatePacket("checkcommands", "/save"));
                        this.lastSaveTick = e.Tick;
                    }
                }

                if (IO.Options.Ping)
                {
                    if (e.Tick > this.tickSinceLastPing + 1000)
                    {
                        Messenger.SendPacket(PMU.Sockets.TcpPacket.CreatePacket("ping"));
                        this.tickSinceLastPing = e.Tick;
                    }
                }

                if (Players.PlayerManager.MyPlayer.MovementPacketCache != null)
                {
                    if (Globals.Tick > Players.PlayerManager.MyPlayer.LastMovementCacheSend + Constants.MovementClusteringFrquency && Players.PlayerManager.MyPlayer.MovementPacketCache.Packets.Count > 0)
                    {
                        NetworkManager.SendData(Players.PlayerManager.MyPlayer.MovementPacketCache);
                        Players.PlayerManager.MyPlayer.MovementPacketCache = new PacketList();
                        Players.PlayerManager.MyPlayer.LastMovementCacheSend = Globals.Tick;
                    }
                }
            }
        }

        private void MenuManager_Click(object sender, SdlDotNet.Widgets.MouseButtonEventArgs e)
        {
            if (this.menuManager.OpenMenus.Count == 0)
            {
                if (!this.menuManager.BlockInput)
                {
                    this.mapViewer.Focus();
                }
            }
else
            {
                this.menuManager.Focus();
            }
        }

        // public override void BlitToScreen(SdlDotNet.Graphics.Surface destinationSurface) {
        //    base.BlitToScreen(destinationSurface);
        //    if (menuManager.Visible) {
        //        menuManager.BlitToScreen(destinationSurface);
        //    }
        // }
        private void MapViewer_KeyUp(object sender, KeyboardEventArgs e)
        {
            Input.InputProcessor.OnKeyUp(e);
            if (this.menuManager.Visible && !(this.menuManager.Visible && !this.menuManager.BlockInput))
            {
                Input.InputProcessor.Attacking = false;
                Input.InputProcessor.MoveUp = false;
                Input.InputProcessor.MoveDown = false;
                Input.InputProcessor.MoveLeft = false;
                Input.InputProcessor.MoveRight = false;
            }

            if (this.inMapEditor && e.Key == (Key.Z & Key.LeftControl))
            {
                if (this.mementoIndex - 1 != -1)
                {
                    this.mementoIndex--;
                    this.mapViewer.ActiveMap = this.mapMemento[this.mementoIndex];
                }
            }
        }

        private void MenuManager_KeyDown(object sender, KeyboardEventArgs e)
        {
            Menus.Core.MenuInputProcessor.OnKeyDown(e);
            this.menuManagerKeyDownTick = Globals.Tick;
            Input.InputProcessor.Attacking = false;
            Input.InputProcessor.MoveUp = false;
            Input.InputProcessor.MoveDown = false;
            Input.InputProcessor.MoveLeft = false;
            Input.InputProcessor.MoveRight = false;
        }

        private void MenuManager_KeyUp(object sender, KeyboardEventArgs e)
        {
            Menus.Core.MenuInputProcessor.OnKeyUp(e);

            if (this.menuManager.Visible && !(this.menuManager.Visible && !this.menuManager.BlockInput))
            {
                Input.InputProcessor.Attacking = false;
                Input.InputProcessor.MoveUp = false;
                Input.InputProcessor.MoveDown = false;
                Input.InputProcessor.MoveLeft = false;
                Input.InputProcessor.MoveRight = false;
            }
        }

        private void MapViewer_KeyDown(object sender, KeyboardEventArgs e)
        {
            if ((e.Mod & ModifierKeys.LeftControl) != 0)
            {
                if (this.InMapEditor)
                {
                    if (e.Key == Key.S)
                    {
                        if (this.activeTool == MappingTool.Editor)
                        {
                            this.SetActiveTool(MappingTool.EyeDropper);
                        }
                        else
                        {
                            this.SetActiveTool(MappingTool.Editor);
                        }
                    }
                }
            }
            else
            {
                if (Globals.Tick < this.menuManagerKeyDownTick + 200)
                {
                    return;
                }

                if (!this.menuManager.Visible || (this.menuManager.Visible && !this.menuManager.BlockInput))
                {
                    Input.InputProcessor.OnKeyDown(e);
                }
                else if (this.menuManager.Visible && !this.menuManager.BlockInput)
                {
                    Input.InputProcessor.Attacking = false;
                    Input.InputProcessor.MoveUp = false;
                    Input.InputProcessor.MoveDown = false;
                    Input.InputProcessor.MoveLeft = false;
                    Input.InputProcessor.MoveRight = false;
                    Menus.Core.MenuInputProcessor.OnKeyDown(e);
                }
                else
                {
                    Input.InputProcessor.Attacking = false;
                    Input.InputProcessor.MoveUp = false;
                    Input.InputProcessor.MoveDown = false;
                    Input.InputProcessor.MoveLeft = false;
                    Input.InputProcessor.MoveRight = false;
                }
            }
        }

        private void MapViewer_Click(object sender, SdlDotNet.Widgets.MouseButtonEventArgs e)
        {
            if (this.InMapEditor)
            {
                this.Editor_mapViewer_Click(sender, e);
            }
else
            {
                if (Globals.InGame && Maps.MapHelper.ActiveMap != null)
                {
                    int newX = (e.RelativePosition.X / Constants.TILEWIDTH) + Graphic.Renderers.Screen.ScreenRenderer.Camera.X;
                    int newY = (e.RelativePosition.Y / Constants.TILEHEIGHT) + Graphic.Renderers.Screen.ScreenRenderer.Camera.Y;
                    if ((Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Mapper) || Maps.MapHelper.ActiveMap.MapID.StartsWith("h-")) && (Keyboard.IsKeyPressed(Key.LeftShift) && e.MouseEventArgs.Button == MouseButton.SecondaryButton))
                    {
                        Messenger.WarpLoc(newX, newY);
                        return;
                    }
                    else if (e.MouseEventArgs.Button == MouseButton.PrimaryButton)
                    {
                        Messenger.SendSearch(newX, newY);
                    }
                    else if (e.MouseEventArgs.Button == MouseButton.SecondaryButton)
                    {
                        Messenger.SendSearchProfile(newX, newY);
                    }
                }
            }
        }

        private void PnlTeam_ActiveRecruitChanged(object sender, Events.ActiveRecruitChangedEventArgs e)
        {
            Messenger.SendActiveCharSwap(e.NewSlot);

            // TODO: Move this to pokemon summary
            /*if (Players.PlayerManager.MyPlayer.Team[e.NewSlot].IQ / 50 >= 1)
            {
                int halfstars = (int)Math.Floor((decimal)Players.PlayerManager.MyPlayer.Team[e.NewSlot].IQ / 50);
                int fullstars = (int)Math.Floor((decimal)halfstars / 2);

                halfstars = ((halfstars / 2) - fullstars > 0.5) ? 1 : 0;

                for (int i = 0; i < fullstars + 1; i++)
                {
                    ActiveTeam.Buffer.Blit(new SdlDotNet.Graphics.Surface((Bitmap)Image.FromFile(System.IO.Path.Combine(IO.Paths.SkinPath,
                                                Skins.SkinManager.ActiveSkin.Name,
                                                @"\Game Window\TeamPanel\iqstars.png"))),
                                           new Point(StatLabel.X + StatLabel.Width + 4 + (16 * i), StatLabel.Y),
                                           new Rectangle(0, 0, 16, 16));
                }

                if (halfstars == 1)
                {
                    ActiveTeam.Buffer.Blit(new SdlDotNet.Graphics.Surface((Bitmap)Image.FromFile(System.IO.Path.Combine(IO.Paths.SkinPath,
                                                Skins.SkinManager.ActiveSkin.Name,
                                                @"\Game Window\TeamPanel\iqstars.png"))),
                                           new Point(StatLabel.X + StatLabel.Width + 4 + (16 * fullstars), StatLabel.Y),
                                           new Rectangle(16, 0, 16, 16));
                }
            }*/
        }
    }
}