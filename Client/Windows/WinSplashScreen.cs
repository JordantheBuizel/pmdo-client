﻿// <copyright file="WinSplashScreen.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Windows.Forms;
    using Extensions;
    using global::DataManager.Players;
    using Network;
    using PokeAPI;

    public partial class WinSplashScreen : Form
    {
        private bool updateNews = true;
        private bool movingToGame = false;
        private WinNewAccount accountWindow = null;
        private List<string> SplashText = new List<string>
        {
            "Carpe diem; Seize the day!",
            "All dreams are but another reality. Never forget.",
            "Living is using the time given to you. You cannot recall lost time. Don't forget that.",
            "These donuts are great! Jelly-filled are my favorite! Nothing beats a jelly-filled donut!",
            "You said you had a dream... That dream... Make it come true! Make your wonderful dream a reality, and it will become your truth!",
            "The more wonderful the meeting, the sadder the parting.",
            "I'd rather eat Randy",
            "You finally realize it now... If you wish, and wish very strongly... Perhaps you will meet again.",
            "YOOM-TAH!",
            "Hey, I know! I'll use my trusty frying pan... As a drying pan!"
        };

        public WinSplashScreen()
        {
            while (Loader.BackgroundLoaded == false)
            {
            }

            this.InitializeComponent();

            this.Text = this.SplashText[MathFunctions.Random.Next(0, this.SplashText.Count - 1)];

            this.BackgroundImage = Skins.SkinManager.ScreenBackground.ToBitmap(true);

            this.tbUsername.Text = IO.Options.SavedAccount;
            this.tbPassword.Text = IO.Options.SavedPassword;

            foreach (TabPage page in this.pages1.TabPages)
            {
                page.BackgroundImage = this.BackgroundImage;
            }

            Timer tmrServerStatus = new Timer();
            tmrServerStatus.Tick += new EventHandler(this.TmrServerStatus_Tick);
            tmrServerStatus.Interval = 50;
            tmrServerStatus.Start();

#if DEBUG
            pbDebug.Image = Graphic.GraphicsManager.GetMugshot(418, string.Empty, 0, 0).GetEmote(0).ToBitmap(true);
#endif
            WindowSwitcher.SplashScreen = this;
        }

        public void ShowMainMenu()
        {
            this.Invoke(new Action(() =>
            {
                this.pages1.SelectTab(this.pgHome);
            }));
        }

        public void ShowLoading()
        {
            this.Invoke(new Action(() =>
            {
                this.pages1.SelectTab(this.pgLoading);
            }));
        }

        public void ShowNewAccountWindow()
        {
            this.Invoke(new Action(() =>
            {
                if (this.accountWindow != null)
                {
                    this.accountWindow.Show();
                }
            }));
        }

        public void CharsRecieved(string char1, string char2, string char3, RecruitData recruit1, RecruitData recruit2, RecruitData recruit3)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<string, string, string, RecruitData, RecruitData, RecruitData>(this.CharsRecieved), new object[] { char1, char2, char3, recruit1, recruit2, recruit3 });
            }

            this.lblChar1.Text = char1;
            this.lblChar2.Text = char2;
            this.lblChar3.Text = char3;

            if (recruit1 != null)
            {
                this.pbChar1.Image = Graphic.GraphicsManager.GetMugshot(recruit1.Species, string.Empty, recruit1.Shiny, recruit1.Sex).GetEmoteBitmap(0);
            }

            if (recruit2 != null)
            {
                this.pbChar2.Image = Graphic.GraphicsManager.GetMugshot(recruit2.Species, string.Empty, recruit2.Shiny, recruit2.Sex).GetEmoteBitmap(0);
            }

            if (recruit3 != null)
            {
                this.pbChar3.Image = Graphic.GraphicsManager.GetMugshot(recruit3.Species, string.Empty, recruit3.Shiny, recruit3.Sex).GetEmoteBitmap(0);
            }

            foreach (PictureBox picture in this.pnlCharSelect.Controls.OfType<PictureBox>())
            {
                if (picture.Image == null)
                {
                    picture.Image = Graphic.GraphicsManager.GetMugshot(0, string.Empty, 0, 0).GetEmoteBitmap(0);
                }
            }

            this.pages1.SelectTab(this.pgCharSelect);
        }

        public void UpdateNews(string news)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<string>(this.UpdateNews), new object[] { news });
            }

            this.rtbNews.Text = news;
            this.updateNews = false;
        }

        protected override void OnClosed(EventArgs e)
        {
            IO.Options.SaveXml();

            if (!this.movingToGame)
            {
                Environment.Exit(0);
            }

            base.OnClosed(e);
        }

        private void TmrServerStatus_Tick(object sender, EventArgs e)
        {
            if (NetworkManager.TcpClient.Socket.Connected)
            {
                if (this.pbConnected.Image != Properties.Resources.Online)
                {
                    this.pbConnected.Image = Properties.Resources.Online;
                }
            }
            else
            {
                if (NetworkManager.ShouldAttemptReconnect())
                {
                    NetworkManager.Connect();
                }

                if (this.pbConnected.Image != Properties.Resources.Offline)
                {
                    this.pbConnected.Image = Properties.Resources.Offline;
                }
            }

            if (this.updateNews)
            {
                Messenger.SendRequestNews();
            }
        }

        /*private void Timer_Tick(object sender, EventArgs e)
        {
            if (new Point(this.gameScreenHwnd.Location.X, this.gameScreenHwnd.Location.Y + this.titleBarOffset) != SplashLocation)
            {
                this.MoveForm(new Point(this.gameScreenHwnd.Location.X, this.gameScreenHwnd.Location.Y + this.titleBarOffset));
            }
        }*/

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            Messenger.SendLogin(this.tbUsername.Text, this.tbPassword.Text);

            this.pages1.SelectTab(this.pgLoading);
            this.lblLoading.Text = "Logging in...";

            if (this.chkRemember.Checked)
            {
                IO.Options.SavedAccount = this.tbUsername.Text;
                IO.Options.SavedPassword = this.tbPassword.Text;
            }
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            NetworkManager.Disconnect();
            NetworkManager.Connect();

            this.rtbNews.Text = "Retrieving news from server...";
            Messenger.SendRequestNews();
            this.updateNews = true;
        }

        private void LblBacktoMain_Click(object sender, EventArgs e)
        {
            this.pages1.SelectTab(this.pgHome);
        }

        private void PgCharSelect_Click(object sender, EventArgs e)
        {
        }

        private void BtnOptions_Click(object sender, EventArgs e)
        {
            WinOptions opt = new WinOptions();
            opt.Show();
        }

        private void BtnNewAccount_Click(object sender, EventArgs e)
        {
            this.accountWindow = new WinNewAccount();
            this.accountWindow.Show();
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
        }

        private void BtnCredits_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(new TextBox(), "http://pmdonlinemmo.weebly.com/staff-list.html");
        }

        private void PbChar1_Click(object sender, EventArgs e)
        {
            if (this.lblChar1.Text != string.Empty)
            {
                Messenger.SendUseChar(1);
            }
            else
            {
                // TODO: Add NewChar page here
            }
        }

        private void LblChar1_Click(object sender, EventArgs e)
        {
            Messenger.SendUseChar(1);
        }

        private void PbChar2_Click(object sender, EventArgs e)
        {
            Messenger.SendUseChar(2);
        }

        private void LblChar2_Click(object sender, EventArgs e)
        {
            Messenger.SendUseChar(2);
        }

        private void PbChar3_Click(object sender, EventArgs e)
        {
            Messenger.SendUseChar(3);
        }

        private void LblChar3_Click(object sender, EventArgs e)
        {
            Messenger.SendUseChar(3);
        }
    }
}
