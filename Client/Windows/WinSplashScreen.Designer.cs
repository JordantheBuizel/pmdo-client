﻿namespace Client.Logic.Windows
{
    partial class WinSplashScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WinSplashScreen));
            this.tbPassword = new Client.Logic.Extensions.RichPassword();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblRemember = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.rtbNews = new System.Windows.Forms.RichTextBox();
            this.tbUsername = new System.Windows.Forms.RichTextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.chkRemember = new Client.Logic.Extensions.CheckBoxImage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnOptions = new System.Windows.Forms.Button();
            this.btnSkins = new System.Windows.Forms.Button();
            this.btnNewAccount = new System.Windows.Forms.Button();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.pages1 = new Client.Logic.Extensions.Pages();
            this.pgHome = new System.Windows.Forms.TabPage();
            this.pbDebug = new System.Windows.Forms.PictureBox();
            this.pbConnected = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnCredits = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pgLoading = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLoading = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pgCharSelect = new System.Windows.Forms.TabPage();
            this.pnlCharTitle = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlCharSelect = new System.Windows.Forms.Panel();
            this.pbChar3 = new System.Windows.Forms.PictureBox();
            this.pbChar2 = new System.Windows.Forms.PictureBox();
            this.pbChar1 = new System.Windows.Forms.PictureBox();
            this.lblBacktoMain = new System.Windows.Forms.Label();
            this.lblChar3 = new System.Windows.Forms.Label();
            this.lblChar2 = new System.Windows.Forms.Label();
            this.lblChar1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRemember)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pages1.SuspendLayout();
            this.pgHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDebug)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbConnected)).BeginInit();
            this.pgLoading.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.pgCharSelect.SuspendLayout();
            this.pnlCharTitle.SuspendLayout();
            this.pnlCharSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbChar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbChar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbChar1)).BeginInit();
            this.SuspendLayout();
            // 
            // tbPassword
            // 
            this.tbPassword.BackColor = System.Drawing.Color.White;
            this.tbPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbPassword.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPassword.Location = new System.Drawing.Point(336, 403);
            this.tbPassword.Multiline = false;
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.tbPassword.Size = new System.Drawing.Size(165, 20);
            this.tbPassword.TabIndex = 0;
            this.tbPassword.Text = "";
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.Transparent;
            this.btnLogin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLogin.BackgroundImage")));
            this.btnLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLogin.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnLogin.Location = new System.Drawing.Point(329, 449);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(180, 31);
            this.btnLogin.TabIndex = 1;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
            // 
            // lblRemember
            // 
            this.lblRemember.AutoSize = true;
            this.lblRemember.BackColor = System.Drawing.Color.Transparent;
            this.lblRemember.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblRemember.Location = new System.Drawing.Point(352, 487);
            this.lblRemember.Name = "lblRemember";
            this.lblRemember.Size = new System.Drawing.Size(101, 13);
            this.lblRemember.TabIndex = 3;
            this.lblRemember.Text = "Remember Account";
            // 
            // lblUsername
            // 
            this.lblUsername.BackColor = System.Drawing.Color.Transparent;
            this.lblUsername.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblUsername.Location = new System.Drawing.Point(329, 298);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(180, 18);
            this.lblUsername.TabIndex = 4;
            this.lblUsername.Text = "Username";
            this.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPassword
            // 
            this.lblPassword.BackColor = System.Drawing.Color.Transparent;
            this.lblPassword.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblPassword.Location = new System.Drawing.Point(329, 373);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(180, 17);
            this.lblPassword.TabIndex = 4;
            this.lblPassword.Text = "Password";
            this.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rtbNews
            // 
            this.rtbNews.BackColor = System.Drawing.Color.White;
            this.rtbNews.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbNews.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtbNews.Location = new System.Drawing.Point(594, 352);
            this.rtbNews.Name = "rtbNews";
            this.rtbNews.ReadOnly = true;
            this.rtbNews.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbNews.Size = new System.Drawing.Size(237, 267);
            this.rtbNews.TabIndex = 6;
            this.rtbNews.Text = "Retrieving news from server...";
            // 
            // tbUsername
            // 
            this.tbUsername.BackColor = System.Drawing.Color.White;
            this.tbUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbUsername.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbUsername.Location = new System.Drawing.Point(336, 328);
            this.tbUsername.Multiline = false;
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.tbUsername.Size = new System.Drawing.Size(165, 20);
            this.tbUsername.TabIndex = 0;
            this.tbUsername.Text = "";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::Client.Logic.Properties.Resources.news;
            this.pictureBox3.Location = new System.Drawing.Point(588, 328);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(250, 300);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // pbLogo
            // 
            this.pbLogo.BackColor = System.Drawing.Color.Transparent;
            this.pbLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbLogo.Image")));
            this.pbLogo.Location = new System.Drawing.Point(256, 47);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(322, 226);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbLogo.TabIndex = 5;
            this.pbLogo.TabStop = false;
            // 
            // chkRemember
            // 
            this.chkRemember.BackColor = System.Drawing.Color.Transparent;
            this.chkRemember.Checked = true;
            this.chkRemember.Image = ((System.Drawing.Image)(resources.GetObject("chkRemember.Image")));
            this.chkRemember.Location = new System.Drawing.Point(333, 486);
            this.chkRemember.Name = "chkRemember";
            this.chkRemember.Size = new System.Drawing.Size(14, 14);
            this.chkRemember.TabIndex = 2;
            this.chkRemember.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = global::Client.Logic.Properties.Resources.textbox;
            this.pictureBox2.Location = new System.Drawing.Point(329, 322);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(180, 30);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::Client.Logic.Properties.Resources.textbox;
            this.pictureBox1.Location = new System.Drawing.Point(329, 397);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(180, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // btnOptions
            // 
            this.btnOptions.BackColor = System.Drawing.Color.Transparent;
            this.btnOptions.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOptions.BackgroundImage")));
            this.btnOptions.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOptions.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.btnOptions.FlatAppearance.BorderSize = 0;
            this.btnOptions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOptions.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnOptions.Location = new System.Drawing.Point(12, 523);
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.Size = new System.Drawing.Size(180, 31);
            this.btnOptions.TabIndex = 1;
            this.btnOptions.Text = "Options";
            this.btnOptions.UseVisualStyleBackColor = false;
            this.btnOptions.Click += new System.EventHandler(this.BtnOptions_Click);
            // 
            // btnSkins
            // 
            this.btnSkins.BackColor = System.Drawing.Color.Transparent;
            this.btnSkins.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSkins.BackgroundImage")));
            this.btnSkins.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSkins.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.btnSkins.FlatAppearance.BorderSize = 0;
            this.btnSkins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSkins.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSkins.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSkins.Location = new System.Drawing.Point(12, 560);
            this.btnSkins.Name = "btnSkins";
            this.btnSkins.Size = new System.Drawing.Size(180, 31);
            this.btnSkins.TabIndex = 1;
            this.btnSkins.Text = "Skins (Coming Soon!)";
            this.btnSkins.UseVisualStyleBackColor = false;
            // 
            // btnNewAccount
            // 
            this.btnNewAccount.BackColor = System.Drawing.Color.Transparent;
            this.btnNewAccount.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNewAccount.BackgroundImage")));
            this.btnNewAccount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNewAccount.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.btnNewAccount.FlatAppearance.BorderSize = 0;
            this.btnNewAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewAccount.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnNewAccount.Location = new System.Drawing.Point(12, 486);
            this.btnNewAccount.Name = "btnNewAccount";
            this.btnNewAccount.Size = new System.Drawing.Size(180, 31);
            this.btnNewAccount.TabIndex = 1;
            this.btnNewAccount.Text = "New Account";
            this.btnNewAccount.UseVisualStyleBackColor = false;
            this.btnNewAccount.Click += new System.EventHandler(this.BtnNewAccount_Click);
            // 
            // lblCopyright
            // 
            this.lblCopyright.AutoSize = true;
            this.lblCopyright.BackColor = System.Drawing.Color.Transparent;
            this.lblCopyright.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyright.Location = new System.Drawing.Point(239, 616);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(313, 15);
            this.lblCopyright.TabIndex = 9;
            this.lblCopyright.Text = "Pokémon Mystery Dungeon Online © 2017 JordantheBuizel, PMDO Team";
            // 
            // pages1
            // 
            this.pages1.Controls.Add(this.pgHome);
            this.pages1.Controls.Add(this.tabPage1);
            this.pages1.Controls.Add(this.pgLoading);
            this.pages1.Controls.Add(this.pgCharSelect);
            this.pages1.Location = new System.Drawing.Point(-4, -22);
            this.pages1.Name = "pages1";
            this.pages1.SelectedIndex = 0;
            this.pages1.Size = new System.Drawing.Size(858, 666);
            this.pages1.TabIndex = 10;
            // 
            // pgHome
            // 
            this.pgHome.BackColor = System.Drawing.Color.Transparent;
            this.pgHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pgHome.Controls.Add(this.pbDebug);
            this.pgHome.Controls.Add(this.pbConnected);
            this.pgHome.Controls.Add(this.label3);
            this.pgHome.Controls.Add(this.btnRefresh);
            this.pgHome.Controls.Add(this.chkRemember);
            this.pgHome.Controls.Add(this.btnCredits);
            this.pgHome.Controls.Add(this.lblRemember);
            this.pgHome.Controls.Add(this.btnSkins);
            this.pgHome.Controls.Add(this.btnOptions);
            this.pgHome.Controls.Add(this.lblUsername);
            this.pgHome.Controls.Add(this.btnNewAccount);
            this.pgHome.Controls.Add(this.lblPassword);
            this.pgHome.Controls.Add(this.btnLogin);
            this.pgHome.Controls.Add(this.pbLogo);
            this.pgHome.Controls.Add(this.tbUsername);
            this.pgHome.Controls.Add(this.lblCopyright);
            this.pgHome.Controls.Add(this.pictureBox2);
            this.pgHome.Controls.Add(this.tbPassword);
            this.pgHome.Controls.Add(this.pictureBox1);
            this.pgHome.Controls.Add(this.rtbNews);
            this.pgHome.Controls.Add(this.pictureBox3);
            this.pgHome.Location = new System.Drawing.Point(4, 22);
            this.pgHome.Name = "pgHome";
            this.pgHome.Padding = new System.Windows.Forms.Padding(3);
            this.pgHome.Size = new System.Drawing.Size(850, 640);
            this.pgHome.TabIndex = 0;
            this.pgHome.Text = "tabPage1";
            // 
            // pbDebug
            // 
            this.pbDebug.Location = new System.Drawing.Point(646, 89);
            this.pbDebug.Name = "pbDebug";
            this.pbDebug.Size = new System.Drawing.Size(56, 56);
            this.pbDebug.TabIndex = 13;
            this.pbDebug.TabStop = false;
            // 
            // pbConnected
            // 
            this.pbConnected.Image = global::Client.Logic.Properties.Resources.Offline;
            this.pbConnected.Location = new System.Drawing.Point(520, 588);
            this.pbConnected.Name = "pbConnected";
            this.pbConnected.Size = new System.Drawing.Size(15, 15);
            this.pbConnected.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbConnected.TabIndex = 12;
            this.pbConnected.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.SteelBlue;
            this.label3.Location = new System.Drawing.Point(412, 586);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 18);
            this.label3.TabIndex = 11;
            this.label3.Text = "Server Status";
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackgroundImage = global::Client.Logic.Properties.Resources.refresh2;
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRefresh.FlatAppearance.BorderSize = 0;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Location = new System.Drawing.Point(541, 584);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(24, 24);
            this.btnRefresh.TabIndex = 10;
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.BtnRefresh_Click);
            // 
            // btnCredits
            // 
            this.btnCredits.BackColor = System.Drawing.Color.Transparent;
            this.btnCredits.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCredits.BackgroundImage")));
            this.btnCredits.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCredits.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.btnCredits.FlatAppearance.BorderSize = 0;
            this.btnCredits.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCredits.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCredits.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCredits.Location = new System.Drawing.Point(12, 597);
            this.btnCredits.Name = "btnCredits";
            this.btnCredits.Size = new System.Drawing.Size(180, 31);
            this.btnCredits.TabIndex = 1;
            this.btnCredits.Text = "Credits";
            this.btnCredits.UseVisualStyleBackColor = false;
            this.btnCredits.Click += new System.EventHandler(this.BtnCredits_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(850, 640);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "tabPage2";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pgLoading
            // 
            this.pgLoading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pgLoading.Controls.Add(this.panel1);
            this.pgLoading.Location = new System.Drawing.Point(4, 22);
            this.pgLoading.Name = "pgLoading";
            this.pgLoading.Padding = new System.Windows.Forms.Padding(3);
            this.pgLoading.Size = new System.Drawing.Size(850, 640);
            this.pgLoading.TabIndex = 2;
            this.pgLoading.Text = "tabPage2";
            this.pgLoading.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::Client.Logic.Properties.Resources.tumblr_nbsf4xn2GD1toiaw4o1_500;
            this.panel1.Controls.Add(this.lblLoading);
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Location = new System.Drawing.Point(159, 210);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(500, 264);
            this.panel1.TabIndex = 0;
            // 
            // lblLoading
            // 
            this.lblLoading.Font = new System.Drawing.Font("Palatino Linotype", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoading.ForeColor = System.Drawing.Color.White;
            this.lblLoading.Location = new System.Drawing.Point(0, 115);
            this.lblLoading.Name = "lblLoading";
            this.lblLoading.Size = new System.Drawing.Size(298, 47);
            this.lblLoading.TabIndex = 0;
            this.lblLoading.Text = "Loading...";
            this.lblLoading.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Client.Logic.Properties.Resources.tumblr_nbsf4xn2GD1toiaw4o1_500;
            this.pictureBox4.Location = new System.Drawing.Point(0, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(503, 264);
            this.pictureBox4.TabIndex = 1;
            this.pictureBox4.TabStop = false;
            // 
            // pgCharSelect
            // 
            this.pgCharSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pgCharSelect.Controls.Add(this.pnlCharTitle);
            this.pgCharSelect.Controls.Add(this.pnlCharSelect);
            this.pgCharSelect.Location = new System.Drawing.Point(4, 22);
            this.pgCharSelect.Name = "pgCharSelect";
            this.pgCharSelect.Padding = new System.Windows.Forms.Padding(3);
            this.pgCharSelect.Size = new System.Drawing.Size(850, 640);
            this.pgCharSelect.TabIndex = 1;
            this.pgCharSelect.Text = "tabPage2";
            this.pgCharSelect.UseVisualStyleBackColor = true;
            this.pgCharSelect.Click += new System.EventHandler(this.PgCharSelect_Click);
            // 
            // pnlCharTitle
            // 
            this.pnlCharTitle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlCharTitle.BackgroundImage")));
            this.pnlCharTitle.Controls.Add(this.label4);
            this.pnlCharTitle.Location = new System.Drawing.Point(252, 170);
            this.pnlCharTitle.Name = "pnlCharTitle";
            this.pnlCharTitle.Size = new System.Drawing.Size(389, 59);
            this.pnlCharTitle.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.SpringGreen;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(389, 59);
            this.label4.TabIndex = 0;
            this.label4.Text = "Select Character";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlCharSelect
            // 
            this.pnlCharSelect.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlCharSelect.BackgroundImage")));
            this.pnlCharSelect.Controls.Add(this.pbChar3);
            this.pnlCharSelect.Controls.Add(this.pbChar2);
            this.pnlCharSelect.Controls.Add(this.pbChar1);
            this.pnlCharSelect.Controls.Add(this.lblBacktoMain);
            this.pnlCharSelect.Controls.Add(this.lblChar3);
            this.pnlCharSelect.Controls.Add(this.lblChar2);
            this.pnlCharSelect.Controls.Add(this.lblChar1);
            this.pnlCharSelect.Location = new System.Drawing.Point(122, 245);
            this.pnlCharSelect.Name = "pnlCharSelect";
            this.pnlCharSelect.Size = new System.Drawing.Size(600, 300);
            this.pnlCharSelect.TabIndex = 0;
            // 
            // pbChar3
            // 
            this.pbChar3.Location = new System.Drawing.Point(457, 14);
            this.pbChar3.Name = "pbChar3";
            this.pbChar3.Size = new System.Drawing.Size(56, 56);
            this.pbChar3.TabIndex = 1;
            this.pbChar3.TabStop = false;
            this.pbChar3.Click += new System.EventHandler(this.PbChar3_Click);
            // 
            // pbChar2
            // 
            this.pbChar2.Location = new System.Drawing.Point(265, 128);
            this.pbChar2.Name = "pbChar2";
            this.pbChar2.Size = new System.Drawing.Size(56, 56);
            this.pbChar2.TabIndex = 1;
            this.pbChar2.TabStop = false;
            this.pbChar2.Click += new System.EventHandler(this.PbChar2_Click);
            // 
            // pbChar1
            // 
            this.pbChar1.Location = new System.Drawing.Point(96, 14);
            this.pbChar1.Name = "pbChar1";
            this.pbChar1.Size = new System.Drawing.Size(56, 56);
            this.pbChar1.TabIndex = 1;
            this.pbChar1.TabStop = false;
            this.pbChar1.Click += new System.EventHandler(this.PbChar1_Click);
            // 
            // lblBacktoMain
            // 
            this.lblBacktoMain.Font = new System.Drawing.Font("Lucida Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBacktoMain.ForeColor = System.Drawing.Color.SpringGreen;
            this.lblBacktoMain.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBacktoMain.Location = new System.Drawing.Point(0, 0);
            this.lblBacktoMain.Name = "lblBacktoMain";
            this.lblBacktoMain.Size = new System.Drawing.Size(77, 27);
            this.lblBacktoMain.TabIndex = 0;
            this.lblBacktoMain.Text = "<-- Go Back";
            this.lblBacktoMain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBacktoMain.Click += new System.EventHandler(this.LblBacktoMain_Click);
            // 
            // lblChar3
            // 
            this.lblChar3.Font = new System.Drawing.Font("Lucida Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChar3.ForeColor = System.Drawing.Color.SpringGreen;
            this.lblChar3.Location = new System.Drawing.Point(375, 73);
            this.lblChar3.Name = "lblChar3";
            this.lblChar3.Size = new System.Drawing.Size(222, 18);
            this.lblChar3.TabIndex = 0;
            this.lblChar3.Text = "(Empty)";
            this.lblChar3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblChar3.Click += new System.EventHandler(this.LblChar3_Click);
            // 
            // lblChar2
            // 
            this.lblChar2.Font = new System.Drawing.Font("Lucida Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChar2.ForeColor = System.Drawing.Color.SpringGreen;
            this.lblChar2.Location = new System.Drawing.Point(182, 187);
            this.lblChar2.Name = "lblChar2";
            this.lblChar2.Size = new System.Drawing.Size(222, 18);
            this.lblChar2.TabIndex = 0;
            this.lblChar2.Text = "(Empty)";
            this.lblChar2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblChar2.Click += new System.EventHandler(this.LblChar2_Click);
            // 
            // lblChar1
            // 
            this.lblChar1.Font = new System.Drawing.Font("Lucida Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChar1.ForeColor = System.Drawing.Color.SpringGreen;
            this.lblChar1.Location = new System.Drawing.Point(13, 73);
            this.lblChar1.Name = "lblChar1";
            this.lblChar1.Size = new System.Drawing.Size(222, 18);
            this.lblChar1.TabIndex = 0;
            this.lblChar1.Text = "(Empty)";
            this.lblChar1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblChar1.Click += new System.EventHandler(this.LblChar1_Click);
            // 
            // WinSplashScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 640);
            this.Controls.Add(this.pages1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WinSplashScreen";
            this.Text = "Pokémon Mystery Dungeon Online";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRemember)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pages1.ResumeLayout(false);
            this.pgHome.ResumeLayout(false);
            this.pgHome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDebug)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbConnected)).EndInit();
            this.pgLoading.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.pgCharSelect.ResumeLayout(false);
            this.pnlCharTitle.ResumeLayout(false);
            this.pnlCharSelect.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbChar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbChar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbChar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Client.Logic.Extensions.RichPassword tbPassword;
        private System.Windows.Forms.Button btnLogin;
        private Extensions.CheckBoxImage chkRemember;
        private System.Windows.Forms.Label lblRemember;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.RichTextBox rtbNews;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.RichTextBox tbUsername;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnOptions;
        private System.Windows.Forms.Button btnSkins;
        private System.Windows.Forms.Button btnNewAccount;
        private System.Windows.Forms.Label lblCopyright;
        private Extensions.Pages pages1;
        private System.Windows.Forms.TabPage pgHome;
        private System.Windows.Forms.TabPage pgCharSelect;
        private System.Windows.Forms.Panel pnlCharSelect;
        private System.Windows.Forms.PictureBox pbChar3;
        private System.Windows.Forms.PictureBox pbChar2;
        private System.Windows.Forms.PictureBox pbChar1;
        private System.Windows.Forms.Label lblChar3;
        private System.Windows.Forms.Label lblChar2;
        private System.Windows.Forms.Label lblChar1;
        private System.Windows.Forms.PictureBox pbConnected;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label lblBacktoMain;
        private System.Windows.Forms.Panel pnlCharTitle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage pgLoading;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblLoading;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button btnCredits;
        private System.Windows.Forms.PictureBox pbDebug;
        private System.Windows.Forms.TabPage tabPage1;
    }
}