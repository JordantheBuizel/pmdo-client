﻿// <copyright file="winServerOverride.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Network;
    using PMU.Sockets;

    public partial class WinServerOverride : Form
    {
        public WinServerOverride()
        {
            this.InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Messenger.SendPacket(TcpPacket.CreatePacket("serveroverride", this.tbPlayerName.Text, this.tbPasscode.Text));
            this.Close();
        }
    }
}
