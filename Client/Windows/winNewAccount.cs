﻿// <copyright file="winNewAccount.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Network;

    public partial class WinNewAccount : Form
    {
        public WinNewAccount()
        {
            this.InitializeComponent();
        }

        private void BtnCreateAccount_Click(object sender, EventArgs e)
        {
            string account = this.tbUsername.Text;
            string password = Security.Hash.GenerateMD5Hash(this.tbPassword.Text);
            if (this.tbPassword.Text == this.tbPassVerify.Text && !this.tbUsername.Text.Contains('\\') && !this.tbUsername.Text.Contains('\''))
            {
                if (NetworkManager.TcpClient.Socket.Connected)
                {
                    Messenger.SendCreateAccountRequest(account, password);
                    this.Hide();
                    WindowSwitcher.SplashScreen.ShowLoading();
                }
                else
                {
                    this.Close();
                    WindowSwitcher.ShowMainMenu();
                    MessageBox.Show("You are not connected to the Server!");
                }
            }
            else
            {
                MessageBox.Show("Passwords do not match or there are unsupported characters used in the username (\"'\" or \"\\\")");
            }
        }

        private void TbPassVerify_TextChanged(object sender, EventArgs e)
        {
            if (this.tbPassword.Text != this.tbPassVerify.Text)
            {
                this.lblVerifyInfo.Visible = true;
            }
            else
            {
                this.lblVerifyInfo.Visible = false;
            }
        }
    }
}
