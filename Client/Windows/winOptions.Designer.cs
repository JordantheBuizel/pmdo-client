﻿namespace Client.Logic.Windows
{
    partial class WinOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.pbHeader = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pbPDName = new System.Windows.Forms.PictureBox();
            this.pbPDDamage = new System.Windows.Forms.PictureBox();
            this.pbPDHPBar = new System.Windows.Forms.PictureBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pbNName = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pbNDamage = new System.Windows.Forms.PictureBox();
            this.pbNHPBar = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.pbSpeechBubbles = new System.Windows.Forms.PictureBox();
            this.pbSoundEffects = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.pbAutoScroll = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.pbRoleplay = new System.Windows.Forms.PictureBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.pbMovement = new System.Windows.Forms.PictureBox();
            this.pbMusic = new System.Windows.Forms.PictureBox();
            this.lblSave = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPDName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPDDamage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPDHPBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNDamage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNHPBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSpeechBubbles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSoundEffects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAutoScroll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRoleplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMovement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMusic)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("PMD", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 48);
            this.label1.TabIndex = 0;
            this.label1.Text = "Player Data:";
            // 
            // pbHeader
            // 
            this.pbHeader.BackColor = System.Drawing.Color.Transparent;
            this.pbHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbHeader.Location = new System.Drawing.Point(0, 0);
            this.pbHeader.Name = "pbHeader";
            this.pbHeader.Size = new System.Drawing.Size(410, 30);
            this.pbHeader.TabIndex = 1;
            this.pbHeader.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("PMD", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 48);
            this.label2.TabIndex = 0;
            this.label2.Text = "Player Data:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("PMD", 16F);
            this.label3.Location = new System.Drawing.Point(25, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 25);
            this.label3.TabIndex = 0;
            this.label3.Text = "Name:";
            this.label3.Click += new System.EventHandler(this.Label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("PMD", 16F);
            this.label4.Location = new System.Drawing.Point(139, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 25);
            this.label4.TabIndex = 0;
            this.label4.Text = "Damage:";
            this.label4.Click += new System.EventHandler(this.Label3_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("PMD", 16F);
            this.label5.Location = new System.Drawing.Point(267, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 25);
            this.label5.TabIndex = 0;
            this.label5.Text = "Mini-HP Bar:";
            this.label5.Click += new System.EventHandler(this.Label3_Click);
            // 
            // pbPDName
            // 
            this.pbPDName.Location = new System.Drawing.Point(80, 74);
            this.pbPDName.Name = "pbPDName";
            this.pbPDName.Size = new System.Drawing.Size(14, 14);
            this.pbPDName.TabIndex = 2;
            this.pbPDName.TabStop = false;
            this.pbPDName.Click += new System.EventHandler(this.PbPDName_Click);
            // 
            // pbPDDamage
            // 
            this.pbPDDamage.Location = new System.Drawing.Point(208, 74);
            this.pbPDDamage.Name = "pbPDDamage";
            this.pbPDDamage.Size = new System.Drawing.Size(14, 14);
            this.pbPDDamage.TabIndex = 2;
            this.pbPDDamage.TabStop = false;
            this.pbPDDamage.Click += new System.EventHandler(this.PbPDDamage_Click);
            // 
            // pbPDHPBar
            // 
            this.pbPDHPBar.Location = new System.Drawing.Point(365, 74);
            this.pbPDHPBar.Name = "pbPDHPBar";
            this.pbPDHPBar.Size = new System.Drawing.Size(14, 14);
            this.pbPDHPBar.TabIndex = 2;
            this.pbPDHPBar.TabStop = false;
            this.pbPDHPBar.Click += new System.EventHandler(this.PbPDHPBar_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(144, 104);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(44, 20);
            this.numericUpDown1.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("PMD", 16F);
            this.label6.Location = new System.Drawing.Point(25, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 25);
            this.label6.TabIndex = 0;
            this.label6.Text = "Auto-Save Speed";
            this.label6.Click += new System.EventHandler(this.Label3_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("PMD", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(22, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 48);
            this.label7.TabIndex = 0;
            this.label7.Text = "Npc Data:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("PMD", 16F);
            this.label8.Location = new System.Drawing.Point(25, 161);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 25);
            this.label8.TabIndex = 0;
            this.label8.Text = "Name:";
            this.label8.Click += new System.EventHandler(this.Label3_Click);
            // 
            // pbNName
            // 
            this.pbNName.Location = new System.Drawing.Point(80, 168);
            this.pbNName.Name = "pbNName";
            this.pbNName.Size = new System.Drawing.Size(14, 14);
            this.pbNName.TabIndex = 2;
            this.pbNName.TabStop = false;
            this.pbNName.Click += new System.EventHandler(this.PbNName_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("PMD", 16F);
            this.label9.Location = new System.Drawing.Point(139, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 25);
            this.label9.TabIndex = 0;
            this.label9.Text = "Damage:";
            this.label9.Click += new System.EventHandler(this.Label3_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("PMD", 16F);
            this.label10.Location = new System.Drawing.Point(267, 161);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 25);
            this.label10.TabIndex = 0;
            this.label10.Text = "Mini-HP Bar:";
            this.label10.Click += new System.EventHandler(this.Label3_Click);
            // 
            // pbNDamage
            // 
            this.pbNDamage.Location = new System.Drawing.Point(208, 168);
            this.pbNDamage.Name = "pbNDamage";
            this.pbNDamage.Size = new System.Drawing.Size(14, 14);
            this.pbNDamage.TabIndex = 2;
            this.pbNDamage.TabStop = false;
            this.pbNDamage.Click += new System.EventHandler(this.PbNDamage_Click);
            // 
            // pbNHPBar
            // 
            this.pbNHPBar.Location = new System.Drawing.Point(365, 168);
            this.pbNHPBar.Name = "pbNHPBar";
            this.pbNHPBar.Size = new System.Drawing.Size(14, 14);
            this.pbNHPBar.TabIndex = 2;
            this.pbNHPBar.TabStop = false;
            this.pbNHPBar.Click += new System.EventHandler(this.PbNHPBar_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("PMD", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(22, 177);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(158, 48);
            this.label11.TabIndex = 0;
            this.label11.Text = "Sound Data:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("PMD", 16F);
            this.label12.Location = new System.Drawing.Point(25, 225);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 25);
            this.label12.TabIndex = 0;
            this.label12.Text = "Music:";
            this.label12.Click += new System.EventHandler(this.Label3_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("PMD", 16F);
            this.label13.Location = new System.Drawing.Point(139, 225);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 25);
            this.label13.TabIndex = 0;
            this.label13.Text = "Sound Effects:";
            this.label13.Click += new System.EventHandler(this.Label3_Click);
            // 
            // pbSpeechBubbles
            // 
            this.pbSpeechBubbles.Location = new System.Drawing.Point(140, 295);
            this.pbSpeechBubbles.Name = "pbSpeechBubbles";
            this.pbSpeechBubbles.Size = new System.Drawing.Size(14, 14);
            this.pbSpeechBubbles.TabIndex = 2;
            this.pbSpeechBubbles.TabStop = false;
            this.pbSpeechBubbles.Click += new System.EventHandler(this.PbSpeechBubbles_Click);
            // 
            // pbSoundEffects
            // 
            this.pbSoundEffects.Location = new System.Drawing.Point(249, 233);
            this.pbSoundEffects.Name = "pbSoundEffects";
            this.pbSoundEffects.Size = new System.Drawing.Size(14, 14);
            this.pbSoundEffects.TabIndex = 2;
            this.pbSoundEffects.TabStop = false;
            this.pbSoundEffects.Click += new System.EventHandler(this.PbSoundEffects_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("PMD", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(23, 238);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(142, 48);
            this.label14.TabIndex = 0;
            this.label14.Text = "Chat Data:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("PMD", 16F);
            this.label15.Location = new System.Drawing.Point(25, 286);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(111, 25);
            this.label15.TabIndex = 0;
            this.label15.Text = "Speech Bubbles:";
            this.label15.Click += new System.EventHandler(this.Label3_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("PMD", 16F);
            this.label16.Location = new System.Drawing.Point(185, 286);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 25);
            this.label16.TabIndex = 0;
            this.label16.Text = "Auto-Scroll:";
            this.label16.Click += new System.EventHandler(this.Label3_Click);
            // 
            // pbAutoScroll
            // 
            this.pbAutoScroll.Location = new System.Drawing.Point(295, 295);
            this.pbAutoScroll.Name = "pbAutoScroll";
            this.pbAutoScroll.Size = new System.Drawing.Size(14, 14);
            this.pbAutoScroll.TabIndex = 2;
            this.pbAutoScroll.TabStop = false;
            this.pbAutoScroll.Click += new System.EventHandler(this.PbAutoScroll_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("PMD", 16F);
            this.label17.Location = new System.Drawing.Point(25, 312);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(119, 25);
            this.label17.TabIndex = 0;
            this.label17.Text = "Roleplay Channel:";
            this.label17.Click += new System.EventHandler(this.Label3_Click);
            // 
            // pbRoleplay
            // 
            this.pbRoleplay.Location = new System.Drawing.Point(150, 320);
            this.pbRoleplay.Name = "pbRoleplay";
            this.pbRoleplay.Size = new System.Drawing.Size(14, 14);
            this.pbRoleplay.TabIndex = 2;
            this.pbRoleplay.TabStop = false;
            this.pbRoleplay.Click += new System.EventHandler(this.PbRoleplay_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("PMD", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(23, 329);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(148, 48);
            this.label18.TabIndex = 0;
            this.label18.Text = "Movement:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("PMD", 16F);
            this.label19.Location = new System.Drawing.Point(26, 377);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(113, 25);
            this.label19.TabIndex = 0;
            this.label19.Text = "Movement Keys:";
            this.label19.Click += new System.EventHandler(this.Label3_Click);
            // 
            // pbMovement
            // 
            this.pbMovement.Image = global::Client.Logic.Properties.Resources.arrows;
            this.pbMovement.Location = new System.Drawing.Point(138, 375);
            this.pbMovement.Name = "pbMovement";
            this.pbMovement.Size = new System.Drawing.Size(64, 36);
            this.pbMovement.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbMovement.TabIndex = 2;
            this.pbMovement.TabStop = false;
            this.pbMovement.Click += new System.EventHandler(this.PbMovement_Click);
            // 
            // pbMusic
            // 
            this.pbMusic.Location = new System.Drawing.Point(83, 233);
            this.pbMusic.Name = "pbMusic";
            this.pbMusic.Size = new System.Drawing.Size(14, 14);
            this.pbMusic.TabIndex = 2;
            this.pbMusic.TabStop = false;
            this.pbMusic.Click += new System.EventHandler(this.PbMusic_Click);
            // 
            // lblSave
            // 
            this.lblSave.AutoSize = true;
            this.lblSave.Font = new System.Drawing.Font("PMD", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSave.Location = new System.Drawing.Point(287, 421);
            this.lblSave.Name = "lblSave";
            this.lblSave.Size = new System.Drawing.Size(78, 48);
            this.lblSave.TabIndex = 0;
            this.lblSave.Text = "Save";
            this.lblSave.Click += new System.EventHandler(this.LblSave_Click);
            // 
            // winOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Logic.Properties.Resources.menu_horizontal_border;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(410, 497);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.pbPDHPBar);
            this.Controls.Add(this.pbPDDamage);
            this.Controls.Add(this.pbNHPBar);
            this.Controls.Add(this.pbNDamage);
            this.Controls.Add(this.pbMusic);
            this.Controls.Add(this.pbSoundEffects);
            this.Controls.Add(this.pbAutoScroll);
            this.Controls.Add(this.pbRoleplay);
            this.Controls.Add(this.pbMovement);
            this.Controls.Add(this.pbSpeechBubbles);
            this.Controls.Add(this.pbNName);
            this.Controls.Add(this.pbPDName);
            this.Controls.Add(this.pbHeader);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblSave);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "winOptions";
            this.Text = "winOptions";
            ((System.ComponentModel.ISupportInitialize)(this.pbHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPDName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPDDamage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPDHPBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNDamage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNHPBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSpeechBubbles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSoundEffects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAutoScroll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRoleplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMovement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMusic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbHeader;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pbPDName;
        private System.Windows.Forms.PictureBox pbPDDamage;
        private System.Windows.Forms.PictureBox pbPDHPBar;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pbNName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pbNDamage;
        private System.Windows.Forms.PictureBox pbNHPBar;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pbSpeechBubbles;
        private System.Windows.Forms.PictureBox pbSoundEffects;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.PictureBox pbAutoScroll;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pbRoleplay;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pbMovement;
        private System.Windows.Forms.PictureBox pbMusic;
        private System.Windows.Forms.Label lblSave;
    }
}