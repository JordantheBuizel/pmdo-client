﻿namespace Client.Logic.Windows
{
    partial class WinMysteryGift
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbHeader = new System.Windows.Forms.PictureBox();
            this.btnCheck = new System.Windows.Forms.PictureBox();
            this.pbLoading = new System.Windows.Forms.PictureBox();
            this.lblChecking = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // pbHeader
            // 
            this.pbHeader.BackgroundImage = global::Client.Logic.Properties.Resources.mysteryGiftheader;
            this.pbHeader.InitialImage = global::Client.Logic.Properties.Resources.mysteryGiftheader;
            this.pbHeader.Location = new System.Drawing.Point(0, 1);
            this.pbHeader.Name = "pbHeader";
            this.pbHeader.Size = new System.Drawing.Size(512, 18);
            this.pbHeader.TabIndex = 0;
            this.pbHeader.TabStop = false;
            // 
            // btnCheck
            // 
            this.btnCheck.BackColor = System.Drawing.Color.Transparent;
            this.btnCheck.BackgroundImage = global::Client.Logic.Properties.Resources.btnCheck;
            this.btnCheck.Location = new System.Drawing.Point(28, 331);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(155, 25);
            this.btnCheck.TabIndex = 1;
            this.btnCheck.TabStop = false;
            this.btnCheck.Click += new System.EventHandler(this.PictureBox1_Click);
            // 
            // pbLoading
            // 
            this.pbLoading.BackColor = System.Drawing.Color.Transparent;
            this.pbLoading.BackgroundImage = global::Client.Logic.Properties.Resources._494;
            this.pbLoading.Location = new System.Drawing.Point(222, 151);
            this.pbLoading.Name = "pbLoading";
            this.pbLoading.Size = new System.Drawing.Size(53, 73);
            this.pbLoading.TabIndex = 2;
            this.pbLoading.TabStop = false;
            this.pbLoading.Visible = false;
            // 
            // lblChecking
            // 
            this.lblChecking.AutoSize = true;
            this.lblChecking.BackColor = System.Drawing.Color.Transparent;
            this.lblChecking.Font = new System.Drawing.Font("PMD", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChecking.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblChecking.Location = new System.Drawing.Point(147, 239);
            this.lblChecking.Name = "lblChecking";
            this.lblChecking.Size = new System.Drawing.Size(201, 36);
            this.lblChecking.TabIndex = 3;
            this.lblChecking.Text = "Checking for gifts...";
            this.lblChecking.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblChecking.Visible = false;
            // 
            // winMysteryGift
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Logic.Properties.Resources.mysteryGiftbg;
            this.ClientSize = new System.Drawing.Size(512, 384);
            this.Controls.Add(this.lblChecking);
            this.Controls.Add(this.pbLoading);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.pbHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "winMysteryGift";
            this.Text = "winMysteryGift";
            ((System.ComponentModel.ISupportInitialize)(this.pbHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoading)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbHeader;
        private System.Windows.Forms.PictureBox btnCheck;
        private System.Windows.Forms.PictureBox pbLoading;
        private System.Windows.Forms.Label lblChecking;
    }
}