﻿// <copyright file="winExpKit.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class WinExpKit : Core.WindowCore
    {
        private readonly ExpKit.KitContainer kitContainer;

        public WinExpKit()
            : base("winExpKit")
        {
            // this.Location = Graphics.DrawingSupport.GetCenter(this.Size);
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.Size = new System.Drawing.Size(200, 450);
            this.Location = new System.Drawing.Point(5, WindowSwitcher.GameWindow.ActiveTeam.Y + WindowSwitcher.GameWindow.ActiveTeam.Height + 5);
            this.AlwaysOnTop = true;
            this.TitleBar.CloseButton.Visible = false;
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.TitleBar.Text = "Explorer Kit";
            this.TitleBar.FillColor = System.Drawing.Color.Transparent;
            this.TitleBar.BackgroundImageSizeMode = ImageSizeMode.StretchImage;
            this.TitleBar.BackgroundImage = Skins.SkinManager.LoadGuiElement("Game Window", "Widgets/expkitTitleBar.png");

            this.kitContainer = new ExpKit.KitContainer("kitContainer");
            this.kitContainer.Location = new System.Drawing.Point(0, 0);
            this.kitContainer.Size = this.Size;

            this.AddWidget(this.kitContainer);

            this.LoadComplete();
        }

        public ExpKit.KitContainer KitContainer
        {
            get { return this.kitContainer; }
        }
    }
}
