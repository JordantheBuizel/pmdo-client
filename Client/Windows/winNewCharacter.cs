﻿// <copyright file="winNewCharacter.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Network;
    using SdlDotNet.Widgets;

    internal class WinNewCharacter : Core.WindowCore
    {
        private readonly Label lblName;
        private readonly TextBox txtName;
        private readonly Label lblCreateChar;
        private readonly Label lblBack;
        private readonly RadioButton optMale;
        private readonly RadioButton optFemale;
        private readonly int charSlot;

        public WinNewCharacter(int charSlot)
            : base("winNewCharacter")
            {
            this.charSlot = charSlot;

            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.TitleBar.Text = "New Character";
            this.TitleBar.CloseButton.Visible = false;

            // this.BackgroundImage = Skins.SkinManager.LoadGui("New Character"); - We should have a better GUI for this.
            // this.Size = this.BackgroundImage.Size;
            this.Size = new Size(400, 200);
            this.Location = new Point(DrawingSupport.GetCenter(SdlDotNet.Graphics.Video.Screen.Size, this.Size).X, 5);

            this.lblName = new Label("lblName");
            this.lblName.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblName.Location = new Point(40, 40);
            this.lblName.AutoSize = true;
            this.lblName.Text = "Name:";

            this.txtName = new TextBox("txtName");
            this.txtName.Size = new Size(177, 16);
            this.txtName.Location = new Point(40, 70);

            this.lblCreateChar = new Label("lblCreateChar");
            this.lblCreateChar.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblCreateChar.Location = new Point(40, 130);
            this.lblCreateChar.AutoSize = true;
            this.lblCreateChar.Text = "Create Character";
            this.lblCreateChar.Click += new EventHandler<MouseButtonEventArgs>(this.LblCreateChar_Click);

            this.lblBack = new Label("btnBack");
            this.lblBack.Font = Graphic.FontManager.LoadFont("PMU", 18);
            this.lblBack.Location = new Point(147, 130);
            this.lblBack.AutoSize = true;
            this.lblBack.Text = "Back to Login Screen";
            this.lblBack.Click += new EventHandler<MouseButtonEventArgs>(this.LblBack_Click);

            this.optMale = new RadioButton("optMale");
            this.optMale.BackColor = Color.Transparent;
            this.optMale.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.optMale.Location = new Point(240, 50);
            this.optMale.Size = new Size(95, 17);
            this.optMale.Text = "Male";
            this.optMale.Checked = true;

            this.optFemale = new RadioButton("optFemale");
            this.optFemale.BackColor = Color.Transparent;
            this.optFemale.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.optFemale.Location = new Point(240, 70);
            this.optFemale.Size = new Size(95, 17);
            this.optFemale.Text = "Female";
            this.optFemale.Checked = false;

            this.AddWidget(this.lblName);
            this.AddWidget(this.txtName);
            this.AddWidget(this.lblCreateChar);
            this.AddWidget(this.lblBack);
            this.AddWidget(this.optMale);
            this.AddWidget(this.optFemale);
            this.LoadComplete();
        }

        private void LblBack_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            Messenger.SendCharListRequest();
        }

        // This is made not to decide rather the player is male or female, but only for sending the new name.
        private void LblCreateChar_Click(object sender, MouseButtonEventArgs e)
        {
            string name = this.txtName.Text;
            Enums.Sex sex = Enums.Sex.Male;
            if (this.optFemale.Checked)
            {
                sex = Enums.Sex.Female;
            }

            Messenger.SendNewChar(name, sex, this.charSlot);
            this.Close();
        }
    }
}
