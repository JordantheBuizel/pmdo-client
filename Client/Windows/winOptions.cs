﻿// <copyright file="winOptions.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Drawing.Text;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Network;

    public partial class WinOptions : Form
    {
        private bool rightClickAllowed = false;
        private int clicks = 0;

        private bool drag = false; // determine if we should be moving the form
        private Point startPoint = new Point(0, 0); // also for the moving

        private List<bool> settingsList = new List<bool>();

        private List<PictureBox> pictureBoxes = new List<PictureBox>();
        private readonly PrivateFontCollection pfc = new PrivateFontCollection();

        public WinOptions()
        {
            this.InitializeComponent();
            this.UpdateSelected();

            this.pbHeader.MouseDown += new MouseEventHandler(this.Title_MouseDown);
            this.pbHeader.MouseUp += new MouseEventHandler(this.Title_MouseUp);
            this.pbHeader.MouseMove += new MouseEventHandler(this.Title_MouseMove);

            this.BackgroundImage = Image.FromFile(Path.Combine(Application.StartupPath, "Skins/" + Skins.SkinManager.ActiveSkin.Name + "/General/Menus/menu-vertical-border.png"));

            this.numericUpDown1.Value = IO.Options.AutoSaveSpeed;
            this.numericUpDown1.ValueChanged += new EventHandler(this.NumericUpDown1_ValueChanged);
            this.numericUpDown1.Minimum = 0;
            this.numericUpDown1.Maximum = 10;
            this.numericUpDown1.Increment = 1;
            this.TopMost = true;

            string fontLoc = Path.Combine(Application.StartupPath, "Fonts", "PMD.ttf");
            this.pfc.AddFontFile(fontLoc);
            Font pMDfontSmall = new Font(this.pfc.Families[0], 16);
            Font pMDfontBig = new Font(this.pfc.Families[0], 32);
            foreach (Control c in this.Controls)
            {
                if (c is Label && c.Font.Size == 16)
                {
                    c.Font = pMDfontSmall;
                }
                else if (c is Label)
                {
                    c.Font = pMDfontBig;
                }
            }

            this.MouseDown += new MouseEventHandler(this.WinOptions_MouseDown);
            this.Focus();
        }

        private void WinOptions_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                this.rightClickAllowed = true;
                return;
            }

            if (this.rightClickAllowed)
            {
                if (e.Button == MouseButtons.Right)
                {
                    this.clicks++;
                }

                if (this.clicks == 5)
                {
                    this.clicks = 0;
                    this.rightClickAllowed = false;
                    WinServerOverride win = new WinServerOverride();
                    this.TopMost = false;
                    win.ShowDialog();
                    this.TopMost = true;
                }
            }
        }

        private void NumericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            IO.Options.AutoSaveSpeed = (int)this.numericUpDown1.Value;
        }

        private void Label3_Click(object sender, EventArgs e)
        {
        }

        private void Label6_Click(object sender, EventArgs e)
        {
        }

        private void LblSave_Click(object sender, EventArgs e)
        {
            IO.Options.SaveXml();
            if (Globals.InGame)
            {
                Messenger.SendRefresh();
            }
            else
            {
                Music.Music.AudioPlayer.PlayMusic("PMD3) Title.ogg");
            }

            this.Close();
        }

        private void PbPDName_Click(object sender, EventArgs e)
        {
            IO.Options.PlayerName = !IO.Options.PlayerName;
            this.UpdateSelected();
        }

        private void PbPDDamage_Click(object sender, EventArgs e)
        {
            IO.Options.PlayerDamage = !IO.Options.PlayerDamage;
            this.UpdateSelected();
        }

        private void PbPDHPBar_Click(object sender, EventArgs e)
        {
            IO.Options.PlayerBar = !IO.Options.PlayerBar;
            this.UpdateSelected();
        }

        private void PbNName_Click(object sender, EventArgs e)
        {
            IO.Options.NpcName = !IO.Options.NpcName;
            this.UpdateSelected();
        }

        private void PbNDamage_Click(object sender, EventArgs e)
        {
            IO.Options.NpcDamage = !IO.Options.NpcDamage;
            this.UpdateSelected();
        }

        private void PbNHPBar_Click(object sender, EventArgs e)
        {
            IO.Options.NpcBar = !IO.Options.NpcBar;
            this.UpdateSelected();
        }

        private void PbMusic_Click(object sender, EventArgs e)
        {
            IO.Options.Music = !IO.Options.Music;
            this.UpdateSelected();
        }

        private void PbSoundEffects_Click(object sender, EventArgs e)
        {
            IO.Options.Sound = !IO.Options.Sound;
            this.UpdateSelected();
        }

        private void PbSpeechBubbles_Click(object sender, EventArgs e)
        {
            IO.Options.SpeechBubbles = !IO.Options.SpeechBubbles;
            this.UpdateSelected();
        }

        private void PbAutoScroll_Click(object sender, EventArgs e)
        {
            IO.Options.AutoScroll = !IO.Options.AutoScroll;
            this.UpdateSelected();
        }

        private void PbRoleplay_Click(object sender, EventArgs e)
        {
            IO.Options.EnableRolePlay = !IO.Options.EnableRolePlay;
            this.UpdateSelected();
        }

        private void PbMovement_Click(object sender, EventArgs e)
        {
            IO.Options.ArrowMoveKeys = !IO.Options.ArrowMoveKeys;

            if (IO.Options.ArrowMoveKeys)
            {
                this.pbMovement.Image = Properties.Resources.arrows;
            }
            else
            {
                this.pbMovement.Image = Properties.Resources.wsad;
            }

            this.UpdateSelected();
        }

        private void UpdateSelected()
        {
            this.settingsList = new List<bool>
        {
            IO.Options.PlayerName,
            IO.Options.PlayerDamage,
            IO.Options.PlayerBar,
            IO.Options.NpcName,
            IO.Options.NpcDamage,
            IO.Options.NpcBar,
            IO.Options.Music,
            IO.Options.Sound,
            IO.Options.SpeechBubbles,
            IO.Options.AutoScroll,
            IO.Options.EnableRolePlay
        };

            this.pictureBoxes = new List<PictureBox>
        {
            this.pbPDName,
            this.pbPDDamage,
            this.pbPDHPBar,
            this.pbNName,
            this.pbNDamage,
            this.pbNHPBar,
            this.pbMusic,
            this.pbSoundEffects,
            this.pbSpeechBubbles,
            this.pbAutoScroll,
            this.pbRoleplay
        };

            for (int i = 0; i < this.pictureBoxes.Count; i++)
            {
                if (this.settingsList[i] == true)
                {
                    this.pictureBoxes[i].Image = Image.FromFile(Path.Combine(Application.StartupPath, "Skins/" + Skins.SkinManager.ActiveSkin.Name + "/Widgets/CheckBox/checked.png"));
                }
                else
                {
                    this.pictureBoxes[i].Image = Image.FromFile(Path.Combine(Application.StartupPath, "Skins/" + Skins.SkinManager.ActiveSkin.Name + "/Widgets/CheckBox/unchecked.png"));
                }
            }

            // Timestamps are removed, so they are always false
            IO.Options.Timestamps = false;
        }

        private void Title_MouseUp(object sender, MouseEventArgs e)
        {
            this.drag = false;
        }

        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            this.startPoint = e.Location;
            this.drag = true;
        }

        private void Title_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.drag)
            { // if we should be dragging it, we need to figure out some movement
                Point p1 = new Point(e.X, e.Y);
                Point p2 = this.PointToScreen(p1);
                Point p3 = new Point(
                    p2.X - this.startPoint.X,
                                     p2.Y - this.startPoint.Y);
                this.Location = p3;
            }
        }
    }
}
