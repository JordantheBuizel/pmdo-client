﻿// <copyright file="winAccountSettings.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class WinAccountSettings : Core.WindowCore
    {
        private readonly Button btnChangePassword;
        private readonly Label lblBack;

        public WinAccountSettings()
            : base("winAccountSettings")
            {
            this.Windowed = true;
            this.ShowInWindowSwitcher = false;
            this.TitleBar.Text = "Account Settings";
            this.TitleBar.CloseButton.Visible = false;
            this.Size = new Size(260, 200);

            // this.BackgroundImage = Skins.SkinManager.LoadGui("Account Settings");
            this.Location = new Point(DrawingSupport.GetCenter(SdlDotNet.Graphics.Video.Screen.Size, this.Size).X, 5);

            this.btnChangePassword = new Button("btnChangePassword");
            this.btnChangePassword.Font = Graphic.FontManager.LoadFont("PMU", 24);
            this.btnChangePassword.Text = "Change Password";
            this.btnChangePassword.Location = new Point(65, 55);
            this.btnChangePassword.Size = new Size(130, 32);
            this.btnChangePassword.Click += new EventHandler<MouseButtonEventArgs>(this.BtnChangePassword_Click);

            this.lblBack = new Label("lblBack");
            this.lblBack.Font = Graphic.FontManager.LoadFont("PMU", 24);
            this.lblBack.Text = "Return to Login Screen";
            this.lblBack.Location = new Point(45, 100);
            this.lblBack.AutoSize = true;
            this.lblBack.ForeColor = Color.Black;
            this.lblBack.Click += new EventHandler<MouseButtonEventArgs>(this.LblBack_Click);

            this.AddWidget(this.btnChangePassword);
            this.AddWidget(this.lblBack);

            this.LoadComplete();
       }

        private void BtnChangePassword_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            WindowManager.AddWindow(new WinChangePassword());
       }

        private void LblBack_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            WindowSwitcher.ShowMainMenu();
       }
    }
}
