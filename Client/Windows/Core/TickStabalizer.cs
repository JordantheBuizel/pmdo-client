﻿// <copyright file="TickStabalizer.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Core
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class TickStabalizer
    {
        private static float lastVal = 0;

        public static bool CanUpdate(SdlDotNet.Core.TickEventArgs e)
        {
            float val = 1.0f / 1000 * e.TicksElapsed * Constants.FRAMERATE;
            val += lastVal;
            if (val > 1)
            {
                lastVal = 0;
                return true;
            }
else
            {
                lastVal = val;
                return false;
            }
        }
    }
}