﻿// <copyright file="GameScreen.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Core
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.Text;
    using System.Threading;
    using System.Windows.Forms;

    using Graphic;

    using SdlEvents = SdlDotNet.Core.Events;
    using SdlGfx = SdlDotNet.Graphics;
    using SdlVideo = SdlDotNet.Graphics.Video;
    using Gfx = Graphic;

    /// <summary>
    /// The actual screen where the game is drawn onto.
    /// </summary>
    public class GameScreen
    {
        internal FadeCallback fadeCallback;
        internal bool FadeOut;
        internal byte FadeSpeed;
        internal bool IsFading;

        private int lastTick = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameScreen"/> class.
        /// </summary>
        public GameScreen()
        {
        }

        internal delegate void FadeCallback();

        // internal Gui.TaskBar.TaskBar TaskBar {
        //    get { return tskTaskBar; }
        // }

        internal void InitControls()
        {
            // Load the debug label
            // lblInfo = new Client.Logic.Gui.Label(FontManager.MainFont);
            // lblInfo.ForeColor = Color.Black;
            // lblInfo.Backcolor = Color.Transparent;
            // lblInfo.AntiAlias = false;

            // btnDebug = new Client.Logic.Gui.Button(Logic.Graphic.FontManager.LoadFont("PMU", 24));
            // btnDebug.Size = new Size(150, 40);
            // btnDebug.Location = new Point(0, 50);
            // btnDebug.Text = "Show Debug Window";
            // btnDebug.OnClick += new EventHandler<SdlDotNet.Input.MouseButtonEventArgs>(btnDebug_OnClick);
            // btnDebug.HoverColor = Color.SteelBlue;
            // btnDebug.BorderWidth = 1;
            // btnDebug.BorderColor = Color.Black;
            // btnDebug.Backcolor = SystemColors.Control;
            // btnDebug.AddEvents();

            // tskTaskBar = new Client.Logic.Gui.TaskBar.TaskBar();
            // tskTaskBar.Location = new Point(0, SdlVideo.Screen.Height - 20);
        }

        private void DebugWindow_OnWindowClosed(object sender, EventArgs e)
        {
            // btnDebug.Text = "Show Debug Window";
        }

        private void SdlEvents_Tick(object sender, SdlDotNet.Core.TickEventArgs e)
        {
            // if (SdlDotNet.Graphics.Video.IsActive == false)
            //    return;
            // if (TickStabalizer.CanUpdate(e)) {
            // if (Gfx.GuiManager.ScreenBackground != null) {
            //    SdlVideo.Screen.Blit(Gfx.GuiManager.ScreenBackground);
            // } else {
            //    SdlVideo.Screen.Fill(Color.SteelBlue);
            // }

            // Version info
            // if (Globals.GameLoaded) {
            //    if (lblInfo != null) {
            //        if (SdlDotNet.Core.Timer.TicksElapsed > lastTick + 1000) {
            //            lblInfo.Text = "Version: PMU 7 [Alpha-Debug]\tFPS: " + e.Fps.ToString();
            //            lastTick = SdlDotNet.Core.Timer.TicksElapsed;
            //        }
            //        lblInfo.Update(SdlDotNet.Graphics.Video.Screen, e);
            //    }
            // }
            // if (Globals.InDebugMode && Globals.GameLoaded) {
            //    if (btnDebug != null) {
            //        btnDebug.Update(SdlDotNet.Graphics.Video.Screen, e);
            //    }
            // }

            // SdlDotNet.Widgets.WindowManager.DrawWindows(e);

            // if (Globals.GameLoaded) {
            //    tskTaskBar.Update(SdlVideo.Screen, e);
            // }
            if (this.IsFading)
            {
                if (this.FadeOut)
                {
                    if (GraphicsManager.FadeSurface.Alpha < 255)
                    {
                        GraphicsManager.FadeSurface.Alpha += (byte)this.FadeSpeed;
                    }
else
                    {
                        this.IsFading = false;
                        this.fadeCallback();
                    }
                }
else
                {
                    if (GraphicsManager.FadeSurface.Alpha > 0)
                    {
                        GraphicsManager.FadeSurface.Alpha -= (byte)this.FadeSpeed;
                    }
else
                    {
                        this.IsFading = false;
                        this.fadeCallback();
                    }
                }

                SdlDotNet.Graphics.Video.Screen.Blit(GraphicsManager.FadeSurface);
            }

            // SdlVideo.Screen.Update();
            // }
        }

        private void BtnDebug_OnClick(object sender, SdlDotNet.Input.MouseButtonEventArgs e)
        {
            // if (SdlDotNet.Widgets.WindowManager.IsWindowOpen(Windows.WindowSwitcher.GetWindow(Windows.WindowSwitcher.Window.Debug)) == false) {
            //    WindowManager.AddWindow(Windows.WindowSwitcher.Window.Debug);
            //    Windows.WindowSwitcher.DebugWindow.OnWindowClosed += new EventHandler(DebugWindow_OnWindowClosed);
            //    btnDebug.Text = "Close Debug Window";
            // } else {
            //    Windows.WindowSwitcher.DebugWindow.OnWindowClosed -= new EventHandler(DebugWindow_OnWindowClosed);
            //    WindowManager.RemoveWindow(Windows.WindowSwitcher.Window.Debug);
            //    btnDebug.Text = "Show Debug Window";
            // }
        }
    }
}