﻿// <copyright file="WindowCore.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows.Core
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class WindowCore : Window
    {
        public WindowCore(string name)
            : base(name)
            {
            this.ShowInWindowSwitcher = false;
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.TitleBar.BackColor = Color.Blue;
            this.TitleBar.FillColor = Color.White;

            // this.AlwaysOnTop = true;
            this.BackColor = Color.White;
            this.BorderStyle = BorderStyle.None;
            this.BorderWidth = 2;
            this.BorderColor = Color.Black;
        }
    }
}
