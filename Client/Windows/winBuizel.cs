﻿// <copyright file="winBuizel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class WinBuizel : Form
    {
        private bool drag = false; // determine if we should be moving the form
        private Point startPoint = new Point(0, 0); // also for the moving

        public WinBuizel()
        {
            this.InitializeComponent();

            this.pbHeader.MouseDown += new MouseEventHandler(this.Title_MouseDown);
            this.pbHeader.MouseUp += new MouseEventHandler(this.Title_MouseUp);
            this.pbHeader.MouseMove += new MouseEventHandler(this.Title_MouseMove);

            this.pictureBox1.Image = Image.FromFile(Path.Combine(Application.StartupPath, "Skins/" + Skins.SkinManager.ActiveSkin.Name + "/Widgets/Windows/closebutton.png"));
        }

        private void Title_MouseUp(object sender, MouseEventArgs e)
        {
            this.drag = false;
        }

        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            this.startPoint = e.Location;
            this.drag = true;
        }

        private void Title_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.drag)
            { // if we should be dragging it, we need to figure out some movement
                Point p1 = new Point(e.X, e.Y);
                Point p2 = this.PointToScreen(p1);
                Point p3 = new Point(
                    p2.X - this.startPoint.X,
                                     p2.Y - this.startPoint.Y);
                this.Location = p3;
            }
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
