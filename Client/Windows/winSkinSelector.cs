﻿// <copyright file="winSkinSelector.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using Extensions;
    using Network;
    using Skins;
    using PMU.Compression.Zip;
    using SdlDotNet.Widgets;
    using Gfx = Graphic;

    internal class WinSkinSelector : Core.WindowCore
    {
        private readonly PictureBox picSkinPreview;
        private readonly ComboBox cmbSkinSelect;
        private readonly Button btnSave;
        private readonly Button btnFindSkin;
        private readonly Button btnCancel;

        private Skin loadedSkin;

        public WinSkinSelector()
            : base("winSkinSelector")
            {
            this.Windowed = true;
            this.Size = new Size(390, 300);
            this.Location = DrawingSupport.GetCenter(SdlDotNet.Graphics.Video.Screen.Size, this.Size);
            this.TitleBar.BackgroundImageSizeMode = ImageSizeMode.StretchImage;
            this.TitleBar.BackgroundImage = SkinManager.LoadGuiElement("Skin Selector", "titlebar.png");
            this.TitleBar.CloseButton.Visible = false;
            this.Text = "Skin Selector";
            this.BackgroundImageSizeMode = ImageSizeMode.StretchImage;
            this.BackgroundImage = SkinManager.LoadGui("Skin Selector");

            this.picSkinPreview = new PictureBox("picSkinPreview");
            this.picSkinPreview.SizeMode = ImageSizeMode.StretchImage;
            this.picSkinPreview.Size = new Size(242, 182);
            this.picSkinPreview.Location = new Point(DrawingSupport.GetCenter(this.Width, this.picSkinPreview.Width), 10);
            this.picSkinPreview.BackColor = Color.Green;
            this.picSkinPreview.BorderStyle = BorderStyle.FixedSingle;
            this.picSkinPreview.BorderColor = Color.Black;
            this.picSkinPreview.BorderWidth = 1;

            this.cmbSkinSelect = new ComboBox("cmbSkinSelect");
            this.cmbSkinSelect.Location = new Point(10, 200);
            this.cmbSkinSelect.Size = new Size(this.Width - 20, 30);
            this.cmbSkinSelect.ItemSelected += new EventHandler(this.CmbSkinSelect_ItemSelected);

            this.btnSave = new Button("btnSave");
            this.btnSave.Size = new Size(100, 30);
            this.btnSave.Location = new Point(10, 240);
            this.btnSave.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.btnSave.Text = "Switch Skins!";
            this.btnSave.Click += new EventHandler<MouseButtonEventArgs>(this.BtnSave_Click);

            this.btnFindSkin = new Button("btnFindSkin");
            this.btnFindSkin.Size = new Size(100, 30);
            this.btnFindSkin.Location = new Point(110, 240);
            this.btnFindSkin.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.btnFindSkin.Text = "Find Skin";
            this.btnFindSkin.Click += new EventHandler<MouseButtonEventArgs>(this.BtnFindSkin_Click);

            this.btnCancel = new Button("btnCancel");
            this.btnCancel.Size = new Size(100, 30);
            this.btnCancel.Location = new Point(210, 240);
            this.btnCancel.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new EventHandler<MouseButtonEventArgs>(this.BtnCancel_Click);

            this.AddWidget(this.picSkinPreview);
            this.AddWidget(this.cmbSkinSelect);
            this.AddWidget(this.btnSave);
            this.AddWidget(this.btnFindSkin);
            this.AddWidget(this.btnCancel);

            this.PopulateSkinList();
        }

        private void BtnFindSkin_Click(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Skin Package|*.pmuskn";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                bool installed = SkinManager.InstallSkin(ofd.FileName);
                if (installed)
                {
                    this.PopulateSkinList();
                }
else
                {
                    MessageBox.Show("The selected file is not a valid skin package.", "Invalid Package");
                }
            }
        }

        private void CmbSkinSelect_ItemSelected(object sender, EventArgs e)
        {
            if (this.loadedSkin != null)
            {
                this.loadedSkin.Unload();
            }

            this.loadedSkin = new Skin();
            if (this.cmbSkinSelect.SelectedItem != null)
            {
                this.loadedSkin.LoadSkin(this.cmbSkinSelect.SelectedItem.TextIdentifier);
                this.picSkinPreview.Image = Graphic.SurfaceManager.LoadSurface(IO.Paths.SkinPath + this.loadedSkin.Name + "/Configuration/preview.png");
            }
        }

        private void BtnSave_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.cmbSkinSelect.SelectedItem != null)
            {
                SkinManager.ChangeActiveSkin(this.cmbSkinSelect.SelectedItem.TextIdentifier);
                SkinManager.PlaySkinMusic();
            }

            WindowSwitcher.ShowMainMenu();
            this.Close();
        }

        private void BtnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            WindowSwitcher.ShowMainMenu();
            this.Close();
        }

        private void PopulateSkinList()
        {
            Thread thread = new Thread(new ThreadStart(this.PopulateSkinListBackgroud));
            thread.IsBackground = true;
            thread.Start();
        }

        private void PopulateSkinListBackgroud()
        {
            this.cmbSkinSelect.Items.Clear();
            string[] directories = Directory.GetDirectories(IO.Paths.SkinPath);
            foreach (string file in directories)
            {// System.IO.Directory.EnumerateDirectories(IO.Paths.SkinPath)) {
                string name = Path.GetFileNameWithoutExtension(file);
                if (!string.IsNullOrEmpty(name))
                {
                    this.cmbSkinSelect.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 12), name));
                }
            }

            this.cmbSkinSelect.SelectItem(SkinManager.ActiveSkin.Name);
        }
    }
}
