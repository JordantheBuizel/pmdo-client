﻿// <copyright file="winPM.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

using Client.Logic.Extensions;

namespace Client.Logic.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Text;
    using System.IO;
    using System.Reflection;
    using System.Windows.Forms;
    using System.Xml;
    using ExpKit.Modules;
    using Graphic;
    using Network;

    public partial class WinPM : Form
    {
        /*private int moduleIndex;
        private bool enabled;
        private string selectedChannel = "Local";*/

        private static WinPM instance;

        private readonly string recepientName;
        private readonly Timer timer;
        private bool drag = false; // determine if we should be moving the form
        private Point startPoint = new Point(0, 0); // also for the moving
        private readonly PrivateFontCollection pfc = new PrivateFontCollection();
        private string[] cachedTextArray = new string[0];
        private int upArrowCounter = 0;
        private Color pMColor = Color.WhiteSmoke;

        public WinPM(string charName)
        {
            this.InitializeComponent();

            this.TopMost = false;

            this.timer = new Timer();
            this.timer.Tick += new EventHandler(this.Timer_Tick);
            this.timer.Start();

            this.recepientName = charName;

            this.pbHeader.MouseDown += new MouseEventHandler(this.Title_MouseDown);
            this.pbHeader.MouseUp += new MouseEventHandler(this.Title_MouseUp);
            this.pbHeader.MouseMove += new MouseEventHandler(this.Title_MouseMove);
            this.txtCommands.KeyDown += new KeyEventHandler(this.TxtCommands_KeyDown);
            this.checkBox1.CheckedChanged += new EventHandler(this.CheckBox1_CheckedChanged);

            // rtbChat.Font = LoadWindowsFont("PMU", 9);
            // channelSelector.Font = LoadWindowsFont("PMU", 9);
            // lblChannel.Font = LoadWindowsFont("tahoma", 9);
            // checkBox1.Font = LoadWindowsFont("tahoma", 9);
            this.pbHeader.Image = Image.FromFile(Path.Combine(Application.StartupPath, "Skins/" + Skins.SkinManager.ActiveSkin.Name + "/Game Window/Widgets/expkitTitleBar.png"));

            string fontLoc = Path.Combine(Application.StartupPath, "Fonts", "PMD.ttf");
            this.pfc.AddFontFile(fontLoc);
            this.rtbChat.Font = new Font(this.pfc.Families[0], 12);

            ToolTip tip = new ToolTip();
            tip.SetToolTip(this.pbHeader, "PM session with " + this.recepientName);
        }

        public delegate void Action();

        public static WinPM Instance
        {
            get { return instance; }
        }

        public RichTextBox Chat
        {
            get
            {
                return this.rtbChat;
            }
        }

        public static Font LoadWindowsFont(string fontName, int emSize)
        {
            if (fontName.EndsWith(".ttf") == false)
            {
                fontName += ".ttf";
            }

            return new Font(IO.Paths.FontPath + fontName, emSize);
        }

        public void FocusChat(string dummy)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<string>(this.FocusChat), new object[] { dummy });
                return;
            }

            this.Focus();
            this.txtCommands.Focus();
            this.txtCommands.Show();
        }

        public void AppendChat(string text, Color color)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<string, Color>(this.AppendChat), new object[] { text, color });
                return;
            }

            if (this.pMColor != color)
            {
                this.rtbChat.AppendText(text, this.pMColor);
            }
            else
            {
                this.rtbChat.AppendText(text, color);
            }

            if (!text.Contains(Players.PlayerManager.MyPlayer.Name) && !text.Contains("Did you know you can change the PM message color? You just have to use the command /color"))
            {
                this.notifyPM.ShowBalloonTip(4000, this.recepientName, text.Split(':')[1], ToolTipIcon.Info);
            }
        }

        /// <inheritdoc/>
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            instance = this;
        }

        /// <inheritdoc/>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            instance = null;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (this.timer.Interval == 900)
            {
                this.rtbChat.AppendText("Did you know you can change the PM message color? You just have to use the command /color {red value} {blue value} {green value} to change it! \n", Color.Green);
                this.timer.Stop();
            }
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = !this.TopMost;
        }

        private void TxtCommands_KeyDown(object sender, KeyEventArgs e)
        {
            List<string> textCache = new List<string>();

            if (e.KeyData == Keys.Return)
            {
                if (this.txtCommands.Text.StartsWith("/color"))
                {
                    try
                    {
                        string[] args = this.txtCommands.Text.Split(' ');
                        this.pMColor = Color.FromArgb(int.Parse(args[1]), int.Parse(args[2]), int.Parse(args[3]));
                        this.txtCommands.Text = string.Empty;
                        return;
                    }
                    catch
                    {
                        this.AppendChat("Error: /color. Correct Syntax: [/color {(int)r} {(int)g} {(int)b} \n", Color.Red);
                        this.txtCommands.Text = string.Empty;
                        return;
                    }
                }

                e.Handled = e.SuppressKeyPress = true;
                Messenger.SendPrivateMessage(this.txtCommands.Text + " \n", this.recepientName);
                this.AppendChat(Players.PlayerManager.MyPlayer.Name + ": " + this.txtCommands.Text + " \n", Color.WhiteSmoke);
                this.txtCommands.Text = string.Empty;
                this.upArrowCounter = 0;
            }

            /*if (((e.Mod & SdlDotNet.Input.ModifierKeys.LeftControl) != 0) && (e.Key == SdlDotNet.Input.Key.X))
            {
                try
                {
                    txtCommands.Text = txtCommands.Text.Substring(0, txtCommands.Text.Length - 1);
                    System.Windows.Forms.Clipboard.SetText(txtCommands.Text);
                    txtCommands.Text = "";
                }
                catch
                {

                }
            }

            if (((e.Mod & SdlDotNet.Input.ModifierKeys.LeftControl) != 0) && e.Key == SdlDotNet.Input.Key.C)
            {
                try
                {
                    txtCommands.Text = txtCommands.Text.Substring(0, txtCommands.Text.Length - 1);
                    System.Windows.Forms.Clipboard.SetText(txtCommands.Text);
                }
                catch
                {

                }
            }

            if (((e.Mod & SdlDotNet.Input.ModifierKeys.LeftControl) != 0) && e.Key == SdlDotNet.Input.Key.V)
            {
                try
                {
                    txtCommands.Text = txtCommands.Text.Substring(0, txtCommands.Text.Length - 1);
                    txtCommands.Text = txtCommands.Text + System.Windows.Forms.Clipboard.GetText();
                }
                catch
                {
                    AppendChat("The paste text must be only standard text (abc123,./!$% etc.)", Color.Red);
                }
            }*/

            if (e.KeyData == Keys.Up)
            {
                XmlDocument cacheXml = new XmlDocument();
                cacheXml.Load(Path.Combine(Application.StartupPath, "PersonalChatCache.xml"));
                XmlNode cacheText = cacheXml.DocumentElement["CacheText"];

                this.cachedTextArray = cacheText.InnerText.Split(',');

                this.upArrowCounter++;

                try
                {
                    if (this.cachedTextArray.Length - this.upArrowCounter < 0)
                    {
                        this.upArrowCounter = 0;
                    }
                    else
                    {
                        this.txtCommands.Text = this.cachedTextArray[this.cachedTextArray.Length - this.upArrowCounter];
                    }
                }
                catch (Exception ex)
                {
                    if (Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Scriptor))
                    {
                        this.AppendChat("upArrowCounter Exception (Starting at line 195)", Color.Red);
                        this.AppendChat(ex.StackTrace, Color.Black);
                    }
                }
            }

            if (e.KeyData == Keys.Down)
            {
                XmlDocument cacheXml = new XmlDocument();
                cacheXml.Load(Path.Combine(Application.StartupPath, "PersonalChatCache.xml"));
                XmlNode cacheText = cacheXml.DocumentElement["CacheText"];

                this.cachedTextArray = cacheText.InnerText.Split(',');

                this.upArrowCounter--;

                try
                {
                    if (this.cachedTextArray.Length - this.upArrowCounter > this.cachedTextArray.Length - 1 || this.cachedTextArray.Length - this.upArrowCounter < 0)
                    {
                        this.upArrowCounter = this.cachedTextArray.Length - 1;
                    }
                    else
                    {
                        this.txtCommands.Text = this.cachedTextArray[this.cachedTextArray.Length - this.upArrowCounter];
                    }
                }
                catch (Exception ex)
                {
                    if (Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Scriptor))
                    {
                        this.AppendChat("dwnArrowCounter Exception (Starting at line 218)", Color.Red);
                        this.AppendChat(ex.StackTrace, Color.Black);
                    }
                }
            }
        }

        /*public void Created(int index)
        {
            moduleIndex = index;
        }

        public void SwitchOut()
        {

        }*/

        /*public int ModuleIndex
        {
            get { return moduleIndex; }
        }

        public string ModuleName
        {
            get { return "Chat"; }
        }

        public Panel ModulePanel
        {
            get { return this; }
        }


        public bool Enabled
        {
            get { return enabled; }
            set
            {
                enabled = value;
                if (EnabledChanged != null)
                    EnabledChanged(this, EventArgs.Empty);
            }
        }


        public event EventHandler EnabledChanged;


        public Enums.ExpKitModules ModuleID
        {
            get { return Enums.ExpKitModules.Chat; }
        }*/

        private void Label1_Click(object sender, EventArgs e)
        {
        }

        private void Title_MouseUp(object sender, MouseEventArgs e)
        {
            this.drag = false;
        }

        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            this.startPoint = e.Location;
            this.drag = true;
        }

        private void Title_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.drag)
            { // if we should be dragging it, we need to figure out some movement
                Point p1 = new Point(e.X, e.Y);
                Point p2 = this.PointToScreen(p1);
                Point p3 = new Point(
                    p2.X - this.startPoint.X,
                                     p2.Y - this.startPoint.Y);
                this.Location = p3;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}