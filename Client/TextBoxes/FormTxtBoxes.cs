﻿// <copyright file="FormTxtBoxes.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.TextBoxes
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using System.Windows.Forms;

    internal class FormTxtBoxes : Form
    {
        private readonly TextBox txtHouse1;
        private readonly TextBox txtHouse2;

        private readonly SdlDotNet.Widgets.Label referenceTile = Menus.MnuAddNotice.LblAddTile;

        public FormTxtBoxes() : base()
        {
            this.txtHouse1 = new TextBox();
            this.txtHouse1.Location = new Point(this.referenceTile.X, this.referenceTile.Y + this.referenceTile.Height + 10);
            this.txtHouse1.Size = new Size(this.Width - (this.referenceTile.X * 2), 16);
            this.txtHouse1.TextChanged += new EventHandler(this.TxtHouse_TextChanged);

            this.txtHouse2 = new TextBox();
            this.txtHouse2.Location = new Point(this.txtHouse1.Location.X, this.txtHouse1.Location.Y + this.txtHouse1.Height + 10);
            this.txtHouse2.Size = new Size(this.Width - (this.txtHouse1.Location.X * 2), 16);
            this.txtHouse2.TextChanged += new EventHandler(this.TxtHouse_TextChanged);
        }

        private void TxtHouse_TextChanged(object sender, EventArgs e)
        {
            Menus.MnuAddNotice.PriceText("Placing this tile will cost " + (((this.txtHouse1.Text.Length + this.txtHouse2.Text.Length) * Menus.MnuAddNotice.WordPrice) + Menus.MnuAddNotice.Price) + " " + Items.ItemHelper.Items[1].Name + ".");
        }
    }
}
