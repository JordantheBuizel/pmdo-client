﻿// <copyright file="SkinManager.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Skins
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Extensions;
    using PMU.Compression.Zip;
    using SdlDotNet.Graphics;
    using SdlDotNet.Widgets;

    internal class SkinManager
    {
        private static Skin activeSkin;
        private static Surface screenBackground;

        public static Surface ScreenBackground
        {
            get { return screenBackground; }
            set { screenBackground = value; }
        }

        public static Skin ActiveSkin
        {
            get { return activeSkin; }
        }

        public static void ChangeActiveSkin(string skinName)
        {
            if (activeSkin != null)
            {
                activeSkin.Unload();
            }

            activeSkin = new Skin();
            activeSkin.LoadSkin(skinName);
            IO.Options.ActiveSkin = skinName;
            if (screenBackground != null)
            {
                screenBackground.Close();
                screenBackground = null;
            }

            screenBackground = LoadGui("General/Background");
            if (screenBackground != null)
            {
                // lock (screenBackground) {
                //    screenBackground = screenBackground.CreateStretchedSurface(SdlDotNet.Graphics.Video.Screen.Size);
                // }
            }

            Widgets.ResourceDirectory = IO.Paths.SkinPath + ActiveSkin.Name + "/Widgets/";
        }

        public static void PlaySkinMusic()
        {
            // Music.Music.AudioPlayer.PlayMusic("Title.ogg");
            // string activeSkinMusicFile = Music.Music.AudioPlayer.FindMusicFile(Skins.SkinManager.GetActiveSkinFolder() + "Music/", "Title");
            // if (!string.IsNullOrEmpty(activeSkinMusicFile)) {
            //    Music.Music.AudioPlayer.PlayMusicDirect(activeSkinMusicFile, -1, false, true);
            // } else {
            //    Music.Music.AudioPlayer.PlayMusic("Title.ogg");
            // }
            string activeSkinMusicFile = Music.AudioHelper.FindMusicFile(GetActiveSkinFolder() + "Music/", "Title");
            if (!string.IsNullOrEmpty(activeSkinMusicFile))
            {
                Music.Music.AudioPlayer.PlayMusic(activeSkinMusicFile, -1, false, true);
            }
else
            {
                Music.Music.AudioPlayer.PlayMusic("Title.ogg");
            }
        }

        public static Surface LoadGui(string guiToLoad)
        {
            if (IO.IO.FileExists("Skins/" + ActiveSkin.Name + "/" + guiToLoad + "/gui.png"))
            {
                Surface surf = Graphic.SurfaceManager.LoadSurface("Skins/" + ActiveSkin.Name + "/" + guiToLoad + "/gui.png");
                surf.Transparent = true;
                return surf;
            }

            return null;
        }

        public static Surface LoadGuiElement(string guiToLoad, string elementName)
        {
            return LoadGuiElement(guiToLoad, elementName, true);
        }

        public static Surface LoadGuiElement(string guiToLoad, string elementName, bool convert)
        {
            if (IO.IO.FileExists("Skins/" + ActiveSkin.Name + "/" + guiToLoad + "/" + elementName))
            {
                Surface surf = Graphic.SurfaceManager.LoadSurface("Skins/" + ActiveSkin.Name + "/" + guiToLoad + "/" + elementName);
                if (convert)
                {
                    Surface surf2 = surf.Convert();
                    surf2.Transparent = true;
                    surf.Close();
                    return surf2;
                }
else
                {
                    return surf;
                }
            }

            return null;
        }

        public static void LoadButtonGui(Button button)
        {
            button.BackColor = Color.Transparent;
            button.BackgroundImageSizeMode = ImageSizeMode.StretchImage;
            button.BorderStyle = BorderStyle.None;
            button.BackgroundImage = LoadGuiElement("Game Window", "Widgets/button.png");
            button.HighlightType = HighlightType.Image;
            Surface unstretchedHoverImage = LoadGuiElement("Game Window", "Widgets/button-h.png");
            button.HighlightSurface = unstretchedHoverImage.CreateStretchedSurface(button.Size);
            unstretchedHoverImage.Close();
        }

        public static void LoadTextBoxGui(TextBox textBox)
        {
            textBox.BackColor = Color.Transparent;
            textBox.ForeColor = Color.WhiteSmoke;
            textBox.BackgroundImageSizeMode = ImageSizeMode.StretchImage;
            textBox.BorderStyle = BorderStyle.None;
            textBox.BackgroundImage = LoadGuiElement("Game Window", "Widgets/textbox.png");
        }

        public static string GetActiveSkinFolder()
        {
            return IO.Paths.SkinPath + ActiveSkin.Name + "/";
        }

        public static bool InstallSkin(string skinPackagePath)
        {
            try
            {
                using (ZipFile zip = new ZipFile(skinPackagePath))
                {
                    bool skinValid = false;
                    foreach (ZipEntry entry in zip.Entries)
                    {
                        if (entry.FileName == "Configuration/config.xml")
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                entry.Extract(ms);
                                ms.Seek(0, SeekOrigin.Begin);
                                skinValid = ValidateSkinConfigEntry(ms);
                            }

                            break;
                        }
                    }

                    if (skinValid)
                    {
                        string skinDir = IO.Paths.SkinPath + Path.GetFileNameWithoutExtension(skinPackagePath);
                        if (Directory.Exists(skinDir) == false)
                        {
                            Directory.CreateDirectory(skinDir);
                        }

                        zip.ExtractAll(skinDir, ExtractExistingFileAction.OverwriteSilently);
                        return true;
                    }
else
                    {
                        return false;
                    }
                }
            }
catch
            {
                return false;
            }
        }

        private static bool ValidateSkinConfigEntry(MemoryStream configStream)
        {
            string creator = null;
            string version = null;
            using (XmlReader reader = XmlReader.Create(configStream))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "Creator":
                            {
                                    creator = reader.ReadString();
                                }

                                break;
                            case "Version":
                            {
                                    version = reader.ReadString();
                                }

                                break;
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(creator) && !string.IsNullOrEmpty(version))
            {
                return true;
            }
else
            {
                return false;
            }
        }
    }
}