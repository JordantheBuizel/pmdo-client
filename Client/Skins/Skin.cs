﻿// <copyright file="Skin.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Skins
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml;
    using SdlDotNet.Graphics;

    internal class Skin
    {
        private string name;
        private string creator;
        private string version;
        private Surface ingameBackground;

        public Surface IngameBackground
        {
            get { return this.ingameBackground; }
        }

        public string Name
        {
            get { return this.name; }
        }

        public string Creator
        {
            get { return this.creator; }
        }

        public string Version
        {
            get { return this.version; }
        }

        public void LoadSkin(string name)
        {
            this.name = name;

            string configPath = IO.Paths.SkinPath + name + "/Configuration/";
            if (System.IO.Directory.Exists(configPath) == false)
            {
                System.IO.Directory.CreateDirectory(configPath);
            }

            this.LoadConfigXml(configPath + "config.xml");
        }

        private void LoadConfigXml(string fullPath)
        {
            if (!IO.IO.FileExists(fullPath))
            {
                this.SaveEmptyConfigFile(fullPath);
            }

            using (XmlReader reader = XmlReader.Create(fullPath))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "Creator":
                            {
                                    this.creator = reader.ReadString();
                                }

                                break;
                            case "Version":
                            {
                                    this.version = reader.ReadString();
                                }

                                break;
                        }
                    }
                }
            }
        }

        private void SaveEmptyConfigFile(string fullPath)
        {
            using (XmlWriter writer = XmlWriter.Create(fullPath))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("SkinConfiguration");

                writer.WriteStartElement("General");

                writer.WriteElementString("Creator", string.Empty);
                writer.WriteElementString("Version", string.Empty);

                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        public void Unload()
        {
            if (this.ingameBackground != null)
            {
                this.ingameBackground.Close();
            }
        }
    }
}
