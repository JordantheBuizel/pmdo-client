﻿// <copyright file="Program.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Threading;
    using System.Windows.Forms;
    using System.Xml;
    using PMU.Core;
    using static System.Environment;

    internal sealed class Program
    {
        private static bool hasUpdated = true;

        [STAThread]
        private static void Main(string[] args)
        {
            // System.Windows.Forms.Application.EnableVisualStyles();
            // System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
            if (File.Exists(Path.Combine(Application.StartupPath, "PersonalChatCache.xml")))
            {
                XmlDocument cacheXml = new XmlDocument();
                cacheXml.Load(Path.Combine(Application.StartupPath, "PersonalChatCache.xml"));
                XmlNode cacheText = cacheXml.DocumentElement["CacheText"];

                cacheText.InnerText = string.Empty;
                cacheXml.Save(Path.Combine(Application.StartupPath, "PersonalChatCache.xml"));
            }
            else
            {
                XmlTextWriter writer = new XmlTextWriter(Path.Combine(Application.StartupPath, "PersonalChatCache.xml"), System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 2;
                writer.WriteStartElement("PersonalChat");
                writer.WriteStartElement("CacheText");
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }

            /*if (!(File.Exists(Path.Combine(Application.StartupPath, "UpdateData.xml"))))
            {
                XmlTextWriter writer = new XmlTextWriter(Path.Combine(Application.StartupPath, "UpdateData.xml"), System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 2;
                writer.WriteStartElement("Update");
                writer.WriteStartElement("Revision");
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }

            XmlDocument settingsXml = new XmlDocument();
            settingsXml.Load(Path.Combine(Application.StartupPath, "UpdateData.xml"));
            XmlNode revision = settingsXml.DocumentElement["Revision"];

            revision.InnerText = Constants.REVISION_NUM.ToString();
            settingsXml.Save(Path.Combine(Application.StartupPath, "UpdateData.xml"));*/

            Loader.InitLoader(args);
            }
    }
}