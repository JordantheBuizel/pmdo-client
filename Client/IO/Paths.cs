﻿// <copyright file="Paths.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.IO
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    internal class Paths
    {
        /// <summary>
        /// Directory seperator character used by the OS.
        /// </summary>
        private static readonly char dirChar = Path.DirectorySeparatorChar;
        private static string fontPath;
        private static string gfxPath;
        private static string mapPath;
        private static string musicPath;
        private static string sfxPath;
        private static string skinPath;
        private static string startupPath;
        private static string storyDataPath;

        public static char DirChar
        {
            get { return dirChar; }
        }

        public static string FontPath
        {
            get { return fontPath; }
        }

        public static string StoryDataPath
        {
            get { return storyDataPath; }
        }

        public static string GfxPath
        {
            get { return gfxPath; }
        }

        public static string MapPath
        {
            get { return mapPath; }
        }

        public static string MusicPath
        {
            get { return musicPath; }
        }

        public static string SfxPath
        {
            get { return sfxPath; }
        }

        public static string SkinPath
        {
            get { return skinPath; }
        }

        public static string StartupPath
        {
            get { return startupPath; }
        }

        /// <summary>
        /// Creates a file path in the format used by the host OS.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>A file path in the format used by the host OS</returns>
        public static string CreateOSPath(string fileName)
        {
            if (Environment.OSVersion.Platform == PlatformID.Unix)
            {
                if (fileName.Contains("\\"))
                {
                    fileName = fileName.Replace('\\', dirChar);
                }
            }
            else if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                if (fileName.Contains("/"))
                {
                    fileName = fileName.Replace('/', dirChar);
                }
            }

            if (fileName.StartsWith(StartupPath) == false)
            {
                fileName = StartupPath + fileName;
            }

            return fileName;
        }

        /// <summary>
        /// Initializes this class
        /// </summary>
        public static void Initialize()
        {
            startupPath = System.Windows.Forms.Application.StartupPath;

            // #if DEBUG
            if (/*Globals.InDebugMode &&*/ Globals.CommandLine.ContainsCommandArg("-overridepath"))
            {
                int index = Globals.CommandLine.FindCommandArg("-overridepath");
                startupPath = Globals.CommandLine.CommandArgs[index + 1];
            }

            // #endif
            startupPath = Path.GetFullPath(startupPath);
            if (startupPath.EndsWith(dirChar.ToString()) == false)
            {
                startupPath += dirChar;
            }

            gfxPath = StartupPath + "GFX" + dirChar;
            skinPath = StartupPath + "Skins" + dirChar;
            fontPath = StartupPath + "Fonts" + dirChar;
            mapPath = StartupPath + "MapData" + dirChar;
            musicPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "PMDO", "Music") + dirChar;
            if (!Directory.Exists(musicPath))
            {
                Directory.CreateDirectory(musicPath);
            }

            sfxPath = StartupPath + "SFX" + dirChar;
            storyDataPath = StartupPath + "Story" + dirChar;
        }
    }
}