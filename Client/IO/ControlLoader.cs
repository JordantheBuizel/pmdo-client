﻿// <copyright file="ControlLoader.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.IO
{
    using System;
    using System.Xml;
    using PMU.Core;
    using SdlInput = SdlDotNet.Input;

    /// <summary>
    /// Description of ControlLoader.
    /// </summary>
    internal class ControlLoader
    {
        /// <summary>
        /// Gets or sets Accept key; Default => Enter
        /// </summary>
        public static SdlInput.Key AcceptKey
        {
            get;
            set;
        }

        public static SdlInput.Key AdventurePackKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Attack key; Default => F
        /// </summary>
        public static SdlInput.Key AttackKey
        {
            get;
            set;
        }

        public static SdlInput.Key BattleLogKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Down Movement key; Default => Down Arrow
        /// </summary>
        public static SdlInput.Key DownKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Unused?
        /// </summary>
        public static SdlInput.Key HealKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Not very specific
        /// </summary>
        public static SdlInput.Key LeaderSwitchKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Left Movement key; Default => Left Arrow
        /// </summary>
        public static SdlInput.Key LeftKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Player Turn key; Default => Home
        /// </summary>
        public static SdlInput.Key TurnKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Default => W
        /// </summary>
        public static SdlInput.Key Move1Key
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Default => A
        /// </summary>
        public static SdlInput.Key Move2Key
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Default => S
        /// </summary>
        public static SdlInput.Key Move3Key
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Default => D
        /// </summary>
        public static SdlInput.Key Move4Key
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Online List key; Default => F9
        /// </summary>
        public static SdlInput.Key OnlineListKey
        {
            get;
            set;
        }

        public static SdlInput.Key RefreshKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Move Right => D
        /// </summary>
        public static SdlInput.Key RightKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RunKey Default => Shift
        /// </summary>
        public static SdlInput.Key RunKey
        {
            get;
            set;
        }

        public static SdlInput.Key ScreenshotKey
        {
            get;
            set;
        }

        /*public static SdlInput.Key Slot1SwitchKey {
            get;
            set;
        }

        public static SdlInput.Key Slot2SwitchKey {
            get;
            set;
        }

        public static SdlInput.Key Slot3SwitchKey {
            get;
            set;
        }
        */

        /// <summary>
        /// Gets or sets ???
        /// </summary>
        public static SdlInput.Key SwitchModeKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ???
        /// </summary>
        public static SdlInput.Key TargetSelectAcceptKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ???
        /// </summary>
        public static SdlInput.Key TargetSelectCancelKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ???
        /// </summary>
        public static SdlInput.Key TargetSelectLeftKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ???
        /// </summary>
        public static SdlInput.Key TargetSelectRightKey
        {
            get;
            set;
        }

        public static SdlInput.Key Team1ItemKey
        {
            get;
            set;
        }

        public static SdlInput.Key Team2ItemKey
        {
            get;
            set;
        }

        public static SdlInput.Key Team3ItemKey
        {
            get;
            set;
        }

        public static SdlInput.Key Team4ItemKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Move Up; Default => Up Arrow
        /// </summary>
        public static SdlInput.Key UpKey
        {
            get;
            set;
        }

        public static void InitDefaultControls()
        {
            UpKey = SdlInput.Key.UpArrow;
            DownKey = SdlInput.Key.DownArrow;
            LeftKey = SdlInput.Key.LeftArrow;
            RightKey = SdlInput.Key.RightArrow;
            TurnKey = SdlInput.Key.Home;
            RunKey = SdlInput.Key.LeftShift;
            AttackKey = SdlInput.Key.F;
            Move1Key = SdlInput.Key.W;
            Move2Key = SdlInput.Key.A;
            Move3Key = SdlInput.Key.S;
            Move4Key = SdlInput.Key.D;
            AcceptKey = SdlInput.Key.Return;
            Team1ItemKey = SdlInput.Key.Z;
            Team2ItemKey = SdlInput.Key.X;
            Team3ItemKey = SdlInput.Key.C;
            Team4ItemKey = SdlInput.Key.V;
            ScreenshotKey = SdlInput.Key.F11;
            OnlineListKey = SdlInput.Key.F9;
            BattleLogKey = SdlInput.Key.F10;
            RefreshKey = SdlInput.Key.End;
            AdventurePackKey = SdlInput.Key.P;
        }

        public static void LoadControls()
        {
            InitDefaultControls();
            if (IO.FileExists("controls.xml") == false)
            {
                SaveControls();
            }

            using (XmlReader reader = XmlReader.Create(Paths.StartupPath + "controls.xml"))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "Up":
                                {
                                    UpKey = (SdlInput.Key)reader.ReadString().ToInt((int)SdlInput.Key.UpArrow);
                                }

                                break;
                            case "Down":
                                {
                                    DownKey = (SdlInput.Key)reader.ReadString().ToInt((int)SdlInput.Key.DownArrow);
                                }

                                break;
                            case "Left":
                                {
                                    LeftKey = (SdlInput.Key)reader.ReadString().ToInt((int)SdlInput.Key.LeftArrow);
                                }

                                break;
                            case "Right":
                                {
                                    RightKey = (SdlInput.Key)reader.ReadString().ToInt((int)SdlInput.Key.RightArrow);
                                }

                                break;
                            case "Turn":
                                {
                                    TurnKey = (SdlInput.Key)reader.ReadString().ToInt((int)SdlInput.Key.Home);
                                }

                                break;
                            case "Run":
                                {
                                    RunKey = (SdlInput.Key)reader.ReadString().ToInt((int)SdlInput.Key.LeftShift);
                                }

                                break;

                                // case "StandardAttack": {
                                //        AttackKey = (SdlInput.Key)reader.ReadString().ToInt((int)SdlInput.Key.Z);
                                //    }
                                //    break;
                        }
                    }
                }
            }
        }

        public static void SaveControls()
        {
            using (XmlWriter writer = XmlWriter.Create(Paths.StartupPath + "controls.xml", Options.XmlWriterSettings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Controls");

                writer.WriteStartElement("Movement");

                writer.WriteElementString("Up", ((int)UpKey).ToString());
                writer.WriteElementString("Down", ((int)DownKey).ToString());
                writer.WriteElementString("Left", ((int)LeftKey).ToString());
                writer.WriteElementString("Right", ((int)RightKey).ToString());
                writer.WriteElementString("Turn", ((int)TurnKey).ToString());
                writer.WriteElementString("Run", ((int)RunKey).ToString());

                writer.WriteEndElement();
                writer.WriteStartElement("Attacking");

                writer.WriteElementString("StandardAttack", ((int)AttackKey).ToString());

                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
    }
}
