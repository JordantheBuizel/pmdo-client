﻿// <copyright file="ItemCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Items
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class ItemCollection
    {
        private readonly PMU.Core.ListPair<int, Item> mItems;

        internal ItemCollection(int maxItems)
        {
            this.mItems = new PMU.Core.ListPair<int, Item>();
        }

        public Item this[int index]
        {
            get { return this.mItems[index]; }

            set
            {
                this.mItems[index] = value;
            }
        }

        public void AddItem(int index, Item value)
        {
            this.mItems.Add(index, value);
        }
    }
}