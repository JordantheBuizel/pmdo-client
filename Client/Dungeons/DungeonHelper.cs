﻿// <copyright file="DungeonHelper.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Dungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;

    internal class DungeonHelper
    {
        private static int dataLoadPercent = 0;
        private static DungeonCollection mDungeons;

        public static int DataLoadPercent
        {
            get { return dataLoadPercent; }
        }

        public static DungeonCollection Dungeons
        {
            get { return mDungeons; }
        }

        public static void InitDungeonCollection()
        {
            mDungeons = new DungeonCollection();
        }

        public static void LoadDungeonsFromPacket(string[] parse)
        {
            try
            {
                int n = 2;
                MaxInfo.MaxDungeons = parse[1].ToInt();
                mDungeons.ClearDungeons();
                if (MaxInfo.MaxDungeons > 0)
                {
                    for (int i = 0; i < MaxInfo.MaxDungeons; i++)
                    {
                        dataLoadPercent = Math.Min(99, Logic.MathFunctions.CalculatePercent(i, MaxInfo.MaxDungeons));
                        mDungeons.AddDungeon(i, new Dungeon());
                        mDungeons[i].Name = parse[n];
                        n += 1;
                    }

                    dataLoadPercent = 100;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionHandler.OnException(ex);
            }
        }
    }
}
