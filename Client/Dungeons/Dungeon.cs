﻿// <copyright file="Dungeon.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Dungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Dungeon
    {
        public string Name { get; set; }
    }
}
