﻿// <copyright file="DungeonCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Dungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class DungeonCollection
    {
        private readonly PMU.Core.ListPair<int, Dungeon> mDungeons;

        internal DungeonCollection()
        {
            this.mDungeons = new PMU.Core.ListPair<int, Dungeon>();
        }

        public Dungeon this[int index]
        {
            get { return this.mDungeons[index]; }
            set { this.mDungeons[index] = value; }
        }

        public void AddDungeon(int index, Dungeon RDungeonToAdd)
        {
            this.mDungeons.Add(index, RDungeonToAdd);
        }

        public void ClearDungeons()
        {
            this.mDungeons.Clear();
        }
    }
}
