﻿// <copyright file="ShortcutBarBuilder.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Graphic;
    using Network;
    using Stories;
    using Stories.Segments;

    /// <summary>
    /// Class to add buttons to a ShortcutBar
    /// </summary>
    internal class ShortcutBarBuilder
    {
        /// <summary>
        /// Adds the 'Show Menu' button to the specified ShortcutBar
        /// </summary>
        /// <param name="shortcutBar">The ShortcutBar to add the button to</param>
        public static void AddShowMenuButtonToBar(Widges.ShortcutBar shortcutBar)
        {
            Widges.ShortcutButton button = new Widges.ShortcutButton("menuButton");
            button.BackColor = Color.Transparent;
            button.Image = Skins.SkinManager.LoadGuiElement("Game Window/ShortcutBar", "showmenu.png");
            button.HighlightImage = Skins.SkinManager.LoadGuiElement("Game Window/ShortcutBar", "showmenu-h.png");
            button.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(MenuButton_Click);
            shortcutBar.AddButton(button);
        }

        private static void MenuButton_Click(object sender, SdlDotNet.Widgets.MouseButtonEventArgs e)
        {
            if (Menus.MenuSwitcher.CanShowMenu())
            {
                Menus.MenuSwitcher.ShowMenu(new Menus.MnuOptions("mnuMainMenu"));
                Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");

                // if (!Windows.WindowSwitcher.GameWindow.MenuManager.Visible) {
                //    Windows.WindowSwitcher.GameWindow.MenuManager.Visible = true;
                //    Windows.WindowSwitcher.GameWindow.MenuManager.Focus();
                //    Menus.MenuSwitcher.ShowMainMenu();
                // } else {
                //    Windows.WindowSwitcher.GameWindow.MapViewer.Focus();
                //    Windows.WindowSwitcher.GameWindow.MenuManager.Visible = false;
                //    Windows.WindowSwitcher.GameWindow.MenuManager.CloseOpenMenus();
                // }
            }
        }

        /// <summary>
        /// Adds the 'Show Inventory' button to the specified ShortcutBar
        /// </summary>
        /// <param name="shortcutBar">The ShortcutBar to add the button to</param>
        public static void AddShowInvButtonToBar(Widges.ShortcutBar shortcutBar)
        {
            Widges.ShortcutButton button = new Widges.ShortcutButton("invButton");
            button.BackColor = Color.Transparent;
            button.Image = Skins.SkinManager.LoadGuiElement("Game Window/ShortcutBar", "showinv.png");
            button.HighlightImage = Skins.SkinManager.LoadGuiElement("Game Window/ShortcutBar", "showinv-h.png");
            button.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(InvButton_Click);
            shortcutBar.AddButton(button);
        }

        private static void InvButton_Click(object sender, SdlDotNet.Widgets.MouseButtonEventArgs e)
        {
            Menus.MenuSwitcher.ShowInventoryMenu(1);
            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
        }

        /// <summary>
        /// Adds the 'Show Moves' button to the specified ShortcutBar
        /// </summary>
        /// <param name="shortcutBar">The ShortcutBar to add the button to</param>
        public static void AddShowMovesButtonToBar(Widges.ShortcutBar shortcutBar)
        {
            Widges.ShortcutButton button = new Widges.ShortcutButton("showMovesButton");
            button.BackColor = Color.Transparent;
            button.Image = Skins.SkinManager.LoadGuiElement("Game Window/ShortcutBar", "showmoves.png");
            button.HighlightImage = Skins.SkinManager.LoadGuiElement("Game Window/ShortcutBar", "showmoves-h.png");
            button.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(ShowMovesButton_Click);
            shortcutBar.AddButton(button);
        }

        private static void ShowMovesButton_Click(object sender, SdlDotNet.Widgets.MouseButtonEventArgs e)
        {
            Menus.MenuSwitcher.ShowMovesMenu();
            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
        }

        /// <summary>
        /// Adds the 'Use Recovery Item' button to the specified ShortcutBar
        /// </summary>
        /// <param name="shortcutBar">The ShortcutBar to add the button to</param>
        public static void AddUseRecoveryItemButtonToBar(Widges.ShortcutBar shortcutBar)
        {
            Widges.ShortcutButton button = new Widges.ShortcutButton("useRecoveryItemButton");
            button.BackColor = Color.Transparent;
            button.Image = Skins.SkinManager.LoadGuiElement("Game Window/ShortcutBar", "userecoveryitem.png");
            button.HighlightImage = Skins.SkinManager.LoadGuiElement("Game Window/ShortcutBar", "userecoveryitem-h.png");
            button.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(UseRecoveryItemButton_Click);
            shortcutBar.AddButton(button);
        }

        private static void UseRecoveryItemButton_Click(object sender, SdlDotNet.Widgets.MouseButtonEventArgs e)
        {
            Players.Inventory inv = Players.PlayerManager.MyPlayer.Inventory;

            // for (int i = 1; i <= inv.Length; i++) {
                // if (inv[i].Num > 0) {
                    // if (Items.ItemHelper.Items[inv[i].Num].Type == Enums.ItemType.PotionAddHP) {
                        // ExpKit.Modules.kitChat chat = (ExpKit.Modules.kitChat)Windows.WindowSwitcher.ExpKit.KitContainer.ModuleSwitcher.FindKitModule(Enums.ExpKitModules.Chat);
                        // if (chat != null) {
                        //    chat.AppendChat("You have used a " + Items.ItemHelper.Items[inv[i].Num].Name + "!", Color.Yellow);
                        // }
                        // Messenger.SendUseItem(i);
                        // break;
                    // }
                // }
            // }
        }

        /// <summary>
        /// Adds the 'Show Online List' button to the specified ShortcutBar
        /// </summary>
        /// <param name="shortcutBar">The ShortcutBar to add the button to</param>
        public static void AddShowOnlineListButtonToBar(Widges.ShortcutBar shortcutBar)
        {
            Widges.ShortcutButton button = new Widges.ShortcutButton("showOnlineListButton");
            button.BackColor = Color.Transparent;
            button.Image = Skins.SkinManager.LoadGuiElement("Game Window/ShortcutBar", "showonline.png");
            button.HighlightImage = Skins.SkinManager.LoadGuiElement("Game Window/ShortcutBar", "showonline-h.png");
            button.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(ShowOnlineListButton_Click);
            shortcutBar.AddButton(button);
        }

        private static void ShowOnlineListButton_Click(object sender, SdlDotNet.Widgets.MouseButtonEventArgs e)
        {
            Menus.MenuSwitcher.ShowMenu(new Menus.MnuOnlineList("mnuOnlineList"));
            Messenger.SendOnlineListRequest();
        }

        /// <summary>
        /// Adds the 'Show Options' button to the specified ShortcutBar
        /// </summary>
        /// <param name="shortcutBar">The ShortcutBar to add the button to</param>
        public static void AddShowOptionsButtonToBar(Widges.ShortcutBar shortcutBar)
        {
            Widges.ShortcutButton button = new Widges.ShortcutButton("showOptionsButton");
            button.BackColor = Color.Transparent;
            button.Image = Skins.SkinManager.LoadGuiElement("Game Window/ShortcutBar", "showoptions.png");
            button.HighlightImage = Skins.SkinManager.LoadGuiElement("Game Window/ShortcutBar", "showoptions-h.png");
            button.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(ShowOptionsButton_Click);
            shortcutBar.AddButton(button);
        }

        private static void ShowOptionsButton_Click(object sender, SdlDotNet.Widgets.MouseButtonEventArgs e)
        {
            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(RunOptions));
            t.SetApartmentState(System.Threading.ApartmentState.STA); // Set the thread to STA
            t.Start();
        }

        private static void RunOptions()
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.Run(new Windows.WinOptions());
        }

        /// <summary>
        /// Adds all buttons to the shortcut bar
        /// </summary>
        /// <param name="shortcutBar">The ShortcutBar to add the buttons to</param>
        public static void AssembleShortcutBarButtons(Widges.ShortcutBar shortcutBar)
        {
            // AddShowMenuButtonToBar(shortcutBar);
            AddShowInvButtonToBar(shortcutBar);
            AddShowMovesButtonToBar(shortcutBar);

            // AddUseRecoveryItemButtonToBar(shortcutBar);
            AddShowOnlineListButtonToBar(shortcutBar);
            AddShowOptionsButtonToBar(shortcutBar);
        }
    }
}
