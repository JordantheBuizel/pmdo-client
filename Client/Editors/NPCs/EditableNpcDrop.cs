﻿// <copyright file="EditableNpcDrop.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.NPCs
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class EditableNpcDrop
    {
        public int Chance
        {
            get;
            set;
        }

        public int ItemNum
        {
            get;
            set;
        }

        public int ItemValue
        {
            get;
            set;
        }

        public string Tag
        {
            get;
            set;
        }
    }
}
