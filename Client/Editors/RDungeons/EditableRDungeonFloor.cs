﻿// <copyright file="EditableRDungeonFloor.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.RDungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Maps;

    /// <summary>
    /// Description of EditableRDungeonFloor.
    /// </summary>
    internal class EditableRDungeonFloor
    {
        public int TrapMin{get; set; }

        public int TrapMax{get; set; }

        public int ItemMin{get; set; }

        public int ItemMax{get; set; }

        public int Intricacy { get; set; }

        public int RoomWidthMin{get; set; }

        public int RoomWidthMax{get; set; }

        public int RoomLengthMin{get; set; }

        public int RoomLengthMax{get; set; }

        public int HallTurnMin{get; set; }

        public int HallTurnMax{get; set; }

        public int HallVarMin {get; set; }

        public int HallVarMax {get; set; }

        public int WaterFrequency {get; set; }

        public int Craters {get; set; }

        public int CraterMinLength {get; set; }

        public int CraterMaxLength {get; set; }

        public bool CraterFuzzy { get; set; }

        public int MinChambers { get; set; }

        public int MaxChambers { get; set; }

        public List<EditableRDungeonChamber> Chambers { get; set; }

        public int Darkness { get; set; }

        public Enums.RFloorGoalType GoalType { get; set; }

        public int GoalMap { get; set; }

        public int GoalX { get; set; }

        public int GoalY { get; set; }

        public string Music { get; set; }

        public int StairsX { get; set; }

        public int StairsSheet { get; set; }

        public int MGroundX { get; set; }

        public int MGroundSheet { get; set; }

        public int MTopLeftX { get; set; }

        public int MTopLeftSheet { get; set; }

        public int MTopCenterX { get; set; }

        public int MTopCenterSheet { get; set; }

        public int MTopRightX { get; set; }

        public int MTopRightSheet { get; set; }

        public int MCenterLeftX { get; set; }

        public int MCenterLeftSheet { get; set; }

        public int MCenterCenterX { get; set; }

        public int MCenterCenterSheet { get; set; }

        public int MCenterRightX { get; set; }

        public int MCenterRightSheet { get; set; }

        public int MBottomLeftX { get; set; }

        public int MBottomLeftSheet { get; set; }

        public int MBottomCenterX { get; set; }

        public int MBottomCenterSheet { get; set; }

        public int MBottomRightX { get; set; }

        public int MBottomRightSheet { get; set; }

        public int MInnerTopLeftX { get; set; }

        public int MInnerTopLeftSheet { get; set; }

        public int MInnerBottomLeftX { get; set; }

        public int MInnerBottomLeftSheet { get; set; }

        public int MInnerTopRightX { get; set; }

        public int MInnerTopRightSheet { get; set; }

        public int MInnerBottomRightX { get; set; }

        public int MInnerBottomRightSheet { get; set; }

        public int MIsolatedWallX { get; set; }

        public int MIsolatedWallSheet { get; set; }

        public int MColumnTopX { get; set; }

        public int MColumnTopSheet { get; set; }

        public int MColumnCenterX { get; set; }

        public int MColumnCenterSheet { get; set; }

        public int MColumnBottomX { get; set; }

        public int MColumnBottomSheet { get; set; }

        public int MRowLeftX { get; set; }

        public int MRowLeftSheet { get; set; }

        public int MRowCenterX { get; set; }

        public int MRowCenterSheet { get; set; }

        public int MRowRightX { get; set; }

        public int MRowRightSheet { get; set; }

        public int MGroundAltX { get; set; }

        public int MGroundAltSheet { get; set; }

        public int MGroundAlt2X { get; set; }

        public int MGroundAlt2Sheet { get; set; }

        public int MTopLeftAltX { get; set; }

        public int MTopLeftAltSheet { get; set; }

        public int MTopCenterAltX { get; set; }

        public int MTopCenterAltSheet { get; set; }

        public int MTopRightAltX { get; set; }

        public int MTopRightAltSheet { get; set; }

        public int MCenterLeftAltX { get; set; }

        public int MCenterLeftAltSheet { get; set; }

        public int MCenterCenterAltX { get; set; }

        public int MCenterCenterAltSheet { get; set; }

        public int MCenterCenterAlt2X { get; set; }

        public int MCenterCenterAlt2Sheet { get; set; }

        public int MCenterRightAltX { get; set; }

        public int MCenterRightAltSheet { get; set; }

        public int MBottomLeftAltX { get; set; }

        public int MBottomLeftAltSheet { get; set; }

        public int MBottomCenterAltX { get; set; }

        public int MBottomCenterAltSheet { get; set; }

        public int MBottomRightAltX { get; set; }

        public int MBottomRightAltSheet { get; set; }

        public int MInnerTopLeftAltX { get; set; }

        public int MInnerTopLeftAltSheet { get; set; }

        public int MInnerBottomLeftAltX { get; set; }

        public int MInnerBottomLeftAltSheet { get; set; }

        public int MInnerTopRightAltX { get; set; }

        public int MInnerTopRightAltSheet { get; set; }

        public int MInnerBottomRightAltX { get; set; }

        public int MInnerBottomRightAltSheet { get; set; }

        public int MIsolatedWallAltX { get; set; }

        public int MIsolatedWallAltSheet { get; set; }

        public int MColumnTopAltX { get; set; }

        public int MColumnTopAltSheet { get; set; }

        public int MColumnCenterAltX { get; set; }

        public int MColumnCenterAltSheet { get; set; }

        public int MColumnBottomAltX { get; set; }

        public int MColumnBottomAltSheet { get; set; }

        public int MRowLeftAltX { get; set; }

        public int MRowLeftAltSheet { get; set; }

        public int MRowCenterAltX { get; set; }

        public int MRowCenterAltSheet { get; set; }

        public int MRowRightAltX { get; set; }

        public int MRowRightAltSheet { get; set; }

        public int MWaterX { get; set; }

        public int MWaterSheet { get; set; }

        public int MWaterAnimX { get; set; }

        public int MWaterAnimSheet { get; set; }

        public int MShoreTopLeftX { get; set; }

        public int MShoreTopLeftSheet { get; set; }

        public int MShoreTopRightX { get; set; }

        public int MShoreTopRightSheet { get; set; }

        public int MShoreBottomRightX { get; set; }

        public int MShoreBottomRightSheet { get; set; }

        public int MShoreBottomLeftX { get; set; }

        public int MShoreBottomLeftSheet { get; set; }

        public int MShoreDiagonalForwardX { get; set; }

        public int MShoreDiagonalForwardSheet { get; set; }

        public int MShoreDiagonalBackX { get; set; }

        public int MShoreDiagonalBackSheet { get; set; }

        public int MShoreTopX { get; set; }

        public int MShoreTopSheet { get; set; }

        public int MShoreRightX { get; set; }

        public int MShoreRightSheet { get; set; }

        public int MShoreBottomX { get; set; }

        public int MShoreBottomSheet { get; set; }

        public int MShoreLeftX { get; set; }

        public int MShoreLeftSheet { get; set; }

        public int MShoreVerticalX { get; set; }

        public int MShoreVerticalSheet { get; set; }

        public int MShoreHorizontalX { get; set; }

        public int MShoreHorizontalSheet { get; set; }

        public int MShoreInnerTopLeftX { get; set; }

        public int MShoreInnerTopLeftSheet { get; set; }

        public int MShoreInnerTopRightX { get; set; }

        public int MShoreInnerTopRightSheet { get; set; }

        public int MShoreInnerBottomRightX { get; set; }

        public int MShoreInnerBottomRightSheet { get; set; }

        public int MShoreInnerBottomLeftX { get; set; }

        public int MShoreInnerBottomLeftSheet { get; set; }

        public int MShoreInnerTopX { get; set; }

        public int MShoreInnerTopSheet { get; set; }

        public int MShoreInnerRightX { get; set; }

        public int MShoreInnerRightSheet { get; set; }

        public int MShoreInnerBottomX { get; set; }

        public int MShoreInnerBottomSheet { get; set; }

        public int MShoreInnerLeftX { get; set; }

        public int MShoreInnerLeftSheet { get; set; }

        public int MShoreSurroundedX { get; set; }

        public int MShoreSurroundedSheet { get; set; }

        public int MShoreTopLeftAnimX { get; set; }

        public int MShoreTopLeftAnimSheet { get; set; }

        public int MShoreTopRightAnimX { get; set; }

        public int MShoreTopRightAnimSheet { get; set; }

        public int MShoreBottomRightAnimX { get; set; }

        public int MShoreBottomRightAnimSheet { get; set; }

        public int MShoreBottomLeftAnimX { get; set; }

        public int MShoreBottomLeftAnimSheet { get; set; }

        public int MShoreDiagonalForwardAnimX { get; set; }

        public int MShoreDiagonalForwardAnimSheet { get; set; }

        public int MShoreDiagonalBackAnimX { get; set; }

        public int MShoreDiagonalBackAnimSheet { get; set; }

        public int MShoreTopAnimX { get; set; }

        public int MShoreTopAnimSheet { get; set; }

        public int MShoreRightAnimX { get; set; }

        public int MShoreRightAnimSheet { get; set; }

        public int MShoreBottomAnimX { get; set; }

        public int MShoreBottomAnimSheet { get; set; }

        public int MShoreLeftAnimX { get; set; }

        public int MShoreLeftAnimSheet { get; set; }

        public int MShoreVerticalAnimX { get; set; }

        public int MShoreVerticalAnimSheet { get; set; }

        public int MShoreHorizontalAnimX { get; set; }

        public int MShoreHorizontalAnimSheet { get; set; }

        public int MShoreInnerTopLeftAnimX { get; set; }

        public int MShoreInnerTopLeftAnimSheet { get; set; }

        public int MShoreInnerTopRightAnimX { get; set; }

        public int MShoreInnerTopRightAnimSheet { get; set; }

        public int MShoreInnerBottomRightAnimX { get; set; }

        public int MShoreInnerBottomRightAnimSheet { get; set; }

        public int MShoreInnerBottomLeftAnimX { get; set; }

        public int MShoreInnerBottomLeftAnimSheet { get; set; }

        public int MShoreInnerTopAnimX { get; set; }

        public int MShoreInnerTopAnimSheet { get; set; }

        public int MShoreInnerRightAnimX { get; set; }

        public int MShoreInnerRightAnimSheet { get; set; }

        public int MShoreInnerBottomAnimX { get; set; }

        public int MShoreInnerBottomAnimSheet { get; set; }

        public int MShoreInnerLeftAnimX { get; set; }

        public int MShoreInnerLeftAnimSheet { get; set; }

        public int MShoreSurroundedAnimX { get; set; }

        public int MShoreSurroundedAnimSheet { get; set; }

        public Tile GroundTile { get; set; }

        public Tile HallTile { get; set; }

        public Tile WaterTile { get; set; }

        public Tile WallTile { get; set; }

        public int NpcSpawnTime { get; set; }

        public int NpcMin { get; set; }

        public int NpcMax { get; set; }

        public List<EditableRDungeonItem> Items { get; set; }

        public List <MapNpcSettings> Npcs { get; set; }

        public List<EditableRDungeonTrap> SpecialTiles { get; set; }

        public List <Enums.Weather> Weather { get; set; }

        public EditableRDungeonFloor()
        {
            this.GroundTile = new Tile();
            this.HallTile = new Tile();
            this.WaterTile = new Tile();
            this.WallTile = new Tile();
            this.SpecialTiles = new List<EditableRDungeonTrap>();
            this.Weather = new List<Enums.Weather>();
            this.Npcs = new List<MapNpcSettings>();
            this.Items = new List<EditableRDungeonItem>();
            this.Chambers = new List<EditableRDungeonChamber>();
        }
    }
}
