﻿// <copyright file="EditableRDungeonItem.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.RDungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Maps;

    internal class EditableRDungeonItem
    {
        public int ItemNum { get; set; }

        public int MinAmount { get; set; }

        public int MaxAmount { get; set; }

        public int AppearanceRate { get; set; }

        public int StickyRate { get; set; }

        public string Tag { get; set; }

        public bool Hidden { get; set; }

        public bool OnGround { get; set; }

        public bool OnWater { get; set; }

        public bool OnWall { get; set; }

        public EditableRDungeonItem()
        {
            this.Tag = string.Empty;
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (!(obj is EditableRDungeonItem))
            {
                return false;
            }

            EditableRDungeonItem item = obj as EditableRDungeonItem;

            if (this.ItemNum != item.ItemNum)
            {
                return false;
            }

            if (this.MinAmount != item.MinAmount)
            {
                return false;
            }

            if (this.MaxAmount != item.MaxAmount)
            {
                return false;
            }

            if (this.AppearanceRate != item.AppearanceRate)
            {
                return false;
            }

            if (this.StickyRate != item.StickyRate)
            {
                return false;
            }

            if (this.Tag != item.Tag)
            {
                return false;
            }

            if (this.Hidden != item.Hidden)
            {
                return false;
            }

            if (this.OnGround != item.OnGround)
            {
                return false;
            }

            if (this.OnWater != item.OnWater)
            {
                return false;
            }

            if (this.OnWall != item.OnWall)
            {
                return false;
            }

            return true;
        }
    }
}
