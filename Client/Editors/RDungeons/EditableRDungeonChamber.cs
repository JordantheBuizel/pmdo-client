﻿// <copyright file="EditableRDungeonChamber.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.RDungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Maps;

    public class EditableRDungeonChamber
    {
        public int ChamberNum { get; set; }

        public string String1 { get; set; }

        public string String2 { get; set; }

        public string String3 { get; set; }
    }
}
