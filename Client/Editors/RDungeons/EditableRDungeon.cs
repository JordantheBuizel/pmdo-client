﻿// <copyright file="EditableRDungeon.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.RDungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Description of EditableRDungeon.
    /// </summary>
    internal class EditableRDungeon
    {
        public string DungeonName { get; set; }

        public Enums.Direction Direction { get; set; }

        public int MaxFloors { get; set; }

        public bool Recruitment { get; set; }

        public bool Exp { get; set; }

        public int WindTimer { get; set; }

        public int DungeonIndex { get; set; }

        public List<EditableRDungeonFloor> Floors { get; set; }

        public int RDungeonIndex;

        public EditableRDungeon(int rDungeonIndex)
        {
            this.RDungeonIndex = rDungeonIndex;
            this.DungeonName = string.Empty;
            this.Floors = new List<EditableRDungeonFloor>();
        }
    }
}
