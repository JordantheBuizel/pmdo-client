﻿// <copyright file="EditableRDungeonTrap.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.RDungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Maps;

    public class EditableRDungeonTrap
    {
        public Tile SpecialTile { get; set; }

        public int AppearanceRate { get; set; }

        public EditableRDungeonTrap()
        {
            this.SpecialTile = new Tile();
        }
    }
}
