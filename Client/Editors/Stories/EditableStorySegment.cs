﻿// <copyright file="EditableStorySegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.Stories
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;

    internal class EditableStorySegment
    {
        private readonly ListPair<string, string> parameters;

        public EditableStorySegment()
        {
            this.parameters = new ListPair<string, string>();
        }

        public Enums.StoryAction Action
        {
            get;
            set;
        }

        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        public void AddParameter(string paramID, string value)
        {
            this.parameters.Add(paramID, value);
        }
    }
}
