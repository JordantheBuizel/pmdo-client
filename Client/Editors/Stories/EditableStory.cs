﻿// <copyright file="EditableStory.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.Stories
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class EditableStory
    {
        public List<int> ExitAndContinue;
        private List<EditableStorySegment> segments;

        public EditableStory()
        {
            this.ExitAndContinue = new List<int>();
            this.segments = new List<EditableStorySegment>();
        }

        public string Name
        {
            get;
            set;
        }

        public List<EditableStorySegment> Segments
        {
            get { return this.segments; }
            set { this.segments = value; }
        }

        public int StoryStart
        {
            get;
            set;
        }
    }
}
