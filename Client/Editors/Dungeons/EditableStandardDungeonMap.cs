﻿// <copyright file="EditableStandardDungeonMap.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.Dungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class EditableStandardDungeonMap
    {
        public int MapNum {get; set; }

        public Enums.JobDifficulty Difficulty {get; set; }

        public bool IsBadGoalMap {get; set; }
    }
}
