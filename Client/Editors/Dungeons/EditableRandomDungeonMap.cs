﻿// <copyright file="EditableRandomDungeonMap.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.Dungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class EditableRandomDungeonMap
    {
        public int RDungeonIndex { get; set; }

        public int RDungeonFloor { get; set; }

        public Enums.JobDifficulty Difficulty { get; set; }

        public bool IsBadGoalMap { get; set; }
    }
}
