﻿// <copyright file="EditableDungeon.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.Dungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;

    internal class EditableDungeon
    {
        public string Name { get; set; }

        public bool AllowsRescue { get; set; }

        public List<EditableStandardDungeonMap> StandardMaps { get; set; }

        public List<EditableRandomDungeonMap> RandomMaps { get; set; }

        public ListPair<int, string> ScriptList { get; set; }

        public EditableDungeon()
        {
            this.StandardMaps = new List<EditableStandardDungeonMap>();
            this.RandomMaps = new List<EditableRandomDungeonMap>();
            this.ScriptList = new ListPair<int, string>();
        }
    }
}
