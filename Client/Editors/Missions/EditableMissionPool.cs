﻿// <copyright file="EditableMissionPool.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.Missions
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;

    internal class EditableMissionPool
    {
        public List<EditableMissionReward> Rewards { get; set; }

        public List<int> Enemies { get; set; }

        public List<EditableMissionClient> Clients { get; set; }

        public EditableMissionPool()
        {
            this.Rewards = new List<EditableMissionReward>();
            this.Clients = new List<EditableMissionClient>();
            this.Enemies = new List<int>();
        }
    }
}
