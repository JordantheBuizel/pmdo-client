﻿// <copyright file="EditableMissionClient.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.Missions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class EditableMissionClient
    {
        public int DexNum { get; set; }

        public int FormNum { get; set; }
    }
}
