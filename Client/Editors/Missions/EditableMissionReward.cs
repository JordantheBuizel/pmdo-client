﻿// <copyright file="EditableMissionReward.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Editors.Missions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class EditableMissionReward
    {
        public int ItemNum { get; set; }

        public int ItemAmount { get; set; }

        public string ItemTag { get; set; }
    }
}
