﻿// <copyright file="CustomMenu.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.CustomMenus
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Menus.Core;

    internal class CustomMenu : Widges.BorderedPanel, IMenu
    {
        /// <inheritdoc/>
        public Widges.BorderedPanel MenuPanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Modal
        {
            get;
            set;
        }

        private CustomMenuOptions menuOptions;

        public CustomMenu(CustomMenuOptions menuOptions, SdlDotNet.Widgets.WidgetCollection widgets)
            : base(menuOptions.Name)
            {
            this.menuOptions = menuOptions;

            this.Size = menuOptions.Size;

            this.MenuDirection = Enums.MenuDirection.Vertical;
            this.Modal = menuOptions.Modal;

            for (int i = 0; i < widgets.Count; i++)
            {
                this.AddWidget(widgets[i]);
            }
        }

        public void ProcessPacketCommand(string[] data)
        {
            switch (data[0])
            {
            }
        }
    }
}
