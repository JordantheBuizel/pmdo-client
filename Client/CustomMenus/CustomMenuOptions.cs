﻿// <copyright file="CustomMenuOptions.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.CustomMenus
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    internal class CustomMenuOptions
    {
        public Size Size { get; set; }

        public Point Location { get; set; }

        public string Name { get; set; }

        public bool Modal { get; set; }
    }
}
