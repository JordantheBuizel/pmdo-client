﻿// <copyright file="Hash.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Security
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using System.Text;

    /// <summary>
    /// Class that creates MD5 hashes
    /// </summary>
    public class Hash
    {
        /// <summary>
        /// Generates a MD5 hash based on the source text.
        /// </summary>
        /// <param name="SourceText">The text to hash.</param>
        /// <returns>The hashed text as a Base64 string.</returns>
        public static string GenerateMD5Hash(string SourceText)
        {
            SourceText = SourceText + "SALT";

            // Create a salted hash so its harder to use hashtables on it
            // Create an encoding object to ensure the encoding standard for the source text
            UnicodeEncoding ue = new UnicodeEncoding();

            // Retrieve a byte array based on the source text
            byte[] byteSourceText = ue.GetBytes(SourceText);

            // Instantiate an MD5 Provider object
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

            // Compute the hash value from the source
            byte[] byteHash = md5.ComputeHash(byteSourceText);

            // And convert it to String format for return
            return Convert.ToBase64String(byteHash);
        }

        public static string GenerateSHA1Hash(string SourceText)
        {
            SourceText = SourceText + "SALT";

            // Create a salted hash so its harder to use hashtables on it
            // Create an encoding object to ensure the encoding standard for the source text
            UnicodeEncoding ue = new UnicodeEncoding();

            // Retrieve a byte array based on the source text
            byte[] byteSourceText = ue.GetBytes(SourceText);

            // Instantiate an MD5 Provider object
            SHA1CryptoServiceProvider sHA1 = new SHA1CryptoServiceProvider();

            // Compute the hash value from the source
            byte[] byteHash = sHA1.ComputeHash(byteSourceText);

            // And convert it to String format for return
            return Convert.ToBase64String(byteHash);
        }
    }
}