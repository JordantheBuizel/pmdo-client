﻿// <copyright file="StoryState.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading;

    internal class StoryState
    {
        private readonly ManualResetEvent resetEvent;

        public bool StoryPaused { get; set; }

        public bool SpeechMenuActive { get; set; }

        public ISegment NextSegment { get; set; }

        public List<FNPCs.FNPC> FNPCs { get; set; }

        public int CurrentSegment { get; set; }

        public StoryState(ManualResetEvent resetEvent)
        {
            this.resetEvent = resetEvent;
            this.FNPCs = new List<FNPCs.FNPC>();
        }

        public void ResetWaitEvent()
        {
            this.resetEvent.Reset();
        }

        public void Pause()
        {
            this.ResetWaitEvent();
            this.resetEvent.WaitOne();
        }

        public void Pause(int milliseconds)
        {
            this.ResetWaitEvent();
            this.resetEvent.WaitOne(milliseconds, false);
        }

        public void Unpause()
        {
            this.resetEvent.Set();
        }
    }
}
