﻿// <copyright file="ScreenImageOverlay.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

using System.Drawing;

namespace Client.Logic.Stories.Components
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SdlDotNet.Graphics;

    internal class ScreenImageOverlay
    {
        private readonly string file;
        private readonly string imageId;
        private readonly int x;
        private readonly int y;
        private Bitmap bitmap;

        public string File
        {
            get { return this.file; }
        }

        public string ImageId
        {
            get { return this.imageId; }
        }

        public int X
        {
            get { return this.x; }
        }

        public int Y
        {
            get { return this.y; }
        }

        public Bitmap Surface
        {
            get { return this.bitmap; }
        }

        public ScreenImageOverlay(string file, string imageID, int x, int y)
        {
            this.file = file;
            this.imageId = imageID;
            this.x = x;
            this.y = y;
        }

        public void LoadImage()
        {
            this.bitmap = new Bitmap(IO.Paths.StoryDataPath + "Images/" + this.file);
        }
    }
}
