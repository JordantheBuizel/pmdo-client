﻿// <copyright file="MapVisibilitySegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Menus.Core;

    using PMU.Core;

    internal class MapVisibilitySegment : ISegment
    {
        private bool visible;
        private ListPair<string, string> parameters;
        private StoryState storyState;

        public MapVisibilitySegment(bool visible)
        {
            this.Load(visible);
        }

        public MapVisibilitySegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.MapVisibility; }
        }

        public bool Visible
        {
            get { return this.visible; }
            set { this.visible = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(bool visible)
        {
            this.visible = visible;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.visible = parameters.GetValue("Visible").ToBool();
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
           Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.ScreenVisible = this.visible;
        }
    }
}