﻿// <copyright file="ISegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories
{
    using System;
    using PMU.Core;

    internal interface ISegment
    {
        Enums.StoryAction Action { get; }

        ListPair<string, string> Parameters { get; }

        bool UsesSpeechMenu { get; }

        void Process(StoryState state);

        void LoadFromSegmentData(ListPair<string, string> parameters);
    }
}
