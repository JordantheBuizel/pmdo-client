﻿// <copyright file="AskQuestionSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Menus.Core;

    using PMU.Core;

    internal class AskQuestionSegment : ISegment
    {
        private int segmentOnNo;
        private ListPair<string, string> parameters;
        private string question;
        private int segmentOnYes;
        private int mugshot;
        private StoryState storyState;
        private string[] options;

        public AskQuestionSegment(string text, int speaker, int segmentOnYes, int segmentOnNo, string[] options)
        {
            this.Load(text, segmentOnYes, segmentOnNo, speaker, options);
        }

        public AskQuestionSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.AskQuestion; }
        }

        public int SegmentOnNo
        {
            get { return this.segmentOnNo; }
            set { this.segmentOnNo = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        public string Question
        {
            get { return this.question; }
            set { this.question = value; }
        }

        public int SegmentOnYes
        {
            get { return this.segmentOnYes; }
            set { this.segmentOnYes = value; }
        }

        public int Mugshot
        {
            get { return this.mugshot; }
            set { this.mugshot = value; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(string question, int segmentOnYes, int segmentOnNo, int mugshot, string[] options)
        {
            this.question = question;
            this.segmentOnYes = segmentOnYes;
            this.segmentOnNo = segmentOnNo;
            this.mugshot = mugshot;
            this.options = options;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;

            // this code never gets reached
            // if (parameters.Count > 4) {
            //    string[] choices = new string[data[4].ToInt()];
            //    int n = 5;
            //    for (int i = 0; i < choices.Length; i++) {
            //        choices[i] = data[n];

            // n += 1;
            //    }
            //    Load(parameters.GetValue("Question"), parameters.GetValue("SegmentOnYes").ToInt(-1), parameters.GetValue("SegmentOnNo").ToInt(-1), parameters.GetValue("Mugshot").ToInt(-1), choices);
            // } else {
            this.Load(parameters.GetValue("Question"), parameters.GetValue("SegmentOnYes").ToInt(-1), parameters.GetValue("SegmentOnNo").ToInt(-1), parameters.GetValue("Mugshot").ToInt(-1), new string[] { "Yes", "No" });

            // }
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            Menus.MenuSwitcher.ShowBlankMenu();
            Components.SpokenTextMenu textMenu;
            IMenu menuToFind = Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("story-spokenTextMenu");
            if (menuToFind != null)
            {
                textMenu = (Components.SpokenTextMenu)menuToFind;
            }
else
            {
                textMenu = new Components.SpokenTextMenu("story-spokenTextMenu", Windows.WindowSwitcher.GameWindow.MapViewer.Size);
            }

            textMenu.DisplayText(StoryProcessor.ReplaceVariables(this.question), this.mugshot);
            Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(textMenu, true);

            Components.OptionSelectionMenu optionMenu = new Components.OptionSelectionMenu("story-optionSelectionMenu", Windows.WindowSwitcher.GameWindow.MapViewer.Size, this.options);
            optionMenu.OptionSelected += new Components.OptionSelectionMenu.OptionSelectedDelegate(this.OptionMenu_OptionSelected);
            Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(optionMenu, true);

            Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu(optionMenu);
            Windows.WindowSwitcher.GameWindow.MenuManager.BlockInput = true;

            this.storyState = state;

            state.Pause();

            optionMenu.OptionSelected -= new Components.OptionSelectionMenu.OptionSelectedDelegate(this.OptionMenu_OptionSelected);
            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(optionMenu);

            // if (state.NextSegment == null || !state.NextSegment.UsesSpeechMenu) {
                Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(textMenu);

            // }
        }

        private void OptionMenu_OptionSelected(string option)
        {
            bool segmentSet = false;
            if (option == "Yes")
            {
                if (this.segmentOnYes > -1)
                {
                    this.storyState.CurrentSegment = this.segmentOnYes - 2;
                    segmentSet = true;
                }
            }
            else if (option == "No")
            {
                if (this.segmentOnNo > -1)
                {
                    this.storyState.CurrentSegment = this.segmentOnNo - 2;
                    segmentSet = true;
                }
            }

            if (!segmentSet)
            {
                Network.Messenger.SendPacket(PMU.Sockets.TcpPacket.CreatePacket("questionresult", option));
            }

            this.storyState.Unpause();
        }
    }
}