﻿// <copyright file="MovePlayerSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using PMU.Core;

    internal class MovePlayerSegment : ISegment
    {
        private bool pause;
        private StoryState storyState;
        private int x;
        private int y;
        private int speed;
        private ListPair<string, string> parameters;

        public MovePlayerSegment(int x, int y, int speed, bool pause)
        {
            this.Load(x, y, speed, pause);
        }

        public MovePlayerSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.MovePlayer; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        public bool Pause
        {
            get { return this.pause; }
            set { this.pause = value; }
        }

        public int X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        public int Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        public int Speed
        {
            get { return this.speed; }
            set { this.speed = value; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(int x, int y, int speed, bool pause)
        {
            this.x = x;
            this.y = y;
            this.speed = speed;
            this.pause = pause;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;

            this.Load(parameters.GetValue("X").ToInt(), parameters.GetValue("Y").ToInt(), parameters.GetValue("Speed").ToInt(), parameters.GetValue("Pause").ToBool());
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            this.storyState = state;

            Players.MyPlayer myPlayer = Players.PlayerManager.MyPlayer;
            myPlayer.TargetX = this.x;
            myPlayer.TargetY = this.y;
            myPlayer.StoryMovementSpeed = (Enums.MovementSpeed)this.speed;

            if (this.pause)
            {
                state.StoryPaused = true;
                state.Pause();
                state.StoryPaused = false;
            }
        }
    }
}