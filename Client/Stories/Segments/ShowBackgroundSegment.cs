﻿// <copyright file="ShowBackgroundSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Menus.Core;

    using PMU.Core;

    internal class ShowBackgroundSegment : ISegment
    {
        private string file;
        private ListPair<string, string> parameters;
        private StoryState storyState;

        public ShowBackgroundSegment(string file)
        {
            this.Load(file);
        }

        public ShowBackgroundSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.ShowBackground; }
        }

        public string File
        {
            get { return this.file; }
            set { this.file = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(string file)
        {
            this.file = file;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.file = parameters.GetValue("File");
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            if (Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.StoryBackground != null)
            {
                Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.StoryBackground.Dispose();
            }

            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.StoryBackground = Graphic.SurfaceManager.LoadSurface(IO.Paths.StoryDataPath + "Backgrounds/" + this.file);
        }
    }
}