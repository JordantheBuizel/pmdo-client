﻿// <copyright file="StopMusicSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;

    internal class StopMusicSegment : ISegment
    {
        private Enums.PadlockState state;
        private ListPair<string, string> parameters;
        private StoryState storyState;

        public StopMusicSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.StopMusic; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            state.ResetWaitEvent();
            Music.Music.AudioPlayer.StopMusic();
        }
    }
}
