﻿// <copyright file="PadlockSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;

    internal class PadlockSegment : ISegment
    {
        private Enums.PadlockState state;
        private ListPair<string, string> parameters;
        private StoryState storyState;

        public PadlockSegment(Enums.PadlockState state)
        {
            this.Load(state);
        }

        public PadlockSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.Padlock; }
        }

        public Enums.PadlockState State
        {
            get { return this.state; }
            set { this.state = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(Enums.PadlockState state)
        {
            this.state = state;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.state = (Enums.PadlockState)parameters.GetValue("State").ToInt(0);
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            Network.Messenger.SendPacket(PMU.Sockets.TcpPacket.CreatePacket("actonaction"));
        }
    }
}
