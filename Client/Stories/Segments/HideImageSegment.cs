﻿// <copyright file="HideImageSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using PMU.Core;

    internal class HideImageSegment : ISegment
    {
        private string imageID;
        private ListPair<string, string> parameters;
        private StoryState storyState;

        public HideImageSegment(string imageID)
        {
            this.Load(imageID);
        }

        public HideImageSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.MapVisibility; }
        }

        public string ImageID
        {
            get { return this.imageID; }
            set { this.imageID = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(string imageID)
        {
            this.imageID = imageID;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.imageID = parameters.GetValue("ImageID");
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            int index = -1;
            for (int i = 0; i < Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.ScreenImageOverlays.Count; i++)
            {
                if (Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.ScreenImageOverlays[i].ImageId == this.imageID)
                {
                    index = i;
                    break;
                }
            }

            if (index > -1)
            {
                Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.ScreenImageOverlays.RemoveAt(index);
            }
        }
    }
}