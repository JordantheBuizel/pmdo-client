﻿// <copyright file="SaySegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Menus.Core;

    using PMU.Core;

    internal class SaySegment : ISegment
    {
        private int pauseLocation;
        private ListPair<string, string> parameters;
        private int speaker;
        private int speed;
        private StoryState storyState;
        private string text;

        public SaySegment(string text, int speaker, int speed, int pauseLocation)
        {
            this.Load(text, speaker, speed, pauseLocation);
        }

        public SaySegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.Say; }
        }

        public int PauseLocation
        {
            get { return this.pauseLocation; }
            set { this.pauseLocation = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        public int Speaker
        {
            get { return this.speaker; }
            set { this.speaker = value; }
        }

        public int Speed
        {
            get { return this.speed; }
            set { this.speed = value; }
        }

        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return true; }
        }

        public void Load(string text, int speaker, int speed, int pauseLocation)
        {
            this.text = text;
            this.speaker = speaker;
            this.speed = speed;
            this.pauseLocation = pauseLocation;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.Load(parameters.GetValue("Text"), parameters.GetValue("Mugshot").ToInt(-1), parameters.GetValue("Speed").ToInt(1), parameters.GetValue("PauseLocation").ToInt(-1));
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            Menus.MenuSwitcher.ShowBlankMenu();
            Components.SpokenTextMenu textMenu;
            IMenu menuToFind = Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("story-spokenTextMenu");
            if (menuToFind != null)
            {
                textMenu = (Components.SpokenTextMenu)menuToFind;
            }
else
            {
                textMenu = new Components.SpokenTextMenu("story-spokenTextMenu", Windows.WindowSwitcher.GameWindow.MapViewer.Size);
            }

            textMenu.Click += new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(this.TextMenu_Click);
            textMenu.KeyDown += new EventHandler<SdlDotNet.Input.KeyboardEventArgs>(this.TextMenu_KeyDown);
            textMenu.DisplayText(StoryProcessor.ReplaceVariables(this.text), this.speaker);
            Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(textMenu, true);
            Windows.WindowSwitcher.GameWindow.MenuManager.BlockInput = false;

            this.storyState = state;

            if (Windows.WindowSwitcher.GameWindow.BattleLog.Visible)
            {
                Windows.WindowSwitcher.GameWindow.BattleLog.Hide();
            }

            state.Pause();

            textMenu.Click -= new EventHandler<SdlDotNet.Widgets.MouseButtonEventArgs>(this.TextMenu_Click);
            textMenu.KeyDown -= new EventHandler<SdlDotNet.Input.KeyboardEventArgs>(this.TextMenu_KeyDown);

            if (state.NextSegment == null || !state.NextSegment.UsesSpeechMenu)
            {
                Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(textMenu);
            }
        }

        private void TextMenu_KeyDown(object sender, SdlDotNet.Input.KeyboardEventArgs e)
        {
            if (e.Key == SdlDotNet.Input.Key.Return)
            {
                this.storyState.Unpause();
            }
        }

        private void TextMenu_Click(object sender, SdlDotNet.Widgets.MouseButtonEventArgs e)
        {
            this.storyState.Unpause();
        }
    }
}