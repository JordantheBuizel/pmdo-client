﻿// <copyright file="PlayerSpeechSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PMU.Core;

    internal class PlayerSpeechSegment : ISegment
    {

        public PlayerSpeechSegment()
        {

        }

        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.PlayerSpeech; }
        }

        public ListPair<string, string> Parameters => throw new NotImplementedException();

        public bool UsesSpeechMenu => throw new NotImplementedException();

        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            throw new NotImplementedException();
        }

        public void Process(StoryState state)
        {
            throw new NotImplementedException();
        }
    }
}
