﻿// <copyright file="PlayerPadlockSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;

    internal class PlayerPadlockSegment : ISegment
    {
        private Enums.PadlockState state;
        private ListPair<string, string> parameters;
        private StoryState storyState;

        public PlayerPadlockSegment(Enums.PadlockState state)
        {
            this.Load(state);
        }

        public PlayerPadlockSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.PlayerPadlock; }
        }

        public Enums.PadlockState State
        {
            get { return this.state; }
            set { this.state = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(Enums.PadlockState state)
        {
            this.state = state;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.state = (Enums.PadlockState)Enum.Parse(typeof(Enums.PadlockState), parameters.GetValue("MovementState"));
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            switch (this.state)
            {
                case Enums.PadlockState.Lock:
                {
                        Players.PlayerManager.MyPlayer.MovementLocked = true;
                    }

                    break;
                case Enums.PadlockState.Unlock:
                {
                        Players.PlayerManager.MyPlayer.MovementLocked = false;
                    }

                    break;
            }
        }
    }
}
