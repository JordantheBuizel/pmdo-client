﻿// <copyright file="CreateFNPCSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using PMU.Core;

    internal class CreateFNPCSegment : ISegment
    {
        private string id;
        private string parentMapID;
        private StoryState storyState;
        private int x;
        private int y;
        private int sprite;
        private int form;
        private Enums.Coloration shiny;
        private Enums.Sex gender;
        private ListPair<string, string> parameters;

        public CreateFNPCSegment(string id, string parentMapID, int x, int y, int sprite, int form, Enums.Coloration shiny, Enums.Sex gender)
        {
            this.Load(id, parentMapID, x, y, sprite, form, shiny, gender);
        }

        public CreateFNPCSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.CreateFNPC; }
        }

        public string ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        public string ParentMapID
        {
            get { return this.parentMapID; }
            set { this.parentMapID = value; }
        }

        public int X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        public int Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        public int Sprite
        {
            get { return this.sprite; }
            set { this.sprite = value; }
        }

        public int Form
        {
            get { return this.form; }
            set { this.form = value; }
        }

        public Enums.Sex Gender
        {
            get { return this.gender; }
            set { this.gender = value; }
        }

        public Enums.Coloration Shiny
        {
            get { return this.shiny; }
            set { this.shiny = value; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(string id, string parentMapID, int x, int y, int sprite, int form, Enums.Coloration shiny, Enums.Sex gender)
        {
            this.id = id;
            this.parentMapID = parentMapID;
            this.x = x;
            this.y = y;
            this.sprite = sprite;
            this.form = form;
            this.shiny = shiny;
            this.gender = gender;

            if (this.parentMapID == "s-2")
            {
                this.parentMapID = "-2";
            }
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.Load(parameters.GetValue("ID"), parameters.GetValue("ParentMapID"),
                parameters.GetValue("X").ToInt(), parameters.GetValue("Y").ToInt(),
                parameters.GetValue("Sprite").ToInt(), 0, Enums.Coloration.Normal, Enums.Sex.Genderless);
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            this.storyState = state;
            FNPCs.FNPC fnpc = new FNPCs.FNPC();
            fnpc.MapID = this.parentMapID;
            fnpc.X = this.x;
            fnpc.Y = this.y;
            fnpc.Sprite = this.sprite;
            fnpc.Form = this.form;
            fnpc.Shiny = this.shiny;
            fnpc.Sex = this.gender;
            fnpc.ID = this.id;

            state.FNPCs.Add(fnpc);
        }
    }
}