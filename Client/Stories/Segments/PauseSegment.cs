﻿// <copyright file="PauseSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Menus.Core;

    using PMU.Core;

    internal class PauseSegment : ISegment
    {
        private int length;
        private ListPair<string, string> parameters;
        private StoryState storyState;

        public PauseSegment(int length)
        {
            this.Load(length);
        }

        public PauseSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.Pause; }
        }

        public int Length
        {
            get { return this.length; }
            set { this.length = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(int length)
        {
            this.length = length;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.length = parameters.GetValue("Length").ToInt(0);
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            state.Pause(this.length);
        }
    }
}