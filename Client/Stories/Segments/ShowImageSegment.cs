﻿// <copyright file="ShowImageSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Menus.Core;

    using PMU.Core;

    internal class ShowImageSegment : ISegment
    {
        private string imageID;
        private ListPair<string, string> parameters;
        private int x;
        private int y;
        private StoryState storyState;
        private string file;

        public ShowImageSegment(string file, string imageID, int x, int y)
        {
            this.Load(file, imageID, x, y);
        }

        public ShowImageSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.ShowImage; }
        }

        public string File
        {
            get { return this.file; }
            set { this.file = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        public string ImageID
        {
            get { return this.imageID; }
            set { this.imageID = value; }
        }

        public int X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        public int Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(string file, string imageID, int x, int y)
        {
            this.file = file;
            this.imageID = imageID;
            this.x = x;
            this.y = y;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.Load(parameters.GetValue("File"), parameters.GetValue("ImageID"), parameters.GetValue("X").ToInt(), parameters.GetValue("Y").ToInt());
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            this.storyState = state;
            state.ResetWaitEvent();

            Components.ScreenImageOverlay imageOverlay = new Components.ScreenImageOverlay(this.file, this.imageID, this.x, this.y);
            imageOverlay.LoadImage();
            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.ScreenImageOverlays.Add(imageOverlay);
        }
    }
}