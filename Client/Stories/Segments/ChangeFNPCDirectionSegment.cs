﻿// <copyright file="ChangeFNPCDirectionSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using PMU.Core;

    internal class ChangeFNPCDirectionSegment : ISegment
    {
        private string id;
        private StoryState storyState;
        private Enums.Direction direction;
        private ListPair<string, string> parameters;

        public ChangeFNPCDirectionSegment(string id, Enums.Direction direction)
        {
            this.Load(id, direction);
        }

        public ChangeFNPCDirectionSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.ChangeFNPCDir; }
        }

        public string ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        public Enums.Direction Direction
        {
            get { return this.direction; }
            set { this.direction = value; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(string id, Enums.Direction direction)
        {
            this.id = id;
            this.direction = direction;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.Load(parameters.GetValue("ID"), (Enums.Direction)parameters.GetValue("Direction").ToInt());
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            this.storyState = state;
            for (int i = 0; i < state.FNPCs.Count; i++)
            {
                if (state.FNPCs[i].ID == this.id)
                {
                    state.FNPCs[i].Direction = this.direction;
                }
            }
        }
    }
}