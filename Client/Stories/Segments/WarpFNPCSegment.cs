﻿// <copyright file="WarpFNPCSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using PMU.Core;

    internal class WarpFNPCSegment : ISegment
    {
        private string id;
        private StoryState storyState;
        private int x;
        private int y;
        private ListPair<string, string> parameters;

        public WarpFNPCSegment(string id, int x, int y)
        {
            this.Load(id, x, y);
        }

        public WarpFNPCSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.WarpFNPC; }
        }

        public string ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        public int X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        public int Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(string id, int x, int y)
        {
            this.id = id;
            this.x = x;
            this.y = y;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.Load(parameters.GetValue("ID"), parameters.GetValue("X").ToInt(), parameters.GetValue("Y").ToInt());
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            this.storyState = state;
            for (int i = 0; i < state.FNPCs.Count; i++)
            {
                if (state.FNPCs[i].ID == this.id)
                {
                    state.FNPCs[i].X = this.x;
                    state.FNPCs[i].Y = this.y;
                }
            }
        }
    }
}