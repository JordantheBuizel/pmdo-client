﻿// <copyright file="HidePlayersSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using PMU.Core;

    internal class HidePlayersSegment : ISegment
    {
        private int x;
        private int y;
        private StoryState storyState;
        private string map;
        private ListPair<string, string> parameters;

        public HidePlayersSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.HidePlayers; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load()
        {
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.PlayersVisible = false;
        }
    }
}