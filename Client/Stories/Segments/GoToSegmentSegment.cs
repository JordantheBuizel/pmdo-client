﻿// <copyright file="GoToSegmentSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Menus.Core;

    using PMU.Core;

    internal class GoToSegmentSegment : ISegment
    {
        private ListPair<string, string> parameters;
        private int segment;
        private StoryState storyState;

        public GoToSegmentSegment(int segment)
        {
            this.Load(segment);
        }

        public GoToSegmentSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.GoToSegment; }
        }

        public int Segment
        {
            get { return this.segment; }
            set { this.segment = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(int segment)
        {
            this.segment = segment;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.Load(parameters.GetValue("Segment").ToInt());
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            this.storyState = state;
            this.storyState.CurrentSegment = this.segment - 1;
        }
    }
}