﻿// <copyright file="WarpSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using PMU.Core;

    internal class WarpSegment : ISegment
    {
        private int x;
        private int y;
        private StoryState storyState;
        private string map;
        private ListPair<string, string> parameters;

        public WarpSegment(string map, int x, int y)
        {
            this.Load(map, x, y);
        }

        public WarpSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.Warp; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        public int Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        public int X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        public string Map
        {
            get { return this.map; }
            set { this.map = value; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(string map, int x, int y)
        {
            this.map = map;
            this.x = x;
            this.y = y;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.Load(parameters.GetValue("MapID"), parameters.GetValue("X").ToInt(), parameters.GetValue("Y").ToInt());
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            Network.Messenger.SendPacket(PMU.Sockets.TcpPacket.CreatePacket("actonaction"));

            state.StoryPaused = true;
            state.Pause();
        }
    }
}