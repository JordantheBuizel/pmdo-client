﻿// <copyright file="ChangePlayerDirectionSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using PMU.Core;

    internal class ChangePlayerDirectionSegment : ISegment
    {
        private StoryState storyState;
        private Enums.Direction direction;
        private ListPair<string, string> parameters;

        public ChangePlayerDirectionSegment(Enums.Direction direction)
        {
            this.Load(direction);
        }

        public ChangePlayerDirectionSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.ChangePlayerDir; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        public Enums.Direction Direction
        {
            get { return this.direction; }
            set { this.direction = value; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(Enums.Direction direction)
        {
            this.direction = direction;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.Load((Enums.Direction)parameters.GetValue("Direction").ToInt());
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            this.storyState = state;
            Players.PlayerManager.MyPlayer.Direction = this.direction;
            Network.Messenger.SendPlayerDir();
        }
    }
}