﻿// <copyright file="DeleteFNPCSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using PMU.Core;

    internal class DeleteFNPCSegment : ISegment
    {
        private string id;
        private StoryState storyState;
        private Enums.Direction direction;
        private ListPair<string, string> parameters;

        public DeleteFNPCSegment(string id)
        {
            this.Load(id);
        }

        public DeleteFNPCSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.DeleteFNPC; }
        }

        public string ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(string id)
        {
            this.id = id;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.Load(parameters.GetValue("ID"));
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            this.storyState = state;
            for (int i = state.FNPCs.Count - 1; i >= 0; i--)
            {
                if (state.FNPCs[i].ID == this.id)
                {
                    state.FNPCs.RemoveAt(i);
                }
            }
        }
    }
}