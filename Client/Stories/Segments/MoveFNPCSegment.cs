﻿// <copyright file="MoveFNPCSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using PMU.Core;

    internal class MoveFNPCSegment : ISegment
    {
        private string id;
        private bool pause;
        private StoryState storyState;
        private int x;
        private int y;
        private int speed;
        private ListPair<string, string> parameters;

        public MoveFNPCSegment(string id, int x, int y, int speed, bool pause)
        {
            this.Load(id, x, y, speed, pause);
        }

        public MoveFNPCSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.MoveFNPC; }
        }

        public string ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        public bool Pause
        {
            get { return this.pause; }
            set { this.pause = value; }
        }

        public int X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        public int Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        public int Speed
        {
            get { return this.speed; }
            set { this.speed = value; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(string id, int x, int y, int speed, bool pause)
        {
            this.id = id;
            this.x = x;
            this.y = y;
            this.speed = speed;
            this.pause = pause;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;

            this.Load(parameters.GetValue("ID"), parameters.GetValue("X").ToInt(), parameters.GetValue("Y").ToInt(),
                parameters.GetValue("Speed").ToInt(), parameters.GetValue("Pause").ToBool());
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            this.storyState = state;
            for (int i = 0; i < state.FNPCs.Count; i++)
            {
                if (state.FNPCs[i].ID == this.id)
                {
                    state.FNPCs[i].TargetX = this.x;
                    state.FNPCs[i].TargetY = this.y;
                }
            }

            if (this.pause)
            {
                state.StoryPaused = true;
                state.Pause();
                state.StoryPaused = false;
            }
        }
    }
}