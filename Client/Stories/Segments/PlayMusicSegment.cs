﻿// <copyright file="PlayMusicSegment.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using PMU.Core;

    internal class PlayMusicSegment : ISegment
    {
        private bool honorSettings;
        private bool loop;
        private StoryState storyState;
        private string file;
        private ListPair<string, string> parameters;

        public PlayMusicSegment(string file, bool honorSettings, bool loop)
        {
            this.Load(file, honorSettings, loop);
        }

        public PlayMusicSegment()
        {
        }

        /// <inheritdoc/>
        public Enums.StoryAction Action
        {
            get { return Enums.StoryAction.PlayMusic; }
        }

        public string File
        {
            get { return this.file; }
            set { this.file = value; }
        }

        /// <inheritdoc/>
        public ListPair<string, string> Parameters
        {
            get { return this.parameters; }
        }

        public bool HonorSettings
        {
            get { return this.honorSettings; }
            set { this.honorSettings = value; }
        }

        public bool Loop
        {
            get { return this.loop; }
            set { this.loop = value; }
        }

        /// <inheritdoc/>
        public bool UsesSpeechMenu
        {
            get { return false; }
        }

        public void Load(string file, bool honorSettings, bool loop)
        {
            this.file = file;
            this.honorSettings = honorSettings;
            this.loop = loop;
        }

        /// <inheritdoc/>
        public void LoadFromSegmentData(ListPair<string, string> parameters)
        {
            this.parameters = parameters;
            this.Load(parameters.GetValue("File"), parameters.GetValue("HonorSettings").ToBool(), parameters.GetValue("Loop").ToBool());
        }

        /// <inheritdoc/>
        public void Process(StoryState state)
        {
            this.storyState = state;
            string fileToPlay = null;
            if (this.file == "%mapmusic%")
            {
                fileToPlay = Maps.MapHelper.ActiveMap.Music;
            }
else
            {
                fileToPlay = this.file;
            }

            if (!string.IsNullOrEmpty(fileToPlay))
            {
                Music.Music.AudioPlayer.PlayMusic(fileToPlay, this.loop ? -1 : 1, !this.honorSettings, true);
            }
        }
    }
}