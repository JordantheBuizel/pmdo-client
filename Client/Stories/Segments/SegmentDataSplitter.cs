﻿// <copyright file="SegmentDataSplitter.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.Segments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class SegmentDataSplitter
    {
        public const char SEPERATORCHAR = '|';

        public static string[] SplitSegmentData(string segmentData)
        {
            if (segmentData.Contains(SEPERATORCHAR.ToString()))
            {
                List<string> parsed = new List<string>();
                bool startNewLine = true;
                int currentLine = -1;
                bool isInQuotes = false;
                for (int i = 0; i < segmentData.Length; i++)
                {
                    if (startNewLine)
                    {
                        parsed.Add(string.Empty);
                        currentLine++;
                        startNewLine = false;
                    }

                    char curChar = segmentData[i];
                    if (curChar == SEPERATORCHAR && isInQuotes == false)
                    {
                        startNewLine = true;
                    }
                    else if (curChar == '"')
                    {
                        isInQuotes = !isInQuotes;
                    }
else
                    {
                        parsed[currentLine] += curChar;
                    }
                }

                return parsed.ToArray();
            }
else
            {
                return new string[] { segmentData };
            }
        }
    }
}
