﻿// <copyright file="FNPC.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories.FNPCs
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class FNPC : Graphic.Renderers.Sprites.ISprite
    {
        /// <inheritdoc/>
        public Graphic.SpriteSheet SpriteSheet
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int IdleTimer { get; set; }
        /// <inheritdoc/>
        public int IdleFrame { get; set; }
        /// <inheritdoc/>
        public int LastWalkTime { get; set; }
        /// <inheritdoc/>
        public int WalkingFrame { get; set; }

        /// <inheritdoc/>
        public int Sprite
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Form { get; set; }
        /// <inheritdoc/>
        public Enums.Coloration Shiny { get; set; }
        /// <inheritdoc/>
        public Enums.Sex Sex { get; set; }

        /// <inheritdoc/>
        public Enums.Direction Direction
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public bool Attacking
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int TotalAttackTime { get; set; }

        /// <inheritdoc/>
        public System.Drawing.Point Offset
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public System.Drawing.Point Location
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int AttackTimer
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int X
        {
            get
            {
                return this.Location.X;
            }

            set
            {
                this.Location = new System.Drawing.Point(value, this.Location.Y);
            }
        }

        /// <inheritdoc/>
        public int Y
        {
            get
            {
                return this.Location.Y;
            }

            set
            {
                this.Location = new System.Drawing.Point(this.Location.X, value);
            }
        }

        /// <inheritdoc/>
        public Enums.MovementSpeed MovementSpeed
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.StatusAilment StatusAilment
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public List<int> VolatileStatus
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public bool Leaving { get; set; }

        /// <inheritdoc/>
        public bool ScreenActive
        {
            get { return true; }
            set { }
        }

        /// <inheritdoc/>
        public int SleepTimer
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int SleepFrame
        {
            get;
            set;
        }

        public string MapID
        {
            get;
            set;
        }

        public string ID
        {
            get;
            set;
        }

        public int TargetX
        {
            get;
            set;
        }

        public int TargetY
        {
            get;
            set;
        }

        public int LastMovement
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Graphic.Renderers.Sprites.SpeechBubble CurrentSpeech { get; set; }

        /// <inheritdoc/>
        public Graphic.Renderers.Sprites.Emoticon CurrentEmote { get; set; }

        public Algorithms.Pathfinder.PathfinderResult PathfinderResult
        {
            get;
            set;
        }

        public FNPC()
        {
            this.TargetX = -1;
            this.TargetY = -1;
            this.VolatileStatus = new List<int>();
        }
    }
}
