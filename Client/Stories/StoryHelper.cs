﻿// <copyright file="StoryHelper.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class StoryHelper
    {
        private static int dataLoadPercent = 0;
        private static StoryCollection mStories;
        private static Story cachedStory;

        public static int DataLoadPercent
        {
            get { return dataLoadPercent; }
        }

        public static StoryCollection Stories
        {
            get { return mStories; }
        }

        public static Story CachedStory
        {
            get { return cachedStory; }
            set { cachedStory = value; }
        }

        public static void InitStoryCollection()
        {
            mStories = new StoryCollection(MaxInfo.MaxStories + 1);
        }

        public static void LoadStoriesFromPacket(string[] parse)
        {
            try
            {
                int n = 1;
                for (int i = 0; i <= MaxInfo.MaxStories; i++)
                {
                    dataLoadPercent = Math.Min(99, MathFunctions.CalculatePercent(i, MaxInfo.MaxStories));
                    mStories[i] = new Story();
                    mStories[i].Name = parse[n];
                    n += 1;
                    ((Windows.WinLoading)Windows.WindowSwitcher.FindWindow("winLoading")).UpdateLoadText("Recieving Data... " + DataManager.AverageLoadPercent().ToString() + "%");
                }

                dataLoadPercent = 100;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionHandler.OnException(ex);
            }
        }
    }
}