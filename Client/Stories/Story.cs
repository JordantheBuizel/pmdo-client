﻿// <copyright file="Story.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Story
    {
        public Story()
        {
            this.Segments = new List<ISegment>();
            this.ExitAndContinue = new List<int>();
        }

        public List<int> ExitAndContinue
        {
            get;
            set;
        }

        public bool Loaded
        {
            get;
            set;
        }

        public bool LocalStory
        {
            get;
            set;
        }

        public int MaxSegments
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public int Revision
        {
            get;
            set;
        }

        public List<ISegment> Segments
        {
            get;
            set;
        }

        public StoryState State
        {
            get;
            set;
        }

        public int StoryStart
        {
            get;
            set;
        }

        public int Version
        {
            get;
            set;
        }

        public void AppendSegment(ISegment segment)
        {
            this.Segments.Add(segment);
        }
    }
}