﻿// <copyright file="StoryCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Stories
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class StoryCollection
    {
        private readonly Story[] mStories;

        internal StoryCollection(int maxStories)
        {
            this.mStories = new Story[maxStories];
        }

        public Story this[int index]
        {
            get { return this.mStories[index]; }
            set { this.mStories[index] = value; }
        }
    }
}