﻿// <copyright file="MaxInfo.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal static class MaxInfo
    {
        public const int MAXACTIVETEAM = 4;
        public const int MAXARROWS = 100;
        public const int MAXMAPNPCS = 20;
        public const int MAXNPCDROPS = 10;
        public const int MAXPLAYERARROWS = 100;
        public const int MAXPLAYERMOVES = 4;
        public const int MAXTRADES = 100;

        public static string GameName;
        public static int MaxInv;

        // public static int MaxBank;
        public static int MaxEmoticons;
        public static int MaxEvolutions;
        public static int MaxItems;
        public static int MaxMapItems;
        public static int MaxMapX;
        public static int MaxMapY;
        public static int MaxNpcs;
        public static int MaxShops;
        public static int MaxSpellAnim = 0;
        public static int MaxMoves;
        public static int MaxStories;
        public static int MaxRDungeons;
        public static int MaxDungeons;
        public static int TotalPokemon;
        public static bool Paperdoll = false;
        public static string Website = string.Empty;
    }
}