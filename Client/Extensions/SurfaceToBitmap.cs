﻿// <copyright file="SurfaceToBitmap.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SdlDotNet.Graphics;

    public static class SurfaceToBitmap
    {
        /// <summary>
        /// Extension to fix the original Surface.Bitmap not preserving transparency
        /// </summary>
        /// <param name="surface">SdldotNet.Graphics.Surface to convert</param>
        /// <param name="preserveTransparency">true to use the fix, otherwise use default SdldotNet method</param>
        /// <returns>Converted Bitmap</returns>
        public static Bitmap ToBitmap(this Surface surface, bool preserveTransparency)
        {
            if (preserveTransparency)
            {
                Bitmap resultBmp = new Bitmap(surface.Width, surface.Height);

                for (int x = 0; x < surface.Width; x++)
                {
                    for (int y = 0; y < surface.Height; y++)
                    {
                        resultBmp.SetPixel(x, y, surface.GetPixel(new Point(x, y)));
                    }
                }

                surface.Dispose();
                return resultBmp;
            }
            else
            {
                return surface.Bitmap;
            }
        }
    }
}
