﻿// <copyright file="ColoredProgressBar.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public class ColoredProgressBar : ProgressBar
    {
        private readonly Label txtLabel;
        private string text;

        public ColoredProgressBar()
        {
            this.SetStyle(ControlStyles.UserPaint, true);

            this.txtLabel = new Label();
            this.txtLabel.Text = string.Empty;
            this.txtLabel.MaximumSize = new Size(0, this.Height);
            this.txtLabel.AutoSize = true;
            this.txtLabel.Size = new Size(this.txtLabel.Width, this.Height);
            this.txtLabel.BackColor = Color.Transparent;
            this.txtLabel.Location = Graphic.DrawingSupport.GetCenter(this.Size, this.txtLabel.Size);

            this.Controls.Add(this.txtLabel);
        }

        public new string Text
        {
            get
            {
                return this.text;
            }

            set
            {
                this.txtLabel.Text = value;
                this.text = value;
                this.RecalculateTextboxPosition();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            this.txtLabel.Text = this.Text;
            this.txtLabel.MaximumSize = new Size(0, this.Height);
            this.txtLabel.AutoSize = true;
            this.txtLabel.Size = new Size(this.txtLabel.Width, this.Height);
            this.txtLabel.Location = Graphic.DrawingSupport.GetCenter(this.Size, this.txtLabel.Size);

            LinearGradientBrush brush = null;
            Rectangle rec = new Rectangle(0, 0, this.Width, this.Height);
            double scaleFactor = (this.Value - (double)this.Minimum) / (this.Maximum - (double)this.Minimum);

            if (ProgressBarRenderer.IsSupported)
            {
                ProgressBarRenderer.DrawHorizontalBar(e.Graphics, rec);
            }

            rec.Width = (int)(rec.Width * scaleFactor) + 1;
            brush = new LinearGradientBrush(rec, this.ForeColor, this.BackColor, LinearGradientMode.Vertical);
            e.Graphics.FillRectangle(brush, 2, 2, rec.Width, rec.Height);
        }

        private void RecalculateTextboxPosition()
        {
            this.txtLabel.Location = Graphic.DrawingSupport.GetCenter(this.Size, this.txtLabel.Size);
        }
    }
}
