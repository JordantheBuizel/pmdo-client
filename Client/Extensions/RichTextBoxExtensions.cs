﻿// <copyright file="RichTextBoxExtensions.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

using System.Drawing;
using System.Windows.Forms;

namespace Client.Logic.Extensions
{
    public static class RichTextBoxExtensions
    {
        /// <summary>
        /// Appends strings with color
        /// </summary>
        /// <param name="box">RTB exension</param>
        /// <param name="text">Text to append</param>
        /// <param name="color">Color to change to</param>
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }
    }
}
