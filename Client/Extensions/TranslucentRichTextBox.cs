﻿// <copyright file="TranslucentRichTextBox.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class TranslucentRichTextBox : RichTextBox
    {
        public TranslucentRichTextBox()
        {
            this.InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                // This makes the control's background transparent
                CreateParams cP = base.CreateParams;
                cP.ExStyle |= 0x20;
                return cP;
            }
        }
    }
}
