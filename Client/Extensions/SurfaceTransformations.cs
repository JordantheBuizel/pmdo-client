﻿// <copyright file="SurfaceTransformations.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SdlDotNet.Graphics;

    public static class SurfaceTransformations
    {
        public static Surface CreateFlippedHorizontalSurface(this Surface surface, bool shouldFlip)
        {
            if (shouldFlip)
            {
                Surface returnSurf;

                using (Bitmap source = new Bitmap(surface.Width, surface.Height))
                {
                    for (int x = 0; x < surface.Width; x++)
                    {
                        for (int y = 0; y < surface.Height; y++)
                        {
                            source.SetPixel(x, y, surface.GetPixel(new Point(x, y)));
                        }
                    }

                    using (Bitmap returnBitmap = new Bitmap(source.Width, source.Height))
                    {
                        for (int i = 0; i < source.Height; i++)
                        {
                            for (int j = 0; j < source.Width; j++)
                            {
                                returnBitmap.SetPixel(j, i, source.GetPixel(source.Width - j - 1, i));
                            }
                        }

                        returnSurf = new Surface(returnBitmap);
                    }
                }

                surface.Dispose();
                return returnSurf;
            }
            else
            {
                return surface;
            }
        }

        public static Surface Rotate(this Surface surface, int angle)
        {
            Surface returnSurf;

            // Create a new empty bitmap to hold rotated image.
            using (Bitmap returnBitmap = new Bitmap(surface.Width, surface.Height))
            {
                switch (angle)
                {
                    case 90:
                        {
                            using (Bitmap source = new Bitmap(surface.Width, surface.Height))
                            {
                                for (int x = 0; x < surface.Width; x++)
                                {
                                    for (int y = 0; y < surface.Height; y++)
                                    {
                                        source.SetPixel(x, y, surface.GetPixel(new Point(x, y)));
                                    }
                                }

                                for (int i = 0; i < source.Height; i++)
                                {
                                    for (int j = 0; j < source.Width; j++)
                                    {
                                        returnBitmap.SetPixel(i, j, source.GetPixel(j, source.Width - i - 1));
                                    }
                                }
                            }
                        }

                        break;
                    case 180:
                        {
                            using (Bitmap source = new Bitmap(surface.Width, surface.Height))
                            {
                                for (int x = 0; x < surface.Width; x++)
                                {
                                    for (int y = 0; y < surface.Height; y++)
                                    {
                                        source.SetPixel(x, y, surface.GetPixel(new Point(x, y)));
                                    }
                                }

                                for (int i = 0; i < source.Height; i++)
                                {
                                    for (int j = 0; j < source.Width; j++)
                                    {
                                        returnBitmap.SetPixel(j, i, source.GetPixel(source.Height - j - 1, source.Width - i - 1));
                                    }
                                }
                            }
                        }

                        break;
                    case 270:
                        {
                            using (Bitmap source = new Bitmap(surface.Width, surface.Height))
                            {
                                for (int x = 0; x < surface.Width; x++)
                                {
                                    for (int y = 0; y < surface.Height; y++)
                                    {
                                        source.SetPixel(x, y, surface.GetPixel(new Point(x, y)));
                                    }
                                }

                                for (int i = 0; i < source.Height; i++)
                                {
                                    for (int j = 0; j < source.Width; j++)
                                    {
                                        returnBitmap.SetPixel(i, j, source.GetPixel(source.Width - j - 1, i));
                                    }
                                }
                            }
                        }

                        break;
                    default:
                        {
                            return surface;
                        }
                }

                returnSurf = new Surface(returnBitmap);
            }

            surface.Dispose();
            return returnSurf;
        }
    }
}
