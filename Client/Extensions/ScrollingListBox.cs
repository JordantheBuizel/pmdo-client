﻿// <copyright file="ScrollingListBox.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class ScrollingListBox : ListBox
    {
        private bool firstCall = true;

        public ScrollingListBox(string name)
            : base(name)
        {
        }

        /// <inheritdoc/>
        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (this.firstCall)
            {
                if (this.SelectedIndex != 0 && this.Items.Count > 0)
                {
                    this.SelectItem(0);
                }

                this.firstCall = false;
            }

            if (e.MouseEventArgs.Button == SdlDotNet.Input.MouseButton.WheelDown)
            {
                if (this.SelectedIndex + 1 != this.Items.Count)
                {
                    this.SelectItem(this.SelectedIndex + 1);
                }
            }
            else if (e.MouseEventArgs.Button == SdlDotNet.Input.MouseButton.WheelUp)
            {
                if (this.SelectedIndex - 1 != -1)
                {
                    this.SelectItem(this.SelectedIndex - 1);
                }
            }

            base.OnMouseDown(e);
        }
    }
}
