﻿// <copyright file="CheckBoxImage.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    internal class CheckBoxImage : PictureBox
    {
        private bool check = true;

        public bool Checked
        {
            get
            {
                return this.check;
            }

            set
            {
                this.check = !value;
                this.CheckBoxImage_Click(null, null);
            }
        }

        public CheckBoxImage()
        {
            this.Size = new Size(14, 14);
            this.Click += new EventHandler(this.CheckBoxImage_Click);
            this.CheckBoxImage_Click(this, new EventArgs());
        }

        private void CheckBoxImage_Click(object sender, EventArgs e)
        {
            this.check = !this.check;
            if (this.check)
            {
                this.Image = Properties.Resources._checked;
            }
            else
            {
                this.Image = Properties.Resources._unchecked;
            }
        }
    }
}
