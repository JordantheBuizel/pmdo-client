﻿// <copyright file="SpriteSheet.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using PMU.Core;
    using SdlDotNet.Graphics;

    public enum FrameType
    {
        Idle = 0,
        Walk,
        Attack,
        AttackArm,
        AltAttack,
        SpAttack,
        SpAttackCharge,
        SpAttackShoot,
        Hurt,
        Sleep
    }

    internal class SpriteSheet : ICacheable
    {
        private FrameData frameData;
        private readonly int num;
        private int sizeInBytes;
        private readonly string form;
        private Dictionary<FrameType, Dictionary<Enums.Direction, Surface>> animations;

        public SpriteSheet(int num, string form)
        {
            this.num = num;
            this.form = form;

            this.frameData = new FrameData();
        }

        /// <inheritdoc/>
        public int BytesUsed
        {
            get { return this.sizeInBytes; }
        }

        public FrameData FrameData
        {
            get { return this.frameData; }
        }

        public int Num
        {
            get { return this.num; }
        }

        public string Form
        {
            get { return this.form; }
        }

        public Rectangle GetFrameBounds(FrameType frameType, Enums.Direction direction, int frameNum)
        {
            Rectangle rec = default(Rectangle);
            rec.X = frameNum * this.frameData.FrameWidth;
            rec.Y = 0;
            rec.Width = this.frameData.FrameWidth;
            rec.Height = this.frameData.FrameHeight;

            return rec;
        }

        public void LoadFromData(BinaryReader reader, int totalByteSize)
        {
            this.frameData = new FrameData();
            this.animations = new Dictionary<FrameType, Dictionary<Enums.Direction, Surface>>();

            foreach (FrameType frameType in Enum.GetValues(typeof(FrameType)))
            {
                if (FrameTypeHelper.IsFrameTypeDirectionless(frameType) == false)
                {
                    for (int i = 0; i < 8; i++)
                    {
                        Enums.Direction dir = GraphicsManager.GetAnimIntDir(i);
                        int frameCount = reader.ReadInt32();
                        this.frameData.SetFrameCount(frameType, dir, frameCount);
                        int size = reader.ReadInt32();
                        if (size > 0)
                        {
                            byte[] imgData = reader.ReadBytes(size);
                            using (MemoryStream stream = new MemoryStream(imgData))
                            {
                                Bitmap bitmap = (Bitmap)Image.FromStream(stream);
                                Surface sheetSurface = new Surface(bitmap);
                                sheetSurface.Transparent = true;

                                this.AddSheet(frameType, dir, sheetSurface);

                                this.frameData.SetFrameSize(sheetSurface.Width, sheetSurface.Height, frameCount);
                            }
                        }
                    }
                }
else
                {
                    int frameCount = reader.ReadInt32();
                    this.frameData.SetFrameCount(frameType, Enums.Direction.Down, frameCount);
                    int size = reader.ReadInt32();
                    if (size > 0)
                    {
                        byte[] imgData = reader.ReadBytes(size);

                        using (MemoryStream stream = new MemoryStream(imgData))
                        {
                            Bitmap bitmap = (Bitmap)Image.FromStream(stream);
                            Surface sheetSurface = new Surface(bitmap);
                            sheetSurface.Transparent = true;

                            this.AddSheet(frameType, Enums.Direction.Down, sheetSurface);

                            this.frameData.SetFrameSize(sheetSurface.Width, sheetSurface.Height, frameCount);
                        }
                    }
                }
            }

            this.sizeInBytes = totalByteSize;
        }

        public Surface GetSheet(FrameType type, Enums.Direction dir)
        {
            if (FrameTypeHelper.IsFrameTypeDirectionless(type))
            {
                dir = Enums.Direction.Down;
            }

            return this.animations[type][dir];
        }

        public void AddSheet(FrameType type, Enums.Direction dir, Surface surface)
        {
            if (!this.animations.ContainsKey(type))
            {
                this.animations.Add(type, new Dictionary<Enums.Direction, Surface>());
            }

            if (this.animations[type].ContainsKey(dir) == false)
            {
                this.animations[type].Add(dir, surface);
            }
else
            {
                this.animations[type][dir] = surface;
            }
        }
    }
}