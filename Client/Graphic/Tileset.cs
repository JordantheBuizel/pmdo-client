﻿// <copyright file="Tileset.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using PMU.Core;
    using SdlDotNet.Graphics;

    internal class Tileset : LRUCache<int, TileGraphic>
    {
        private int tileCount;
        private readonly int tileSetNumber;
        private Size size;
        private string filePath;
        private object lockObject = new object();

        private long headerSize;
        private long[] tilePositionCache;
        private int[] tileSizeCache;

        public Size Size
        {
            get { return this.size; }
        }

        public int TilesetNumber
        {
            get { return this.tileSetNumber; }
        }

        public int TileCount
        {
            get { return this.tileCount; }
        }

        public Tileset(int tileSetNumber, int maxCacheSize)
            : base(maxCacheSize)
            {
            this.tileSetNumber = tileSetNumber;
            this.size = default(Size);
        }

        public long GetTilePosition(int index)
        {
            return this.tilePositionCache[index] + this.headerSize;
        }

        public void Load(string filePath)
        {
            this.filePath = filePath;

            // File format:
            // [tileset-width(4)][tileset-height(4)][tile-count(4)]
            // [tileposition-1(4)][tilesize-1(4)][tileposition-2(4)][tilesize-2(4)][tileposition-n(n*4)][tilesize-n(n*4)]
            // [tile-1(variable)][tile-2(variable)][tile-n(variable)]
            using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    // Read tileset width
                    this.size.Width = reader.ReadInt32();

                    // Read tileset height
                    this.size.Height = reader.ReadInt32();

                    this.tileCount = (this.size.Width / Constants.TILEWIDTH) * (this.size.Height / Constants.TILEHEIGHT);

                    // Prepare tile information cache
                    this.tilePositionCache = new long[this.tileCount];
                    this.tileSizeCache = new int[this.tileCount];

                    // Load tile information
                    for (int i = 0; i < this.tileCount; i++)
                    {
                        // Read tile position data
                        this.tilePositionCache[i] = reader.ReadInt64();

                        // Read tile size data
                        this.tileSizeCache[i] = reader.ReadInt32();
                    }

                    this.headerSize = stream.Position;
                }
            }

            this.SetupInitialDataFromTile(0);
        }

        private void SetupInitialDataFromTile(int tileNumber)
        {
            byte[] tileBytes = new byte[this.tileSizeCache[tileNumber]];
            using (FileStream stream = new FileStream(this.filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                // Seek to the location of the tile
                stream.Seek(this.GetTilePosition(tileNumber), SeekOrigin.Begin);
                stream.Read(tileBytes, 0, tileBytes.Length);
            }

            using (MemoryStream stream = new MemoryStream(tileBytes))
            {
                Bitmap bitmap = (Bitmap)Image.FromStream(stream);
                Surface tileSurface = new Surface(bitmap);

                tileSurface.Transparent = true;
                int rawSurfaceSize = tileSurface.Width * tileSurface.Height * tileSurface.BitsPerPixel / 8;
                TileGraphic tileGraphic = new TileGraphic(this.tileSetNumber, tileNumber, tileSurface, rawSurfaceSize);

                // base.Add(tileNumber, tileGraphic);
                this.Add(tileNumber, tileGraphic);
            }
        }

        public TileGraphic GetTileGraphic(int tileNumber)
        {
            if (tileNumber > -1 && tileNumber < this.tileCount)
            {
                TileGraphic graphic = this.Get(tileNumber);
                if (graphic != null)
                {
                    return graphic;
                }
else
                {
                    byte[] tileBytes = new byte[this.tileSizeCache[tileNumber]];
                    using (FileStream stream = new FileStream(this.filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        // Seek to the location of the tile
                        stream.Seek(this.GetTilePosition(tileNumber), SeekOrigin.Begin);
                        stream.Read(tileBytes, 0, tileBytes.Length);
                    }

                    using (MemoryStream stream = new MemoryStream(tileBytes))
                    {
                        Bitmap bitmap = (Bitmap)Image.FromStream(stream);
                        Surface tileSurface = new Surface(bitmap);
                        tileSurface.Transparent = true;
                        int rawSurfaceSize = tileSurface.Width * tileSurface.Height * tileSurface.BitsPerPixel / 8;
                        TileGraphic tileGraphic = new TileGraphic(this.tileSetNumber, tileNumber, tileSurface, rawSurfaceSize);

                        // base.Add(tileNumber, tileGraphic);
                        this.Add(tileNumber, tileGraphic);

                        // tiles[tileNumber] = tileGraphic;
                        return tileGraphic;
                    }
                }
            }
else
            {
                return this.GetTileGraphic(0);
            }
        }

        public Surface this[int tileNumber]
        {
            get { return this.GetTileGraphic(tileNumber).Tile; }
        }
    }
}
