﻿// <copyright file="RenderLocation.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    using SdlDotNet.Graphics;

    internal class RenderLocation
    {
        public RenderLocation(RendererDestinationData destinationData, int x, int y)
        {
            this.DestinationSurface = destinationData;
            this.DestinationPoint = new Point(x, y);
        }

        public RenderLocation(RendererDestinationData destinationData, Point destinationPoint)
        {
            this.DestinationSurface = destinationData;
            this.DestinationPoint = destinationPoint;
        }

        public Point DestinationPoint
        {
            get;
            set;
        }

        public RendererDestinationData DestinationSurface
        {
            get;
            set;
        }
    }
}