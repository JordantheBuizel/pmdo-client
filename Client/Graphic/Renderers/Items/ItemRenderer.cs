﻿// <copyright file="ItemRenderer.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Items
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Screen;
    using Logic.Items;
    using Logic.Maps;
    using SdlDotNet.Graphics;

    internal class ItemRenderer
    {
        public static void DrawMapItem(RendererDestinationData destData, Map map, Enums.MapID targetMapID, int itemSlot)
        {
            Item item = ItemHelper.Items[map.MapItems[itemSlot].Num];

            Rectangle cropRect = new Rectangle(
                (item.Pic - ((item.Pic / 6) * 6)) * Constants.TILEWIDTH,
                                               (item.Pic / 6) * Constants.TILEHEIGHT, Constants.TILEWIDTH, Constants.TILEHEIGHT);

            int itemX = map.MapItems[itemSlot].X;
            int itemY = map.MapItems[itemSlot].Y;

            Maps.SeamlessWorldHelper.ConvertCoordinatesToBorderless(map, targetMapID, ref itemX, ref itemY);
            Point dstPoint = new Point(
                ScreenRenderer.ToScreenX(itemX * Constants.TILEWIDTH),
                                       ScreenRenderer.ToScreenY(itemY * Constants.TILEHEIGHT));

            // Surface itemSurface = new Surface(32,32);
            // itemSurface.Blit(Graphic.GraphicsManager.Items, cropRect);

            // if (darkness != null && !darkness.Disposed) {
            //    Point darknessPoint = new Point(darkness.Buffer.Width / 2 + dstPoint.X - darkness.Focus.X, darkness.Buffer.Height / 2 + dstPoint.Y - darkness.Focus.Y);
            //    Surface darknessSurface = new Surface(32, 32);
            //    darknessSurface.Blit(darkness.Buffer, new Point(0, 0), new Rectangle(darknessPoint, new Size(Constants.TILE_WIDTH, Constants.TILE_HEIGHT)));

            // }
            // destData.Blit(itemSurface, dstPoint);
            destData.Blit(GraphicsManager.Items, dstPoint, cropRect);
        }
    }
}
