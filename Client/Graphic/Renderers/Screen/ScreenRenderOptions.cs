﻿// <copyright file="ScreenRenderOptions.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Screen
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Logic.Maps;
    using SdlDotNet.Graphics;

    internal class ScreenRenderOptions
    {
        public bool MinimapVisible { get; set; }

        public Map Map { get; set; }

        public bool DisplayAnimation { get; set; }

        public bool DisplayAttributes { get; set; }

        public bool DisplayDungeonValues { get; set; }

        public bool DisplayMapGrid { get; set; }

        public bool DisplayLocation { get; set; }

        public int RecentPing { get; set; }

        public int RecentFPS { get; set; }

        public int RecentRenders { get; set; }

        public bool ScreenVisible { get; set; }

        public bool PlayersVisible { get; set; }

        public bool NpcsVisible { get; set; }

        public Effects.Overlays.IOverlay Overlay { get; set; }

        public Effects.Overlays.DarknessOverlay Darkness { get; set; }

        public Effects.Weather.IWeather Weather { get; set; }

        public Effects.Overlays.IOverlay ScreenOverlay { get; set; }

        private readonly List<Stories.Components.ScreenImageOverlay> screenImageOverlays;

        public Surface StoryBackground { get; set; }

        public List<Stories.Components.ScreenImageOverlay> ScreenImageOverlays
        {
            get { return this.screenImageOverlays; }
        }

        public ScreenRenderOptions()
        {
            this.ScreenVisible = true;
            this.screenImageOverlays = new List<Stories.Components.ScreenImageOverlay>();
            this.MinimapVisible = false;
        }

        public void SetWeather(Enums.Weather weather)
        {
            // weather = Enums.Weather.Raining;
            if (this.Weather != null)
            {
                if (this.Weather.ID != weather)
                {
                    this.Weather.FreeResources();
                }
else
                {
                    // Same weather as the active one, don't do anything
                    return;
                }
            }

            switch (weather)
            {
                case Enums.Weather.Ambiguous:
                case Enums.Weather.None:
                {
                        this.Weather = null;
                    }

                    break;
                case Enums.Weather.Raining:
                {
                        this.Weather = new Effects.Weather.Rain();
                    }

                    break;
                case Enums.Weather.Snowing:
                {
                        this.Weather = new Effects.Weather.Snow();
                    }

                    break;
                case Enums.Weather.Thunder:
                {
                        this.Weather = new Effects.Weather.Thunder();
                    }

                    break;
                case Enums.Weather.Hail:
                {
                        this.Weather = new Effects.Weather.Hail();
                    }

                    break;
                case Enums.Weather.DiamondDust:
                {
                        this.Weather = new Effects.Weather.DiamondDust();
                    }

                    break;
                case Enums.Weather.Cloudy:
                {
                        this.Weather = new Effects.Weather.Cloudy();
                    }

                    break;
                case Enums.Weather.Fog:
                {
                        this.Weather = new Effects.Weather.Fog();
                    }

                    break;
                case Enums.Weather.Sunny:
                {
                        this.Weather = new Effects.Weather.Sunny();
                    }

                    break;
                case Enums.Weather.Sandstorm:
                {
                        this.Weather = new Effects.Weather.Sandstorm();
                    }

                    break;
                case Enums.Weather.Snowstorm:
                {
                        this.Weather = new Effects.Weather.Snowstorm();
                    }

                    break;
                case Enums.Weather.Ashfall:
                {
                        this.Weather = new Effects.Weather.Ashfall();
                    }

                    break;
                    default:
                    {
                    }

                    break;
            }
        }

        public void SetOverlay(Enums.Overlay overlay)
        {
            // overlay = Enums.Overlay.None;
            if (this.Overlay != null)
            {
                this.Overlay.FreeResources();
            }

            switch (overlay)
            {
                case Enums.Overlay.Night:
                {
                        this.Overlay = new Effects.Overlays.NightOverlay();
                    }

                    break;
                case Enums.Overlay.Dawn:
                    {
                        this.Overlay = new Effects.Overlays.DawnOverlay();
                    }

                    break;
                case Enums.Overlay.Dusk:
                    {
                        this.Overlay = new Effects.Overlays.DuskOverlay();
                    }

                    break;
                default:
                    if (MapHelper.ActiveMap.Indoors == false)
                    {
                        switch (Globals.GameTime)
                        {
                            case Enums.Time.Dawn:
                            {
                                    this.SetOverlay(Enums.Overlay.Dawn);
                                }

                                break;
                            case Enums.Time.Dusk:
                                {
                                    this.SetOverlay(Enums.Overlay.Dusk);
                                }

                                break;
                            case Enums.Time.Night:
                                {
                                    this.SetOverlay(Enums.Overlay.Night);
                                }

                                break;
                            default:
                                {
                                    this.Overlay = null;
                                }

                                break;
                        }
                    }
else
                    {
                        this.Overlay = null;
                    }

                    break;
            }
        }

        public void SetDarkness(int range)
        {
            if (range > -1)
            {
                this.Darkness = new Effects.Overlays.DarknessOverlay(range);
            }
else
            {
                this.Darkness = null;
            }
        }
    }
}
