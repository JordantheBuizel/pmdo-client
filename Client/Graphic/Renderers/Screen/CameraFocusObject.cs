﻿// <copyright file="CameraFocusObject.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Screen
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class CameraFocusObject
    {
        public int FocusedX
        {
            get;
            set;
        }

        public int FocusedXOffset
        {
            get;
            set;
        }

        public int FocusedY
        {
            get;
            set;
        }

        public int FocusedYOffset
        {
            get;
            set;
        }

        public Enums.Direction FocusedDirection
        {
            get;
            set;
        }

        public void Process(int tick)
        {
        }
    }
}
