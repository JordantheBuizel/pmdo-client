﻿// <copyright file="Camera.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Screen
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    internal class Camera
    {
        public CameraFocusObject FocusObject
        {
            get;
            set;
        }

        public int FocusedX
        {
            get;
            set;
        }

        public int FocusedXOffset
        {
            get;
            set;
        }

        public int FocusedY
        {
            get;
            set;
        }

        public int FocusedYOffset
        {
            get;
            set;
        }

        public Enums.Direction FocusedDirection
        {
            get;
            set;
        }

        public int X
        {
            get;
            set;
        }

        public int X2
        {
            get;
            set;
        }

        public int Y
        {
            get;
            set;
        }

        public int Y2
        {
            get;
            set;
        }

        public void FocusOnSprite(Sprites.ISprite sprite)
        {
            this.FocusedX = sprite.X;
            this.FocusedY = sprite.Y;
            this.FocusedXOffset = sprite.Offset.X;
            this.FocusedYOffset = sprite.Offset.Y;
            this.FocusedDirection = sprite.Direction;
        }

        public void FocusOnFocusObject(CameraFocusObject focusObject)
        {
            this.FocusedX = focusObject.FocusedX;
            this.FocusedY = focusObject.FocusedX;
            this.FocusedXOffset = focusObject.FocusedXOffset;
            this.FocusedYOffset = focusObject.FocusedYOffset;
            this.FocusedDirection = focusObject.FocusedDirection;

            this.FocusObject = focusObject;
        }
    }
}