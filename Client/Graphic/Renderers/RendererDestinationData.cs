﻿// <copyright file="RendererDestinationData.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Widges.Interfaces;
    using SdlDotNet.Graphics;

    internal class RendererDestinationData
    {
        private const bool BUFFERLESS = false;

        private IViewLens control;

        public IViewLens Control
        {
            get { return this.control; }
            set { this.control = value; }
        }

        private readonly Bitmap bitmap = null;

        private Point location = new Point(0, 0);
        private Size size;

        /// <summary>
        /// Initializes a new instance of the <see cref="RendererDestinationData"/> class.
        /// Used to render Images to controls
        /// </summary>
        /// <param name="control">Windows Forms Control that implements <see cref="IViewLens"/></param>
        public RendererDestinationData(IViewLens control)
        {
            this.control = control;
            this.location = new Point(0, 0);
            this.size = control.Size;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RendererDestinationData"/> class.
        /// Used to draw directly to input <see cref="Bitmap"/>.
        /// </summary>
        /// <param name="bitmap"><see cref="Bitmap"/> to draw to.</param>
        public RendererDestinationData(Bitmap bitmap)
        {
            this.bitmap = bitmap;
            this.location = new Point(0, 0);
            this.size = bitmap.Size;
        }

        public Size Size
        {
            get { return this.size; }
            set { this.size = value; }
        }

        public Point Location
        {
            get { return this.location; }
            set { this.location = value; }
        }

        public void Blit(Image image, Point location)
        {
            if (this.bitmap != null)
            {
                using (Graphics g = Graphics.FromImage(this.bitmap))
                {
                    g.DrawImage(image, location);
                }
            }
            else
            {
                using (Bitmap bmp = new Bitmap(this.Size.Width, this.Size.Height))
                {
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        g.DrawImage(image, location);
                    }

                    this.Control.ViewLens = new Bitmap(bmp);
                }
            }
        }

        public void Blit(Image image, Point location, Rectangle sourceRectangle)
        {
            if (this.bitmap != null)
            {
                using (Graphics g = Graphics.FromImage(this.bitmap))
                {
                    g.DrawImage(image, location.X, location.Y, sourceRectangle, GraphicsUnit.Pixel);
                }
            }
            else
            {
                using (Bitmap bmp = new Bitmap(this.Size.Width, this.Size.Height))
                {
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        g.DrawImage(image, location.X, location.Y, sourceRectangle, GraphicsUnit.Pixel);
                    }

                    this.Control.ViewLens = new Bitmap(bmp);
                }
            }
        }

        public void Draw(IPrimitive primitive, Color color, bool antiAlias, bool fill)
        {
            this.Surface.Draw(primitive, color, antiAlias, fill);
        }
    }
}
