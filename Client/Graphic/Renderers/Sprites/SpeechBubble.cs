﻿// <copyright file="SpeechBubble.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Sprites
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Graphics;
    using SdlDotNet.Widgets;

    internal class SpeechBubble
    {
        private int bubbleDisplayStart;
        private string bubbleText;
        private Surface buffer;
        private SdlDotNet.Graphics.Font font;
        private bool markedForRemoval;

        public SpeechBubble()
        {
        }

        public bool RedrawRequested
        {
            get;
            set;
        }

        public int BubbleDisplayStart
        {
            get { return this.bubbleDisplayStart; }
            set { this.bubbleDisplayStart = value; }
        }

        public Surface Buffer
        {
            get { return this.buffer; }
        }

        public bool MarkedForRemoval
        {
            get { return this.markedForRemoval; }
        }

        public void DrawBuffer()
        {
            Surface textSurf;
            Size textSize = TextRenderer.SizeText2(this.font, this.bubbleText, false, 0);
            CharRenderOptions[] renderOptions = new CharRenderOptions[this.bubbleText.Length];
            for (int i = 0; i < renderOptions.Length; i++)
            {
                renderOptions[i] = new CharRenderOptions(Color.WhiteSmoke);
            }

            renderOptions = Network.MessageProcessor.ParseText(renderOptions, ref this.bubbleText);
            if (textSize.Width > 300)
            {
                textSurf = TextRenderer.RenderTextBasic2(this.font, this.bubbleText, renderOptions, Color.WhiteSmoke, false, 300, 0, 0, 0);
            }
else
            {
                textSurf = TextRenderer.RenderTextBasic2(this.font, this.bubbleText, renderOptions, Color.WhiteSmoke, false, 0, 0, 0, 0);
            }

            int tilesWidth = Math.Max(textSurf.Width / Constants.TILEWIDTH, 2);
            int tilesHeight = Math.Max(textSurf.Height / Constants.TILEHEIGHT, 2);

            if (textSurf.Width > tilesWidth * Constants.TILEWIDTH)
            {
                tilesWidth++;
            }

            if (textSurf.Height > tilesHeight * Constants.TILEHEIGHT * 0.7)
            {
                tilesHeight++;
            }

            if (this.buffer != null)
            {
                this.buffer.Close();
            }

            this.buffer = new Surface(new Size(tilesWidth * Constants.TILEWIDTH, tilesHeight * Constants.TILEHEIGHT));
            this.buffer.Fill(Color.Transparent);
            this.buffer.TransparentColor = Color.Transparent;
            this.buffer.Transparent = true;
            for (int i = 0; i < tilesHeight; i++)
            {
                if (i == 0)
                {
                    Maps.MapRenderer.DrawTileToSurface(this.buffer, 10, 1, 0, 0, 0, false);
                    for (int n = 0; n < tilesWidth - 2; n++)
                    {
                        Maps.MapRenderer.DrawTileToSurface(this.buffer, 10, 16, n + 1, 0, 0, false);
                    }

                    Maps.MapRenderer.DrawTileToSurface(this.buffer, 10, 2, tilesWidth - 1, 0, 0, false);
                }
                else if (i == tilesHeight - 1)
                {
                    Maps.MapRenderer.DrawTileToSurface(this.buffer, 10, 3, 0, tilesHeight - 1, 0, false);
                    for (int n = 0; n < tilesWidth - 2; n++)
                    {
                        Maps.MapRenderer.DrawTileToSurface(this.buffer, 10, 15, n + 1, tilesHeight - 1, 0, false);
                    }

                    Maps.MapRenderer.DrawTileToSurface(this.buffer, 10, 4, tilesWidth - 1, tilesHeight - 1, 0, false);
                }
else
                {
                    Maps.MapRenderer.DrawTileToSurface(this.buffer, 10, 18, 0, i, 0, false);
                    for (int n = 0; n < tilesWidth - 2; n++)
                    {
                        Maps.MapRenderer.DrawTileToSurface(this.buffer, 10, 5, n + 1, i, 0, false);
                    }

                    Maps.MapRenderer.DrawTileToSurface(this.buffer, 10, 17, tilesWidth - 1, i, 0, false);
                }
            }

            this.buffer = this.buffer.CreateScaledSurface(1, 0.7);
            this.buffer.Transparent = true;
            this.buffer.Blit(textSurf, new Point(Graphic.DrawingSupport.GetCenterX(this.buffer.Width, textSurf.Width), 5));
            textSurf.Close();
            this.RedrawRequested = false;
        }

        public void FreeResources()
        {
            if (this.buffer != null)
            {
                this.buffer.Close();
                this.buffer = null;
            }

            if (this.font != null)
            {
                this.font.Close();
                this.font = null;
            }
        }

        public void Process(int tick)
        {
            if (tick > this.bubbleDisplayStart + 2000)
            {
                this.markedForRemoval = true;
            }
        }

        public void SetBubbleText(string text)
        {
            this.CheckFont();
            this.bubbleText = text;
            this.RedrawRequested = true;

            // DrawBuffer();
        }

        private void CheckFont()
        {
            if (this.font == null)
            {
                this.font = FontManager.LoadFont("PMU", 23);
            }
        }
    }
}