﻿// <copyright file="Emoticon.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Sprites
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class Emoticon
    {
        public int EmoteIndex { get; set; }

        public int EmoteSpeed { get; set; }

        public int EmoteTime { get; set; }

        public int EmoteFrame { get; set; }

        public int EmoteCycles { get; set; }

        public int CurrentCycle { get; set; }

        public Emoticon(int index, int speed, int cycles)
        {
            this.EmoteIndex = index;
            this.EmoteSpeed = speed;
            this.EmoteCycles = cycles;
        }
    }
}
