﻿// <copyright file="ArrowMoveAnimation.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Moves
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    internal class ArrowMoveAnimation : IMoveAnimation
    {
        public ArrowMoveAnimation(int X1, int Y1, Enums.Direction dir, int distance)
        {
            this.StartX = X1;
            this.StartY = Y1;
            this.Direction = dir;
            this.Distance = distance;
            this.TotalMoveTime = Globals.Tick;
        }

        /// <inheritdoc/>
        public bool Active
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int AnimationIndex
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int CompletedLoops
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Frame
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int FrameLength
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int MoveTime
        {
            get;
            set;
        }

        public int TotalMoveTime
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int RenderLoops
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.MoveAnimationType AnimType
        {
            get { return Enums.MoveAnimationType.Arrow; }
        }

        /// <inheritdoc/>
        public int StartX
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int StartY
        {
            get;
            set;
        }

        public int Distance
        {
            get;
            set;
        }

        public Enums.Direction Direction
        {
            get;
            set;
        }
    }
}