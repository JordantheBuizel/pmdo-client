﻿// <copyright file="ItemThrowMoveAnimation.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Moves
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    internal class ItemThrowMoveAnimation : IMoveAnimation
    {
        public ItemThrowMoveAnimation(int X1, int Y1, int DX, int DY)
        {
            this.StartX = X1;
            this.StartY = Y1;
            this.XChange = DX;
            this.YChange = DY;
            this.TotalMoveTime = Globals.Tick;
        }

        /// <inheritdoc/>
        public bool Active
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int AnimationIndex
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int CompletedLoops
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Frame
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int FrameLength
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int MoveTime
        {
            get;
            set;
        }

        public int TotalMoveTime
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int RenderLoops
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.MoveAnimationType AnimType
        {
            get { return Enums.MoveAnimationType.ItemThrow; }
        }

        /// <inheritdoc/>
        public int StartX
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int StartY
        {
            get;
            set;
        }

        public int XChange
        {
            get;
            set;
        }

        public int YChange
        {
            get;
            set;
        }
    }
}