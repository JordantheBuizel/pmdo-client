﻿// <copyright file="MoveRenderer.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Moves
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    using SdlDotNet.Graphics;

    internal class MoveRenderer
    {
        private static List<IMoveAnimation> activeAnimations;
        private static Surface srfcMoveTargetTile;
        private static Surface srfcMoveTargetTileHit;
        private static Surface srfcMoveTargetTileDark;
        private static Surface srfcMoveTargetUnknown;

        public static List<IMoveAnimation> ActiveAnimations
        {
            get { return activeAnimations; }
        }

        public static void Initialize()
        {
            activeAnimations = new List<IMoveAnimation>();
            srfcMoveTargetTile = new Surface(Constants.TILEWIDTH, Constants.TILEHEIGHT);
            srfcMoveTargetTile.Blit(GraphicsManager.Tiles[10][77], new Point(0, 0));
            srfcMoveTargetTile.Transparent = true;

            // srfcMoveTargetTile.Alpha = 150;
            // srfcMoveTargetTile.AlphaBlending = true;
            srfcMoveTargetTileHit = new Surface(Constants.TILEWIDTH, Constants.TILEHEIGHT);
            srfcMoveTargetTileHit.Blit(GraphicsManager.Tiles[10][91], new Point(0,0));
            srfcMoveTargetTileHit.Transparent = true;

            // srfcMoveTargetTileHit.Alpha = 150;
            // srfcMoveTargetTileHit.AlphaBlending = true;
            srfcMoveTargetTileDark = new Surface(Constants.TILEWIDTH, Constants.TILEHEIGHT);
            srfcMoveTargetTileDark.Blit(GraphicsManager.Tiles[10][105], new Point(0, 0));
            srfcMoveTargetTileDark.Transparent = true;

            srfcMoveTargetUnknown = new Surface(Constants.TILEWIDTH * 3, Constants.TILEHEIGHT * 3);
            for (int i = 0; i < 9; i++)
            {
                srfcMoveTargetUnknown.Blit(GraphicsManager.Tiles[10][8 + (i % 3) + (i / 3 * 14)], new Point((i % 3) * Constants.TILEWIDTH, i / 3 * Constants.TILEHEIGHT));
            }

            srfcMoveTargetUnknown.Transparent = true;
            srfcMoveTargetUnknown.Alpha = 150;
            srfcMoveTargetUnknown.AlphaBlending = true;
        }

        public static void RenderMoveAnimation(RendererDestinationData destData, IMoveAnimation animation, Point pinnedPoint)
        {
            int animTime = 400;
            switch (animation.AnimType)
            {
                case Enums.MoveAnimationType.Normal:
                {
                        NormalMoveAnimation specifiedAnim = animation as NormalMoveAnimation;
                        if (specifiedAnim.CompletedLoops < specifiedAnim.RenderLoops)
                        {
                            SpellSheet spriteSheet = GraphicsManager.GetSpellSheet(Enums.StationaryAnimType.Spell, specifiedAnim.AnimationIndex, false);
                            Surface spriteToBlit = null;
                            if (spriteSheet != null)
                            {
                                spriteToBlit = spriteSheet.Sheet;
                            }
else
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                specifiedAnim.Frame * spriteToBlit.Height,
                                0, spriteToBlit.Height, spriteToBlit.Height);

                            pinnedPoint.X = pinnedPoint.X + (Constants.TILEWIDTH / 2) - (spriteToBlit.Height / 2);
                            pinnedPoint.Y = pinnedPoint.Y + (Constants.TILEHEIGHT / 2) - (spriteToBlit.Height / 2);

                            // blit
                            destData.Blit(spriteToBlit, pinnedPoint, sourceRec);

                            if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength)
                            {
                                specifiedAnim.MoveTime = Globals.Tick;
                                specifiedAnim.Frame++;
                            }

                            if (specifiedAnim.Frame >= spriteToBlit.Width / spriteToBlit.Height)
                            {
                                specifiedAnim.CompletedLoops++;
                                specifiedAnim.Frame = 0;
                            }
                        }
else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.Arrow:
                {
                        ArrowMoveAnimation specifiedAnim = animation as ArrowMoveAnimation;
                        int time = Globals.Tick - specifiedAnim.TotalMoveTime;
                        if (time < animTime)
                        {
                            SpellSheet spriteSheet = GraphicsManager.GetSpellSheet(Enums.StationaryAnimType.Arrow, specifiedAnim.AnimationIndex, false);
                            Surface spriteToBlit = null;
                            if (spriteSheet != null)
                            {
                                spriteToBlit = spriteSheet.Sheet;
                            }
else
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                specifiedAnim.Frame * spriteToBlit.Height / 8,
                                GraphicsManager.GetAnimDirInt(specifiedAnim.Direction) * spriteToBlit.Height / 8, spriteToBlit.Height / 8, spriteToBlit.Height / 8);

                            pinnedPoint.X = pinnedPoint.X + (Constants.TILEWIDTH / 2) - (spriteToBlit.Height / 2 / 8);
                            pinnedPoint.Y = pinnedPoint.Y + (Constants.TILEHEIGHT / 2) - (spriteToBlit.Height / 2 / 8);

                            switch (specifiedAnim.Direction)
                            {
                                case Enums.Direction.Up:
                                {
                                        pinnedPoint.Y -= specifiedAnim.Distance * Constants.TILEHEIGHT * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.Down:
                                {
                                        pinnedPoint.Y += specifiedAnim.Distance * Constants.TILEHEIGHT * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.Left:
                                {
                                        pinnedPoint.X -= specifiedAnim.Distance * Constants.TILEWIDTH * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.Right:
                                {
                                        pinnedPoint.X += specifiedAnim.Distance * Constants.TILEWIDTH * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.UpRight:
                                case Enums.Direction.DownRight:
                                case Enums.Direction.DownLeft:
                                case Enums.Direction.UpLeft:
                                    break;
                            }

                            // blit
                            destData.Blit(spriteToBlit, pinnedPoint, sourceRec);

                            if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength)
                            {
                                specifiedAnim.MoveTime = Globals.Tick;
                                specifiedAnim.Frame++;
                            }

                            if (specifiedAnim.Frame >= spriteToBlit.Width / (spriteToBlit.Height / 8))
                            {
                                specifiedAnim.CompletedLoops++;
                                specifiedAnim.Frame = 0;
                            }
                        }
else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.Throw:
                {
                        ThrowMoveAnimation specifiedAnim = animation as ThrowMoveAnimation;
                        int time = Globals.Tick - specifiedAnim.TotalMoveTime;
                        if (time < animTime)
                        {
                            SpellSheet spriteSheet = GraphicsManager.GetSpellSheet(Enums.StationaryAnimType.Spell, specifiedAnim.AnimationIndex, false);
                            Surface spriteToBlit = null;
                            if (spriteSheet != null)
                            {
                                spriteToBlit = spriteSheet.Sheet;
                            }
else
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                specifiedAnim.Frame * spriteToBlit.Height,
                                0, spriteToBlit.Height, spriteToBlit.Height);

                            double distance = Math.Sqrt(Math.Pow(specifiedAnim.XChange * Constants.TILEWIDTH, 2) + Math.Pow(specifiedAnim.YChange * Constants.TILEHEIGHT, 2));

                            int x = pinnedPoint.X + (specifiedAnim.XChange * Constants.TILEWIDTH * time / animTime);
                            int y = (int)(pinnedPoint.Y + (specifiedAnim.YChange * Constants.TILEHEIGHT * time / animTime) - ((((-4) * distance * Math.Pow(time, 2) / animTime) + (4 * distance * time)) / animTime));

                            x = x + (Constants.TILEWIDTH / 2) - (spriteToBlit.Height / 2);
                            y = y + (Constants.TILEHEIGHT / 2) - (spriteToBlit.Height / 2);

                            // blit
                            destData.Blit(spriteToBlit, new Point(x, y), sourceRec);

                            if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength)
                            {
                                specifiedAnim.MoveTime = Globals.Tick;
                                specifiedAnim.Frame++;
                            }

                            if (specifiedAnim.Frame >= spriteToBlit.Width / spriteToBlit.Height)
                            {
                                specifiedAnim.CompletedLoops++;
                                specifiedAnim.Frame = 0;
                            }
                        }
else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.Beam:
                {
                        BeamMoveAnimation specifiedAnim = animation as BeamMoveAnimation;
                        if (specifiedAnim.CompletedLoops < specifiedAnim.RenderLoops + specifiedAnim.Distance)
                        {
                            SpellSheet spriteSheet = GraphicsManager.GetSpellSheet(Enums.StationaryAnimType.Beam, specifiedAnim.AnimationIndex, false);
                            Surface spriteToBlit = null;
                            if (spriteSheet != null)
                            {
                                spriteToBlit = spriteSheet.Sheet;
                            }
else
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            int curDistance = specifiedAnim.Distance;
                            Rectangle sourceRec = default(Rectangle);
                            if (specifiedAnim.CompletedLoops < specifiedAnim.Distance)
                            {
                                curDistance = specifiedAnim.CompletedLoops;
                            }

                            for (int i = 0; i <= curDistance; i++)
                            {
                                if (i == 0)
                                {
                                    // draw beginning
                                    sourceRec = new Rectangle(
                                        specifiedAnim.Frame * spriteToBlit.Height / 32,
                                GraphicsManager.GetAnimDirInt(specifiedAnim.Direction) * 4 * spriteToBlit.Height / 32, spriteToBlit.Height / 32, spriteToBlit.Height / 32);
                                }
                                else if (i == curDistance)
                                {
                                    if (curDistance == specifiedAnim.Distance)
                                    {
                                        sourceRec = new Rectangle(
                                            specifiedAnim.Frame * spriteToBlit.Height / 32,
                                ((GraphicsManager.GetAnimDirInt(specifiedAnim.Direction) * 4) + 3) * spriteToBlit.Height / 32, spriteToBlit.Height / 32, spriteToBlit.Height / 32);
                                    }
else
                                    {
                                        sourceRec = new Rectangle(
                                            specifiedAnim.Frame * spriteToBlit.Height / 32,
                                ((GraphicsManager.GetAnimDirInt(specifiedAnim.Direction) * 4) + 2) * spriteToBlit.Height / 32, spriteToBlit.Height / 32, spriteToBlit.Height / 32);
                                    }
                                }
else
                                {
                                    // draw body
                                    sourceRec = new Rectangle(
                                        specifiedAnim.Frame * spriteToBlit.Height / 32,
                                ((GraphicsManager.GetAnimDirInt(specifiedAnim.Direction) * 4) + 1) * spriteToBlit.Height / 32, spriteToBlit.Height / 32, spriteToBlit.Height / 32);
                                }

                                Point blitPoint = default(Point);

                                switch (specifiedAnim.Direction)
                                {
                                    case Enums.Direction.Up:
                                    {
                                        blitPoint = new Point(pinnedPoint.X, pinnedPoint.Y - (i * Constants.TILEHEIGHT));
                                        }

                                        break;
                                    case Enums.Direction.Down:
                                    {
                                        blitPoint = new Point(pinnedPoint.X, pinnedPoint.Y + (i * Constants.TILEHEIGHT));
                                        }

                                        break;
                                    case Enums.Direction.Left:
                                    {
                                        blitPoint = new Point(pinnedPoint.X - (i * Constants.TILEWIDTH), pinnedPoint.Y);
                                        }

                                        break;
                                    case Enums.Direction.Right:
                                    {
                                        blitPoint = new Point(pinnedPoint.X + (i * Constants.TILEWIDTH), pinnedPoint.Y);
                                        }

                                        break;
                                    case Enums.Direction.UpRight:
                                    case Enums.Direction.DownRight:
                                    case Enums.Direction.DownLeft:
                                    case Enums.Direction.UpLeft:
                                        break;
                                }

                                blitPoint.X = blitPoint.X + (Constants.TILEWIDTH / 2) - (spriteToBlit.Height / 2 / 32);
                                blitPoint.Y = blitPoint.Y + (Constants.TILEHEIGHT / 2) - (spriteToBlit.Height / 2 / 32);

                                // blit
                                destData.Blit(spriteToBlit, blitPoint, sourceRec);
                            }

                            if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength)
                            {
                                specifiedAnim.MoveTime = Globals.Tick;
                                specifiedAnim.Frame++;
                            }

                            if (specifiedAnim.Frame >= spriteToBlit.Width / (spriteToBlit.Height / 32))
                            {
                                specifiedAnim.CompletedLoops++;
                                specifiedAnim.Frame = 0;
                            }
                        }
else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.Overlay:
                {
                        OverlayMoveAnimation specifiedAnim = animation as OverlayMoveAnimation;
                        if (specifiedAnim.CompletedLoops < specifiedAnim.RenderLoops)
                        {
                            SpellSheet spriteSheet = GraphicsManager.GetSpellSheet(Enums.StationaryAnimType.Spell, specifiedAnim.AnimationIndex, true);
                            Surface spriteToBlit = null;
                            if (spriteSheet != null)
                            {
                                spriteToBlit = spriteSheet.Sheet;
                            }
else
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                specifiedAnim.Frame * spriteToBlit.Height,
                                0, spriteToBlit.Height, spriteToBlit.Height);

                            // blit
                            for (int y = 0; y < Constants.TILEHEIGHT * 15; y += spriteToBlit.Height)
                            {
                                for (int x = 0; x < Constants.TILEWIDTH * 20; x += spriteToBlit.Height)
                                {
                                    destData.Blit(spriteToBlit, new Point(x, y), sourceRec);
                                }
                            }

                            if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength)
                            {
                                specifiedAnim.MoveTime = Globals.Tick;
                                specifiedAnim.Frame++;
                            }

                            if (specifiedAnim.Frame >= spriteToBlit.Width / spriteToBlit.Height)
                            {
                                specifiedAnim.CompletedLoops++;
                                specifiedAnim.Frame = 0;
                            }
                        }
else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.Tile:
                {
                        TileMoveAnimation specifiedAnim = animation as TileMoveAnimation;
                        if (specifiedAnim.CompletedLoops < specifiedAnim.RenderLoops)
                        {
                            SpellSheet spriteSheet = GraphicsManager.GetSpellSheet(Enums.StationaryAnimType.Spell, specifiedAnim.AnimationIndex, false);
                            Surface spriteToBlit = null;
                            if (spriteSheet != null)
                            {
                                spriteToBlit = spriteSheet.Sheet;
                            }
else
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                specifiedAnim.Frame * spriteToBlit.Height,
                                0, spriteToBlit.Height, spriteToBlit.Height);

                            Point blitPoint = new Point(pinnedPoint.X + (Constants.TILEWIDTH / 2) - (spriteToBlit.Height / 2), pinnedPoint.Y + (Constants.TILEHEIGHT / 2) - (spriteToBlit.Height / 2));

                            // blit
                            switch (specifiedAnim.RangeType)
                            {
                                case Enums.MoveRange.FrontOfUserUntil:
                                case Enums.MoveRange.LineUntilHit:
                                {
                                        switch (specifiedAnim.Direction)
                                        {
                                            case Enums.Direction.Up:
                                            {
                                                    int y = specifiedAnim.StartY;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X, blitPoint.Y - (Constants.TILEHEIGHT * i)), sourceRec);

                                                        if (IsRenderingTargetOnSprite(
                                                            Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX, specifiedAnim.StartY - i))
                                                            {
                                                            break;
                                                        }
                                                    }
                                                }

                                                break;
                                            case Enums.Direction.Down:
                                            {
                                                    int y = specifiedAnim.StartY;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X, blitPoint.Y + (Constants.TILEHEIGHT * i)), sourceRec);

                                                        if (IsRenderingTargetOnSprite(
                                                            Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX, specifiedAnim.StartY + i))
                                                            {
                                                            break;
                                                        }
                                                    }
                                                }

                                                break;
                                            case Enums.Direction.Left:
                                            {
                                                    int x = specifiedAnim.StartX;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X - (Constants.TILEWIDTH * i), blitPoint.Y), sourceRec);

                                                        if (IsRenderingTargetOnSprite(
                                                            Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX - i, specifiedAnim.StartY))
                                                            {
                                                            break;
                                                        }
                                                    }
                                                }

                                                break;
                                            case Enums.Direction.Right:
                                            {
                                                    int x = specifiedAnim.StartX;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X + (Constants.TILEWIDTH * i), blitPoint.Y), sourceRec);

                                                        if (IsRenderingTargetOnSprite(
                                                            Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX + i, specifiedAnim.StartY))
                                                            {
                                                            break;
                                                        }
                                                    }
                                                }

                                                break;
                                        }
                                    }

                                    break;
                                case Enums.MoveRange.StraightLine:
                                case Enums.MoveRange.FrontOfUser:
                                {
                                        switch (specifiedAnim.Direction)
                                        {
                                            case Enums.Direction.Up:
                                            {
                                                    int y = specifiedAnim.StartY;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X, blitPoint.Y - (Constants.TILEHEIGHT * i)), sourceRec);
                                                    }
                                                }

                                                break;
                                            case Enums.Direction.Down:
                                            {
                                                    int y = specifiedAnim.StartY;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X, blitPoint.Y + (Constants.TILEHEIGHT * i)), sourceRec);
                                                    }
                                                }

                                                break;
                                            case Enums.Direction.Left:
                                            {
                                                    int x = specifiedAnim.StartX;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X - (Constants.TILEWIDTH * i), blitPoint.Y), sourceRec);
                                                    }
                                                }

                                                break;
                                            case Enums.Direction.Right:
                                            {
                                                    int x = specifiedAnim.StartX;
                                                    for (int i = 1; i <= specifiedAnim.Range; i++)
                                                    {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X + (Constants.TILEWIDTH * i), blitPoint.Y), sourceRec);
                                                    }
                                                }

                                                break;
                                        }
                                    }

                                    break;
                                case Enums.MoveRange.User:
                                case Enums.MoveRange.Special:
                                {
                                        destData.Blit(spriteToBlit, blitPoint, sourceRec);
                                    }

                                    break;
                                case Enums.MoveRange.Floor:
                                {
                                        for (int x = 0; x < 20; x++)
                                        {
                                            for (int y = 0; y < 15; y++)
                                            {
                                                destData.Blit(spriteToBlit, new Point((x * Constants.TILEWIDTH) + (Constants.TILEWIDTH / 2) - (spriteToBlit.Height / 2), (y * Constants.TILEHEIGHT) + (Constants.TILEHEIGHT / 2) - (spriteToBlit.Height / 2)), sourceRec);
                                            }
                                        }
                                    }

                                    break;
                                case Enums.MoveRange.Room:
                                {
                                        for (int x = -specifiedAnim.Range; x <= specifiedAnim.Range; x++)
                                        {
                                            for (int y = -specifiedAnim.Range; y <= specifiedAnim.Range; y++)
                                            {
                                                destData.Blit(spriteToBlit, new Point(blitPoint.X + (x * Constants.TILEWIDTH), blitPoint.Y + (y * Constants.TILEHEIGHT)), sourceRec);
                                            }
                                        }
                                    }

                                    break;
                                case Enums.MoveRange.FrontAndSides:
                                {
                                        for (int r = 0; r <= specifiedAnim.Range; r++)
                                        {
                                            // check adjacent tiles
                                            switch (specifiedAnim.Direction)
                                            {
                                                case Enums.Direction.Down:
                                                {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X, blitPoint.Y + (r * Constants.TILEHEIGHT)), sourceRec);

                                                        for (int s = 1; s <= r; s++)
                                                        {
                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X - (s * Constants.TILEWIDTH), blitPoint.Y + (r * Constants.TILEHEIGHT)), sourceRec);

                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X + (s * Constants.TILEWIDTH), blitPoint.Y + (r * Constants.TILEHEIGHT)), sourceRec);
                                                        }
                                                    }

                                                    break;
                                                case Enums.Direction.Up:
                                                {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X, blitPoint.Y - (r * Constants.TILEHEIGHT)), sourceRec);

                                                        for (int s = 1; s <= r; s++)
                                                        {
                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X - (s * Constants.TILEWIDTH), blitPoint.Y - (r * Constants.TILEHEIGHT)), sourceRec);

                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X + (s * Constants.TILEWIDTH), blitPoint.Y - (r * Constants.TILEHEIGHT)), sourceRec);
                                                        }
                                                    }

                                                    break;
                                                case Enums.Direction.Left:
                                                {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X - (r * Constants.TILEWIDTH), blitPoint.Y), sourceRec);

                                                        for (int s = 1; s <= r; s++)
                                                        {
                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X - (r * Constants.TILEWIDTH), blitPoint.Y - (s * Constants.TILEHEIGHT)), sourceRec);

                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X - (r * Constants.TILEWIDTH), blitPoint.Y + (s * Constants.TILEHEIGHT)), sourceRec);
                                                        }
                                                    }

                                                    break;
                                                case Enums.Direction.Right:
                                                {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X + (r * Constants.TILEWIDTH), blitPoint.Y), sourceRec);

                                                        for (int s = 1; s <= r; s++)
                                                        {
                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X + (r * Constants.TILEWIDTH), blitPoint.Y - (s * Constants.TILEHEIGHT)), sourceRec);

                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X + (r * Constants.TILEWIDTH), blitPoint.Y + (s * Constants.TILEHEIGHT)), sourceRec);
                                                        }
                                                    }

                                                    break;
                                            }
                                        }
                                    }

                                    break;
                                case Enums.MoveRange.ArcThrow:
                                {
                                        bool stopattile = false;

                                        for (int r = 0; r <= specifiedAnim.Range; r++)
                                        {
                                            // check adjacent tiles
                                            switch (specifiedAnim.Direction)
                                            {
                                                case Enums.Direction.Down:
                                                {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X, blitPoint.Y + (r * Constants.TILEHEIGHT)), sourceRec);

                                                        if (IsRenderingTargetOnSprite(
                                                            Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX, specifiedAnim.StartY + r))
                                                            {
                                                                stopattile = true;
                                                        }

                                                        if (stopattile)
                                                        {
                                                            break;
                                                        }

                                                        for (int s = 1; s <= r; s++)
                                                        {
                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X - (s * Constants.TILEWIDTH), blitPoint.Y + (r * Constants.TILEHEIGHT)), sourceRec);

                                                            if (IsRenderingTargetOnSprite(
                                                                Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX - s, specifiedAnim.StartY + r))
                                                            {
                                                                stopattile = true;
                                                            }

                                                            if (stopattile)
                                                            {
                                                                break;
                                                            }

                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X + (s * Constants.TILEWIDTH), blitPoint.Y + (r * Constants.TILEHEIGHT)), sourceRec);

                                                            if (IsRenderingTargetOnSprite(
                                                                Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX + s, specifiedAnim.StartY + r))
                                                            {
                                                                stopattile = true;
                                                            }

                                                            if (stopattile)
                                                            {
                                                                break;
                                                            }
                                                        }

                                                        if (stopattile)
                                                        {
                                                            break;
                                                        }
                                                    }

                                                    break;
                                                case Enums.Direction.Up:
                                                {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X, blitPoint.Y - (r * Constants.TILEHEIGHT)), sourceRec);

                                                        if (IsRenderingTargetOnSprite(
                                                            Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX, specifiedAnim.StartY - r))
                                                            {
                                                            stopattile = true;
                                                        }

                                                        if (stopattile)
                                                        {
                                                            break;
                                                        }

                                                        for (int s = 1; s <= r; s++)
                                                        {
                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X - (s * Constants.TILEWIDTH), blitPoint.Y - (r * Constants.TILEHEIGHT)), sourceRec);

                                                            if (IsRenderingTargetOnSprite(
                                                                Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX - s, specifiedAnim.StartY - r))
                                                            {
                                                                stopattile = true;
                                                            }

                                                            if (stopattile)
                                                            {
                                                                break;
                                                            }

                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X + (s * Constants.TILEWIDTH), blitPoint.Y - (r * Constants.TILEHEIGHT)), sourceRec);

                                                            if (IsRenderingTargetOnSprite(
                                                                Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX + s, specifiedAnim.StartY - r))
                                                            {
                                                                stopattile = true;
                                                            }

                                                            if (stopattile)
                                                            {
                                                                break;
                                                            }
                                                        }
                                                    }

                                                    break;
                                                case Enums.Direction.Left:
                                                {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X - (r * Constants.TILEWIDTH), blitPoint.Y), sourceRec);

                                                        if (IsRenderingTargetOnSprite(
                                                            Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX - r, specifiedAnim.StartY))
                                                            {
                                                            stopattile = true;
                                                        }

                                                        if (stopattile)
                                                        {
                                                            break;
                                                        }

                                                        for (int s = 1; s <= r; s++)
                                                        {
                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X - (r * Constants.TILEWIDTH), blitPoint.Y - (s * Constants.TILEHEIGHT)), sourceRec);

                                                            if (IsRenderingTargetOnSprite(
                                                                Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX - r, specifiedAnim.StartY - s))
                                                            {
                                                                stopattile = true;
                                                            }

                                                            if (stopattile)
                                                            {
                                                                break;
                                                            }

                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X - (r * Constants.TILEWIDTH), blitPoint.Y + (s * Constants.TILEHEIGHT)), sourceRec);

                                                            if (IsRenderingTargetOnSprite(
                                                                Logic.Maps.MapHelper.ActiveMap,
                                                            specifiedAnim.StartX - r, specifiedAnim.StartY + s))
                                                            {
                                                                stopattile = true;
                                                            }

                                                            if (stopattile)
                                                            {
                                                                break;
                                                            }
                                                        }
                                                    }

                                                    break;
                                                case Enums.Direction.Right:
                                                {
                                                        destData.Blit(spriteToBlit, new Point(blitPoint.X + (r * Constants.TILEWIDTH), blitPoint.Y), sourceRec);

                                                        if (IsRenderingTargetOnSprite(
                                                            Logic.Maps.MapHelper.ActiveMap,
                                                                specifiedAnim.StartX + r, specifiedAnim.StartY))
                                                                {
                                                            stopattile = true;
                                                        }

                                                        if (stopattile)
                                                        {
                                                            break;
                                                        }

                                                        for (int s = 1; s <= r; s++)
                                                        {
                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X + (r * Constants.TILEWIDTH), blitPoint.Y - (s * Constants.TILEHEIGHT)), sourceRec);

                                                            if (IsRenderingTargetOnSprite(
                                                                Logic.Maps.MapHelper.ActiveMap,
                                                                specifiedAnim.StartX + r, specifiedAnim.StartY - s))
                                                                {
                                                                stopattile = true;
                                                            }

                                                            if (stopattile)
                                                            {
                                                                break;
                                                            }

                                                            destData.Blit(spriteToBlit, new Point(blitPoint.X + (r * Constants.TILEWIDTH), blitPoint.Y + (s * Constants.TILEHEIGHT)), sourceRec);

                                                            if (IsRenderingTargetOnSprite(
                                                                Logic.Maps.MapHelper.ActiveMap,
                                                                specifiedAnim.StartX + r, specifiedAnim.StartY + s))
                                                                {
                                                                stopattile = true;
                                                            }

                                                            if (stopattile)
                                                            {
                                                                break;
                                                            }
                                                        }
                                                    }

                                                    break;
                                            }

                                            if (stopattile)
                                            {
                                                break;
                                            }
                                        }
                                    }

                                    break;
                            }

                            // for (int y = specifiedAnim.StartY; y <= specifiedAnim.EndY; y++) {
                            //    for (int x = specifiedAnim.StartX; x <= specifiedAnim.EndX; x++) {

                            // Point blitPoint = Screen.ScreenRenderer.ToTilePoint(new Point(x, y), useScrolling);
                            //        blitPoint.X = blitPoint.X + Constants.TILE_WIDTH / 2 - spriteToBlit.Height / 2;
                            //        blitPoint.Y = blitPoint.Y + Constants.TILE_HEIGHT / 2 - spriteToBlit.Height / 2;

                            // destData.Blit(spriteToBlit, blitPoint, sourceRec);
                            //    }
                            // }
                            if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength)
                            {
                                specifiedAnim.MoveTime = Globals.Tick;
                                specifiedAnim.Frame++;
                            }

                            if (specifiedAnim.Frame >= spriteToBlit.Width / spriteToBlit.Height)
                            {
                                specifiedAnim.CompletedLoops++;
                                specifiedAnim.Frame = 0;
                            }
                        }
else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.ItemArrow:
                {
                        ItemArrowMoveAnimation specifiedAnim = animation as ItemArrowMoveAnimation;
                        int time = Globals.Tick - specifiedAnim.TotalMoveTime;
                        if (time < animTime)
                        {
                            if (specifiedAnim.AnimationIndex < 0)
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                (specifiedAnim.AnimationIndex - ((specifiedAnim.AnimationIndex / 6) * 6)) * Constants.TILEWIDTH,
                                               (specifiedAnim.AnimationIndex / 6) * Constants.TILEHEIGHT, Constants.TILEWIDTH, Constants.TILEHEIGHT);

                            switch (specifiedAnim.Direction)
                            {
                                case Enums.Direction.Up:
                                {
                                        pinnedPoint.Y -= specifiedAnim.Distance * Constants.TILEHEIGHT * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.Down:
                                {
                                        pinnedPoint.Y += specifiedAnim.Distance * Constants.TILEHEIGHT * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.Left:
                                {
                                        pinnedPoint.X -= specifiedAnim.Distance * Constants.TILEWIDTH * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.Right:
                                {
                                        pinnedPoint.X += specifiedAnim.Distance * Constants.TILEWIDTH * time / animTime;
                                    }

                                    break;
                                case Enums.Direction.UpRight:
                                case Enums.Direction.DownRight:
                                case Enums.Direction.DownLeft:
                                case Enums.Direction.UpLeft:
                                    break;
                            }

                            // blit
                            destData.Blit(GraphicsManager.Items, pinnedPoint, sourceRec);

                            // if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength) {
                            //    specifiedAnim.MoveTime = Globals.Tick;
                            //    specifiedAnim.Frame++;
                            // }

                            // if (specifiedAnim.Frame >= spriteToBlit.Width / spriteToBlit.Height) {
                            //    specifiedAnim.CompletedLoops++;
                            //    specifiedAnim.Frame = 0;
                            // }
                        }
else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
                case Enums.MoveAnimationType.ItemThrow:
                {
                        ItemThrowMoveAnimation specifiedAnim = animation as ItemThrowMoveAnimation;
                        int time = Globals.Tick - specifiedAnim.TotalMoveTime;
                        if (time < animTime)
                        {
                            if (specifiedAnim.AnimationIndex < 0)
                            {
                                specifiedAnim.Active = false;
                                return;
                            }

                            Rectangle sourceRec = new Rectangle(
                                (specifiedAnim.AnimationIndex - ((specifiedAnim.AnimationIndex / 6) * 6)) * Constants.TILEWIDTH,
                                               (specifiedAnim.AnimationIndex / 6) * Constants.TILEHEIGHT, Constants.TILEWIDTH, Constants.TILEHEIGHT);

                            double distance = Math.Sqrt(Math.Pow(specifiedAnim.XChange * Constants.TILEWIDTH, 2) + Math.Pow(specifiedAnim.YChange * Constants.TILEHEIGHT, 2));

                            int x = pinnedPoint.X + (specifiedAnim.XChange * Constants.TILEWIDTH * time / animTime);
                            int y = (int)(pinnedPoint.Y + (specifiedAnim.YChange * Constants.TILEHEIGHT * time / animTime) - ((((-4) * distance * Math.Pow(time, 2) / animTime) + (4 * distance * time)) / animTime));

                            // blit
                            destData.Blit(GraphicsManager.Items, new Point(x, y), sourceRec);

                            // if (Globals.Tick > specifiedAnim.MoveTime + specifiedAnim.FrameLength) {
                            //    specifiedAnim.MoveTime = Globals.Tick;
                            //    specifiedAnim.Frame++;
                            // }

                            // if (specifiedAnim.Frame >= spriteToBlit.Width / spriteToBlit.Height) {
                            //    specifiedAnim.CompletedLoops++;
                            //    specifiedAnim.Frame = 0;
                            // }
                        }
else
                        {
                            specifiedAnim.Active = false;
                        }
                    }

                    break;
            }
        }

        public static void RenderMoveTargettingDisplay(RendererDestinationData destData, Sprites.ISprite attacker, Logic.Moves.Move move)
        {
            switch (move.RangeType)
            {
                case Enums.MoveRange.FrontOfUserUntil:
                case Enums.MoveRange.LineUntilHit:
                    {
                        switch (attacker.Direction)
                        {
                            case Enums.Direction.Up:
                                {
                                    int y = attacker.Y;
                                    for (int i = 1; i <= move.Range; i++)
                                    {
                                        y = attacker.Y - i;

                                        // if (!ShouldContinueRenderingTargettingDisplay(Logic.Maps.MapHelper.ActiveMap,
                                        //    attacker.X, y)) {
                                        //    break;
                                        // }
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X, y), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X), Screen.ScreenRenderer.ToTileY(y) + Logic.Players.PlayerManager.MyPlayer.Offset.Y));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X, y))
                                        {
                                            destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X), Screen.ScreenRenderer.ToTileY(y) + Logic.Players.PlayerManager.MyPlayer.Offset.Y));
                                        }
                                        else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X), Screen.ScreenRenderer.ToTileY(y) + Logic.Players.PlayerManager.MyPlayer.Offset.Y));
                                            break;
                                        }
                                    }
                                }

                                break;
                            case Enums.Direction.Down:
                                {
                                    int y = attacker.Y;
                                    for (int i = 1; i <= move.Range; i++)
                                    {
                                        y = attacker.Y + i;

                                        // if (!ShouldContinueRenderingTargettingDisplay(Logic.Maps.MapHelper.ActiveMap,
                                        //    attacker.X, y)) {
                                        //    break;
                                        // }
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X, y), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X), Screen.ScreenRenderer.ToTileY(y) + Logic.Players.PlayerManager.MyPlayer.Offset.Y));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X, y))
                                        {
                                            destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X), Screen.ScreenRenderer.ToTileY(y) + Logic.Players.PlayerManager.MyPlayer.Offset.Y));
                                        }
                                        else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X), Screen.ScreenRenderer.ToTileY(y) + Logic.Players.PlayerManager.MyPlayer.Offset.Y));
                                            break;
                                        }
                                    }
                                }

                                break;
                            case Enums.Direction.Left:
                                {
                                    int x = attacker.X;
                                    for (int i = 1; i <= move.Range; i++)
                                    {
                                        x = attacker.X - i;

                                        // if (!ShouldContinueRenderingTargettingDisplay(Logic.Maps.MapHelper.ActiveMap,
                                        //     x, attacker.Y)) {
                                        //    break;
                                        // }
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(x, attacker.Y), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(x) + Logic.Players.PlayerManager.MyPlayer.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y)));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            x, attacker.Y))
                                        {
                                            destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(x) + Logic.Players.PlayerManager.MyPlayer.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y)));
                                        }
                                        else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(x) + Logic.Players.PlayerManager.MyPlayer.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y)));
                                            break;
                                        }
                                    }
                                }

                                break;
                            case Enums.Direction.Right:
                                {
                                    int x = attacker.X;
                                    for (int i = 1; i <= move.Range; i++)
                                    {
                                        x = attacker.X + i;

                                        // if (!ShouldContinueRenderingTargettingDisplay(Logic.Maps.MapHelper.ActiveMap,
                                        //    x, attacker.Y)) {
                                        //    break;
                                        // }
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(x, attacker.Y), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(x) + Logic.Players.PlayerManager.MyPlayer.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y)));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            x, attacker.Y))
                                        {
                                            destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(x) + Logic.Players.PlayerManager.MyPlayer.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y)));
                                        }
                                        else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(x) + Logic.Players.PlayerManager.MyPlayer.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y)));
                                            break;
                                        }
                                    }
                                }

                                break;
                        }
                    }

                    break;
                case Enums.MoveRange.StraightLine:
                case Enums.MoveRange.FrontOfUser:
                {
                        switch (attacker.Direction)
                        {
                            case Enums.Direction.Up:
                            {
                                    int y = attacker.Y;
                                    for (int i = 1; i <= move.Range; i++)
                                    {
                                        y = attacker.Y - i;

                                        // if (!ShouldContinueRenderingTargettingDisplay(Logic.Maps.MapHelper.ActiveMap,
                                        //    attacker.X, y)) {
                                        //    break;
                                        // }
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X, y), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X), Screen.ScreenRenderer.ToTileY(y) + Logic.Players.PlayerManager.MyPlayer.Offset.Y));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X, y))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X), Screen.ScreenRenderer.ToTileY(y) + Logic.Players.PlayerManager.MyPlayer.Offset.Y));
                                        }
else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X), Screen.ScreenRenderer.ToTileY(y) + Logic.Players.PlayerManager.MyPlayer.Offset.Y));
                                        }
                                    }
                                }

                                break;
                            case Enums.Direction.Down:
                            {
                                    int y = attacker.Y;
                                    for (int i = 1; i <= move.Range; i++)
                                    {
                                        y = attacker.Y + i;

                                        // if (!ShouldContinueRenderingTargettingDisplay(Logic.Maps.MapHelper.ActiveMap,
                                        //    attacker.X, y)) {
                                        //    break;
                                        // }
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X, y), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X), Screen.ScreenRenderer.ToTileY(y) + Logic.Players.PlayerManager.MyPlayer.Offset.Y));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X, y))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X), Screen.ScreenRenderer.ToTileY(y) + Logic.Players.PlayerManager.MyPlayer.Offset.Y));
                                        }
else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X), Screen.ScreenRenderer.ToTileY(y) + Logic.Players.PlayerManager.MyPlayer.Offset.Y));
                                        }
                                    }
                                }

                                break;
                            case Enums.Direction.Left:
                            {
                                    int x = attacker.X;
                                    for (int i = 1; i <= move.Range; i++)
                                    {
                                        x = attacker.X - i;

                                        // if (!ShouldContinueRenderingTargettingDisplay(Logic.Maps.MapHelper.ActiveMap,
                                        //     x, attacker.Y)) {
                                        //    break;
                                        // }
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(x, attacker.Y), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(x) + Logic.Players.PlayerManager.MyPlayer.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y)));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            x, attacker.Y))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(x) + Logic.Players.PlayerManager.MyPlayer.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y)));
                                        }
else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(x) + Logic.Players.PlayerManager.MyPlayer.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y)));
                                        }
                                    }
                                }

                                break;
                            case Enums.Direction.Right:
                            {
                                    int x = attacker.X;
                                    for (int i = 1; i <= move.Range; i++)
                                    {
                                        x = attacker.X + i;

                                        // if (!ShouldContinueRenderingTargettingDisplay(Logic.Maps.MapHelper.ActiveMap,
                                        //    x, attacker.Y)) {
                                        //    break;
                                        // }
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(x, attacker.Y), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(x) + Logic.Players.PlayerManager.MyPlayer.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y)));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            x, attacker.Y))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(x) + Logic.Players.PlayerManager.MyPlayer.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y)));
                                        }
else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(x) + Logic.Players.PlayerManager.MyPlayer.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y)));
                                        }
                                    }
                                }

                                break;
                        }
                    }

                    break;
                case Enums.MoveRange.User:
                {
                        destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                    }

                    break;
                case Enums.MoveRange.Special:
                    {
                        destData.Blit(srfcMoveTargetUnknown, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - 1) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - 1) + attacker.Offset.Y));
                    }

                    break;
                case Enums.MoveRange.Floor:
                    {
                        int startX = Screen.ScreenRenderer.GetScreenLeft() - 1;
                        int startY = Screen.ScreenRenderer.GetScreenTop();
                        if (startX < 0)
                        {
                            startX = 0;
                        }
                        else if (startX + 19 > Screen.ScreenRenderer.RenderOptions.Map.MaxX)
                        {
                            startX = Screen.ScreenRenderer.RenderOptions.Map.MaxX - 19;
                        }

                        if (startY < 0)
                        {
                            startY = 0;
                        }
                        else if (startY + 14 > Screen.ScreenRenderer.RenderOptions.Map.MaxY)
                        {
                            startY = Screen.ScreenRenderer.RenderOptions.Map.MaxY - 14;
                        }

                        for (int x = startX; x < startX + 20; x++)
                        {
                            for (int y = startY; y < startY + 15; y++)
                            {
                                if (!Screen.ScreenRenderer.CanBeSeen(new Point(x, y), Enums.MapID.Active))
                                {
                                    destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(x), Screen.ScreenRenderer.ToTileY(y)));
                                }
                                else if (!IsRenderingTargetOnSprite(
                                    Logic.Maps.MapHelper.ActiveMap,
                                    x, y))
                                {
                                    destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(x), Screen.ScreenRenderer.ToTileY(y)));
                                }
                                else
                                {
                                    destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(x), Screen.ScreenRenderer.ToTileY(y)));
                                }
                            }
                        }
                    }

                    break;
                case Enums.MoveRange.Room:
                    {
                        for (int x = attacker.X - move.Range; x <= attacker.X + move.Range; x++)
                        {
                            for (int y = attacker.Y - move.Range; y <= attacker.Y + move.Range; y++)
                            {
                                if (!Screen.ScreenRenderer.CanBeSeen(new Point(x, y), Enums.MapID.Active))
                                {
                                    destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(x), Screen.ScreenRenderer.ToTileY(y)));
                                }
                                else if (!IsRenderingTargetOnSprite(
                                    Logic.Maps.MapHelper.ActiveMap,
                                    x, y))
                                {
                                    destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(x) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(y) + attacker.Offset.Y));
                                }
                                else
                                {
                                    destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(x) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(y) + attacker.Offset.Y));
                                }
                            }
                        }
                    }

                    break;
                case Enums.MoveRange.FrontAndSides:
                    {
                        for (int r = 0; r <= move.Range; r++)
                        {
                            // check adjacent tiles
                            switch (attacker.Direction)
                            {
                                case Enums.Direction.Down:
                                    {
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X, attacker.Y + r), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X, attacker.Y + r))
                                        {
                                            destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                        }
                                        else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                        }

                                        for (int s = 1; s <= r; s++)
                                        {
                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X - s, attacker.Y + r), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X - s, attacker.Y + r))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                            }

                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X + s, attacker.Y + r), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X + s, attacker.Y + r))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                            }
                                        }
                                    }

                                    break;
                                case Enums.Direction.Up:
                                    {
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X, attacker.Y - r), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X, attacker.Y - r))
                                        {
                                            destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                        }
                                        else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                        }

                                        for (int s = 1; s <= r; s++)
                                        {
                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X - s, attacker.Y - r), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X - s, attacker.Y - r))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                            }

                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X + s, attacker.Y - r), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X + s, attacker.Y - r))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                            }
                                        }
                                    }

                                    break;
                                case Enums.Direction.Left:
                                    {
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X - r, attacker.Y), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X - r, attacker.Y))
                                        {
                                            destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                                        }
                                        else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                                        }

                                        for (int s = 1; s <= r; s++)
                                        {
                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X - r, attacker.Y - s), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - s) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X - r, attacker.Y - s))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - s) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - s) + attacker.Offset.Y));
                                            }

                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X - r, attacker.Y + s), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + s) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X - r, attacker.Y + s))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + s) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + s) + attacker.Offset.Y));
                                            }
                                        }
                                    }

                                    break;
                                case Enums.Direction.Right:
                                    {
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X + r, attacker.Y), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X + r, attacker.Y))
                                        {
                                            destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                                        }
                                        else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                                        }

                                        for (int s = 1; s <= r; s++)
                                        {
                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X + r, attacker.Y - s), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - s) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X + r, attacker.Y - s))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - s) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - s) + attacker.Offset.Y));
                                            }

                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X + r, attacker.Y + s), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + s) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X + r, attacker.Y + s))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + s) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + s) + attacker.Offset.Y));
                                            }
                                        }
                                    }

                                    break;
                            }
                        }
                    }

                    break;
                case Enums.MoveRange.ArcThrow:
                    {
                        bool stopattile = false;
                        for (int r = 0; r <= move.Range; r++)
                        {
                            // check adjacent tiles
                            switch (attacker.Direction)
                            {
                                case Enums.Direction.Down:
                                    {
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X, attacker.Y + r), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X, attacker.Y + r))
                                        {
                                            destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                        }
                                        else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                            stopattile = true;
                                        }

                                        if (stopattile)
                                        {
                                            break;
                                        }

                                        for (int s = 1; s <= r; s++)
                                        {
                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X - s, attacker.Y + r), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X - s, attacker.Y + r))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                                stopattile = true;
                                            }

                                            if (stopattile)
                                            {
                                                break;
                                            }

                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X + s, attacker.Y + r), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X + s, attacker.Y + r))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + r) + attacker.Offset.Y));
                                                stopattile = true;
                                            }

                                            if (stopattile)
                                            {
                                                break;
                                            }
                                        }
                                    }

                                    break;
                                case Enums.Direction.Up:
                                    {
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X, attacker.Y - r), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X, attacker.Y - r))
                                        {
                                            destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                        }
                                        else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                            stopattile = true;
                                        }

                                        if (stopattile)
                                        {
                                            break;
                                        }

                                        for (int s = 1; s <= r; s++)
                                        {
                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X - s, attacker.Y - r), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X - s, attacker.Y - r))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                                stopattile = true;
                                            }

                                            if (stopattile)
                                            {
                                                break;
                                            }

                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X + s, attacker.Y - r), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X + s, attacker.Y - r))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + s) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - r) + attacker.Offset.Y));
                                                stopattile = true;
                                            }

                                            if (stopattile)
                                            {
                                                break;
                                            }
                                        }
                                    }

                                    break;
                                case Enums.Direction.Left:
                                    {
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X - r, attacker.Y), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X - r, attacker.Y))
                                        {
                                            destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                                        }
                                        else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                                            stopattile = true;
                                        }

                                        if (stopattile)
                                        {
                                            break;
                                        }

                                        for (int s = 1; s <= r; s++)
                                        {
                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X - r, attacker.Y - s), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - s) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X - r, attacker.Y - s))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - s) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - s) + attacker.Offset.Y));
                                                stopattile = true;
                                            }

                                            if (stopattile)
                                            {
                                                break;
                                            }

                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X - r, attacker.Y + s), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + s) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X - r, attacker.Y + s))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + s) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X - r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + s) + attacker.Offset.Y));
                                                stopattile = true;
                                            }

                                            if (stopattile)
                                            {
                                                break;
                                            }
                                        }
                                    }

                                    break;
                                case Enums.Direction.Right:
                                    {
                                        if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X + r, attacker.Y), Enums.MapID.Active))
                                        {
                                            destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                                        }
                                        else if (!IsRenderingTargetOnSprite(
                                            Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X + r, attacker.Y))
                                        {
                                            destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                                        }
                                        else
                                        {
                                            destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y) + attacker.Offset.Y));
                                            stopattile = true;
                                        }

                                        if (stopattile)
                                        {
                                            break;
                                        }

                                        for (int s = 1; s <= r; s++)
                                        {
                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X + r, attacker.Y - s), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - s) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X + r, attacker.Y - s))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - s) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y - s) + attacker.Offset.Y));
                                                stopattile = true;
                                            }

                                            if (stopattile)
                                            {
                                                break;
                                            }

                                            if (!Screen.ScreenRenderer.CanBeSeen(new Point(attacker.X + r, attacker.Y + s), Enums.MapID.Active))
                                            {
                                                destData.Blit(srfcMoveTargetTileDark, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + s) + attacker.Offset.Y));
                                            }
                                            else if (!IsRenderingTargetOnSprite(
                                                Logic.Maps.MapHelper.ActiveMap,
                                            attacker.X + r, attacker.Y + s))
                                            {
                                                destData.Blit(srfcMoveTargetTile, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + s) + attacker.Offset.Y));
                                            }
                                            else
                                            {
                                                destData.Blit(srfcMoveTargetTileHit, new Point(Screen.ScreenRenderer.ToTileX(attacker.X + r) + attacker.Offset.X, Screen.ScreenRenderer.ToTileY(attacker.Y + s) + attacker.Offset.Y));
                                                stopattile = true;
                                            }

                                            if (stopattile)
                                            {
                                                break;
                                            }
                                        }
                                    }

                                    break;
                            }

                            if (stopattile)
                            {
                                break;
                            }
                        }
                    }

                    break;
            }
        }

        private static bool ShouldContinueRenderingTargettingDisplay(Logic.Maps.Map activeMap, int x, int y)
        {
            if (x < 0 || y < 0 || x > activeMap.MaxX || y > activeMap.MaxY)
            {
                return false;
            }

            if (GameProcessor.IsBlocked(activeMap, x, y))
            {
                return false;
            }
else
            {
                return true;
            }
        }

        public static bool IsRenderingTargetOnSprite(Logic.Maps.Map activeMap, int x, int y)
        {
            for (int i = 0; i < activeMap.MapNpcs.Length; i++)
            {
                if (activeMap.MapNpcs[i].Num > 0 && activeMap.MapNpcs[i].ScreenActive &&
                    activeMap.MapNpcs[i].X == x &&
                    activeMap.MapNpcs[i].Y == y)
                    {
                    return true;
                }
            }

            if (activeMap.Players != null)
            {
                for (int i = 0; i < activeMap.Players.Count; i++)
                {
                    if (activeMap.Players[i].ScreenActive && activeMap.Players[i].X == x &&
                        activeMap.Players[i].Y == y)
                        {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}