﻿// <copyright file="NormalMoveAnimation.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Moves
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    internal class NormalMoveAnimation : IMoveAnimation
    {
        public NormalMoveAnimation(int targetX, int targetY)
        {
            this.StartX = targetX;
            this.StartY = targetY;
        }

        /// <inheritdoc/>
        public bool Active
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int AnimationIndex
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int CompletedLoops
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Frame
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int FrameLength
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int MoveTime
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int RenderLoops
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.MoveAnimationType AnimType
        {
            get { return Enums.MoveAnimationType.Normal; }
        }

        /// <inheritdoc/>
        public int StartX
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int StartY
        {
            get;
            set;
        }
    }
}