﻿// <copyright file="TileMoveAnimation.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Moves
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    internal class TileMoveAnimation : IMoveAnimation
    {
        public TileMoveAnimation(int targetX, int targetY, Enums.MoveRange rangeType, Enums.Direction dir, int range)
        {
            this.StartX = targetX;
            this.StartY = targetY;
            this.RangeType = rangeType;
            this.Direction = dir;
            this.Range = range;
        }

        /// <inheritdoc/>
        public bool Active
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int AnimationIndex
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int CompletedLoops
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Frame
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int FrameLength
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int MoveTime
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int RenderLoops
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.MoveAnimationType AnimType
        {
            get { return Enums.MoveAnimationType.Tile; }
        }

        /// <inheritdoc/>
        public int StartX
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int StartY
        {
            get;
            set;
        }

        public Enums.MoveRange RangeType
        {
            get;
            set;
        }

        public int Range
        {
            get;
            set;
        }

        public Enums.Direction Direction
        {
            get;
            set;
        }
    }
}