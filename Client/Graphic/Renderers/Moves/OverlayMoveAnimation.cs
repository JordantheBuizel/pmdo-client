﻿// <copyright file="OverlayMoveAnimation.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Moves
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    internal class OverlayMoveAnimation : IMoveAnimation
    {
        public OverlayMoveAnimation()
        {
        }

        /// <inheritdoc/>
        public bool Active
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int AnimationIndex
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int CompletedLoops
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Frame
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int FrameLength
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int MoveTime
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int RenderLoops
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int StartX
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int StartY
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.MoveAnimationType AnimType
        {
            get { return Enums.MoveAnimationType.Overlay; }
        }
    }
}