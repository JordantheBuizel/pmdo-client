﻿// <copyright file="IMoveAnimation.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Moves
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    internal interface IMoveAnimation
    {
        bool Active
        {
            get;
            set;
        }

        int AnimationIndex
        {
            get;
            set;
        }

        int CompletedLoops
        {
            get;
            set;
        }

        int Frame
        {
            get;
            set;
        }

        int FrameLength
        {
            get;
            set;
        }

        int MoveTime
        {
            get;
            set;
        }

        int RenderLoops
        {
            get;
            set;
        }

        int StartX
        {
            get;
            set;
        }

        int StartY
        {
            get;
            set;
        }

        Enums.MoveAnimationType AnimType
        {
            get;
        }
    }
}