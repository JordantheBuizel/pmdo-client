﻿// <copyright file="WorldMap.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Renderers.Maps
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Screen;
    using Network;
    using Logic.Players;

    internal class WorldMap
    {
        private static readonly string mapDataPath = Path.Combine(Application.StartupPath, "MapData", "PlayerMap.dat");
        private static readonly string mapDataXMLPath = Path.Combine(Application.StartupPath, "MapData", "PlayerMap.xml");
        private static Logic.Maps.Map lastMap;
        private static Point lastPlayerPos = new Point(-99999, -99999);

        // Scrapped until further notice
        public static void RenderMap(ScreenRenderOptions renderOptions, Camera camera)
        {
            try
            {
                bool isInternalMap = false;

                // We dont want Dungeons/PlayerHouses to draw on the map
                if (renderOptions.Map.MapID.StartsWith("s") == false)
                {
                    return;
                }

                Point drawLocation = default(Point);
                if (lastPlayerPos == new Point(-99999, -99999))
                {
                    if (File.Exists(mapDataXMLPath))
                    {
                    }
                    else
                    {
                        // Assume this is a new player
                        drawLocation = new Point(0, 0);
                    }
                }
                else
                {
                    if (lastMap.MapID != renderOptions.Map.MapID)
                    {
                       switch (lastMap.Tile[lastPlayerPos.X, lastPlayerPos.Y].Type)
                        {
                            case Enums.TileType.Warp:
                                {
                                    isInternalMap = true;
                                }

                                break;
                        }
                    }
                }

                Image worldMapImage;

                Security.Encryption encryption = new Security.Encryption(PlayerManager.MyPlayer.Name);
                if (File.Exists(mapDataPath))
                {
                    using (MemoryStream stream = new MemoryStream(encryption.DecryptBytes(File.ReadAllBytes(mapDataPath))))
                    {
                        worldMapImage = Image.FromStream(stream);
                    }
                }
                else
                {
                    worldMapImage = new Bitmap(Windows.WindowSwitcher.GameWindow.MapViewer.Width, Windows.WindowSwitcher.GameWindow.MapViewer.Height);
                }

                SdlDotNet.Graphics.Surface surface = new SdlDotNet.Graphics.Surface((Bitmap)worldMapImage);
                RendererDestinationData destData = new RendererDestinationData(surface, drawLocation, Windows.WindowSwitcher.GameWindow.MapViewer.Size);

                MapRenderer.DrawGroundTilesSeamless(destData, renderOptions.Map, renderOptions.DisplayAnimation, camera.X, camera.X2, camera.Y, camera.Y2);
                MapRenderer.DrawFringeTilesSeamless(destData, renderOptions.Map, renderOptions.DisplayAnimation, camera.X, camera.X2, camera.Y, camera.Y2);

                lastPlayerPos = PlayerManager.MyPlayer.Location;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("World Map Rendering:");
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
        }
    }
}
