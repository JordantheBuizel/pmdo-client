﻿// <copyright file="SurfaceManager.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Security;
    using System.Text;
    using SdlDotNet.Graphics;

    internal class SurfaceManager
    {
        // private static string gfxEncryptionKey = @"^87cn23{}&*678!@f:98&(873320)*&}{fwe\d";
        private static readonly string gfxEncryptionKey = @"&(*hvsdhj^%RtifgyhjHI&%tg8fhdiks*(&hfwielsaifudsoh&(*Y";

        public enum SurfaceSaveType
        {
            Unknown,
            Png,
            Gif,
            Jpg,
            Bmp,
            Ico,
            Pmugfx
        }

        public static SurfaceSaveType DetermineSaveType(string filePath)
        {
            switch (Path.GetExtension(filePath))
            {
                case ".png":
                    return SurfaceSaveType.Png;
                case ".gif":
                    return SurfaceSaveType.Gif;
                case ".jpg":
                case ".jpeg":
                    return SurfaceSaveType.Jpg;
                case ".bmp":
                    return SurfaceSaveType.Bmp;
                case ".ico":
                    return SurfaceSaveType.Ico;
                case ".pmugfx":
                    return SurfaceSaveType.Pmugfx;
                default:
                    return SurfaceSaveType.Unknown;
            }
        }

        public static Surface LoadSurface(string filePath)
        {
            return LoadSurface(filePath, false, false);
        }

        public static Surface LoadSurface(string filePath, bool convert, bool transparent)
        {
            filePath = IO.Paths.CreateOSPath(filePath);
            Surface returnSurf;
            switch (Path.GetExtension(filePath))
            {
                case ".pmugfx":
                {
                        if (IO.IO.FileExists(filePath))
                        {
                            using (MemoryStream stream = new MemoryStream(DecryptSurface(filePath)))
                            {
                                Bitmap bitmap = (Bitmap)Image.FromStream(stream);
                                returnSurf = new Surface(bitmap);
                                if (convert)
                                {
                                    Surface returnSurf2 = returnSurf.Convert();
                                    returnSurf2.Transparent = true;
                                    returnSurf.Close();
                                    return returnSurf2;
                                }
                                else
                                {
                                    return returnSurf;
                                }
                            }
                        }
else
                        {
                            return null;
                        }
                    }

                case ".gif":
                case ".png":
                default:
                {
                        if (IO.IO.FileExists(filePath))
                        {
                            using (FileStream stream = File.OpenRead(filePath))
                            {
                                if (transparent)
                                {
                                    Bitmap bitmap = (Bitmap)Image.FromStream(stream);
                                    Bitmap clone = new Bitmap(bitmap.Width, bitmap.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                                    using (Graphics gr = Graphics.FromImage(clone))
                                    {
                                        gr.DrawImage(bitmap, new Rectangle(0, 0, clone.Width, clone.Height));
                                    }

                                    returnSurf = new Surface(clone);
                                }
                                else
                                {
                                    Bitmap bitmap = (Bitmap)Image.FromStream(stream);
                                    returnSurf = new Surface(bitmap);
                                }

                                if (convert)
                                {
                                    Surface returnSurf2 = returnSurf.Convert();
                                    returnSurf2.Transparent = true;
                                    returnSurf.Close();
                                    return returnSurf2;
                                }
                                else
                                {
                                    return returnSurf;
                                }
                            }
                        }
else
                        {
                            return null;
                        }
                    }
            }
        }

        public static void SaveSurface(Surface surfaceToSave, string filePath)
        {
            Image img = surfaceToSave.Bitmap;
            switch (DetermineSaveType(filePath))
            {
                case SurfaceSaveType.Png:
                    img.Save(filePath, System.Drawing.Imaging.ImageFormat.Png);
                    break;
                case SurfaceSaveType.Gif:
                    img.Save(filePath, System.Drawing.Imaging.ImageFormat.Gif);
                    break;
                case SurfaceSaveType.Jpg:
                    img.Save(filePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                    break;
                case SurfaceSaveType.Bmp:
                    img.Save(filePath, System.Drawing.Imaging.ImageFormat.Bmp);
                    break;
                case SurfaceSaveType.Ico:
                    img.Save(filePath, System.Drawing.Imaging.ImageFormat.Icon);
                    break;
                case SurfaceSaveType.Pmugfx:
                {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                            byte[] encryptedBytes = EncryptSurface(ms.ToArray());
                            File.WriteAllBytes(filePath, encryptedBytes);
                        }
                    }

                    break;
            }

            img.Dispose();
        }

        internal static Surface LoadPMUGfx(byte[] imageBytes)
        {
            return LoadPMUGfx(imageBytes, false);
        }

        internal static Surface LoadPMUGfx(byte[] imageBytes, bool convert)
        {
            Surface returnSurf = new Surface(DecryptSurface(imageBytes));
            if (convert)
            {
                Surface returnSurf2 = returnSurf.Convert();
                returnSurf2.Transparent = true;
                returnSurf.Close();
                return returnSurf2;
            }
else
            {
                return returnSurf;
            }
        }

        internal static byte[] DecryptSurface(byte[] imageBytes)
        {
            Security.Encryption encryption = new Security.Encryption(gfxEncryptionKey);
            byte[] decryptedSurface = encryption.DecryptBytes(imageBytes);
            return decryptedSurface;
        }

        internal static byte[] DecryptSurface(string filePath)
        {
            return DecryptSurface(File.ReadAllBytes(filePath));
        }

        internal static byte[] EncryptSurface(string filePath)
        {
            return EncryptSurface(File.ReadAllBytes(filePath));
        }

        internal static byte[] EncryptSurface(byte[] imageBytes)
        {
            Security.Encryption encryption = new Security.Encryption(gfxEncryptionKey);
            byte[] encryptedSurface = encryption.EncryptBytes(imageBytes);
            return encryptedSurface;
        }
    }
}