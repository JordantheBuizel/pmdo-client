﻿// <copyright file="TileCache.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;

    internal class TileCache
    {
        private readonly Tileset[] tileSets;

        public TileCache(int maxTileSets)
        {
            this.tileSets = new Tileset[maxTileSets];
        }

        public void AddTileset(Tileset tileSet)
        {
            this.tileSets[tileSet.TilesetNumber] = tileSet;
        }

        public Tileset this[int index]
        {
            get { return this.tileSets[index]; }
        }
    }
}
