﻿// <copyright file="SpellSheet.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;
    using SdlDotNet.Graphics;

    internal class SpellSheet : ICacheable
    {
        private readonly Surface sheet;
        private readonly int sizeInBytes;

        /// <inheritdoc/>
        public int BytesUsed
        {
            get { return this.sizeInBytes; }
        }

        public Surface Sheet
        {
            get { return this.sheet; }
        }

        public SpellSheet(Surface surface, int sizeInBytes)
        {
            this.sheet = surface;
            this.sizeInBytes = sizeInBytes;
        }
    }
}
