﻿// <copyright file="TileGraphic.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;
    using SdlDotNet.Graphics;

    public class TileGraphic : ICacheable
    {
        private readonly int sizeInBytes;
        private readonly Surface tile;
        private readonly int tileSet;
        private readonly int tileNum;

        public TileGraphic(int tileSet, int tileNum, Surface tile, int sizeInBytes)
        {
            this.tileSet = tileSet;
            this.tileNum = tileNum;
            this.tile = tile;
            this.sizeInBytes = sizeInBytes;
        }

        public Surface Tile
        {
            get
            {
                return this.tile;
            }
        }

        public int TileSet
        {
            get { return this.tileSet; }
        }

        public int TileNum
        {
            get { return this.tileNum; }
        }

        /// <inheritdoc/>
        public int BytesUsed
        {
            get { return this.sizeInBytes; }
        }
    }
}
