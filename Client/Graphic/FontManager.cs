﻿// <copyright file="FontManager.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using SdlDotNet.Graphics;

    /// <summary>
    /// Class used for managing the game fonts.
    /// </summary>
    public class FontManager
    {
        /// <summary>
        /// Gets or sets the font used as the main map font
        /// </summary>
        public static Font GameFont
        {
            get;
            set;
        }

        public static Font GameFontSmall
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes the textbox font.
        /// </summary>
        public static void InitFonts()
        {
            GameFont = LoadFont("PMD.ttf", 24);
            GameFontSmall = LoadFont("PMD.ttf", 16);
        }

        /// <summary>
        /// Loads a font.
        /// </summary>
        /// <param name="fontName">Filename of the font to load.</param>
        /// <param name="pointSize">Size of the font.</param>
        /// <returns>Font that's loaded</returns>
        public static Font LoadFont(string fontName, int pointSize)
        {
            if (fontName.Contains("PMU"))
            {
                fontName = fontName.Replace("PMU", "PMD");
            }

            if (fontName.EndsWith(".ttf") == false)
            {
                fontName += ".ttf";
            }

            Font font = new Font(IO.Paths.FontPath + fontName, pointSize);
            return font;
        }

        public static System.Drawing.Font LoadWindowsFont(string fontName, int emSize)
        {
            if (fontName.Contains("PMU"))
            {
                fontName = fontName.Replace("PMU", "PMD");
            }

            if (fontName.EndsWith(".ttf") == false)
            {
                fontName += ".ttf";
            }

            return new System.Drawing.Font(IO.Paths.FontPath + fontName, emSize);
        }
    }
}