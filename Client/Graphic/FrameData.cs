﻿// <copyright file="FrameData.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using PMU.Core;
    using SdlDotNet.Graphics;

    internal class FrameData
    {
        private int frameWidth;
        private int frameHeight;

        private readonly Dictionary<FrameType, Dictionary<Enums.Direction, int>> frameCount;

        public int FrameWidth
        {
            get { return this.frameWidth; }
        }

        public int FrameHeight
        {
            get { return this.frameHeight; }
        }

        public FrameData()
        {
            this.frameCount = new Dictionary<FrameType, Dictionary<Enums.Direction, int>>();
        }

        public void SetFrameSize(int animWidth, int animHeight, int frames)
        {
            this.frameWidth = animWidth / frames;

            this.frameHeight = animHeight;
        }

        public void SetFrameCount(FrameType type, Enums.Direction dir, int count)
        {
            if (this.frameCount.ContainsKey(type) == false)
            {
                this.frameCount.Add(type, new Dictionary<Enums.Direction, int>());
            }

            if (this.frameCount[type].ContainsKey(dir) == false)
            {
                this.frameCount[type].Add(dir, count);
            }
else
            {
                this.frameCount[type][dir] = count;
            }
        }

        public int GetFrameCount(FrameType type, Enums.Direction dir)
        {
            Dictionary<Enums.Direction, int> dirs = null;
            if (this.frameCount.TryGetValue(type, out dirs))
            {
                int value = 0;
                if (dirs.TryGetValue(dir, out value))
                {
                    return value;
                }
else
                {
                    return 0;
                }
            }
else
            {
                return 0;
            }
        }
    }
}