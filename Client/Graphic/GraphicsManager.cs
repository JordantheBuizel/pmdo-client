﻿// <copyright file="GraphicsManager.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Net;
    using System.Text;
    using Windows;
    using PMU.Core;
    using SdlDotNet.Graphics;

    /// <summary>
    /// Manages the game graphics.
    /// </summary>
    internal class GraphicsManager
    {
        internal const int MAXTILES = 11;
        internal const int PICX = 32;
        internal const int PICY = 32;

        private static readonly int SpriteCacheSize = 200; // Cache 200 sprites
        private static readonly int SpellCacheSize = 1024 * 1024 * 5; // Total of 5 MB
        private static readonly int MugshotCacheSize = 1024 * 100; // Total of 100kb cache
        private static readonly int TileSetCacheSize = 500; /* This cache uses item amount as the limit.
                                                     * Each tile is 4096 bytes * 100 = 819.2 KB
                                                     * There are 10 tilesets, 819.2 KB * 10 = 8192 KB = 8.192 MB
                                                     */

        private static MultiNameLRUCache<string, SpriteSheet> spriteCache;
        private static List<(string Key, Mugshot Value)> mugshotCache;
        private static Cache<string, SpellSheet> spellCache;
        private static TileCache tileCache;
        private static bool initialized = false;

        internal static Surface FadeSurface
        {
            get;
            set;
        }

        internal static Surface Items
        {
            get;
            set;
        }

        internal static TileCache Tiles
        {
            get { return tileCache; }
        }

        /// <summary>
        /// Inits this instance.
        /// </summary>
        public static void Initialize()
        {
            // FadeSurface = new SdlDotNet.Graphics.Surface(SdlDotNet.Graphics.Video.Screen.Size);
            if (!initialized)
            {
                spriteCache = new MultiNameLRUCache<string, SpriteSheet>(SpriteCacheSize);
                spellCache = new Cache<string, SpellSheet>(SpellCacheSize);
                mugshotCache = new List<(string, Mugshot)>();
                tileCache = new TileCache(MAXTILES);
                initialized = true;
            }
        }

        /// <summary>
        /// Loads a tilesheet.
        /// </summary>
        /// <param name="index">The index.</param>
        public static void LoadTilesheet(int index)
        {
            string path = IO.Paths.GfxPath + "Tiles\\Tiles" + index.ToString() + ".tile";

            int aprilIndex = index;
            if (index == 10 && ((DateTime.Now.Month == 4 & DateTime.Now.Day == 1) || File.Exists(Path.Combine(System.Windows.Forms.Application.StartupPath, "aprfltest.dat"))))
            {
                path = Path.Combine(System.Windows.Forms.Application.StartupPath, "PMDO.Data.dll");
            }

            Tileset tileSet = new Tileset(index, TileSetCacheSize);
            tileSet.Load(path);
            tileCache.AddTileset(tileSet);
        }

        public static int GetAnimDirInt(Enums.Direction dir)
        {
            switch (dir)
            {
                case Enums.Direction.Down:
                    return 0;
                case Enums.Direction.Left:
                    return 1;
                case Enums.Direction.Up:
                    return 2;
                case Enums.Direction.Right:
                    return 3;
                case Enums.Direction.DownLeft:
                    return 4;
                case Enums.Direction.UpLeft:
                    return 5;
                case Enums.Direction.UpRight:
                    return 6;
                case Enums.Direction.DownRight:
                    return 7;
                default:
                    return 0;
            }
        }

        public static Enums.Direction GetAnimIntDir(int dir)
        {
            switch (dir)
            {
                case 0:
                    return Enums.Direction.Down;
                case 1:
                    return Enums.Direction.Left;
                case 2:
                    return Enums.Direction.Up;
                case 3:
                    return Enums.Direction.Right;
                case 4:
                    return Enums.Direction.DownLeft;
                case 5:
                    return Enums.Direction.UpLeft;
                case 6:
                    return Enums.Direction.UpRight;
                case 7:
                    return Enums.Direction.DownRight;
                default:
                    return Enums.Direction.Down;
            }
        }

        public static SpriteSheet GetSpriteSheet(int num)
        {
            return GetSpriteSheet(num, -1, -1, -1);
        }

        public static SpriteSheet GetSpriteSheet(int num, int form, int shiny, int gender)
        {
            // System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            // watch.Start();
            // System.Diagnostics.Debug.WriteLine("PkMn #"+num+"("+form+"/"+shiny+"/"+gender+") requested");
            string formString = "r";

            if (form >= 0)
            {
                formString += "-" + form;
                if (shiny >= 0)
                {
                    formString += "-" + shiny;
                    if (gender >= 0)
                    {
                        formString += "-" + gender;
                    }
                }
            }

            SpriteSheet sheet = null;
            lock (spriteCache)
            {
                sheet = spriteCache.Get(num + formString);
            }

            if (sheet != null)
            {
                // watch.Stop();
                // System.Diagnostics.Debug.WriteLine("PkMn #"+num+"("+form+"/"+shiny+"/"+gender+") retrieved in cache in "+watch.ElapsedMilliseconds);
                return sheet;
            }

            // If we are still here, that means the sprite wasn't in the cache

            // April Fools idea XD
            if (DateTime.Now.Day == 1 && DateTime.Now.Month == 4)
            {
                // Taco Sprite
                num = 6666;
            }

            List<int> lockedGraphics = new List<int>
            {
                26,
                502,
                725,
                726,
                773,
                7026,
                7148
            };

            if (lockedGraphics.Contains(num))
            {
                string domain = "pmdonlineupdates.serveftp.com";

                // Check if main domain is down
                {
                    try
                    {
                        FtpWebRequest requestdir = (FtpWebRequest)WebRequest.Create("ftp://" + domain);
                        requestdir.Credentials = new NetworkCredential("anonomyous", string.Empty);
                        requestdir.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                        WebResponse response = requestdir.GetResponse();
                    }
                    catch
                    {
                        domain = "pmdonline.servegame.com";
                    }
                }

                sheet = new SpriteSheet(num, formString);
                string changedFormString = formString;

                WebClient request = new WebClient();
                request.Credentials = new NetworkCredential("anonymous", string.Empty);

                using (MemoryStream fileStream = new MemoryStream(request.DownloadData("ftp://" + domain + "/Sprites/Sprite" + num + ".sprite")))
                {
                    using (BinaryReader reader = new BinaryReader(fileStream))
                    {
                        int formCount = reader.ReadInt32();
                        Dictionary<string, int[]> formData = new Dictionary<string, int[]>();

                        for (int i = 0; i < formCount; i++)
                        {
                            // Read the form name
                            string formName = reader.ReadString();

                            int[] formIntData = new int[2];

                            // Load form position
                            formIntData[0] = reader.ReadInt32();

                            // Load form size
                            formIntData[1] = reader.ReadInt32();

                            // Add form data to collection
                            formData.Add(formName, formIntData);
                        }

                        while (true)
                        {
                            if (spriteCache.ContainsKey(num + changedFormString))
                            {// this point will be hit if the first fallback data to be found is already in the cache
                                // the cache needs to be updated for aliases, but that's it.  No need to load any new data.
                                sheet = spriteCache.Get(num + changedFormString);
                                break;
                            }
                            else if (formData.ContainsKey(changedFormString) || changedFormString == "r")
                            {// we've found a spritesheet in the file, so load it.
                                int[] formInt = formData[changedFormString];

                                // Jump to the correct position
                                fileStream.Seek(formInt[0], SeekOrigin.Current);

                                sheet.LoadFromData(reader, formInt[1]);

                                spriteCache.Add(num + changedFormString, sheet);

                                break;
                            }

                            // If the form specified wasn't found, continually revert to the backup until only "r" is reached
                            changedFormString = changedFormString.Substring(0, changedFormString.LastIndexOf('-'));
                        }
                    }
                }

                // continually add aliases
                string aliasString = formString;
                while (aliasString != changedFormString)
                {
                    // add aliases here
                    spriteCache.AddAlias(num + aliasString, num + changedFormString);

                    // If the form specified wasn't found, continually revert to the backup until only "r" is reached
                    aliasString = aliasString.Substring(0, aliasString.LastIndexOf('-'));
                }

                // string rootForm = spriteCache.GetOriginalKeyFromAlias(num + formString);
                // if (rootForm != num + formString)
                // {
                //    Logs.Logger.LogDebug("Could not load " + num + formString + ", loaded " + num + rootForm +" instead.");
                // }
                return sheet;
            }

            if (File.Exists(IO.Paths.GfxPath + "Sprites/Sprite" + num + ".sprite"))
            {
                sheet = new SpriteSheet(num, formString);
                string changedFormString = formString;

                using (FileStream fileStream = File.OpenRead(IO.Paths.GfxPath + "Sprites/Sprite" + num + ".sprite"))
                {
                    using (BinaryReader reader = new BinaryReader(fileStream))
                    {
                        int formCount = reader.ReadInt32();
                        Dictionary<string, int[]> formData = new Dictionary<string, int[]>();

                        for (int i = 0; i < formCount; i++)
                        {
                            // Read the form name
                            string formName = reader.ReadString();

                            int[] formIntData = new int[2];

                            // Load form position
                            formIntData[0] = reader.ReadInt32();

                            // Load form size
                            formIntData[1] = reader.ReadInt32();

                            // Add form data to collection
                            formData.Add(formName, formIntData);
                        }

                        while (true)
                        {
                            if (spriteCache.ContainsKey(num + changedFormString))
                            {// this point will be hit if the first fallback data to be found is already in the cache
                                // the cache needs to be updated for aliases, but that's it.  No need to load any new data.
                                sheet = spriteCache.Get(num + changedFormString);
                                break;
                            }
                            else if (formData.ContainsKey(changedFormString) || changedFormString == "r")
                            {// we've found a spritesheet in the file, so load it.
                                int[] formInt = formData[changedFormString];

                                // Jump to the correct position
                                fileStream.Seek(formInt[0], SeekOrigin.Current);

                                sheet.LoadFromData(reader, formInt[1]);

                                spriteCache.Add(num + changedFormString, sheet);

                                break;
                            }

                            // If the form specified wasn't found, continually revert to the backup until only "r" is reached
                            changedFormString = changedFormString.Substring(0, changedFormString.LastIndexOf('-'));
                        }
                    }
                }

                // continually add aliases
                string aliasString = formString;
                while (aliasString != changedFormString)
                {
                    // add aliases here
                    spriteCache.AddAlias(num + aliasString, num + changedFormString);

                    // If the form specified wasn't found, continually revert to the backup until only "r" is reached
                    aliasString = aliasString.Substring(0, aliasString.LastIndexOf('-'));
                }

                // string rootForm = spriteCache.GetOriginalKeyFromAlias(num + formString);
                // if (rootForm != num + formString)
                // {
                //    Logs.Logger.LogDebug("Could not load " + num + formString + ", loaded " + num + rootForm +" instead.");
                // }
                return sheet;
            }
            else
            {
                // watch.Stop();
                // System.Diagnostics.Debug.WriteLine("PkMn #"+num+"("+form+"/"+shiny+"/"+gender+") failed retrieval in "+watch.ElapsedMilliseconds);
                return null;
            }
        }

        public static SpellSheet GetSpellSheet(Enums.StationaryAnimType animType, int num, bool semiTransparent)
        {
            lock (spellCache)
            {
                if (spellCache.ContainsKey(animType.ToString() + "-" + num + "-" + semiTransparent.ToIntString()))
                {
                    return spellCache[animType.ToString() + "-" + num + "-" + semiTransparent.ToIntString()];
                }
else
                {
                    if (File.Exists(IO.Paths.GfxPath + "Spells/" + animType.ToString() + "-" + num + ".png"))
                    {
                        Surface surf;

                        if (semiTransparent)
                        {
                            surf = SurfaceManager.LoadSurface(IO.Paths.GfxPath + "Spells/" + animType.ToString() + "-" + num + ".png", false, true);
                            surf.Transparent = true;
                            surf.TransparentColor = Color.Black;
                            surf.AlphaBlending = true;
                            surf.Alpha = 128;
                        }
                        else
                        {
                            surf = SurfaceManager.LoadSurface(IO.Paths.GfxPath + "Spells/" + animType.ToString() + "-" + num + ".png");
                            surf.Transparent = true;
                        }

                        int bytesUsed = surf.Width * surf.Height * surf.BitsPerPixel / 8;
                        SpellSheet sheet = new SpellSheet(surf, bytesUsed);
                        spellCache.Add(animType.ToString() + "-" + num + "-" + semiTransparent.ToIntString(), sheet);
                        return sheet;
                    }
else
                    {
                        return null;
                    }
                }
            }
        }

        public static SpellSheet GetEmoteSheet(int num)
        {
            lock (spellCache)
            {
                if (spellCache.ContainsKey("Status-" + num + "-0"))
                {
                    return spellCache["Status-" + num + "-0"];
                }
                else
                {
                    if (File.Exists(IO.Paths.GfxPath + "Status/Status-" + num + ".png"))
                    {
                        Surface surf = SurfaceManager.LoadSurface(IO.Paths.GfxPath + "Status/Status-" + num + ".png");
                        surf.Transparent = true;

                        int bytesUsed = surf.Width * surf.Height * surf.BitsPerPixel / 8;
                        SpellSheet sheet = new SpellSheet(surf, bytesUsed);
                        spellCache.Add("Status-" + num + "-0", sheet);
                        return sheet;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        public static Mugshot GetMugshot(int num, string form, int shiny, int gender)
        {
            Initialize();

            (bool Exists, int IndexOfMugshot) testingTuple;
            testingTuple.Exists = false;
            testingTuple.IndexOfMugshot = -1;
            for (int i = 0; i < mugshotCache.Count; i++)
            {
                if (mugshotCache[i].Key == $"{num}:{form}:{gender}")
                {
                    testingTuple.Exists = true;
                    testingTuple.IndexOfMugshot = i;
                    break;
                }
            }

            if (testingTuple.Exists)
            {
                return mugshotCache[testingTuple.IndexOfMugshot].Value;
            }
            else
            {
                mugshotCache.Add(($"{num}:{form}:{gender}", new Mugshot(num, form, gender)));
                return mugshotCache[mugshotCache.Count - 1].Value;
            }

            /*
            string formString = "r";

            if (form >= 0)
            {
                formString += "-" + form;
                if (shiny >= 0)
                {
                    formString += "-" + shiny;
                    if (gender >= 0)
                    {
                        formString += "-" + gender;
                    }
                }
            }

            Mugshot sheet = null;
            lock (mugshotCache)
            {
                sheet = mugshotCache.Get(num + formString);
            }

            if (sheet != null)
            {
                return sheet;
            }

            try
            {
                // If we are still here, that means the sprite wasn't in the cache
                if (System.IO.File.Exists(IO.Paths.GfxPath + "Mugshots/Portrait" + num + ".portrait"))
                {
                    sheet = new Mugshot(num, formString);
                    string changedFormString = formString;

                    using (FileStream fileStream = File.OpenRead(IO.Paths.GfxPath + "Mugshots/Portrait" + num + ".portrait"))
                    {
                        using (BinaryReader reader = new BinaryReader(fileStream))
                        {
                            int formCount = reader.ReadInt32();
                            Dictionary<string, int[]> formData = new Dictionary<string, int[]>();

                            for (int i = 0; i < formCount; i++)
                            {
                                // Read the form name
                                string formName = reader.ReadString();

                                int[] formIntData = new int[2];

                                // Load form position
                                formIntData[0] = reader.ReadInt32();

                                // Load form size
                                formIntData[1] = reader.ReadInt32();

                                // Add form data to collection
                                formData.Add(formName, formIntData);
                            }

                            while (true)
                            {
                                if (mugshotCache.ContainsKey(num + changedFormString))
                                {// this point will be hit if the first fallback data to be found is already in the cache
                                 // the cache needs to be updated for aliases, but that's it.  No need to load any new data.
                                    sheet = mugshotCache.Get(num + changedFormString);
                                    break;
                                }
                                else if (formData.ContainsKey(changedFormString) || changedFormString == "r")
                                {// we've found a spritesheet in the file, so load it.
                                    int[] formInt = formData[changedFormString];

                                    // Jump to the correct position
                                    fileStream.Seek(formInt[0], SeekOrigin.Current);

                                    int frameCount = reader.ReadInt32();
                                    int size = reader.ReadInt32();
                                    byte[] imgData = reader.ReadBytes(size);

                                    sheet.LoadFromData(imgData);

                                    mugshotCache.Add(num + changedFormString, sheet);

                                    break;
                                }

                                // If the form specified wasn't found, continually revert to the backup until only "r" is reached
                                changedFormString = changedFormString.Substring(0, changedFormString.LastIndexOf('-'));
                            }
                        }
                    }

                    // continually add aliases
                    string aliasString = formString;
                    while (aliasString != changedFormString)
                    {
                        if (!mugshotCache.ContainsKey(num + aliasString))
                        {
                            // add aliases here
                            mugshotCache.AddAlias(num + aliasString, num + changedFormString);

                            // If the form specified wasn't found, continually revert to the backup until only "r" is reached
                            aliasString = aliasString.Substring(0, aliasString.LastIndexOf('-'));
                        }
                    }

                    return sheet;
                }

                else
                {
                    return null;
                }
            }
            catch
            {
                // Weird issue where "Key cannot be added" but the sheet still is valid.
                return sheet;
            }*/
        }

        private static FtpWebRequest CreateFtpWebRequest(string ftpDirectoryPath, string userName, string password, bool keepAlive = false)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(ftpDirectoryPath));

            // Set proxy to null. Under current configuration if this option is not set then the proxy that is used will get an html response from the web content gateway (firewall monitoring system)
            request.Proxy = null;

            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = keepAlive;

            request.Credentials = new NetworkCredential(userName, password);

            return request;
        }
    }
}