﻿// <copyright file="DuskOverlay.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Overlays
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using SdlDotNet.Graphics;

    internal class DuskOverlay : IOverlay
    {
        private readonly Surface buffer;
        private bool disposed;

        public DuskOverlay()
        {
            this.disposed = false;
            this.buffer = new Surface(20 * Constants.TILEWIDTH, 15 * Constants.TILEHEIGHT);
            for (int x = 0; x < 20; x++)
            {
                for (int y = 0; y < 15; y++)
                {
                    this.buffer.Blit(GraphicsManager.Tiles[10][63], new Point(x * Constants.TILEWIDTH, y * Constants.TILEHEIGHT));
                }
            }

            this.buffer.AlphaBlending = true;
            this.buffer.Alpha = 60;
        }

        public Surface Buffer
        {
            get { return this.buffer; }
        }

        /// <inheritdoc/>
        public bool Disposed
        {
            get { return this.disposed; }
        }

        /// <inheritdoc/>
        public void FreeResources()
        {
            this.disposed = true;
            this.buffer.Dispose();
        }

        /// <inheritdoc/>
        public void Render(Renderers.RendererDestinationData destData, int tick)
        {
            // We don't need to render anything as this overlay isn't animated and always remains the same
            destData.Blit(this.buffer, new Point(0, 0));
        }
    }
}