﻿// <copyright file="DarknessOverlay.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Overlays
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    using SdlDotNet.Graphics;

    internal class DarknessOverlay : IOverlay
    {
        private readonly Surface buffer;
        private bool disposed;

        private readonly int range;

        public DarknessOverlay()
        {
            this.disposed = false;
            this.buffer = new Surface(20 * Constants.TILEWIDTH, 15 * Constants.TILEHEIGHT);
            for (int x = 0; x < 20; x++)
            {
                for (int y = 0; y < 15; y++)
                {
                    this.buffer.Blit(GraphicsManager.Tiles[10][45], new Point(x * Constants.TILEWIDTH, y * Constants.TILEHEIGHT));
                }
            }

            this.buffer.AlphaBlending = true;
            this.buffer.Alpha = 120;
        }

        public DarknessOverlay(int newRange)
        {
            this.disposed = false;
            this.range = newRange;
            this.buffer = new Surface(40 * Constants.TILEWIDTH, 30 * Constants.TILEHEIGHT);
            Surface holepart = new Surface(14 * Constants.TILEWIDTH, 14 * Constants.TILEHEIGHT);
            for (int x = 0; x < 14; x++)
            {
                for (int y = 0; y < 14; y++)
                {
                    holepart.Blit(GraphicsManager.Tiles[10][210 + x + (14 * y)], new Point(x * Constants.TILEWIDTH, y * Constants.TILEHEIGHT));
                }
            }

            Surface hole = new Surface(28 * Constants.TILEWIDTH, 28 * Constants.TILEHEIGHT);
            hole.Blit(holepart, new Point(0,0));
            hole.Blit(holepart.CreateFlippedHorizontalSurface(), new Point(hole.Width / 2, -2));
            hole.Blit(holepart.CreateFlippedVerticalSurface(), new Point(0, hole.Height / 2));
            hole.Blit(holepart.CreateFlippedVerticalSurface().CreateFlippedHorizontalSurface(), new Point(hole.Width / 2, (hole.Height / 2) - 2));
            hole = hole.CreateStretchedSurface(new Size(this.range * Constants.TILEWIDTH, this.range * Constants.TILEHEIGHT));

            this.buffer.Blit(GraphicsManager.Tiles[10][45].CreateStretchedSurface(new Size(40 * Constants.TILEWIDTH, 30 * Constants.TILEHEIGHT)), new Point(0,0));
            this.buffer.Blit(hole, new Point((this.buffer.Width / 2) - (hole.Width / 2), (this.buffer.Height / 2) - (hole.Height / 2)));

            // buffer = hole;
            this.buffer.Transparent = true;

            this.buffer.AlphaBlending = true;
            this.buffer.Alpha = 180;
        }

        public Surface Buffer
        {
            get { return this.buffer; }
        }

        /// <inheritdoc/>
        public bool Disposed
        {
            get { return this.disposed; }
        }

        public Point Focus { get; set; }

        public int Range
        {
            get { return this.range; }
        }

        /// <inheritdoc/>
        public void FreeResources()
        {
            this.disposed = true;
            this.buffer.Dispose();
        }

        /// <inheritdoc/>
        public void Render(Renderers.RendererDestinationData destData, int tick)
        {
            // We don't need to render anything as this overlay isn't animated and always remains the same
            destData.Blit(this.buffer, new Point(0, 0));
        }

        public void Render(Renderers.RendererDestinationData destData, int tick, Point focus)
        {
            // We don't need to render anything as this overlay isn't animated and always remains the same
            destData.Blit(this.buffer, new Point(focus.X - (this.buffer.Width / 2), focus.Y - (this.buffer.Height / 2)));
        }
    }
}