﻿// <copyright file="MapChangeInfoOverlay.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Overlays.ScreenOverlays
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using PMU.Core;
    using SdlDotNet.Graphics;
    using SdlDotNet.Widgets;

    internal class MapChangeInfoOverlay : IOverlay
    {
        private readonly Surface buffer;
        private bool disposed;
        private readonly SdlDotNet.Graphics.Font textFont;
        private readonly TickCount tickCount;
        private readonly int minDisplayTime;
        private readonly string mapName;

        public bool MinTimePassed { get; set; }

        public MapChangeInfoOverlay(string mapName, int minDisplayTime)
        {
            this.disposed = false;

            this.mapName = mapName;
            this.textFont = FontManager.LoadFont("PMU", 36);
            this.buffer = new Surface(20 * Constants.TILEWIDTH, 15 * Constants.TILEHEIGHT);
            this.buffer.Fill(Color.Black);
            Surface textSurf = TextRenderer.RenderTextBasic(this.textFont, mapName, null, Color.WhiteSmoke, false, 0, 0, 0, 0);
            this.buffer.Blit(textSurf, new Point(Graphic.DrawingSupport.GetCenterX(this.buffer.Width, textSurf.Width), 100));
            this.minDisplayTime = minDisplayTime;
            this.tickCount = new TickCount(SdlDotNet.Core.Timer.TicksElapsed);

            // TextRenderer.RenderText(buffer, textFont, mapName, Color.WhiteSmoke, true, 0, 0, 100, 50);
            // for (int x = 0; x < 20; x++) {
            //    for (int y = 0; y < 15; y++) {

            // buffer.Blit(GraphicsManager.Tiles[10][59 + (x % 2) + 2 * (y % 2)], new Point(x * Constants.TILE_WIDTH, y * Constants.TILE_HEIGHT));
            //    }
            // }
            // buffer.AlphaBlending = true;
            // buffer.Alpha = 50;
        }

        public Surface Buffer
        {
            get { return this.buffer; }
        }

        /// <inheritdoc/>
        public bool Disposed
        {
            get { return this.disposed; }
        }

        /// <inheritdoc/>
        public void FreeResources()
        {
            this.disposed = true;
            this.buffer.Dispose();
        }

        /// <inheritdoc/>
        public void Render(Renderers.RendererDestinationData destData, int tick)
        {
            // We don't need to render anything as this overlay isn't animated and always remains the same
            destData.Blit(this.buffer, new Point(0, 0));
            if (tick > this.tickCount.Tick + this.minDisplayTime)
            {
                // if (Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.Map.Loaded && Graphic.Renderers.Screen.ScreenRenderer.RenderOptions.Map == Maps.MapHelper.Maps[Enums.MapID.TempActive]) {
                    if (Renderers.Screen.ScreenRenderer.RenderOptions.Map.Name == this.mapName)
                    {
                        Renderers.Screen.ScreenRenderer.RenderOptions.ScreenOverlay = null;
                    }

                // }
                this.MinTimePassed = true;
            }
        }
    }
}
