﻿// <copyright file="IOverlay.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Overlays
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SdlDotNet.Graphics;

    internal interface IOverlay
    {
        bool Disposed { get; }

        void Render(Renderers.RendererDestinationData destData, int tick);

        void FreeResources();
    }
}
