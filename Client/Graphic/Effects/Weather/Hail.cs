﻿// <copyright file="Hail.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Weather
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Overlays;
    using SdlDotNet.Graphics;
    using SdlDotNet.Graphics.Sprites;

    internal class Hail : IWeather
    {
        private bool disposed;

        private readonly List<Hailstone> hailstones = new List<Hailstone>();

        public Hail()
        {
            this.disposed = false;

            for (int i = 0; i < 50; i++)
            {
                this.hailstones.Add(new Hailstone());
            }
        }

        /// <inheritdoc/>
        public bool Disposed
        {
            get { return this.disposed; }
        }

        /// <inheritdoc/>
        public void FreeResources()
        {
            this.disposed = true;
            for (int i = this.hailstones.Count - 1; i >= 0; i--)
            {
                this.hailstones[i].Dispose();
                this.hailstones.RemoveAt(i);
            }
        }

        /// <inheritdoc/>
        public void Render(Renderers.RendererDestinationData destData, int tick)
        {
            for (int i = 0; i < this.hailstones.Count; i++)
            {
                this.hailstones[i].UpdateLocation();
                destData.Blit(this.hailstones[i], new Point(this.hailstones[i].X, this.hailstones[i].Y));
            }
        }

        /// <inheritdoc/>
        public Enums.Weather ID
        {
            get { return Enums.Weather.Hail; }
        }
    }
}
