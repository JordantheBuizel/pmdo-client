﻿// <copyright file="Rain.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Weather
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Overlays;
    using SdlDotNet.Graphics;

    internal class Rain : IWeather
    {
        private bool disposed;
        private readonly List<Raindrop> raindrops = new List<Raindrop>();

        public Rain()
        {
            this.disposed = false;
            for (int i = 0; i < 80; i++)
            {
                this.raindrops.Add(new Raindrop());
            }
        }

        /// <inheritdoc/>
        public bool Disposed
        {
            get { return this.disposed; }
        }

        /// <inheritdoc/>
        public void FreeResources()
        {
            this.disposed = true;
            for (int i = this.raindrops.Count - 1; i >= 0; i--)
            {
                this.raindrops[i].Dispose();
                this.raindrops.RemoveAt(i);
            }
        }

        /// <inheritdoc/>
        public void Render(Renderers.RendererDestinationData destData, int tick)
        {
            for (int i = 0; i < this.raindrops.Count; i++)
            {
                this.raindrops[i].UpdateLocation(1);
                destData.Blit(this.raindrops[i], new Point(this.raindrops[i].X, this.raindrops[i].Y));
            }
        }

        /// <inheritdoc/>
        public Enums.Weather ID
        {
            get { return Enums.Weather.Raining; }
        }
    }
}
