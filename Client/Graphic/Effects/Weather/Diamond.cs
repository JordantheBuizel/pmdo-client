﻿// <copyright file="Diamond.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Weather
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using SdlDotNet.Core;
    using SdlDotNet.Graphics;
    using SdlDotNet.Graphics.Sprites;

    internal class Diamond : Surface
    {
        private float speed;
        private float wind;
        private readonly float delta = 0.05f;

        public int X;
        public int Y;

        /// <summary>
        /// Initializes a new instance of the <see cref="Diamond"/> class.
        ///
        /// </summary>
        public Diamond()
            : base(new Surface(32, 32))
        {
            this.Initialize();
            this.Reset();
            this.Y = -1 * MathFunctions.Random.Next(500 - this.Height);
        }

        private new void Initialize()
        {
            this.Blit(GraphicsManager.Tiles[10][197], new Point(0, 0));
            this.Transparent = true;

            // base.Surface.TransparentColor = Color.FromArgb(255, 0, 255);
            // base.Rectangle = new Rectangle(this.Width, this.Height, 0, 0);
        }

        private void Reset()
        {
            this.wind = MathFunctions.Random.Next(3) / 30.0f;

            this.X = (int)MathFunctions.Random.Next(-1 * (int)(this.wind * 640), 640 - this.Width);
            this.Y = 0 - this.Width;

            this.speed = MathFunctions.Random.Next(40, 120);

            this.AlphaBlending = true;
            this.Alpha = 0;
        }

        /// <summary>
        /// Updates the diamonds onscreen
        /// </summary>
        public void UpdateLocation()
        {
            float change = this.delta * this.speed;

            // base.Alpha = (byte)((this.Y % 64)/ 4 /* 16 + 1*/);
            switch ((this.Y % 64) / 4)
            {
                case 0:
                    {
                        this.Alpha = 255;
                    }

                    break;
                case 15: case 1:
                    {
                        this.Alpha = 192;
                    }

                    break;
                case 14: case 2:
                    {
                        this.Alpha = 128;
                    }

                    break;
                default:
                    {
                        this.Alpha = 0;
                    }

                    break;
            }

            // base.Alpha = (byte)System.Math.Max(0, (System.Math.Abs(this.Y % 64 - 32) - 24)* 32 - 1);
            this.Y += (int)change;
            this.X += (int)Math.Ceiling(change * this.wind);

            if (this.Y > 480)
            {
                this.Reset();
            }
        }

        private bool disposed;

        /// <summary>
        /// Destroys the surface object and frees its memory
        /// </summary>
        /// <param name="disposing">If true, dispose unmanaged resources</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                    }

                    this.disposed = true;
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }
    }
}
