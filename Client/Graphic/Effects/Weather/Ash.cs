﻿// <copyright file="Ash.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Weather
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using SdlDotNet.Core;
    using SdlDotNet.Graphics;
    using SdlDotNet.Graphics.Sprites;

    internal class Ash : Surface
    {
        public int X;
        public int Y;

        private float speed;
        private float wind;
        private readonly float delta = 0.05f;

        /// <summary>
        /// Initializes a new instance of the <see cref="Ash"/> class.
        /// </summary>
        public Ash()
            : base(new Surface(32, 32))
            {
            this.Initialize();
            this.Reset();
            this.Y = -1 * MathFunctions.Random.Next(500 - this.Height);
        }

        private new void Initialize()
        {
            this.Blit(GraphicsManager.Tiles[10][14], new Point(0, 0));
            this.Transparent = true;

            // base.Surface.TransparentColor = Color.FromArgb(255, 0, 255);
            // base.Rectangle = new Rectangle(this.Width, this.Height, 0, 0);
        }

        private void Reset()
        {
            this.wind = MathFunctions.Random.Next(3) / 10.0f;

            this.X = (int)MathFunctions.Random.Next(-1 * (int)(this.wind * 640), 640 - this.Width);
            this.Y = 0 - this.Width;

            this.speed = MathFunctions.Random.Next(50, 150);

            // base.Surface.Alpha =
            //    (byte)((150 - 50) / (speed - 50) * -255);
            // base.Surface.AlphaBlending = true;
        }

        /// <summary>
        /// Updates ash ketchum onscreen
        /// </summary>
        public void UpdateLocation()
        {
            float change = this.delta * this.speed;

            this.Y += (int)change;
            this.X += (int)Math.Ceiling(change * this.wind);

            if (this.Y > 480)
            {
                this.Reset();
            }
        }

        private bool disposed;

        /// <summary>
        /// Destroys the surface object and frees its memory
        /// </summary>
        /// <param name="disposing">If true, dispose unmanaged resources</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                    }

                    this.disposed = true;
                }
            }
finally
            {
                base.Dispose(disposing);
            }
        }
    }
}
