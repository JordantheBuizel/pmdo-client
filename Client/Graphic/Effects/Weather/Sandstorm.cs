﻿// <copyright file="Sandstorm.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Weather
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Overlays;
    using SdlDotNet.Graphics;
    using SdlDotNet.Graphics.Sprites;

    internal class Sandstorm : IWeather
    {
        private readonly Surface buffer;
        private bool disposed;
        private int x;

        public Sandstorm()
        {
            this.disposed = false;
            this.buffer = new Surface(24 * Constants.TILEWIDTH, 15 * Constants.TILEHEIGHT);
            for (int x = 0; x < 24; x++)
            {
                for (int y = 0; y < 15; y++)
                {
                    this.buffer.Blit(GraphicsManager.Tiles[10][76 + (14 * (x % 4))], new Point(x * Constants.TILEWIDTH, y * Constants.TILEHEIGHT));
                }
            }

            this.buffer.AlphaBlending = true;
            this.buffer.Alpha = 160;
            this.x = 0;
        }

        public Surface Buffer
        {
            get { return this.buffer; }
        }

        /// <inheritdoc/>
        public bool Disposed
        {
            get { return this.disposed; }
        }

        /// <inheritdoc/>
        public void FreeResources()
        {
            this.disposed = true;
            this.buffer.Dispose();
        }

        /// <inheritdoc/>
        public void Render(Renderers.RendererDestinationData destData, int tick)
        {
            // We don't need to render anything as this overlay isn't animated and always remains the same
            this.x = (this.x + 4) % 128;
            destData.Blit(this.buffer, new Point(-128 + this.x, 0));
        }

        /// <inheritdoc/>
        public Enums.Weather ID
        {
            get { return Enums.Weather.Sandstorm; }
        }
    }
}
