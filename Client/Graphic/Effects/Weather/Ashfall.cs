﻿// <copyright file="Ashfall.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Weather
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Overlays;
    using SdlDotNet.Graphics;
    using SdlDotNet.Graphics.Sprites;

    internal class Ashfall : IWeather
    {
        private bool disposed;

        private readonly List<Ash> ashes = new List<Ash>();

        public Ashfall()
        {
            this.disposed = false;

            for (int i = 0; i < 40; i++)
            {
                this.ashes.Add(new Ash());
            }
        }

        /// <inheritdoc/>
        public bool Disposed
        {
            get { return this.disposed; }
        }

        /// <inheritdoc/>
        public void FreeResources()
        {
            this.disposed = true;
            for (int i = this.ashes.Count - 1; i >= 0; i--)
            {
                this.ashes[i].Dispose();
                this.ashes.RemoveAt(i);
            }
        }

        /// <inheritdoc/>
        public void Render(Renderers.RendererDestinationData destData, int tick)
        {
            for (int i = 0; i < this.ashes.Count; i++)
            {
                this.ashes[i].UpdateLocation();
                destData.Blit(this.ashes[i], new Point(this.ashes[i].X, this.ashes[i].Y));
            }
        }

        /// <inheritdoc/>
        public Enums.Weather ID
        {
            get { return Enums.Weather.Ashfall; }
        }
    }
}
