﻿// <copyright file="Sunny.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Weather
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Overlays;
    using SdlDotNet.Graphics;
    using SdlDotNet.Graphics.Sprites;

    internal class Sunny : IWeather
    {
        private readonly Surface[] buffer;
        private bool disposed;

        public Sunny()
        {
            this.disposed = false;

            this.buffer = new Surface[6];

            for (int i = 0; i < this.buffer.Length; i++)
            {
                int height = 8;
                int divide = 2;
                this.buffer[i] = new Surface(20 * Constants.TILEWIDTH, height * Constants.TILEHEIGHT / ((int)Math.Pow(divide, i)));
                for (int x = 0; x < 20; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        this.buffer[i].Blit(GraphicsManager.Tiles[10][7/*132 + 14 * (x % 4)*/], new Point(x * Constants.TILEWIDTH, y * Constants.TILEHEIGHT / ((int)Math.Pow(divide, i))));
                    }
                }

                this.buffer[i].AlphaBlending = true;

                this.buffer[i].Alpha = 32;
            }
        }

        public Surface Buffer
        {
            get { return this.buffer[0]; }
        }

        /// <inheritdoc/>
        public bool Disposed
        {
            get { return this.disposed; }
        }

        /// <inheritdoc/>
        public void FreeResources()
        {
            this.disposed = true;
            for (int i = 0; i < this.buffer.Length; i++)
            {
                this.buffer[i].Dispose();
            }
        }

        /// <inheritdoc/>
        public void Render(Renderers.RendererDestinationData destData, int tick)
        {
            // We don't need to render anything as this overlay isn't animated and always remains the same
            int add = (((tick / 4) % 8) - 4) * 6;
            if (add < 0)
            {
                add *= -1;
            }

            for (int i = 0; i < this.buffer.Length; i++)
            {
                this.buffer[i].Alpha = (byte)(32 + add);
                destData.Blit(this.buffer[i], new Point(0, 0));
            }
        }

        /// <inheritdoc/>
        public Enums.Weather ID
        {
            get { return Enums.Weather.Sunny; }
        }
    }
}
