﻿// <copyright file="DiamondDust.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Weather
{
    // Copyright (C) 2016 JordantheBuizel, PMDO Staff
    // Mystery Dungeon eXtended is free software: you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation, either version 3 of the License, or
    // (at your option) any later version.
    // Mystery Dungeon eXtended is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.
    // <http://www.gnu.org/licenses/>.
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Overlays;
    using SdlDotNet.Graphics;
    using SdlDotNet.Graphics.Sprites;

    internal class DiamondDust : IWeather
    {
        private bool disposed;

        private readonly List<Diamond> diamonds = new List<Diamond>();

        public DiamondDust()
        {
            this.disposed = false;

            for (int i = 0; i < 80; i++)
            {
                this.diamonds.Add(new Diamond());
            }
        }

        /// <inheritdoc/>
        public bool Disposed
        {
            get { return this.disposed; }
        }

        /// <inheritdoc/>
        public void FreeResources()
        {
            this.disposed = true;
            for (int i = this.diamonds.Count - 1; i >= 0; i--)
            {
                this.diamonds[i].Dispose();
                this.diamonds.RemoveAt(i);
            }
        }

        /// <inheritdoc/>
        public void Render(Renderers.RendererDestinationData destData, int tick)
        {
            for (int i = 0; i < this.diamonds.Count; i++)
            {
                this.diamonds[i].UpdateLocation();
                destData.Blit(this.diamonds[i], new Point(this.diamonds[i].X, this.diamonds[i].Y));
            }
        }

        /// <inheritdoc/>
        public Enums.Weather ID
        {
            get { return Enums.Weather.DiamondDust; }
        }
    }
}
