﻿// <copyright file="Snowstorm.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Weather
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Overlays;
    using SdlDotNet.Graphics;
    using SdlDotNet.Graphics.Sprites;

    internal class Snowstorm : IWeather
    {
        private bool disposed;

        private readonly List<Snowflake> snowflakes = new List<Snowflake>();

        public Snowstorm()
        {
            this.disposed = false;

            for (int i = 0; i < 120; i++)
            {
                this.snowflakes.Add(new Snowflake());
            }
        }

        /// <inheritdoc/>
        public bool Disposed
        {
            get { return this.disposed; }
        }

        /// <inheritdoc/>
        public void FreeResources()
        {
            this.disposed = true;
            for (int i = this.snowflakes.Count - 1; i >= 0; i--)
            {
                this.snowflakes[i].Dispose();
                this.snowflakes.RemoveAt(i);
            }
        }

        /// <inheritdoc/>
        public void Render(Renderers.RendererDestinationData destData, int tick)
        {
            for (int i = 0; i < this.snowflakes.Count; i++)
            {
                this.snowflakes[i].UpdateLocation(5);
                destData.Blit(this.snowflakes[i], new Point(this.snowflakes[i].X, this.snowflakes[i].Y));
            }
        }

        /// <inheritdoc/>
        public Enums.Weather ID
        {
            get { return Enums.Weather.Snowstorm; }
        }
    }
}
