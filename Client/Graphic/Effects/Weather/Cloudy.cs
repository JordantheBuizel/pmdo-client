﻿// <copyright file="Cloudy.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Weather
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Overlays;
    using SdlDotNet.Graphics;
    using SdlDotNet.Graphics.Sprites;

    internal class Cloudy : IWeather
    {
        private readonly Surface buffer;
        private bool disposed;

        public Cloudy()
        {
            this.disposed = false;
            this.buffer = new Surface(20 * Constants.TILEWIDTH, 15 * Constants.TILEHEIGHT);
            for (int x = 0; x < 20; x++)
            {
                for (int y = 0; y < 15; y++)
                {
                    this.buffer.Blit(GraphicsManager.Tiles[10][59 + (x % 2) + (2 * (y % 2))], new Point(x * Constants.TILEWIDTH, y * Constants.TILEHEIGHT));
                }
            }

            this.buffer.AlphaBlending = true;
            this.buffer.Alpha = 50;
        }

        public Surface Buffer
        {
            get { return this.buffer; }
        }

        /// <inheritdoc/>
        public bool Disposed
        {
            get { return this.disposed; }
        }

        /// <inheritdoc/>
        public void FreeResources()
        {
            this.disposed = true;
            this.buffer.Close();
        }

        /// <inheritdoc/>
        public void Render(Renderers.RendererDestinationData destData, int tick)
        {
            // We don't need to render anything as this overlay isn't animated and always remains the same
            destData.Blit(this.buffer, new Point(0, 0));
        }

        /// <inheritdoc/>
        public Enums.Weather ID
        {
            get { return Enums.Weather.Cloudy; }
        }
    }
}
