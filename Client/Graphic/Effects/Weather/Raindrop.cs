﻿// <copyright file="Raindrop.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Weather
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using SdlDotNet.Graphics;

    internal class Raindrop : Surface
    {
        private float speed;
        private float wind;
        private readonly float delta = 0.1f;

        private int x;
        private int y;

        // public int terminalY;
        public Raindrop()
            : base(new Surface(32, 32))
            {
            this.Initialize();
            this.Reset();
            this.Y = -1 * MathFunctions.Random.Next(5000 - this.Height);
        }

        /// <summary>
        /// Updates the location of raindrops
        /// </summary>
        /// <param name="intensity">Intensity of the raindrops</param>
        public void UpdateLocation(int intensity)
        {
            float change = this.delta * this.speed;

            this.X += (int)(change / 2) * intensity;
            this.Y += (int)change * intensity;

            // this.Y += (int)System.Math.Ceiling(change * wind);
            if (this.Y > 480)
            {
                this.Reset();
            }
        }

        private new void Initialize()
        {
            this.Blit(GraphicsManager.Tiles[10][6], new Point(0, 0));
            this.Transparent = true;

            // base.Surface.TransparentColor = Color.FromArgb(255, 0, 255);
            // base.Rectangle = new Rectangle(this.Width, this.Height, 0, 0);
        }

        private void Reset()
        {
            this.wind = MathFunctions.Random.Next(3) / 10.0f;

            this.X = (int)MathFunctions.Random.Next(-1 * (int)(this.wind * 640), 720 - this.Width) - 128;
            this.Y = -64; // (int)Logic.Math.Random.Next(-1 * (int)(wind * 640), 640 - base.Width);//0 - base.Width + (Logic.Math.Random.Next(100));

            // terminalY = (int)Logic.Math.Random.Next(0, 640 - base.Width);
            this.speed = MathFunctions.Random.Next(100, 150);

            // base.Draw(new SdlDotNet.Graphics.Primitives.Line(0, 0, (short)(speed / 6), (short)(speed / 3)), Color.Blue);

            // base.Surface.Alpha =
            //    (byte)((150 - 50) / (speed - 50) * -255);
            // base.Surface.AlphaBlending = true;
        }

        private bool disposed;

        public int Y { get => this.y; set => this.y = value; }

        public int X { get => this.x; set => this.x = value; }

        /// <summary>
        /// Destroys the surface object and frees its memory
        /// </summary>
        /// <param name="disposing">If true, dispose unmanaged resources</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                    }

                    this.disposed = true;
                }
            }
finally
            {
                base.Dispose(disposing);
            }
        }
    }
}
