﻿// <copyright file="IWeather.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic.Effects.Weather
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal interface IWeather : Overlays.IOverlay
    {
        Enums.Weather ID { get; }
    }
}
