﻿// <copyright file="Mugshot.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Humanizer;
    using PMU.Core;
    using SdlDotNet.Graphics;

    internal class Mugshot
    {
        private readonly int num;
        private Surface sheet;
        private Bitmap bitmap;
        private int sizeInBytes;
        private readonly string form;
        private readonly int gender;
        private int frameCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="Mugshot"/> class.
        /// </summary>
        /// <param name="num">Pokemon's Dex Number</param>
        /// <param name="emoteNum">Number of the face to display</param>
        /// <param name="form">Pokemon's form</param>
        /// <param name="gender">0 for male, 1 for female. Does nothing unless gender changes forms.</param>
        public Mugshot(int num, string form, int gender)
        {
            this.num = num;
            this.form = form;
            this.gender = gender;
            this.sheet = null;
        }

        public int Num
        {
            get { return this.num; }
        }

        public Bitmap GetEmoteBitmap(int emoteNum)
        {
            return this.GetEmoteBitmap(emoteNum.ToWords());
        }

        public Bitmap GetEmoteBitmap(string emoteNum)
        {
            if (this.bitmap == null)
            {
                if (string.IsNullOrEmpty(this.form))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
                    using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(doc.SelectNodes($"//{Constants.PokémonNames[this.num]}/{emoteNum}")[0].InnerText)))
                    {
                        this.bitmap = (Bitmap)Image.FromStream(ms);
                        return this.bitmap;
                    }
                }
                else if (this.gender != 0)
                {
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
                        using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(doc.SelectNodes($"//{Constants.PokémonNames[this.num]}/f/{emoteNum}")[0].InnerText)))
                        {
                            this.bitmap = (Bitmap)Image.FromStream(ms);
                            return this.bitmap;
                        }
                    }
                    catch
                    {
                        if (string.IsNullOrEmpty(this.form))
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.Load(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
                            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(doc.SelectNodes($"//{Constants.PokémonNames[this.num]}/{emoteNum}")[0].InnerText)))
                            {
                                this.bitmap = (Bitmap)Image.FromStream(ms);
                                return this.bitmap;
                            }
                        }
                        else
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.Load(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
                            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(doc.SelectNodes($"//{Constants.PokémonNames[this.num]}/{this.form}/{emoteNum}")[0].InnerText)))
                            {
                                this.bitmap = (Bitmap)Image.FromStream(ms);
                                return this.bitmap;
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
                        using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(doc.SelectNodes($"//{Constants.PokémonNames[this.num]}/{this.form}/{emoteNum}")[0].InnerText)))
                        {
                            this.bitmap = (Bitmap)Image.FromStream(ms);
                            return this.bitmap;
                        }
                    }
                    catch
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
                        using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(doc.SelectNodes($"//{Constants.PokémonNames[this.num]}/{emoteNum}")[0].InnerText)))
                        {
                            this.bitmap = (Bitmap)Image.FromStream(ms);
                            return this.bitmap;
                        }
                    }
                }
            }
            else
            {
                return this.bitmap;
            }
        }

        public Surface GetEmote(int emoteNum)
        {
            return this.GetEmote(emoteNum.ToWords());
        }

        public Surface GetEmote(string emoteNum)
        {
            if (this.sheet == null)
            {
                if (string.IsNullOrEmpty(this.form))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
                    using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(doc.SelectNodes($"//{Constants.PokémonNames[this.num]}/{emoteNum}")[0].InnerText)))
                    {
                        this.sheet = new Surface((Bitmap)Image.FromStream(ms));
                        return this.sheet;
                    }
                }
                else if (this.gender != 0)
                {
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
                        using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(doc.SelectNodes($"//{Constants.PokémonNames[this.num]}/f/{emoteNum}")[0].InnerText)))
                        {
                            this.sheet = new Surface((Bitmap)Image.FromStream(ms));
                            return this.sheet;
                        }
                    }
                    catch
                    {
                        if (string.IsNullOrEmpty(this.form))
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.Load(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
                            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(doc.SelectNodes($"//{Constants.PokémonNames[this.num]}/{emoteNum}")[0].InnerText)))
                            {
                                this.sheet = new Surface((Bitmap)Image.FromStream(ms));
                                return this.sheet;
                            }
                        }
                        else
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.Load(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
                            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(doc.SelectNodes($"//{Constants.PokémonNames[this.num]}/{this.form}/{emoteNum}")[0].InnerText)))
                            {
                                this.sheet = new Surface((Bitmap)Image.FromStream(ms));
                                return this.sheet;
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
                        using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(doc.SelectNodes($"//{Constants.PokémonNames[this.num]}/{this.form}/{emoteNum}")[0].InnerText)))
                        {
                            this.sheet = new Surface((Bitmap)Image.FromStream(ms));
                            return this.sheet;
                        }
                    }
                    catch
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
                        using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(doc.SelectNodes($"//{Constants.PokémonNames[this.num]}/{emoteNum}")[0].InnerText)))
                        {
                            this.sheet = new Surface((Bitmap)Image.FromStream(ms));
                            return this.sheet;
                        }
                    }
                }
            }
            else
            {
                return this.sheet;
            }
        }
    }
}
