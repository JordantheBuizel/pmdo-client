﻿// <copyright file="FrameTypeHelper.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Graphic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using PMU.Core;
    using SdlDotNet.Graphics;

    internal class FrameTypeHelper
    {
        public static bool IsFrameTypeDirectionless(FrameType frameType)
        {
            switch (frameType)
            {
                case FrameType.Sleep:
                    return true;
                default:
                    return false;
            }
        }
    }
}