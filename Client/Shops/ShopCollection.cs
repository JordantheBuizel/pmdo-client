﻿// <copyright file="ShopCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Shops
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class ShopCollection
    {
        private readonly Shop[] mShops;

        internal ShopCollection(int maxShops)
        {
            this.mShops = new Shop[maxShops];
        }

        public Shop this[int index]
        {
            get { return this.mShops[index]; }
            set { this.mShops[index] = value; }
        }
    }
}