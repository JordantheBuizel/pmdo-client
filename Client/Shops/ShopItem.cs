﻿// <copyright file="ShopItem.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Shops
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class ShopItem
    {
        public int GetItem
        {
            get; set;
        }

        // public int GetValue
        // {
        //    get; set;
        // }
        public int GiveItem
        {
            get; set;
        }

        public int GiveValue
        {
            get; set;
        }
    }
}