﻿// <copyright file="Shop.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Shops
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Shop
    {
        public Shop()
        {
            this.Items = new ShopItem[MaxInfo.MAXTRADES];
            for (int i = 0; i < MaxInfo.MAXTRADES; i++)
            {
                this.Items[i] = new ShopItem();
            }
        }

        public ShopItem[] Items
        {
            get; set;
        }

        public bool FixesItems
        {
            get; set;
        }

        public string JoinSay
        {
            get; set;
        }

        public string LeaveSay
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
    }
}