﻿// <copyright file="Tools.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using System.Windows.Forms;

    /// <summary>
    /// Provides misc. methods
    /// </summary>
    public class Tools
    {
        /// <summary>
        /// Displays a messagebox.
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="text">The text.</param>
        /// <param name="buttons">The buttons.</param>
        /// <param name="icon">The icon.</param>
        public static void MessageBox(string caption, string text, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            System.Windows.Forms.MessageBox.Show(text, caption, buttons, icon);
        }

        /// <summary>
        /// Displays a messagebox.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="buttons">The buttons.</param>
        /// <param name="icon">The icon.</param>
        public static void MessageBox(string text, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            MessageBox("Pokémon Mystery Dungeon Online", text, buttons, icon);
        }

        /// <summary>
        /// Displays a messagebox.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="buttons">The buttons.</param>
        public static void MessageBox(string text, MessageBoxButtons buttons)
        {
            MessageBox("Pokémon Mystery Dungeon Online", text, buttons, MessageBoxIcon.None);
        }

        /// <summary>
        /// Displays a messagebox.
        /// </summary>
        /// <param name="text">The text.</param>
        public static void MessageBox(string text)
        {
            MessageBox("Pokémon Mystery Dungeon Online", text, MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        /// <summary>
        /// Crops a bitmap from the specified bitmap.
        /// </summary>
        /// <param name="surfaceToCrop">The bitmap to crop.</param>
        /// <param name="cropRectangle">The rectangle bounds to crop.</param>
        /// <returns>The cropped bitmap.</returns>
        public static Bitmap CropImage(Bitmap bitmapToCrop, Rectangle cropRectangle)
        {
            Bitmap returnSurf = new Bitmap(cropRectangle.Size.Width, cropRectangle.Size.Height);
            using (Graphics g = Graphics.FromImage(returnSurf))
            {
                g.DrawImage(bitmapToCrop, 0, 0, cropRectangle, GraphicsUnit.Pixel);
                return returnSurf;
            }
        }

        /// <summary>
        /// Combines two surfaces together.
        /// </summary>
        /// <param name="bottomImage">The surface that will be used as the background.</param>
        /// <param name="topImage">The surface that will be used as the foreground.</param>
        /// <returns>The combined surface.</returns>
        public static SdlDotNet.Graphics.Surface CombineImage(SdlDotNet.Graphics.Surface bottomImage, SdlDotNet.Graphics.Surface topImage)
        {
            SdlDotNet.Graphics.Surface returnSurf = new SdlDotNet.Graphics.Surface(new Size(Math.Max(bottomImage.Width, topImage.Width), Math.Max(bottomImage.Height, topImage.Height)));
            returnSurf.Blit(bottomImage, new Point(0, 0));
            returnSurf.Blit(topImage, new Point(0, 0));
            return returnSurf;
        }
    }
}