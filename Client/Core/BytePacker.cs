﻿// <copyright file="BytePacker.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Core
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class BytePacker
    {
        private readonly List<BytePackerItem> items;

        public BytePacker()
        {
            this.items = new List<BytePackerItem>();
        }

        public List<BytePackerItem> Items
        {
            get { return this.items; }
        }

        public void AddItem(int highestRangeValue, int value)
        {
            this.items.Add(new BytePackerItem(highestRangeValue, value));
        }

        public int PackItems()
        {
            int baseNumber = 1;
            int packedNumber = 0;
            for (int i = 0; i < this.items.Count; i++)
            {
                packedNumber += this.items[i].Value * baseNumber;
                baseNumber *= this.items[i].HighestRangeValue;
            }

            return packedNumber;
        }
    }
}