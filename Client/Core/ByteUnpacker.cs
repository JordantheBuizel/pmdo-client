﻿// <copyright file="ByteUnpacker.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Core
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ByteUnpacker
    {
        private readonly List<int> items;
        private List<BytePackerItem> unpackedItems;

        public ByteUnpacker()
        {
            this.items = new List<int>();
        }

        public List<int> Items
        {
            get { return this.items; }
        }

        public List<BytePackerItem> UnpackedItems
        {
            get { return this.unpackedItems; }
        }

        public void AddRange(int highestRangeValue)
        {
            this.items.Add(highestRangeValue);
        }

        public List<BytePackerItem> UnpackByte(int packedValue)
        {
            this.unpackedItems = new List<BytePackerItem>();
            int baseNumber = 1;
            for (int i = 0; i < this.items.Count; i++)
            {
                this.unpackedItems.Add(new BytePackerItem(this.items[i], (packedValue % (this.items[i] * baseNumber)) / baseNumber));
                baseNumber *= this.items[i];
            }

            return this.unpackedItems;
        }
    }
}