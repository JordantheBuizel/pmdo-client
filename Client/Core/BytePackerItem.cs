﻿// <copyright file="BytePackerItem.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Core
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class BytePackerItem
    {
        public BytePackerItem(int highestRangeValue, int value)
        {
            this.HighestRangeValue = highestRangeValue;
            this.Value = value;
        }

        public int HighestRangeValue
        {
            get;
            set;
        }

        public int Value
        {
            get;
            set;
        }
    }
}