﻿// <copyright file="ArrowHelper.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Arrows
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;

    internal class ArrowHelper
    {
        private static int dataLoadPercent = 0;
        private static ArrowCollection mArrows;

        public static ArrowCollection Arrows
        {
            get { return mArrows; }
        }

        public static int DataLoadPercent
        {
            get { return dataLoadPercent; }
        }

        public static void InitArrowCollection()
        {
            mArrows = new ArrowCollection(MaxInfo.MAXARROWS + 2);
        }

        public static void LoadArrowsFromPacket(string[] parse)
        {
            try
            {
                int n = 1;
                for (int i = 0; i < MaxInfo.MAXARROWS; i++)
                {
                    dataLoadPercent = Math.Min(99, Logic.MathFunctions.CalculatePercent(i, MaxInfo.MAXARROWS));
                    mArrows[i] = new Arrow();
                    mArrows[i].Name = parse[n + 1];
                    mArrows[i].Pic = parse[n + 2].ToInt();
                    mArrows[i].Range = parse[n + 3].ToInt();
                    mArrows[i].Amount = parse[n + 4].ToInt();
                    n += 5;
                }

                dataLoadPercent = 100;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionHandler.OnException(ex);
            }
        }
    }
}