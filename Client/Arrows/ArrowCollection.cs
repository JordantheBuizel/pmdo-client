﻿// <copyright file="ArrowCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Arrows
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class ArrowCollection
    {
        private readonly Arrow[] mArrows;

        internal ArrowCollection(int maxArrows)
        {
            this.mArrows = new Arrow[maxArrows];
        }

        public Arrow this[int index]
        {
            get { return this.mArrows[index]; }
            set { this.mArrows[index] = value; }
        }
    }
}