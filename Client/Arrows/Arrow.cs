﻿// <copyright file="Arrow.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Arrows
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Arrow
    {
        public int Amount
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public int Pic
        {
            get; set;
        }

        public int Range
        {
            get; set;
        }
    }
}