﻿// <copyright file="EvolutionCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Evolutions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class EvolutionCollection
    {
        private readonly Evolution[] mEvos;

        internal EvolutionCollection(int maxEvos)
        {
            this.mEvos = new Evolution[maxEvos];
        }

        public Evolution this[int index]
        {
            get { return this.mEvos[index]; }
            set { this.mEvos[index] = value; }
        }
    }
}