﻿// <copyright file="Evolution.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Evolutions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Evolution
    {
        public string Name {get; set; }

        public int Species {get; set; }

        public List<EvolutionBranch> Branches { get; set; }

        public Evolution()
        {
            // if (!splitEvo) {
            //    splitEvos = new Evolution[1];
            //    splitEvos[0] = new Evolution(true);
            // }
            this.Branches = new List<EvolutionBranch>();
        }
    }
}