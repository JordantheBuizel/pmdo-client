﻿// <copyright file="EvolutionBranch.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Evolutions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Description of EvolutionBranch.
    /// </summary>
    internal class EvolutionBranch
    {
        public string Name {get; set; }

        public int NewSpecies { get; set; }

        public int ReqScript { get; set; }

        public int Data1 { get; set; }

        public int Data2 { get; set; }

        public int Data3 { get; set; }

        public EvolutionBranch()
        {
            this.Name = string.Empty;
        }
    }
}
