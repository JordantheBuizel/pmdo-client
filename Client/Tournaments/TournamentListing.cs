﻿// <copyright file="TournamentListing.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Tournaments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class TournamentListing
    {
        private readonly string tournamentID;
        private readonly string name;
        private readonly string mainAdmin;

        public string Name
        {
            get { return this.name; }
        }

        public string TournamentID
        {
            get { return this.tournamentID; }
        }

        public string MainAdmin
        {
            get { return this.mainAdmin; }
        }

        public TournamentListing(string name, string tournamentID, string mainAdmin)
        {
            this.name = name;
            this.tournamentID = tournamentID;
            this.mainAdmin = mainAdmin;
        }
    }
}
