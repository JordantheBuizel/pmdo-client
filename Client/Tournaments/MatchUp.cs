﻿// <copyright file="MatchUp.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Tournaments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class MatchUp
    {
        private int PlayerOneMugshot { get; set; }

        private int PlayerTwoMugshot { get; set; }
    }
}
