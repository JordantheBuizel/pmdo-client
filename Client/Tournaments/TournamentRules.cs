﻿// <copyright file="TournamentRules.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Tournaments
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class TournamentRules
    {
        public bool SleepClause { get; set; }

        public bool AccuracyClause { get; set; }

        public bool SpeciesClause { get; set; }

        public bool FreezeClause { get; set; }

        public bool OHKOClause { get; set; }

        public bool SelfKOClause { get; set; }
    }
}
