﻿using Client.Logic.Items;

namespace Client.Logic.Menus.InventoryChildren
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class InventoryUseItem : UserControl
    {
        private int itemSlot;
        private readonly bool useable;
        private readonly Label lblHold;
        private readonly Label lblUse;
        private readonly Label lblDrop;
        private readonly Label lblSummary;
        private readonly Label lblThrow;
        private readonly NumericUpDown nudAmount;
        private readonly Widges.MenuItemPicker itemPicker;
        private readonly int maxItems;
        private readonly Font defaultFont = new Font("Tahoma", 15F);

        public InventoryUseItem()
        {
            this.InitializeComponent();

            this.SizeChanged += (sender, e) =>
            {
                this.panel1.Size = this.Size;
            };

            if (ItemHelper.ItemUsable(this.itemSlot))
            {
                // can use item
                this.Size = new Size(150, 150);
                this.maxItems = 4;
                this.useable = true;
            }
            else
            {
                // cannot use item
                this.Size = new Size(150, 165);
                this.maxItems = 3;
                this.useable = false;
            }

            int widgetY = 8;

            this.lblHold = new Label
            {
                Text = "Hold",
                AutoSize = true,
                Font = this.defaultFont,
                Location = new Point(30, widgetY)
            };
            this.lblHold.Click += this.LblHold_Click;

            this.panel1.Controls.Add(this.lblHold);

            widgetY += 24;

            if (this.useable)
            {
                this.lblUse = new Label
                {
                    Font = this.defaultFont,
                    AutoSize = true,
                    Text = "Use",
                    Location = new Point(30, widgetY)
                };
                this.lblUse.Click += this.LblUse_Click;

                this.panel1.Controls.Add(this.lblUse);

                widgetY += 24;
            }

            this.lblThrow = new Label
            {
                AutoSize = true,
                
            };

        }

        public int ItemSlot
        {
            get
            {
                return this.itemSlot;
            }

            set
            {
                this.itemSlot = value;
                this.lblHold.Text = Players.PlayerManager.MyPlayer.GetActiveRecruit().HeldItemSlot == this.itemSlot ? "Take" : "Give";
            }
        }

        public void Show(int slot)
        {
            this.ItemSlot = slot;
            base.Show();
        }

        // Hides default show to prevent accidental display errors
        private new void Show()
        {
        }
    }
}
