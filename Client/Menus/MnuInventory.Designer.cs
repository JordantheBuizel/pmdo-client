﻿namespace Client.Logic.Menus
{
    partial class MnuInventory
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MnuInventory));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbPointer = new System.Windows.Forms.PictureBox();
            this.pbArrowRight = new System.Windows.Forms.PictureBox();
            this.pbArrowLeft = new System.Windows.Forms.PictureBox();
            this.lblPage = new System.Windows.Forms.Label();
            this.lblItemName9 = new System.Windows.Forms.Label();
            this.lblItemName8 = new System.Windows.Forms.Label();
            this.lblItemName7 = new System.Windows.Forms.Label();
            this.lblItemName6 = new System.Windows.Forms.Label();
            this.lblItemName5 = new System.Windows.Forms.Label();
            this.lblItemName4 = new System.Windows.Forms.Label();
            this.lblItemName3 = new System.Windows.Forms.Label();
            this.lblItemName2 = new System.Windows.Forms.Label();
            this.lblItemName1 = new System.Windows.Forms.Label();
            this.lblItemName0 = new System.Windows.Forms.Label();
            this.pbItemIcon9 = new System.Windows.Forms.PictureBox();
            this.pbItemIcon8 = new System.Windows.Forms.PictureBox();
            this.pbItemIcon7 = new System.Windows.Forms.PictureBox();
            this.pbItemIcon6 = new System.Windows.Forms.PictureBox();
            this.pbItemIcon5 = new System.Windows.Forms.PictureBox();
            this.pbItemIcon4 = new System.Windows.Forms.PictureBox();
            this.pbItemIcon3 = new System.Windows.Forms.PictureBox();
            this.pbItemIcon2 = new System.Windows.Forms.PictureBox();
            this.pbItemIcon1 = new System.Windows.Forms.PictureBox();
            this.pbItemIcon0 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPointer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbArrowRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbArrowLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon0)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.pbPointer);
            this.panel1.Controls.Add(this.pbArrowRight);
            this.panel1.Controls.Add(this.pbArrowLeft);
            this.panel1.Controls.Add(this.lblPage);
            this.panel1.Controls.Add(this.lblItemName9);
            this.panel1.Controls.Add(this.lblItemName8);
            this.panel1.Controls.Add(this.lblItemName7);
            this.panel1.Controls.Add(this.lblItemName6);
            this.panel1.Controls.Add(this.lblItemName5);
            this.panel1.Controls.Add(this.lblItemName4);
            this.panel1.Controls.Add(this.lblItemName3);
            this.panel1.Controls.Add(this.lblItemName2);
            this.panel1.Controls.Add(this.lblItemName1);
            this.panel1.Controls.Add(this.lblItemName0);
            this.panel1.Controls.Add(this.pbItemIcon9);
            this.panel1.Controls.Add(this.pbItemIcon8);
            this.panel1.Controls.Add(this.pbItemIcon7);
            this.panel1.Controls.Add(this.pbItemIcon6);
            this.panel1.Controls.Add(this.pbItemIcon5);
            this.panel1.Controls.Add(this.pbItemIcon4);
            this.panel1.Controls.Add(this.pbItemIcon3);
            this.panel1.Controls.Add(this.pbItemIcon2);
            this.panel1.Controls.Add(this.pbItemIcon1);
            this.panel1.Controls.Add(this.pbItemIcon0);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(315, 360);
            this.panel1.TabIndex = 0;
            // 
            // pbPointer
            // 
            this.pbPointer.Image = global::Client.Logic.Properties.Resources.ItemPicker;
            this.pbPointer.Location = new System.Drawing.Point(22, 29);
            this.pbPointer.Name = "pbPointer";
            this.pbPointer.Size = new System.Drawing.Size(12, 12);
            this.pbPointer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPointer.TabIndex = 9;
            this.pbPointer.TabStop = false;
            // 
            // pbArrowRight
            // 
            this.pbArrowRight.Image = global::Client.Logic.Properties.Resources.page_arrow_right;
            this.pbArrowRight.Location = new System.Drawing.Point(168, 7);
            this.pbArrowRight.Name = "pbArrowRight";
            this.pbArrowRight.Size = new System.Drawing.Size(12, 12);
            this.pbArrowRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbArrowRight.TabIndex = 8;
            this.pbArrowRight.TabStop = false;
            // 
            // pbArrowLeft
            // 
            this.pbArrowLeft.Image = global::Client.Logic.Properties.Resources.page_arrow_left;
            this.pbArrowLeft.Location = new System.Drawing.Point(103, 7);
            this.pbArrowLeft.Name = "pbArrowLeft";
            this.pbArrowLeft.Size = new System.Drawing.Size(12, 12);
            this.pbArrowLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbArrowLeft.TabIndex = 8;
            this.pbArrowLeft.TabStop = false;
            // 
            // lblPage
            // 
            this.lblPage.AutoSize = true;
            this.lblPage.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblPage.Location = new System.Drawing.Point(125, 4);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(37, 17);
            this.lblPage.TabIndex = 7;
            this.lblPage.Text = "0 / 0";
            // 
            // lblItemName9
            // 
            this.lblItemName9.AutoSize = true;
            this.lblItemName9.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lblItemName9.Location = new System.Drawing.Point(71, 306);
            this.lblItemName9.Name = "lblItemName9";
            this.lblItemName9.Size = new System.Drawing.Size(0, 24);
            this.lblItemName9.TabIndex = 5;
            // 
            // lblItemName8
            // 
            this.lblItemName8.AutoSize = true;
            this.lblItemName8.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lblItemName8.Location = new System.Drawing.Point(71, 275);
            this.lblItemName8.Name = "lblItemName8";
            this.lblItemName8.Size = new System.Drawing.Size(0, 24);
            this.lblItemName8.TabIndex = 5;
            // 
            // lblItemName7
            // 
            this.lblItemName7.AutoSize = true;
            this.lblItemName7.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lblItemName7.Location = new System.Drawing.Point(71, 244);
            this.lblItemName7.Name = "lblItemName7";
            this.lblItemName7.Size = new System.Drawing.Size(0, 24);
            this.lblItemName7.TabIndex = 5;
            // 
            // lblItemName6
            // 
            this.lblItemName6.AutoSize = true;
            this.lblItemName6.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lblItemName6.Location = new System.Drawing.Point(71, 214);
            this.lblItemName6.Name = "lblItemName6";
            this.lblItemName6.Size = new System.Drawing.Size(0, 24);
            this.lblItemName6.TabIndex = 5;
            // 
            // lblItemName5
            // 
            this.lblItemName5.AutoSize = true;
            this.lblItemName5.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lblItemName5.Location = new System.Drawing.Point(71, 183);
            this.lblItemName5.Name = "lblItemName5";
            this.lblItemName5.Size = new System.Drawing.Size(0, 24);
            this.lblItemName5.TabIndex = 5;
            // 
            // lblItemName4
            // 
            this.lblItemName4.AutoSize = true;
            this.lblItemName4.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lblItemName4.Location = new System.Drawing.Point(71, 152);
            this.lblItemName4.Name = "lblItemName4";
            this.lblItemName4.Size = new System.Drawing.Size(0, 24);
            this.lblItemName4.TabIndex = 5;
            // 
            // lblItemName3
            // 
            this.lblItemName3.AutoSize = true;
            this.lblItemName3.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lblItemName3.Location = new System.Drawing.Point(71, 121);
            this.lblItemName3.Name = "lblItemName3";
            this.lblItemName3.Size = new System.Drawing.Size(0, 24);
            this.lblItemName3.TabIndex = 5;
            // 
            // lblItemName2
            // 
            this.lblItemName2.AutoSize = true;
            this.lblItemName2.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lblItemName2.Location = new System.Drawing.Point(71, 90);
            this.lblItemName2.Name = "lblItemName2";
            this.lblItemName2.Size = new System.Drawing.Size(0, 24);
            this.lblItemName2.TabIndex = 5;
            // 
            // lblItemName1
            // 
            this.lblItemName1.AutoSize = true;
            this.lblItemName1.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lblItemName1.Location = new System.Drawing.Point(71, 59);
            this.lblItemName1.Name = "lblItemName1";
            this.lblItemName1.Size = new System.Drawing.Size(0, 24);
            this.lblItemName1.TabIndex = 5;
            // 
            // lblItemName0
            // 
            this.lblItemName0.AutoSize = true;
            this.lblItemName0.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lblItemName0.Location = new System.Drawing.Point(71, 28);
            this.lblItemName0.Name = "lblItemName0";
            this.lblItemName0.Size = new System.Drawing.Size(0, 24);
            this.lblItemName0.TabIndex = 5;
            // 
            // pbItemIcon9
            // 
            this.pbItemIcon9.Location = new System.Drawing.Point(40, 299);
            this.pbItemIcon9.Name = "pbItemIcon9";
            this.pbItemIcon9.Size = new System.Drawing.Size(32, 32);
            this.pbItemIcon9.TabIndex = 6;
            this.pbItemIcon9.TabStop = false;
            // 
            // pbItemIcon8
            // 
            this.pbItemIcon8.Location = new System.Drawing.Point(40, 268);
            this.pbItemIcon8.Name = "pbItemIcon8";
            this.pbItemIcon8.Size = new System.Drawing.Size(32, 32);
            this.pbItemIcon8.TabIndex = 6;
            this.pbItemIcon8.TabStop = false;
            // 
            // pbItemIcon7
            // 
            this.pbItemIcon7.Location = new System.Drawing.Point(40, 237);
            this.pbItemIcon7.Name = "pbItemIcon7";
            this.pbItemIcon7.Size = new System.Drawing.Size(32, 32);
            this.pbItemIcon7.TabIndex = 6;
            this.pbItemIcon7.TabStop = false;
            // 
            // pbItemIcon6
            // 
            this.pbItemIcon6.Location = new System.Drawing.Point(40, 206);
            this.pbItemIcon6.Name = "pbItemIcon6";
            this.pbItemIcon6.Size = new System.Drawing.Size(32, 32);
            this.pbItemIcon6.TabIndex = 6;
            this.pbItemIcon6.TabStop = false;
            // 
            // pbItemIcon5
            // 
            this.pbItemIcon5.Location = new System.Drawing.Point(40, 175);
            this.pbItemIcon5.Name = "pbItemIcon5";
            this.pbItemIcon5.Size = new System.Drawing.Size(32, 32);
            this.pbItemIcon5.TabIndex = 6;
            this.pbItemIcon5.TabStop = false;
            // 
            // pbItemIcon4
            // 
            this.pbItemIcon4.Location = new System.Drawing.Point(40, 144);
            this.pbItemIcon4.Name = "pbItemIcon4";
            this.pbItemIcon4.Size = new System.Drawing.Size(32, 32);
            this.pbItemIcon4.TabIndex = 6;
            this.pbItemIcon4.TabStop = false;
            // 
            // pbItemIcon3
            // 
            this.pbItemIcon3.Location = new System.Drawing.Point(40, 113);
            this.pbItemIcon3.Name = "pbItemIcon3";
            this.pbItemIcon3.Size = new System.Drawing.Size(32, 32);
            this.pbItemIcon3.TabIndex = 6;
            this.pbItemIcon3.TabStop = false;
            // 
            // pbItemIcon2
            // 
            this.pbItemIcon2.Location = new System.Drawing.Point(40, 82);
            this.pbItemIcon2.Name = "pbItemIcon2";
            this.pbItemIcon2.Size = new System.Drawing.Size(32, 32);
            this.pbItemIcon2.TabIndex = 6;
            this.pbItemIcon2.TabStop = false;
            // 
            // pbItemIcon1
            // 
            this.pbItemIcon1.Location = new System.Drawing.Point(40, 51);
            this.pbItemIcon1.Name = "pbItemIcon1";
            this.pbItemIcon1.Size = new System.Drawing.Size(32, 32);
            this.pbItemIcon1.TabIndex = 6;
            this.pbItemIcon1.TabStop = false;
            // 
            // pbItemIcon0
            // 
            this.pbItemIcon0.Location = new System.Drawing.Point(40, 20);
            this.pbItemIcon0.Name = "pbItemIcon0";
            this.pbItemIcon0.Size = new System.Drawing.Size(32, 32);
            this.pbItemIcon0.TabIndex = 6;
            this.pbItemIcon0.TabStop = false;
            // 
            // MnuInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.panel1);
            this.Name = "MnuInventory";
            this.Size = new System.Drawing.Size(315, 360);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPointer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbArrowRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbArrowLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemIcon0)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pbItemIcon9;
        private System.Windows.Forms.PictureBox pbItemIcon8;
        private System.Windows.Forms.PictureBox pbItemIcon7;
        private System.Windows.Forms.PictureBox pbItemIcon6;
        private System.Windows.Forms.PictureBox pbItemIcon5;
        private System.Windows.Forms.PictureBox pbItemIcon4;
        private System.Windows.Forms.PictureBox pbItemIcon3;
        private System.Windows.Forms.PictureBox pbItemIcon2;
        private System.Windows.Forms.PictureBox pbItemIcon1;
        private System.Windows.Forms.Label lblItemName8;
        private System.Windows.Forms.Label lblItemName7;
        private System.Windows.Forms.Label lblItemName6;
        private System.Windows.Forms.Label lblItemName5;
        private System.Windows.Forms.Label lblItemName4;
        private System.Windows.Forms.Label lblItemName3;
        private System.Windows.Forms.Label lblItemName2;
        private System.Windows.Forms.Label lblItemName1;
        private System.Windows.Forms.Label lblItemName0;
        private System.Windows.Forms.Label lblItemName9;
        private System.Windows.Forms.PictureBox pbItemIcon0;
        private System.Windows.Forms.PictureBox pbArrowLeft;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.PictureBox pbArrowRight;
        private System.Windows.Forms.PictureBox pbPointer;
    }
}
