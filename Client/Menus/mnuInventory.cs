﻿// <copyright file="MnuInventory.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

using Client.Logic.Menus.InventoryChildren;

namespace Client.Logic.Menus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Extensions;
    using Graphic;

    public partial class MnuInventory : UserControl
    {
        private const int PointerStartY = 29;
        private const int PointerMaxY = 308;

        private List<Label> itemLabels = new List<Label>();
        private List<PictureBox> itemIcons = new List<PictureBox>();
        private Enums.InvMenuType mode;
        private int currentTen;
        private int selectedItemNum = 0;
        private InventoryUseItem useItem;

        public int CurrentTen => this.currentTen;

        public MnuInventory(Enums.InvMenuType menuType, int itemSelected)
        {
            this.InitializeComponent();

            int requiredIndex = 0;
            for (int i = 0; i < this.panel1.Controls.Count; i++)
            {
                if (this.Controls[i].Name.Contains($"lblItemName{requiredIndex}"))
                {
                    this.itemLabels.Add(this.Controls[i] as Label);
                }

                if (requiredIndex != 9)
                {
                    i = 0;
                    requiredIndex++;
                    continue;
                }

                break;
            }

            requiredIndex = 0;
            for (int i = 0; i < this.panel1.Controls.Count; i++)
            {
                if (this.Controls[i].Name.Contains($"pbItemIcon{requiredIndex}"))
                {
                    this.itemIcons.Add(this.Controls[i] as PictureBox);
                }

                if (requiredIndex != 9)
                {
                    i = 0;
                    requiredIndex++;
                    continue;
                }

                break;
            }

            this.mode = menuType;

            foreach (PictureBox pb in this.itemIcons)
            {
                pb.MouseDown += this.Item_MouseDown;
                pb.Click += this.Item_Click;
            }

            foreach (Label lbl in this.itemLabels)
            {
                lbl.MouseDown += this.Item_MouseDown;
                lbl.Click += this.Item_Click;
            }

            this.currentTen = (itemSelected - 1) / 10;
        }

        private void Item_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.mode == Enums.InvMenuType.Use)
            {
                // TODO: Click item to display "Use" Menu
            }
        }

        private void Item_Click(object sender, EventArgs e)
        {
            Label item = sender as Label;
            if (item != null)
            {
                if (
                    Players.PlayerManager.MyPlayer.GetInvItemNum((this.CurrentTen * 10) + 1 +
                                                                 this.itemLabels.IndexOf(item)) > 0)
                {
                    this.ChangeSelected(this.itemLabels.IndexOf(item));

                    if (this.mode == Enums.InvMenuType.Store)
                    {
                        MnuBankItemSelected selectedMenu = (MnuBankItemSelected)Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuBankItemSelected");
                        if (selectedMenu != null)
                        {
                            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);

                            // selectedMenu.ItemSlot = GetSelectedItemSlot();
                            // selectedMenu.ItemNum = Players.PlayerManager.MyPlayer.GetInvItemNum(GetSelectedItemSlot());
                        }

                        Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(
                            new MnuBankItemSelected(
                                "mnuBankItemSelected",
                                Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()),
                                Players.PlayerManager.MyPlayer.Inventory[this.GetSelectedItemSlot()].Value,
                                this.GetSelectedItemSlot(),
                                Enums.InvMenuType.Store));
                        Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuBankItemSelected");
                    }
                    else if (this.mode == Enums.InvMenuType.Use)
                    {
                        // Handled in Mouse_Down
                    }
                    else if (this.mode == Enums.InvMenuType.Sell)
                    {
                        MnuShopItemSelected selectedMenu =
                            (MnuShopItemSelected)
                            Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuShopItemSelected");
                        if (selectedMenu != null)
                        {
                            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);

                            // selectedMenu.ItemSlot = GetSelectedItemSlot();
                            // ChangeSelected(Array.IndexOf(lblVisibleItems, sender));
                        }

                        Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(
                            new MnuShopItemSelected("mnuShopItemSelected",
                                Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()),
                                this.GetSelectedItemSlot(), Enums.InvMenuType.Sell));
                        Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuShopItemSelected");

                        // ChangeSelected(Array.IndexOf(lblVisibleItems, sender));
                    }

                    Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                }
            }
            else
            {
                PictureBox pbItem = sender as PictureBox;
                if (
                    Players.PlayerManager.MyPlayer.GetInvItemNum((this.CurrentTen * 10) + 1 +
                                                                 this.itemIcons.IndexOf(pbItem)) > 0)
                {
                    this.ChangeSelected(this.itemIcons.IndexOf(pbItem));

                    if (this.mode == Enums.InvMenuType.Store)
                    {
                        MnuBankItemSelected selectedMenu =
                            (MnuBankItemSelected)
                            Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuBankItemSelected");
                        if (selectedMenu != null)
                        {
                            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);

                            // selectedMenu.ItemSlot = GetSelectedItemSlot();
                            // selectedMenu.ItemNum = Players.PlayerManager.MyPlayer.GetInvItemNum(GetSelectedItemSlot());
                        }

                        Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(
                            new MnuBankItemSelected("mnuBankItemSelected",
                                Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()),
                                Players.PlayerManager.MyPlayer.Inventory[this.GetSelectedItemSlot()].Value,
                                this.GetSelectedItemSlot(), Enums.InvMenuType.Store));
                        Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuBankItemSelected");
                    }
                    else if (this.mode == Enums.InvMenuType.Use)
                    {
                        // Don't select the item, interferes with drag & drop
                        // mnuItemSelected selectedMenu = (mnuItemSelected)Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuItemSelected");
                        // if (selectedMenu != null) {
                        //    Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);
                        //    //selectedMenu.ItemSlot = GetSelectedItemSlot();
                        //    //ChangeSelected(Array.IndexOf(lblVisibleItems, sender));
                        // }
                        // Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new Menus.mnuItemSelected("mnuItemSelected", currentTen * 10 + 1 + Array.IndexOf(lblVisibleItems, sender)));
                        // Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuItemSelected");
                        ////ChangeSelected(Array.IndexOf(lblVisibleItems, sender));
                    }
                    else if (this.mode == Enums.InvMenuType.Sell)
                    {
                        MnuShopItemSelected selectedMenu =
                            (MnuShopItemSelected)
                            Windows.WindowSwitcher.GameWindow.MenuManager.FindMenu("mnuShopItemSelected");
                        if (selectedMenu != null)
                        {
                            Windows.WindowSwitcher.GameWindow.MenuManager.RemoveMenu(selectedMenu);

                            // selectedMenu.ItemSlot = GetSelectedItemSlot();
                            // ChangeSelected(Array.IndexOf(lblVisibleItems, sender));
                        }

                        Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(
                            new MnuShopItemSelected("mnuShopItemSelected",
                                Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()),
                                this.GetSelectedItemSlot(), Enums.InvMenuType.Sell));
                        Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuShopItemSelected");

                        // ChangeSelected(Array.IndexOf(lblVisibleItems, sender));
                    }

                    Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                }
            }
        }

        public void DisplayItems(int startNum)
        {
            for (int i = 0; i < this.itemLabels.Count; i++)
            {
                if (Players.PlayerManager.MyPlayer.GetInvItemNum(startNum + i) > 0)
                {
                    // Check if the item is equiped
                    if (Players.PlayerManager.MyPlayer.IsEquiped(startNum + i))
                    {
                        // If it is equiped, set the labels forecolor to yellow, if it isn't yellow already
                        if (this.itemLabels[i].ForeColor != Color.Yellow)
                        {
                            this.itemLabels[i].ForeColor = Color.Yellow;
                        }
                    }
                    else
                    {
                        // If it isn't equiped, set the labels' forecolor to black, if it isn't black already
                        if (this.itemLabels[i].ForeColor != Color.Black)
                        {
                            this.itemLabels[i].ForeColor = Color.Black;
                        }
                    }

                    string itemName =
                        Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(startNum + i)].Name;
                    if (!string.IsNullOrEmpty(itemName))
                    {
                        int itemAmount = Players.PlayerManager.MyPlayer.GetInvItemAmount(startNum + i);
                        if (itemAmount > 0)
                        {
                            itemName += " (" + itemAmount + ")";
                        }

                        if (Players.PlayerManager.MyPlayer.GetInvItemSticky(startNum + i))
                        {
                            itemName = "x" + itemName;
                        }

                        this.itemLabels[i].Text = itemName;
                    }
                    else
                    {
                        this.itemLabels[i].Text = string.Empty;
                    }
                }
                else
                {
                    if (this.itemLabels[i].ForeColor != Color.Black)
                    {
                        this.itemLabels[i].ForeColor = Color.Black;
                    }

                    this.itemLabels[i].Text = string.Empty;
                }

                if (this.itemLabels[i].Text != string.Empty)
                {
                    this.itemIcons[i].Image = Tools.CropImage(
                        GraphicsManager.Items.ToBitmap(true),
                        new Rectangle((Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(startNum + i)].Pic - ((Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(startNum + i)].Pic / 6) * 6)) * Constants.TILEWIDTH, (Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(startNum + i)].Pic / 6) * Constants.TILEHEIGHT, Constants.TILEWIDTH, Constants.TILEHEIGHT));
                }
                else
                {
                    this.itemIcons[i].Image = new Bitmap(this.itemIcons[i].Width, this.itemIcons[i].Height);
                }
            }
        }

        public void UpdateVisibleItem(int itemNum)
        {// appears to be unused
            if (itemNum / 10 != this.CurrentTen)
            {
                return;
            }

            int itemIndex = itemNum - (this.CurrentTen * 10) - 1;
            if (Players.PlayerManager.MyPlayer.GetInvItemNum(itemNum) > 0)
            {
                if (Players.PlayerManager.MyPlayer.IsEquiped(itemNum))
                { // Check if the item is equiped
                  // If it is equiped, set the labels forecolor to yellow, if it isn't yellow already
                    if (this.itemLabels[itemIndex].ForeColor != Color.Yellow)
                    {
                        this.itemLabels[itemIndex].ForeColor = Color.Yellow;
                    }
                }
                else
                {
                    // If it isn't equiped, set the labels' forecolor to black, if it isn't black already
                    if (this.itemLabels[itemIndex].ForeColor != Color.WhiteSmoke)
                    {
                        this.itemLabels[itemIndex].ForeColor = Color.WhiteSmoke;
                    }
                }

                string itemName = Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(itemNum)].Name;
                if (!string.IsNullOrEmpty(itemName))
                {
                    int itemAmount = Players.PlayerManager.MyPlayer.GetInvItemAmount(itemNum);
                    if (itemAmount > 0)
                    {
                        itemName += " (" + itemAmount + ")";
                    }

                    if (Players.PlayerManager.MyPlayer.GetInvItemSticky(itemNum))
                    {
                        itemName = "x" + itemName;
                    }

                    this.itemLabels[itemIndex].Text = itemName;
                }
                else
                {
                    this.itemLabels[itemIndex].Text = string.Empty;
                }
            }
            else
            {
                if (this.itemLabels[itemIndex].ForeColor != Color.WhiteSmoke)
                {
                    this.itemLabels[itemIndex].ForeColor = Color.WhiteSmoke;
                }

                this.itemLabels[itemIndex].Text = string.Empty;
            }

            if (this.itemLabels[itemIndex].Text != string.Empty)
            {
                this.itemIcons[itemIndex].Image = Tools.CropImage(
                    GraphicsManager.Items.ToBitmap(true),
                    new Rectangle((Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(itemIndex)].Pic - ((Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(itemIndex)].Pic / 6) * 6)) * Constants.TILEWIDTH, (Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(itemIndex)].Pic / 6) * Constants.TILEHEIGHT, Constants.TILEWIDTH, Constants.TILEHEIGHT));
            }
        }

        private void ChangeSelected(int itemNum)
        {
            this.pbPointer.Location = new Point(22, PointerStartY + (31 * itemNum));
            this.selectedItemNum = itemNum;
        }

        private int GetSelectedItemSlot()
        {
            return this.selectedItemNum + (this.CurrentTen * 10) + 1;
        }

        /// <inheritdoc/>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            switch (e.KeyData)
            {
                case Keys.Down:
                    {
                        if (this.selectedItemNum >= 9)
                        {
                            this.ChangeSelected(0);

                            // DisplayItems(1);
                        }
                        else
                        {
                            this.ChangeSelected(this.selectedItemNum + 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case Keys.Up:
                    {
                        if (this.selectedItemNum <= 0)
                        {
                            this.ChangeSelected(9);
                        }
                        else
                        {
                            this.ChangeSelected(this.selectedItemNum - 1);
                        }

                        Music.Music.AudioPlayer.PlaySoundEffect("beep1.wav");
                    }

                    break;
                case Keys.Left:
                    {
                        // int itemSlot = (currentTen + 1) - 10;//System.Math.Max(1, GetSelectedItemSlot() - (11 - itemPicker.SelectedItem));
                        if (this.currentTen <= 0)
                        {
                            this.currentTen = (MaxInfo.MaxInv - 1) / 10;
                        }
                        else
                        {
                            this.currentTen--;
                        }

                        this.DisplayItems((this.currentTen * 10) + 1);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                    }

                    break;
                case Keys.Right:
                    {
                        // int itemSlot = currentTen + 1 + 10;
                        if (this.currentTen >= ((MaxInfo.MaxInv - 1) / 10))
                        {
                            this.currentTen = 0;
                        }
                        else
                        {
                            this.currentTen++;
                        }

                        this.DisplayItems((this.currentTen * 10) + 1);
                        Music.Music.AudioPlayer.PlaySoundEffect("beep4.wav");
                    }

                    break;
                case Keys.Return:
                    {
                        if (Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()) > 0)
                        {
                            if (this.mode == Enums.InvMenuType.Store)
                            {
                                Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuBankItemSelected("mnuBankItemSelected", Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()), Players.PlayerManager.MyPlayer.Inventory[this.GetSelectedItemSlot()].Value, this.GetSelectedItemSlot(), Enums.InvMenuType.Store));
                                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuBankItemSelected");
                            }
                            else if (this.mode == Enums.InvMenuType.Use)
                            {
                                if (!this.pnlMenu.Visible)
                                {
                                    
                                }
                            }
                            else if (this.mode == Enums.InvMenuType.Sell)
                            {
                                Windows.WindowSwitcher.GameWindow.MenuManager.AddMenu(new MnuShopItemSelected("mnuShopItemSelected", Players.PlayerManager.MyPlayer.GetInvItemNum(this.GetSelectedItemSlot()), this.GetSelectedItemSlot(), Enums.InvMenuType.Sell));
                                Windows.WindowSwitcher.GameWindow.MenuManager.SetActiveMenu("mnuShopItemSelected");
                            }

                            Music.Music.AudioPlayer.PlaySoundEffect("beep2.wav");
                        }
                    }

                    break;
                case Keys.Back:
                    {
                        if (this.mode == Enums.InvMenuType.Store)
                        {
                            MenuSwitcher.OpenBankOptions();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                        }
                        else if (this.mode == Enums.InvMenuType.Use)
                        {
                            MenuSwitcher.ShowMainMenu();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                        }
                        else if (this.mode == Enums.InvMenuType.Sell)
                        {
                            MenuSwitcher.OpenShopOptions();
                            Music.Music.AudioPlayer.PlaySoundEffect("beep3.wav");
                        }
                    }

                    break;
            }
        }
    }
}