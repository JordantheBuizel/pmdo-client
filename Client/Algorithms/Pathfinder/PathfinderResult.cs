﻿// <copyright file="PathfinderResult.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Algorithms.Pathfinder
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class PathfinderResult
    {
        private readonly List<Enums.Direction> path;
        private readonly int tileCount;
        private readonly bool isPath;

        private int currentItem = 0;

        internal PathfinderResult(List<Enums.Direction> path, bool isPath)
        {
            this.path = path;
            this.tileCount = path.Count;
            this.isPath = isPath;
        }

        public List<Enums.Direction> Path
        {
            get { return this.path; }
        }

        public int TileCount
        {
            get { return this.tileCount; }
        }

        public bool IsPath
        {
            get { return this.isPath; }
        }

        public Enums.Direction GetNextItem()
        {
            this.currentItem++;
            return this.path[this.currentItem - 1];
        }
    }
}
