﻿// <copyright file="PathfinderSquare.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Algorithms.Pathfinder
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class PathfinderSquare
    {
        public int DistanceSteps { get; set; }

        public bool IsPath { get; set; }

        public Enums.TileType TileType { get; set; }

        public string SessionID { get; set; }
    }
}
