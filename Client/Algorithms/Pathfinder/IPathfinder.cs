﻿// <copyright file="IPathfinder.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Algorithms.Pathfinder
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal interface IPathfinder
    {
        PathfinderResult FindPath(int startX, int startY, int endX, int endY);
    }
}
