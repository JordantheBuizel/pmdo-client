﻿// <copyright file="AStarPathfinder.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Algorithms.Pathfinder
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    internal class AStarPathfinder : IPathfinder
    {
        private Point[] mMovements;
        private PathfinderSquare[,] mSquares;

        private readonly int maxX;
        private readonly int maxY;

        private readonly Maps.Map map;

        public AStarPathfinder(Maps.Map map)
        {
            this.map = map;

            this.maxX = map.MaxX;
            this.maxY = map.MaxY;

            this.InitMovements();
            this.InitSquares();
        }

        /// <inheritdoc/>
        public PathfinderResult FindPath(int startX, int startY, int endX, int endY)
        {
            if (this.ValidCoordinates(startX, startY) == false || this.ValidCoordinates(endX, endY) == false)
            {
                return new PathfinderResult(new List<Enums.Direction>(), false);
            }

            int tempStartX = startX;
            int tempStartY = startY;

            startX = endX;
            startY = endY;
            endX = tempStartX;
            endY = tempStartY;

            string findPathID = Globals.Tick.ToString();

            this.mSquares[startX, startY].DistanceSteps = 0;
            this.mSquares[startX, startY].SessionID = findPathID;

            while (true)
            {
                bool madeProgress = false;

                foreach (Point mainPoint in this.AllSquares())
                {
                    int x = mainPoint.X;
                    int y = mainPoint.Y;
                    if (this.mSquares[x, y].SessionID != findPathID)
                    {
                        this.mSquares[x, y].DistanceSteps = 10000;
                        this.mSquares[x, y].IsPath = false;
                        this.mSquares[x, y].SessionID = findPathID;
                    }

                    if (this.IsSquareOpen(x, y))
                    {
                        int passHere = this.mSquares[x, y].DistanceSteps;

                        foreach (Point movePoint in this.ValidMoves(x, y))
                        {
                            int newX = movePoint.X;
                            int newY = movePoint.Y;
                            int newPass = passHere + 1;

                            if (this.mSquares[newX, newY].DistanceSteps > newPass)
                            {
                                this.mSquares[newX, newY].DistanceSteps = newPass;
                                madeProgress = true;
                            }
                        }
                    }
                }

                if (!madeProgress)
                {
                    break;
                }
            }

            // Create the actual path
            int pointX = endX;
            int pointY = endY;

            int lastPointX = endX;
            int lastPointY = endY;

            List<Enums.Direction> pathList = new List<Enums.Direction>();
            while (true)
            {
                Point lowestPoint = Point.Empty;
                int lowest = 10000;

                foreach (Point movePoint in this.ValidMoves(pointX, pointY))
                {
                    int count = this.mSquares[movePoint.X, movePoint.Y].DistanceSteps;
                    if (count < lowest)
                    {
                        lowest = count;
                        lowestPoint.X = movePoint.X;
                        lowestPoint.Y = movePoint.Y;
                    }
                }

                if (lowest != 10000)
                {
                    // Mark the square as part of the path if it is the lowest
                    // number. Set the current position as the square with
                    // that number of steps.
                    this.mSquares[lowestPoint.X, lowestPoint.Y].IsPath = true;
                    pointX = lowestPoint.X;
                    pointY = lowestPoint.Y;
                    if (lastPointX > pointX)
                    {
                        pathList.Add(Enums.Direction.Left);
                    }
                    else if (lastPointX < pointX)
                    {
                        pathList.Add(Enums.Direction.Right);
                    }
                    else if (lastPointY > pointY)
                    {
                        pathList.Add(Enums.Direction.Up);
                    }
                    else if (lastPointY < pointY)
                    {
                        pathList.Add(Enums.Direction.Down);
                    }

                    lastPointX = pointX;
                    lastPointY = pointY;
                }
                else
                {
                    break;
                }

                if (pointX == startX && pointY == startY)
                {
                    // We went from monster to hero, so we're finished.
                    break;
                }
            }

            if (pathList.Count > 0)
            {
                // pathList.Reverse();
                return new PathfinderResult(pathList, true);
            }
            else
            {
                return new PathfinderResult(pathList, false);
            }
        }

        private void InitMovements()
        {
            this.mMovements = new Point[]
            {
                new Point(0, -1),
                new Point(1, 0),
                new Point(0, 1),
                new Point(-1, 0)
            };
        }

        private void InitSquares()
        {
            this.mSquares = new PathfinderSquare[this.maxX + 1, this.maxY + 1];

            for (int x = 0; x <= this.maxX; x++)
            {
                for (int y = 0; y <= this.maxY; y++)
                {
                    this.mSquares[x, y] = new PathfinderSquare();
                    this.mSquares[x, y].TileType = this.map.Tile[x, y].Type;
                    this.mSquares[x, y].DistanceSteps = 10000;
                    this.mSquares[x, y].IsPath = false;
                }
            }
        }

        private bool IsSquareOpen(int x, int y)
        {
            if (GameProcessor.IsBlocked(this.map, x, y))
            {
                return false;
            }
            else
            {
                return true;
            }

            // First check the tile types
            /*switch (this.mSquares[x, y].TileType)
            {
                case Enums.TileType.Walkable:
                case Enums.TileType.Item:
                    // Do nothing since it's walkable
                    break;
                default:
                    return false;
            }

            /// Check other npcs on the map
            // for (int i = 0; i < Globals.MAX_MAP_NPCS; i++) {
            //    if (mMap.ActiveNpc[i].X == x && mMap.ActiveNpc[i].Y == y) {
            //        return false;
            //    }
            // }
            ///// Check for players
            // for (int i = 0; i < Program.ClassMan.mPlayers.mMaxPlayers; i++) {
            //    if (Program.ClassMan.mTcp.IsPlaying(i) && Program.ClassMan.mPlayers[i].mMap == mMapNum
            //        && Program.ClassMan.mPlayers[i].mX == x && Program.ClassMan.mPlayers[i].mY == y) {
            //        return false;
            //    }
            // }
            return true;*/
        }

        private IEnumerable<Point> ValidMoves(int x, int y)
        {
            // Return each valid square we can move to.
            foreach (Point movePoint in this.mMovements)
            {
                int newX = x + movePoint.X;
                int newY = y + movePoint.Y;

                if (this.ValidCoordinates(newX, newY) &&
                    this.IsSquareOpen(newX, newY))
                    {
                    yield return new Point(newX, newY);
                }
            }
        }

        private bool ValidCoordinates(int x, int y)
        {
            // Our coordinates are constrained between 0 and 14.
            if (x < 0)
            {
                return false;
            }

            if (y < 0)
            {
                return false;
            }

            if (x > this.maxX)
            {
                return false;
            }

            if (y > this.maxY)
            {
                return false;
            }

            return true;
        }

        private IEnumerable<Point> AllSquares()
        {
            // Return every point on the board in order.
            for (int x = 0; x <= this.maxX; x++)
            {
                for (int y = 0; y <= this.maxY; y++)
                {
                    yield return new Point(x, y);
                }
            }
        }
    }
}
