﻿// <copyright file="RDungeon.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.RDungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Description of RDungeon.
    /// </summary>
    internal class RDungeon
    {
        public string Name
        {
            get;
            set;
        }
    }
}
