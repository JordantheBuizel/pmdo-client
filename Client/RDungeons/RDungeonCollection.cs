﻿// <copyright file="RDungeonCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.RDungeons
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Description of RDungeonCollection.
    /// </summary>
    internal class RDungeonCollection
    {
        private readonly PMU.Core.ListPair<int, RDungeon> mRDungeons;

        internal RDungeonCollection()
        {
            this.mRDungeons = new PMU.Core.ListPair<int, RDungeon>();
        }

        public RDungeon this[int index]
        {
            get { return this.mRDungeons[index]; }
            set { this.mRDungeons[index] = value; }
        }

        public void AddRDungeon(int index, RDungeon RDungeonToAdd)
        {
            this.mRDungeons.Add(index, RDungeonToAdd);
        }

        public void ClearRDungeons()
        {
            this.mRDungeons.Clear();
        }
    }
}
