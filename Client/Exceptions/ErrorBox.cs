﻿// <copyright file="ErrorBox.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Text;
    using System.Windows.Forms;

    /// <summary>
    /// </summary>
    public partial class ErrorBox : Form
    {
        private bool errorShown;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorBox"/> class.
        /// </summary>
        public ErrorBox()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Shows the dialog box
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="message">The message.</param>
        /// <param name="details">The details.</param>
        /// <returns>Shows bork box</returns>
        public static DialogResult ShowDialog(string caption, string message, string details)
        {
            ErrorBox error = new ErrorBox();
            error.lblError.Text = message;
            error.Text = caption;
            error.txtDetails.Text = details;

            try
            {
                Network.Messenger.SendPacket(PMU.Sockets.TcpPacket.CreatePacket("clienterror", details));
            }
            catch { }

            return error.ShowDialog();
        }

        private void BtnDetails_Click(object sender, EventArgs e)
        {
            if (this.errorShown == false)
            {
                this.Size = new Size(499, 292);
                this.btnDetails.Text = "Details ^";
                this.errorShown = true;
            }
else
            {
                this.Size = new Size(499, 193);
                this.btnDetails.Text = "Details v";
                this.errorShown = false;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}