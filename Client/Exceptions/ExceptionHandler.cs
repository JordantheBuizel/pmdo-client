﻿// <copyright file="ExceptionHandler.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Class that handles any exceptions that occur in the program.
    /// </summary>
    public class ExceptionHandler
    {
        /// <summary>
        /// Called when an exception occurs.
        /// </summary>
        /// <param name="ex">The exception.</param>
        public static void OnException(Exception ex)
        {
            ErrorBox.ShowDialog("Unhandled Exception", ex.Message, ex.ToString());
        }
    }
}