﻿// <copyright file="DataManager.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class DataManager
    {
        public static int AverageLoadPercent()
        {
            return 100;

            // return (Items.ItemHelper.DataLoadPercent + Emotions.EmotionHelper.DataLoadPercent +
            //    + Arrows.ArrowHelper.DataLoadPercent + Npc.NpcHelper.DataLoadPercent + Shops.ShopHelper.DataLoadPercent
            //    + Moves.MoveHelper.DataLoadPercent + Evolutions.EvolutionHelper.DataLoadPercent + Stories.StoryHelper.DataLoadPercent + Missions.MissionHelper.DataLoadPercent +
            //    RDungeons.RDungeonHelper.DataLoadPercent + Dungeons.DungeonHelper.DataLoadPercent) / 10;
        }
    }
}