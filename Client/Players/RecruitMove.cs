﻿// <copyright file="RecruitMove.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class RecruitMove
    {
        public RecruitMove()
        {
            this.MoveNum = -1;
            this.CurrentPP = -1;
            this.MaxPP = -1;
        }

        public int CurrentPP
        {
            get;
            set;
        }

        public int MaxPP
        {
            get;
            set;
        }

        public int MoveNum
        {
            get;
            set;
        }

        public bool Sealed { get; set; }
    }
}
