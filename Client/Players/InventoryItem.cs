﻿// <copyright file="InventoryItem.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class InventoryItem
    {
        public int Num
        {
            get;
            set;
        }

        public int Value
        {
            get;
            set;
        }

        public bool Sticky
        {
            get;
            set;
        }
    }
}