﻿// <copyright file="Recruit.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Recruit
    {
        public Recruit()
        {
            this.Loaded = false;
        }

        public int Num
        {
            get;
            set;
        }

        public int Form
        {
            get;
            set;
        }

        public Enums.Coloration Shiny
        {
            get;
            set;
        }

        public int IQ
        {
            get;
            set;
        }

        public bool Loaded
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public int HP
        {
            get;
            set;
        }

        public int MaxHP
        {
            get;
            set;
        }

        public Enums.Sex Sex
        {
            get;
            set;
        }

        public Enums.StatusAilment StatusAilment
        {
            get;
            set;
        }

        public int HeldItemSlot { get; set; }

        public int ExpPercent { get; set; }

        public int Level { get; set; }
    }
}