﻿// <copyright file="PlayerPet.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Graphic.Renderers.Sprites;

    internal class PlayerPet : ISprite
    {
        /// <inheritdoc/>
        public Graphic.SpriteSheet SpriteSheet
        {
            get;
            set;
        }

        public int Slot { get; set; }

        /// <inheritdoc/>
        public int IdleTimer { get; set; }
        /// <inheritdoc/>
        public int IdleFrame { get; set; }
        /// <inheritdoc/>
        public int LastWalkTime { get; set; }
        /// <inheritdoc/>
        public int WalkingFrame { get; set; }

        /// <inheritdoc/>
        public int Sprite
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Form { get; set; }
        /// <inheritdoc/>
        public Enums.Coloration Shiny { get; set; }
        /// <inheritdoc/>
        public Enums.Sex Sex { get; set; }

        /// <inheritdoc/>
        public Enums.Direction Direction
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public bool Attacking
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Point Offset
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Point Location
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int AttackTimer
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int TotalAttackTime { get; set; }

        /// <inheritdoc/>
        public int X
        {
            get { return this.Location.X; }
            set { this.Location = new Point(value, this.Location.Y); }
        }

        /// <inheritdoc/>
        public int Y
        {
            get { return this.Location.Y; }
            set { this.Location = new Point(this.Location.X, value); }
        }

        /// <inheritdoc/>
        public Enums.MovementSpeed MovementSpeed
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.StatusAilment StatusAilment
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public List<int> VolatileStatus
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public bool Leaving { get; set; }

        /// <inheritdoc/>
        public bool ScreenActive { get; set; }

        /// <inheritdoc/>
        public int SleepTimer
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int SleepFrame
        {
            get;
            set;
        }

        public IPlayer Player
        {
            get;
            set;
        }

        public int LastPlayerX { get; set; }

        public int LastPlayerY { get; set; }

        /// <inheritdoc/>
        public SpeechBubble CurrentSpeech { get; set; }

        /// <inheritdoc/>
        public Emoticon CurrentEmote { get; set; }

        public PlayerPet(int slot, IPlayer player)
        {
            this.Slot = slot;
            this.Player = player;

            this.LastPlayerX = player.X;
            this.LastPlayerY = player.Y;

            this.VolatileStatus = new List<int>();
        }

        public void Update()
        {
            this.X = this.GetXBehindPlayer(this.Slot);
            this.Y = this.GetYBehindPlayer(this.Slot);

            if (this.Direction != this.Player.Direction)
            {
                this.Offset = new Point(0, 0);
                this.Direction = this.Player.Direction;
            }
        }

        public int GetHorizDistanceFromPlayer()
        {
            return this.Player.X - this.X;
        }

        public int GetXBehindPlayer(int distance)
        {
            switch (this.Player.Direction)
            {
                case Enums.Direction.Left:
                    this.Offset = new Point(Constants.TILEWIDTH, this.Offset.Y);
                    return this.Player.X + distance;
                case Enums.Direction.Right:
                    this.Offset = new Point(Constants.TILEWIDTH * -1, this.Offset.Y);
                    return this.Player.X - distance;
                case Enums.Direction.Up:
                case Enums.Direction.Down:
                    return this.Player.X;
                default:
                    return this.Player.X;
            }
        }

        public int GetVertDistanceFromPlayer()
        {
            return Math.Abs(this.Player.Y - this.Y);
        }

        public int GetYBehindPlayer(int distance)
        {
            switch (this.Player.Direction)
            {
                case Enums.Direction.Down:
                    this.Offset = new Point(this.Offset.X, Constants.TILEHEIGHT);
                    return this.Player.Y - distance;
                case Enums.Direction.Up:
                    this.Offset = new Point(this.Offset.X, Constants.TILEHEIGHT * -1);
                    return this.Player.Y + distance;
                case Enums.Direction.Right:
                case Enums.Direction.Left:
                    return this.Player.Y;
                default:
                    return this.Player.Y;
            }
        }
    }
}
