﻿// <copyright file="MailCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players.Mail
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class MailCollection
    {
        private readonly List<IMail> mail;

        public MailCollection()
        {
            this.mail = new List<IMail>();
        }

        public void Add(IMail mail)
        {
            this.mail.Add(mail);
        }

        public IMail this[int index]
        {
            get { return this.mail[index]; }
        }

        public int Count
        {
            get { return this.mail.Count; }
        }
    }
}
