﻿// <copyright file="MessageMail.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players.Mail
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class MessageMail : IMail
    {
        private InventoryItem attachedItem;
        private string recieverID;
        private string senderID;
        private string text;
        private string title;

        private Panel pnlMailInterface;

        public InventoryItem AttachedItem
        {
            get { return this.attachedItem; }
            set { this.attachedItem = value; }
        }

        /// <inheritdoc/>
        public string RecieverID
        {
            get { return this.recieverID; }
            set { this.recieverID = value; }
        }

        /// <inheritdoc/>
        public string SenderID
        {
            get { return this.senderID; }
            set { this.senderID = value; }
        }

        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }

        public string Title
        {
            get { return this.title; }
            set { this.title = value; }
        }

        /// <inheritdoc/>
        public MailType Type
        {
            get { return MailType.Message; }
        }

        /// <inheritdoc/>
        public bool Unread
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Panel MailInterfacePanel
        {
            get
            {
                this.CheckMailInterfacePanel();
                return this.pnlMailInterface;
            }
        }

        private void CheckMailInterfacePanel()
        {
            if (this.pnlMailInterface == null)
            {
            }
        }

        private void CreateMailInterfacePanel()
        {
            this.pnlMailInterface = new Panel("pnlMailInterface");
            this.pnlMailInterface.Size = new System.Drawing.Size(300, 400);
        }
    }
}
