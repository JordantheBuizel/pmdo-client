﻿// <copyright file="IMail.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players.Mail
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal interface IMail
    {
        string SenderID { get; set; }

        string RecieverID { get; set; }

        MailType Type { get; }

        bool Unread { get; set; }

        Panel MailInterfacePanel { get; }
    }
}
