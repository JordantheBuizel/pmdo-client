﻿// <copyright file="PlayerType.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal enum PlayerType
    {
        Generic,
        My
    }
}
