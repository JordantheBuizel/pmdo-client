﻿// <copyright file="GenericPlayer.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    internal class GenericPlayer : IPlayer
    {
        /// <inheritdoc/>
        public PlayerType PlayerType
        {
            get { return PlayerType.Generic; }
        }

        /// <inheritdoc/>
        public Graphic.SpriteSheet SpriteSheet
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public string Name
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int IdleTimer { get; set; }
        /// <inheritdoc/>
        public int IdleFrame { get; set; }
        /// <inheritdoc/>
        public int LastWalkTime { get; set; }
        /// <inheritdoc/>
        public int WalkingFrame { get; set; }

        /// <inheritdoc/>
        public string MapID
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int X
        {
            get { return this.Location.X; }
            set { this.Location = new Point(value, this.Location.Y); }
        }

        /// <inheritdoc/>
        public int Y
        {
            get { return this.Location.Y; }
            set { this.Location = new Point(this.Location.X, value); }
        }

        /// <inheritdoc/>
        public Point Location
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public string Guild
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.GuildRank GuildAccess
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public string Status
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Sprite
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Form { get; set; }
        /// <inheritdoc/>
        public Enums.Coloration Shiny { get; set; }
        /// <inheritdoc/>
        public Enums.Sex Sex { get; set; }

        /// <inheritdoc/>
        public bool Hunted
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public bool Dead
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.Rank Access
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.Direction Direction
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public string ID
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.MovementSpeed MovementSpeed { get; set; }

        /// <inheritdoc/>
        public Enums.StatusAilment StatusAilment { get; set; }

        /// <inheritdoc/>
        public List<int> VolatileStatus
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public bool Attacking
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int TotalAttackTime { get; set; }

        /// <inheritdoc/>
        public int AttackTimer
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Point Offset { get; set; }

        // public Enums.Size Size
        // {
        //    get;
        //    set;
        // }

        /// <inheritdoc/>
        public bool Leaving { get; set; }

        /// <inheritdoc/>
        public bool ScreenActive { get; set; }

        /// <inheritdoc/>
        public int SleepTimer
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int SleepFrame
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Graphic.Renderers.Sprites.SpeechBubble CurrentSpeech { get; set; }

        /// <inheritdoc/>
        public Graphic.Renderers.Sprites.Emoticon CurrentEmote { get; set; }

        /// <inheritdoc/>
        public PlayerPet[] Pets
        {
            get;
            set;
        }

        public GenericPlayer()
        {
            this.Pets = new PlayerPet[MaxInfo.MAXACTIVETEAM];
            this.VolatileStatus = new List<int>();
        }
    }
}
