﻿// <copyright file="PlayerManager.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;

    internal class PlayerManager
    {
        private static bool loggedIn = false;
        private static string myConnectionID = string.Empty;
        private static PlayerCollection players;
        private static MyPlayer myPlayer;

        public static string MyConnectionID
        {
            get { return myConnectionID; }
            set { myConnectionID = value; }
        }

        public static PlayerCollection Players
        {
            get { return players; }
        }

        public static void Initialize()
        {
            players = new PlayerCollection();
        }

        // public static bool IsPlaying(int index) {
        //    return (!string.IsNullOrEmpty(players[index].Name));
        // }
        public static bool IsPlaying(string id)
        {
            foreach (IPlayer player in players.GetAllPlayers())
            {
                if (player.ID == id)
                {
                    return true;
                }
            }

            return false;
        }

        public static MyPlayer MyPlayer
        {
            get
            {
                if (players == null)
                {
                    return null;
                }

                if (myPlayer == null)
                {
                    myPlayer = players[myConnectionID] as MyPlayer;
                }

                return myPlayer;
            }
        }
    }
}