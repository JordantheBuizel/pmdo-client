﻿// <copyright file="PlayerArrow.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class PlayerArrow
    {
        public int Arrow
        {
            get; set;
        }

        public int ArrowAmount
        {
            get; set;
        }

        public int ArrowAnim
        {
            get; set;
        }

        public int ArrowNum
        {
            get; set;
        }

        public int ArrowPosition
        {
            get; set;
        }

        public int ArrowTime
        {
            get; set;
        }

        public int ArrowVarX
        {
            get; set;
        }

        public int ArrowVarY
        {
            get; set;
        }

        public int ArrowX
        {
            get; set;
        }

        public int ArrowY
        {
            get; set;
        }
    }
}