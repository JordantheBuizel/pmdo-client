﻿// <copyright file="PartyData.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players.Parties
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class PartyData
    {
        private readonly PartyMember[] members;

        public PartyMember[] Members
        {
            get { return this.members; }
        }

        public PartyData()
        {
            this.members = new PartyMember[4];
        }

        public void LoadMember(int slot, string name, int mugshot, int form, Enums.Coloration shiny, Enums.Sex gender, ulong exp, ulong maxExp, int hp, int maxHP)
        {
            this.members[slot] = new PartyMember();
            this.members[slot].Name = name;
            this.members[slot].MugshotNum = mugshot;
            this.members[slot].MugshotForm = form;
            this.members[slot].MugshotShiny = shiny;
            this.members[slot].MugshotGender = gender;
            this.members[slot].Exp = exp;
            this.members[slot].MaxExp = maxExp;
            this.members[slot].HP = hp;
            this.members[slot].MaxHP = maxHP;
        }

        public void ClearSlot(int slot)
        {
            this.members[slot] = null;
        }
    }
}
