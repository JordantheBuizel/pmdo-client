﻿// <copyright file="Inventory.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Inventory
    {
        private readonly InventoryItem[] items;

        public Inventory(int maxItems)
        {
            this.items = new InventoryItem[maxItems];
            for (int i = 0; i < this.items.Length; i++)
            {
                this.items[i] = new InventoryItem();
            }
        }

        public InventoryItem this[int index]
        {
            get
            {
                return this.items[index - 1];
            }
        }

        public int Length
        {
            get { return this.items.Length; }
        }
    }
}
