﻿// <copyright file="PlayerCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;

    internal class PlayerCollection
    {
        private readonly ListPair<string, IPlayer> players;
        private readonly object lockObject = new object();

        // public ListPair<string, IPlayer> Players {
        //    get { return players; }
        // }
        public PlayerCollection()
        {
            this.players = new ListPair<string, IPlayer>();
        }

        public IPlayer this[string connectionID]
        {
            get
            {
                lock (this.lockObject)
                {
                    if (this.players.ContainsKey(connectionID) == false)
                    {
                        return null;
                    }
else
                    {
                        return this.players[connectionID];
                    }
                }
            }

            set
            {
                lock (this.lockObject)
                {
                    this.players[connectionID] = value;
                }
            }
        }

        public void Clear()
        {
            lock (this.lockObject)
            {
                this.players.Clear();
            }
        }

        public void Add(string connectionID, IPlayer player)
        {
            lock (this.lockObject)
            {
                if (!this.players.ContainsKey(connectionID))
                {
                    this.players.Add(connectionID, player);
                }
else
                {
                    this.players[connectionID] = player;
                }
            }
        }

        public void Remove(string connectionID)
        {
            lock (this.lockObject)
            {
                this.players.RemoveAtKey(connectionID);
            }
        }

        public IEnumerable<IPlayer> GetAllPlayers()
        {
            lock (this.lockObject)
            {
                foreach (IPlayer player in this.players.Values)
                {
                    yield return player;
                }
            }
        }
    }
}
