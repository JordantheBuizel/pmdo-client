﻿// <copyright file="Friend.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Friend
    {
        public string Name
        {
            get; set;
        }

        public bool Online
        {
            get; set;
        }
    }
}