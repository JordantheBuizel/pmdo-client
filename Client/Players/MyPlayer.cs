﻿// <copyright file="MyPlayer.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Players
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using ExpKit.Modules;
    using Missions;
    using Network;
    using Windows;

    internal class MyPlayer : IPlayer
    {
        /// <inheritdoc/>
        public Graphic.SpriteSheet SpriteSheet
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Sprite
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Form { get; set; }
        /// <inheritdoc/>
        public Enums.Coloration Shiny { get; set; }

        /// <inheritdoc/>
        public Enums.Sex Sex
        {
            get
            {
                return this.GetActiveRecruit().Sex;
            }

            set
            {
                this.GetActiveRecruit().Sex = value;
            }
        }

        /// <inheritdoc/>
        public PlayerType PlayerType
        {
            get { return PlayerType.My; }
        }

        /// <inheritdoc/>
        public int IdleTimer { get; set; }
        /// <inheritdoc/>
        public int IdleFrame { get; set; }
        /// <inheritdoc/>
        public int LastWalkTime { get; set; }
        /// <inheritdoc/>
        public int WalkingFrame { get; set; }

        public bool SwitchingSeamlessMaps { get; set; }

        /// <inheritdoc/>
        public string Name
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public string MapID
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int X
        {
            get { return this.Location.X; }
            set { this.Location = new Point(value, this.Location.Y); }
        }

        /// <inheritdoc/>
        public int Y
        {
            get { return this.Location.Y; }
            set { this.Location = new Point(this.Location.X, value); }
        }

        public int TargetX
        {
            get;
            set;
        }

        public int TargetY
        {
            get;
            set;
        }

        public int LastMovement { get; set; }

        public Enums.MovementSpeed StoryMovementSpeed { get; set; }

        public Algorithms.Pathfinder.PathfinderResult StoryPathfinderResult { get; set; }

        /// <inheritdoc/>
        public Point Location
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public bool Leaving { get; set; }

        /// <inheritdoc/>
        public bool ScreenActive { get; set; }

        /// <inheritdoc/>
        public int SleepTimer
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int SleepFrame
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public string Guild
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.GuildRank GuildAccess
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public string Status
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public bool Hunted
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public bool Dead
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.Rank Access
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public Enums.Direction Direction
        {
            get;
            set;
        }

        public bool MovementLocked
        {
            get;
            set;
        }

        public bool Solid
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public string ID
        {
            get;
            set;
        }

        public int Atk { get; set; }

        public int Def { get; set; }

        public int Spd { get; set; }

        public int SpAtk { get; set; }

        public int SpDef { get; set; }

        public ulong Exp { get; set; }

        public ulong MaxExp { get; set; }

        public int Belly { get; set; }

        public int MaxBelly { get; set; }

        public bool Confused { get; set; }
        /// <inheritdoc/>
        public Enums.StatusAilment StatusAilment
        {
            get
            {
                return this.GetActiveRecruit().StatusAilment;
            }

            set
            {
                this.GetActiveRecruit().StatusAilment = value;
            }
        }

        /// <inheritdoc/>
        public List<int> VolatileStatus
        {
            get;
            set;
        }

        public Enums.MovementSpeed SpeedLimit { get; set; }

        public Inventory Inventory { get; set; }

        public int Level
        {
            get
            {
                return this.GetActiveRecruit().Level;
            }

            set
            {
                this.GetActiveRecruit().Level = value;
            }
        }

        public bool[] Mobility
        {
            get;
            set;
        }

        public int TimeMultiplier
        {
            get;
            set;
        }

        public int Darkness
        {
            get;
            set;
        }

        // public int WeaponSlot { get; set; }
        // public int ArmorSlot { get; set; }
        // public int ShieldSlot { get; set; }
        // public int LegsSlot { get; set; }
        // public int NecklaceSlot { get; set; }
        // public int RingSlot { get; set; }
        // public int HelmetSlot { get; set; }
        private int activeTeamNum;

        public int ActiveTeamNum
        {
            get
            {
                return this.activeTeamNum;
            }

            set
            {
                this.activeTeamNum = value;
                WindowSwitcher.GameWindow.ActiveTeam.SetSelected(value);
            }
        }

        public int PauseTimer { get; set; }
        /// <inheritdoc/>
        public int AttackTimer { get; set; }
        /// <inheritdoc/>
        public int TotalAttackTime { get; set; }
        /// <inheritdoc/>
        public bool Attacking { get; set; }

        public int GetTimer { get; set; }

        public int TalkTimer { get; set; }

        public int TempMuteTimer { get; set; }

        public Recruit[] Team { get; set; }

        public RecruitMove[] Moves { get; set; }

        /// <inheritdoc/>
        public Enums.MovementSpeed MovementSpeed { get; set; }
        /// <inheritdoc/>
        public Point Offset { get; set; }

        private readonly JobList jobList;

        public int MissionExp { get; set; }

        public Enums.ExplorerRank ExplorerRank { get; set; }

        public Parties.PartyData Party { get; set; }

        public List<Friend> FriendsList { get; set; }

        public Maps.DungeonRoom CurrentRoom { get; set; }

        /// <inheritdoc/>
        public PlayerPet[] Pets { get; set; }

        public PacketList MovementPacketCache { get; set; }

        public int LastMovementCacheSend { get; set; }

        public MyPlayer()
        {
            // Initialize the job list
            this.jobList = new JobList();

            // Initialize the move list
            this.Moves = new RecruitMove[MaxInfo.MAXPLAYERMOVES];
            for (int i = 0; i < this.Moves.Length; i++)
            {
                this.Moves[i] = new RecruitMove();
            }

            // Initialize the player team
            this.Team = new Recruit[MaxInfo.MAXACTIVETEAM];
            this.Pets = new PlayerPet[MaxInfo.MAXACTIVETEAM];

            // Initialize the inventory and bank
            this.Inventory = new Inventory(MaxInfo.MaxInv);
            this.FriendsList = new List<Friend>();
            this.Mobility = new bool[16];
            this.TimeMultiplier = 1000;
            this.VolatileStatus = new List<int>();
            this.MapGoals = new List<MissionGoal>();
            this.ScreenActive = true;

            this.TargetX = -1;
            this.TargetY = -1;
        }

        public List<MissionGoal> MapGoals
        {
            get;
            set;
        }

        public JobList JobList
        {
            get { return this.jobList; }
        }

        /// <inheritdoc/>
        public Graphic.Renderers.Sprites.SpeechBubble CurrentSpeech { get; set; }

        /// <inheritdoc/>
        public Graphic.Renderers.Sprites.Emoticon CurrentEmote { get; set; }

        public Recruit GetActiveRecruit()
        {
            return this.Team[this.ActiveTeamNum];
        }

        public int GetInvItemNum(int invSlot)
        {
            if (invSlot > 0)
            {
                return this.Inventory[invSlot].Num;
            }
else
            {
                return 0;
            }
        }

        public int GetInvItemAmount(int invSlot)
        {
            if (invSlot > -1)
            {
                return this.Inventory[invSlot].Value;
            }
else
            {
                return 0;
            }
        }

        public bool GetInvItemSticky(int invSlot)
        {
            if (invSlot > -1)
            {
                return this.Inventory[invSlot].Sticky;
            }
else
            {
                return false;
            }
        }

        public bool IsEquiped(int invSlot)
        {
            for (int i = 0; i < 4; i++)
            {
                if (this.Team[i] != null && this.Team[i].HeldItemSlot == invSlot)
                {
                    return true;
                }
            }

            return false;
        }

        public void UseMove(int moveSlot)
        {
            if (this.Moves[moveSlot].MoveNum > 0  && this.MovementSpeed != Enums.MovementSpeed.Slip)
            {
                int attackSpeed = 0;

                // if (GetActiveRecruit().HeldItemSlot > 0) {
                //    attackSpeed = Items.ItemHelper.Items[Players.PlayerManager.MyPlayer.GetInvItemNum(Players.PlayerManager.MyPlayer.GetActiveRecruit().HeldItemSlot)].AttackSpeed;
                // } else {
                attackSpeed = Logic.Moves.MoveHelper.Moves[this.Moves[moveSlot].MoveNum].HitTime;

                // }
                attackSpeed = attackSpeed * PlayerManager.MyPlayer.TimeMultiplier / 1000;

                if (PlayerManager.MyPlayer.AttackTimer < Globals.Tick && PlayerManager.MyPlayer.Attacking == false)
                {
                    Messenger.SendUseMove(moveSlot);
                    this.Attacking = true;
                    this.AttackTimer = Globals.Tick + attackSpeed;
                    this.TotalAttackTime = attackSpeed;
                    if (Logic.Moves.MoveHelper.Moves[this.Moves[moveSlot].MoveNum].HitFreeze)
                    {
                        this.PauseTimer = Globals.Tick + attackSpeed;
                    }

                    // no more having to cast while standing
                }
            }
else
            {
                KitChat chat = (KitChat)System.Windows.Forms.Application.OpenForms["kitChat"];
                if (chat != null)
                {
                    chat.AppendChat("There is no move here!", Color.Red);
                }
            }
        }

        public void ForgetMove(int moveSlot)
        {
            // Dim CstSplWalk As String
            //            Dim CstSplWalk2 As String
            if (this.Moves[moveSlot].MoveNum > 0)
            {
                Messenger.SendForgetMove(moveSlot);
            }
else
            {
                // TODO: AddText("There is no move here!");
            }
        }

        public void ShiftMove(int moveSlot, bool shiftUp)
        {
            // Dim CstSplWalk As String
            //            Dim CstSplWalk2 As String
            // if (Moves[moveSlot].MoveNum > 0)
            // {
            if (moveSlot < 1 && shiftUp || moveSlot > 2 && !shiftUp)
            {
                // tell the player it can't be done in some way (maybe beep at him...)
            }
else
            {
                Messenger.SendShiftMove(moveSlot, shiftUp);
            }

            // }
            // else
            // {
            // shifting empty move slots is allowed
            // }
        }

        public void SwapMoves(int oldMoveSlot, int newMoveSlot)
        {
            Messenger.SendSwapMoves(oldMoveSlot, newMoveSlot);
        }

        // public Enums.Size Size {
        //    get;
        //    set;
        // }
        public void CharSwap(int slot)
        {// may require timing restrictions?
            if (slot == this.ActiveTeamNum)
            {
                // (insert nickname) is already in!
            }
else
            {
                Messenger.SendActiveCharSwap(slot);
            }
        }

        public void LeaderSwap(int slot)// may require timing restrictions?
        {
            if (slot == 0)
            {
                // That Pokémon is already leader!
            }
else
            {
                Messenger.SendSwitchLeader(slot);
            }
        }

        public void SendHome(int slot)// may require timing restrictions?
        {
            if (slot == 0)
            {
                // The leader cannot be sent home!
            }
else
            {
                Messenger.SendRemoveFromTeam(slot);
            }
        }

        public void SetCurrentRoom()
        {
            if (this.MapID.StartsWith("rd"))
            {
                this.CurrentRoom = this.GetTargetRoom();

                if (PlayerManager.MyPlayer.CurrentRoom.Width < 3)
                {
                    if (PlayerManager.MyPlayer.CurrentRoom.Height > 2)
                    {
                        this.CurrentRoom.Y = this.Y - 1;
                        this.CurrentRoom.Height = 2;
                    }
                }

                if (PlayerManager.MyPlayer.CurrentRoom.Height < 3)
                {
                    if (PlayerManager.MyPlayer.CurrentRoom.Width > 2)
                    {
                        this.CurrentRoom.X = this.X - 1;
                        this.CurrentRoom.Width = 2;
                    }
                }
            }
else
            {
                if (PlayerManager.MyPlayer.CurrentRoom == null)
                {
                    this.CurrentRoom = new Maps.DungeonRoom(0, 0, 0, 0);
                }

                this.CurrentRoom.X = 0;
                this.CurrentRoom.Y = 0;
                this.CurrentRoom.Width = Maps.MapHelper.ActiveMap.MaxX;
                this.CurrentRoom.Height = Maps.MapHelper.ActiveMap.MaxY;
            }
        }

        public Maps.DungeonRoom GetTargetRoom()
        {
            Maps.Map map = Maps.MapHelper.ActiveMap;
            int targetX = this.X;
            int targetY = this.Y;

            int x1 = targetX;
            int y1 = targetY;
            int blockedCount = 0;

            int leftXDistance = -1;
            int rightXDistance = -1;

            int upYDistance = -1;
            int downYDistance = -1;

            int roomStartX = -1;
            int roomWidth = -1;

            int roomStartY = -1;
            int roomHeight = -1;

            while (true)
            {
                // Keep going left until we've hit a wall...
                x1--;
                blockedCount = 0;
                if (x1 < 0)
                {
                    roomStartX = 0;
                    break;
                }

                if (GameProcessor.IsBlocked(map, x1, targetY))
                {
                    blockedCount++;
                }

                if (GameProcessor.IsBlocked(map, x1, targetY - 1))
                {
                    blockedCount++;
                }

                if (GameProcessor.IsBlocked(map, x1, targetY + 1))
                {
                    blockedCount++;
                }

                if (blockedCount == 3 || blockedCount == 2)
                {
                    // This means that a hallway was found between blocks!
                    leftXDistance = x1;
                    roomStartX = x1;
                    break;
                }
            }

            x1 = targetX;
            while (true)
            {
                // Keep going right until we've hit a wall...
                x1++;
                blockedCount = 0;
                if (x1 > map.MaxX)
                {
                    roomWidth = map.MaxX - roomStartX;
                    break;
                }

                if (GameProcessor.IsBlocked(map, x1, targetY))
                {
                    blockedCount++;
                }

                if (GameProcessor.IsBlocked(map, x1, targetY - 1))
                {
                    blockedCount++;
                }

                if (GameProcessor.IsBlocked(map, x1, targetY + 1))
                {
                    blockedCount++;
                }

                if (blockedCount == 3 || blockedCount == 2)
                {
                    // This means that a hallway was found between blocks!
                    rightXDistance = x1;
                    roomWidth = (x1 - targetX) + (targetX - roomStartX);
                    break;
                }
            }

            while (true)
            {
                // Keep going up until we've hit a wall...
                y1--;
                blockedCount = 0;
                if (y1 < 0)
                {
                    roomStartY = 0;
                    break;
                }

                if (GameProcessor.IsBlocked(map, targetX, y1))
                {
                    blockedCount++;
                }

                if (GameProcessor.IsBlocked(map, targetX - 1, y1))
                {
                    blockedCount++;
                }

                if (GameProcessor.IsBlocked(map, targetX + 1, y1))
                {
                    blockedCount++;
                }

                if (blockedCount == 3 || blockedCount == 2)
                {
                    // This means that a hallway was found between blocks!
                    upYDistance = y1;
                    roomStartY = y1;
                    break;
                }
            }

            y1 = targetY;
            while (true)
            {
                // Keep going down until we've hit a wall...
                y1++;
                blockedCount = 0;
                if (y1 > map.MaxY)
                {
                    roomHeight = map.MaxY - roomStartY;
                    break;
                }

                if (GameProcessor.IsBlocked(map, targetX, y1))
                {
                    blockedCount++;
                }

                if (GameProcessor.IsBlocked(map, targetX - 1, y1))
                {
                    blockedCount++;
                }

                if (GameProcessor.IsBlocked(map, targetX + 1, y1))
                {
                    blockedCount++;
                }

                if (blockedCount == 3 || blockedCount == 2)
                {
                    // This means that a hallway was found between blocks!
                    downYDistance = y1;
                    roomHeight = (y1 - targetY) + (targetY - roomStartY);
                    break;
                }
            }

            return new Maps.DungeonRoom(roomStartX, roomStartY, roomWidth, roomHeight);
        }
    }
}
