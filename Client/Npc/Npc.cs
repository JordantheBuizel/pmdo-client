﻿// <copyright file="Npc.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Npc
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Npc
    {
        public Npc()
        {
            this.ItemDrops = new NpcDrop[MaxInfo.MAXNPCDROPS];
        }

        public string AIScript
        {
            get;
            set;
        }

        public string AttackSay
        {
            get;
            set;
        }

        public Enums.NpcBehavior Behavior
        {
            get;
            set;
        }

        public NpcDrop[] ItemDrops
        {
            get;
            set;
        }

        public int Species { get; set; }

        public bool SpawnsAtDay { get; set; }

        public bool SpawnsAtNight { get; set; }

        public bool SpawnsAtDawn { get; set; }

        public bool SpawnsAtDusk { get; set; }

        public int[] Moves { get; set; }

        public string Name
        {
            get;
            set;
        }

        public int ShinyChance
        {
            get;
            set;
        }

        public int Size
        {
            get;
            set;
        }

        public int Form
        {
            get;
            set;
        }
    }
}