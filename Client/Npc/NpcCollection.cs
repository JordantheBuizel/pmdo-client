﻿// <copyright file="NpcCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Npc
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class NpcCollection
    {
        private readonly List<Npc> mNpcArray;

        internal NpcCollection(int maxNpcs)
        {
            this.mNpcArray = new List<Npc>(maxNpcs);
        }

        public Npc this[int index]
        {
            get { return this.mNpcArray[index - 1]; }
            set { this.mNpcArray[index - 1] = value; }
        }

        public void AddNpc(string name)
        {
            Npc npc = new Npc();
            npc.Name = name;
            npc.AttackSay = string.Empty;
            npc.Behavior = Enums.NpcBehavior.AttackOnSight;
            npc.ShinyChance = 0;
            for (int a = 0; a < MaxInfo.MAXNPCDROPS; a++)
            {
                npc.ItemDrops[a] = new NpcDrop();
                npc.ItemDrops[a].Chance = 0;
                npc.ItemDrops[a].ItemNum = 0;
                npc.ItemDrops[a].ItemValue = 0;
            }

            this.mNpcArray.Add(npc);
        }
    }
}