﻿// <copyright file="NpcDrop.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Npc
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class NpcDrop
    {
        public int Chance
        {
            get; set;
        }

        public int ItemNum
        {
            get; set;
        }

        public int ItemValue
        {
            get; set;
        }
    }
}