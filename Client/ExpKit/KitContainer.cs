﻿// <copyright file="KitContainer.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.ExpKit
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class KitContainer : Panel
    {
        private readonly ModuleSwitcher moduleSwitcher;
        private IKitModule activeModule;
        private readonly Button btnRight;
        private readonly Button btnLeft;
        private readonly Label lblModuleName;

        public ModuleSwitcher ModuleSwitcher
        {
            get { return this.moduleSwitcher; }
        }

        public new Size Size
        {
            get { return base.Size; }

            set
            {
                base.Size = value;
                this.RecalculateWidgetPositions();
            }
        }

        public IKitModule ActiveModule
        {
            get
            {
                return this.activeModule;
            }
        }

        public KitContainer(string name)
            : base(name)
            {
            this.BackColor = Color.FromArgb(32, 69, 79);

            this.moduleSwitcher = new ModuleSwitcher();

            this.btnRight = new Button("btnRight");
            this.btnRight.Size = new Size(30, 20);
            this.btnRight.Text = "->";
            Skins.SkinManager.LoadButtonGui(this.btnRight);
            this.btnRight.Click += new EventHandler<MouseButtonEventArgs>(this.BtnRight_Click);

            this.btnLeft = new Button("btnLeft");
            this.btnLeft.Size = new Size(30, 20);
            this.btnLeft.Text = "<-";
            Skins.SkinManager.LoadButtonGui(this.btnLeft);
            this.btnLeft.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLeft_Click);

            this.lblModuleName = new Label("lblModuleName");
            this.lblModuleName.BackColor = Color.Transparent;
            this.lblModuleName.AutoSize = true;
            this.lblModuleName.Location = new Point(0, 0);
            this.lblModuleName.ForeColor = Color.WhiteSmoke;

            this.AddWidget(this.btnRight);
            this.AddWidget(this.btnLeft);
            this.AddWidget(this.lblModuleName);

            // SetActiveModule(0);
        }

        private void BtnLeft_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.moduleSwitcher.AvailableKitModules.IndexOf(this.activeModule) - 1 >= 0)
            {
                this.SetActiveModule(this.moduleSwitcher.AvailableKitModules.IndexOf(this.activeModule) - 1);
            }
        }

        private void BtnRight_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.moduleSwitcher.AvailableKitModules.IndexOf(this.activeModule) + 1 < this.moduleSwitcher.AvailableKitModules.Count)
            {
                try
                {
                    this.SetActiveModule(this.moduleSwitcher.AvailableKitModules.IndexOf(this.activeModule) + 1);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " [Module: " + ((Enums.ExpKitModules)this.moduleSwitcher.AvailableKitModules.IndexOf(this.activeModule) + 1).ToString() + "]");
                }
            }
        }

        private void RecalculateWidgetPositions()
        {
            this.btnRight.Location = new Point(this.Width - this.btnRight.Width, 0);
            this.btnLeft.Location = new Point(this.Width - this.btnRight.Width - this.btnLeft.Width, 0);
        }

        public void SetActiveModule(Enums.ExpKitModules module)
        {
            for (int i = 0; i < this.moduleSwitcher.AvailableKitModules.Count; i++)
            {
                if (this.moduleSwitcher.AvailableKitModules[i].ModuleID == module)
                {
                    this.SetActiveModule(i);
                    break;
                }
            }
        }

        public void SetActiveModule(int index)
        {
            if (this.activeModule != null)
            {
                this.activeModule.SwitchOut();
                this.activeModule.ModulePanel.Hide();

                // this.RemoveWidget(activeModule.ModulePanel.Name);
            }

            this.activeModule = this.moduleSwitcher.GetAvailableKitModule(index);
            Network.Messenger.SendPacket(PMU.Sockets.TcpPacket.CreatePacket("activekitmodule", ((int)this.activeModule.ModuleID).ToString()));
            this.activeModule.Initialize(new Size(this.Width, this.Height - this.btnLeft.Height));
            this.lblModuleName.Text = this.activeModule.ModuleName + " [" + (index + 1) + "/" + this.moduleSwitcher.AvailableKitModules.Count + "]";
            if (this.ContainsWidget(this.activeModule.ModulePanel.Name) == false)
            {
                this.AddWidget(this.activeModule.ModulePanel);
            }

            this.activeModule.ModulePanel.Show();
        }

        /// <inheritdoc/>
        public override void OnTick(SdlDotNet.Core.TickEventArgs e)
        {
            base.OnTick(e);
            if (this.activeModule != null)
            {
                if (this.activeModule.ModulePanel.Location.X != 0 || this.activeModule.ModulePanel.Location.Y != this.btnLeft.Height)
                {
                    this.activeModule.ModulePanel.Location = new Point(0, this.btnLeft.Height);
                }

                if (this.activeModule.ModulePanel.Size.Width != this.Width || this.activeModule.ModulePanel.Size.Height != this.Height - this.btnLeft.Height)
                {
                    this.activeModule.ModulePanel.Size = new Size(this.Width, this.Height - this.btnLeft.Height);
                }

                // activeModule.ModulePanel.BlitToScreen(destinationSurface);
            }

            // if (activeModule != null) {
            //    activeModule.ModulePanel.OnTick(e);
            // }
        }

        /// <inheritdoc/>
        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);

            // if (activeModule != null) {
            //    activeModule.ModulePanel.OnMouseDown(new MouseButtonEventArgs(e));
            // }
        }

        /// <inheritdoc/>
        public override void OnMouseMotion(SdlDotNet.Input.MouseMotionEventArgs e)
        {
            base.OnMouseMotion(e);

            // if (activeModule != null) {
            //    activeModule.ModulePanel.OnMouseMotion(e);
            // }
        }

        /// <inheritdoc/>
        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);

            // if (activeModule != null) {
            //    activeModule.ModulePanel.OnMouseUp(new MouseButtonEventArgs(e));
            // }
        }
    }
}
