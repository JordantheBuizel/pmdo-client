﻿// <copyright file="kitChat.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

using Client.Logic.Extensions;

namespace Client.Logic.ExpKit.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Text;
    using System.IO;
    using System.Reflection;
    using System.Windows.Forms;
    using System.Xml;
    using Graphic;

    public partial class KitChat : Form
    {
        private readonly List<string> availableComms = new List<string>
        {
            "/gmmode",
            "/p",
            "/savelogs",
            "/textstory",
            "/updateserver",
            "/buizel",
            "/spawnminions",
            "/serverstatus",
            "/togglequiz",
            "/questionready",
            "/yatterman",
            "/glomp",
            "/setquizanswer",
            "/cry",
            "/calm",
            "/uhoh",
            "/buzz",
            "/endgame",
            "/snowballplayers",
            "/gmmode",
            "/copymap",
            "/packetcaching",
            "/foolsstory",
            "/foolsmode",
            "/lokmovementall",
            "/unlokmovementall",
            "/currentsection",
            "/resetstory",
            "/storymode",
            "/staffauction",
            "/itemowners",
            "/givejob",
            "/dungeonnpcs",
            "/rdstartcheck",
            "/activemaps",
            "/daynight",
            "/currenttime",
            "/tournyplayers",
            "/createtourny",
            "/jointourny",
            "/estlevel",
            "/april",
            "/restartserver",
            "/checktime",
            "/checkparty",
            "/playtime",
            "/shutdown",
            "/serverinfo",
            "/nuke",
            "/warn*",
            "/warn",
            "/getcharinfo",
            "/clearjoblist",
            "/void",
            "/voidplayer*",
            "/voidplayer",
            "/unvoid",
            "/getid",
            "/regenboard",
            "/who",
            "/saveall",
            "/isveteran",
            "/explode",
            "/wind",
            "/silentkick*",
            "/silentkick",
            "/kick*",
            "/kick",
            "/ban*",
            "/ban",
            "/htest",
            "/findplayer",
            "/emptyhouse",
            "/leavehouse",
            "/cmapper",
            "/hcommands",
            "/houseentrance",
            "/time",
            "/houseroof",
            "/houseweather",
            "/popcorn",
            "/houselight",
            "/:3",
            "/houseexpand",
            "/houseshop",
            "/housesign",
            "/housesound",
            "/housenotice",
            "/cmenu",
            "/darktest",
            "/kipz",
            "/stafflist",
            "/isitrandom",
            "/rdungeonscriptgoal",
            "/fdh",
            "/findaccount",
            "/cc",
            "/regenlotto",
            "/lottostats",
            "/playeros",
            "/lounge",
            "/gccollect",
            "/adminmsg",
            "/motd",
            "/ctfcreate",
            "/ctfjoin",
            "/ctfleave",
            "/ctfstart",
            "/ctfflags",
            "/ctfend",
            "/ctfforceend",
            "/ctf",
            "/ctft",
            "/ctfgen",
            "/checkstack",
            "/checkinv*",
            "/checkinv",
            "/clearinv",
            "/checkbank*",
            "/checkbank",
            "/checktile",
            "/checkmoves",
            "/checkmissions",
            "/masswarpauction",
            "/createauction",
            "/startauction",
            "/endauction",
            "/auctionadminhelp",
            "/auctionhelp",
            "/checkbidder",
            "/setauctionitem",
            "/setauctionminbid",
            "/setbidincrement",
            "/bid",
            "/handoutbirthdayribbon",
            "/setname",
            "/hb",
            "/pk",
            "/eat",
            "/._.",
            "/updatenews",
            "/reloadnews",
            "/news",
            "/givemove",
            "/hunt",
            "/learnmove",
            "/give",
            "/rps",
            "/sleep",
            "/take",
            "/save",
            "/s",
            "/g",
            "/finditemend",
            "/finditem",
            "/itemcheck",
            "/itemreq",
            "/itemdesc",
            "/findnpc",
            "/findnpcuse",
            "/finditemuse",
            "/online",
            "/finddex",
            "/findmove",
            "/findmoverange",
            "/findrdungeon",
            "/finddungeon",
            "/findpokemon",
            "/addfriend",
            "/removefriend",
            "/unlockstory",
            "/setstorystate",
            "/resetstats",
            "/recruitnum",
            "/storytest",
            "/teststory",
            "/teststorybreak",
            "/findlockedstory",
            "/mute*",
            "/mute",
            "/permamute*",
            "/permamute",
            "/unmute*",
            "/unmute",
            "/kill",
            "/jail",
            "/unjail",
            "/tostartall",
            "/tostart*",
            "/tostart",
            "/world",
            "/warpto",
            "/activatemap",
            "/summon*",
            "/summon",
            "/masswarptome",
            "/help",
            "/viewemotes",
            "/map*",
            "/map",
            "/tcpid",
            "/playerin",
            "/playerindungeon",
            "/playerintc",
            "/playerindungeons",
            "/hp",
            "/playerid",
            "/forceswap",
            "/info*",
            "/info",
            "/statusinfo",
            "/getip*",
            "/getip",
            "/findip",
            "/ability",
            "/abilities",
            "/praise",
            "/hug",
            "/notepad",
            "/me",
            "/poke",
            "/yawn",
            "/wave",
            "/away",
            "/wb*",
            "/wb",
            "/pichu!",
            "/muwaha",
            "/status",
            "/giveup",
            "/watch",
            "/stopwatch",
            "/sethouse",
            "/rstart",
            "/findstory",
            "/nextfloor",
            "/confuse",
            "/hittime",
            "/visible",
            "/infoexp",
            "/testexp",
            "/fixrank",
            "/;",
            "/'",
            "/*",
            "/)",
            "/))",
            "/)))",
            "/.",
            "/..",
            "/...",
            "/!",
            "/?",
            "/!?",
            "/+",
            "/testailment",
            "/addvstatus",
            "/removevstatus",
            "/diagonal",
            "/checkailment",
            "/checkdungeons",
            "/testegg",
            "/speed",
            "/testdeath",
            "/mobile",
            "/immobile",
            "/createparty",
            "/joinparty",
            "/leaveparty",
            "/myparty",
            "/kickparty",
            "/moduleswitch",
            "/sticky",
            "/thticky",
            "/trade",
            "/editemoticon",
            "/editemotions",
            "/edititem",
            "/edititems",
            "/editmove",
            "/editmoves",
            "/editdungeon",
            "/editdungeons",
            "/editnpc",
            "/editnpcs",
            "/editrdungeon",
            "/editrdungeons",
            "/editstory",
            "/editstories",
            "/mapreport",
            "/editevolutions",
            "/editevolution",
            "/editshop",
            "/editshops",
            "/testrecall",
            "/test",
            "/setform",
            "/setgender",
            "/offlinetostart",
            "/fixhouse",
            "/copycharacter",
            "/editscript"
        };

        private string selectedChannel = "Local";

        private bool drag = false; // determine if we should be moving the form
        private Point startPoint = new Point(0, 0); // also for the moving

        private readonly PrivateFontCollection pfc = new PrivateFontCollection();

        private static KitChat instance;

        public static KitChat Instance
        {
            get { return instance; }
        }

        public delegate void Action();

        private string[] cachedTextArray = new string[0];
        private int upArrowCounter = 0;

        public RichTextBox Chat
        {
            get
            {
                return this.rtbChat;
            }
        }

        public KitChat()
        {
            this.InitializeComponent();
            this.RecalculatePositions();

            this.TopMost = false;

            this.pbHeader.MouseDown += new MouseEventHandler(this.Title_MouseDown);
            this.pbHeader.MouseUp += new MouseEventHandler(this.Title_MouseUp);
            this.pbHeader.MouseMove += new MouseEventHandler(this.Title_MouseMove);
            this.txtCommands.KeyDown += new KeyEventHandler(this.TxtCommands_KeyDown);
            this.checkBox1.CheckedChanged += new EventHandler(this.CheckBox1_CheckedChanged);

            // rtbChat.Font = LoadWindowsFont("PMU", 9);
            // channelSelector.Font = LoadWindowsFont("PMU", 9);
            // lblChannel.Font = LoadWindowsFont("tahoma", 9);
            // checkBox1.Font = LoadWindowsFont("tahoma", 9);
            this.pbHeader.Image = Image.FromFile(Path.Combine(Application.StartupPath, "Skins/" + Skins.SkinManager.ActiveSkin.Name + "/Game Window/Widgets/expkitTitleBar.png"));

            string fontLoc = Path.Combine(Application.StartupPath, "Fonts", "PMD.ttf");
            this.pfc.AddFontFile(fontLoc);
            this.rtbChat.Font = new Font(this.pfc.Families[0], 12);
            this.rtbChat.LinkClicked += new LinkClickedEventHandler(this.RtbChat_LinkClicked);
            this.channelSelector.Font = new Font(this.pfc.Families[0], 12);

            this.txtCommands.PreviewKeyDown += new PreviewKeyDownEventHandler(this.TxtCommands_PreviewKeyDown);
        }

        private void TxtCommands_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Tab)
            {
                e.IsInputKey = true;
            }
        }

        private void RtbChat_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Process.Start(e.LinkText);
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = !this.TopMost;
        }

        /// <inheritdoc/>
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            instance = this;
        }

        /// <inheritdoc/>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            instance = null;
        }

        public void AppendChat(string text, Color color, bool containsName, bool staffName, Color staffColor)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<string, Color, bool, bool, Color>(this.AppendChat), new object[] { text, color, containsName, staffName, staffColor });
                return;
            }

            this.AppendChat(text, color, staffName, staffColor, containsName);
        }

        public void AppendChat(string text, SdlDotNet.Widgets.CharRenderOptions renderOptions)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<string, SdlDotNet.Widgets.CharRenderOptions>(this.AppendChat), new object[] { text, renderOptions });
                return;
            }

            this.AppendChat(text, renderOptions.ForeColor);
        }

        public void AppendChat(string text, Color color)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<string, Color>(this.AppendChat), new object[] { text, color });
                return;
            }

            this.AppendChat(text, color, false, color, false);
        }

        public void AppendChat(string text, Color color, bool foundStaffName, Color staffColor, bool foundName)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<string, Color, bool, Color, bool>(this.AppendChat), new object[] { text, color, foundStaffName, staffColor, foundName });
                return;
            }

            if (IO.Options.EnableRolePlay == false && text.StartsWith("[RP]"))
            {
            }
            else
            {
                if (IO.Options.Timestamps)
                {
                    this.rtbChat.AppendText("[" + DateTime.Now.ToLongTimeString() + "] " + text);
                }
                else if (text.Contains("Error: GetCharacterWeather"))
                {
                }
                else
                {
                    if (foundName)
                    {
                        string[] findName = text.Split(' ');

                        int nameIndex = -1;
                        for (int i = 0; i < findName.Length; i++)
                        {
                            // No more PMU workarounds!
                            /*if (findName[i].StartsWith("[c]") && findName[i].EndsWith("[/c]:"))
                            {
                                foundStaffName = true;
                                string removeStart = findName[i].TrimStart('[', 'c', ']', '[');
                                List<char> letters = new List<char>();
                                foreach (char character in findName[i].ToCharArray())
                                {
                                    int dummy;
                                    if (!Int32.TryParse(character.ToString(), out dummy))
                                    {
                                        letters.Add(character);
                                    }
                                }
                                string nameColorString = removeStart.TrimEnd(letters.ToArray());
                                try
                                {
                                    staffColor = Color.FromArgb((int)uint.Parse(nameColorString));
                                }
                                catch
                                {
                                    staffColor = Color.FromArgb(int.Parse(nameColorString));
                                }

                                int colorCodeIndex = -1;
                                for (int j = 0; j < findName[i].ToCharArray().Length; j++)
                                {
                                    if (findName[i].ToCharArray()[j] == '[')
                                    {
                                        if (findName[i].ToCharArray()[j + 1] == 'c')
                                        {
                                            colorCodeIndex = j;
                                            break;
                                        }
                                    }
                                }
                                List<char> countLingeringSDL = new List<char>
                                {
                                    '[',
                                    'c',
                                    ']',
                                    '['
                                };
                                foreach (char character in nameColorString)
                                {
                                    countLingeringSDL.Add(character);
                                }
                                countLingeringSDL.Add(']');
                                findName[i] = findName[i].Remove(colorCodeIndex, countLingeringSDL.Count);
                                findName[i] = findName[i].TrimEnd('[', '/', 'c', ']', ':');
                                findName[i] = findName[i].Insert(findName[i].Length, ":");
                                nameIndex = i;
                            }*/

                            /*else*/
                            if (findName[i].EndsWith(":"))
                            {
                                nameIndex = i;
                                break;
                            }
                        }

                        for (int i = 0; i < findName.Length; i++)
                        {
                            if (i == nameIndex)
                            {
                                if (foundStaffName)
                                {
                                    this.rtbChat.AppendText(findName[i] + " ", staffColor);
                                }

                                /*else if (color == Color.FromArgb(255, 255, 255, 254))
                                {
                                    rtbChat.AppendText(findName[i] + " ", color);
                                }*/
                                else
                                {
                                    this.rtbChat.AppendText(findName[i] + " ", Color.Red);
                                }
                            }
                            else
                            {
                                this.rtbChat.AppendText(findName[i] + " ", color);
                            }
                        }
                    }
                    else
                    {
                        this.rtbChat.AppendText(text, color);
                    }

                    foundName = false;
                }
            }

            if (IO.Options.AutoScroll)
            {
                this.rtbChat.ScrollToCaret();
            }
        }

        private bool shouldAutoComplete = false;

        private void TxtCommands_KeyDown(object sender, KeyEventArgs e)
        {
            List<string> textCache = new List<string>();

            if (e.KeyData == Keys.Tab)
            {
                this.shouldAutoComplete = !this.shouldAutoComplete;
                if (this.shouldAutoComplete && Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Mapper))
                {
                    AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
                    coll.AddRange(this.availableComms.ToArray());
                    this.txtCommands.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    this.txtCommands.AutoCompleteCustomSource = coll;
                    this.txtCommands.AutoCompleteMode = AutoCompleteMode.Append;
                }
                else
                {
                    this.txtCommands.AutoCompleteSource = AutoCompleteSource.None;
                    this.txtCommands.AutoCompleteMode = AutoCompleteMode.None;
                }
            }

            if (e.KeyData == Keys.Return)
            {
                e.Handled = e.SuppressKeyPress = true;
                this.txtCommands.AutoCompleteSource = AutoCompleteSource.None;
                this.txtCommands.AutoCompleteMode = AutoCompleteMode.None;
                if (IO.Options.EnableRolePlay == false && this.channelSelector.SelectedText == "Roleplay")
                {
                    this.AppendChat("You have roleplay disabled! Check your settings in Esc>Others>Settings (It must be clicked with the mouse to toggle)", Color.Red);
                }
                else
                {
                    if (Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Moniter))
                    {
                        XmlDocument cacheXml = new XmlDocument();
                        cacheXml.Load(Path.Combine(Application.StartupPath, "PersonalChatCache.xml"));
                        XmlNode cacheText = cacheXml.DocumentElement["CacheText"];

                        cacheText.InnerText = cacheText.InnerText + "," + this.txtCommands.Text;
                        cacheXml.Save(Path.Combine(Application.StartupPath, "PersonalChatCache.xml"));
                    }

                    CommandProcessor.ProcessCommand(this.txtCommands.Text, (Enums.ChatChannel)Enum.Parse(typeof(Enums.ChatChannel), this.selectedChannel, true));
                    this.txtCommands.Text = string.Empty;
                    this.upArrowCounter = 0;
                }
            }

            /*if (((e.Mod & SdlDotNet.Input.ModifierKeys.LeftControl) != 0) && (e.Key == SdlDotNet.Input.Key.X))
            {
                try
                {
                    txtCommands.Text = txtCommands.Text.Substring(0, txtCommands.Text.Length - 1);
                    System.Windows.Forms.Clipboard.SetText(txtCommands.Text);
                    txtCommands.Text = "";
                }
                catch
                {

                }
            }

            if (((e.Mod & SdlDotNet.Input.ModifierKeys.LeftControl) != 0) && e.Key == SdlDotNet.Input.Key.C)
            {
                try
                {
                    txtCommands.Text = txtCommands.Text.Substring(0, txtCommands.Text.Length - 1);
                    System.Windows.Forms.Clipboard.SetText(txtCommands.Text);
                }
                catch
                {

                }
            }

            if (((e.Mod & SdlDotNet.Input.ModifierKeys.LeftControl) != 0) && e.Key == SdlDotNet.Input.Key.V)
            {
                try
                {
                    txtCommands.Text = txtCommands.Text.Substring(0, txtCommands.Text.Length - 1);
                    txtCommands.Text = txtCommands.Text + System.Windows.Forms.Clipboard.GetText();
                }
                catch
                {
                    AppendChat("The paste text must be only standard text (abc123,./!$% etc.)", Color.Red);
                }
            }*/

            if (e.KeyData == Keys.Up)
            {
                XmlDocument cacheXml = new XmlDocument();
                cacheXml.Load(Path.Combine(Application.StartupPath, "PersonalChatCache.xml"));
                XmlNode cacheText = cacheXml.DocumentElement["CacheText"];

                this.cachedTextArray = cacheText.InnerText.Split(',');

                this.upArrowCounter++;

                try
                {
                    if (this.cachedTextArray.Length - this.upArrowCounter < 0)
                    {
                        this.upArrowCounter = 0;
                    }
                    else
                    {
                        this.txtCommands.Text = this.cachedTextArray[this.cachedTextArray.Length - this.upArrowCounter];
                    }
                }
                catch (Exception ex)
                {
                    if (Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Scriptor))
                    {
                        this.AppendChat("upArrowCounter Exception (Starting at line 195)", Color.Red);
                        this.AppendChat(ex.StackTrace, Color.Black);
                    }
                }
            }

            if (e.KeyData == Keys.Down)
            {
                XmlDocument cacheXml = new XmlDocument();
                cacheXml.Load(Path.Combine(Application.StartupPath, "PersonalChatCache.xml"));
                XmlNode cacheText = cacheXml.DocumentElement["CacheText"];

                this.cachedTextArray = cacheText.InnerText.Split(',');

                this.upArrowCounter--;

                try
                {
                    if (this.cachedTextArray.Length - this.upArrowCounter > this.cachedTextArray.Length - 1 || this.cachedTextArray.Length - this.upArrowCounter < 0)
                    {
                        this.upArrowCounter = this.cachedTextArray.Length - 1;
                    }
                    else
                    {
                        this.txtCommands.Text = this.cachedTextArray[this.cachedTextArray.Length - this.upArrowCounter];
                    }
                }
                catch (Exception ex)
                {
                    if (Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Scriptor))
                    {
                        this.AppendChat("dwnArrowCounter Exception (Starting at line 218)", Color.Red);
                        this.AppendChat(ex.StackTrace, Color.Black);
                    }
                }
            }
        }

        /*public void Created(int index)
        {
            moduleIndex = index;
        }

        public void SwitchOut()
        {

        }*/

        private void RecalculatePositions()
        {
            this.lblChannel.Font = FontManager.LoadWindowsFont("PMU", 9);

            int selectedIndex = this.channelSelector.SelectedIndex;
            this.channelSelector.Items.Clear();
            this.channelSelector.Items.Add("Local");
            this.channelSelector.Items.Add("Global");
            this.channelSelector.Items.Add("Guild");
            this.channelSelector.Items.Add("Roleplay");
            if (Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Moniter))
            {
                this.channelSelector.Items.Add("Staff");
            }

            if (selectedIndex < this.channelSelector.Items.Count && selectedIndex > -1)
            {
                this.channelSelector.SelectedIndex = selectedIndex;
            }
            else
            {
                this.channelSelector.SelectedIndex = 0;
            }

            // lblCounter.Size = new Size(containerSize.Width - 10, 30);
            // btnIncrement.Location = new Point(DrawingSupport.GetCenter(containerSize, btnIncrement.Size).X - (btnIncrement.Width / 2), lblCounter.Y + lblCounter.Height + 5);
            // btnDecrement.Location = new Point(DrawingSupport.GetCenter(containerSize, btnDecrement.Size).X + (btnDecrement.Width / 2), lblCounter.Y + lblCounter.Height + 5);
        }

        /*public int ModuleIndex
        {
            get { return moduleIndex; }
        }

        public string ModuleName
        {
            get { return "Chat"; }
        }

        public Panel ModulePanel
        {
            get { return this; }
        }


        public bool Enabled
        {
            get { return enabled; }
            set
            {
                enabled = value;
                if (EnabledChanged != null)
                    EnabledChanged(this, EventArgs.Empty);
            }
        }


        public event EventHandler EnabledChanged;


        public Enums.ExpKitModules ModuleID
        {
            get { return Enums.ExpKitModules.Chat; }
        }*/

        private void Label1_Click(object sender, EventArgs e)
        {
        }

        private void ChannelSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> channels = new List<string>
            {
                "Local",
                "Global",
                "Guild",
                "Roleplay",
                "Staff"
            };

            this.selectedChannel = channels[this.channelSelector.SelectedIndex];
        }

        private void Title_MouseUp(object sender, MouseEventArgs e)
        {
            this.drag = false;
        }

        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            this.startPoint = e.Location;
            this.drag = true;
        }

        private void Title_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.drag)
            { // if we should be dragging it, we need to figure out some movement
                Point p1 = new Point(e.X, e.Y);
                Point p2 = this.PointToScreen(p1);
                Point p3 = new Point(
                    p2.X - this.startPoint.X,
                                     p2.Y - this.startPoint.Y);
                this.Location = p3;
            }
        }

        public static Font LoadWindowsFont(string fontName, int emSize)
        {
            if (fontName.EndsWith(".ttf") == false)
            {
                fontName += ".ttf";
            }

            return new Font(IO.Paths.FontPath + fontName, emSize);
        }

        public void FocusChat(string dummy)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<string>(this.FocusChat), new object[] { dummy });
                return;
            }

            this.Focus();
            this.txtCommands.Focus();
            this.txtCommands.Show();
        }
    }
}

