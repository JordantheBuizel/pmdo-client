﻿// <copyright file="kitFriendsList.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.ExpKit.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class KitFriendsList : Panel, IKitModule
    {
        private int moduleIndex;
        private bool enabled;
        private Size containerSize;

        private readonly List<Label> friendNames;
        private readonly List<PictureBox> friendOnlineStatus;

        public KitFriendsList(string name)
            : base(name)
            {
            this.enabled = true;

            this.friendNames = new List<Label>();
            this.friendOnlineStatus = new List<PictureBox>();

            this.BackColor = Color.Transparent;
        }

        /// <inheritdoc/>
        public void SwitchOut()
        {
        }

        /// <inheritdoc/>
        public void Initialize(Size containerSize)
        {
            this.containerSize = containerSize;
            this.UpdateList(Players.PlayerManager.MyPlayer.FriendsList);
        }

        public void UpdateList(List<Players.Friend> friends)
        {
            if (friends.Count < this.friendNames.Count)
            {
                int widgetsToRemove = -1;
                widgetsToRemove = this.friendNames.Count - friends.Count;
                widgetsToRemove *= 2;
                for (int i = widgetsToRemove - 1; i >= 0; i--)
                {
                    this.RemoveWidget(this.ChildWidgets[(this.ChildWidgets.Count - 1) - i].Name);
                }

                for (int i = (widgetsToRemove / 2) - 1; i >= 0; i--)
                {
                    this.friendNames.RemoveAt(i);
                    this.friendOnlineStatus.RemoveAt(i);
                }
            }

            for (int i = 0; i < friends.Count; i++)
            {
                if (this.friendNames.Count <= i)
                {
                    Label lblName = new Label("lblName" + i);
                    lblName.Location = new Point(5, i * 25);
                    lblName.AutoSize = true;
                    lblName.Font = Graphic.FontManager.LoadFont("PMU", 16);
                    lblName.ForeColor = Color.WhiteSmoke;

                    PictureBox picOnlineStatus = new PictureBox("picOnlineStatus" + i);
                    picOnlineStatus.Size = new Size(20, 20);
                    picOnlineStatus.Location = new Point(this.containerSize.Width - picOnlineStatus.Width - 10, lblName.Y);
                    picOnlineStatus.BorderStyle = BorderStyle.FixedSingle;

                    this.AddWidget(lblName);
                    this.AddWidget(picOnlineStatus);

                    this.friendNames.Add(lblName);
                    this.friendOnlineStatus.Add(picOnlineStatus);
                }

                this.friendNames[i].Text = friends[i].Name;
                if (friends[i].Online)
                {
                    this.friendOnlineStatus[i].BackColor = Color.Green;
                }
else
                {
                    this.friendOnlineStatus[i].BackColor = Color.Red;
                }
            }
        }

        /// <inheritdoc/>
        public int ModuleIndex
        {
            get { return this.moduleIndex; }
        }

        /// <inheritdoc/>
        public string ModuleName
        {
            get { return "Friends List"; }
        }

        /// <inheritdoc/>
        public void Created(int index)
        {
            this.moduleIndex = index;
        }

        /// <inheritdoc/>
        public Panel ModulePanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Enabled
        {
            get { return this.enabled; }

            set
            {
                this.enabled = value;
                if (this.EnabledChanged != null)
                {
                    this.EnabledChanged(this, EventArgs.Empty);
                }
            }
        }

        /// <inheritdoc/>
        public event EventHandler EnabledChanged;

        /// <inheritdoc/>
        public Enums.ExpKitModules ModuleID
        {
            get { return Enums.ExpKitModules.FriendsList; }
        }
    }
}
