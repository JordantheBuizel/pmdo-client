﻿// <copyright file="kitMapReport.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.ExpKit.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class KitMapReport : Panel, IKitModule
    {
        private int currentTen = 0;
        private int moduleIndex;
        private bool mapNamesLoaded;
        private bool enabled;
        private string[] mapNames;
        private readonly ScrollingListBox lstMaps;
        private readonly Button btnLoadMapNames;
        private readonly VScrollBar vsbMaps;
        private readonly Label lblFindMap;
        private readonly TextBox txtFindMap;
        private readonly ScrollingListBox lstFindResults;

        public KitMapReport(string name)
            : base(name)
            {
            this.enabled = true;
            this.BackColor = Color.Transparent;

            this.lstMaps = new ScrollingListBox("lstMaps");
            this.lstMaps.Location = new Point(5, 5);
            this.lstMaps.MultiSelect = false;
            this.lstMaps.Height = 300;
            for (int i = 0; i < 10; i++)
            {
                ListBoxTextItem lbiMaps = new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), string.Empty); // (i + 1) + ": " + MapName);
                this.lstMaps.Items.Add(lbiMaps);
            }

            this.vsbMaps = new VScrollBar("vsbMaps");
            this.vsbMaps.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.VsbMaps_ValueChanged);
            this.vsbMaps.Maximum = 0;

            this.btnLoadMapNames = new Button("btnLoadMapNames");
            this.btnLoadMapNames.Size = new Size(100, 30);
            this.btnLoadMapNames.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.btnLoadMapNames.Text = "Load Maps";
            Skins.SkinManager.LoadButtonGui(this.btnLoadMapNames);
            this.btnLoadMapNames.Click += new EventHandler<MouseButtonEventArgs>(this.BtnLoadMapNames_Click);

            this.lblFindMap = new Label("lblFindMap");
            this.lblFindMap.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.lblFindMap.AutoSize = true;
            this.lblFindMap.Text = "Search for maps";
            this.lblFindMap.ForeColor = Color.WhiteSmoke;
            this.lblFindMap.Hide();

            this.txtFindMap = new TextBox("txtFindMap");
            this.txtFindMap.KeyDown += new EventHandler<SdlDotNet.Input.KeyboardEventArgs>(this.TxtFindMap_KeyDown);
            this.txtFindMap.Hide();

            this.lstFindResults = new ScrollingListBox("lstFindResults");
            this.lstFindResults.MultiSelect = false;
            this.lstFindResults.Hide();

            this.AddWidget(this.lstMaps);
            this.AddWidget(this.vsbMaps);
            this.AddWidget(this.btnLoadMapNames);
            this.AddWidget(this.lblFindMap);
            this.AddWidget(this.txtFindMap);
            this.AddWidget(this.lstFindResults);
        }

        private void TxtFindMap_KeyDown(object sender, SdlDotNet.Input.KeyboardEventArgs e)
        {
            if (e.Key == SdlDotNet.Input.Key.Return)
            {
                this.lstFindResults.Items.Clear();
                if (!string.IsNullOrEmpty(this.txtFindMap.Text) && this.txtFindMap.Text.Length > 2)
                {
                    for (int i = 0; i < this.mapNames.Length; i++)
                    {
                        if (this.mapNames[i].Contains(this.txtFindMap.Text))
                        {
                            this.lstFindResults.Items.Add(new ListBoxTextItem(Graphic.FontManager.LoadFont("tahoma", 10), (i + 1) + ": " + this.mapNames[i]));
                        }
                    }
                }
            }
        }

        private void BtnLoadMapNames_Click(object sender, MouseButtonEventArgs e)
        {
            if (this.btnLoadMapNames.Text != "Loading...")
            {
                Network.Messenger.MapReportRequest();
                this.btnLoadMapNames.Text = "Loading...";
            }
        }

        private void VsbMaps_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.currentTen = e.NewValue;

            this.RefreshMapList();
        }

        public void LoadAllMapNames(string[] mapNames)
        {
            this.btnLoadMapNames.Hide();
            this.mapNames = mapNames;
            this.currentTen = 0;
            this.vsbMaps.Maximum = (this.mapNames.Length / 10) - 1;
            this.mapNamesLoaded = true;
            this.RefreshMapList();

            this.lstFindResults.Show();
            this.lblFindMap.Show();
            this.txtFindMap.Show();
        }

        public void UpdateMapName(int slot, string newName)
        {
            if (this.mapNamesLoaded)
            {
                this.mapNames[slot - 1] = newName;
                this.RefreshMapList();
            }
        }

        public void RefreshMapList()
        {
            if (this.mapNamesLoaded)
            {
                for (int i = 0; i < 10; i++)
                {
                    if ((i + (this.currentTen * 10)) < this.mapNames.Length)
                    {
                        ((ListBoxTextItem)this.lstMaps.Items[i]).Text = ((i + 1) + (10 * this.currentTen)) + ": " + this.mapNames[i + (10 * this.currentTen)];
                    }
else
                    {
                        ((ListBoxTextItem)this.lstMaps.Items[i]).Text = "---";
                    }
                }
            }
        }

        /// <inheritdoc/>
        public void SwitchOut()
        {
        }

        /// <inheritdoc/>
        public void Initialize(Size containerSize)
        {
            this.lstMaps.Size = new Size(containerSize.Width - (this.lstMaps.X * 2) - 14, 140);
            this.vsbMaps.Size = new Size(14, this.lstMaps.Height);
            this.vsbMaps.Location = new Point(this.lstMaps.X + this.lstMaps.Width, this.lstMaps.Y);

            this.btnLoadMapNames.Location = new Point(5, this.lstMaps.Y + this.lstMaps.Height + 5);

            this.lblFindMap.Location = new Point(5, this.lstMaps.Y + this.lstMaps.Height + 5);
            this.txtFindMap.Location = new Point(5, this.lblFindMap.Y + this.lblFindMap.Height + 5);
            this.txtFindMap.Size = new Size(containerSize.Width - (this.txtFindMap.X * 2), 14);
            this.lstFindResults.Location = new Point(5, this.txtFindMap.Y + this.txtFindMap.Height + 5);
            this.lstFindResults.Size = new Size(containerSize.Width - (this.lstFindResults.X * 2), 100);
        }

        /// <inheritdoc/>
        public int ModuleIndex
        {
            get { return this.moduleIndex; }
        }

        /// <inheritdoc/>
        public string ModuleName
        {
            get { return "Map Report"; }
        }

        /// <inheritdoc/>
        public void Created(int index)
        {
            this.moduleIndex = index;
        }

        /// <inheritdoc/>
        public Panel ModulePanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Enabled
        {
            get { return this.enabled; }

            set
            {
                this.enabled = value;
                if (this.EnabledChanged != null)
                {
                    this.EnabledChanged(this, EventArgs.Empty);
                }
            }
        }

        /// <inheritdoc/>
        public event EventHandler EnabledChanged;

        /// <inheritdoc/>
        public Enums.ExpKitModules ModuleID
        {
            get { return Enums.ExpKitModules.MapReport; }
        }
    }
}
