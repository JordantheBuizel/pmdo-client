﻿// <copyright file="kitCounter.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.ExpKit.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class KitCounter : Panel, IKitModule
    {
        private int moduleIndex;
        private Size containerSize;
        private readonly Label lblCounter;
        private readonly Button btnIncrement;
        private readonly Button btnDecrement;
        private bool enabled;

        public KitCounter(string name)
            : base(name)
            {
            this.enabled = true;

            this.BackColor = Color.Transparent;

            this.lblCounter = new Label("lblCounter");
            this.lblCounter.Location = new Point(5, 5);
            this.lblCounter.Font = Graphic.FontManager.LoadFont("tahoma", 16);
            this.lblCounter.ForeColor = Color.WhiteSmoke;
            this.lblCounter.Text = "0";
            this.lblCounter.Centered = true;

            this.btnIncrement = new Button("btnIncrement");
            this.btnIncrement.Location = new Point(5, this.lblCounter.Y + this.lblCounter.Height + 5);
            this.btnIncrement.Size = new Size(30, 20);
            this.btnIncrement.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.btnIncrement.Text = "+";
            Skins.SkinManager.LoadButtonGui(this.btnIncrement);
            this.btnIncrement.Click += new EventHandler<MouseButtonEventArgs>(this.BtnIncrement_Click);

            this.btnDecrement = new Button("btnDecrement");
            this.btnDecrement.Location = new Point(this.btnIncrement.X + this.btnIncrement.Width, this.lblCounter.Y + this.lblCounter.Height + 5);
            this.btnDecrement.Size = new Size(30, 20);
            this.btnDecrement.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.btnDecrement.Text = "-";
            Skins.SkinManager.LoadButtonGui(this.btnDecrement);
            this.btnDecrement.Click += new EventHandler<MouseButtonEventArgs>(this.BtnDecrement_Click);

            this.AddWidget(this.lblCounter);
            this.AddWidget(this.btnIncrement);
            this.AddWidget(this.btnDecrement);
        }

        private void BtnDecrement_Click(object sender, MouseButtonEventArgs e)
        {
            this.lblCounter.Text = (this.lblCounter.Text.ToInt() - 1).ToString();
        }

        private void BtnIncrement_Click(object sender, MouseButtonEventArgs e)
        {
            this.lblCounter.Text = (this.lblCounter.Text.ToInt() + 1).ToString();
        }

        /// <inheritdoc/>
        public void Created(int index)
        {
            this.moduleIndex = index;
        }

        /// <inheritdoc/>
        public void SwitchOut()
        {
        }

        /// <inheritdoc/>
        public void Initialize(Size containerSize)
        {
            this.containerSize = containerSize;
            this.RecalculatePositions();
            this.RequestRedraw();
        }

        private void RecalculatePositions()
        {
            this.lblCounter.Size = new Size(this.containerSize.Width - 10, 30);
            this.btnIncrement.Location = new Point(DrawingSupport.GetCenter(this.containerSize, this.btnIncrement.Size).X - (this.btnIncrement.Width / 2), this.lblCounter.Y + this.lblCounter.Height + 5);
            this.btnDecrement.Location = new Point(DrawingSupport.GetCenter(this.containerSize, this.btnDecrement.Size).X + (this.btnDecrement.Width / 2), this.lblCounter.Y + this.lblCounter.Height + 5);
        }

        /// <inheritdoc/>
        public int ModuleIndex
        {
            get { return this.moduleIndex; }
        }

        /// <inheritdoc/>
        public string ModuleName
        {
            get { return "Counter"; }
        }

        /// <inheritdoc/>
        public Panel ModulePanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Enabled
        {
            get { return this.enabled; }

            set
            {
                this.enabled = value;
                if (this.EnabledChanged != null)
                {
                    this.EnabledChanged(this, EventArgs.Empty);
                }
            }
        }

        /// <inheritdoc/>
        public event EventHandler EnabledChanged;

        /// <inheritdoc/>
        public Enums.ExpKitModules ModuleID
        {
            get { return Enums.ExpKitModules.Counter; }
        }
    }
}
