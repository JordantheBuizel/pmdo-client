﻿// <copyright file="kitChat_old.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.ExpKit.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Extensions;
    using Graphic;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class KitChat_old : Panel, IKitModule
    {
        private int moduleIndex;
        private Size containerSize;
        private readonly Label lblChat;
        private readonly TextBox txtCommands;
        private bool enabled;
        private readonly Label lblChannel;
        private readonly ComboBox channelSelector;

        private string[] cachedTextArray = new string[0];
        private int upArrowCounter = 0;

        public Label Chat
        {
            get
            {
                return this.lblChat;
            }
        }

        public KitChat_old(string name)
            : base(name)
        {
            this.enabled = false;

            this.BackColor = Color.Transparent;

            this.lblChat = new Label("lblChat");

            // lblChat.Location = new Point(0, 0);
            // lblChat.Size = new Size(200, this.Height - 20);
            this.lblChat.AutoScroll = IO.Options.AutoScroll;
            this.lblChat.Font = FontManager.LoadFont("PMU", 16);

            this.lblChannel = new Label("lblChannel");
            this.lblChannel.Font = FontManager.LoadFont("tahoma", 15);
            this.lblChannel.Text = "Channel:";
            this.lblChannel.ForeColor = Color.WhiteSmoke;

            this.channelSelector = new ComboBox("channelSelector");

            this.txtCommands = new TextBox("txtCommands");
            Skins.SkinManager.LoadTextBoxGui(this.txtCommands);

            // txtCommands.Location = new Point(0, lblChat.Y + lblChat.Height);
            // txtCommands.Size = new Size(this.Width, 20);
            this.txtCommands.Font = FontManager.LoadFont("PMU", 16);
            this.txtCommands.KeyUp += new EventHandler<SdlDotNet.Input.KeyboardEventArgs>(this.TxtCommands_KeyUp);

            // txtCommands. = FontManager.LoadFont("tahoma", 12);
            this.AddWidget(this.lblChat);
            this.AddWidget(this.txtCommands);
            this.AddWidget(this.lblChannel);
            this.AddWidget(this.channelSelector);
        }

        public void AppendChat(string text, CharRenderOptions[] renderOptions)
        {
            if (IO.Options.EnableRolePlay == false && text.StartsWith("[RP]"))
            {
            }
            else
            {
                if (IO.Options.Timestamps)
                {
                    this.lblChat.AppendText("[" + DateTime.Now.ToLongTimeString() + "] " + text, renderOptions);
                }
                else
                {
                    this.lblChat.AppendText(text, renderOptions);
                }
            }

            if (this.lblChat.Text.Length > 10000)
            {
                GlyphRenderData[] newRenderOptions = new GlyphRenderData[10000];
                this.lblChat.CharRenderOptions.CopyTo(this.lblChat.CharRenderOptions.Count - 10000, newRenderOptions, 0, 10000);
                this.lblChat.SetText(newRenderOptions);
            }

            this.lblChat.ScrollToBottom();
        }

        public void AppendChat(string text, CharRenderOptions renderOptions)
        {
            if (IO.Options.EnableRolePlay == false && text.StartsWith("[RP]"))
            {
            }
            else
            {
                if (IO.Options.Timestamps)
                {
                    this.lblChat.AppendText("[" + DateTime.Now.ToLongTimeString() + "] " + text, renderOptions);
                }
                else
                {
                    this.lblChat.AppendText(text, renderOptions);
                }
            }

            this.lblChat.ScrollToBottom();
        }

        public void AppendChat(string text, Color color)
        {
            if (IO.Options.EnableRolePlay == false && text.StartsWith("[RP]"))
            {
            }
            else
            {
                if (IO.Options.Timestamps)
                {
                    this.AppendChat("[" + DateTime.Now.ToLongTimeString() + "] " + text, new CharRenderOptions(color));
                }
                else
                {
                    this.AppendChat(text + "\n", new CharRenderOptions(color));
                }
            }
        }

        private void TxtCommands_KeyUp(object sender, SdlDotNet.Input.KeyboardEventArgs e)
        {
            List<string> textCache = new List<string>();

            if (e.Key == SdlDotNet.Input.Key.Return)
            {
                if (IO.Options.EnableRolePlay == false && this.channelSelector.SelectedItem.TextIdentifier == "Roleplay")
                {
                    this.AppendChat("You have roleplay disabled! Check your settings in Esc>Others>Settings (It must be clicked with the mouse to toggle)", Color.Red);
                }
                else
                {
                    if (Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Moniter))
                    {
                        XmlDocument cacheXml = new XmlDocument();
                        cacheXml.Load(Path.Combine(System.Windows.Forms.Application.StartupPath, "PersonalChatCache.xml"));
                        XmlNode cacheText = cacheXml.DocumentElement["CacheText"];

                        cacheText.InnerText = cacheText.InnerText + "," + this.txtCommands.Text;
                        cacheXml.Save(Path.Combine(System.Windows.Forms.Application.StartupPath, "PersonalChatCache.xml"));
                    }

                    Logic.CommandProcessor.ProcessCommand(this.txtCommands.Text, (Enums.ChatChannel)Enum.Parse(typeof(Enums.ChatChannel), this.channelSelector.SelectedItem.TextIdentifier, true));
                    this.txtCommands.Text = string.Empty;
                    this.upArrowCounter = 0;
                }
            }

            if (((e.Mod & SdlDotNet.Input.ModifierKeys.LeftControl) != 0) && (e.Key == SdlDotNet.Input.Key.X))
            {
                try
                {
                    this.txtCommands.Text = this.txtCommands.Text.Substring(0, this.txtCommands.Text.Length - 1);
                    System.Windows.Forms.Clipboard.SetText(this.txtCommands.Text);
                    this.txtCommands.Text = string.Empty;
                }
                catch
                {
                }
            }

            if (((e.Mod & SdlDotNet.Input.ModifierKeys.LeftControl) != 0) && e.Key == SdlDotNet.Input.Key.C)
            {
                try
                {
                    this.txtCommands.Text = this.txtCommands.Text.Substring(0, this.txtCommands.Text.Length - 1);
                    System.Windows.Forms.Clipboard.SetText(this.txtCommands.Text);
                }
                catch
                {
                }
            }

            if (((e.Mod & SdlDotNet.Input.ModifierKeys.LeftControl) != 0) && e.Key == SdlDotNet.Input.Key.V)
            {
                try
                {
                    this.txtCommands.Text = this.txtCommands.Text.Substring(0, this.txtCommands.Text.Length - 1);
                    this.txtCommands.Text = this.txtCommands.Text + System.Windows.Forms.Clipboard.GetText();
                }
                catch
                {
                    this.AppendChat("The paste text must be only standard text (abc123,./!$% etc.)", Color.Red);
                }
            }

            if (e.Key == SdlDotNet.Input.Key.UpArrow)
            {
                XmlDocument cacheXml = new XmlDocument();
                cacheXml.Load(Path.Combine(System.Windows.Forms.Application.StartupPath, "PersonalChatCache.xml"));
                XmlNode cacheText = cacheXml.DocumentElement["CacheText"];

                this.cachedTextArray = cacheText.InnerText.Split(',');

                this.upArrowCounter++;

                try
                {
                    if (this.cachedTextArray.Length - this.upArrowCounter < 0)
                    {
                        this.upArrowCounter = 0;
                    }
                    else
                    {
                        this.txtCommands.Text = this.cachedTextArray[this.cachedTextArray.Length - this.upArrowCounter];
                    }
                }
                catch (Exception ex)
                {
                    if (Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Scriptor))
                    {
                        this.AppendChat("upArrowCounter Exception (Starting at line 195)", Color.Red);
                        this.AppendChat(ex.StackTrace, Color.Black);
                    }
                }
            }

            if (e.Key == SdlDotNet.Input.Key.DownArrow)
            {
                XmlDocument cacheXml = new XmlDocument();
                cacheXml.Load(Path.Combine(System.Windows.Forms.Application.StartupPath, "PersonalChatCache.xml"));
                XmlNode cacheText = cacheXml.DocumentElement["CacheText"];

                this.cachedTextArray = cacheText.InnerText.Split(',');

                this.upArrowCounter--;

                try
                {
                    if (this.cachedTextArray.Length - this.upArrowCounter > this.cachedTextArray.Length - 1 || this.cachedTextArray.Length - this.upArrowCounter < 0)
                    {
                        this.upArrowCounter = this.cachedTextArray.Length - 1;
                    }
                    else
                    {
                        this.txtCommands.Text = this.cachedTextArray[this.cachedTextArray.Length - this.upArrowCounter];
                    }
                }
                catch (Exception ex)
                {
                    if (Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Scriptor))
                    {
                        this.AppendChat("dwnArrowCounter Exception (Starting at line 218)", Color.Red);
                        this.AppendChat(ex.StackTrace, Color.Black);
                    }
                }
            }
        }

        public void SetAutoScroll(bool set)
        {
            this.lblChat.AutoScroll = set;
        }

        /// <inheritdoc/>
        public void Created(int index)
        {
            this.moduleIndex = index;
        }

        /// <inheritdoc/>
        public void SwitchOut()
        {
        }

        /// <inheritdoc/>
        public void Initialize(Size containerSize)
        {
            this.containerSize = containerSize;
            this.RecalculatePositions();
            this.RequestRedraw();
        }

        private void RecalculatePositions()
        {
            this.lblChat.Location = new Point(0, 0);
            this.lblChat.Size = new Size(this.containerSize.Width, this.containerSize.Height - 40 - 18);
            this.txtCommands.Location = new Point(0, this.lblChat.Y + this.lblChat.Height + 1);
            this.txtCommands.Size = new Size(this.containerSize.Width, 20);
            Skins.SkinManager.LoadTextBoxGui(this.txtCommands);

            this.lblChannel.Font = FontManager.LoadFont("PMU", 16);
            this.lblChannel.Size = new Size(75, 18);
            this.lblChannel.Location = new Point(0, this.txtCommands.Y + this.txtCommands.Height);

            this.channelSelector.Size = new Size(this.containerSize.Width - this.lblChannel.Width, 18);
            this.channelSelector.Location = new Point(this.lblChannel.Width, this.txtCommands.Y + this.txtCommands.Height);
            this.channelSelector.BorderColor = Color.Black;
            this.channelSelector.BorderWidth = 1;
            this.channelSelector.BorderStyle = BorderStyle.FixedSingle;

            int selectedIndex = this.channelSelector.SelectedIndex;
            this.channelSelector.Items.Clear();
            this.channelSelector.Items.Add(new ListBoxTextItem(FontManager.LoadFont("PMU", 16), "Local"));
            this.channelSelector.Items.Add(new ListBoxTextItem(FontManager.LoadFont("PMU", 16), "Global"));
            this.channelSelector.Items.Add(new ListBoxTextItem(FontManager.LoadFont("PMU", 16), "Guild"));
            this.channelSelector.Items.Add(new ListBoxTextItem(FontManager.LoadFont("PMU", 16), "Roleplay"));
            if (Ranks.IsAllowed(Players.PlayerManager.MyPlayer, Enums.Rank.Moniter))
            {
                this.channelSelector.Items.Add(new ListBoxTextItem(FontManager.LoadFont("PMU", 16), "Staff"));
            }

            if (selectedIndex < this.channelSelector.Items.Count && selectedIndex > -1)
            {
                this.channelSelector.SelectItem(selectedIndex);
            }
            else
            {
                this.channelSelector.SelectItem(0);
            }

            // lblCounter.Size = new Size(containerSize.Width - 10, 30);
            // btnIncrement.Location = new Point(DrawingSupport.GetCenter(containerSize, btnIncrement.Size).X - (btnIncrement.Width / 2), lblCounter.Y + lblCounter.Height + 5);
            // btnDecrement.Location = new Point(DrawingSupport.GetCenter(containerSize, btnDecrement.Size).X + (btnDecrement.Width / 2), lblCounter.Y + lblCounter.Height + 5);
        }

        /// <inheritdoc/>
        public int ModuleIndex
        {
            get { return this.moduleIndex; }
        }

        /// <inheritdoc/>
        public string ModuleName
        {
            get { return "Chat"; }
        }

        /// <inheritdoc/>
        public Panel ModulePanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Enabled
        {
            get { return this.enabled; }

            set
            {
                this.enabled = value;
                if (this.EnabledChanged != null)
                {
                    this.EnabledChanged(this, EventArgs.Empty);
                }
            }
        }

        /// <inheritdoc/>
        public event EventHandler EnabledChanged;

        /// <inheritdoc/>
        public Enums.ExpKitModules ModuleID
        {
            get { return Enums.ExpKitModules.Chat; }
        }
    }
}
