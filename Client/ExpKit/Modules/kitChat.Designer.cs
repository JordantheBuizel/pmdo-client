﻿namespace Client.Logic.ExpKit.Modules
{
    partial class KitChat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KitChat));
            this.pbHeader = new System.Windows.Forms.PictureBox();
            this.rtbChat = new System.Windows.Forms.RichTextBox();
            this.channelSelector = new System.Windows.Forms.ComboBox();
            this.lblChannel = new System.Windows.Forms.Label();
            this.txtCommands = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeader)).BeginInit();
            this.SuspendLayout();
            // 
            // pbHeader
            // 
            this.pbHeader.Location = new System.Drawing.Point(0, 0);
            this.pbHeader.Name = "pbHeader";
            this.pbHeader.Size = new System.Drawing.Size(200, 20);
            this.pbHeader.TabIndex = 0;
            this.pbHeader.TabStop = false;
            // 
            // rtbChat
            // 
            this.rtbChat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(69)))), ((int)(((byte)(79)))));
            this.rtbChat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbChat.Font = new System.Drawing.Font("Power Clear", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbChat.Location = new System.Drawing.Point(0, 19);
            this.rtbChat.Name = "rtbChat";
            this.rtbChat.ReadOnly = true;
            this.rtbChat.Size = new System.Drawing.Size(200, 358);
            this.rtbChat.TabIndex = 1;
            this.rtbChat.Text = "";
            // 
            // channelSelector
            // 
            this.channelSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.channelSelector.Font = new System.Drawing.Font("PMD", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channelSelector.Location = new System.Drawing.Point(79, 406);
            this.channelSelector.Name = "channelSelector";
            this.channelSelector.Size = new System.Drawing.Size(121, 23);
            this.channelSelector.TabIndex = 2;
            this.channelSelector.SelectedIndexChanged += new System.EventHandler(this.ChannelSelector_SelectedIndexChanged);
            // 
            // lblChannel
            // 
            this.lblChannel.AutoSize = true;
            this.lblChannel.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lblChannel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblChannel.Location = new System.Drawing.Point(12, 412);
            this.lblChannel.Name = "lblChannel";
            this.lblChannel.Size = new System.Drawing.Size(54, 14);
            this.lblChannel.TabIndex = 3;
            this.lblChannel.Text = "Channel:";
            this.lblChannel.Click += new System.EventHandler(this.Label1_Click);
            // 
            // txtCommands
            // 
            this.txtCommands.Location = new System.Drawing.Point(0, 383);
            this.txtCommands.Name = "txtCommands";
            this.txtCommands.Size = new System.Drawing.Size(200, 20);
            this.txtCommands.TabIndex = 4;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox1.Location = new System.Drawing.Point(46, 432);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox1.Size = new System.Drawing.Size(108, 18);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = ":Always on top";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // KitChat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(69)))), ((int)(((byte)(79)))));
            this.ClientSize = new System.Drawing.Size(200, 450);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.txtCommands);
            this.Controls.Add(this.lblChannel);
            this.Controls.Add(this.channelSelector);
            this.Controls.Add(this.rtbChat);
            this.Controls.Add(this.pbHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "KitChat";
            this.Text = "PMDO";
            ((System.ComponentModel.ISupportInitialize)(this.pbHeader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbHeader;
        private System.Windows.Forms.RichTextBox rtbChat;
        private System.Windows.Forms.ComboBox channelSelector;
        private System.Windows.Forms.Label lblChannel;
        private System.Windows.Forms.TextBox txtCommands;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}