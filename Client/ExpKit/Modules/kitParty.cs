﻿// <copyright file="kitParty.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.ExpKit.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Players.Parties;
    using PMU.Core;
    using SdlDotNet.Widgets;

    internal class KitParty : Panel, IKitModule
    {
        private int moduleIndex;
        private Size containerSize;
        private readonly PictureBox[] picMemberMugshot;
        private readonly Label[] lblMemberName;
        private readonly ProgressBar[] pgbMemberHP;
        private readonly ProgressBar[] pgbMemberExp;

        private bool enabled;

        private const int PARTYSIZE = 4;

        public KitParty(string name)
            : base(name)
            {
            this.enabled = true;

            this.BackColor = Color.Transparent;
            this.picMemberMugshot = new PictureBox[PARTYSIZE];
            for (int i = 0; i < PARTYSIZE; i++)
            {
                this.picMemberMugshot[i] = new PictureBox("picMemberMugshot" + i);
                this.picMemberMugshot[i].Size = new Size(40, 40);
                this.picMemberMugshot[i].BorderStyle = BorderStyle.FixedSingle;
                this.picMemberMugshot[i].BorderWidth = 1;
                this.picMemberMugshot[i].BorderColor = Color.Black;

                this.picMemberMugshot[i].X = 10;

                this.AddWidget(this.picMemberMugshot[i]);
            }

            this.lblMemberName = new Label[PARTYSIZE];
            for (int i = 0; i < PARTYSIZE; i++)
            {
                this.lblMemberName[i] = new Label("lblMemberName" + i);
                this.lblMemberName[i].AutoSize = true;
                this.lblMemberName[i].Font = Graphic.FontManager.LoadFont("PMU", 24);
                this.lblMemberName[i].ForeColor = Color.WhiteSmoke;

                this.AddWidget(this.lblMemberName[i]);
            }

            this.pgbMemberHP = new ProgressBar[PARTYSIZE];
            this.pgbMemberExp = new ProgressBar[PARTYSIZE];
            for (int i = 0; i < PARTYSIZE; i++)
            {
                this.pgbMemberHP[i] = new ProgressBar("pgbMemberHP" + i);
                this.pgbMemberExp[i] = new ProgressBar("pgbMemberExp" + i);

                this.pgbMemberHP[i].Maximum = 100;
                this.pgbMemberHP[i].TextStyle = ProgressBarTextStyle.Custom;

                this.pgbMemberExp[i].Maximum = 100;
                this.pgbMemberExp[i].TextStyle = ProgressBarTextStyle.Custom;

                this.AddWidget(this.pgbMemberHP[i]);
                this.AddWidget(this.pgbMemberExp[i]);
            }
        }

        /// <inheritdoc/>
        public void Created(int index)
        {
            this.moduleIndex = index;
        }

        /// <inheritdoc/>
        public void SwitchOut()
        {
        }

        /// <inheritdoc/>
        public void Initialize(Size containerSize)
        {
            this.containerSize = containerSize;
            this.RecalculatePositions();
            for (int i = 0; i < this.picMemberMugshot.Length; i++)
            {
                this.DisplayPartyMemberData(i);
            }

            this.RequestRedraw();
        }

        private void RecalculatePositions()
        {
            for (int i = 0; i < PARTYSIZE; i++)
            {
                this.picMemberMugshot[i].Visible = false;
                this.lblMemberName[i].Visible = false;
                this.pgbMemberHP[i].Visible = false;
                this.pgbMemberExp[i].Visible = false;

                this.picMemberMugshot[i].Y = (80 * i) + 30;
                this.lblMemberName[i].Location = new Point(5, 80 * i);
                this.pgbMemberHP[i].Location = new Point(this.picMemberMugshot[i].X + 45, (80 * i) + 32);
                this.pgbMemberExp[i].Location = new Point(this.picMemberMugshot[i].X + 45, (80 * i) + 52);

                this.pgbMemberHP[i].Size = new Size(this.containerSize.Width - this.picMemberMugshot[i].X - this.picMemberMugshot[i].Width - 10, 15);
                this.pgbMemberExp[i].Size = new Size(this.containerSize.Width - this.picMemberMugshot[i].X - this.picMemberMugshot[i].Width - 10, 15);
            }
        }

        public void DisplayPartyMemberData(int slot)
        {
            PartyData party = Players.PlayerManager.MyPlayer.Party;
            if (party != null)
            {
                PartyMember member = party.Members[slot];
                if (member != null)
                {
                    this.picMemberMugshot[slot].Image = Graphic.GraphicsManager.GetMugshot(member.MugshotNum, string.Empty, (int)member.MugshotShiny, (int)member.MugshotGender).GetEmote(0); // Tools.CropImage(Logic.Graphic.GraphicsManager.Speakers, new Rectangle(((member.Mugshot - 1) % 15) * 40, string.Empty, 40, 40));
                    this.lblMemberName[slot].Text = member.Name;

                    this.pgbMemberHP[slot].Value = MathFunctions.CalculatePercent(member.HP, member.MaxHP);
                    this.pgbMemberExp[slot].Value = (int)MathFunctions.CalculatePercent(member.Exp, member.MaxExp);

                    this.pgbMemberHP[slot].Text = "HP: " + member.HP + "/" + member.MaxHP;
                    this.pgbMemberExp[slot].Text = "Exp: " + this.pgbMemberExp[slot].Percent + "%";

                    if (member.HP < member.MaxHP / 5)
                    {
                        this.pgbMemberHP[slot].BarColor = Color.Red;
                    }
                    else if (member.HP < member.MaxHP / 2)
                    {
                        this.pgbMemberHP[slot].BarColor = Color.Yellow;
                    }
else
                    {
                        this.pgbMemberHP[slot].BarColor = Color.Green;
                    }

                    this.ChangeSlotVisibility(slot, true);
                }
else
                {
                    this.ChangeSlotVisibility(slot, false);
                }
            }
else
            {
                this.ChangeSlotVisibility(slot, false);
            }
        }

        public void ChangeSlotVisibility(int slot, bool visible)
        {
            this.picMemberMugshot[slot].Visible = visible;
            this.lblMemberName[slot].Visible = visible;
            this.pgbMemberHP[slot].Visible = visible;
            this.pgbMemberExp[slot].Visible = visible;
        }

        /// <inheritdoc/>
        public int ModuleIndex
        {
            get { return this.moduleIndex; }
        }

        /// <inheritdoc/>
        public string ModuleName
        {
            get { return "Party"; }
        }

        /// <inheritdoc/>
        public Panel ModulePanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Enabled
        {
            get { return this.enabled; }

            set
            {
                this.enabled = value;
                if (this.EnabledChanged != null)
                {
                    this.EnabledChanged(this, EventArgs.Empty);
                }
            }
        }

        /// <inheritdoc/>
        public event EventHandler EnabledChanged;

        /// <inheritdoc/>
        public Enums.ExpKitModules ModuleID
        {
            get { return Enums.ExpKitModules.Party; }
        }
    }
}
