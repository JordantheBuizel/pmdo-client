﻿// <copyright file="kitDebug.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.ExpKit.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class KitDebug : Panel, IKitModule
    {
        private int moduleIndex;
        private readonly Timer tmrUpdate;
        private readonly Label lblFps;
        private readonly Button btnTest;
        private bool enabled;

        public KitDebug(string name)
            : base(name)
            {
            this.enabled = true;

            this.BackColor = Color.Transparent;

            this.tmrUpdate = new Timer("tmrUpdate");
            this.tmrUpdate.Interval = 500;
            this.tmrUpdate.Elapsed += new EventHandler(this.TmrUpdate_Elapsed);

            this.lblFps = new Label("lblFps");
            this.lblFps.Location = new Point(0, 0);
            this.lblFps.Font = Graphic.FontManager.LoadFont("tahoma", 10);
            this.lblFps.Text = "FPS";
            this.lblFps.AutoSize = true;

            this.btnTest = new Button("btnTest");
            this.btnTest.Location = new Point(0, 40);
            this.btnTest.Size = new Size(40, 20);
            this.btnTest.Text = "Test!";
            Skins.SkinManager.LoadButtonGui(this.btnTest);
            this.btnTest.Click += new EventHandler<MouseButtonEventArgs>(this.BtnTest_Click);

            this.AddWidget(this.tmrUpdate);
            this.AddWidget(this.lblFps);
            this.AddWidget(this.btnTest);
        }

        private void BtnTest_Click(object sender, MouseButtonEventArgs e)
        {
            this.lblFps.Text = "Testing!";
        }

        private void TmrUpdate_Elapsed(object sender, EventArgs e)
        {
            this.lblFps.Text = "FPS: " + SdlDotNet.Core.Events.Fps.ToString();
        }

        /// <inheritdoc/>
        public void SwitchOut()
        {
            this.tmrUpdate.Stop();
        }

        /// <inheritdoc/>
        public void Initialize(Size containerSize)
        {
            this.tmrUpdate.Start();
        }

        /// <inheritdoc/>
        public int ModuleIndex
        {
            get { return this.moduleIndex; }
        }

        /// <inheritdoc/>
        public string ModuleName
        {
            get { return "Debug Module"; }
        }

        /// <inheritdoc/>
        public void Created(int index)
        {
            this.moduleIndex = index;
        }

        /// <inheritdoc/>
        public Panel ModulePanel
        {
            get { return this; }
        }

        /// <inheritdoc/>
        public bool Enabled
        {
            get { return this.enabled; }

            set
            {
                this.enabled = value;
                if (this.EnabledChanged != null)
                {
                    this.EnabledChanged(this, EventArgs.Empty);
                }
            }
        }

        /// <inheritdoc/>
        public event EventHandler EnabledChanged;

        /// <inheritdoc/>
        public Enums.ExpKitModules ModuleID
        {
            get { return Enums.ExpKitModules.Debug; }
        }
    }
}
