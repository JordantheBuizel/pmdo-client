﻿// <copyright file="ModuleSwitcher.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.ExpKit
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class ModuleSwitcher
    {
        private readonly List<IKitModule> allKitModules;
        private readonly List<IKitModule> availableKitModules;

        public List<IKitModule> AllKitModules
        {
            get
            {
                return this.allKitModules;
            }
        }

        public List<IKitModule> AvailableKitModules
        {
            get { return this.availableKitModules; }
        }

        public ModuleSwitcher()
        {
            this.allKitModules = new List<IKitModule>();
            this.availableKitModules = new List<IKitModule>();
            this.LoadKitModules();
            this.CreateAvailableModulesList();
        }

        public void LoadKitModules()
        {
            Modules.KitDebug kitDebug = new Modules.KitDebug("kitDebug");
            kitDebug.Created(this.allKitModules.Count);
            kitDebug.EnabledChanged += new EventHandler(this.Module_EnabledChanged);
            this.allKitModules.Add(kitDebug);

            Modules.KitChat_old kitChat = new Modules.KitChat_old("kitChat");
            kitChat.Created(this.allKitModules.Count);
            kitChat.EnabledChanged += new EventHandler(this.Module_EnabledChanged);
            this.allKitModules.Add(kitChat);

            Modules.KitCounter kitCounter = new Modules.KitCounter("kitCounter");
            kitCounter.Created(this.allKitModules.Count);
            kitCounter.EnabledChanged += new EventHandler(this.Module_EnabledChanged);
            this.allKitModules.Add(kitCounter);

            Modules.KitParty kitParty = new Modules.KitParty("kitParty");
            kitParty.Created(this.allKitModules.Count);
            kitParty.EnabledChanged += new EventHandler(this.Module_EnabledChanged);
            this.allKitModules.Add(kitParty);

            Modules.KitFriendsList kitFriendsList = new Modules.KitFriendsList("kitFriendsList");
            kitFriendsList.Created(this.allKitModules.Count);
            kitFriendsList.EnabledChanged += new EventHandler(this.Module_EnabledChanged);
            this.allKitModules.Add(kitFriendsList);

            Modules.KitMapReport kitMapReport = new Modules.KitMapReport("kitMapReport");
            kitMapReport.Created(this.allKitModules.Count);
            kitMapReport.EnabledChanged += new EventHandler(this.Module_EnabledChanged);
            this.allKitModules.Add(kitMapReport);
        }

        private void Module_EnabledChanged(object sender, EventArgs e)
        {
            this.CreateAvailableModulesList();
        }

        private void CreateAvailableModulesList()
        {
            this.availableKitModules.Clear();
            for (int i = 0; i < this.allKitModules.Count; i++)
            {
                if (this.allKitModules[i].Enabled)
                {
                    this.availableKitModules.Add(this.allKitModules[i]);
                }
                else
                {
                }
            }
        }

        public void DisableAllModules()
        {
            for (int i = 0; i < this.allKitModules.Count; i++)
            {
                this.allKitModules[i].EnabledChanged -= new EventHandler(this.Module_EnabledChanged);
                this.allKitModules[i].Enabled = false;
                this.allKitModules[i].EnabledChanged += new EventHandler(this.Module_EnabledChanged);
            }

            this.availableKitModules.Clear();
        }

        public IKitModule GetAvailableKitModule(int index)
        {
            if (index > this.availableKitModules.Count - 1)
            {
                return this.availableKitModules[this.availableKitModules.Count - 1];
            }
else
            {
                return this.availableKitModules[index];
            }
        }

        public IKitModule FindAvailableKitModule(Enums.ExpKitModules module)
        {
            for (int i = 0; i < this.availableKitModules.Count; i++)
            {
                if (this.availableKitModules[i].ModuleID == module)
                {
                    return this.availableKitModules[i];
                }
            }

            return null;
        }

        public bool IsModuleAvailable(Enums.ExpKitModules module)
        {
            for (int i = 0; i < this.availableKitModules.Count; i++)
            {
                if (this.availableKitModules[i].ModuleID == module)
                {
                    return true;
                }
            }

            return false;
        }

        public IKitModule FindKitModule(Enums.ExpKitModules module)
        {
            for (int i = 0; i < this.allKitModules.Count; i++)
            {
                if (this.allKitModules[i].ModuleID == module)
                {
                    return this.allKitModules[i];
                }
            }

            return null;
        }
    }
}
