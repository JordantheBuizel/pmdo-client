﻿// <copyright file="IKitModule.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.ExpKit
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal interface IKitModule
    {
        event EventHandler EnabledChanged;

        /// <summary>
        /// Gets the friendly name of the module
        /// </summary>
        string ModuleName { get; }

        Panel ModulePanel { get; }

        bool Enabled { get; set; }

        Enums.ExpKitModules ModuleID { get; }

        /// <summary>
        /// Gets the index # of the module
        /// </summary>
        int ModuleIndex { get; }

        void Created(int index);

        /// <summary>
        /// Called when the module is switched for another module
        /// </summary>
        void SwitchOut();

        /// <summary>
        /// Called when the module is set as the active module
        /// </summary>
        /// <param name="containerSize">The size of the container</param>
        void Initialize(Size containerSize);
    }
}
