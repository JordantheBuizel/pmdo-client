﻿// <copyright file="BorderedPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Widges
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using System.Windows.Forms;
    using Extensions;
    using Graphic;

    internal class BorderedPanel : Menus.Core.MenuBase
    {
        private const int VERTICALBORDERWIDTH = 5;
        private const int VERTICALBORDERHEIGHT = 11;

        private const int HORIZONTALBORDERWIDTH = 14;
        private const int HORIZONTALBORDERHEIGHT = 5;

        private readonly Panel widgetPanel;
        private readonly PictureBox border;

        public BorderedPanel()
        {
            this.BackColor = Color.Transparent;

            this.widgetPanel = new Panel();
            this.widgetPanel.BackColor = Color.Transparent;

            this.border = new PictureBox();
            this.border.Location = new Point(0, 0);
            this.border.BackColor = Color.Transparent;

            // this.AddWidget(border);
            this.Controls.Add(this.widgetPanel);
            this.Controls.Add(this.border);

            this.SizeChanged += new EventHandler(this.BorderedPanel_Resized);
            this.LocationChanged += new EventHandler(this.BorderedPanel_LocationChanged);
        }

        public Panel WidgetPanel
        {
            get { return this.widgetPanel; }
        }

        public new Enums.MenuDirection MenuDirection
        {
            get { return this.DetermineDirection(); }
            set { }
        }

        public new void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
        }

        public new void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
        }

        public new void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
        }

        public new void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
        }

        public new void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
        }

        private void BorderedPanel_LocationChanged(object sender, EventArgs e)
        {
            this.border.Location = this.Location;
        }

        private void BorderedPanel_Resized(object sender, EventArgs e)
        {
            switch (this.DetermineDirection())
            {
                case Enums.MenuDirection.Horizontal:
                    {
                        this.BackgroundImage = GraphicsCache.MenuHorizontalBorder.CreateStretchedSurface(this.Size).ToBitmap(true);
                        this.border.Size = this.Size;
                        Bitmap imageSurf = GraphicsCache.MenuHorizontalFill.CreateStretchedSurface(this.Size).ToBitmap(true);
                        this.border.Image = imageSurf;
                        this.widgetPanel.Size = new Size(this.Width - (HORIZONTALBORDERWIDTH * 2), this.Height - (HORIZONTALBORDERHEIGHT * 2));
                        this.widgetPanel.Location = new Point(HORIZONTALBORDERWIDTH, HORIZONTALBORDERHEIGHT);
                    }

                    break;
                case Enums.MenuDirection.Vertical:
                    {
                        this.BackgroundImage = GraphicsCache.MenuVerticalBorder.CreateStretchedSurface(this.Size).ToBitmap(true);
                        this.border.Size = this.Size;
                        Bitmap imageSurf = GraphicsCache.MenuVerticalFill.CreateStretchedSurface(this.Size).ToBitmap(true);
                        this.border.Image = imageSurf;
                        this.widgetPanel.Size = new Size(this.Width - (VERTICALBORDERWIDTH * 2), this.Height - (VERTICALBORDERHEIGHT * 2));
                        this.widgetPanel.Location = new Point(VERTICALBORDERWIDTH, VERTICALBORDERHEIGHT);
                    }

                    break;
            }
        }

        private Enums.MenuDirection DetermineDirection()
        {
            if (this.Width > this.Height)
            {
                return Enums.MenuDirection.Horizontal;
            }
            else
            {
                return Enums.MenuDirection.Vertical;
            }
        }
    }
}
