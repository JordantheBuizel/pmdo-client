﻿// <copyright file="ShortcutButton.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Widges
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using SdlDotNet.Widgets;

    internal class ShortcutButton : PictureBox
    {
        public ShortcutButton(string name)
            : base(name)
            {
            this.Size = new Size(50, 50);
        }
    }
}
