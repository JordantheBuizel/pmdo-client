﻿// <copyright file="MenuNumberSelector.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Widges
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Extensions;
    using SdlDotNet.Graphics;
    using SdlDotNet.Widgets;

    internal class MenuNumberSelector : Widget
    {
        private Label lblLeftArrow;
        private Label lblRightArrow;
        private Label lblSelectedNumber;

        private Font font;

        public Font Font
        {
            get
            {
                this.CheckFont();
                return this.font;
            }

            set
            {
                this.font = value;
            }
        }

        public MenuNumberSelector(string name)
            : base(name, true)
            {
            this.Paint += new EventHandler(this.MenuNumberSelector_Paint);
        }

        private void MenuNumberSelector_Paint(object sender, EventArgs e)
        {
        }

        private void CheckFont()
        {
            if (this.font == null)
            {
                this.font = new Font(Widgets.DefaultFontPath, Widgets.DefaultFontSize);
            }
        }
    }
}
