﻿// <copyright file="MenuItemPicker.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Widges
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class MenuItemPicker : Widget
    {
        private readonly short lineLength = 10;
        private int selectedItem;

        public int SelectedItem
        {
            get { return this.selectedItem; }
            set { this.selectedItem = value; }
        }

        public MenuItemPicker(string name)
            : base(name)
            {
            this.Size = new Size(30, 20);
            this.BackColor = Color.Transparent;

            this.Paint += new EventHandler(this.MenuItemPicker_Paint);
        }

        private void MenuItemPicker_Paint(object sender, EventArgs e)
        {
            this.Buffer.Draw(new SdlDotNet.Graphics.Primitives.Triangle(0, 0, 0, this.lineLength, this.lineLength, (short)(this.lineLength / 2)), Color.WhiteSmoke, false, true);
            this.Buffer.Draw(new SdlDotNet.Graphics.Primitives.Triangle(0, 0, 0, this.lineLength, this.lineLength, (short)(this.lineLength / 2)), Color.Black, false, false);
        }
    }
}
