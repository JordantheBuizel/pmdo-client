﻿// <copyright file="ShortcutBar.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Widges
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class ShortcutBar : Panel
    {
        private readonly List<ShortcutButton> buttons;

        public ShortcutBar(string name)
            : base(name)
            {
            this.Size = new Size(400, 60);
            this.BackColor = Color.Transparent;

            this.buttons = new List<ShortcutButton>();
        }

        public void AddButton(ShortcutButton button)
        {
            int totalWidth = 5; // Padding
            for (int i = 0; i < this.buttons.Count; i++)
            {
                totalWidth += this.buttons[i].Width + 5;
            }

            button.Location = new Point(this.Width - totalWidth - button.Width, 5);
            this.buttons.Add(button);
            this.AddWidget(button);
        }

        /// <inheritdoc/>
        public override void FreeResources()
        {
            base.FreeResources();
            this.buttons.Clear();
        }
    }
}
