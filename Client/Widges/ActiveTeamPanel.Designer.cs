﻿namespace Client.Logic.Widges
{
    partial class ActiveTeamPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHP0 = new System.Windows.Forms.Label();
            this.lblHP1 = new System.Windows.Forms.Label();
            this.lblHP2 = new System.Windows.Forms.Label();
            this.lblHP3 = new System.Windows.Forms.Label();
            this.lblExp0 = new System.Windows.Forms.Label();
            this.lblExp1 = new System.Windows.Forms.Label();
            this.lblExp2 = new System.Windows.Forms.Label();
            this.lblExp3 = new System.Windows.Forms.Label();
            this.pbStatus3 = new System.Windows.Forms.PictureBox();
            this.pbStatus2 = new System.Windows.Forms.PictureBox();
            this.pbStatus1 = new System.Windows.Forms.PictureBox();
            this.pbStatus0 = new System.Windows.Forms.PictureBox();
            this.pbHeldItem3 = new System.Windows.Forms.PictureBox();
            this.pbHeldItem2 = new System.Windows.Forms.PictureBox();
            this.pbHeldItem1 = new System.Windows.Forms.PictureBox();
            this.pbHeldItem0 = new System.Windows.Forms.PictureBox();
            this.pbRecruit3 = new System.Windows.Forms.PictureBox();
            this.pbRecruit2 = new System.Windows.Forms.PictureBox();
            this.pbRecruit1 = new System.Windows.Forms.PictureBox();
            this.pbRecruit0 = new System.Windows.Forms.PictureBox();
            this.pbarHP0 = new System.Windows.Forms.ProgressBar();
            this.pbarHP1 = new System.Windows.Forms.ProgressBar();
            this.pbarHP2 = new System.Windows.Forms.ProgressBar();
            this.pbarHP3 = new System.Windows.Forms.ProgressBar();
            this.pbarExp0 = new System.Windows.Forms.ProgressBar();
            this.pbarExp1 = new System.Windows.Forms.ProgressBar();
            this.pbarExp2 = new System.Windows.Forms.ProgressBar();
            this.pbarExp3 = new System.Windows.Forms.ProgressBar();
            this.lblLevel2 = new System.Windows.Forms.Label();
            this.lblLevel1 = new System.Windows.Forms.Label();
            this.lblLevel0 = new System.Windows.Forms.Label();
            this.lblLevel3 = new System.Windows.Forms.Label();
            this.pbSelectedRecruit = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeldItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeldItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeldItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeldItem0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRecruit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRecruit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRecruit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRecruit0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSelectedRecruit)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHP0
            // 
            this.lblHP0.AutoSize = true;
            this.lblHP0.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHP0.Location = new System.Drawing.Point(39, 97);
            this.lblHP0.Name = "lblHP0";
            this.lblHP0.Size = new System.Drawing.Size(28, 16);
            this.lblHP0.TabIndex = 3;
            this.lblHP0.Text = "HP:";
            // 
            // lblHP1
            // 
            this.lblHP1.AutoSize = true;
            this.lblHP1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHP1.Location = new System.Drawing.Point(249, 97);
            this.lblHP1.Name = "lblHP1";
            this.lblHP1.Size = new System.Drawing.Size(28, 16);
            this.lblHP1.TabIndex = 3;
            this.lblHP1.Text = "HP:";
            this.lblHP1.Visible = false;
            // 
            // lblHP2
            // 
            this.lblHP2.AutoSize = true;
            this.lblHP2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHP2.Location = new System.Drawing.Point(459, 97);
            this.lblHP2.Name = "lblHP2";
            this.lblHP2.Size = new System.Drawing.Size(28, 16);
            this.lblHP2.TabIndex = 3;
            this.lblHP2.Text = "HP:";
            this.lblHP2.Visible = false;
            // 
            // lblHP3
            // 
            this.lblHP3.AutoSize = true;
            this.lblHP3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHP3.Location = new System.Drawing.Point(679, 97);
            this.lblHP3.Name = "lblHP3";
            this.lblHP3.Size = new System.Drawing.Size(28, 16);
            this.lblHP3.TabIndex = 3;
            this.lblHP3.Text = "HP:";
            this.lblHP3.Visible = false;
            // 
            // lblExp0
            // 
            this.lblExp0.AutoSize = true;
            this.lblExp0.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExp0.Location = new System.Drawing.Point(39, 76);
            this.lblExp0.Name = "lblExp0";
            this.lblExp0.Size = new System.Drawing.Size(33, 16);
            this.lblExp0.TabIndex = 3;
            this.lblExp0.Text = "Exp:";
            // 
            // lblExp1
            // 
            this.lblExp1.AutoSize = true;
            this.lblExp1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExp1.Location = new System.Drawing.Point(249, 76);
            this.lblExp1.Name = "lblExp1";
            this.lblExp1.Size = new System.Drawing.Size(33, 16);
            this.lblExp1.TabIndex = 3;
            this.lblExp1.Text = "Exp:";
            this.lblExp1.Visible = false;
            // 
            // lblExp2
            // 
            this.lblExp2.AutoSize = true;
            this.lblExp2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExp2.Location = new System.Drawing.Point(459, 76);
            this.lblExp2.Name = "lblExp2";
            this.lblExp2.Size = new System.Drawing.Size(33, 16);
            this.lblExp2.TabIndex = 3;
            this.lblExp2.Text = "Exp:";
            this.lblExp2.Visible = false;
            // 
            // lblExp3
            // 
            this.lblExp3.AutoSize = true;
            this.lblExp3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExp3.Location = new System.Drawing.Point(679, 76);
            this.lblExp3.Name = "lblExp3";
            this.lblExp3.Size = new System.Drawing.Size(33, 16);
            this.lblExp3.TabIndex = 3;
            this.lblExp3.Text = "Exp:";
            this.lblExp3.Visible = false;
            // 
            // pbStatus3
            // 
            this.pbStatus3.Location = new System.Drawing.Point(713, 56);
            this.pbStatus3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbStatus3.Name = "pbStatus3";
            this.pbStatus3.Size = new System.Drawing.Size(32, 16);
            this.pbStatus3.TabIndex = 2;
            this.pbStatus3.TabStop = false;
            // 
            // pbStatus2
            // 
            this.pbStatus2.Location = new System.Drawing.Point(493, 56);
            this.pbStatus2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbStatus2.Name = "pbStatus2";
            this.pbStatus2.Size = new System.Drawing.Size(32, 16);
            this.pbStatus2.TabIndex = 2;
            this.pbStatus2.TabStop = false;
            // 
            // pbStatus1
            // 
            this.pbStatus1.Location = new System.Drawing.Point(283, 56);
            this.pbStatus1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbStatus1.Name = "pbStatus1";
            this.pbStatus1.Size = new System.Drawing.Size(32, 16);
            this.pbStatus1.TabIndex = 2;
            this.pbStatus1.TabStop = false;
            // 
            // pbStatus0
            // 
            this.pbStatus0.Location = new System.Drawing.Point(73, 56);
            this.pbStatus0.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbStatus0.Name = "pbStatus0";
            this.pbStatus0.Size = new System.Drawing.Size(32, 16);
            this.pbStatus0.TabIndex = 2;
            this.pbStatus0.TabStop = false;
            // 
            // pbHeldItem3
            // 
            this.pbHeldItem3.Location = new System.Drawing.Point(765, 40);
            this.pbHeldItem3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbHeldItem3.Name = "pbHeldItem3";
            this.pbHeldItem3.Size = new System.Drawing.Size(32, 32);
            this.pbHeldItem3.TabIndex = 1;
            this.pbHeldItem3.TabStop = false;
            // 
            // pbHeldItem2
            // 
            this.pbHeldItem2.Location = new System.Drawing.Point(544, 40);
            this.pbHeldItem2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbHeldItem2.Name = "pbHeldItem2";
            this.pbHeldItem2.Size = new System.Drawing.Size(32, 32);
            this.pbHeldItem2.TabIndex = 1;
            this.pbHeldItem2.TabStop = false;
            // 
            // pbHeldItem1
            // 
            this.pbHeldItem1.Location = new System.Drawing.Point(339, 40);
            this.pbHeldItem1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbHeldItem1.Name = "pbHeldItem1";
            this.pbHeldItem1.Size = new System.Drawing.Size(32, 32);
            this.pbHeldItem1.TabIndex = 1;
            this.pbHeldItem1.TabStop = false;
            // 
            // pbHeldItem0
            // 
            this.pbHeldItem0.Location = new System.Drawing.Point(130, 40);
            this.pbHeldItem0.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbHeldItem0.Name = "pbHeldItem0";
            this.pbHeldItem0.Size = new System.Drawing.Size(32, 32);
            this.pbHeldItem0.TabIndex = 1;
            this.pbHeldItem0.TabStop = false;
            // 
            // pbRecruit3
            // 
            this.pbRecruit3.Location = new System.Drawing.Point(722, 4);
            this.pbRecruit3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbRecruit3.Name = "pbRecruit3";
            this.pbRecruit3.Size = new System.Drawing.Size(56, 56);
            this.pbRecruit3.TabIndex = 0;
            this.pbRecruit3.TabStop = false;
            // 
            // pbRecruit2
            // 
            this.pbRecruit2.Location = new System.Drawing.Point(501, 4);
            this.pbRecruit2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbRecruit2.Name = "pbRecruit2";
            this.pbRecruit2.Size = new System.Drawing.Size(56, 56);
            this.pbRecruit2.TabIndex = 0;
            this.pbRecruit2.TabStop = false;
            // 
            // pbRecruit1
            // 
            this.pbRecruit1.Location = new System.Drawing.Point(296, 4);
            this.pbRecruit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbRecruit1.Name = "pbRecruit1";
            this.pbRecruit1.Size = new System.Drawing.Size(56, 56);
            this.pbRecruit1.TabIndex = 0;
            this.pbRecruit1.TabStop = false;
            // 
            // pbRecruit0
            // 
            this.pbRecruit0.Location = new System.Drawing.Point(87, 4);
            this.pbRecruit0.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbRecruit0.Name = "pbRecruit0";
            this.pbRecruit0.Size = new System.Drawing.Size(56, 56);
            this.pbRecruit0.TabIndex = 0;
            this.pbRecruit0.TabStop = false;
            // 
            // pbarHP0
            // 
            this.pbarHP0.Location = new System.Drawing.Point(73, 99);
            this.pbarHP0.Name = "pbarHP0";
            this.pbarHP0.Size = new System.Drawing.Size(100, 14);
            this.pbarHP0.TabIndex = 4;
            // 
            // pbarHP1
            // 
            this.pbarHP1.Location = new System.Drawing.Point(283, 99);
            this.pbarHP1.Name = "pbarHP1";
            this.pbarHP1.Size = new System.Drawing.Size(100, 14);
            this.pbarHP1.TabIndex = 4;
            // 
            // pbarHP2
            // 
            this.pbarHP2.Location = new System.Drawing.Point(493, 99);
            this.pbarHP2.Name = "pbarHP2";
            this.pbarHP2.Size = new System.Drawing.Size(100, 14);
            this.pbarHP2.TabIndex = 4;
            // 
            // pbarHP3
            // 
            this.pbarHP3.Location = new System.Drawing.Point(713, 100);
            this.pbarHP3.Name = "pbarHP3";
            this.pbarHP3.Size = new System.Drawing.Size(100, 14);
            this.pbarHP3.TabIndex = 4;
            // 
            // pbarExp0
            // 
            this.pbarExp0.Location = new System.Drawing.Point(73, 79);
            this.pbarExp0.Name = "pbarExp0";
            this.pbarExp0.Size = new System.Drawing.Size(100, 14);
            this.pbarExp0.TabIndex = 4;
            // 
            // pbarExp1
            // 
            this.pbarExp1.Location = new System.Drawing.Point(283, 79);
            this.pbarExp1.Name = "pbarExp1";
            this.pbarExp1.Size = new System.Drawing.Size(100, 14);
            this.pbarExp1.TabIndex = 4;
            // 
            // pbarExp2
            // 
            this.pbarExp2.Location = new System.Drawing.Point(493, 79);
            this.pbarExp2.Name = "pbarExp2";
            this.pbarExp2.Size = new System.Drawing.Size(100, 14);
            this.pbarExp2.TabIndex = 4;
            // 
            // pbarExp3
            // 
            this.pbarExp3.Location = new System.Drawing.Point(713, 79);
            this.pbarExp3.Name = "pbarExp3";
            this.pbarExp3.Size = new System.Drawing.Size(100, 14);
            this.pbarExp3.TabIndex = 4;
            // 
            // lblLevel2
            // 
            this.lblLevel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel2.Location = new System.Drawing.Point(415, 4);
            this.lblLevel2.Name = "lblLevel2";
            this.lblLevel2.Size = new System.Drawing.Size(37, 37);
            this.lblLevel2.TabIndex = 3;
            this.lblLevel2.Text = "Level";
            this.lblLevel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblLevel2.Visible = false;
            // 
            // lblLevel1
            // 
            this.lblLevel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel1.Location = new System.Drawing.Point(202, 4);
            this.lblLevel1.Name = "lblLevel1";
            this.lblLevel1.Size = new System.Drawing.Size(37, 37);
            this.lblLevel1.TabIndex = 3;
            this.lblLevel1.Text = "Level";
            this.lblLevel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblLevel1.Visible = false;
            // 
            // lblLevel0
            // 
            this.lblLevel0.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel0.Location = new System.Drawing.Point(3, 4);
            this.lblLevel0.Name = "lblLevel0";
            this.lblLevel0.Size = new System.Drawing.Size(37, 37);
            this.lblLevel0.TabIndex = 3;
            this.lblLevel0.Text = "Level";
            this.lblLevel0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblLevel0.Visible = false;
            // 
            // lblLevel3
            // 
            this.lblLevel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel3.Location = new System.Drawing.Point(626, 4);
            this.lblLevel3.Name = "lblLevel3";
            this.lblLevel3.Size = new System.Drawing.Size(37, 37);
            this.lblLevel3.TabIndex = 3;
            this.lblLevel3.Text = "Level";
            this.lblLevel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblLevel3.Visible = false;
            // 
            // pbSelectedRecruit
            // 
            this.pbSelectedRecruit.Location = new System.Drawing.Point(0, 0);
            this.pbSelectedRecruit.Name = "pbSelectedRecruit";
            this.pbSelectedRecruit.Size = new System.Drawing.Size(64, 64);
            this.pbSelectedRecruit.TabIndex = 5;
            this.pbSelectedRecruit.TabStop = false;
            // 
            // ActiveTeamPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pbarHP3);
            this.Controls.Add(this.pbarHP2);
            this.Controls.Add(this.pbarHP1);
            this.Controls.Add(this.pbarExp3);
            this.Controls.Add(this.pbarExp2);
            this.Controls.Add(this.pbarExp1);
            this.Controls.Add(this.pbarExp0);
            this.Controls.Add(this.pbarHP0);
            this.Controls.Add(this.lblHP3);
            this.Controls.Add(this.lblLevel3);
            this.Controls.Add(this.lblLevel0);
            this.Controls.Add(this.lblLevel1);
            this.Controls.Add(this.lblLevel2);
            this.Controls.Add(this.lblHP2);
            this.Controls.Add(this.lblHP1);
            this.Controls.Add(this.lblExp3);
            this.Controls.Add(this.lblExp2);
            this.Controls.Add(this.lblExp1);
            this.Controls.Add(this.lblExp0);
            this.Controls.Add(this.lblHP0);
            this.Controls.Add(this.pbStatus3);
            this.Controls.Add(this.pbStatus2);
            this.Controls.Add(this.pbStatus1);
            this.Controls.Add(this.pbStatus0);
            this.Controls.Add(this.pbHeldItem3);
            this.Controls.Add(this.pbHeldItem2);
            this.Controls.Add(this.pbHeldItem1);
            this.Controls.Add(this.pbHeldItem0);
            this.Controls.Add(this.pbRecruit3);
            this.Controls.Add(this.pbRecruit2);
            this.Controls.Add(this.pbRecruit1);
            this.Controls.Add(this.pbRecruit0);
            this.Controls.Add(this.pbSelectedRecruit);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ActiveTeamPanel";
            this.Size = new System.Drawing.Size(850, 123);
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeldItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeldItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeldItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeldItem0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRecruit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRecruit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRecruit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRecruit0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSelectedRecruit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbRecruit0;
        private System.Windows.Forms.PictureBox pbHeldItem0;
        private System.Windows.Forms.PictureBox pbRecruit1;
        private System.Windows.Forms.PictureBox pbHeldItem1;
        private System.Windows.Forms.PictureBox pbRecruit2;
        private System.Windows.Forms.PictureBox pbHeldItem2;
        private System.Windows.Forms.PictureBox pbRecruit3;
        private System.Windows.Forms.PictureBox pbHeldItem3;
        private System.Windows.Forms.PictureBox pbStatus0;
        private System.Windows.Forms.PictureBox pbStatus1;
        private System.Windows.Forms.PictureBox pbStatus2;
        private System.Windows.Forms.PictureBox pbStatus3;
        private System.Windows.Forms.Label lblHP0;
        private System.Windows.Forms.Label lblHP1;
        private System.Windows.Forms.Label lblHP2;
        private System.Windows.Forms.Label lblHP3;
        private System.Windows.Forms.Label lblExp0;
        private System.Windows.Forms.Label lblExp1;
        private System.Windows.Forms.Label lblExp2;
        private System.Windows.Forms.Label lblExp3;
        private System.Windows.Forms.ProgressBar pbarHP0;
        private System.Windows.Forms.ProgressBar pbarHP1;
        private System.Windows.Forms.ProgressBar pbarHP2;
        private System.Windows.Forms.ProgressBar pbarHP3;
        private System.Windows.Forms.ProgressBar pbarExp0;
        private System.Windows.Forms.ProgressBar pbarExp1;
        private System.Windows.Forms.ProgressBar pbarExp2;
        private System.Windows.Forms.ProgressBar pbarExp3;
        private System.Windows.Forms.Label lblLevel2;
        private System.Windows.Forms.Label lblLevel1;
        private System.Windows.Forms.Label lblLevel0;
        private System.Windows.Forms.Label lblLevel3;
        private System.Windows.Forms.PictureBox pbSelectedRecruit;
    }
}
