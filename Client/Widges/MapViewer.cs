﻿// <copyright file="MapViewerNew.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Widges
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Graphic.Renderers.Screen;
    using Interfaces;

    internal partial class MapViewer : UserControl, IViewLens
    {
        private int lastAnimTick;
        private Bitmap viewLens;
        private readonly Stopwatch watch;

        public MapViewer()
        {
            this.InitializeComponent();

            this.Resize += new EventHandler(this.MapViewer_Resized);
            this.Paint += new PaintEventHandler(this.MapViewer_Paint);

            Timer timer = new Timer();
            timer.Tick += new EventHandler(this.OnTick);
            this.watch = new Stopwatch();
            timer.Start();
            this.watch.Start();
        }

        public event EventHandler MapUpdated;

        public Maps.Map ActiveMap
        {
            get
            {
                return ScreenRenderer.RenderOptions.Map;
            }

            set
            {
                ScreenRenderer.RenderOptions.Map = value;
                this.MapUpdated?.Invoke(this, null);
                if (value != null)
                {
                    ScreenRenderer.Camera.FocusOnSprite(Players.PlayerManager.MyPlayer);

                    // Get the camera coordinates
                    ScreenRenderer.Camera.X = ScreenRenderer.GetScreenLeft() - 1;
                    ScreenRenderer.Camera.Y = ScreenRenderer.GetScreenTop();
                    ScreenRenderer.Camera.X2 = ScreenRenderer.GetScreenRight();
                    ScreenRenderer.Camera.Y2 = ScreenRenderer.GetScreenBottom() + 1;

                    // Verify that the coordinates aren't outside the map bounds
                    if (ScreenRenderer.Camera.X < 0 && ScreenRenderer.RenderOptions.Map.Left < 1)
                    {
                        ScreenRenderer.Camera.X = 0;
                        ScreenRenderer.Camera.X2 = 20;
                    }
                    else if (ScreenRenderer.Camera.X2 > ScreenRenderer.RenderOptions.Map.MaxX + 1 && ScreenRenderer.RenderOptions.Map.Right < 1)
                    {
                        ScreenRenderer.Camera.X = ScreenRenderer.RenderOptions.Map.MaxX - 19;
                        ScreenRenderer.Camera.X2 = ScreenRenderer.RenderOptions.Map.MaxX + 1;
                    }

                    if (ScreenRenderer.Camera.Y < 0 && ScreenRenderer.RenderOptions.Map.Up < 1)
                    {
                        ScreenRenderer.Camera.Y = 0;
                        ScreenRenderer.Camera.Y2 = 15;
                    }
                    else if (ScreenRenderer.Camera.Y2 > ScreenRenderer.RenderOptions.Map.MaxY + 1 && ScreenRenderer.RenderOptions.Map.Down < 1)
                    {
                        ScreenRenderer.Camera.Y = ScreenRenderer.RenderOptions.Map.MaxY - 14;
                        ScreenRenderer.Camera.Y2 = ScreenRenderer.RenderOptions.Map.MaxY + 1;
                    }
                }
            }
        }

        public Bitmap ViewLens
        {
            get
            {
                return this.viewLens;
            }

            set
            {
                this.viewLens = value;
                this.Invalidate();
                this.Update();
            }
        }

        public void OnTick(object sender, EventArgs e)
        {
            if (this.watch.ElapsedMilliseconds > this.lastAnimTick + 250)
            {
                ScreenRenderer.RenderOptions.DisplayAnimation = !ScreenRenderer.RenderOptions.DisplayAnimation;
                this.lastAnimTick = (int)this.watch.ElapsedMilliseconds;
            }

            if (Globals.Tick > ((Music.Bass.BassAudioPlayer)Music.Music.AudioPlayer).TimeOfNextSong
                && ((Music.Bass.BassAudioPlayer)Music.Music.AudioPlayer).TimeOfNextSong > 0)
            {
                ((Music.Bass.BassAudioPlayer)Music.Music.AudioPlayer).PlayNextMusic();
            }

            this.Invalidate();
        }

        private void MapViewer_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(this.viewLens, new Point(0, 0));

            try
            {
                if (ScreenRenderer.RenderOptions.Map != null && ScreenRenderer.RenderOptions.Map.Loaded)
                {
                    // Graphic.Renderers.Screen.ScreenRenderer.RenderScreen(base.Buffer, this.Location);
                    // gl.DrawScreen(base.Buffer, activeMap, mapAnim, this.cameraX, this.cameraX2, this.cameraY, this.cameraY2, displayAttributes, displayMapGrid, displayLocation, overlay);
                    ScreenRenderer.RenderScreen(this.destData);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Rendering:");
                Debug.WriteLine(ex.ToString());
                Debug.WriteLine(ex.StackTrace);
            }
        }

        private void MapViewer_Resized(object sender, EventArgs e)
        {
            this.RecreateRendererDestinationData();
        }

        private Graphic.Renderers.RendererDestinationData destData;

        private void RecreateRendererDestinationData()
        {
            this.destData = new Graphic.Renderers.RendererDestinationData(this);
        }

        public Bitmap CaptureMapImage(bool captureVisibleArea, bool captureAttributes, bool captureMapGrid)
        {
            int cameraX;
            int cameraX2;
            int cameraY;
            int cameraY2;
            if (captureVisibleArea)
            {
                cameraX = ScreenRenderer.Camera.X;
                cameraX2 = ScreenRenderer.Camera.X2;
                cameraY = ScreenRenderer.Camera.Y;
                cameraY2 = ScreenRenderer.Camera.Y2;
            }
            else
            {
                cameraX = 0;
                cameraX2 = ScreenRenderer.RenderOptions.Map.MaxX;
                cameraY = 0;
                cameraY2 = ScreenRenderer.RenderOptions.Map.MaxY;
            }

            Bitmap screenshotSurf = new Bitmap((cameraX2 - cameraX) * Constants.TILEWIDTH, (cameraY2 - cameraY) * Constants.TILEHEIGHT);
            using (Graphics g = Graphics.FromImage(screenshotSurf))
            {
                g.Clear(Color.White);
            }

            Graphic.Renderers.Maps.MapRenderer.DrawTiles(new Graphic.Renderers.RendererDestinationData(screenshotSurf), ScreenRenderer.RenderOptions.Map, ScreenRenderer.RenderOptions.DisplayAnimation, cameraX, cameraX2, cameraY, cameraY2, captureAttributes, captureMapGrid);
            return screenshotSurf;
        }
    }
}
