﻿namespace Client.Logic.Widges
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class StatLabel : UserControl
    {
        private string currentAtk;
        private string currentDef;
        private string currentSpd;
        private string currentSpclAtk;
        private string currentSpclDef;

        public StatLabel()
        {
            this.InitializeComponent();
        }

        public void SetStats(string atk, string def, string spd, string spclAtk, string spclDef)
        {
            this.currentAtk = atk;
            this.currentDef = def;
            this.currentSpd = spd;
            this.currentSpclAtk = spclAtk;
            this.currentSpclDef = spclDef;
            this.lblStats.Text = "Atk: " + atk + " Def: " + def + " Spd: " + spd + " Sp. Atk: " + spclAtk + " Sp. Def: " + spclDef;

            // SetAtk(atk);
            // SetDef(def);
            // SetSpd(spd);
            // SetSpclAtk(spclAtk);
            // SetSpclDef(spclDef);
        }

        public void SetAtk(string atk)
        {
            // lblAtk.Text = "Atk: " + atk;
            // lblAtk.Location = new Point(5, 5);
            this.SetStats(atk, this.currentDef, this.currentSpd, this.currentSpclAtk, this.currentSpclDef);
        }

        public void SetDef(string def)
        {
            // lblDef.Text = "Def: " + def;
            // lblDef.Location = new Point(lblAtk.X + lblAtk.Width + 5, 5);
            this.SetStats(this.currentAtk, def, this.currentSpd, this.currentSpclAtk, this.currentSpclDef);
        }

        public void SetSpd(string spd)
        {
            // lblSpd.Text = "Spd: " + spd;
            // lblSpd.Location = new Point(lblDef.X + lblDef.Width + 5, 5);
            this.SetStats(this.currentAtk, this.currentDef, spd, this.currentSpclAtk, this.currentSpclDef);
        }

        public void SetSpclAtk(string spclAtk)
        {
            // lblSpclAtk.Text = "Sp. Atk: " + spclAtk;
            // lblSpclAtk.Location = new Point(lblSpd.X + lblSpd.Width + 5, 5);
            this.SetStats(this.currentAtk, this.currentDef, this.currentSpd, spclAtk, this.currentSpclDef);
        }

        public void SetSpclDef(string spclDef)
        {
            // lblSpclDef.Text = "Sp. Def: " + spclDef;
            // lblSpclDef.Location = new Point(lblSpclAtk.X + lblSpclAtk.Width + 5, 5);
            this.SetStats(this.currentAtk, this.currentDef, this.currentSpd, this.currentSpclAtk, spclDef);
        }
    }
}
