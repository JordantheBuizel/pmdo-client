﻿// <copyright file="TilesetViewer.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Widges
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class TilesetViewer : ContainerWidget
    {
        private Graphic.Tileset activeTilesetSurf;
        private readonly HScrollBar hScroll;
        private Point selectedTile;
        private Point endTile;
        private readonly VScrollBar vScroll;

        public new bool Visible
        {
            get
            {
                return base.Visible;
            }

            set
            {
                if (value == false)
                {
                    if (File.Exists(Path.Combine(IO.Paths.StartupPath + "TileCache.xml")))
                    {
                        File.Delete(Path.Combine(IO.Paths.StartupPath + "TileCache.xml"));
                    }
                }

                base.Visible = value;
            }
        }

        public TilesetViewer(string name)
            : base(name)
            {
            this.BackColor = Color.White;

            this.vScroll = new VScrollBar("vScroll");
            this.vScroll.Visible = false;
            this.vScroll.BackColor = Color.Transparent;
            this.vScroll.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.VScroll_ValueChanged);

            this.hScroll = new HScrollBar("hScroll");
            this.hScroll.Visible = false;
            this.hScroll.BackColor = Color.Transparent;
            this.hScroll.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.HScroll_ValueChanged);

            this.Click += new EventHandler<MouseButtonEventArgs>(this.TilesetViewer_Click);
            this.MouseEnter += new EventHandler(this.TilesetViewer_MouseEnter);
            this.MouseLeave += new EventHandler(this.TilesetViewer_MouseLeave);
            this.MouseDown += new EventHandler<MouseButtonEventArgs>(this.MouseMotion_Move);

            this.selectedTile = new Point(0, 0);

            this.AddWidget(this.vScroll);
            this.AddWidget(this.hScroll);

            this.Paint += new EventHandler(this.TilesetViewer_Paint);
        }

        private void MouseMotion_Move(object sender, MouseButtonEventArgs e)
        {
            switch (e.MouseEventArgs.Button)
            {
                case SdlDotNet.Input.MouseButton.WheelDown:
                    {
                        this.vScroll.Value += 10;
                    }

                    break;
                case SdlDotNet.Input.MouseButton.WheelUp:
                    {
                        this.vScroll.Value -= 10;
                    }

                    break;
            }
        }

        private void TilesetViewer_MouseLeave(object sender, EventArgs e)
        {
            this.Width = this.originalWidth;
            this.RecalculateScrollBars();
            this.RequestRedraw();
            if ((this.selectedTile.X + 1) * 32 > this.Width)
            {
                int value = this.selectedTile.X - 1;
                this.hScroll.Value = value;
            }
        }

        private int originalWidth;

        private void TilesetViewer_MouseEnter(object sender, EventArgs e)
        {
            this.originalWidth = this.Width;
            this.Size = new Size(this.ActiveTilesetSurface.Size.Width + 32, this.Height);

            // this.Width = this.ActiveTilesetSurface.Size.Width + 32;
            // RecalculateScrollBars();
            // RequestRedraw();
        }

        public Graphic.Tileset ActiveTilesetSurface
        {
            get { return this.activeTilesetSurf; }

            set
            {
                this.activeTilesetSurf = value;
                this.RecalculateScrollBars();
                this.RequestRedraw();
            }
        }

        public Point SelectedTile
        {
            get { return this.selectedTile; }

            set
            {
                this.selectedTile = value;
                this.RequestRedraw();
            }
        }

        public Point EndTile
        {
            get { return this.endTile; }

            set
            {
                this.endTile = value;
                this.RequestRedraw();
            }
        }

        private new Size Size
        {
            get { return base.Size; }

            set
            {
                base.Size = value;
                this.RecalculateScrollBars();
                this.RequestRedraw();
            }
        }

        /// <inheritdoc/>
        public override void FreeResources()
        {
            base.FreeResources();
        }

        /// <inheritdoc/>
        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
        }

        /// <inheritdoc/>
        public override void OnMouseMotion(SdlDotNet.Input.MouseMotionEventArgs e)
        {
            base.OnMouseMotion(e);
        }

        /// <inheritdoc/>
        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
        }

        /// <inheritdoc/>
        public override void OnTick(SdlDotNet.Core.TickEventArgs e)
        {
            base.OnTick(e);
            if (this.vScroll != null)
            {
                this.vScroll.OnTick(e);
            }

            if (this.hScroll != null)
            {
                this.hScroll.OnTick(e);
            }
        }

        private void TilesetViewer_Paint(object sender, EventArgs e)
        {
            if (this.vScroll != null && this.hScroll != null)
            {
                this.vScroll.BlitToScreen(this.Buffer);
                this.hScroll.BlitToScreen(this.Buffer);
                this.DrawTiles();
                SdlDotNet.Graphics.Primitives.Box box = new SdlDotNet.Graphics.Primitives.Box(new Point((this.selectedTile.X - this.hScroll.Value) * Constants.TILEWIDTH, (this.selectedTile.Y - this.vScroll.Value) * Constants.TILEHEIGHT), new Size(Constants.TILEWIDTH * (1 + this.endTile.X - this.selectedTile.X), Constants.TILEHEIGHT * (1 + this.endTile.Y - this.selectedTile.Y)));
                this.Buffer.Draw(box, Color.Red);
                this.DrawBorder();
                if (this.ParentContainer != null)
                {
                    this.ParentContainer.RequestRedraw();
                }
            }
        }

        private int DetermineTileNumber(int x, int y)
        {
            return (y * (this.activeTilesetSurf.Size.Width / Constants.TILEWIDTH)) + x;
        }

        private void DrawTiles()
        {
            if (this.vScroll != null && this.hScroll != null & this.activeTilesetSurf != null)
            {
                int maxTilesX = Math.Min(this.activeTilesetSurf.Size.Width, this.Width - this.vScroll.Width) / Constants.TILEWIDTH;
                int maxTilesY = Math.Min(this.activeTilesetSurf.Size.Height, this.Height - this.hScroll.Height) / Constants.TILEHEIGHT;
                int startX = this.hScroll.Value;
                int startY = this.vScroll.Value;
                for (int y = startY; y < maxTilesY + startY; y++)
                {
                    for (int x = startX; x < maxTilesX + startX; x++)
                    {
                        int num = this.DetermineTileNumber(x, y);
                        if (num < this.activeTilesetSurf.TileCount)
                        {
                            this.Buffer.Blit(this.activeTilesetSurf[num], new Point((x - startX) * Constants.TILEWIDTH, (y - startY) * Constants.TILEHEIGHT));
                        }
                    }
                }
            }
        }

        private void RecalculateScrollBars()
        {
            this.vScroll.Size = new Size(12, this.Height - this.vScroll.ButtonHeight);
            this.vScroll.Location = new Point(this.Width - this.vScroll.Width, 0);
            this.hScroll.Size = new Size(this.Width - this.hScroll.ButtonWidth, 12);
            this.hScroll.Location = new Point(0, this.Height - this.hScroll.Height);
            if (this.vScroll != null)
            {
                if (this.activeTilesetSurf.Size.Height > this.Height - this.hScroll.Height)
                {
                    this.vScroll.Visible = true;
                    this.vScroll.Minimum = 0;
                }
else
                {
                    this.vScroll.Visible = false;
                }

                this.vScroll.Maximum = Math.Max(this.vScroll.Minimum, (this.activeTilesetSurf.Size.Height / Constants.TILEHEIGHT) - (this.Height / Constants.TILEHEIGHT));
            }

            if (this.hScroll != null)
            {
                if (this.activeTilesetSurf.Size.Width > this.Width - this.vScroll.Width)
                {
                    this.hScroll.Visible = true;
                    this.hScroll.Minimum = 0;
                }
else
                {
                    this.hScroll.Visible = false;
                }

                this.hScroll.Maximum = Math.Max(this.hScroll.Minimum, (this.activeTilesetSurf.Size.Width / Constants.TILEWIDTH) - (this.Width / Constants.TILEWIDTH));
            }
        }

        private void TilesetViewer_Click(object sender, MouseButtonEventArgs e)
        {
            // Point location = this.ScreenLocation;
            // Point relPoint = new Point(e.Position.X - location.X, e.Position.Y - location.Y);
            if (e.MouseEventArgs.Button == SdlDotNet.Input.MouseButton.PrimaryButton)
            {
                if (!DrawingSupport.PointInBounds(e.RelativePosition, this.vScroll.Bounds) && !DrawingSupport.PointInBounds(e.RelativePosition, this.hScroll.Bounds))
                {
                    if (e.RelativePosition.X + (this.hScroll.Value * Constants.TILEWIDTH) > this.activeTilesetSurf.Size.Width)
                    {
                        this.selectedTile.X = (this.activeTilesetSurf.Size.Width / 32) - 1;
                        this.endTile.X = this.selectedTile.X;
                    }
                    else
                    {
                        this.selectedTile.X = (e.RelativePosition.X / 32) + this.hScroll.Value;
                        this.endTile.X = this.selectedTile.X;
                    }

                    if (e.RelativePosition.Y + (this.vScroll.Value * Constants.TILEHEIGHT) > this.activeTilesetSurf.Size.Height)
                    {
                        this.selectedTile.Y = (this.activeTilesetSurf.Size.Height / 32) - 1;
                        this.endTile.Y = this.selectedTile.Y;
                    }
                    else
                    {
                        this.selectedTile.Y = (e.RelativePosition.Y / 32) + this.vScroll.Value;
                        this.endTile.Y = this.selectedTile.Y;
                    }
                }

                this.RequestRedraw();
            }
            else if (e.MouseEventArgs.Button == SdlDotNet.Input.MouseButton.SecondaryButton)
            {
                if (!DrawingSupport.PointInBounds(e.RelativePosition, this.vScroll.Bounds) && !DrawingSupport.PointInBounds(e.RelativePosition, this.hScroll.Bounds))
                {
                    if (e.RelativePosition.X + (this.hScroll.Value * Constants.TILEWIDTH) > this.activeTilesetSurf.Size.Width)
                    {
                        this.endTile.X = (this.activeTilesetSurf.Size.Width / 32) - 1;
                    }
                    else
                    {
                        this.endTile.X = (e.RelativePosition.X / 32) + this.hScroll.Value;
                    }

                    if (e.RelativePosition.Y + (this.vScroll.Value * Constants.TILEHEIGHT) > this.activeTilesetSurf.Size.Height)
                    {
                        this.endTile.Y = (this.activeTilesetSurf.Size.Height / 32) - 1;
                    }
                    else
                    {
                        this.endTile.Y = (e.RelativePosition.Y / 32) + this.vScroll.Value;
                    }

                    if (this.DetermineTileNumber(this.endTile.X, this.endTile.Y) < this.DetermineTileNumber(this.selectedTile.X, this.selectedTile.Y))
                    {
                        this.endTile = this.selectedTile;
                    }
                }

                this.RequestRedraw();
            }
        }

        private void HScroll_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.RequestRedraw();
        }

        private void VScroll_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.RequestRedraw();
        }
    }
}
