﻿// <copyright file="ItemsetViewer.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Widges
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using SdlDotNet.Widgets;

    internal class ItemsetViewer : ContainerWidget
    {
        private SdlDotNet.Graphics.Surface activeItemSurf;
        private readonly HScrollBar hScroll;
        private Point selectedTile;
        private readonly VScrollBar vScroll;

        public ItemsetViewer(string name)
            : base(name)
        {
            this.BackColor = Color.Black;

            this.vScroll = new VScrollBar("vScroll");
            this.vScroll.Visible = false;
            this.vScroll.BackColor = Color.Transparent;
            this.vScroll.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.VScroll_ValueChanged);

            this.hScroll = new HScrollBar("hScroll");
            this.hScroll.Visible = false;
            this.hScroll.BackColor = Color.Transparent;
            this.hScroll.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.HScroll_ValueChanged);

            this.Click += new EventHandler<MouseButtonEventArgs>(this.ItemsetViewer_Click);

            this.selectedTile = new Point(0, 0);

            this.AddWidget(this.vScroll);
            this.AddWidget(this.hScroll);

            this.Paint += new EventHandler(this.ItemsetViewer_Paint);
        }

        public SdlDotNet.Graphics.Surface ActiveItemSurface
        {
            get { return this.activeItemSurf; }

            set
            {
                this.activeItemSurf = value;
                this.RecalculateScrollBars();
            }
        }

        public Point SelectedTile
        {
            get { return this.selectedTile; }

            set
            {
                this.selectedTile = value;
                this.RequestRedraw();
            }
        }

        private new Size Size
        {
            get { return base.Size; }

            set
            {
                base.Size = value;
                this.RecalculateScrollBars();
            }
        }

        /// <inheritdoc/>
        public override void FreeResources()
        {
            base.FreeResources();
        }

        /// <inheritdoc/>
        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
        }

        /// <inheritdoc/>
        public override void OnMouseMotion(SdlDotNet.Input.MouseMotionEventArgs e)
        {
            base.OnMouseMotion(e);
        }

        /// <inheritdoc/>
        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
        }

        /// <inheritdoc/>
        public override void OnTick(SdlDotNet.Core.TickEventArgs e)
        {
            base.OnTick(e);
            if (this.vScroll != null)
            {
                this.vScroll.OnTick(e);
            }

            if (this.hScroll != null)
            {
                this.hScroll.OnTick(e);
            }
        }

        private void ItemsetViewer_Paint(object sender, EventArgs e)
        {
            if (this.vScroll != null && this.hScroll != null)
            {
                this.vScroll.BlitToScreen(this.Buffer);
                this.hScroll.BlitToScreen(this.Buffer);
                this.DrawTiles();
                SdlDotNet.Graphics.Primitives.Box box = new SdlDotNet.Graphics.Primitives.Box(new Point((this.selectedTile.X - this.hScroll.Value) * Constants.TILEWIDTH, (this.selectedTile.Y - this.vScroll.Value) * Constants.TILEHEIGHT), new Size(Constants.TILEWIDTH, Constants.TILEHEIGHT));
                this.Buffer.Draw(box, Color.Red);
                this.DrawBorder();
            }
        }

        public int DetermineTileNumber(int x, int y)
        {
            return (y * (this.activeItemSurf.Size.Width / Constants.TILEWIDTH)) + x;
        }

        private void DrawTiles()
        {
            if (this.vScroll != null && this.hScroll != null & this.activeItemSurf != null)
            {
                int maxTilesX = Math.Min(this.activeItemSurf.Size.Width, this.Width - this.vScroll.Width) / Constants.TILEWIDTH;
                int maxTilesY = Math.Min(this.activeItemSurf.Size.Height, this.Height - this.hScroll.Height) / Constants.TILEHEIGHT;
                int startX = this.hScroll.Value;
                int startY = this.vScroll.Value;

                for (int y = startY; y < maxTilesY + startY; y++)
                {
                    for (int x = startX; x < maxTilesX + startX; x++)
                    {
                        // int num = DetermineTileNumber(x, y);
                        // if (x < (activeItemSurf.Size.Width / Constants.TILE_WIDTH) && y < (activeItemSurf.Size.Height / Constants.TILE_HEIGHT))
                        // {
                        SdlDotNet.Graphics.Surface tile = new SdlDotNet.Graphics.Surface(Constants.TILEWIDTH, Constants.TILEHEIGHT);
                        tile.Blit(this.activeItemSurf, new Point(0, 0), new Rectangle(x * Constants.TILEWIDTH, y * Constants.TILEHEIGHT, Constants.TILEWIDTH, Constants.TILEHEIGHT));
                        this.Buffer.Blit(tile, new Point((x - startX) * Constants.TILEWIDTH, (y - startY) * Constants.TILEHEIGHT));

                        // }
                    }
                }
            }
        }

        private void RecalculateScrollBars()
        {
            this.vScroll.Size = new Size(12, this.Height - this.vScroll.ButtonHeight);
            this.vScroll.Location = new Point(this.Width - this.vScroll.Width, 0);
            this.hScroll.Size = new Size(this.Width - this.hScroll.ButtonWidth, 12);
            this.hScroll.Location = new Point(0, this.Height - this.hScroll.Height);
            if (this.vScroll != null)
            {
                if (this.activeItemSurf.Size.Height > this.Height - this.hScroll.Height)
                {
                    this.vScroll.Visible = true;
                    this.vScroll.Minimum = 0;
                }
                else
                {
                    this.vScroll.Visible = false;
                }

                this.vScroll.Maximum = Math.Max(this.vScroll.Minimum, (this.activeItemSurf.Size.Height / Constants.TILEHEIGHT) - (this.Height / Constants.TILEHEIGHT));
            }

            if (this.hScroll != null)
            {
                if (this.activeItemSurf.Size.Width > this.Width - this.vScroll.Width)
                {
                    this.hScroll.Visible = true;
                    this.hScroll.Minimum = 0;
                }
                else
                {
                    this.hScroll.Visible = false;
                }

                this.hScroll.Maximum = Math.Max(this.hScroll.Minimum, (this.activeItemSurf.Size.Width / Constants.TILEWIDTH) - (this.Width / Constants.TILEWIDTH));
            }

            this.RequestRedraw();
        }

        private void ItemsetViewer_Click(object sender, MouseButtonEventArgs e)
        {
            Point location = this.Location;
            Point relPoint = new Point(e.Position.X - location.X, e.Position.Y - location.Y);
            if (!DrawingSupport.PointInBounds(relPoint, this.vScroll.Bounds) && !DrawingSupport.PointInBounds(relPoint, this.hScroll.Bounds))
            {
                if (relPoint.X + (this.hScroll.Value * Constants.TILEWIDTH) > this.activeItemSurf.Size.Width)
                {
                    this.selectedTile.X = (this.activeItemSurf.Size.Width / 32) - 1;
                }
                else
                {
                    this.selectedTile.X = (relPoint.X / 32) + this.hScroll.Value;
                }

                if (relPoint.Y + (this.vScroll.Value * Constants.TILEHEIGHT) > this.activeItemSurf.Size.Height)
                {
                    this.selectedTile.Y = (this.activeItemSurf.Size.Height / 32) - 1;
                }
                else
                {
                    this.selectedTile.Y = (relPoint.Y / 32) + this.vScroll.Value;
                }
            }

            this.RequestRedraw();
        }

        private void HScroll_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.RequestRedraw();
        }

        private void VScroll_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.RequestRedraw();
        }
    }
}
