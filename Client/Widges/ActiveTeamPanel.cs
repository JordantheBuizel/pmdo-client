﻿// <copyright file="ActiveTeamPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Widges
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Extensions;
    using Network;
    using Players;

    internal partial class ActiveTeamPanel : UserControl
    {
        private int selectedSlot;

        public ActiveTeamPanel()
        {
            this.InitializeComponent();

            foreach (PictureBox pbox in this.Controls.OfType<PictureBox>())
            {
                if (pbox.Width == 56)
                {
                    pbox.Click += new EventHandler(this.PbRecruit_Click);
                }

                if (pbox.Height == 32)
                {
                    pbox.Click += new EventHandler(this.PbHeldItem_Click);
                }
            }

            foreach (ColoredProgressBar pbar in this.Controls.OfType<ColoredProgressBar>())
            {
                if (pbar.Name.Contains("Exp"))
                {
                    pbar.ForeColor = Color.CornflowerBlue;
                }
            }
        }

        public event EventHandler<Events.ActiveRecruitChangedEventArgs> ActiveRecruitChanged;

        public void DisplayRecruitData(int teamSlot)
        {
            Recruit recruit = PlayerManager.MyPlayer.Team[teamSlot];
            if (recruit.Loaded)
            {
                this.ShowRecruitSlot(teamSlot);

                (this.GetChildFromName("pbRecruit" + teamSlot.ToString()) as PictureBox).Image = Graphic.GraphicsManager.GetMugshot(recruit.Num, string.Empty, (int)recruit.Shiny, (int)recruit.Sex).GetEmoteBitmap(0); // Tools.CropImage(Logic.Graphic.GraphicsManager.Speakers, new Rectangle((recruit.Mugshot % 15) * 40, string.Empty, 40, 40));
                this.DisplayRecruitStatusAilment(teamSlot);
                this.DisplayRecruitHeldItem(teamSlot);

                string text = "Level " + recruit.Level.ToString() + "              ";

                if (recruit.Sex == Enums.Sex.Male)
                {
                    text += "♂";
                }
                else if (recruit.Sex == Enums.Sex.Female)
                {
                    text += "♀";
                }

                this.GetChildFromName("lblLevel" + teamSlot.ToString()).Text = text;

                this.DisplayRecruitHP(teamSlot);
                this.DisplayRecruitExp(teamSlot);
            }
            else
            {
                this.HideRecruitSlot(teamSlot);
            }
        }

        public void HideRecruitSlot(int teamSlot)
        {
            this.GetChildFromName("pbRecruit" + teamSlot.ToString()).Visible = false;
            this.GetChildFromName("pbStatus" + teamSlot.ToString()).Visible = false;
            this.GetChildFromName("pbHeldItem" + teamSlot.ToString()).Visible = false;
            this.GetChildFromName("lblLevel" + teamSlot.ToString()).Visible = false;
            this.GetChildFromName("lblHP" + teamSlot.ToString()).Visible = false;
            this.GetChildFromName("lblExp" + teamSlot.ToString()).Visible = false;
            this.GetChildFromName("pbarExp" + teamSlot.ToString()).Visible = false;
            this.GetChildFromName("pbarHP" + teamSlot.ToString()).Visible = false;
        }

        public void ShowRecruitSlot(int slot)
        {
            this.GetChildFromName("pbRecruit" + slot.ToString()).Visible = true;

            // this.GetChildFromName("pbStatus" + teamSlot.ToString()).Visible = true;
            // this.GetChildFromName("pbHeldItem" + teamSlot.ToString()).Visible = true;
            this.GetChildFromName("lblLevel" + slot.ToString()).Visible = true;
            this.GetChildFromName("lblHP" + slot.ToString()).Visible = true;
            this.GetChildFromName("lblExp" + slot.ToString()).Visible = true;
            this.GetChildFromName("pbarExp" + slot.ToString()).Visible = true;
            this.GetChildFromName("pbarHP" + slot.ToString()).Visible = true;
        }

        public void DisplayRecruitHP(int teamSlot)
        {
            ColoredProgressBar pbar = this.GetChildFromName("pbarHP" + teamSlot.ToString()) as ColoredProgressBar;

            pbar.Value = PlayerManager.MyPlayer.Team[teamSlot].HP * 100 / PlayerManager.MyPlayer.Team[teamSlot].MaxHP;
            pbar.Text = PlayerManager.MyPlayer.Team[teamSlot].HP + "/" + PlayerManager.MyPlayer.Team[teamSlot].MaxHP;
            if (pbar.Value < pbar.Maximum / 5)
            {
                pbar.ForeColor = Color.Red;
            }
            else if (pbar.Value < pbar.Maximum / 2)
            {
                pbar.ForeColor = Color.Yellow;
            }
            else
            {
                pbar.ForeColor = Color.Green;
            }
        }

        public void DisplayRecruitExp(int teamSlot)
        {
            ColoredProgressBar pbar = this.GetChildFromName("pbarExp" + teamSlot.ToString()) as ColoredProgressBar;

            pbar.Value = PlayerManager.MyPlayer.Team[teamSlot].ExpPercent;
            pbar.Text = (pbar.Value / pbar.Maximum) + "%";

            // if (teamSlot == PlayerHelper.Players.GetMyPlayer().ActiveTeamNum) {
            //    Windows.WindowSwitcher.GameWindow.lblExp.Text = GetMyPlayerRecruit(teamSlot).Exp.ToString() + "/" + GetMyPlayerRecruit(teamSlot).MaxExp.ToString();
            // }
        }

        public void DisplayRecruitLevel(int teamSlot)
        {
            this.GetChildFromName("lblLevel" + teamSlot.ToString()).Text = "Level " + PlayerManager.MyPlayer.Team[teamSlot].Level.ToString();

            // lblLevel.Text = Players.PlayerHelper.Players.GetMyPlayerRecruit(slot).Level.ToString();
        }

        public void DisplayRecruitStatusAilment(int teamSlot)
        {
            if (PlayerManager.MyPlayer.Team[teamSlot].StatusAilment != Enums.StatusAilment.OK)
            {
                this.GetChildFromName("pbStatus" + teamSlot.ToString()).Visible = true;
                (this.GetChildFromName("pbStatus" + teamSlot.ToString()) as PictureBox).Image = Tools.CropImage(Skins.SkinManager.LoadGuiElement("Game Window", "TeamPanel\\StatusAilments.png").ToBitmap(true), new Rectangle(0, ((int)PlayerManager.MyPlayer.Team[teamSlot].StatusAilment - 1) * 16, 32, 16));

                // picStatus[teamSlot].Image = surf;
            }
            else
            {
                this.GetChildFromName("pbStatus" + teamSlot.ToString()).Visible = false;
            }
        }

        public void DisplayRecruitHeldItem(int teamSlot)
        {
            int heldItemSlot = PlayerManager.MyPlayer.Team[teamSlot].HeldItemSlot;
            if (heldItemSlot > 0)
            {
                (this.GetChildFromName("pbHeldItem" + teamSlot) as PictureBox).Image = Tools.CropImage(Graphic.GraphicsManager.Items.ToBitmap(true), new Rectangle((Items.ItemHelper.Items[PlayerManager.MyPlayer.GetInvItemNum(heldItemSlot)].Pic - ((int)(Items.ItemHelper.Items[PlayerManager.MyPlayer.GetInvItemNum(heldItemSlot)].Pic / 6) * 6)) * Constants.TILEWIDTH, (int)(Items.ItemHelper.Items[PlayerManager.MyPlayer.GetInvItemNum(heldItemSlot)].Pic / 6) * Constants.TILEWIDTH, Constants.TILEWIDTH, Constants.TILEHEIGHT));
                (this.GetChildFromName("pbHeldItem" + teamSlot) as PictureBox).Visible = true;
            }
            else
            {
                (this.GetChildFromName("pbHeldItem" + teamSlot) as PictureBox).Visible = false;
            }
        }

        public void SetSelected(int slot)
        {
            if (!this.pbSelectedRecruit.Visible)
            {
                this.pbSelectedRecruit.Visible = true;
            }

            if (this.selectedSlot != slot)
            {
                this.pbSelectedRecruit.Location = new Point(this.GetChildFromName("pbRecruit" + slot).Location.X - 8, this.GetChildFromName("pbRecruit" + slot).Location.Y - 8);
                this.selectedSlot = slot;
            }
        }

        private void PbRecruit_Click(object sender, EventArgs e)
        {
            this.ActiveRecruitChanged?.Invoke(this, new Events.ActiveRecruitChangedEventArgs(Convert.ToInt32((sender as PictureBox).Name.ToCharArray()[(sender as PictureBox).Name.ToCharArray().Length - 1])));
        }

        private void PbHeldItem_Click(object sender, EventArgs e)
        {
            int itemNum = PlayerManager.MyPlayer.GetInvItemNum(PlayerManager.MyPlayer.Team[Convert.ToInt32((sender as PictureBox).Name.ToCharArray()[(sender as PictureBox).Name.ToCharArray().Length - 1])].HeldItemSlot);
            if (itemNum > 0)
            {
                if (!((int)Items.ItemHelper.Items[itemNum].Type < 8 || (int)Items.ItemHelper.Items[itemNum].Type == 15))
                {
                    Messenger.SendUseItem(PlayerManager.MyPlayer.Team[Convert.ToInt32((sender as PictureBox).Name.ToCharArray()[(sender as PictureBox).Name.ToCharArray().Length - 1])].HeldItemSlot);
                }
            }
        }

        private Control GetChildFromName(string name)
        {
            foreach (Control control in this.Controls)
            {
                if (control.Name == name)
                {
                    return control;
                }
            }

            return null;
        }
    }
}
