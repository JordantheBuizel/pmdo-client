﻿// <copyright file="BattleLog.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

using System;
using System.Drawing;
using Client.Logic.ExpKit.Modules;
using Client.Logic.Extensions;
using SdlDotNet.Widgets;
using Timer = System.Windows.Forms.Timer;

namespace Client.Logic.Widges
{
    internal class BattleLog : BattleLogPanel
    {
        private readonly TranslucentRichTextBox rtbLog;
        private readonly TranslucentPanel pnlRtbParent;
        private Timer tmrHide;

        public Timer TmrHide
        {
            get
            {
                return this.tmrHide;
            }

            set
            {
                this.tmrHide = value;
            }
        }

        public BattleLog()
        {
            this.pnlRtbParent = new TranslucentPanel
            {
                Size = this.Size,
                Location = new Point(0, 0)
            };

            this.rtbLog = new TranslucentRichTextBox
            {
                BackColor = Color.Transparent,
                Size = new Size(this.Size.Width - 10, this.Size.Height),
                Font = new Font("Tahoma", 9.75F, FontStyle.Regular, GraphicsUnit.Point, 0),
                Location = new Point(5, -4)
            };

            this.SizeChanged += this.BattleLog_Resized;

            this.TmrHide = new Timer();
            this.TmrHide.Interval = 5000;
            this.TmrHide.Tick += this.TmrHide_Elapsed;
            this.TmrHide.Start();

            this.WidgetPanel.Controls.Add(this.pnlRtbParent);
            this.pnlRtbParent.Controls.Add(this.rtbLog);
        }

        private void TmrHide_Elapsed(object sender, EventArgs e)
        {
            this.Visible = false;
            this.TmrHide.Stop();
        }

        private void BattleLog_Resized(object sender, EventArgs e)
        {
            this.rtbLog.Size = new Size(this.Size.Width, this.Size.Height);
        }

        public void AddLog(string message, Color color)
        {
            Logs.BattleLog.AddLog(message, color);
            string[] messageArray = Logs.BattleLog.Messages.ToArray();
            Color[] colorArray = Logs.BattleLog.MessageColor.ToArray();

            this.rtbLog.Text = string.Empty;
            for (int i = Math.Max(messageArray.Length - Logs.BattleLog.MaxShownMessages, 0); i < messageArray.Length; i++)
            {
                this.rtbLog.AppendText(messageArray[i], new CharRenderOptions(colorArray[i]).ForeColor);
                this.rtbLog.AppendText("\n");
            }
        }
    }
}
