﻿// <copyright file="BattleLogPanel.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Widges
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using System.Windows.Forms;
    using Extensions;
    using Graphic;

    internal class BattleLogPanel : Menus.Core.MenuBase
    {
        private const int VERTICALBORDERWIDTH = 5;
        private const int VERTICALBORDERHEIGHT = 11;

        private const int HORIZONTALBORDERWIDTH = 14;
        private const int HORIZONTALBORDERHEIGHT = 5;

        private readonly Panel widgetPanel;
        private readonly PictureBox border;

        private byte backgroundAlpha;

        public Panel WidgetPanel
        {
            get { return this.widgetPanel; }
        }

        public BattleLogPanel()
        {
            this.BackColor = Color.Transparent;

            this.backgroundAlpha = 150;

            this.widgetPanel = new Panel();
            this.widgetPanel.BackColor = Color.Transparent;

            this.border = new PictureBox();
            this.border.Location = new Point(0, 0);
            this.border.BackColor = Color.Transparent;

            // this.AddWidget(border);
            this.Controls.Add(this.widgetPanel);

            this.SizeChanged += new EventHandler(this.BorderedPanel_Resized);
            this.LocationChanged += new EventHandler(this.BorderedPanel_LocationChanged);
        }

        private void BorderedPanel_LocationChanged(object sender, EventArgs e)
        {
            this.border.Location = this.Location;
        }

        private void BorderedPanel_Resized(object sender, EventArgs e)
        {
            switch (this.DetermineDirection())
            {
                case Enums.MenuDirection.Horizontal:
                    {
                        this.border.Size = this.Size;
                        Bitmap imageSurf = GraphicsCache.MenuHorizontalFill.ToBitmap(true);
                        this.border.Image = imageSurf;
                        this.widgetPanel.Size = new Size(this.Width - (HORIZONTALBORDERWIDTH * 2), this.Height - (HORIZONTALBORDERHEIGHT * 2));
                        this.widgetPanel.Location = new Point(HORIZONTALBORDERWIDTH, HORIZONTALBORDERHEIGHT);
                    }

                    break;
                case Enums.MenuDirection.Vertical:
                    {
                        this.border.Size = this.Size;
                        Bitmap imageSurf = GraphicsCache.MenuVerticalFill.ToBitmap(true);
                        this.border.Image = imageSurf;
                        this.widgetPanel.Size = new Size(this.Width - (VERTICALBORDERWIDTH * 2), this.Height - (VERTICALBORDERHEIGHT * 2));
                        this.widgetPanel.Location = new Point(VERTICALBORDERWIDTH, VERTICALBORDERHEIGHT);
                    }

                    break;
            }
        }

        public new Enums.MenuDirection MenuDirection
        {
            get { return this.DetermineDirection(); }
            set { }
        }

        private Enums.MenuDirection DetermineDirection()
        {
            if (this.Width > this.Height)
            {
                return Enums.MenuDirection.Horizontal;
            }
            else
            {
                return Enums.MenuDirection.Vertical;
            }
        }
    }
}