﻿// <copyright file="MissionTitle.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Widges
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using Graphic;
    using Missions;
    using SdlDotNet.Widgets;

    internal class MissionTitle : Panel
    {
        private readonly Label lblJobName;
        private readonly Label lblDifficulty;
        private readonly Label lblGoal;

        public MissionTitle(string name, int containerWidth)
            : base(name)
            {
            this.BackColor = Color.Transparent;

            this.Size = new Size(containerWidth - 20, 40);

            this.lblJobName = new Label("lblJobName");
            this.lblJobName.Font = FontManager.LoadFont("PMU", 16);
            this.lblJobName.Location = new Point(15, 0);
            this.lblJobName.AutoSize = true;
            this.lblJobName.ForeColor = Color.WhiteSmoke;

            this.lblDifficulty = new Label("lblDifficulty");
            this.lblDifficulty.Location = new Point(containerWidth - 40, 5);
            this.lblDifficulty.Font = FontManager.LoadFont("PMU", 16);
            this.lblDifficulty.AutoSize = true;
            this.lblDifficulty.ForeColor = Color.WhiteSmoke;

            this.lblGoal = new Label("lblGoal");
            this.lblGoal.Font = FontManager.LoadFont("PMU", 16);
            this.lblGoal.Location = new Point(this.lblJobName.X, this.lblJobName.Y + 16);
            this.lblGoal.AutoSize = true;
            this.lblGoal.ForeColor = Color.WhiteSmoke;

            this.AddWidget(this.lblJobName);
            this.AddWidget(this.lblDifficulty);
            this.AddWidget(this.lblGoal);
        }

        public void SetJob(Job job)
        {
            if (job != null)
            {
                if (job.Accepted == Enums.JobStatus.Taken)
                {
                    this.lblJobName.ForeColor = Color.Yellow;
                    this.lblGoal.ForeColor = Color.Yellow;
                    this.lblDifficulty.ForeColor = Color.Yellow;
                }
                else if (job.Accepted == Enums.JobStatus.Failed)
                {
                    this.lblJobName.ForeColor = Color.Red;
                    this.lblGoal.ForeColor = Color.Red;
                    this.lblDifficulty.ForeColor = Color.Red;
                }
                else if (job.Accepted == Enums.JobStatus.Finished)
                {
                    this.lblJobName.ForeColor = Color.LightGreen;
                    this.lblGoal.ForeColor = Color.LightGreen;
                    this.lblDifficulty.ForeColor = Color.LightGreen;
                }
else
                {
                    this.lblJobName.ForeColor = Color.WhiteSmoke;
                    this.lblGoal.ForeColor = Color.WhiteSmoke;
                    this.lblDifficulty.ForeColor = Color.WhiteSmoke;
                }

                this.lblJobName.Text = job.Title;
                this.lblDifficulty.Text = MissionManager.DifficultyToString(job.Difficulty);
                this.lblDifficulty.Location = new Point(this.Width - this.lblDifficulty.Width - 40, 0);
                this.lblGoal.Text = job.GoalName;
            }
else
            {
                this.lblJobName.ForeColor = Color.Gray;
                this.lblJobName.Text = "----------";
                this.lblDifficulty.Text = string.Empty;
                this.lblDifficulty.Location = new Point(this.Width - this.lblDifficulty.Width - 40, 0);
                this.lblGoal.Text = string.Empty;
            }
        }
    }
}
