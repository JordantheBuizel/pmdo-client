﻿// <copyright file="Loader.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

using Client.Logic.Localization;

namespace Client.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using Graphic;
    using Network;
    using Windows;
    using Microsoft.Win32;
    using PMU.Core;
    using Microsoft.VisualBasic;
    using System.Windows.Forms;

    /// <summary>
    /// Loader that will load the game data.
    /// </summary>
    public class Loader
    {

        public static bool BackgroundLoaded
        {
            get
            {
                return backgroundLoaded;
            }
        }

        private static bool backgroundLoaded = false;

        /// <summary>
        /// Checks the folders to see if they exist.
        /// </summary>
        public static void CheckFolders()
        {
            IO.IO.CheckFolders();
        }

        /// <summary>
        /// Initializes the loader.
        /// </summary>
        /// <param name="args">Command-line args</param>
        [STAThread]
        public static void InitLoader(string[] args)
        {
            Globals.CommandLine = PMU.Core.CommandProcessor.ParseCommand(System.Environment.CommandLine);
            Globals.GameLoaded = false;

            Globals.GameScreen = new Windows.Core.GameScreen();
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            IO.IO.Init();
            if (IsRunningUnderMaintenanceMode(Globals.CommandLine))
            {
                RunMaintenanceMode(Globals.CommandLine);
            }
else
            {
                RunGame();
            }
        }

        private static void RunGame()
        {
#if !DEBUG
            // Naming a Mutex makes it available computer-wide. Use a name that's
            // unique to your company and application (e.g., include your URL).
            using (Mutex mutex = new Mutex(false, "Pokemon Mystery Dungeon Online (http://pmdonlinemmo.boards.net/)"))
            {
                // Wait a few seconds if contended, in case another instance
                // of the program is still in the process of shutting down.
                if (!mutex.WaitOne(TimeSpan.FromSeconds(3), false))
                {
                    Exceptions.ExceptionHandler.OnException(new Exception("Another instance of PMDO is already running!"));
                    return;
                }

                InitializeGame();
            }
#else
            InitializeGame();
#endif
        }

        private static void InitializeGame()
        {
            // Create filetype associations
            CheckFileAssociations();

            // Move settings to Documents folder
            try
            {
                if (!File.Exists(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), "PMDO", "settings.xml")))
                {
                    if (File.Exists(Path.Combine(IO.Paths.StartupPath, "settings.xml")))
                    {
                        File.Copy(Path.Combine(IO.Paths.StartupPath, "settings.xml"), Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), "PMDO", "settings.xml"));
                        File.Delete(Path.Combine(IO.Paths.StartupPath, "settings.xml"));
                        while (File.Exists(Path.Combine(IO.Paths.StartupPath, "settings.xml")))
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }

            // Create and run the loading thread
            Thread loadThread = new Thread(new ThreadStart(LoadData))
            {
                IsBackground = true,
                Name = "PMDO Load Thread",
                Priority = ThreadPriority.Normal
            };
            loadThread.Start();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new WinSplashScreen());

            // Sdl.SdlLoader.InitializeSdl();
        }

        private static void CheckFileAssociations()
        {
            /*RegistryKey key = Registry.ClassesRoot.OpenSubKey(".pmuskn");
            if (key == null) {
                // The association doesn't exist, restart as admin in maintenance mode
                if (!Client.Logic.Security.Admin.IsAdmin()) {
                    if (Client.Logic.Security.Admin.IsVistaOrHigher()) {
                        Process process = Client.Logic.Security.Admin.StartProcessElevated(PMU.Core.Environment.StartupPath, "-createfileassociations");
                        process.WaitForExit();
                    }
                } else {
                    Process process = Process.Start(PMU.Core.Environment.StartupPath, "-createfileassociations");
                    process.WaitForExit();
                }
            }*/
        }

        private static void RunMaintenanceMode(Command command)
        {
            if (command.ContainsCommandArg("-installskin"))
            {
                // We are trying to install a new skin
                string skinPackagePath = command["-installskin"];
                if (!string.IsNullOrEmpty(skinPackagePath) && File.Exists(skinPackagePath))
                {
                    bool installed = Skins.SkinManager.InstallSkin(skinPackagePath);
                    if (installed)
                    {
                        MessageBox.Show("The skin has been installed!", "Installation completed!");
                    }
else
                    {
                        MessageBox.Show("The selected file is not a valid skin package.", "Invalid Package");
                    }
                }
            }
            else if (command.ContainsCommandArg("-createfileassociations"))
            {
                // Create associations for the skin loader
                RegistryKey regKey = Registry.ClassesRoot.CreateSubKey(".pmuskn");
                regKey.SetValue(string.Empty, "PMU.Skin.Loader");
                regKey.Close();

                regKey = Registry.ClassesRoot.CreateSubKey("PMU.Skin.Loader");
                regKey.SetValue(string.Empty, "Pokémon Mystery Universe Skin Package");
                regKey.Close();

                regKey = Registry.ClassesRoot.CreateSubKey("PMU.Skin.Loader" + "\\DefaultIcon");
                regKey.SetValue(string.Empty, @"C:\Program Files\Pokémon Mystery Dungeon Online\Client\PMDO.ico" + "," + "0");
                regKey.Close();

                regKey = Registry.ClassesRoot.CreateSubKey("PMU.Skin.Loader" + "\\" + "Shell" + "\\" + "Open");
                regKey = regKey.CreateSubKey("Command");
                regKey.SetValue(string.Empty, "\"" + PMU.Core.Environment.StartupPath + "\" -installskin \"%1\"");
                regKey.Close();
            }
        }

        private static bool IsRunningUnderMaintenanceMode(Command command)
        {
            if (command.ContainsCommandArg("-installskin"))
            {
                return true;
            }
            else if (command.ContainsCommandArg("-createfileassociations"))
            {
                return true;
            }

            return false;
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exceptions.ErrorBox.ShowDialog("Error", ((Exception)e.ExceptionObject).Message, ((Exception)e.ExceptionObject).ToString());
        }

        // static void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e) {
        //    //System.Windows.Forms.MessageBox.Show(e.Exception.ToString());
        // }

        /// <summary>
        /// Loads the game data.
        /// </summary>
        public static void LoadData()
        {
            // Load the main font
            FontManager.InitFonts();

            IO.Options.Initialize();
            IO.Options.LoadOptions();
            IO.ControlLoader.LoadControls();

            // Load Portraits
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://pmdonline.servegame.com/Sprites/Portraits/Portraits.dat");
            request.Method = WebRequestMethods.Ftp.GetFileSize;
            request.UseBinary = true;
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            long size = response.ContentLength;
            response.Close();

            FileInfo info = new FileInfo(IO.Paths.GfxPath + "Mugshots/Portraits.dat");
            if (info.Length != size)
            {
                WebClient client = new WebClient();
                using (MemoryStream ms = new MemoryStream(client.DownloadData("ftp://pmdonline.servegame.com/Sprites/Portraits/Portraits.dat")))
                {
                    File.WriteAllBytes(IO.Paths.GfxPath + "Mugshots/Portraits.dat", ms.ToArray());
                }
            }

            // Load the initial skin
            Skins.SkinManager.ChangeActiveSkin(IO.Options.ActiveSkin);

            backgroundLoaded = true;

            if (Globals.InDebugMode)
            {
                // Init the debug controls
                Globals.GameScreen.InitControls();
            }

            /*SdlDotNet.Widgets.Widgets.Initialize(
                SdlDotNet.Graphics.Video.Screen,
                SdlDotNet.Widgets.Widgets.ResourceDirectory,
                IO.Paths.FontPath + "tahoma.ttf",
                12);*/

            // SdlDotNet.Widgets.Settings.DefaultFont = ;
            // SdlDotNet.Widgets.WindowManager.Initialize(SdlDotNet.Graphics.Video.Screen);
            // SdlDotNet.Widgets.WindowManager.WindowSwitcherEnabled = false;
            GraphicsCache.LoadCache();
            Input.InputProcessor.Initialize();

            // Switch to the loading window
            // SdlDotNet.Widgets.WindowManager.AddWindow(new WinLoading());
            // ((WinLoading)Windows.WindowSwitcher.FindWindow("winLoading")).UpdateLoadText("Loading...");
            PostLoad();
        }

        private static void PostLoad()
        {
            Music.Music.Initialize();
            Skins.SkinManager.PlaySkinMusic();

            // WinLoading winLoading = Windows.WindowSwitcher.FindWindow("winLoading") as WinLoading;
            // winLoading.UpdateLoadText("Loading game...");
            CheckFolders();
            LoadGraphics();

            // TODO: Add encryption key here
            Globals.Encryption = new Security.Encryption();

            // winLoading.UpdateLoadText("Connecting to server...");
            // Load TCP and connect to the server
            NetworkManager.InitializeTcp();
            NetworkManager.InitializePacketSecurity();
            NetworkManager.Connect();

            Globals.GameLoaded = true;
        }

        private static void DeleteToDeleteFiles()
        {
            string[] files = Directory.GetFiles(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "*ToDelete", SearchOption.TopDirectoryOnly);
            for (int i = 0; i < files.Length; i++)
            {
                try
                {
                    File.Delete(files[i]);
                }
                catch
                {
                }
            }
        }

        /// <summary>
        /// Loads the game graphics.
        /// </summary>
        public static void LoadGraphics()
        {
            GraphicsManager.Initialize();

            // if (IO.IO.FileExists("GFX\\BigSpells.pmugfx")) {
            //    ((Windows.winLoading)Windows.WindowSwitcher.FindWindow("winLoading")).UpdateLoadText("Loading graphics... [Big Spells]");
            //    Graphic.GraphicsManager.BigSpells = Graphics.SurfaceManager.LoadSurface(IO.IO.GetGfxPath("BigSpells.pmugfx"));
            //    Graphic.GraphicsManager.BigSpells.TransparentColor = Graphic.GraphicsManager.BigSpells.GetPixel(new Point(0, 0));
            //    Graphic.GraphicsManager.BigSpells.Transparent = true;
            // }
            // if (IO.IO.FileExists("GFX\\Arrows.pmugfx")) {
            //    ((Windows.winLoading)Windows.WindowSwitcher.FindWindow("winLoading")).UpdateLoadText("Loading graphics... [Arrows]");
            //    Graphic.GraphicsManager.Arrows = Graphics.SurfaceManager.LoadSurface(IO.IO.GetGfxPath("Arrows.pmugfx"));
            //    Graphic.GraphicsManager.Arrows.TransparentColor = Graphic.GraphicsManager.Arrows.GetPixel(new Point(0, 0));
            //    Graphic.GraphicsManager.Arrows.Transparent = true;
            // }
            // if (IO.IO.FileExists("GFX\\BigSprites.pmugfx")) {
            //    ((Windows.winLoading)Windows.WindowSwitcher.FindWindow("winLoading")).UpdateLoadText("Loading graphics... [Big Sprites]");
            //    Graphic.GraphicsManager.BigSprites = Graphics.SurfaceManager.LoadSurface(IO.IO.GetGfxPath("BigSprites.pmugfx"));
            //    Graphic.GraphicsManager.BigSprites.TransparentColor = Graphic.GraphicsManager.BigSprites.GetPixel(new Point(0, 0));
            //    Graphic.GraphicsManager.BigSprites.Transparent = true;
            // }
            if (IO.IO.FileExists("GFX\\Items\\Items.png"))
            {
                GraphicsManager.Items = SurfaceManager.LoadSurface(IO.IO.GetGfxPath("Items\\Items.png"));
                GraphicsManager.Items.Transparent = true;
            }

            // if (IO.IO.FileExists("GFX\\Spells.pmugfx")) {
            //    ((Windows.winLoading)Windows.WindowSwitcher.FindWindow("winLoading")).UpdateLoadText("Loading graphics... [Spells]");
            //    Graphic.GraphicsManager.Spells = Graphics.SurfaceManager.LoadSurface(IO.IO.GetGfxPath("Spells.pmugfx"));
            //    Graphic.GraphicsManager.Spells.TransparentColor = Graphic.GraphicsManager.Spells.GetPixel(new Point(0, 0));
            //    Graphic.GraphicsManager.Spells.Transparent = true;
            // }
            for (int i = 0; i < GraphicsManager.MAXTILES; i++)
            {
                if (IO.IO.FileExists(IO.Paths.GfxPath + "Tiles\\Tiles" + i.ToString() + ".tile"))
                {
                    GraphicsManager.LoadTilesheet(i);
                }
            }
        }

        public static FtpWebRequest CreateFtpWebRequest(string ftpDirectoryPath, string userName, string password, bool keepAlive = false)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(ftpDirectoryPath));

            // Set proxy to null. Under current configuration if this option is not set then the proxy that is used will get an html response from the web content gateway (firewall monitoring system)
            request.Proxy = null;

            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = keepAlive;

            request.Credentials = new NetworkCredential(userName, password);

            return request;
        }
    }
}