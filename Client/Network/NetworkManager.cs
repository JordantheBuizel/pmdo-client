﻿// <copyright file="NetworkManager.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

using System.Net.Sockets;

namespace Client.Logic.Network
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Windows.Forms;
    using PMU.Core;
    using PMU.Sockets;
    using Tcp = PMU.Sockets.Tcp;
    using System.Net.Sockets;
    using System.Runtime.InteropServices;

    internal class NetworkManager
    {
        internal static PacketModifiers PacketModifiers;

        private static Tcp.TcpClient tcpClient;
        private static int lastConnectionAttempt;

        /// <summary>
        /// Gets the TCP client.
        /// </summary>
        /// <value>The TCP client.</value>
        public static Tcp.TcpClient TcpClient
        {
            get { return tcpClient; }
        }

        /// <summary>
        /// Connects to the server.
        /// </summary>
        public static void Connect()
        {
            try
            {
                string hostName = "pmdonline.servegame.com";

                if (File.Exists(Path.Combine(IO.Paths.StartupPath, "debugservertrue.dat")))
                {
                    hostName = "buizelwindows.ddns.net";
                }

                if (tcpClient.SocketState == Tcp.TcpSocketState.Idle)
                {
                    tcpClient.Connect(Constants.IP, Constants.Port);
                }
            }
            catch (Exception)
            {
            }
        }

        public static bool ShouldAttemptReconnect()
        {
            if (SdlDotNet.Core.Timer.TicksElapsed > lastConnectionAttempt + 60000)
            {
                lastConnectionAttempt = SdlDotNet.Core.Timer.TicksElapsed;
            }
else
            {
                return false;
            }

            if (tcpClient != null && tcpClient.Socket.Connected)
            {
                return false;
            }

            return false;
        }

        public static void Disconnect()
        {
            tcpClient.Close();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpManager"/> class.
        /// </summary>
        public static void InitializeTcp()
        {
            if (tcpClient != null)
            {
                if (tcpClient.Socket.Connected)
                {
                    tcpClient.Close();
                }

                tcpClient.DataReceived -= new EventHandler<Tcp.DataReceivedEventArgs>(TcpClient_DataReceived);
            }

            tcpClient = new Tcp.TcpClient();
            tcpClient.CustomHeaderSize = GetCustomPacketHeaderSize();

            int keepaliveTime = 600000;
            int keepaliveInterval = 1000;
            uint dummy = 0;
            byte[] inOptionValues = new byte[Marshal.SizeOf(dummy) * 3];
            BitConverter.GetBytes((uint)keepaliveTime).CopyTo(inOptionValues, 0);
            BitConverter.GetBytes((uint)keepaliveTime).CopyTo(inOptionValues, Marshal.SizeOf(dummy));
            BitConverter.GetBytes((uint)keepaliveInterval).CopyTo(inOptionValues, Marshal.SizeOf(dummy) * 2);
            tcpClient.Socket.IOControl(IOControlCode.KeepAliveValues, inOptionValues, null);

            tcpClient.DataReceived += TcpClient_DataReceived;

            // packetSecurity.SetKey("abcdefgh76876bfgjhgfy8u7iy");
        }

        private static int GetCustomPacketHeaderSize()
        {
            return
                1 // [byte] Compression enabled
                + 1 // [byte] Encryption enabled
                + 1 // [byte] Send as packet list
                ;
        }

        public static void InitializePacketSecurity()
        {
            PacketModifiers = new PacketModifiers();
        }

        public static void SendData(IPacket packet)
        {
            SendData(packet, false, false);
        }

        public static void SendData(IPacket packet, bool compress, bool encrypt)
        {
            SendData(ByteEncoder.StringToByteArray(packet.PacketString), compress, encrypt, false);
        }

        public static void SendData(PacketList packetList)
        {
            SendData(packetList.CombinePackets(), false, false, true);
        }

        public static void SendData(byte[] packet, bool compress, bool encrypt, bool isPacketList)
        {
            if (tcpClient != null && tcpClient.Socket.Connected)
            {
                byte[] customHeader = new byte[GetCustomPacketHeaderSize()];
                if (encrypt)
                {
                    packet = PacketModifiers.EncryptPacket(packet);
                    customHeader[1] = 1;
                }

                if (packet.Length > 2000)
                {
                    if (compress == false)
                    {
                        compress = true;
                    }
                }

                if (compress)
                {
                    packet = PacketModifiers.CompressPacket(packet);
                    customHeader[0] = 1;
                }

                if (isPacketList)
                {
                    customHeader[2] = 1;
                }
else
                {
                    customHeader[2] = 0;
                }

                tcpClient.Send(packet, customHeader);
            }
        }

        private static void TcpClient_DataReceived(object sender, Tcp.DataReceivedEventArgs e)
        {
            try
            {
                bool compression = false;
                if (e.CustomHeader[0] == 1)
                {
                    compression = true;
                }

                bool encryption = false;
                if (e.CustomHeader[1] == 1)
                {
                    encryption = true;
                }

                byte[] packetBytes = e.ByteData;
                if (compression)
                {
                    packetBytes = PacketModifiers.DecompressPacket(packetBytes);
                }

                if (encryption)
                {
                    packetBytes = PacketModifiers.DecompressPacket(packetBytes);
                }

                if (e.CustomHeader[2] == 1)
                {
                    // This was a packet list, process it
                    int position = 0;
                    while (position < packetBytes.Length)
                    {
                        int segmentSize = ByteEncoder.ByteArrayToInt(packetBytes, position);
                        position += 4;
                        MessageProcessor.HandleData(ByteEncoder.ByteArrayToString(packetBytes, position, segmentSize));
                        position += segmentSize;
                    }
                }
else
                {
                    MessageProcessor.HandleData(ByteEncoder.ByteArrayToString(packetBytes));
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Packet:");
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
        }
    }
}