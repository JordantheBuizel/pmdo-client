﻿// <copyright file="PacketModifiers.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Network
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using System.Text;
    using Security;
    using PMU.Sockets;

    internal class PacketModifiers
    {
        private Encryption crypt;
        private bool obtainedKey;

        private bool encryptionEnabled = true;

        private const string DEFAULTKEY = "abcdefgh!6876b)(gjhgfy8u7y"; // "abcdefgh76876bfgjhgfy8u7iy";

        public bool ObtainedKey
        {
            get { return this.obtainedKey; }
        }

        public PacketModifiers()
        {
            this.crypt = new Encryption();
            this.crypt.SetKey(DEFAULTKEY);
        }

        public void SetKey(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                this.encryptionEnabled = false;
            }
else
            {
                if (!this.obtainedKey)
                {
                    this.crypt = new Encryption(key);
                    this.obtainedKey = true;
                }
            }
        }

        // public string DecryptPacket(string data) {
        //    if (encryptionEnabled) {
        //        return crypt.DecryptData(data);
        //    } else {
        //        return data;
        //    }
        // }
        public byte[] DecryptPacket(byte[] packet)
        {
            return this.crypt.DecryptBytes(packet);
        }

        // public string EncryptPacket(IPacket packet) {
        //    if (encryptionEnabled) {
        //        return crypt.EncryptData(packet.PacketString);
        //    } else {
        //        return packet.PacketString;
        //    }
        // }
        public byte[] EncryptPacket(byte[] packet)
        {
            return this.crypt.EncryptBytes(packet);
        }

        public byte[] CompressPacket(byte[] packet)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(ms, CompressionMode.Compress, true))
                {
                    gzip.Write(packet, 0, packet.Length);
                }

                return ms.ToArray();
            }
        }

        public byte[] DecompressPacket(byte[] packet)
        {
            // Create a GZIP stream with decompression mode.
            // ... Then create a buffer and write into while reading from the GZIP stream.
            using (GZipStream stream = new GZipStream(new MemoryStream(packet), CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    return memory.ToArray();
                }
            }
        }
    }
}
