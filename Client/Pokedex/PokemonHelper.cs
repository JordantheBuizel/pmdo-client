﻿// <copyright file="PokemonHelper.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Pokedex
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Core;

    internal class PokemonHelper
    {
        private static int dataLoadPercent = 0;
        private static PokemonCollection mPokemon;

        public static int DataLoadPercent
        {
            get { return dataLoadPercent; }
        }

        public static PokemonCollection Pokemon
        {
            get { return mPokemon; }
        }

        public static void InitPokemonCollection()
        {
            mPokemon = new PokemonCollection();
        }

        public static void LoadPokemonFromPacket(string[] parse)
        {
            try
            {
                int n = 2;
                MaxInfo.TotalPokemon = parse[1].ToInt();
                mPokemon.ClearPokemon();
                if (MaxInfo.TotalPokemon > 0)
                {
                    for (int i = 0; i < MaxInfo.TotalPokemon; i++)
                    {
                        dataLoadPercent = Math.Min(99, Logic.MathFunctions.CalculatePercent(i, MaxInfo.TotalPokemon));
                        mPokemon.AddPokemon(i, new Pokemon());
                        mPokemon[i].Name = parse[n];
                        int count = parse[n + 1].ToInt();
                        n += 2;

                        // for (int j = 0; j < count; j++) {
                        //    mPokemon[i].FormSprites.Add(parse[n + j].ToInt());
                        // }
                        // n += count;
                        ((Windows.WinLoading)Windows.WindowSwitcher.FindWindow("winLoading")).UpdateLoadText("Receiving Data... " + DataManager.AverageLoadPercent().ToString() + "%");
                    }

                    dataLoadPercent = 100;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionHandler.OnException(ex);
            }
        }
    }
}
