﻿// <copyright file="PokemonCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Pokedex
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Description of PokemonCollection.
    /// </summary>
    internal class PokemonCollection
    {
        private readonly PMU.Core.ListPair<int, Pokemon> mPokemon;

        internal PokemonCollection()
        {
            this.mPokemon = new PMU.Core.ListPair<int, Pokemon>();
        }

        public Pokemon this[int index]
        {
            get { return this.mPokemon[index]; }
            set { this.mPokemon[index] = value; }
        }

        public void AddPokemon(int index, Pokemon pokemonToAdd)
        {
            this.mPokemon.Add(index, pokemonToAdd);
        }

        public void ClearPokemon()
        {
            this.mPokemon.Clear();
        }
    }
}
