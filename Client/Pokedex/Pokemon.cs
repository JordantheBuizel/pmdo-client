﻿// <copyright file="Pokemon.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Pokedex
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Description of Pokedex.
    /// </summary>
    internal class Pokemon
    {
        public Pokemon()
        {
            // FormSprites = new List<int>();
        }

        public string Name
        {
            get;
            set;
        }

        // public List<int> FormSprites {
        //    get;
        //    set;
        // }
    }
}
