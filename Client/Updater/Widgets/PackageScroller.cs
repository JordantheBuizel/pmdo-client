﻿// <copyright file="PackageScroller.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Updater.Widgets
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using PMU.Updater.Linker;
    using SdlDotNet.Widgets;

    internal class PackageScroller : Panel
    {
        private readonly List<PackageButton> buttons;
        private readonly VScrollBar vScroll;
        private readonly int maxVisibleButtons;
        private bool locked;

        public List<PackageButton> Buttons
        {
            get { return this.buttons; }
        }

        public bool Locked
        {
            get { return this.locked; }
            set { this.locked = value; }
        }

        public event EventHandler<PackageButtonSelectedEventArgs> PackageButtonSelected;

        public PackageScroller(string name)
            : base(name)
            {
            this.Size = new Size(212, 300);
            this.BackColor = Color.White;
            this.vScroll = new VScrollBar("vScroll");
            this.vScroll.BackColor = Color.Transparent;
            this.vScroll.Size = new Size(12, this.Height);
            this.vScroll.Location = new Point(this.Width - this.vScroll.Width, 0);
            this.vScroll.Minimum = 1;
            this.vScroll.Value = 1;
            this.vScroll.ValueChanged += new EventHandler<ValueChangedEventArgs>(this.VScroll_ValueChanged);

            this.buttons = new List<PackageButton>();
            this.maxVisibleButtons = (this.Height / PackageButton.BUTTONHEIGHT) - 1;

            this.AddWidget(this.vScroll);
        }

        private void VScroll_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.VerifyButtons();
        }

        private void VerifyButtons()
        {
            int lastY = 0;
            for (int i = 0; i < this.buttons.Count; i++)
            {
                if (this.IsButtonVisible(this.buttons[i]))
                {
                    this.buttons[i].Location = new Point(0, lastY * PackageButton.BUTTONHEIGHT);
                    this.buttons[i].Visible = true;
                    lastY++;
                }
else
                {
                    this.buttons[i].Visible = false;
                }
            }
        }

        public void AddPackage(IPackageInfo package)
        {
            PackageButton button = new PackageButton("button" + this.buttons.Count, package);
            button.Click += new EventHandler<MouseButtonEventArgs>(this.Button_Click);
            this.buttons.Add(button);
            this.vScroll.Maximum = Math.Max(1, this.buttons.Count - this.maxVisibleButtons);
            this.AddWidget(button);
            this.VerifyButtons();
        }

        private void Button_Click(object sender, MouseButtonEventArgs e)
        {
            if (!this.locked)
            {
                for (int i = 0; i < this.buttons.Count; i++)
                {
                    if (this.buttons[i] != (PackageButton)sender)
                    {
                        this.buttons[i].Selected = false;
                    }
else
                    {
                        this.buttons[i].Selected = true;
                    }
                }
            }

            if (this.PackageButtonSelected != null)
            {
                this.PackageButtonSelected(this, new PackageButtonSelectedEventArgs((PackageButton)sender));
            }
        }

        public void DeselectAll()
        {
            for (int i = 0; i < this.buttons.Count; i++)
            {
                this.buttons[i].Selected = false;
            }
        }

        public void ScrollToButton(int index)
        {
            this.DeselectAll();
            this.vScroll.Value = index - (this.maxVisibleButtons - 1);
            this.buttons[index].Selected = true;
            if (this.PackageButtonSelected != null)
            {
                this.PackageButtonSelected(this, new PackageButtonSelectedEventArgs(this.buttons[index]));
            }
        }

        private bool IsButtonVisible(PackageButton button)
        {
            int index = this.buttons.IndexOf(button) + 1;
            if (index >= this.vScroll.Value && index <= this.vScroll.Value + this.maxVisibleButtons)
            {
                return true;
            }
else
            {
                return false;
            }
        }
    }
}
