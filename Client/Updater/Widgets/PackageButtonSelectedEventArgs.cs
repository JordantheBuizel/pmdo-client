﻿// <copyright file="PackageButtonSelectedEventArgs.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Updater.Widgets
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class PackageButtonSelectedEventArgs:EventArgs
    {
        private readonly PackageButton packageButton;

        public PackageButtonSelectedEventArgs(PackageButton packageButton)
        {
            this.packageButton = packageButton;
        }

        public PackageButton PackageButton
        {
            get { return this.packageButton; }
        }
    }
}
