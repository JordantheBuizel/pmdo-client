﻿// <copyright file="PackageButton.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Updater.Widgets
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Extensions;
    using PMU.Updater.Linker;
    using SdlDotNet.Graphics;
    using SdlDotNet.Widgets;

    internal class PackageButton : Button
    {
        public const int BUTTONHEIGHT = 50;
        private IPackageInfo attachedPackage;
        private readonly Surface installedSurf;
        private bool installed;

        public IPackageInfo AttachedPackage
        {
            get { return this.attachedPackage; }
            set { this.attachedPackage = value; }
        }

        public PackageButton(string name, IPackageInfo attachedPackage)
            : base(name)
            {
            this.attachedPackage = attachedPackage;
            this.Size = new Size(200, 50);
            this.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.BackColor = Color.SteelBlue;
            this.HighlightType = HighlightType.Color;
            this.HighlightColor = Color.LightSteelBlue;

            this.installedSurf = Graphic.SurfaceManager.LoadSurface(IO.Paths.GfxPath + "Updater\\package-installed.png");

            this.Text = attachedPackage.FullID;

            this.Paint += new EventHandler(this.PackageButton_Paint);
        }

        public bool Installed
        {
            get { return this.installed; }

            set
            {
                this.installed = value;
                this.RequestRedraw();
            }
        }

        private void PackageButton_Paint(object sender, EventArgs e)
        {
            if (this.installed)
            {
                if (this.installedSurf != null)
                {
                    this.Buffer.Blit(this.installedSurf, new Point(5, (this.Height / 2) - (this.installedSurf.Height / 2)));
                }
            }
        }
    }
}
