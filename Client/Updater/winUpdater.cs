﻿// <copyright file="winUpdater.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Updater
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using System.Threading;
    using Extensions;
    using Widgets;
    using PMU.Updater.Linker;
    using SdlDotNet.Widgets;

    internal class WinUpdater : Window
    {
        private readonly Label lblUpdateFound;
        private readonly PackageScroller packageScroller;
        private readonly Label lblUpdateInfo;
        private readonly Label lblStatus;
        private readonly Button btnAccept;
        private readonly Button btnDecline;
        private readonly SdlDotNet.Widgets.Timer tmrRestart;
        private readonly ProgressBar pgbDownloadProgress;

        private UpdateEngine updateEngine;

        public UpdateEngine UpdateEngine
        {
            get { return this.updateEngine; }
            set { this.updateEngine = value; }
        }

        public WinUpdater(UpdateEngine updateEngine)
            : base("winUpdater")
            {
            this.updateEngine = updateEngine;
            this.updateEngine.Updater.StatusUpdated += new EventHandler(this.Updater_StatusUpdated);
            this.updateEngine.Updater.PackageDownloadStart += new EventHandler<PMU.Updater.PackageDownloadStartEventArgs>(this.Updater_PackageDownloadStart);
            this.updateEngine.Updater.PackageInstallationComplete += new EventHandler<PMU.Updater.PackageInstallationCompleteEventArgs>(this.Updater_PackageInstallationComplete);
            this.updateEngine.Updater.InstallationComplete += new EventHandler(this.Updater_InstallationComplete);
            this.Windowed = true;
            this.TitleBar.Text = "Updater";
            this.TitleBar.Font = Graphic.FontManager.LoadFont("tahoma", 10);

            this.Size = new Size(400, 400);
            this.BackColor = Color.White;
            this.Location = DrawingSupport.GetCenter(WindowManager.ScreenSize, this.Size);
            this.lblUpdateFound = new Label("lblUpdateFound");
            this.lblUpdateFound.Location = new Point(10, 300);
            this.lblUpdateFound.AutoSize = true;
            this.lblUpdateFound.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.lblUpdateFound.Text = "An update has been found! Would you like to install this update?";

            this.packageScroller = new PackageScroller("packageScroller");
            this.packageScroller.Location = new Point(0, 0);

            for (int i = 0; i < updateEngine.LastCheckResult.PackagesToUpdate.Count; i++)
            {
                this.packageScroller.AddPackage(updateEngine.LastCheckResult.PackagesToUpdate[i]);
            }

            this.packageScroller.PackageButtonSelected += new EventHandler<PackageButtonSelectedEventArgs>(this.PackageScroller_PackageButtonSelected);

            this.lblUpdateInfo = new Label("lblUpdateInfo");
            this.lblUpdateInfo.Size = new Size(188, 300);
            this.lblUpdateInfo.Location = new Point(212, 0);
            this.lblUpdateInfo.WordWrap = true;
            this.lblUpdateInfo.Font = Graphic.FontManager.LoadFont("tahoma", 12);

            this.lblStatus = new Label("lblStatus");
            this.lblStatus.Location = new Point(this.lblUpdateFound.Location.X, this.lblUpdateFound.Location.Y + this.lblUpdateFound.Height + 5);
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.lblStatus.Visible = false;

            this.lblStatus = new Label("lblStatus");
            this.lblStatus.Location = new Point(this.lblUpdateFound.Location.X, this.lblUpdateFound.Location.Y + this.lblUpdateFound.Height + 5);
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.lblStatus.Visible = false;

            this.pgbDownloadProgress = new ProgressBar("pgbDownloadProgress");
            this.pgbDownloadProgress.Location = new Point(this.lblUpdateFound.Location.X, this.lblUpdateFound.Location.Y + this.lblUpdateFound.Height + 5);
            this.pgbDownloadProgress.Size = new Size(this.Width - (this.lblUpdateFound.Location.X * 2), 20);
            this.pgbDownloadProgress.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.pgbDownloadProgress.Visible = false;
            this.pgbDownloadProgress.TextStyle = ProgressBarTextStyle.Percent;

            this.btnAccept = new Button("btnAccept");
            this.btnAccept.Text = "Yes";
            this.btnAccept.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.btnAccept.Size = new Size(100, 20);
            this.btnAccept.Location = new Point(this.lblUpdateFound.Location.X, this.lblUpdateFound.Location.Y + this.lblUpdateFound.Height + 5);
            this.btnAccept.Click += new EventHandler<MouseButtonEventArgs>(this.BtnAccept_Click);

            this.btnDecline = new Button("btnDecline");
            this.btnDecline.Text = "No";
            this.btnDecline.Font = Graphic.FontManager.LoadFont("tahoma", 12);
            this.btnDecline.Size = new Size(100, 20);
            this.btnDecline.Location = new Point(this.lblUpdateFound.Location.X + this.btnAccept.Width + 5, this.lblUpdateFound.Location.Y + this.lblUpdateFound.Height + 5);
            this.btnDecline.Click += new EventHandler<MouseButtonEventArgs>(this.BtnDecline_Click);

            this.tmrRestart = new SdlDotNet.Widgets.Timer("restartTimer");
            this.tmrRestart.Interval = 1000;
            this.tmrRestart.Elapsed += new EventHandler(this.TmrRestart_Elapsed);

            this.AddWidget(this.packageScroller);
            this.AddWidget(this.lblUpdateInfo);
            this.AddWidget(this.lblUpdateFound);
            this.AddWidget(this.btnAccept);
            this.AddWidget(this.btnDecline);
            this.AddWidget(this.lblStatus);
            this.AddWidget(this.pgbDownloadProgress);
            this.AddWidget(this.tmrRestart);

            this.LoadComplete();

            this.packageScroller.ScrollToButton(0);
            this.LoadPackageInfo(this.packageScroller.Buttons[0]);
        }

        private int restartCountdown;

        private void TmrRestart_Elapsed(object sender, EventArgs e)
        {
            this.restartCountdown--;
            if (this.restartCountdown <= 0)
            {
                this.RestartApplication();
            }
else
            {
                this.UpdateStatus("Update complete! This program will restart in " + this.restartCountdown + "...");
            }
        }

        private void RestartApplication()
        {
            // Restarting!
            // Get the parameters/arguments passed to program if any
            string arguments = string.Empty;
            string[] args = Environment.GetCommandLineArgs();
            for (int i = 1; i < args.Length; i++) // args[0] is always exe path/filename
            {
                arguments += args[i] + " ";
            }

            // Restart current application, with same arguments/parameters
            System.Diagnostics.Process.Start(System.Reflection.Assembly.GetEntryAssembly().Location, arguments);
            Sdl.SdlCore.QuitApplication();
        }

        private void Updater_InstallationComplete(object sender, EventArgs e)
        {
            this.UpdateStatus("Update complete! This program will restart in 5...");
            this.restartCountdown = 5;
            this.tmrRestart.Start();
        }

        private void Updater_PackageInstallationComplete(object sender, PMU.Updater.PackageInstallationCompleteEventArgs e)
        {
            this.packageScroller.Buttons[e.PackageIndex].Installed = true;
            if (this.updateEngine.LastCheckResult.PackagesToUpdate.Count != e.PackageIndex + 1)
            {
                this.packageScroller.ScrollToButton(e.PackageIndex + 1);
                this.LoadPackageInfo(this.packageScroller.Buttons[e.PackageIndex + 1]);
            }
        }

        private void Updater_PackageDownloadStart(object sender, PMU.Updater.PackageDownloadStartEventArgs e)
        {
            this.lblUpdateFound.Text = "Package: " + e.Package.FullID;
            this.lblStatus.Hide();
            this.pgbDownloadProgress.Show();

            // UpdateStatus("Downloading...");
            e.Download.DownloadUpdate += new EventHandler<FileDownloadingEventArgs>(this.Download_DownloadUpdate);
            e.Download.DownloadComplete += new EventHandler<FileDownloadingEventArgs>(this.Download_DownloadComplete);
        }

        private void Download_DownloadComplete(object sender, FileDownloadingEventArgs e)
        {
            this.pgbDownloadProgress.Hide();
            this.lblStatus.Show();
            this.UpdateStatus("Installing...");
        }

        private void Download_DownloadUpdate(object sender, FileDownloadingEventArgs e)
        {
            this.pgbDownloadProgress.Value = e.Percent;

            // UpdateStatus("Downloading: " + PMU.Core.IO.Files.GetFileSize(e.Position) + "/" + PMU.Core.IO.Files.GetFileSize(e.FileSize) + " (" + e.Percent + "%)");
        }

        private void BtnAccept_Click(object sender, MouseButtonEventArgs e)
        {
            this.btnAccept.Visible = false;
            this.btnDecline.Visible = false;
            this.lblStatus.Visible = true;
            Thread updateThread = new Thread(new ThreadStart(delegate()
            {
                this.updateEngine.Updater.PerformUpdate(this.updateEngine.LastCheckResult);
            }));
            updateThread.Start();
        }

        private void BtnDecline_Click(object sender, MouseButtonEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Updater_StatusUpdated(object sender, EventArgs e)
        {
            this.UpdateStatus(this.updateEngine.Updater.Status);
        }

        private void UpdateStatus(string status)
        {
            this.lblStatus.Text = status;
        }

        private void PackageScroller_PackageButtonSelected(object sender, PackageButtonSelectedEventArgs e)
        {
            this.LoadPackageInfo(e.PackageButton);
        }

        private void LoadPackageInfo(PackageButton packageButton)
        {
            this.lblUpdateInfo.Text = string.Empty;
            CharRenderOptions renderOptions = new CharRenderOptions(Color.Black);
            renderOptions.Bold = true;
            this.lblUpdateInfo.AppendText("Package: " + packageButton.AttachedPackage.FullID + "\n", renderOptions);
            this.lblUpdateInfo.AppendText(
                "Name: " + packageButton.AttachedPackage.Name + "\nSize: " + PMU.Core.IO.Files.GetFileSize(packageButton.AttachedPackage.Size) +
                "\nPublish Date: " + packageButton.AttachedPackage.PublishDate.ToLongDateString() + "\n\nDescription:\n" + packageButton.AttachedPackage.Description, new CharRenderOptions(Color.Black));
        }
    }
}
