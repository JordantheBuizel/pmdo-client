﻿// <copyright file="UpdateEngine.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Updater
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PMU.Net;
    using PMU.Updater;
    using PMU.Updater.Linker;

    internal class UpdateEngine
    {
        private readonly UpdateClient updater;
        private IUpdateCheckResult lastCheckResult;

        public readonly string UpdateURL = IO.Options.UpdateAddress;

        public UpdateClient Updater
        {
            get { return this.updater; }
        }

        public IUpdateCheckResult LastCheckResult
        {
            get { return this.lastCheckResult; }
        }

        public UpdateEngine(string packageListKey)
        {
            this.updater = new UpdateClient(packageListKey, System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\"); // System.Windows.Forms.Application.StartupPath + "\\");
        }

        public bool CheckForUpdates()
        {
            // if (NetTools.IsConnected()) {
            // Load the installed package list
            this.updater.LoadInstalledPackageList();
            this.updater.InstallationComplete += new EventHandler(this.Updater_InstallationComplete);
            this.lastCheckResult = this.updater.CheckForUpdates(this.UpdateURL);
            return this.lastCheckResult.PackagesToUpdate.Count != 0;

            // } else {
            //    return false;
            // }
        }

        public void PerformUpdate(IUpdateCheckResult result)
        {
            if (result.PackagesToUpdate.Count != 0)
            {
                this.updater.PerformUpdate(result);
            }
        }

        // public void PerformUpdate() {
        //    // Load the installed package list
        //    updater.LoadInstalledPackageList();
        //    updater.InstallationComplete += new EventHandler(updater_InstallationComplete);
        //    updater.PackageDownloadStart += new EventHandler<PackageDownloadStartEventArgs>(updater_PackageDownloadStart);
        //    IUpdateCheckResult result = updater.CheckForUpdates("http://www.pmuniverse.net/PMU/Updates/Map%20Editor%203.1/UpdateData.xml");
        //    if (result.PackagesToUpdate.Count != 0) {
        //        updater.PerformUpdate(result);
        //    } else {
        //        System.Windows.Forms.MessageBox.Show("No updates found!");
        //        Environment.Exit(0);
        //    }
        // }
        private void Updater_InstallationComplete(object sender, EventArgs e)
        {
            // System.Windows.Forms.MessageBox.Show("Update complete!");
            // Environment.Exit(0);
        }
    }
}
