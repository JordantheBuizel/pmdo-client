﻿// <copyright file="MissionCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Missions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class MissionCollection
    {
        private readonly PMU.Core.ListPair<int, Mission> mMissions;

        internal MissionCollection()
        {
            this.mMissions = new PMU.Core.ListPair<int, Mission>();
        }

        public Mission this[int index]
        {
            get { return this.mMissions[index]; }
            set { this.mMissions[index] = value; }
        }

        public void AddMission(int index, Mission missionToAdd)
        {
            this.mMissions.Add(index, missionToAdd);
        }

        public void ClearMissions()
        {
            this.mMissions.Clear();
        }
    }
}