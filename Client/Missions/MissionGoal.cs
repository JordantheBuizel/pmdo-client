﻿// <copyright file="MissionGoal.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Missions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class MissionGoal
    {
        // public string GoalMapID { get; set; }
        public int GoalX { get; set; }

        public int GoalY { get; set; }

        public int JobListSlot { get; set; }
    }
}
