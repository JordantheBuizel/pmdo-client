﻿// <copyright file="Move.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Moves
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Move
    {
        public string Name
        {
            get;
            set;
        }

        public Enums.MoveRange RangeType
        {
            get;
            set;
        }

        public int Range
        {
            get;
            set;
        }

        public Enums.MoveTarget TargetType
        {
            get;
            set;
        }

        public int HitTime
        {
            get;
            set;
        }

        public bool HitFreeze
        {
            get;
            set;
        }
    }
}