﻿// <copyright file="MoveCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Moves
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class MoveCollection
    {
        private readonly Move[] mMoves;

        internal MoveCollection(int maxMoves)
        {
            this.mMoves = new Move[maxMoves + 1];
        }

        public Move this[int index]
        {
            get { return this.mMoves[index]; }
            set { this.mMoves[index] = value; }
        }
    }
}