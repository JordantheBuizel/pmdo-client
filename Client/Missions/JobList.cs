﻿// <copyright file="JobList.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Missions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class JobList
    {
        private readonly List<Job> jobs;

        public JobList()
        {
            this.jobs = new List<Job>();
        }

        public List<Job> Jobs
        {
            get
            {
                return this.jobs;
            }
        }
    }
}
