﻿// <copyright file="Emotion.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Emotions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Emotion
    {
        public string Command
        {
            get; set;
        }

        public int Pic
        {
            get; set;
        }
    }
}