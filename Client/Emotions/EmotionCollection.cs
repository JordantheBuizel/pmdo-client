﻿// <copyright file="EmotionCollection.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Emotions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class EmotionCollection
    {
        private readonly Emotion[] mEmotions;

        internal EmotionCollection(int maxEmotions)
        {
            this.mEmotions = new Emotion[maxEmotions];
        }

        public Emotion this[int index]
        {
            get { return this.mEmotions[index]; }
            set { this.mEmotions[index] = value; }
        }
    }
}