﻿// <copyright file="ActiveRecruitChangedEventArgs.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Events
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class ActiveRecruitChangedEventArgs : EventArgs
    {
        private readonly int newSlot;

        public int NewSlot
        {
            get { return this.newSlot; }
        }

        public ActiveRecruitChangedEventArgs(int newSlot)
        {
            this.newSlot = newSlot;
        }
    }
}
